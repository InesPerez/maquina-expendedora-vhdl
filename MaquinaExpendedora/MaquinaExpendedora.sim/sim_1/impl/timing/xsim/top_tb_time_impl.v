// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Sat Dec 26 13:25:26 2020
// Host        : LAPTOP-7BL7BHFF running 64-bit major release  (build 9200)
// Command     : write_verilog -mode timesim -nolib -sdf_anno true -force -file
//               C:/Users/Inees/Desktop/Trabajo_SED/MaquinaExpendedora_aux/MaquinaExpendedora_aux.sim/sim_1/impl/timing/xsim/top_tb_time_impl.v
// Design      : top
// Purpose     : This verilog netlist is a timing simulation representation of the design and should not be modified or
//               synthesized. Please ensure that this netlist is used with the corresponding SDF file.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps
`define XIL_TIMING

module contador
   (reset,
    clk_pre_reg,
    clk_100Mh_IBUF_BUFG,
    reset_IBUF,
    clk);
  output reset;
  output clk_pre_reg;
  input clk_100Mh_IBUF_BUFG;
  input reset_IBUF;
  input clk;

  wire clk;
  wire clk_100Mh_IBUF_BUFG;
  wire clk_pre_i_2_n_0;
  wire clk_pre_i_3_n_0;
  wire clk_pre_i_4_n_0;
  wire clk_pre_reg;
  wire \reg[0]_i_2_n_0 ;
  wire [16:0]reg_reg;
  wire \reg_reg[0]_i_1_n_0 ;
  wire \reg_reg[0]_i_1_n_4 ;
  wire \reg_reg[0]_i_1_n_5 ;
  wire \reg_reg[0]_i_1_n_6 ;
  wire \reg_reg[0]_i_1_n_7 ;
  wire \reg_reg[12]_i_1_n_0 ;
  wire \reg_reg[12]_i_1_n_4 ;
  wire \reg_reg[12]_i_1_n_5 ;
  wire \reg_reg[12]_i_1_n_6 ;
  wire \reg_reg[12]_i_1_n_7 ;
  wire \reg_reg[16]_i_1_n_7 ;
  wire \reg_reg[4]_i_1_n_0 ;
  wire \reg_reg[4]_i_1_n_4 ;
  wire \reg_reg[4]_i_1_n_5 ;
  wire \reg_reg[4]_i_1_n_6 ;
  wire \reg_reg[4]_i_1_n_7 ;
  wire \reg_reg[8]_i_1_n_0 ;
  wire \reg_reg[8]_i_1_n_4 ;
  wire \reg_reg[8]_i_1_n_5 ;
  wire \reg_reg[8]_i_1_n_6 ;
  wire \reg_reg[8]_i_1_n_7 ;
  wire reset;
  wire reset_IBUF;
  wire [2:0]\NLW_reg_reg[0]_i_1_CO_UNCONNECTED ;
  wire [2:0]\NLW_reg_reg[12]_i_1_CO_UNCONNECTED ;
  wire [3:0]\NLW_reg_reg[16]_i_1_CO_UNCONNECTED ;
  wire [3:1]\NLW_reg_reg[16]_i_1_O_UNCONNECTED ;
  wire [2:0]\NLW_reg_reg[4]_i_1_CO_UNCONNECTED ;
  wire [2:0]\NLW_reg_reg[8]_i_1_CO_UNCONNECTED ;

  LUT4 #(
    .INIT(16'h7F80)) 
    clk_pre_i_1
       (.I0(clk_pre_i_2_n_0),
        .I1(clk_pre_i_3_n_0),
        .I2(clk_pre_i_4_n_0),
        .I3(clk),
        .O(clk_pre_reg));
  LUT6 #(
    .INIT(64'h0080000000000000)) 
    clk_pre_i_2
       (.I0(reg_reg[13]),
        .I1(reg_reg[14]),
        .I2(reg_reg[11]),
        .I3(reg_reg[12]),
        .I4(reg_reg[16]),
        .I5(reg_reg[15]),
        .O(clk_pre_i_2_n_0));
  LUT5 #(
    .INIT(32'h00000080)) 
    clk_pre_i_3
       (.I0(reg_reg[0]),
        .I1(reg_reg[1]),
        .I2(reg_reg[2]),
        .I3(reg_reg[4]),
        .I4(reg_reg[3]),
        .O(clk_pre_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    clk_pre_i_4
       (.I0(reg_reg[7]),
        .I1(reg_reg[8]),
        .I2(reg_reg[6]),
        .I3(reg_reg[5]),
        .I4(reg_reg[10]),
        .I5(reg_reg[9]),
        .O(clk_pre_i_4_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \current_state[1]_i_1 
       (.I0(reset_IBUF),
        .O(reset));
  LUT1 #(
    .INIT(2'h1)) 
    \reg[0]_i_2 
       (.I0(reg_reg[0]),
        .O(\reg[0]_i_2_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \reg_reg[0] 
       (.C(clk_100Mh_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset),
        .D(\reg_reg[0]_i_1_n_7 ),
        .Q(reg_reg[0]));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \reg_reg[0]_i_1 
       (.CI(1'b0),
        .CO({\reg_reg[0]_i_1_n_0 ,\NLW_reg_reg[0]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\reg_reg[0]_i_1_n_4 ,\reg_reg[0]_i_1_n_5 ,\reg_reg[0]_i_1_n_6 ,\reg_reg[0]_i_1_n_7 }),
        .S({reg_reg[3:1],\reg[0]_i_2_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \reg_reg[10] 
       (.C(clk_100Mh_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset),
        .D(\reg_reg[8]_i_1_n_5 ),
        .Q(reg_reg[10]));
  FDCE #(
    .INIT(1'b0)) 
    \reg_reg[11] 
       (.C(clk_100Mh_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset),
        .D(\reg_reg[8]_i_1_n_4 ),
        .Q(reg_reg[11]));
  FDCE #(
    .INIT(1'b0)) 
    \reg_reg[12] 
       (.C(clk_100Mh_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset),
        .D(\reg_reg[12]_i_1_n_7 ),
        .Q(reg_reg[12]));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \reg_reg[12]_i_1 
       (.CI(\reg_reg[8]_i_1_n_0 ),
        .CO({\reg_reg[12]_i_1_n_0 ,\NLW_reg_reg[12]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\reg_reg[12]_i_1_n_4 ,\reg_reg[12]_i_1_n_5 ,\reg_reg[12]_i_1_n_6 ,\reg_reg[12]_i_1_n_7 }),
        .S(reg_reg[15:12]));
  FDCE #(
    .INIT(1'b0)) 
    \reg_reg[13] 
       (.C(clk_100Mh_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset),
        .D(\reg_reg[12]_i_1_n_6 ),
        .Q(reg_reg[13]));
  FDCE #(
    .INIT(1'b0)) 
    \reg_reg[14] 
       (.C(clk_100Mh_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset),
        .D(\reg_reg[12]_i_1_n_5 ),
        .Q(reg_reg[14]));
  FDCE #(
    .INIT(1'b0)) 
    \reg_reg[15] 
       (.C(clk_100Mh_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset),
        .D(\reg_reg[12]_i_1_n_4 ),
        .Q(reg_reg[15]));
  FDCE #(
    .INIT(1'b0)) 
    \reg_reg[16] 
       (.C(clk_100Mh_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset),
        .D(\reg_reg[16]_i_1_n_7 ),
        .Q(reg_reg[16]));
  CARRY4 \reg_reg[16]_i_1 
       (.CI(\reg_reg[12]_i_1_n_0 ),
        .CO(\NLW_reg_reg[16]_i_1_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_reg_reg[16]_i_1_O_UNCONNECTED [3:1],\reg_reg[16]_i_1_n_7 }),
        .S({1'b0,1'b0,1'b0,reg_reg[16]}));
  FDCE #(
    .INIT(1'b0)) 
    \reg_reg[1] 
       (.C(clk_100Mh_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset),
        .D(\reg_reg[0]_i_1_n_6 ),
        .Q(reg_reg[1]));
  FDCE #(
    .INIT(1'b0)) 
    \reg_reg[2] 
       (.C(clk_100Mh_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset),
        .D(\reg_reg[0]_i_1_n_5 ),
        .Q(reg_reg[2]));
  FDCE #(
    .INIT(1'b0)) 
    \reg_reg[3] 
       (.C(clk_100Mh_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset),
        .D(\reg_reg[0]_i_1_n_4 ),
        .Q(reg_reg[3]));
  FDCE #(
    .INIT(1'b0)) 
    \reg_reg[4] 
       (.C(clk_100Mh_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset),
        .D(\reg_reg[4]_i_1_n_7 ),
        .Q(reg_reg[4]));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \reg_reg[4]_i_1 
       (.CI(\reg_reg[0]_i_1_n_0 ),
        .CO({\reg_reg[4]_i_1_n_0 ,\NLW_reg_reg[4]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\reg_reg[4]_i_1_n_4 ,\reg_reg[4]_i_1_n_5 ,\reg_reg[4]_i_1_n_6 ,\reg_reg[4]_i_1_n_7 }),
        .S(reg_reg[7:4]));
  FDCE #(
    .INIT(1'b0)) 
    \reg_reg[5] 
       (.C(clk_100Mh_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset),
        .D(\reg_reg[4]_i_1_n_6 ),
        .Q(reg_reg[5]));
  FDCE #(
    .INIT(1'b0)) 
    \reg_reg[6] 
       (.C(clk_100Mh_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset),
        .D(\reg_reg[4]_i_1_n_5 ),
        .Q(reg_reg[6]));
  FDCE #(
    .INIT(1'b0)) 
    \reg_reg[7] 
       (.C(clk_100Mh_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset),
        .D(\reg_reg[4]_i_1_n_4 ),
        .Q(reg_reg[7]));
  FDCE #(
    .INIT(1'b0)) 
    \reg_reg[8] 
       (.C(clk_100Mh_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset),
        .D(\reg_reg[8]_i_1_n_7 ),
        .Q(reg_reg[8]));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \reg_reg[8]_i_1 
       (.CI(\reg_reg[4]_i_1_n_0 ),
        .CO({\reg_reg[8]_i_1_n_0 ,\NLW_reg_reg[8]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\reg_reg[8]_i_1_n_4 ,\reg_reg[8]_i_1_n_5 ,\reg_reg[8]_i_1_n_6 ,\reg_reg[8]_i_1_n_7 }),
        .S(reg_reg[11:8]));
  FDCE #(
    .INIT(1'b0)) 
    \reg_reg[9] 
       (.C(clk_100Mh_IBUF_BUFG),
        .CE(1'b1),
        .CLR(reset),
        .D(\reg_reg[8]_i_1_n_6 ),
        .Q(reg_reg[9]));
endmodule

(* ORIG_REF_NAME = "contador" *) 
module contador__parameterized1
   (\current_state_reg[0] ,
    \charact_dot_reg[0] ,
    \current_state_reg[1] ,
    D,
    \reg_reg[1]_0 ,
    \reg_reg[2]_0 ,
    \reg_reg[1]_1 ,
    \charact_dot_reg[0]_0 ,
    \reg_reg[1]_2 ,
    \reg_reg[0]_0 ,
    \charact_dot_reg[0]_1 ,
    \reg_reg[2]_1 ,
    plusOp,
    \charact_dot_reg[0]_2 ,
    \charact_dot_reg[0]_3 ,
    \reg_reg[2]_2 ,
    \reg_reg[1]_3 ,
    \charact_dot_reg[0]_4 ,
    elem_OBUF,
    \charact_dot_reg[0]_5 ,
    \current_state_reg[0]_0 ,
    CO,
    \charact_dot_reg[0]_6 ,
    \disp_dinero[1]_0 ,
    \disp_dinero[3]_1 ,
    \caracter_reg[3] ,
    Q,
    \caracter_reg[6] ,
    \caracter_reg[4] ,
    \caracter_reg[2] ,
    \caracter_reg[2]_0 ,
    \caracter_reg[4]_0 ,
    \caracter_reg[4]_1 ,
    \caracter_reg[6]_0 ,
    \caracter[4]_i_2_0 ,
    \caracter_reg[4]_2 ,
    \caracter_reg[0] ,
    \caracter_reg[0]_0 ,
    \disp_dinero[5]_2 ,
    cent,
    \caracter_reg[2]_1 ,
    \caracter_reg[1] ,
    \disp_dinero[2]_3 ,
    \caracter_reg[6]_1 ,
    \caracter_reg[3]_0 ,
    \caracter_reg[3]_1 ,
    \caracter_reg[3]_2 ,
    \caracter[4]_i_2_1 ,
    dot_reg,
    \caracter[6]_i_5_0 ,
    \caracter_reg[3]_3 ,
    \caracter[2]_i_6_0 ,
    reset_IBUF,
    \charact_dot_reg[0]_7 ,
    \caracter[2]_i_5_0 ,
    dot_reg_0,
    dot_reg_1,
    clk_BUFG,
    output0);
  output \current_state_reg[0] ;
  output \charact_dot_reg[0] ;
  output \current_state_reg[1] ;
  output [4:0]D;
  output \reg_reg[1]_0 ;
  output [2:0]\reg_reg[2]_0 ;
  output \reg_reg[1]_1 ;
  output \charact_dot_reg[0]_0 ;
  output \reg_reg[1]_2 ;
  output \reg_reg[0]_0 ;
  output \charact_dot_reg[0]_1 ;
  output \reg_reg[2]_1 ;
  output [0:0]plusOp;
  output \charact_dot_reg[0]_2 ;
  output \charact_dot_reg[0]_3 ;
  output \reg_reg[2]_2 ;
  output \reg_reg[1]_3 ;
  output \charact_dot_reg[0]_4 ;
  output [7:0]elem_OBUF;
  output \charact_dot_reg[0]_5 ;
  output \current_state_reg[0]_0 ;
  output [0:0]CO;
  output \charact_dot_reg[0]_6 ;
  input [0:0]\disp_dinero[1]_0 ;
  input [1:0]\disp_dinero[3]_1 ;
  input \caracter_reg[3] ;
  input [1:0]Q;
  input \caracter_reg[6] ;
  input \caracter_reg[4] ;
  input \caracter_reg[2] ;
  input \caracter_reg[2]_0 ;
  input \caracter_reg[4]_0 ;
  input \caracter_reg[4]_1 ;
  input \caracter_reg[6]_0 ;
  input \caracter[4]_i_2_0 ;
  input \caracter_reg[4]_2 ;
  input \caracter_reg[0] ;
  input \caracter_reg[0]_0 ;
  input [0:0]\disp_dinero[5]_2 ;
  input [0:0]cent;
  input \caracter_reg[2]_1 ;
  input \caracter_reg[1] ;
  input [0:0]\disp_dinero[2]_3 ;
  input \caracter_reg[6]_1 ;
  input [2:0]\caracter_reg[3]_0 ;
  input \caracter_reg[3]_1 ;
  input \caracter_reg[3]_2 ;
  input \caracter[4]_i_2_1 ;
  input dot_reg;
  input \caracter[6]_i_5_0 ;
  input \caracter_reg[3]_3 ;
  input \caracter[2]_i_6_0 ;
  input reset_IBUF;
  input \charact_dot_reg[0]_7 ;
  input \caracter[2]_i_5_0 ;
  input dot_reg_0;
  input dot_reg_1;
  input clk_BUFG;
  input output0;

  wire [0:0]CO;
  wire [4:0]D;
  wire [1:0]Q;
  wire \caracter[0]_i_10_n_0 ;
  wire \caracter[0]_i_11_n_0 ;
  wire \caracter[0]_i_12_n_0 ;
  wire \caracter[0]_i_2_n_0 ;
  wire \caracter[0]_i_3_n_0 ;
  wire \caracter[0]_i_5_n_0 ;
  wire \caracter[0]_i_6_n_0 ;
  wire \caracter[0]_i_9_n_0 ;
  wire \caracter[1]_i_11_n_0 ;
  wire \caracter[1]_i_13_n_0 ;
  wire \caracter[1]_i_14_n_0 ;
  wire \caracter[1]_i_15_n_0 ;
  wire \caracter[1]_i_16_n_0 ;
  wire \caracter[2]_i_12_n_0 ;
  wire \caracter[2]_i_13_n_0 ;
  wire \caracter[2]_i_14_n_0 ;
  wire \caracter[2]_i_15_n_0 ;
  wire \caracter[2]_i_2_n_0 ;
  wire \caracter[2]_i_5_0 ;
  wire \caracter[2]_i_5_n_0 ;
  wire \caracter[2]_i_6_0 ;
  wire \caracter[2]_i_6_n_0 ;
  wire \caracter[2]_i_7_n_0 ;
  wire \caracter[2]_i_8_n_0 ;
  wire \caracter[3]_i_10_n_0 ;
  wire \caracter[3]_i_11_n_0 ;
  wire \caracter[3]_i_19_n_0 ;
  wire \caracter[3]_i_2_n_0 ;
  wire \caracter[3]_i_3_n_0 ;
  wire \caracter[3]_i_7_n_0 ;
  wire \caracter[3]_i_8_n_0 ;
  wire \caracter[4]_i_13_n_0 ;
  wire \caracter[4]_i_2_0 ;
  wire \caracter[4]_i_2_1 ;
  wire \caracter[4]_i_2_n_0 ;
  wire \caracter[4]_i_5_n_0 ;
  wire \caracter[4]_i_7_n_0 ;
  wire \caracter[4]_i_8_n_0 ;
  wire \caracter[4]_i_9_n_0 ;
  wire \caracter[6]_i_10_n_0 ;
  wire \caracter[6]_i_11_n_0 ;
  wire \caracter[6]_i_12_n_0 ;
  wire \caracter[6]_i_13_n_0 ;
  wire \caracter[6]_i_14_n_0 ;
  wire \caracter[6]_i_18_n_0 ;
  wire \caracter[6]_i_5_0 ;
  wire \caracter[6]_i_5_n_0 ;
  wire \caracter[6]_i_9_n_0 ;
  wire \caracter_reg[0] ;
  wire \caracter_reg[0]_0 ;
  wire \caracter_reg[1] ;
  wire \caracter_reg[2] ;
  wire \caracter_reg[2]_0 ;
  wire \caracter_reg[2]_1 ;
  wire \caracter_reg[3] ;
  wire [2:0]\caracter_reg[3]_0 ;
  wire \caracter_reg[3]_1 ;
  wire \caracter_reg[3]_2 ;
  wire \caracter_reg[3]_3 ;
  wire \caracter_reg[4] ;
  wire \caracter_reg[4]_0 ;
  wire \caracter_reg[4]_1 ;
  wire \caracter_reg[4]_2 ;
  wire \caracter_reg[6] ;
  wire \caracter_reg[6]_0 ;
  wire \caracter_reg[6]_1 ;
  wire [0:0]cent;
  wire \charact_dot_reg[0] ;
  wire \charact_dot_reg[0]_0 ;
  wire \charact_dot_reg[0]_1 ;
  wire \charact_dot_reg[0]_2 ;
  wire \charact_dot_reg[0]_3 ;
  wire \charact_dot_reg[0]_4 ;
  wire \charact_dot_reg[0]_5 ;
  wire \charact_dot_reg[0]_6 ;
  wire \charact_dot_reg[0]_7 ;
  wire clk_BUFG;
  wire \current_state_reg[0] ;
  wire \current_state_reg[0]_0 ;
  wire \current_state_reg[1] ;
  wire [0:0]\disp_dinero[1]_0 ;
  wire [0:0]\disp_dinero[2]_3 ;
  wire [1:0]\disp_dinero[3]_1 ;
  wire [0:0]\disp_dinero[5]_2 ;
  wire dot_i_3_n_0;
  wire dot_reg;
  wire dot_reg_0;
  wire dot_reg_1;
  wire [7:0]elem_OBUF;
  wire output0;
  wire [0:0]plusOp;
  wire \reg[0]_i_1_n_0 ;
  wire \reg[1]_i_1_n_0 ;
  wire \reg[2]_i_1_n_0 ;
  wire \reg_reg[0]_0 ;
  wire \reg_reg[1]_0 ;
  wire \reg_reg[1]_1 ;
  wire \reg_reg[1]_2 ;
  wire \reg_reg[1]_3 ;
  wire [2:0]\reg_reg[2]_0 ;
  wire \reg_reg[2]_1 ;
  wire \reg_reg[2]_2 ;
  wire reset_IBUF;
  wire [2:0]\NLW_caracter_reg[6]_i_4_CO_UNCONNECTED ;
  wire [3:0]\NLW_caracter_reg[6]_i_4_O_UNCONNECTED ;

  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFF8)) 
    \caracter[0]_i_1 
       (.I0(\caracter[0]_i_2_n_0 ),
        .I1(\caracter_reg[6] ),
        .I2(\caracter[0]_i_3_n_0 ),
        .I3(\caracter_reg[0] ),
        .I4(\caracter[0]_i_5_n_0 ),
        .I5(\caracter[0]_i_6_n_0 ),
        .O(D[0]));
  LUT6 #(
    .INIT(64'h000050500000F400)) 
    \caracter[0]_i_10 
       (.I0(\reg_reg[1]_1 ),
        .I1(\reg_reg[0]_0 ),
        .I2(\charact_dot_reg[0] ),
        .I3(\caracter_reg[3]_0 [2]),
        .I4(Q[1]),
        .I5(Q[0]),
        .O(\caracter[0]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h22F8228800000000)) 
    \caracter[0]_i_11 
       (.I0(\caracter[0]_i_12_n_0 ),
        .I1(\caracter_reg[4] ),
        .I2(\caracter_reg[4]_0 ),
        .I3(\reg_reg[2]_0 [0]),
        .I4(\reg_reg[2]_0 [1]),
        .I5(\disp_dinero[5]_2 ),
        .O(\caracter[0]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'h00000000406C0000)) 
    \caracter[0]_i_12 
       (.I0(\caracter_reg[4] ),
        .I1(\reg_reg[2]_0 [1]),
        .I2(\reg_reg[2]_0 [0]),
        .I3(\reg_reg[2]_0 [2]),
        .I4(Q[1]),
        .I5(Q[0]),
        .O(\caracter[0]_i_12_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h0120)) 
    \caracter[0]_i_2 
       (.I0(\caracter_reg[4] ),
        .I1(\reg_reg[2]_0 [2]),
        .I2(\reg_reg[2]_0 [0]),
        .I3(\reg_reg[2]_0 [1]),
        .O(\caracter[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88A8202200A82000)) 
    \caracter[0]_i_3 
       (.I0(\reg_reg[1]_2 ),
        .I1(\reg_reg[2]_0 [1]),
        .I2(\disp_dinero[3]_1 [0]),
        .I3(\caracter_reg[4] ),
        .I4(\reg_reg[2]_0 [0]),
        .I5(cent),
        .O(\caracter[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFF0FFFFFFF0FFF8)) 
    \caracter[0]_i_5 
       (.I0(\caracter[1]_i_14_n_0 ),
        .I1(\caracter[1]_i_13_n_0 ),
        .I2(\caracter[0]_i_9_n_0 ),
        .I3(\caracter[0]_i_10_n_0 ),
        .I4(\reg_reg[0]_0 ),
        .I5(\caracter_reg[6]_1 ),
        .O(\caracter[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFA8FFA8FFA8)) 
    \caracter[0]_i_6 
       (.I0(\caracter_reg[0]_0 ),
        .I1(\charact_dot_reg[0] ),
        .I2(\reg_reg[1]_1 ),
        .I3(\caracter[0]_i_11_n_0 ),
        .I4(\reg_reg[1]_0 ),
        .I5(\caracter_reg[6]_0 ),
        .O(\caracter[0]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'h00000880)) 
    \caracter[0]_i_9 
       (.I0(\reg_reg[2]_0 [2]),
        .I1(\caracter_reg[6] ),
        .I2(\reg_reg[2]_0 [0]),
        .I3(\caracter_reg[4] ),
        .I4(\reg_reg[2]_0 [1]),
        .O(\caracter[0]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'h20032000E2882288)) 
    \caracter[1]_i_11 
       (.I0(\caracter_reg[6]_0 ),
        .I1(\caracter_reg[4] ),
        .I2(\reg_reg[2]_0 [1]),
        .I3(\reg_reg[2]_0 [0]),
        .I4(\caracter_reg[4]_2 ),
        .I5(\reg_reg[2]_0 [2]),
        .O(\caracter[1]_i_11_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'h0095)) 
    \caracter[1]_i_13 
       (.I0(\reg_reg[2]_0 [2]),
        .I1(\reg_reg[2]_0 [0]),
        .I2(\reg_reg[2]_0 [1]),
        .I3(Q[1]),
        .O(\caracter[1]_i_13_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h2EF02200)) 
    \caracter[1]_i_14 
       (.I0(\caracter_reg[3]_0 [0]),
        .I1(\caracter_reg[4] ),
        .I2(\reg_reg[2]_0 [1]),
        .I3(\reg_reg[2]_0 [0]),
        .I4(\caracter_reg[3]_0 [2]),
        .O(\caracter[1]_i_14_n_0 ));
  LUT6 #(
    .INIT(64'hECCC00CACA0000CE)) 
    \caracter[1]_i_15 
       (.I0(Q[0]),
        .I1(\caracter_reg[6]_0 ),
        .I2(\reg_reg[2]_0 [2]),
        .I3(\reg_reg[2]_0 [0]),
        .I4(\caracter_reg[4] ),
        .I5(\reg_reg[2]_0 [1]),
        .O(\caracter[1]_i_15_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'h8222)) 
    \caracter[1]_i_16 
       (.I0(Q[0]),
        .I1(\reg_reg[2]_0 [2]),
        .I2(\reg_reg[2]_0 [0]),
        .I3(\reg_reg[2]_0 [1]),
        .O(\caracter[1]_i_16_n_0 ));
  LUT6 #(
    .INIT(64'h0000000400000400)) 
    \caracter[1]_i_5 
       (.I0(\reg_reg[2]_0 [2]),
        .I1(\caracter_reg[3]_0 [1]),
        .I2(\caracter_reg[3]_3 ),
        .I3(\reg_reg[2]_0 [0]),
        .I4(\reg_reg[2]_0 [1]),
        .I5(\caracter_reg[4] ),
        .O(\reg_reg[2]_2 ));
  LUT6 #(
    .INIT(64'hFAFAEAAAEAAAEAAA)) 
    \caracter[1]_i_6 
       (.I0(\caracter[1]_i_11_n_0 ),
        .I1(\caracter_reg[1] ),
        .I2(\reg_reg[0]_0 ),
        .I3(\caracter[4]_i_13_n_0 ),
        .I4(\caracter[1]_i_13_n_0 ),
        .I5(\caracter[1]_i_14_n_0 ),
        .O(\charact_dot_reg[0]_1 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFAEAEAEA)) 
    \caracter[1]_i_7 
       (.I0(\caracter[1]_i_15_n_0 ),
        .I1(\caracter[1]_i_16_n_0 ),
        .I2(\charact_dot_reg[0] ),
        .I3(\current_state_reg[1] ),
        .I4(\disp_dinero[1]_0 ),
        .I5(\caracter[3]_i_8_n_0 ),
        .O(\current_state_reg[0] ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'hDB7B)) 
    \caracter[1]_i_8 
       (.I0(\caracter_reg[4] ),
        .I1(\reg_reg[2]_0 [2]),
        .I2(\reg_reg[2]_0 [0]),
        .I3(\reg_reg[2]_0 [1]),
        .O(\charact_dot_reg[0]_3 ));
  LUT6 #(
    .INIT(64'h00F800F088F0F800)) 
    \caracter[1]_i_9 
       (.I0(\reg_reg[2]_0 [2]),
        .I1(\caracter_reg[6] ),
        .I2(\caracter_reg[0]_0 ),
        .I3(\reg_reg[2]_0 [1]),
        .I4(\caracter_reg[4] ),
        .I5(\reg_reg[2]_0 [0]),
        .O(\reg_reg[2]_1 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \caracter[2]_i_1 
       (.I0(\caracter[2]_i_2_n_0 ),
        .I1(\caracter_reg[2] ),
        .I2(\caracter_reg[2]_0 ),
        .I3(\caracter[2]_i_5_n_0 ),
        .I4(\caracter[2]_i_6_n_0 ),
        .I5(\caracter[2]_i_7_n_0 ),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'h81)) 
    \caracter[2]_i_10 
       (.I0(\reg_reg[2]_0 [1]),
        .I1(\caracter_reg[4] ),
        .I2(\reg_reg[2]_0 [0]),
        .O(\reg_reg[1]_3 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'h80020202)) 
    \caracter[2]_i_12 
       (.I0(\caracter[2]_i_5_0 ),
        .I1(\reg_reg[2]_0 [1]),
        .I2(\reg_reg[2]_0 [2]),
        .I3(\reg_reg[2]_0 [0]),
        .I4(\caracter_reg[4] ),
        .O(\caracter[2]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888F88888)) 
    \caracter[2]_i_13 
       (.I0(\reg_reg[1]_3 ),
        .I1(\caracter[2]_i_6_0 ),
        .I2(\caracter_reg[3]_1 ),
        .I3(\caracter_reg[3]_0 [0]),
        .I4(\caracter_reg[4] ),
        .I5(\charact_dot_reg[0]_4 ),
        .O(\caracter[2]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'h0009000900090000)) 
    \caracter[2]_i_14 
       (.I0(\reg_reg[2]_0 [0]),
        .I1(\caracter_reg[4] ),
        .I2(\reg_reg[1]_1 ),
        .I3(Q[1]),
        .I4(\caracter_reg[3]_0 [0]),
        .I5(Q[0]),
        .O(\caracter[2]_i_14_n_0 ));
  LUT6 #(
    .INIT(64'h0CCE0AC0C00A00C0)) 
    \caracter[2]_i_15 
       (.I0(\caracter_reg[4]_2 ),
        .I1(\caracter_reg[6] ),
        .I2(\reg_reg[2]_0 [2]),
        .I3(\reg_reg[2]_0 [0]),
        .I4(\caracter_reg[4] ),
        .I5(\reg_reg[2]_0 [1]),
        .O(\caracter[2]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'h00880000000000F0)) 
    \caracter[2]_i_2 
       (.I0(\charact_dot_reg[0]_0 ),
        .I1(Q[0]),
        .I2(\caracter[2]_i_8_n_0 ),
        .I3(Q[1]),
        .I4(\reg_reg[1]_1 ),
        .I5(\reg_reg[0]_0 ),
        .O(\caracter[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF8080F080)) 
    \caracter[2]_i_5 
       (.I0(\reg_reg[1]_1 ),
        .I1(\caracter_reg[6]_0 ),
        .I2(\caracter[4]_i_13_n_0 ),
        .I3(\caracter_reg[2]_1 ),
        .I4(\reg_reg[0]_0 ),
        .I5(\caracter[2]_i_12_n_0 ),
        .O(\caracter[2]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAEEAFEAAAEEAAEAA)) 
    \caracter[2]_i_6 
       (.I0(\caracter[2]_i_13_n_0 ),
        .I1(\caracter[2]_i_14_n_0 ),
        .I2(\reg_reg[2]_0 [0]),
        .I3(\reg_reg[2]_0 [1]),
        .I4(\caracter_reg[4] ),
        .I5(\caracter_reg[6] ),
        .O(\caracter[2]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hEEFEEEEEFEEEEEEE)) 
    \caracter[2]_i_7 
       (.I0(\caracter[2]_i_15_n_0 ),
        .I1(\caracter[3]_i_11_n_0 ),
        .I2(\caracter_reg[4]_0 ),
        .I3(\caracter_reg[4] ),
        .I4(\reg_reg[2]_0 [0]),
        .I5(\reg_reg[2]_0 [1]),
        .O(\caracter[2]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'hCC0E00CC)) 
    \caracter[2]_i_8 
       (.I0(\caracter_reg[3]_0 [2]),
        .I1(Q[0]),
        .I2(\caracter_reg[4] ),
        .I3(\reg_reg[2]_0 [1]),
        .I4(\reg_reg[2]_0 [0]),
        .O(\caracter[2]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \caracter[2]_i_9 
       (.I0(\reg_reg[2]_0 [0]),
        .I1(\caracter_reg[4] ),
        .O(\reg_reg[0]_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFEEEEEEE)) 
    \caracter[3]_i_1 
       (.I0(\caracter[3]_i_2_n_0 ),
        .I1(\caracter[3]_i_3_n_0 ),
        .I2(\reg_reg[1]_0 ),
        .I3(\current_state_reg[1] ),
        .I4(\disp_dinero[3]_1 [1]),
        .I5(\caracter[3]_i_7_n_0 ),
        .O(D[2]));
  LUT6 #(
    .INIT(64'h8000000000000008)) 
    \caracter[3]_i_10 
       (.I0(\disp_dinero[2]_3 ),
        .I1(Q[1]),
        .I2(\reg_reg[2]_0 [2]),
        .I3(\reg_reg[2]_0 [0]),
        .I4(\caracter_reg[4] ),
        .I5(\reg_reg[2]_0 [1]),
        .O(\caracter[3]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hFF80808080808080)) 
    \caracter[3]_i_11 
       (.I0(\caracter[4]_i_13_n_0 ),
        .I1(\caracter_reg[0]_0 ),
        .I2(\charact_dot_reg[0]_3 ),
        .I3(\charact_dot_reg[0] ),
        .I4(\caracter[1]_i_13_n_0 ),
        .I5(\caracter_reg[3]_0 [0]),
        .O(\caracter[3]_i_11_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h00002000)) 
    \caracter[3]_i_19 
       (.I0(\caracter_reg[4] ),
        .I1(\reg_reg[2]_0 [1]),
        .I2(\reg_reg[2]_0 [0]),
        .I3(Q[1]),
        .I4(\reg_reg[2]_0 [2]),
        .O(\caracter[3]_i_19_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFEEE)) 
    \caracter[3]_i_2 
       (.I0(\caracter[3]_i_8_n_0 ),
        .I1(\caracter_reg[3] ),
        .I2(\charact_dot_reg[0] ),
        .I3(Q[0]),
        .I4(\caracter[3]_i_10_n_0 ),
        .I5(\caracter[3]_i_11_n_0 ),
        .O(\caracter[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00004444000044F4)) 
    \caracter[3]_i_3 
       (.I0(\charact_dot_reg[0]_0 ),
        .I1(Q[0]),
        .I2(\caracter_reg[3]_1 ),
        .I3(plusOp),
        .I4(\reg_reg[1]_1 ),
        .I5(\caracter_reg[3]_2 ),
        .O(\caracter[3]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'h14)) 
    \caracter[3]_i_4 
       (.I0(\reg_reg[2]_0 [1]),
        .I1(\caracter_reg[4] ),
        .I2(\reg_reg[2]_0 [0]),
        .O(\reg_reg[1]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT4 #(
    .INIT(16'h8222)) 
    \caracter[3]_i_5 
       (.I0(Q[1]),
        .I1(\reg_reg[2]_0 [2]),
        .I2(\reg_reg[2]_0 [0]),
        .I3(\reg_reg[2]_0 [1]),
        .O(\current_state_reg[1] ));
  LUT6 #(
    .INIT(64'h0400000000000008)) 
    \caracter[3]_i_7 
       (.I0(\reg_reg[2]_0 [2]),
        .I1(\caracter_reg[3]_0 [2]),
        .I2(\caracter_reg[3]_3 ),
        .I3(\reg_reg[2]_0 [0]),
        .I4(\reg_reg[2]_0 [1]),
        .I5(\caracter_reg[4] ),
        .O(\caracter[3]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h8002FFFF80028002)) 
    \caracter[3]_i_8 
       (.I0(\caracter_reg[6] ),
        .I1(\reg_reg[2]_0 [1]),
        .I2(\caracter_reg[4] ),
        .I3(\reg_reg[2]_0 [0]),
        .I4(\disp_dinero[1]_0 ),
        .I5(\caracter[3]_i_19_n_0 ),
        .O(\caracter[3]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFAAEA)) 
    \caracter[4]_i_1 
       (.I0(\caracter[4]_i_2_n_0 ),
        .I1(\caracter_reg[4]_0 ),
        .I2(\caracter_reg[4] ),
        .I3(\reg_reg[2]_0 [0]),
        .I4(\caracter_reg[4]_1 ),
        .I5(\caracter[4]_i_5_n_0 ),
        .O(D[3]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT4 #(
    .INIT(16'hBDED)) 
    \caracter[4]_i_10 
       (.I0(\caracter_reg[4] ),
        .I1(\reg_reg[2]_0 [2]),
        .I2(\reg_reg[2]_0 [0]),
        .I3(\reg_reg[2]_0 [1]),
        .O(\charact_dot_reg[0]_4 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \caracter[4]_i_11 
       (.I0(\reg_reg[2]_0 [0]),
        .I1(\reg_reg[2]_0 [1]),
        .O(plusOp));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \caracter[4]_i_12 
       (.I0(\caracter_reg[4] ),
        .I1(\reg_reg[2]_0 [0]),
        .O(\charact_dot_reg[0]_5 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'h93)) 
    \caracter[4]_i_13 
       (.I0(\caracter_reg[4] ),
        .I1(\reg_reg[2]_0 [1]),
        .I2(\reg_reg[2]_0 [0]),
        .O(\caracter[4]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFF0FFF2FFF0)) 
    \caracter[4]_i_2 
       (.I0(\reg_reg[1]_1 ),
        .I1(\charact_dot_reg[0]_0 ),
        .I2(\caracter[4]_i_7_n_0 ),
        .I3(\caracter[4]_i_8_n_0 ),
        .I4(\caracter_reg[6]_0 ),
        .I5(\caracter[4]_i_9_n_0 ),
        .O(\caracter[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF222F222F222)) 
    \caracter[4]_i_5 
       (.I0(\caracter_reg[4]_0 ),
        .I1(\reg_reg[2]_0 [0]),
        .I2(\reg_reg[1]_2 ),
        .I3(\caracter[4]_i_13_n_0 ),
        .I4(\caracter_reg[4]_2 ),
        .I5(\caracter[0]_i_2_n_0 ),
        .O(\caracter[4]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hD3)) 
    \caracter[4]_i_6 
       (.I0(\caracter_reg[4] ),
        .I1(\reg_reg[2]_0 [1]),
        .I2(\reg_reg[2]_0 [0]),
        .O(\charact_dot_reg[0]_0 ));
  LUT6 #(
    .INIT(64'h0040004000400000)) 
    \caracter[4]_i_7 
       (.I0(\caracter_reg[4] ),
        .I1(\reg_reg[2]_0 [1]),
        .I2(\reg_reg[2]_0 [0]),
        .I3(\reg_reg[2]_0 [2]),
        .I4(\caracter_reg[6] ),
        .I5(\caracter[4]_i_2_0 ),
        .O(\caracter[4]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h8002800280C28002)) 
    \caracter[4]_i_8 
       (.I0(\caracter[4]_i_2_1 ),
        .I1(\caracter_reg[4] ),
        .I2(\reg_reg[2]_0 [1]),
        .I3(\reg_reg[2]_0 [0]),
        .I4(\caracter_reg[6] ),
        .I5(\reg_reg[2]_0 [2]),
        .O(\caracter[4]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h1202)) 
    \caracter[4]_i_9 
       (.I0(\caracter_reg[4] ),
        .I1(\reg_reg[2]_0 [2]),
        .I2(\reg_reg[2]_0 [0]),
        .I3(\reg_reg[2]_0 [1]),
        .O(\caracter[4]_i_9_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \caracter[6]_i_10 
       (.I0(\reg_reg[2]_0 [0]),
        .I1(\reg_reg[2]_0 [1]),
        .O(\caracter[6]_i_10_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \caracter[6]_i_11 
       (.I0(\reg_reg[2]_0 [0]),
        .O(\caracter[6]_i_11_n_0 ));
  LUT3 #(
    .INIT(8'h78)) 
    \caracter[6]_i_12 
       (.I0(\reg_reg[2]_0 [1]),
        .I1(\reg_reg[2]_0 [0]),
        .I2(\reg_reg[2]_0 [2]),
        .O(\caracter[6]_i_12_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \caracter[6]_i_13 
       (.I0(\reg_reg[2]_0 [0]),
        .I1(\reg_reg[2]_0 [1]),
        .O(\caracter[6]_i_13_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \caracter[6]_i_14 
       (.I0(\reg_reg[2]_0 [0]),
        .I1(\caracter_reg[4] ),
        .O(\caracter[6]_i_14_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT5 #(
    .INIT(32'h00008700)) 
    \caracter[6]_i_15 
       (.I0(\reg_reg[2]_0 [1]),
        .I1(\reg_reg[2]_0 [0]),
        .I2(\reg_reg[2]_0 [2]),
        .I3(Q[1]),
        .I4(Q[0]),
        .O(\reg_reg[1]_2 ));
  LUT6 #(
    .INIT(64'hFFFFAA0008080000)) 
    \caracter[6]_i_18 
       (.I0(\reg_reg[0]_0 ),
        .I1(\caracter_reg[3]_0 [1]),
        .I2(Q[1]),
        .I3(\charact_dot_reg[0]_0 ),
        .I4(\charact_dot_reg[0]_2 ),
        .I5(\caracter[6]_i_5_0 ),
        .O(\caracter[6]_i_18_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \caracter[6]_i_19 
       (.I0(\caracter_reg[4] ),
        .I1(\reg_reg[2]_0 [1]),
        .I2(\reg_reg[2]_0 [0]),
        .O(\charact_dot_reg[0] ));
  LUT6 #(
    .INIT(64'hFABAFAFAFAFAEABA)) 
    \caracter[6]_i_2 
       (.I0(\caracter[6]_i_5_n_0 ),
        .I1(\reg_reg[2]_0 [0]),
        .I2(\caracter_reg[6] ),
        .I3(\caracter_reg[4] ),
        .I4(\reg_reg[2]_0 [2]),
        .I5(\reg_reg[2]_0 [1]),
        .O(D[4]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'hD333)) 
    \caracter[6]_i_21 
       (.I0(\caracter_reg[4] ),
        .I1(\reg_reg[2]_0 [2]),
        .I2(\reg_reg[2]_0 [0]),
        .I3(\reg_reg[2]_0 [1]),
        .O(\charact_dot_reg[0]_2 ));
  LUT6 #(
    .INIT(64'hFFFEFFFCFFFFFFFC)) 
    \caracter[6]_i_5 
       (.I0(\reg_reg[1]_2 ),
        .I1(\caracter_reg[6]_0 ),
        .I2(\caracter_reg[6]_1 ),
        .I3(\caracter[6]_i_18_n_0 ),
        .I4(\charact_dot_reg[0] ),
        .I5(dot_reg),
        .O(\caracter[6]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'h78)) 
    \caracter[6]_i_9 
       (.I0(\reg_reg[2]_0 [1]),
        .I1(\reg_reg[2]_0 [0]),
        .I2(\reg_reg[2]_0 [2]),
        .O(\caracter[6]_i_9_n_0 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \caracter_reg[6]_i_4 
       (.CI(1'b0),
        .CO({CO,\NLW_caracter_reg[6]_i_4_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,\caracter[6]_i_9_n_0 ,\caracter[6]_i_10_n_0 ,\caracter[6]_i_11_n_0 }),
        .O(\NLW_caracter_reg[6]_i_4_O_UNCONNECTED [3:0]),
        .S({1'b1,\caracter[6]_i_12_n_0 ,\caracter[6]_i_13_n_0 ,\caracter[6]_i_14_n_0 }));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'hBF88)) 
    \charact_dot[0]_i_1 
       (.I0(\charact_dot_reg[0]_7 ),
        .I1(reset_IBUF),
        .I2(CO),
        .I3(\caracter_reg[4] ),
        .O(\charact_dot_reg[0]_6 ));
  LUT4 #(
    .INIT(16'hFF32)) 
    dot_i_1
       (.I0(Q[0]),
        .I1(CO),
        .I2(\reg_reg[1]_1 ),
        .I3(dot_i_3_n_0),
        .O(\current_state_reg[0]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'h78)) 
    dot_i_2
       (.I0(\reg_reg[2]_0 [1]),
        .I1(\reg_reg[2]_0 [0]),
        .I2(\reg_reg[2]_0 [2]),
        .O(\reg_reg[1]_1 ));
  LUT6 #(
    .INIT(64'h5545504055555555)) 
    dot_i_3
       (.I0(CO),
        .I1(dot_reg_0),
        .I2(\reg_reg[2]_0 [0]),
        .I3(dot_reg),
        .I4(dot_reg_1),
        .I5(\reg_reg[2]_0 [1]),
        .O(dot_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT4 #(
    .INIT(16'hFFFB)) 
    \elem_OBUF[0]_inst_i_1 
       (.I0(\reg_reg[2]_0 [2]),
        .I1(reset_IBUF),
        .I2(\reg_reg[2]_0 [1]),
        .I3(\reg_reg[2]_0 [0]),
        .O(elem_OBUF[0]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT4 #(
    .INIT(16'hFBFF)) 
    \elem_OBUF[1]_inst_i_1 
       (.I0(\reg_reg[2]_0 [2]),
        .I1(reset_IBUF),
        .I2(\reg_reg[2]_0 [1]),
        .I3(\reg_reg[2]_0 [0]),
        .O(elem_OBUF[1]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT4 #(
    .INIT(16'hFFDF)) 
    \elem_OBUF[2]_inst_i_1 
       (.I0(\reg_reg[2]_0 [1]),
        .I1(\reg_reg[2]_0 [2]),
        .I2(reset_IBUF),
        .I3(\reg_reg[2]_0 [0]),
        .O(elem_OBUF[2]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'hDFFF)) 
    \elem_OBUF[3]_inst_i_1 
       (.I0(reset_IBUF),
        .I1(\reg_reg[2]_0 [2]),
        .I2(\reg_reg[2]_0 [1]),
        .I3(\reg_reg[2]_0 [0]),
        .O(elem_OBUF[3]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'hFFF7)) 
    \elem_OBUF[4]_inst_i_1 
       (.I0(reset_IBUF),
        .I1(\reg_reg[2]_0 [2]),
        .I2(\reg_reg[2]_0 [1]),
        .I3(\reg_reg[2]_0 [0]),
        .O(elem_OBUF[4]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'hF7FF)) 
    \elem_OBUF[5]_inst_i_1 
       (.I0(reset_IBUF),
        .I1(\reg_reg[2]_0 [2]),
        .I2(\reg_reg[2]_0 [1]),
        .I3(\reg_reg[2]_0 [0]),
        .O(elem_OBUF[5]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT4 #(
    .INIT(16'hFF7F)) 
    \elem_OBUF[6]_inst_i_1 
       (.I0(\reg_reg[2]_0 [1]),
        .I1(reset_IBUF),
        .I2(\reg_reg[2]_0 [2]),
        .I3(\reg_reg[2]_0 [0]),
        .O(elem_OBUF[6]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \elem_OBUF[7]_inst_i_1 
       (.I0(\reg_reg[2]_0 [2]),
        .I1(\reg_reg[2]_0 [0]),
        .I2(\reg_reg[2]_0 [1]),
        .I3(reset_IBUF),
        .O(elem_OBUF[7]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \reg[0]_i_1 
       (.I0(\reg_reg[2]_0 [0]),
        .O(\reg[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \reg[1]_i_1 
       (.I0(\reg_reg[2]_0 [1]),
        .I1(\reg_reg[2]_0 [0]),
        .O(\reg[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \reg[2]_i_1 
       (.I0(\reg_reg[2]_0 [2]),
        .I1(\reg_reg[2]_0 [0]),
        .I2(\reg_reg[2]_0 [1]),
        .O(\reg[2]_i_1_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \reg_reg[0] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .CLR(output0),
        .D(\reg[0]_i_1_n_0 ),
        .Q(\reg_reg[2]_0 [0]));
  FDCE #(
    .INIT(1'b0)) 
    \reg_reg[1] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .CLR(output0),
        .D(\reg[1]_i_1_n_0 ),
        .Q(\reg_reg[2]_0 [1]));
  FDCE #(
    .INIT(1'b0)) 
    \reg_reg[2] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .CLR(output0),
        .D(\reg[2]_i_1_n_0 ),
        .Q(\reg_reg[2]_0 [2]));
endmodule

module decod_monedas
   (\v_reg[1]_0 ,
    Q,
    \next_state_reg[1]_i_1 ,
    D,
    clk_BUFG);
  output \v_reg[1]_0 ;
  output [6:0]Q;
  input \next_state_reg[1]_i_1 ;
  input [6:0]D;
  input clk_BUFG;

  wire [6:0]D;
  wire [6:0]Q;
  wire clk_BUFG;
  wire \next_state_reg[1]_i_1 ;
  wire \v_reg[1]_0 ;

  LUT6 #(
    .INIT(64'hFFFF0000FFFE0000)) 
    \next_state_reg[1]_i_3 
       (.I0(Q[1]),
        .I1(Q[4]),
        .I2(Q[5]),
        .I3(Q[3]),
        .I4(\next_state_reg[1]_i_1 ),
        .I5(Q[2]),
        .O(\v_reg[1]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \v_reg[0] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .D(D[0]),
        .Q(Q[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \v_reg[1] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .D(D[1]),
        .Q(Q[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \v_reg[2] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .D(D[2]),
        .Q(Q[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \v_reg[3] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .D(D[3]),
        .Q(Q[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \v_reg[4] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .D(D[4]),
        .Q(Q[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \v_reg[5] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .D(D[5]),
        .Q(Q[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \v_reg[6] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .D(D[6]),
        .Q(Q[6]),
        .R(1'b0));
endmodule

module detectorflancos
   (D,
    \sreg_reg[0] ,
    clk_BUFG,
    \sreg_reg[0]_0 ,
    \sreg_reg[0]_1 ,
    \sreg_reg[0]_2 ,
    \sreg_reg[0]_3 );
  output [6:0]D;
  input \sreg_reg[0] ;
  input clk_BUFG;
  input \sreg_reg[0]_0 ;
  input \sreg_reg[0]_1 ;
  input \sreg_reg[0]_2 ;
  input \sreg_reg[0]_3 ;

  wire [6:0]D;
  wire clk_BUFG;
  wire inst_edgedtctr0_n_1;
  wire inst_edgedtctr1_n_4;
  wire inst_edgedtctr2_n_0;
  wire inst_edgedtctr2_n_1;
  wire inst_edgedtctr3_n_4;
  wire inst_edgedtctr4_n_4;
  wire inst_edgedtctr4_n_5;
  wire [2:0]sreg;
  wire [2:0]sreg_0;
  wire \sreg_reg[0] ;
  wire \sreg_reg[0]_0 ;
  wire \sreg_reg[0]_1 ;
  wire \sreg_reg[0]_2 ;
  wire \sreg_reg[0]_3 ;

  edgedtctr inst_edgedtctr0
       (.D(D[0]),
        .clk_BUFG(clk_BUFG),
        .\sreg_reg[0]_0 (\sreg_reg[0] ),
        .\sreg_reg[2]_0 (inst_edgedtctr0_n_1),
        .\v_reg[0] (inst_edgedtctr2_n_0),
        .\v_reg[0]_0 (inst_edgedtctr4_n_5));
  edgedtctr_4 inst_edgedtctr1
       (.D(D[3]),
        .clk_BUFG(clk_BUFG),
        .sreg(sreg),
        .\sreg_reg[0]_0 (\sreg_reg[0]_0 ),
        .\sreg_reg[2]_0 (inst_edgedtctr1_n_4),
        .\v_reg[3] (inst_edgedtctr2_n_1),
        .\v_reg[3]_0 (inst_edgedtctr0_n_1),
        .\v_reg[3]_1 (inst_edgedtctr4_n_5));
  edgedtctr_5 inst_edgedtctr2
       (.clk_BUFG(clk_BUFG),
        .sreg(sreg),
        .\sreg_reg[0]_0 (inst_edgedtctr2_n_0),
        .\sreg_reg[0]_1 (\sreg_reg[0]_1 ),
        .\sreg_reg[2]_0 (inst_edgedtctr2_n_1));
  edgedtctr_6 inst_edgedtctr3
       (.D(D[6]),
        .clk_BUFG(clk_BUFG),
        .sreg(sreg_0),
        .\sreg_reg[0]_0 (\sreg_reg[0]_2 ),
        .\sreg_reg[2]_0 (inst_edgedtctr3_n_4),
        .\v_reg[6] (inst_edgedtctr4_n_4),
        .\v_reg[6]_0 (inst_edgedtctr0_n_1),
        .\v_reg[6]_1 (inst_edgedtctr2_n_0));
  edgedtctr_7 inst_edgedtctr4
       (.D({D[5:4],D[2:1]}),
        .clk_BUFG(clk_BUFG),
        .sreg(sreg_0),
        .\sreg_reg[0]_0 (inst_edgedtctr4_n_5),
        .\sreg_reg[0]_1 (\sreg_reg[0]_3 ),
        .\sreg_reg[2]_0 (inst_edgedtctr4_n_4),
        .\v_reg[2] (inst_edgedtctr3_n_4),
        .\v_reg[2]_0 (inst_edgedtctr0_n_1),
        .\v_reg[2]_1 (inst_edgedtctr1_n_4),
        .\v_reg[2]_2 (inst_edgedtctr2_n_1),
        .\v_reg[5] (inst_edgedtctr2_n_0));
endmodule

module digito
   (digit_OBUF,
    Q);
  output [6:0]digit_OBUF;
  input [6:0]Q;

  wire [6:0]Q;
  wire [6:0]digit_OBUF;
  wire \digit_OBUF[0]_inst_i_2_n_0 ;
  wire \digit_OBUF[1]_inst_i_2_n_0 ;
  wire \digit_OBUF[2]_inst_i_2_n_0 ;
  wire \digit_OBUF[3]_inst_i_2_n_0 ;
  wire \digit_OBUF[4]_inst_i_2_n_0 ;
  wire \digit_OBUF[5]_inst_i_2_n_0 ;
  wire \digit_OBUF[6]_inst_i_2_n_0 ;

  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \digit_OBUF[0]_inst_i_1 
       (.I0(\digit_OBUF[0]_inst_i_2_n_0 ),
        .I1(Q[5]),
        .O(digit_OBUF[0]));
  LUT6 #(
    .INIT(64'h200210000800000D)) 
    \digit_OBUF[0]_inst_i_2 
       (.I0(Q[0]),
        .I1(Q[4]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(Q[1]),
        .I5(Q[6]),
        .O(\digit_OBUF[0]_inst_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \digit_OBUF[1]_inst_i_1 
       (.I0(\digit_OBUF[1]_inst_i_2_n_0 ),
        .I1(Q[5]),
        .O(digit_OBUF[1]));
  LUT6 #(
    .INIT(64'hFEDFECF22FDF9FDF)) 
    \digit_OBUF[1]_inst_i_2 
       (.I0(Q[0]),
        .I1(Q[3]),
        .I2(Q[6]),
        .I3(Q[2]),
        .I4(Q[1]),
        .I5(Q[4]),
        .O(\digit_OBUF[1]_inst_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \digit_OBUF[2]_inst_i_1 
       (.I0(\digit_OBUF[2]_inst_i_2_n_0 ),
        .I1(Q[5]),
        .O(digit_OBUF[2]));
  LUT6 #(
    .INIT(64'hFEEFFEFA2F9FDFDF)) 
    \digit_OBUF[2]_inst_i_2 
       (.I0(Q[0]),
        .I1(Q[3]),
        .I2(Q[6]),
        .I3(Q[1]),
        .I4(Q[2]),
        .I5(Q[4]),
        .O(\digit_OBUF[2]_inst_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \digit_OBUF[3]_inst_i_1 
       (.I0(\digit_OBUF[3]_inst_i_2_n_0 ),
        .I1(Q[5]),
        .O(digit_OBUF[3]));
  LUT6 #(
    .INIT(64'hFEEDDCF27F9FDFFF)) 
    \digit_OBUF[3]_inst_i_2 
       (.I0(Q[0]),
        .I1(Q[3]),
        .I2(Q[6]),
        .I3(Q[1]),
        .I4(Q[2]),
        .I5(Q[4]),
        .O(\digit_OBUF[3]_inst_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \digit_OBUF[4]_inst_i_1 
       (.I0(\digit_OBUF[4]_inst_i_2_n_0 ),
        .I1(Q[5]),
        .O(digit_OBUF[4]));
  LUT6 #(
    .INIT(64'hF3FFDFFDCFCFDF0F)) 
    \digit_OBUF[4]_inst_i_2 
       (.I0(Q[0]),
        .I1(Q[3]),
        .I2(Q[4]),
        .I3(Q[1]),
        .I4(Q[2]),
        .I5(Q[6]),
        .O(\digit_OBUF[4]_inst_i_2_n_0 ));
  LUT2 #(
    .INIT(4'hB)) 
    \digit_OBUF[5]_inst_i_1 
       (.I0(\digit_OBUF[5]_inst_i_2_n_0 ),
        .I1(Q[5]),
        .O(digit_OBUF[5]));
  LUT6 #(
    .INIT(64'hF3FFFFFDDFEFCF0F)) 
    \digit_OBUF[5]_inst_i_2 
       (.I0(Q[0]),
        .I1(Q[3]),
        .I2(Q[4]),
        .I3(Q[1]),
        .I4(Q[2]),
        .I5(Q[6]),
        .O(\digit_OBUF[5]_inst_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \digit_OBUF[6]_inst_i_1 
       (.I0(\digit_OBUF[6]_inst_i_2_n_0 ),
        .I1(Q[5]),
        .O(digit_OBUF[6]));
  LUT6 #(
    .INIT(64'hFCFDDCF26FDFDFDF)) 
    \digit_OBUF[6]_inst_i_2 
       (.I0(Q[0]),
        .I1(Q[3]),
        .I2(Q[6]),
        .I3(Q[1]),
        .I4(Q[2]),
        .I5(Q[4]),
        .O(\digit_OBUF[6]_inst_i_2_n_0 ));
endmodule

module display
   (dot_OBUF,
    \current_state_reg[0] ,
    \charact_dot_reg[0]_0 ,
    \current_state_reg[1] ,
    \reg_reg[1] ,
    \reg_reg[2] ,
    \reg_reg[1]_0 ,
    \charact_dot_reg[0]_1 ,
    \reg_reg[1]_1 ,
    \reg_reg[0] ,
    \charact_dot_reg[0]_2 ,
    \reg_reg[2]_0 ,
    plusOp,
    \charact_dot_reg[0]_3 ,
    \charact_dot_reg[0]_4 ,
    \reg_reg[2]_1 ,
    \reg_reg[1]_2 ,
    \charact_dot_reg[0]_5 ,
    elem_OBUF,
    \charact_dot_reg[0]_6 ,
    CO,
    \caracter_reg[6]_0 ,
    digit_OBUF,
    E,
    clk_BUFG,
    output0,
    \disp_dinero[1]_0 ,
    \disp_dinero[3]_1 ,
    \caracter_reg[3]_0 ,
    Q,
    \caracter_reg[6]_1 ,
    \caracter_reg[2]_0 ,
    \caracter_reg[2]_1 ,
    \caracter_reg[4]_0 ,
    \caracter_reg[4]_1 ,
    \caracter_reg[6]_2 ,
    \caracter[4]_i_2 ,
    \caracter_reg[4]_2 ,
    \caracter_reg[0]_0 ,
    \caracter_reg[0]_1 ,
    \disp_dinero[5]_2 ,
    cent,
    \caracter_reg[2]_2 ,
    \caracter_reg[1]_0 ,
    \disp_dinero[2]_3 ,
    \caracter_reg[6]_3 ,
    \caracter_reg[3]_1 ,
    \caracter_reg[3]_2 ,
    \caracter_reg[3]_3 ,
    \caracter[4]_i_2_0 ,
    dot_reg_0,
    \caracter[6]_i_5 ,
    \caracter_reg[3]_4 ,
    \caracter[2]_i_6 ,
    reset_IBUF,
    \charact_dot_reg[0]_7 ,
    \caracter[2]_i_5 ,
    dot_reg_1,
    dot_reg_2,
    D,
    lopt,
    lopt_1,
    lopt_2,
    lopt_3,
    lopt_4,
    lopt_5,
    lopt_6);
  output dot_OBUF;
  output \current_state_reg[0] ;
  output \charact_dot_reg[0]_0 ;
  output \current_state_reg[1] ;
  output \reg_reg[1] ;
  output [2:0]\reg_reg[2] ;
  output \reg_reg[1]_0 ;
  output \charact_dot_reg[0]_1 ;
  output \reg_reg[1]_1 ;
  output \reg_reg[0] ;
  output \charact_dot_reg[0]_2 ;
  output \reg_reg[2]_0 ;
  output [0:0]plusOp;
  output \charact_dot_reg[0]_3 ;
  output \charact_dot_reg[0]_4 ;
  output \reg_reg[2]_1 ;
  output \reg_reg[1]_2 ;
  output \charact_dot_reg[0]_5 ;
  output [7:0]elem_OBUF;
  output \charact_dot_reg[0]_6 ;
  output [0:0]CO;
  output [6:0]\caracter_reg[6]_0 ;
  output [6:0]digit_OBUF;
  input [0:0]E;
  input clk_BUFG;
  input output0;
  input [0:0]\disp_dinero[1]_0 ;
  input [1:0]\disp_dinero[3]_1 ;
  input \caracter_reg[3]_0 ;
  input [1:0]Q;
  input \caracter_reg[6]_1 ;
  input \caracter_reg[2]_0 ;
  input \caracter_reg[2]_1 ;
  input \caracter_reg[4]_0 ;
  input \caracter_reg[4]_1 ;
  input \caracter_reg[6]_2 ;
  input \caracter[4]_i_2 ;
  input \caracter_reg[4]_2 ;
  input \caracter_reg[0]_0 ;
  input \caracter_reg[0]_1 ;
  input [0:0]\disp_dinero[5]_2 ;
  input [0:0]cent;
  input \caracter_reg[2]_2 ;
  input \caracter_reg[1]_0 ;
  input [0:0]\disp_dinero[2]_3 ;
  input \caracter_reg[6]_3 ;
  input [2:0]\caracter_reg[3]_1 ;
  input \caracter_reg[3]_2 ;
  input \caracter_reg[3]_3 ;
  input \caracter[4]_i_2_0 ;
  input dot_reg_0;
  input \caracter[6]_i_5 ;
  input \caracter_reg[3]_4 ;
  input \caracter[2]_i_6 ;
  input reset_IBUF;
  input \charact_dot_reg[0]_7 ;
  input \caracter[2]_i_5 ;
  input dot_reg_1;
  input dot_reg_2;
  input [1:0]D;
  output lopt;
  output lopt_1;
  output lopt_2;
  output lopt_3;
  output lopt_4;
  output lopt_5;
  output lopt_6;

  wire [0:0]CO;
  wire [1:0]D;
  wire [0:0]E;
  wire Inst_contador_n_3;
  wire Inst_contador_n_33;
  wire Inst_contador_n_35;
  wire Inst_contador_n_4;
  wire Inst_contador_n_5;
  wire Inst_contador_n_6;
  wire Inst_contador_n_7;
  wire [1:0]Q;
  wire \caracter[2]_i_5 ;
  wire \caracter[2]_i_6 ;
  wire \caracter[4]_i_2 ;
  wire \caracter[4]_i_2_0 ;
  wire \caracter[6]_i_5 ;
  wire \caracter_reg[0]_0 ;
  wire \caracter_reg[0]_1 ;
  wire \caracter_reg[0]_lopt_replica_1 ;
  wire \caracter_reg[1]_0 ;
  wire \caracter_reg[1]_lopt_replica_1 ;
  wire \caracter_reg[2]_0 ;
  wire \caracter_reg[2]_1 ;
  wire \caracter_reg[2]_2 ;
  wire \caracter_reg[2]_lopt_replica_1 ;
  wire \caracter_reg[3]_0 ;
  wire [2:0]\caracter_reg[3]_1 ;
  wire \caracter_reg[3]_2 ;
  wire \caracter_reg[3]_3 ;
  wire \caracter_reg[3]_4 ;
  wire \caracter_reg[3]_lopt_replica_1 ;
  wire \caracter_reg[4]_0 ;
  wire \caracter_reg[4]_1 ;
  wire \caracter_reg[4]_2 ;
  wire \caracter_reg[4]_lopt_replica_1 ;
  wire \caracter_reg[5]_lopt_replica_1 ;
  wire [6:0]\caracter_reg[6]_0 ;
  wire \caracter_reg[6]_1 ;
  wire \caracter_reg[6]_2 ;
  wire \caracter_reg[6]_3 ;
  wire \caracter_reg[6]_lopt_replica_1 ;
  wire [0:0]cent;
  wire \charact_dot_reg[0]_0 ;
  wire \charact_dot_reg[0]_1 ;
  wire \charact_dot_reg[0]_2 ;
  wire \charact_dot_reg[0]_3 ;
  wire \charact_dot_reg[0]_4 ;
  wire \charact_dot_reg[0]_5 ;
  wire \charact_dot_reg[0]_6 ;
  wire \charact_dot_reg[0]_7 ;
  wire \charact_dot_reg_n_0_[0] ;
  wire clk_BUFG;
  wire \current_state_reg[0] ;
  wire \current_state_reg[1] ;
  wire [6:0]digit_OBUF;
  wire [0:0]\disp_dinero[1]_0 ;
  wire [0:0]\disp_dinero[2]_3 ;
  wire [1:0]\disp_dinero[3]_1 ;
  wire [0:0]\disp_dinero[5]_2 ;
  wire dot_OBUF;
  wire dot_reg_0;
  wire dot_reg_1;
  wire dot_reg_2;
  wire [7:0]elem_OBUF;
  wire output0;
  wire [0:0]plusOp;
  wire \reg_reg[0] ;
  wire \reg_reg[1] ;
  wire \reg_reg[1]_0 ;
  wire \reg_reg[1]_1 ;
  wire \reg_reg[1]_2 ;
  wire [2:0]\reg_reg[2] ;
  wire \reg_reg[2]_0 ;
  wire \reg_reg[2]_1 ;
  wire reset_IBUF;

  assign lopt = \caracter_reg[0]_lopt_replica_1 ;
  assign lopt_1 = \caracter_reg[1]_lopt_replica_1 ;
  assign lopt_2 = \caracter_reg[2]_lopt_replica_1 ;
  assign lopt_3 = \caracter_reg[3]_lopt_replica_1 ;
  assign lopt_4 = \caracter_reg[4]_lopt_replica_1 ;
  assign lopt_5 = \caracter_reg[5]_lopt_replica_1 ;
  assign lopt_6 = \caracter_reg[6]_lopt_replica_1 ;
  contador__parameterized1 Inst_contador
       (.CO(CO),
        .D({Inst_contador_n_3,Inst_contador_n_4,Inst_contador_n_5,Inst_contador_n_6,Inst_contador_n_7}),
        .Q(Q),
        .\caracter[2]_i_5_0 (\caracter[2]_i_5 ),
        .\caracter[2]_i_6_0 (\caracter[2]_i_6 ),
        .\caracter[4]_i_2_0 (\caracter[4]_i_2 ),
        .\caracter[4]_i_2_1 (\caracter[4]_i_2_0 ),
        .\caracter[6]_i_5_0 (\caracter[6]_i_5 ),
        .\caracter_reg[0] (\caracter_reg[0]_0 ),
        .\caracter_reg[0]_0 (\caracter_reg[0]_1 ),
        .\caracter_reg[1] (\caracter_reg[1]_0 ),
        .\caracter_reg[2] (\caracter_reg[2]_0 ),
        .\caracter_reg[2]_0 (\caracter_reg[2]_1 ),
        .\caracter_reg[2]_1 (\caracter_reg[2]_2 ),
        .\caracter_reg[3] (\caracter_reg[3]_0 ),
        .\caracter_reg[3]_0 (\caracter_reg[3]_1 ),
        .\caracter_reg[3]_1 (\caracter_reg[3]_2 ),
        .\caracter_reg[3]_2 (\caracter_reg[3]_3 ),
        .\caracter_reg[3]_3 (\caracter_reg[3]_4 ),
        .\caracter_reg[4] (\charact_dot_reg_n_0_[0] ),
        .\caracter_reg[4]_0 (\caracter_reg[4]_0 ),
        .\caracter_reg[4]_1 (\caracter_reg[4]_1 ),
        .\caracter_reg[4]_2 (\caracter_reg[4]_2 ),
        .\caracter_reg[6] (\caracter_reg[6]_1 ),
        .\caracter_reg[6]_0 (\caracter_reg[6]_2 ),
        .\caracter_reg[6]_1 (\caracter_reg[6]_3 ),
        .cent(cent),
        .\charact_dot_reg[0] (\charact_dot_reg[0]_0 ),
        .\charact_dot_reg[0]_0 (\charact_dot_reg[0]_1 ),
        .\charact_dot_reg[0]_1 (\charact_dot_reg[0]_2 ),
        .\charact_dot_reg[0]_2 (\charact_dot_reg[0]_3 ),
        .\charact_dot_reg[0]_3 (\charact_dot_reg[0]_4 ),
        .\charact_dot_reg[0]_4 (\charact_dot_reg[0]_5 ),
        .\charact_dot_reg[0]_5 (\charact_dot_reg[0]_6 ),
        .\charact_dot_reg[0]_6 (Inst_contador_n_35),
        .\charact_dot_reg[0]_7 (\charact_dot_reg[0]_7 ),
        .clk_BUFG(clk_BUFG),
        .\current_state_reg[0] (\current_state_reg[0] ),
        .\current_state_reg[0]_0 (Inst_contador_n_33),
        .\current_state_reg[1] (\current_state_reg[1] ),
        .\disp_dinero[1]_0 (\disp_dinero[1]_0 ),
        .\disp_dinero[2]_3 (\disp_dinero[2]_3 ),
        .\disp_dinero[3]_1 (\disp_dinero[3]_1 ),
        .\disp_dinero[5]_2 (\disp_dinero[5]_2 ),
        .dot_reg(dot_reg_0),
        .dot_reg_0(dot_reg_1),
        .dot_reg_1(dot_reg_2),
        .elem_OBUF(elem_OBUF),
        .output0(output0),
        .plusOp(plusOp),
        .\reg_reg[0]_0 (\reg_reg[0] ),
        .\reg_reg[1]_0 (\reg_reg[1] ),
        .\reg_reg[1]_1 (\reg_reg[1]_0 ),
        .\reg_reg[1]_2 (\reg_reg[1]_1 ),
        .\reg_reg[1]_3 (\reg_reg[1]_2 ),
        .\reg_reg[2]_0 (\reg_reg[2] ),
        .\reg_reg[2]_1 (\reg_reg[2]_0 ),
        .\reg_reg[2]_2 (\reg_reg[2]_1 ),
        .reset_IBUF(reset_IBUF));
  digito Inst_digito
       (.Q(\caracter_reg[6]_0 ),
        .digit_OBUF(digit_OBUF));
  FDCE #(
    .INIT(1'b0)) 
    \caracter_reg[0] 
       (.C(clk_BUFG),
        .CE(E),
        .CLR(output0),
        .D(Inst_contador_n_7),
        .Q(\caracter_reg[6]_0 [0]));
  (* OPT_INSERTED_REPDRIVER *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  FDCE #(
    .INIT(1'b0)) 
    \caracter_reg[0]_lopt_replica 
       (.C(clk_BUFG),
        .CE(E),
        .CLR(output0),
        .D(Inst_contador_n_7),
        .Q(\caracter_reg[0]_lopt_replica_1 ));
  FDCE #(
    .INIT(1'b0)) 
    \caracter_reg[1] 
       (.C(clk_BUFG),
        .CE(E),
        .CLR(output0),
        .D(D[0]),
        .Q(\caracter_reg[6]_0 [1]));
  (* OPT_INSERTED_REPDRIVER *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  FDCE #(
    .INIT(1'b0)) 
    \caracter_reg[1]_lopt_replica 
       (.C(clk_BUFG),
        .CE(E),
        .CLR(output0),
        .D(D[0]),
        .Q(\caracter_reg[1]_lopt_replica_1 ));
  FDCE #(
    .INIT(1'b0)) 
    \caracter_reg[2] 
       (.C(clk_BUFG),
        .CE(E),
        .CLR(output0),
        .D(Inst_contador_n_6),
        .Q(\caracter_reg[6]_0 [2]));
  (* OPT_INSERTED_REPDRIVER *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  FDCE #(
    .INIT(1'b0)) 
    \caracter_reg[2]_lopt_replica 
       (.C(clk_BUFG),
        .CE(E),
        .CLR(output0),
        .D(Inst_contador_n_6),
        .Q(\caracter_reg[2]_lopt_replica_1 ));
  FDCE #(
    .INIT(1'b0)) 
    \caracter_reg[3] 
       (.C(clk_BUFG),
        .CE(E),
        .CLR(output0),
        .D(Inst_contador_n_5),
        .Q(\caracter_reg[6]_0 [3]));
  (* OPT_INSERTED_REPDRIVER *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  FDCE #(
    .INIT(1'b0)) 
    \caracter_reg[3]_lopt_replica 
       (.C(clk_BUFG),
        .CE(E),
        .CLR(output0),
        .D(Inst_contador_n_5),
        .Q(\caracter_reg[3]_lopt_replica_1 ));
  FDCE #(
    .INIT(1'b0)) 
    \caracter_reg[4] 
       (.C(clk_BUFG),
        .CE(E),
        .CLR(output0),
        .D(Inst_contador_n_4),
        .Q(\caracter_reg[6]_0 [4]));
  (* OPT_INSERTED_REPDRIVER *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  FDCE #(
    .INIT(1'b0)) 
    \caracter_reg[4]_lopt_replica 
       (.C(clk_BUFG),
        .CE(E),
        .CLR(output0),
        .D(Inst_contador_n_4),
        .Q(\caracter_reg[4]_lopt_replica_1 ));
  FDPE #(
    .INIT(1'b1)) 
    \caracter_reg[5] 
       (.C(clk_BUFG),
        .CE(E),
        .D(D[1]),
        .PRE(output0),
        .Q(\caracter_reg[6]_0 [5]));
  (* OPT_INSERTED_REPDRIVER *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  FDPE #(
    .INIT(1'b1)) 
    \caracter_reg[5]_lopt_replica 
       (.C(clk_BUFG),
        .CE(E),
        .D(D[1]),
        .PRE(output0),
        .Q(\caracter_reg[5]_lopt_replica_1 ));
  FDCE #(
    .INIT(1'b0)) 
    \caracter_reg[6] 
       (.C(clk_BUFG),
        .CE(E),
        .CLR(output0),
        .D(Inst_contador_n_3),
        .Q(\caracter_reg[6]_0 [6]));
  (* OPT_INSERTED_REPDRIVER *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  FDCE #(
    .INIT(1'b0)) 
    \caracter_reg[6]_lopt_replica 
       (.C(clk_BUFG),
        .CE(E),
        .CLR(output0),
        .D(Inst_contador_n_3),
        .Q(\caracter_reg[6]_lopt_replica_1 ));
  FDRE #(
    .INIT(1'b0)) 
    \charact_dot_reg[0] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .D(Inst_contador_n_35),
        .Q(\charact_dot_reg_n_0_[0] ),
        .R(1'b0));
  FDPE #(
    .INIT(1'b1)) 
    dot_reg
       (.C(clk_BUFG),
        .CE(E),
        .D(Inst_contador_n_33),
        .PRE(output0),
        .Q(dot_OBUF));
endmodule

module edgedtctr
   (D,
    \sreg_reg[2]_0 ,
    \sreg_reg[0]_0 ,
    clk_BUFG,
    \v_reg[0] ,
    \v_reg[0]_0 );
  output [0:0]D;
  output \sreg_reg[2]_0 ;
  input \sreg_reg[0]_0 ;
  input clk_BUFG;
  input \v_reg[0] ;
  input \v_reg[0]_0 ;

  wire [0:0]D;
  wire clk_BUFG;
  wire [2:0]sreg;
  wire \sreg_reg[0]_0 ;
  wire \sreg_reg[2]_0 ;
  wire \v_reg[0] ;
  wire \v_reg[0]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \sreg_reg[0] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .D(\sreg_reg[0]_0 ),
        .Q(sreg[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sreg_reg[1] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .D(sreg[0]),
        .Q(sreg[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sreg_reg[2] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .D(sreg[1]),
        .Q(sreg[2]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h02000000)) 
    \v[0]_i_1 
       (.I0(sreg[2]),
        .I1(sreg[1]),
        .I2(sreg[0]),
        .I3(\v_reg[0] ),
        .I4(\v_reg[0]_0 ),
        .O(D));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hFD)) 
    \v[6]_i_3 
       (.I0(sreg[2]),
        .I1(sreg[1]),
        .I2(sreg[0]),
        .O(\sreg_reg[2]_0 ));
endmodule

(* ORIG_REF_NAME = "edgedtctr" *) 
module edgedtctr_4
   (sreg,
    D,
    \sreg_reg[2]_0 ,
    \sreg_reg[0]_0 ,
    clk_BUFG,
    \v_reg[3] ,
    \v_reg[3]_0 ,
    \v_reg[3]_1 );
  output [2:0]sreg;
  output [0:0]D;
  output \sreg_reg[2]_0 ;
  input \sreg_reg[0]_0 ;
  input clk_BUFG;
  input \v_reg[3] ;
  input \v_reg[3]_0 ;
  input \v_reg[3]_1 ;

  wire [0:0]D;
  wire clk_BUFG;
  wire [2:0]sreg;
  wire \sreg_reg[0]_0 ;
  wire \sreg_reg[2]_0 ;
  wire \v_reg[3] ;
  wire \v_reg[3]_0 ;
  wire \v_reg[3]_1 ;

  FDRE #(
    .INIT(1'b0)) 
    \sreg_reg[0] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .D(\sreg_reg[0]_0 ),
        .Q(sreg[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sreg_reg[1] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .D(sreg[0]),
        .Q(sreg[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sreg_reg[2] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .D(sreg[1]),
        .Q(sreg[2]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0008000000000000)) 
    \v[3]_i_1 
       (.I0(\v_reg[3] ),
        .I1(\v_reg[3]_0 ),
        .I2(sreg[0]),
        .I3(sreg[1]),
        .I4(sreg[2]),
        .I5(\v_reg[3]_1 ),
        .O(D));
  LUT3 #(
    .INIT(8'hFD)) 
    \v[4]_i_2 
       (.I0(sreg[2]),
        .I1(sreg[1]),
        .I2(sreg[0]),
        .O(\sreg_reg[2]_0 ));
endmodule

(* ORIG_REF_NAME = "edgedtctr" *) 
module edgedtctr_5
   (\sreg_reg[0]_0 ,
    \sreg_reg[2]_0 ,
    \sreg_reg[0]_1 ,
    clk_BUFG,
    sreg);
  output \sreg_reg[0]_0 ;
  output \sreg_reg[2]_0 ;
  input \sreg_reg[0]_1 ;
  input clk_BUFG;
  input [2:0]sreg;

  wire clk_BUFG;
  wire [2:0]sreg;
  wire [2:0]sreg_0;
  wire \sreg_reg[0]_0 ;
  wire \sreg_reg[0]_1 ;
  wire \sreg_reg[2]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    \sreg_reg[0] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .D(\sreg_reg[0]_1 ),
        .Q(sreg_0[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sreg_reg[1] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .D(sreg_0[0]),
        .Q(sreg_0[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sreg_reg[2] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .D(sreg_0[1]),
        .Q(sreg_0[2]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hFD)) 
    \v[4]_i_3 
       (.I0(sreg_0[2]),
        .I1(sreg_0[1]),
        .I2(sreg_0[0]),
        .O(\sreg_reg[2]_0 ));
  LUT6 #(
    .INIT(64'hEFEFEF00EFEFEFEF)) 
    \v[6]_i_4 
       (.I0(sreg_0[0]),
        .I1(sreg_0[1]),
        .I2(sreg_0[2]),
        .I3(sreg[0]),
        .I4(sreg[1]),
        .I5(sreg[2]),
        .O(\sreg_reg[0]_0 ));
endmodule

(* ORIG_REF_NAME = "edgedtctr" *) 
module edgedtctr_6
   (sreg,
    D,
    \sreg_reg[2]_0 ,
    \sreg_reg[0]_0 ,
    clk_BUFG,
    \v_reg[6] ,
    \v_reg[6]_0 ,
    \v_reg[6]_1 );
  output [2:0]sreg;
  output [0:0]D;
  output \sreg_reg[2]_0 ;
  input \sreg_reg[0]_0 ;
  input clk_BUFG;
  input \v_reg[6] ;
  input \v_reg[6]_0 ;
  input \v_reg[6]_1 ;

  wire [0:0]D;
  wire clk_BUFG;
  wire [2:0]sreg;
  wire \sreg_reg[0]_0 ;
  wire \sreg_reg[2]_0 ;
  wire \v_reg[6] ;
  wire \v_reg[6]_0 ;
  wire \v_reg[6]_1 ;

  FDRE #(
    .INIT(1'b0)) 
    \sreg_reg[0] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .D(\sreg_reg[0]_0 ),
        .Q(sreg[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sreg_reg[1] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .D(sreg[0]),
        .Q(sreg[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sreg_reg[2] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .D(sreg[1]),
        .Q(sreg[2]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hFD)) 
    \v[5]_i_2 
       (.I0(sreg[2]),
        .I1(sreg[1]),
        .I2(sreg[0]),
        .O(\sreg_reg[2]_0 ));
  LUT6 #(
    .INIT(64'h5551000000000000)) 
    \v[6]_i_1 
       (.I0(\v_reg[6] ),
        .I1(sreg[2]),
        .I2(sreg[1]),
        .I3(sreg[0]),
        .I4(\v_reg[6]_0 ),
        .I5(\v_reg[6]_1 ),
        .O(D));
endmodule

(* ORIG_REF_NAME = "edgedtctr" *) 
module edgedtctr_7
   (D,
    \sreg_reg[2]_0 ,
    \sreg_reg[0]_0 ,
    \sreg_reg[0]_1 ,
    clk_BUFG,
    \v_reg[2] ,
    \v_reg[2]_0 ,
    \v_reg[2]_1 ,
    \v_reg[2]_2 ,
    \v_reg[5] ,
    sreg);
  output [3:0]D;
  output \sreg_reg[2]_0 ;
  output \sreg_reg[0]_0 ;
  input \sreg_reg[0]_1 ;
  input clk_BUFG;
  input \v_reg[2] ;
  input \v_reg[2]_0 ;
  input \v_reg[2]_1 ;
  input \v_reg[2]_2 ;
  input \v_reg[5] ;
  input [2:0]sreg;

  wire [3:0]D;
  wire clk_BUFG;
  wire [2:0]sreg;
  wire [2:0]sreg_0;
  wire \sreg_reg[0]_0 ;
  wire \sreg_reg[0]_1 ;
  wire \sreg_reg[2]_0 ;
  wire \v_reg[2] ;
  wire \v_reg[2]_0 ;
  wire \v_reg[2]_1 ;
  wire \v_reg[2]_2 ;
  wire \v_reg[5] ;

  FDRE #(
    .INIT(1'b0)) 
    \sreg_reg[0] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .D(\sreg_reg[0]_1 ),
        .Q(sreg_0[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sreg_reg[1] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .D(sreg_0[0]),
        .Q(sreg_0[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sreg_reg[2] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .D(sreg_0[1]),
        .Q(sreg_0[2]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h28000000)) 
    \v[1]_i_1 
       (.I0(\sreg_reg[2]_0 ),
        .I1(\v_reg[2] ),
        .I2(\v_reg[2]_1 ),
        .I3(\v_reg[2]_0 ),
        .I4(\v_reg[2]_2 ),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h48008000)) 
    \v[2]_i_1 
       (.I0(\sreg_reg[2]_0 ),
        .I1(\v_reg[2] ),
        .I2(\v_reg[2]_0 ),
        .I3(\v_reg[2]_1 ),
        .I4(\v_reg[2]_2 ),
        .O(D[1]));
  LUT6 #(
    .INIT(64'hEFEFEF00EFEFEFEF)) 
    \v[3]_i_2 
       (.I0(sreg_0[0]),
        .I1(sreg_0[1]),
        .I2(sreg_0[2]),
        .I3(sreg[0]),
        .I4(sreg[1]),
        .I5(sreg[2]),
        .O(\sreg_reg[0]_0 ));
  LUT5 #(
    .INIT(32'h20008000)) 
    \v[4]_i_1 
       (.I0(\sreg_reg[2]_0 ),
        .I1(\v_reg[2] ),
        .I2(\v_reg[2]_0 ),
        .I3(\v_reg[2]_1 ),
        .I4(\v_reg[2]_2 ),
        .O(D[2]));
  LUT6 #(
    .INIT(64'h0008000088808888)) 
    \v[5]_i_1 
       (.I0(\v_reg[5] ),
        .I1(\v_reg[2]_0 ),
        .I2(sreg_0[0]),
        .I3(sreg_0[1]),
        .I4(sreg_0[2]),
        .I5(\v_reg[2] ),
        .O(D[3]));
  LUT3 #(
    .INIT(8'hFD)) 
    \v[6]_i_2 
       (.I0(sreg_0[2]),
        .I1(sreg_0[1]),
        .I2(sreg_0[0]),
        .O(\sreg_reg[2]_0 ));
endmodule

module fsm
   (D,
    \FSM_onehot_current_refresco_reg[0] ,
    Q,
    \current_state_reg[1]_0 ,
    \disp_dinero[1]_0 ,
    \current_state_reg[1]_1 ,
    \reg_reg[0] ,
    \FSM_onehot_current_refresco_reg[2] ,
    \reg_reg[1] ,
    \current_state_reg[1]_2 ,
    \reg_reg[1]_0 ,
    \FSM_onehot_current_refresco_reg[4] ,
    \FSM_onehot_current_refresco_reg[2]_0 ,
    \FSM_onehot_current_refresco_reg[1] ,
    \current_state_reg[0]_0 ,
    \FSM_onehot_current_refresco_reg[4]_0 ,
    \current_state_reg[0]_1 ,
    \FSM_onehot_current_refresco_reg[1]_0 ,
    \FSM_onehot_current_refresco_reg[0]_0 ,
    \FSM_onehot_current_refresco_reg[3] ,
    \current_state_reg[0]_2 ,
    \FSM_onehot_current_refresco_reg[1]_1 ,
    \FSM_onehot_current_refresco_reg[3]_0 ,
    \current_state_reg[0]_3 ,
    \FSM_onehot_current_refresco_reg[0]_1 ,
    \FSM_onehot_current_refresco_reg[3]_1 ,
    \FSM_onehot_current_refresco_reg[3]_2 ,
    \current_state_reg[1]_3 ,
    E,
    \total_reg[0] ,
    \current_state_reg[1]_4 ,
    \current_state_reg[0]_4 ,
    \disp_dinero[3]_1 ,
    \disp_dinero[2]_2 ,
    \disp_dinero[5]_3 ,
    refresco_OBUF,
    devolver_OBUF,
    clk_BUFG,
    output0,
    \caracter_reg[1] ,
    \caracter_reg[1]_0 ,
    \caracter_reg[1]_1 ,
    \caracter_reg[2] ,
    \caracter_reg[2]_0 ,
    \caracter_reg[6] ,
    \caracter_reg[1]_2 ,
    \caracter_reg[1]_3 ,
    \caracter_reg[2]_1 ,
    \caracter_reg[1]_4 ,
    \caracter_reg[2]_2 ,
    \caracter_reg[0] ,
    \caracter_reg[1]_5 ,
    \caracter_reg[1]_6 ,
    \caracter[0]_i_4 ,
    \caracter_reg[4] ,
    plusOp,
    \caracter_reg[1]_7 ,
    \total_reg[7]_i_2 ,
    \current_state_reg[0]_5 ,
    button_ok_IBUF,
    CO,
    ref_option_IBUF);
  output [1:0]D;
  output \FSM_onehot_current_refresco_reg[0] ;
  output [2:0]Q;
  output [1:0]\current_state_reg[1]_0 ;
  output [0:0]\disp_dinero[1]_0 ;
  output \current_state_reg[1]_1 ;
  output \reg_reg[0] ;
  output \FSM_onehot_current_refresco_reg[2] ;
  output \reg_reg[1] ;
  output \current_state_reg[1]_2 ;
  output \reg_reg[1]_0 ;
  output \FSM_onehot_current_refresco_reg[4] ;
  output \FSM_onehot_current_refresco_reg[2]_0 ;
  output \FSM_onehot_current_refresco_reg[1] ;
  output \current_state_reg[0]_0 ;
  output \FSM_onehot_current_refresco_reg[4]_0 ;
  output \current_state_reg[0]_1 ;
  output \FSM_onehot_current_refresco_reg[1]_0 ;
  output \FSM_onehot_current_refresco_reg[0]_0 ;
  output \FSM_onehot_current_refresco_reg[3] ;
  output \current_state_reg[0]_2 ;
  output \FSM_onehot_current_refresco_reg[1]_1 ;
  output \FSM_onehot_current_refresco_reg[3]_0 ;
  output \current_state_reg[0]_3 ;
  output \FSM_onehot_current_refresco_reg[0]_1 ;
  output \FSM_onehot_current_refresco_reg[3]_1 ;
  output \FSM_onehot_current_refresco_reg[3]_2 ;
  output \current_state_reg[1]_3 ;
  output [0:0]E;
  output [0:0]\total_reg[0] ;
  output \current_state_reg[1]_4 ;
  output \current_state_reg[0]_4 ;
  output [1:0]\disp_dinero[3]_1 ;
  output [0:0]\disp_dinero[2]_2 ;
  output [0:0]\disp_dinero[5]_3 ;
  output refresco_OBUF;
  output devolver_OBUF;
  input clk_BUFG;
  input output0;
  input \caracter_reg[1] ;
  input \caracter_reg[1]_0 ;
  input \caracter_reg[1]_1 ;
  input \caracter_reg[2] ;
  input \caracter_reg[2]_0 ;
  input [2:0]\caracter_reg[6] ;
  input \caracter_reg[1]_2 ;
  input \caracter_reg[1]_3 ;
  input \caracter_reg[2]_1 ;
  input \caracter_reg[1]_4 ;
  input \caracter_reg[2]_2 ;
  input \caracter_reg[0] ;
  input \caracter_reg[1]_5 ;
  input \caracter_reg[1]_6 ;
  input \caracter[0]_i_4 ;
  input \caracter_reg[4] ;
  input [0:0]plusOp;
  input \caracter_reg[1]_7 ;
  input [6:0]\total_reg[7]_i_2 ;
  input \current_state_reg[0]_5 ;
  input button_ok_IBUF;
  input [0:0]CO;
  input [3:0]ref_option_IBUF;

  wire [0:0]CO;
  wire [1:0]D;
  wire [0:0]E;
  wire \FSM_onehot_current_refresco_reg[0] ;
  wire \FSM_onehot_current_refresco_reg[0]_0 ;
  wire \FSM_onehot_current_refresco_reg[0]_1 ;
  wire \FSM_onehot_current_refresco_reg[1] ;
  wire \FSM_onehot_current_refresco_reg[1]_0 ;
  wire \FSM_onehot_current_refresco_reg[1]_1 ;
  wire \FSM_onehot_current_refresco_reg[2] ;
  wire \FSM_onehot_current_refresco_reg[2]_0 ;
  wire \FSM_onehot_current_refresco_reg[3] ;
  wire \FSM_onehot_current_refresco_reg[3]_0 ;
  wire \FSM_onehot_current_refresco_reg[3]_1 ;
  wire \FSM_onehot_current_refresco_reg[3]_2 ;
  wire \FSM_onehot_current_refresco_reg[4] ;
  wire \FSM_onehot_current_refresco_reg[4]_0 ;
  wire Inst_gestion_dinero_n_2;
  wire Inst_gestion_dinero_n_3;
  wire Inst_gestion_dinero_n_7;
  wire Inst_gestion_refresco_n_16;
  wire [2:0]Q;
  wire button_ok_IBUF;
  wire \caracter[0]_i_4 ;
  wire \caracter_reg[0] ;
  wire \caracter_reg[1] ;
  wire \caracter_reg[1]_0 ;
  wire \caracter_reg[1]_1 ;
  wire \caracter_reg[1]_2 ;
  wire \caracter_reg[1]_3 ;
  wire \caracter_reg[1]_4 ;
  wire \caracter_reg[1]_5 ;
  wire \caracter_reg[1]_6 ;
  wire \caracter_reg[1]_7 ;
  wire \caracter_reg[2] ;
  wire \caracter_reg[2]_0 ;
  wire \caracter_reg[2]_1 ;
  wire \caracter_reg[2]_2 ;
  wire \caracter_reg[4] ;
  wire [2:0]\caracter_reg[6] ;
  wire clk_BUFG;
  wire clr_refresco_r;
  wire \current_state_reg[0]_0 ;
  wire \current_state_reg[0]_1 ;
  wire \current_state_reg[0]_2 ;
  wire \current_state_reg[0]_3 ;
  wire \current_state_reg[0]_4 ;
  wire \current_state_reg[0]_5 ;
  wire [1:0]\current_state_reg[1]_0 ;
  wire \current_state_reg[1]_1 ;
  wire \current_state_reg[1]_2 ;
  wire \current_state_reg[1]_3 ;
  wire \current_state_reg[1]_4 ;
  wire devolver_OBUF;
  wire [0:0]\disp_dinero[1]_0 ;
  wire [0:0]\disp_dinero[2]_2 ;
  wire [1:0]\disp_dinero[3]_1 ;
  wire [0:0]\disp_dinero[5]_3 ;
  wire [1:0]next_state;
  wire \next_state_reg[0]_i_1_n_0 ;
  wire output0;
  wire [0:0]plusOp;
  wire [3:0]ref_option_IBUF;
  wire refresco_OBUF;
  wire \reg_reg[0] ;
  wire \reg_reg[1] ;
  wire \reg_reg[1]_0 ;
  wire [0:0]\total_reg[0] ;
  wire [6:0]\total_reg[7]_i_2 ;

  gestion_dinero Inst_gestion_dinero
       (.DI(\total_reg[0] ),
        .E(Inst_gestion_dinero_n_7),
        .Q(\current_state_reg[1]_0 ),
        .button_ok_IBUF(button_ok_IBUF),
        .\caracter[1]_i_6 (\caracter_reg[1]_4 ),
        .\caracter_reg[1] (\caracter_reg[1]_2 ),
        .\caracter_reg[1]_0 (\caracter_reg[1]_3 ),
        .\caracter_reg[2] (\caracter_reg[2]_1 ),
        .\caracter_reg[2]_0 (\caracter_reg[2]_2 ),
        .\caracter_reg[4] (\caracter_reg[6] ),
        .clk_BUFG(clk_BUFG),
        .clr_refresco_r(clr_refresco_r),
        .\current_state_reg[0] (Inst_gestion_dinero_n_2),
        .\current_state_reg[0]_0 (\current_state_reg[0]_4 ),
        .\current_state_reg[0]_1 (Inst_gestion_refresco_n_16),
        .\current_state_reg[0]_2 (\current_state_reg[0]_5 ),
        .\current_state_reg[1] (\current_state_reg[1]_1 ),
        .\current_state_reg[1]_0 (Inst_gestion_dinero_n_3),
        .\current_state_reg[1]_1 (\current_state_reg[1]_2 ),
        .\current_state_reg[1]_2 (\current_state_reg[1]_4 ),
        .devolver_OBUF(devolver_OBUF),
        .\disp_dinero[3]_1 (\disp_dinero[3]_1 ),
        .\disp_dinero[5]_3 (\disp_dinero[5]_3 ),
        .refresco_OBUF(refresco_OBUF),
        .\reg_reg[1] (\reg_reg[1] ),
        .\reg_reg[1]_0 (\reg_reg[1]_0 ),
        .\string_cent_decenas[1]5__186_carry (\disp_dinero[2]_2 ),
        .\total_reg[15]_0 (\disp_dinero[1]_0 ),
        .\total_reg[7]_i_2_0 (\total_reg[7]_i_2 ));
  gestion_refresco Inst_gestion_refresco
       (.CO(CO),
        .D(D),
        .E(E),
        .\FSM_onehot_current_refresco_reg[0]_0 (\FSM_onehot_current_refresco_reg[0] ),
        .\FSM_onehot_current_refresco_reg[0]_1 (\FSM_onehot_current_refresco_reg[0]_0 ),
        .\FSM_onehot_current_refresco_reg[0]_2 (\FSM_onehot_current_refresco_reg[0]_1 ),
        .\FSM_onehot_current_refresco_reg[1]_0 (\FSM_onehot_current_refresco_reg[1] ),
        .\FSM_onehot_current_refresco_reg[1]_1 (\FSM_onehot_current_refresco_reg[1]_0 ),
        .\FSM_onehot_current_refresco_reg[1]_2 (\FSM_onehot_current_refresco_reg[1]_1 ),
        .\FSM_onehot_current_refresco_reg[2]_0 (\FSM_onehot_current_refresco_reg[2] ),
        .\FSM_onehot_current_refresco_reg[2]_1 (\FSM_onehot_current_refresco_reg[2]_0 ),
        .\FSM_onehot_current_refresco_reg[2]_2 (\current_state_reg[1]_0 ),
        .\FSM_onehot_current_refresco_reg[3]_0 (\FSM_onehot_current_refresco_reg[3] ),
        .\FSM_onehot_current_refresco_reg[3]_1 (\FSM_onehot_current_refresco_reg[3]_0 ),
        .\FSM_onehot_current_refresco_reg[3]_2 (\FSM_onehot_current_refresco_reg[3]_1 ),
        .\FSM_onehot_current_refresco_reg[3]_3 (\FSM_onehot_current_refresco_reg[3]_2 ),
        .\FSM_onehot_current_refresco_reg[4]_0 (\FSM_onehot_current_refresco_reg[4] ),
        .\FSM_onehot_current_refresco_reg[4]_1 (\FSM_onehot_current_refresco_reg[4]_0 ),
        .Q(Q),
        .button_ok_IBUF(button_ok_IBUF),
        .\caracter[0]_i_4_0 (\caracter[0]_i_4 ),
        .\caracter_reg[0] (\caracter_reg[0] ),
        .\caracter_reg[1] (Inst_gestion_dinero_n_3),
        .\caracter_reg[1]_0 (\caracter_reg[1] ),
        .\caracter_reg[1]_1 (\caracter_reg[1]_0 ),
        .\caracter_reg[1]_2 (\caracter_reg[1]_1 ),
        .\caracter_reg[1]_3 (\caracter_reg[1]_5 ),
        .\caracter_reg[1]_4 (\caracter_reg[1]_6 ),
        .\caracter_reg[1]_5 (\caracter_reg[1]_4 ),
        .\caracter_reg[1]_6 (\caracter_reg[1]_7 ),
        .\caracter_reg[2] (\caracter_reg[2] ),
        .\caracter_reg[2]_0 (\caracter_reg[2]_0 ),
        .\caracter_reg[4] (\caracter_reg[4] ),
        .\caracter_reg[5] (\current_state_reg[0]_3 ),
        .\caracter_reg[6] (\caracter_reg[6] ),
        .\caracter_reg[6]_0 (Inst_gestion_dinero_n_2),
        .clk_BUFG(clk_BUFG),
        .clr_refresco_r(clr_refresco_r),
        .\current_state_reg[0] (\current_state_reg[0]_0 ),
        .\current_state_reg[0]_0 (\current_state_reg[0]_1 ),
        .\current_state_reg[1] (\current_state_reg[1]_3 ),
        .\disp_dinero[1]_0 (\disp_dinero[1]_0 ),
        .\next_state_reg[1]_i_1 (\current_state_reg[0]_2 ),
        .\next_state_reg[1]_i_1_0 (\total_reg[7]_i_2 [6]),
        .plusOp(plusOp),
        .ref_option_IBUF(ref_option_IBUF),
        .\reg_reg[0] (\reg_reg[0] ),
        .\v_reg[6] (Inst_gestion_refresco_n_16));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \caracter[5]_i_2 
       (.I0(\current_state_reg[1]_0 [0]),
        .I1(\current_state_reg[1]_0 [1]),
        .O(\current_state_reg[0]_3 ));
  FDCE #(
    .INIT(1'b0)) 
    clr_dinero_r_reg
       (.C(clk_BUFG),
        .CE(1'b1),
        .CLR(output0),
        .D(1'b1),
        .Q(clr_refresco_r));
  FDCE #(
    .INIT(1'b0)) 
    \current_state_reg[0] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .CLR(output0),
        .D(next_state[0]),
        .Q(\current_state_reg[1]_0 [0]));
  FDCE #(
    .INIT(1'b0)) 
    \current_state_reg[1] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .CLR(output0),
        .D(next_state[1]),
        .Q(\current_state_reg[1]_0 [1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \next_state_reg[0] 
       (.CLR(1'b0),
        .D(\next_state_reg[0]_i_1_n_0 ),
        .G(Inst_gestion_dinero_n_7),
        .GE(1'b1),
        .Q(next_state[0]));
  (* SOFT_HLUTNM = "soft_lutpair80" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \next_state_reg[0]_i_1 
       (.I0(\current_state_reg[1]_0 [1]),
        .I1(\current_state_reg[1]_0 [0]),
        .O(\next_state_reg[0]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \next_state_reg[1] 
       (.CLR(1'b0),
        .D(\current_state_reg[1]_0 [0]),
        .G(Inst_gestion_dinero_n_7),
        .GE(1'b1),
        .Q(next_state[1]));
  LUT2 #(
    .INIT(4'h2)) 
    \next_state_reg[1]_i_5 
       (.I0(\current_state_reg[1]_0 [0]),
        .I1(\current_state_reg[1]_0 [1]),
        .O(\current_state_reg[0]_2 ));
endmodule

module gestion_dinero
   (\current_state_reg[1] ,
    \total_reg[15]_0 ,
    \current_state_reg[0] ,
    \current_state_reg[1]_0 ,
    \reg_reg[1] ,
    \current_state_reg[1]_1 ,
    \reg_reg[1]_0 ,
    E,
    DI,
    \current_state_reg[1]_2 ,
    \current_state_reg[0]_0 ,
    \disp_dinero[3]_1 ,
    \string_cent_decenas[1]5__186_carry ,
    \disp_dinero[5]_3 ,
    refresco_OBUF,
    devolver_OBUF,
    Q,
    \caracter_reg[4] ,
    \caracter_reg[1] ,
    \caracter_reg[1]_0 ,
    \caracter_reg[2] ,
    \caracter[1]_i_6 ,
    \caracter_reg[2]_0 ,
    \total_reg[7]_i_2_0 ,
    \current_state_reg[0]_1 ,
    \current_state_reg[0]_2 ,
    button_ok_IBUF,
    clr_refresco_r,
    clk_BUFG);
  output \current_state_reg[1] ;
  output \total_reg[15]_0 ;
  output \current_state_reg[0] ;
  output \current_state_reg[1]_0 ;
  output \reg_reg[1] ;
  output \current_state_reg[1]_1 ;
  output \reg_reg[1]_0 ;
  output [0:0]E;
  output [0:0]DI;
  output \current_state_reg[1]_2 ;
  output \current_state_reg[0]_0 ;
  output [1:0]\disp_dinero[3]_1 ;
  output \string_cent_decenas[1]5__186_carry ;
  output [0:0]\disp_dinero[5]_3 ;
  output refresco_OBUF;
  output devolver_OBUF;
  input [1:0]Q;
  input [2:0]\caracter_reg[4] ;
  input \caracter_reg[1] ;
  input \caracter_reg[1]_0 ;
  input \caracter_reg[2] ;
  input \caracter[1]_i_6 ;
  input \caracter_reg[2]_0 ;
  input [6:0]\total_reg[7]_i_2_0 ;
  input \current_state_reg[0]_1 ;
  input \current_state_reg[0]_2 ;
  input button_ok_IBUF;
  input clr_refresco_r;
  input clk_BUFG;

  wire [0:0]DI;
  wire [0:0]E;
  wire \FSM_sequential_current_state[1]_i_1_n_0 ;
  wire \FSM_sequential_current_state_reg_n_0_[1] ;
  wire \FSM_sequential_next_state_reg[0]_i_1_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_10_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_11_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_12_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_13_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_14_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_15_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_16_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_17_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_18_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_19_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_1_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_20_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_21_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_22_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_23_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_24_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_25_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_26_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_27_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_28_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_29_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_2_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_30_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_31_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_32_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_33_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_34_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_35_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_36_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_37_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_38_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_39_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_3_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_40_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_41_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_42_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_43_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_44_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_45_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_6_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_7_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_8_n_0 ;
  wire \FSM_sequential_next_state_reg[1]_i_9_n_0 ;
  wire [1:0]Q;
  wire [0:0]aux;
  wire \aux_reg[0]_i_1_n_0 ;
  wire bdf_dinero;
  wire bdf_reg_i_1_n_0;
  wire button_ok_IBUF;
  wire \caracter[1]_i_6 ;
  wire \caracter_reg[1] ;
  wire \caracter_reg[1]_0 ;
  wire \caracter_reg[2] ;
  wire \caracter_reg[2]_0 ;
  wire [2:0]\caracter_reg[4] ;
  wire clk_BUFG;
  wire clr_refresco_r;
  wire [0:0]current_state;
  wire \current_state_reg[0] ;
  wire \current_state_reg[0]_0 ;
  wire \current_state_reg[0]_1 ;
  wire \current_state_reg[0]_2 ;
  wire \current_state_reg[1] ;
  wire \current_state_reg[1]_0 ;
  wire \current_state_reg[1]_1 ;
  wire \current_state_reg[1]_2 ;
  wire data0;
  wire devolver_OBUF;
  wire [1:0]\disp_dinero[3]_1 ;
  wire [0:0]\disp_dinero[5]_3 ;
  wire inst_int_to_string_n_11;
  wire inst_int_to_string_n_12;
  wire inst_int_to_string_n_9;
  wire [1:0]next_state__0;
  wire refresco_OBUF;
  wire \reg_reg[1] ;
  wire \reg_reg[1]_0 ;
  wire \string_cent_decenas[1]5__186_carry ;
  wire total;
  wire \total[0]_i_1_n_0 ;
  wire \total[10]_i_1_n_0 ;
  wire \total[11]_i_1_n_0 ;
  wire \total[12]_i_1_n_0 ;
  wire \total[13]_i_1_n_0 ;
  wire \total[14]_i_1_n_0 ;
  wire \total[15]_i_1_n_0 ;
  wire \total[16]_i_1_n_0 ;
  wire \total[17]_i_1_n_0 ;
  wire \total[18]_i_1_n_0 ;
  wire \total[19]_i_1_n_0 ;
  wire \total[1]_i_1_n_0 ;
  wire \total[20]_i_1_n_0 ;
  wire \total[21]_i_1_n_0 ;
  wire \total[22]_i_1_n_0 ;
  wire \total[23]_i_1_n_0 ;
  wire \total[24]_i_1_n_0 ;
  wire \total[25]_i_1_n_0 ;
  wire \total[26]_i_1_n_0 ;
  wire \total[27]_i_1_n_0 ;
  wire \total[28]_i_1_n_0 ;
  wire \total[29]_i_1_n_0 ;
  wire \total[2]_i_1_n_0 ;
  wire \total[30]_i_1_n_0 ;
  wire \total[31]_i_2_n_0 ;
  wire \total[3]_i_1_n_0 ;
  wire \total[3]_i_3_n_0 ;
  wire \total[3]_i_4_n_0 ;
  wire \total[3]_i_5_n_0 ;
  wire \total[3]_i_6_n_0 ;
  wire \total[4]_i_1_n_0 ;
  wire \total[5]_i_1_n_0 ;
  wire \total[6]_i_1_n_0 ;
  wire \total[7]_i_1_n_0 ;
  wire \total[7]_i_3_n_0 ;
  wire \total[7]_i_4_n_0 ;
  wire \total[7]_i_5_n_0 ;
  wire \total[8]_i_1_n_0 ;
  wire \total[9]_i_1_n_0 ;
  wire \total_reg[11]_i_2_n_0 ;
  wire \total_reg[11]_i_2_n_4 ;
  wire \total_reg[11]_i_2_n_5 ;
  wire \total_reg[11]_i_2_n_6 ;
  wire \total_reg[11]_i_2_n_7 ;
  wire \total_reg[15]_0 ;
  wire \total_reg[15]_i_2_n_0 ;
  wire \total_reg[15]_i_2_n_4 ;
  wire \total_reg[15]_i_2_n_5 ;
  wire \total_reg[15]_i_2_n_6 ;
  wire \total_reg[15]_i_2_n_7 ;
  wire \total_reg[19]_i_2_n_0 ;
  wire \total_reg[19]_i_2_n_4 ;
  wire \total_reg[19]_i_2_n_5 ;
  wire \total_reg[19]_i_2_n_6 ;
  wire \total_reg[19]_i_2_n_7 ;
  wire \total_reg[23]_i_2_n_0 ;
  wire \total_reg[23]_i_2_n_4 ;
  wire \total_reg[23]_i_2_n_5 ;
  wire \total_reg[23]_i_2_n_6 ;
  wire \total_reg[23]_i_2_n_7 ;
  wire \total_reg[27]_i_2_n_0 ;
  wire \total_reg[27]_i_2_n_4 ;
  wire \total_reg[27]_i_2_n_5 ;
  wire \total_reg[27]_i_2_n_6 ;
  wire \total_reg[27]_i_2_n_7 ;
  wire \total_reg[31]_i_3_n_4 ;
  wire \total_reg[31]_i_3_n_5 ;
  wire \total_reg[31]_i_3_n_6 ;
  wire \total_reg[31]_i_3_n_7 ;
  wire \total_reg[3]_i_2_n_0 ;
  wire \total_reg[3]_i_2_n_4 ;
  wire \total_reg[3]_i_2_n_5 ;
  wire \total_reg[3]_i_2_n_6 ;
  wire \total_reg[3]_i_2_n_7 ;
  wire [6:0]\total_reg[7]_i_2_0 ;
  wire \total_reg[7]_i_2_n_0 ;
  wire \total_reg[7]_i_2_n_4 ;
  wire \total_reg[7]_i_2_n_5 ;
  wire \total_reg[7]_i_2_n_6 ;
  wire \total_reg[7]_i_2_n_7 ;
  wire \total_reg_n_0_[0] ;
  wire \total_reg_n_0_[10] ;
  wire \total_reg_n_0_[11] ;
  wire \total_reg_n_0_[12] ;
  wire \total_reg_n_0_[13] ;
  wire \total_reg_n_0_[14] ;
  wire \total_reg_n_0_[15] ;
  wire \total_reg_n_0_[16] ;
  wire \total_reg_n_0_[17] ;
  wire \total_reg_n_0_[18] ;
  wire \total_reg_n_0_[19] ;
  wire \total_reg_n_0_[1] ;
  wire \total_reg_n_0_[20] ;
  wire \total_reg_n_0_[21] ;
  wire \total_reg_n_0_[22] ;
  wire \total_reg_n_0_[23] ;
  wire \total_reg_n_0_[24] ;
  wire \total_reg_n_0_[25] ;
  wire \total_reg_n_0_[26] ;
  wire \total_reg_n_0_[27] ;
  wire \total_reg_n_0_[28] ;
  wire \total_reg_n_0_[29] ;
  wire \total_reg_n_0_[2] ;
  wire \total_reg_n_0_[30] ;
  wire \total_reg_n_0_[31] ;
  wire \total_reg_n_0_[3] ;
  wire \total_reg_n_0_[4] ;
  wire \total_reg_n_0_[5] ;
  wire \total_reg_n_0_[6] ;
  wire \total_reg_n_0_[7] ;
  wire \total_reg_n_0_[8] ;
  wire \total_reg_n_0_[9] ;
  wire [2:0]\NLW_FSM_sequential_next_state_reg[1]_i_22_CO_UNCONNECTED ;
  wire [3:0]\NLW_FSM_sequential_next_state_reg[1]_i_22_O_UNCONNECTED ;
  wire [2:0]\NLW_FSM_sequential_next_state_reg[1]_i_31_CO_UNCONNECTED ;
  wire [3:0]\NLW_FSM_sequential_next_state_reg[1]_i_31_O_UNCONNECTED ;
  wire [2:0]\NLW_FSM_sequential_next_state_reg[1]_i_4_CO_UNCONNECTED ;
  wire [3:0]\NLW_FSM_sequential_next_state_reg[1]_i_4_O_UNCONNECTED ;
  wire [2:0]\NLW_FSM_sequential_next_state_reg[1]_i_9_CO_UNCONNECTED ;
  wire [3:0]\NLW_FSM_sequential_next_state_reg[1]_i_9_O_UNCONNECTED ;
  wire [2:0]\NLW_total_reg[11]_i_2_CO_UNCONNECTED ;
  wire [2:0]\NLW_total_reg[15]_i_2_CO_UNCONNECTED ;
  wire [2:0]\NLW_total_reg[19]_i_2_CO_UNCONNECTED ;
  wire [2:0]\NLW_total_reg[23]_i_2_CO_UNCONNECTED ;
  wire [2:0]\NLW_total_reg[27]_i_2_CO_UNCONNECTED ;
  wire [3:0]\NLW_total_reg[31]_i_3_CO_UNCONNECTED ;
  wire [2:0]\NLW_total_reg[3]_i_2_CO_UNCONNECTED ;
  wire [2:0]\NLW_total_reg[7]_i_2_CO_UNCONNECTED ;

  LUT3 #(
    .INIT(8'h1F)) 
    \FSM_sequential_current_state[1]_i_1 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(clr_refresco_r),
        .O(\FSM_sequential_current_state[1]_i_1_n_0 ));
  (* FSM_ENCODED_STATES = "iSTATE:11,s1:01,s0:00,iSTATE0:10" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_current_state_reg[0] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(next_state__0[0]),
        .Q(current_state));
  (* FSM_ENCODED_STATES = "iSTATE:11,s1:01,s0:00,iSTATE0:10" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_current_state_reg[1] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(next_state__0[1]),
        .Q(\FSM_sequential_current_state_reg_n_0_[1] ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_next_state_reg[0] 
       (.CLR(1'b0),
        .D(\FSM_sequential_next_state_reg[0]_i_1_n_0 ),
        .G(\FSM_sequential_next_state_reg[1]_i_2_n_0 ),
        .GE(1'b1),
        .Q(next_state__0[0]));
  LUT2 #(
    .INIT(4'h1)) 
    \FSM_sequential_next_state_reg[0]_i_1 
       (.I0(\FSM_sequential_current_state_reg_n_0_[1] ),
        .I1(current_state),
        .O(\FSM_sequential_next_state_reg[0]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_next_state_reg[1] 
       (.CLR(1'b0),
        .D(\FSM_sequential_next_state_reg[1]_i_1_n_0 ),
        .G(\FSM_sequential_next_state_reg[1]_i_2_n_0 ),
        .GE(1'b1),
        .Q(next_state__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT4 #(
    .INIT(16'h2023)) 
    \FSM_sequential_next_state_reg[1]_i_1 
       (.I0(button_ok_IBUF),
        .I1(\FSM_sequential_current_state_reg_n_0_[1] ),
        .I2(current_state),
        .I3(\FSM_sequential_next_state_reg[1]_i_3_n_0 ),
        .O(\FSM_sequential_next_state_reg[1]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \FSM_sequential_next_state_reg[1]_i_10 
       (.I0(\total_reg_n_0_[30] ),
        .I1(\total_reg_n_0_[31] ),
        .O(\FSM_sequential_next_state_reg[1]_i_10_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \FSM_sequential_next_state_reg[1]_i_11 
       (.I0(\total_reg_n_0_[29] ),
        .I1(\total_reg_n_0_[28] ),
        .O(\FSM_sequential_next_state_reg[1]_i_11_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \FSM_sequential_next_state_reg[1]_i_12 
       (.I0(\total_reg_n_0_[27] ),
        .I1(\total_reg_n_0_[26] ),
        .O(\FSM_sequential_next_state_reg[1]_i_12_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \FSM_sequential_next_state_reg[1]_i_13 
       (.I0(\total_reg_n_0_[25] ),
        .I1(\total_reg_n_0_[24] ),
        .O(\FSM_sequential_next_state_reg[1]_i_13_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \FSM_sequential_next_state_reg[1]_i_14 
       (.I0(\total_reg_n_0_[30] ),
        .I1(\total_reg_n_0_[31] ),
        .O(\FSM_sequential_next_state_reg[1]_i_14_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \FSM_sequential_next_state_reg[1]_i_15 
       (.I0(\total_reg_n_0_[28] ),
        .I1(\total_reg_n_0_[29] ),
        .O(\FSM_sequential_next_state_reg[1]_i_15_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \FSM_sequential_next_state_reg[1]_i_16 
       (.I0(\total_reg_n_0_[26] ),
        .I1(\total_reg_n_0_[27] ),
        .O(\FSM_sequential_next_state_reg[1]_i_16_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \FSM_sequential_next_state_reg[1]_i_17 
       (.I0(\total_reg_n_0_[24] ),
        .I1(\total_reg_n_0_[25] ),
        .O(\FSM_sequential_next_state_reg[1]_i_17_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \FSM_sequential_next_state_reg[1]_i_18 
       (.I0(\total_reg_n_0_[0] ),
        .I1(\total_reg_n_0_[1] ),
        .O(\FSM_sequential_next_state_reg[1]_i_18_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \FSM_sequential_next_state_reg[1]_i_19 
       (.I0(\total_reg_n_0_[13] ),
        .I1(\total_reg_n_0_[15] ),
        .O(\FSM_sequential_next_state_reg[1]_i_19_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT5 #(
    .INIT(32'hAAAFAAAC)) 
    \FSM_sequential_next_state_reg[1]_i_2 
       (.I0(button_ok_IBUF),
        .I1(\FSM_sequential_next_state_reg[1]_i_3_n_0 ),
        .I2(current_state),
        .I3(\FSM_sequential_current_state_reg_n_0_[1] ),
        .I4(data0),
        .O(\FSM_sequential_next_state_reg[1]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \FSM_sequential_next_state_reg[1]_i_20 
       (.I0(\total_reg_n_0_[17] ),
        .I1(\total_reg_n_0_[19] ),
        .O(\FSM_sequential_next_state_reg[1]_i_20_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \FSM_sequential_next_state_reg[1]_i_21 
       (.I0(\total_reg_n_0_[21] ),
        .I1(\total_reg_n_0_[23] ),
        .O(\FSM_sequential_next_state_reg[1]_i_21_n_0 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \FSM_sequential_next_state_reg[1]_i_22 
       (.CI(\FSM_sequential_next_state_reg[1]_i_31_n_0 ),
        .CO({\FSM_sequential_next_state_reg[1]_i_22_n_0 ,\NLW_FSM_sequential_next_state_reg[1]_i_22_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\FSM_sequential_next_state_reg[1]_i_32_n_0 ,\FSM_sequential_next_state_reg[1]_i_33_n_0 ,\FSM_sequential_next_state_reg[1]_i_34_n_0 ,\FSM_sequential_next_state_reg[1]_i_35_n_0 }),
        .O(\NLW_FSM_sequential_next_state_reg[1]_i_22_O_UNCONNECTED [3:0]),
        .S({\FSM_sequential_next_state_reg[1]_i_36_n_0 ,\FSM_sequential_next_state_reg[1]_i_37_n_0 ,\FSM_sequential_next_state_reg[1]_i_38_n_0 ,\FSM_sequential_next_state_reg[1]_i_39_n_0 }));
  LUT2 #(
    .INIT(4'hE)) 
    \FSM_sequential_next_state_reg[1]_i_23 
       (.I0(\total_reg_n_0_[23] ),
        .I1(\total_reg_n_0_[22] ),
        .O(\FSM_sequential_next_state_reg[1]_i_23_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \FSM_sequential_next_state_reg[1]_i_24 
       (.I0(\total_reg_n_0_[21] ),
        .I1(\total_reg_n_0_[20] ),
        .O(\FSM_sequential_next_state_reg[1]_i_24_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \FSM_sequential_next_state_reg[1]_i_25 
       (.I0(\total_reg_n_0_[19] ),
        .I1(\total_reg_n_0_[18] ),
        .O(\FSM_sequential_next_state_reg[1]_i_25_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \FSM_sequential_next_state_reg[1]_i_26 
       (.I0(\total_reg_n_0_[17] ),
        .I1(\total_reg_n_0_[16] ),
        .O(\FSM_sequential_next_state_reg[1]_i_26_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \FSM_sequential_next_state_reg[1]_i_27 
       (.I0(\total_reg_n_0_[22] ),
        .I1(\total_reg_n_0_[23] ),
        .O(\FSM_sequential_next_state_reg[1]_i_27_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \FSM_sequential_next_state_reg[1]_i_28 
       (.I0(\total_reg_n_0_[20] ),
        .I1(\total_reg_n_0_[21] ),
        .O(\FSM_sequential_next_state_reg[1]_i_28_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \FSM_sequential_next_state_reg[1]_i_29 
       (.I0(\total_reg_n_0_[18] ),
        .I1(\total_reg_n_0_[19] ),
        .O(\FSM_sequential_next_state_reg[1]_i_29_n_0 ));
  LUT6 #(
    .INIT(64'h0000000001000000)) 
    \FSM_sequential_next_state_reg[1]_i_3 
       (.I0(inst_int_to_string_n_11),
        .I1(\total_reg_n_0_[7] ),
        .I2(\total_reg_n_0_[4] ),
        .I3(\FSM_sequential_next_state_reg[1]_i_6_n_0 ),
        .I4(\FSM_sequential_next_state_reg[1]_i_7_n_0 ),
        .I5(\FSM_sequential_next_state_reg[1]_i_8_n_0 ),
        .O(\FSM_sequential_next_state_reg[1]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \FSM_sequential_next_state_reg[1]_i_30 
       (.I0(\total_reg_n_0_[16] ),
        .I1(\total_reg_n_0_[17] ),
        .O(\FSM_sequential_next_state_reg[1]_i_30_n_0 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \FSM_sequential_next_state_reg[1]_i_31 
       (.CI(1'b0),
        .CO({\FSM_sequential_next_state_reg[1]_i_31_n_0 ,\NLW_FSM_sequential_next_state_reg[1]_i_31_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\total_reg_n_0_[7] ,\FSM_sequential_next_state_reg[1]_i_40_n_0 ,\total_reg_n_0_[3] ,\FSM_sequential_next_state_reg[1]_i_41_n_0 }),
        .O(\NLW_FSM_sequential_next_state_reg[1]_i_31_O_UNCONNECTED [3:0]),
        .S({\FSM_sequential_next_state_reg[1]_i_42_n_0 ,\FSM_sequential_next_state_reg[1]_i_43_n_0 ,\FSM_sequential_next_state_reg[1]_i_44_n_0 ,\FSM_sequential_next_state_reg[1]_i_45_n_0 }));
  LUT2 #(
    .INIT(4'hE)) 
    \FSM_sequential_next_state_reg[1]_i_32 
       (.I0(\total_reg_n_0_[15] ),
        .I1(\total_reg_n_0_[14] ),
        .O(\FSM_sequential_next_state_reg[1]_i_32_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \FSM_sequential_next_state_reg[1]_i_33 
       (.I0(\total_reg_n_0_[13] ),
        .I1(\total_reg_n_0_[12] ),
        .O(\FSM_sequential_next_state_reg[1]_i_33_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \FSM_sequential_next_state_reg[1]_i_34 
       (.I0(\total_reg_n_0_[11] ),
        .I1(\total_reg_n_0_[10] ),
        .O(\FSM_sequential_next_state_reg[1]_i_34_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \FSM_sequential_next_state_reg[1]_i_35 
       (.I0(\total_reg_n_0_[9] ),
        .I1(\total_reg_n_0_[8] ),
        .O(\FSM_sequential_next_state_reg[1]_i_35_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \FSM_sequential_next_state_reg[1]_i_36 
       (.I0(\total_reg_n_0_[14] ),
        .I1(\total_reg_n_0_[15] ),
        .O(\FSM_sequential_next_state_reg[1]_i_36_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \FSM_sequential_next_state_reg[1]_i_37 
       (.I0(\total_reg_n_0_[12] ),
        .I1(\total_reg_n_0_[13] ),
        .O(\FSM_sequential_next_state_reg[1]_i_37_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \FSM_sequential_next_state_reg[1]_i_38 
       (.I0(\total_reg_n_0_[10] ),
        .I1(\total_reg_n_0_[11] ),
        .O(\FSM_sequential_next_state_reg[1]_i_38_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \FSM_sequential_next_state_reg[1]_i_39 
       (.I0(\total_reg_n_0_[8] ),
        .I1(\total_reg_n_0_[9] ),
        .O(\FSM_sequential_next_state_reg[1]_i_39_n_0 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \FSM_sequential_next_state_reg[1]_i_4 
       (.CI(\FSM_sequential_next_state_reg[1]_i_9_n_0 ),
        .CO({data0,\NLW_FSM_sequential_next_state_reg[1]_i_4_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\FSM_sequential_next_state_reg[1]_i_10_n_0 ,\FSM_sequential_next_state_reg[1]_i_11_n_0 ,\FSM_sequential_next_state_reg[1]_i_12_n_0 ,\FSM_sequential_next_state_reg[1]_i_13_n_0 }),
        .O(\NLW_FSM_sequential_next_state_reg[1]_i_4_O_UNCONNECTED [3:0]),
        .S({\FSM_sequential_next_state_reg[1]_i_14_n_0 ,\FSM_sequential_next_state_reg[1]_i_15_n_0 ,\FSM_sequential_next_state_reg[1]_i_16_n_0 ,\FSM_sequential_next_state_reg[1]_i_17_n_0 }));
  LUT2 #(
    .INIT(4'h8)) 
    \FSM_sequential_next_state_reg[1]_i_40 
       (.I0(\total_reg_n_0_[5] ),
        .I1(\total_reg_n_0_[4] ),
        .O(\FSM_sequential_next_state_reg[1]_i_40_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \FSM_sequential_next_state_reg[1]_i_41 
       (.I0(\total_reg_n_0_[1] ),
        .I1(\total_reg_n_0_[0] ),
        .O(\FSM_sequential_next_state_reg[1]_i_41_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \FSM_sequential_next_state_reg[1]_i_42 
       (.I0(\total_reg_n_0_[6] ),
        .I1(\total_reg_n_0_[7] ),
        .O(\FSM_sequential_next_state_reg[1]_i_42_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \FSM_sequential_next_state_reg[1]_i_43 
       (.I0(\total_reg_n_0_[5] ),
        .I1(\total_reg_n_0_[4] ),
        .O(\FSM_sequential_next_state_reg[1]_i_43_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \FSM_sequential_next_state_reg[1]_i_44 
       (.I0(\total_reg_n_0_[2] ),
        .I1(\total_reg_n_0_[3] ),
        .O(\FSM_sequential_next_state_reg[1]_i_44_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \FSM_sequential_next_state_reg[1]_i_45 
       (.I0(\total_reg_n_0_[0] ),
        .I1(\total_reg_n_0_[1] ),
        .O(\FSM_sequential_next_state_reg[1]_i_45_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \FSM_sequential_next_state_reg[1]_i_6 
       (.I0(\total_reg_n_0_[9] ),
        .I1(\total_reg_n_0_[11] ),
        .O(\FSM_sequential_next_state_reg[1]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h2000000000000000)) 
    \FSM_sequential_next_state_reg[1]_i_7 
       (.I0(\total_reg_n_0_[2] ),
        .I1(\total_reg_n_0_[3] ),
        .I2(\FSM_sequential_next_state_reg[1]_i_18_n_0 ),
        .I3(\FSM_sequential_next_state_reg[1]_i_19_n_0 ),
        .I4(\total_reg_n_0_[5] ),
        .I5(\total_reg_n_0_[6] ),
        .O(\FSM_sequential_next_state_reg[1]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hFFFBFFFFFFFFFFFF)) 
    \FSM_sequential_next_state_reg[1]_i_8 
       (.I0(inst_int_to_string_n_9),
        .I1(inst_int_to_string_n_12),
        .I2(\total_reg_n_0_[31] ),
        .I3(\total_reg_n_0_[30] ),
        .I4(\FSM_sequential_next_state_reg[1]_i_20_n_0 ),
        .I5(\FSM_sequential_next_state_reg[1]_i_21_n_0 ),
        .O(\FSM_sequential_next_state_reg[1]_i_8_n_0 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \FSM_sequential_next_state_reg[1]_i_9 
       (.CI(\FSM_sequential_next_state_reg[1]_i_22_n_0 ),
        .CO({\FSM_sequential_next_state_reg[1]_i_9_n_0 ,\NLW_FSM_sequential_next_state_reg[1]_i_9_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\FSM_sequential_next_state_reg[1]_i_23_n_0 ,\FSM_sequential_next_state_reg[1]_i_24_n_0 ,\FSM_sequential_next_state_reg[1]_i_25_n_0 ,\FSM_sequential_next_state_reg[1]_i_26_n_0 }),
        .O(\NLW_FSM_sequential_next_state_reg[1]_i_9_O_UNCONNECTED [3:0]),
        .S({\FSM_sequential_next_state_reg[1]_i_27_n_0 ,\FSM_sequential_next_state_reg[1]_i_28_n_0 ,\FSM_sequential_next_state_reg[1]_i_29_n_0 ,\FSM_sequential_next_state_reg[1]_i_30_n_0 }));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \aux_reg[0] 
       (.CLR(1'b0),
        .D(\FSM_sequential_next_state_reg[0]_i_1_n_0 ),
        .G(\aux_reg[0]_i_1_n_0 ),
        .GE(1'b1),
        .Q(aux));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'hF1)) 
    \aux_reg[0]_i_1 
       (.I0(\FSM_sequential_current_state_reg_n_0_[1] ),
        .I1(current_state),
        .I2(button_ok_IBUF),
        .O(\aux_reg[0]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    bdf_reg
       (.CLR(1'b0),
        .D(\FSM_sequential_current_state_reg_n_0_[1] ),
        .G(bdf_reg_i_1_n_0),
        .GE(1'b1),
        .Q(bdf_dinero));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT2 #(
    .INIT(4'h7)) 
    bdf_reg_i_1
       (.I0(current_state),
        .I1(\FSM_sequential_current_state_reg_n_0_[1] ),
        .O(bdf_reg_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \caracter[4]_i_15 
       (.I0(Q[1]),
        .I1(\total_reg[15]_0 ),
        .I2(Q[0]),
        .O(\current_state_reg[1] ));
  LUT6 #(
    .INIT(64'h0000000040040404)) 
    \caracter[4]_i_3 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(\caracter_reg[4] [2]),
        .I3(\caracter_reg[4] [0]),
        .I4(\caracter_reg[4] [1]),
        .I5(\total_reg[15]_0 ),
        .O(\current_state_reg[0]_0 ));
  LUT6 #(
    .INIT(64'h0000001000000000)) 
    \caracter[6]_i_8 
       (.I0(\total_reg[15]_0 ),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(\caracter_reg[4] [2]),
        .I4(\caracter_reg[4] [0]),
        .I5(\caracter_reg[4] [1]),
        .O(\current_state_reg[0] ));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT2 #(
    .INIT(4'h8)) 
    devolver_OBUF_inst_i_1
       (.I0(\FSM_sequential_current_state_reg_n_0_[1] ),
        .I1(current_state),
        .O(devolver_OBUF));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT2 #(
    .INIT(4'hB)) 
    dot_i_5
       (.I0(\total_reg[15]_0 ),
        .I1(Q[1]),
        .O(\current_state_reg[1]_2 ));
  int_to_string inst_int_to_string
       (.Q({\total_reg_n_0_[30] ,\total_reg_n_0_[29] ,\total_reg_n_0_[28] ,\total_reg_n_0_[27] ,\total_reg_n_0_[26] ,\total_reg_n_0_[25] ,\total_reg_n_0_[24] ,\total_reg_n_0_[23] ,\total_reg_n_0_[22] ,\total_reg_n_0_[21] ,\total_reg_n_0_[20] ,\total_reg_n_0_[19] ,\total_reg_n_0_[18] ,\total_reg_n_0_[17] ,\total_reg_n_0_[16] ,\total_reg_n_0_[15] ,\total_reg_n_0_[14] ,\total_reg_n_0_[13] ,\total_reg_n_0_[12] ,\total_reg_n_0_[11] ,\total_reg_n_0_[10] ,\total_reg_n_0_[9] ,\total_reg_n_0_[8] ,\total_reg_n_0_[7] ,\total_reg_n_0_[6] ,\total_reg_n_0_[5] ,\total_reg_n_0_[4] ,\total_reg_n_0_[3] ,\total_reg_n_0_[2] ,\total_reg_n_0_[1] ,\total_reg_n_0_[0] }),
        .\caracter[1]_i_6 (\caracter[1]_i_6 ),
        .\caracter[1]_i_6_0 (Q[1]),
        .\caracter_reg[1] (\caracter_reg[1] ),
        .\caracter_reg[1]_0 (\caracter_reg[1]_0 ),
        .\caracter_reg[2] (\caracter_reg[2] ),
        .\caracter_reg[2]_0 (\caracter_reg[2]_0 ),
        .\current_state_reg[1] (\current_state_reg[1]_0 ),
        .\current_state_reg[1]_0 (\current_state_reg[1]_1 ),
        .\disp_dinero[3]_1 (\disp_dinero[3]_1 ),
        .\disp_dinero[5]_3 (\disp_dinero[5]_3 ),
        .\reg_reg[1] (\reg_reg[1] ),
        .\reg_reg[1]_0 (\reg_reg[1]_0 ),
        .\string_cent_decenas[1]5__186_carry_0 (\string_cent_decenas[1]5__186_carry ),
        .\total_reg[0] (DI),
        .\total_reg[15] (\total_reg[15]_0 ),
        .\total_reg[20] (inst_int_to_string_n_9),
        .\total_reg[26] (inst_int_to_string_n_12),
        .\total_reg[29] (inst_int_to_string_n_11));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFEA40)) 
    \next_state_reg[1]_i_1 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(\total_reg[7]_i_2_0 [0]),
        .I3(bdf_dinero),
        .I4(\current_state_reg[0]_1 ),
        .I5(\current_state_reg[0]_2 ),
        .O(E));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT2 #(
    .INIT(4'h6)) 
    refresco_OBUF_inst_i_1
       (.I0(\FSM_sequential_current_state_reg_n_0_[1] ),
        .I1(current_state),
        .O(refresco_OBUF));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \total[0]_i_1 
       (.I0(aux),
        .I1(\total_reg[3]_i_2_n_7 ),
        .O(\total[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \total[10]_i_1 
       (.I0(aux),
        .I1(\total_reg[11]_i_2_n_5 ),
        .O(\total[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \total[11]_i_1 
       (.I0(aux),
        .I1(\total_reg[11]_i_2_n_4 ),
        .O(\total[11]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \total[12]_i_1 
       (.I0(aux),
        .I1(\total_reg[15]_i_2_n_7 ),
        .O(\total[12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \total[13]_i_1 
       (.I0(aux),
        .I1(\total_reg[15]_i_2_n_6 ),
        .O(\total[13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \total[14]_i_1 
       (.I0(aux),
        .I1(\total_reg[15]_i_2_n_5 ),
        .O(\total[14]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \total[15]_i_1 
       (.I0(aux),
        .I1(\total_reg[15]_i_2_n_4 ),
        .O(\total[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \total[16]_i_1 
       (.I0(aux),
        .I1(\total_reg[19]_i_2_n_7 ),
        .O(\total[16]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \total[17]_i_1 
       (.I0(aux),
        .I1(\total_reg[19]_i_2_n_6 ),
        .O(\total[17]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \total[18]_i_1 
       (.I0(aux),
        .I1(\total_reg[19]_i_2_n_5 ),
        .O(\total[18]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \total[19]_i_1 
       (.I0(aux),
        .I1(\total_reg[19]_i_2_n_4 ),
        .O(\total[19]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \total[1]_i_1 
       (.I0(aux),
        .I1(\total_reg[3]_i_2_n_6 ),
        .O(\total[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \total[20]_i_1 
       (.I0(aux),
        .I1(\total_reg[23]_i_2_n_7 ),
        .O(\total[20]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \total[21]_i_1 
       (.I0(aux),
        .I1(\total_reg[23]_i_2_n_6 ),
        .O(\total[21]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \total[22]_i_1 
       (.I0(aux),
        .I1(\total_reg[23]_i_2_n_5 ),
        .O(\total[22]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \total[23]_i_1 
       (.I0(aux),
        .I1(\total_reg[23]_i_2_n_4 ),
        .O(\total[23]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \total[24]_i_1 
       (.I0(aux),
        .I1(\total_reg[27]_i_2_n_7 ),
        .O(\total[24]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \total[25]_i_1 
       (.I0(aux),
        .I1(\total_reg[27]_i_2_n_6 ),
        .O(\total[25]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \total[26]_i_1 
       (.I0(aux),
        .I1(\total_reg[27]_i_2_n_5 ),
        .O(\total[26]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \total[27]_i_1 
       (.I0(aux),
        .I1(\total_reg[27]_i_2_n_4 ),
        .O(\total[27]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \total[28]_i_1 
       (.I0(aux),
        .I1(\total_reg[31]_i_3_n_7 ),
        .O(\total[28]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \total[29]_i_1 
       (.I0(aux),
        .I1(\total_reg[31]_i_3_n_6 ),
        .O(\total[29]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \total[2]_i_1 
       (.I0(aux),
        .I1(\total_reg[3]_i_2_n_5 ),
        .O(\total[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \total[30]_i_1 
       (.I0(aux),
        .I1(\total_reg[31]_i_3_n_5 ),
        .O(\total[30]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h1F)) 
    \total[31]_i_1 
       (.I0(current_state),
        .I1(\FSM_sequential_current_state_reg_n_0_[1] ),
        .I2(aux),
        .O(total));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \total[31]_i_2 
       (.I0(aux),
        .I1(\total_reg[31]_i_3_n_4 ),
        .O(\total[31]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \total[3]_i_1 
       (.I0(aux),
        .I1(\total_reg[3]_i_2_n_4 ),
        .O(\total[3]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \total[3]_i_3 
       (.I0(\total_reg_n_0_[3] ),
        .I1(\total_reg[7]_i_2_0 [3]),
        .O(\total[3]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \total[3]_i_4 
       (.I0(\total_reg_n_0_[2] ),
        .I1(\total_reg[7]_i_2_0 [2]),
        .O(\total[3]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \total[3]_i_5 
       (.I0(\total_reg_n_0_[1] ),
        .I1(\total_reg[7]_i_2_0 [1]),
        .O(\total[3]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \total[3]_i_6 
       (.I0(\total_reg_n_0_[0] ),
        .I1(\total_reg[7]_i_2_0 [0]),
        .O(\total[3]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \total[4]_i_1 
       (.I0(aux),
        .I1(\total_reg[7]_i_2_n_7 ),
        .O(\total[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \total[5]_i_1 
       (.I0(aux),
        .I1(\total_reg[7]_i_2_n_6 ),
        .O(\total[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \total[6]_i_1 
       (.I0(aux),
        .I1(\total_reg[7]_i_2_n_5 ),
        .O(\total[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \total[7]_i_1 
       (.I0(aux),
        .I1(\total_reg[7]_i_2_n_4 ),
        .O(\total[7]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \total[7]_i_3 
       (.I0(\total_reg_n_0_[6] ),
        .I1(\total_reg[7]_i_2_0 [6]),
        .O(\total[7]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \total[7]_i_4 
       (.I0(\total_reg_n_0_[5] ),
        .I1(\total_reg[7]_i_2_0 [5]),
        .O(\total[7]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \total[7]_i_5 
       (.I0(\total_reg_n_0_[4] ),
        .I1(\total_reg[7]_i_2_0 [4]),
        .O(\total[7]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \total[8]_i_1 
       (.I0(aux),
        .I1(\total_reg[11]_i_2_n_7 ),
        .O(\total[8]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \total[9]_i_1 
       (.I0(aux),
        .I1(\total_reg[11]_i_2_n_6 ),
        .O(\total[9]_i_1_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \total_reg[0] 
       (.C(clk_BUFG),
        .CE(total),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(\total[0]_i_1_n_0 ),
        .Q(\total_reg_n_0_[0] ));
  FDCE #(
    .INIT(1'b0)) 
    \total_reg[10] 
       (.C(clk_BUFG),
        .CE(total),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(\total[10]_i_1_n_0 ),
        .Q(\total_reg_n_0_[10] ));
  FDCE #(
    .INIT(1'b0)) 
    \total_reg[11] 
       (.C(clk_BUFG),
        .CE(total),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(\total[11]_i_1_n_0 ),
        .Q(\total_reg_n_0_[11] ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \total_reg[11]_i_2 
       (.CI(\total_reg[7]_i_2_n_0 ),
        .CO({\total_reg[11]_i_2_n_0 ,\NLW_total_reg[11]_i_2_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\total_reg_n_0_[11] ,\total_reg_n_0_[10] ,\total_reg_n_0_[9] ,\total_reg_n_0_[8] }),
        .O({\total_reg[11]_i_2_n_4 ,\total_reg[11]_i_2_n_5 ,\total_reg[11]_i_2_n_6 ,\total_reg[11]_i_2_n_7 }),
        .S({\total_reg_n_0_[11] ,\total_reg_n_0_[10] ,\total_reg_n_0_[9] ,\total_reg_n_0_[8] }));
  FDCE #(
    .INIT(1'b0)) 
    \total_reg[12] 
       (.C(clk_BUFG),
        .CE(total),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(\total[12]_i_1_n_0 ),
        .Q(\total_reg_n_0_[12] ));
  FDCE #(
    .INIT(1'b0)) 
    \total_reg[13] 
       (.C(clk_BUFG),
        .CE(total),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(\total[13]_i_1_n_0 ),
        .Q(\total_reg_n_0_[13] ));
  FDCE #(
    .INIT(1'b0)) 
    \total_reg[14] 
       (.C(clk_BUFG),
        .CE(total),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(\total[14]_i_1_n_0 ),
        .Q(\total_reg_n_0_[14] ));
  FDCE #(
    .INIT(1'b0)) 
    \total_reg[15] 
       (.C(clk_BUFG),
        .CE(total),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(\total[15]_i_1_n_0 ),
        .Q(\total_reg_n_0_[15] ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \total_reg[15]_i_2 
       (.CI(\total_reg[11]_i_2_n_0 ),
        .CO({\total_reg[15]_i_2_n_0 ,\NLW_total_reg[15]_i_2_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\total_reg_n_0_[15] ,\total_reg_n_0_[14] ,\total_reg_n_0_[13] ,\total_reg_n_0_[12] }),
        .O({\total_reg[15]_i_2_n_4 ,\total_reg[15]_i_2_n_5 ,\total_reg[15]_i_2_n_6 ,\total_reg[15]_i_2_n_7 }),
        .S({\total_reg_n_0_[15] ,\total_reg_n_0_[14] ,\total_reg_n_0_[13] ,\total_reg_n_0_[12] }));
  FDCE #(
    .INIT(1'b0)) 
    \total_reg[16] 
       (.C(clk_BUFG),
        .CE(total),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(\total[16]_i_1_n_0 ),
        .Q(\total_reg_n_0_[16] ));
  FDCE #(
    .INIT(1'b0)) 
    \total_reg[17] 
       (.C(clk_BUFG),
        .CE(total),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(\total[17]_i_1_n_0 ),
        .Q(\total_reg_n_0_[17] ));
  FDCE #(
    .INIT(1'b0)) 
    \total_reg[18] 
       (.C(clk_BUFG),
        .CE(total),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(\total[18]_i_1_n_0 ),
        .Q(\total_reg_n_0_[18] ));
  FDCE #(
    .INIT(1'b0)) 
    \total_reg[19] 
       (.C(clk_BUFG),
        .CE(total),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(\total[19]_i_1_n_0 ),
        .Q(\total_reg_n_0_[19] ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \total_reg[19]_i_2 
       (.CI(\total_reg[15]_i_2_n_0 ),
        .CO({\total_reg[19]_i_2_n_0 ,\NLW_total_reg[19]_i_2_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\total_reg_n_0_[19] ,\total_reg_n_0_[18] ,\total_reg_n_0_[17] ,\total_reg_n_0_[16] }),
        .O({\total_reg[19]_i_2_n_4 ,\total_reg[19]_i_2_n_5 ,\total_reg[19]_i_2_n_6 ,\total_reg[19]_i_2_n_7 }),
        .S({\total_reg_n_0_[19] ,\total_reg_n_0_[18] ,\total_reg_n_0_[17] ,\total_reg_n_0_[16] }));
  FDCE #(
    .INIT(1'b0)) 
    \total_reg[1] 
       (.C(clk_BUFG),
        .CE(total),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(\total[1]_i_1_n_0 ),
        .Q(\total_reg_n_0_[1] ));
  FDCE #(
    .INIT(1'b0)) 
    \total_reg[20] 
       (.C(clk_BUFG),
        .CE(total),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(\total[20]_i_1_n_0 ),
        .Q(\total_reg_n_0_[20] ));
  FDCE #(
    .INIT(1'b0)) 
    \total_reg[21] 
       (.C(clk_BUFG),
        .CE(total),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(\total[21]_i_1_n_0 ),
        .Q(\total_reg_n_0_[21] ));
  FDCE #(
    .INIT(1'b0)) 
    \total_reg[22] 
       (.C(clk_BUFG),
        .CE(total),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(\total[22]_i_1_n_0 ),
        .Q(\total_reg_n_0_[22] ));
  FDCE #(
    .INIT(1'b0)) 
    \total_reg[23] 
       (.C(clk_BUFG),
        .CE(total),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(\total[23]_i_1_n_0 ),
        .Q(\total_reg_n_0_[23] ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \total_reg[23]_i_2 
       (.CI(\total_reg[19]_i_2_n_0 ),
        .CO({\total_reg[23]_i_2_n_0 ,\NLW_total_reg[23]_i_2_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\total_reg_n_0_[23] ,\total_reg_n_0_[22] ,\total_reg_n_0_[21] ,\total_reg_n_0_[20] }),
        .O({\total_reg[23]_i_2_n_4 ,\total_reg[23]_i_2_n_5 ,\total_reg[23]_i_2_n_6 ,\total_reg[23]_i_2_n_7 }),
        .S({\total_reg_n_0_[23] ,\total_reg_n_0_[22] ,\total_reg_n_0_[21] ,\total_reg_n_0_[20] }));
  FDCE #(
    .INIT(1'b0)) 
    \total_reg[24] 
       (.C(clk_BUFG),
        .CE(total),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(\total[24]_i_1_n_0 ),
        .Q(\total_reg_n_0_[24] ));
  FDCE #(
    .INIT(1'b0)) 
    \total_reg[25] 
       (.C(clk_BUFG),
        .CE(total),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(\total[25]_i_1_n_0 ),
        .Q(\total_reg_n_0_[25] ));
  FDCE #(
    .INIT(1'b0)) 
    \total_reg[26] 
       (.C(clk_BUFG),
        .CE(total),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(\total[26]_i_1_n_0 ),
        .Q(\total_reg_n_0_[26] ));
  FDCE #(
    .INIT(1'b0)) 
    \total_reg[27] 
       (.C(clk_BUFG),
        .CE(total),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(\total[27]_i_1_n_0 ),
        .Q(\total_reg_n_0_[27] ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \total_reg[27]_i_2 
       (.CI(\total_reg[23]_i_2_n_0 ),
        .CO({\total_reg[27]_i_2_n_0 ,\NLW_total_reg[27]_i_2_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\total_reg_n_0_[27] ,\total_reg_n_0_[26] ,\total_reg_n_0_[25] ,\total_reg_n_0_[24] }),
        .O({\total_reg[27]_i_2_n_4 ,\total_reg[27]_i_2_n_5 ,\total_reg[27]_i_2_n_6 ,\total_reg[27]_i_2_n_7 }),
        .S({\total_reg_n_0_[27] ,\total_reg_n_0_[26] ,\total_reg_n_0_[25] ,\total_reg_n_0_[24] }));
  FDCE #(
    .INIT(1'b0)) 
    \total_reg[28] 
       (.C(clk_BUFG),
        .CE(total),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(\total[28]_i_1_n_0 ),
        .Q(\total_reg_n_0_[28] ));
  FDCE #(
    .INIT(1'b0)) 
    \total_reg[29] 
       (.C(clk_BUFG),
        .CE(total),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(\total[29]_i_1_n_0 ),
        .Q(\total_reg_n_0_[29] ));
  FDCE #(
    .INIT(1'b0)) 
    \total_reg[2] 
       (.C(clk_BUFG),
        .CE(total),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(\total[2]_i_1_n_0 ),
        .Q(\total_reg_n_0_[2] ));
  FDCE #(
    .INIT(1'b0)) 
    \total_reg[30] 
       (.C(clk_BUFG),
        .CE(total),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(\total[30]_i_1_n_0 ),
        .Q(\total_reg_n_0_[30] ));
  FDCE #(
    .INIT(1'b0)) 
    \total_reg[31] 
       (.C(clk_BUFG),
        .CE(total),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(\total[31]_i_2_n_0 ),
        .Q(\total_reg_n_0_[31] ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \total_reg[31]_i_3 
       (.CI(\total_reg[27]_i_2_n_0 ),
        .CO(\NLW_total_reg[31]_i_3_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,\total_reg_n_0_[30] ,\total_reg_n_0_[29] ,\total_reg_n_0_[28] }),
        .O({\total_reg[31]_i_3_n_4 ,\total_reg[31]_i_3_n_5 ,\total_reg[31]_i_3_n_6 ,\total_reg[31]_i_3_n_7 }),
        .S({\total_reg_n_0_[31] ,\total_reg_n_0_[30] ,\total_reg_n_0_[29] ,\total_reg_n_0_[28] }));
  FDCE #(
    .INIT(1'b0)) 
    \total_reg[3] 
       (.C(clk_BUFG),
        .CE(total),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(\total[3]_i_1_n_0 ),
        .Q(\total_reg_n_0_[3] ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \total_reg[3]_i_2 
       (.CI(1'b0),
        .CO({\total_reg[3]_i_2_n_0 ,\NLW_total_reg[3]_i_2_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\total_reg_n_0_[3] ,\total_reg_n_0_[2] ,\total_reg_n_0_[1] ,\total_reg_n_0_[0] }),
        .O({\total_reg[3]_i_2_n_4 ,\total_reg[3]_i_2_n_5 ,\total_reg[3]_i_2_n_6 ,\total_reg[3]_i_2_n_7 }),
        .S({\total[3]_i_3_n_0 ,\total[3]_i_4_n_0 ,\total[3]_i_5_n_0 ,\total[3]_i_6_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \total_reg[4] 
       (.C(clk_BUFG),
        .CE(total),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(\total[4]_i_1_n_0 ),
        .Q(\total_reg_n_0_[4] ));
  FDCE #(
    .INIT(1'b0)) 
    \total_reg[5] 
       (.C(clk_BUFG),
        .CE(total),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(\total[5]_i_1_n_0 ),
        .Q(\total_reg_n_0_[5] ));
  FDCE #(
    .INIT(1'b0)) 
    \total_reg[6] 
       (.C(clk_BUFG),
        .CE(total),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(\total[6]_i_1_n_0 ),
        .Q(\total_reg_n_0_[6] ));
  FDCE #(
    .INIT(1'b0)) 
    \total_reg[7] 
       (.C(clk_BUFG),
        .CE(total),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(\total[7]_i_1_n_0 ),
        .Q(\total_reg_n_0_[7] ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \total_reg[7]_i_2 
       (.CI(\total_reg[3]_i_2_n_0 ),
        .CO({\total_reg[7]_i_2_n_0 ,\NLW_total_reg[7]_i_2_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\total_reg_n_0_[7] ,\total_reg_n_0_[6] ,\total_reg_n_0_[5] ,\total_reg_n_0_[4] }),
        .O({\total_reg[7]_i_2_n_4 ,\total_reg[7]_i_2_n_5 ,\total_reg[7]_i_2_n_6 ,\total_reg[7]_i_2_n_7 }),
        .S({\total_reg_n_0_[7] ,\total[7]_i_3_n_0 ,\total[7]_i_4_n_0 ,\total[7]_i_5_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \total_reg[8] 
       (.C(clk_BUFG),
        .CE(total),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(\total[8]_i_1_n_0 ),
        .Q(\total_reg_n_0_[8] ));
  FDCE #(
    .INIT(1'b0)) 
    \total_reg[9] 
       (.C(clk_BUFG),
        .CE(total),
        .CLR(\FSM_sequential_current_state[1]_i_1_n_0 ),
        .D(\total[9]_i_1_n_0 ),
        .Q(\total_reg_n_0_[9] ));
endmodule

module gestion_refresco
   (D,
    \FSM_onehot_current_refresco_reg[0]_0 ,
    Q,
    \reg_reg[0] ,
    \FSM_onehot_current_refresco_reg[2]_0 ,
    \FSM_onehot_current_refresco_reg[4]_0 ,
    \FSM_onehot_current_refresco_reg[2]_1 ,
    \FSM_onehot_current_refresco_reg[1]_0 ,
    \current_state_reg[0] ,
    \FSM_onehot_current_refresco_reg[4]_1 ,
    \current_state_reg[0]_0 ,
    \FSM_onehot_current_refresco_reg[1]_1 ,
    \FSM_onehot_current_refresco_reg[0]_1 ,
    \v_reg[6] ,
    \FSM_onehot_current_refresco_reg[3]_0 ,
    \FSM_onehot_current_refresco_reg[1]_2 ,
    \FSM_onehot_current_refresco_reg[3]_1 ,
    \FSM_onehot_current_refresco_reg[0]_2 ,
    \FSM_onehot_current_refresco_reg[3]_2 ,
    \FSM_onehot_current_refresco_reg[3]_3 ,
    \current_state_reg[1] ,
    E,
    \caracter_reg[1] ,
    \caracter_reg[1]_0 ,
    \caracter_reg[1]_1 ,
    \caracter_reg[1]_2 ,
    \caracter_reg[2] ,
    \caracter_reg[2]_0 ,
    \FSM_onehot_current_refresco_reg[2]_2 ,
    \disp_dinero[1]_0 ,
    \caracter_reg[6] ,
    \caracter_reg[6]_0 ,
    \caracter_reg[0] ,
    \caracter_reg[1]_3 ,
    \caracter_reg[1]_4 ,
    \caracter_reg[1]_5 ,
    \caracter[0]_i_4_0 ,
    \caracter_reg[4] ,
    plusOp,
    \caracter_reg[1]_6 ,
    \next_state_reg[1]_i_1 ,
    \next_state_reg[1]_i_1_0 ,
    button_ok_IBUF,
    \caracter_reg[5] ,
    CO,
    clr_refresco_r,
    clk_BUFG,
    ref_option_IBUF);
  output [1:0]D;
  output \FSM_onehot_current_refresco_reg[0]_0 ;
  output [2:0]Q;
  output \reg_reg[0] ;
  output \FSM_onehot_current_refresco_reg[2]_0 ;
  output \FSM_onehot_current_refresco_reg[4]_0 ;
  output \FSM_onehot_current_refresco_reg[2]_1 ;
  output \FSM_onehot_current_refresco_reg[1]_0 ;
  output \current_state_reg[0] ;
  output \FSM_onehot_current_refresco_reg[4]_1 ;
  output \current_state_reg[0]_0 ;
  output \FSM_onehot_current_refresco_reg[1]_1 ;
  output \FSM_onehot_current_refresco_reg[0]_1 ;
  output \v_reg[6] ;
  output \FSM_onehot_current_refresco_reg[3]_0 ;
  output \FSM_onehot_current_refresco_reg[1]_2 ;
  output \FSM_onehot_current_refresco_reg[3]_1 ;
  output \FSM_onehot_current_refresco_reg[0]_2 ;
  output \FSM_onehot_current_refresco_reg[3]_2 ;
  output \FSM_onehot_current_refresco_reg[3]_3 ;
  output \current_state_reg[1] ;
  output [0:0]E;
  input \caracter_reg[1] ;
  input \caracter_reg[1]_0 ;
  input \caracter_reg[1]_1 ;
  input \caracter_reg[1]_2 ;
  input \caracter_reg[2] ;
  input \caracter_reg[2]_0 ;
  input [1:0]\FSM_onehot_current_refresco_reg[2]_2 ;
  input [0:0]\disp_dinero[1]_0 ;
  input [2:0]\caracter_reg[6] ;
  input \caracter_reg[6]_0 ;
  input \caracter_reg[0] ;
  input \caracter_reg[1]_3 ;
  input \caracter_reg[1]_4 ;
  input \caracter_reg[1]_5 ;
  input \caracter[0]_i_4_0 ;
  input \caracter_reg[4] ;
  input [0:0]plusOp;
  input \caracter_reg[1]_6 ;
  input \next_state_reg[1]_i_1 ;
  input [0:0]\next_state_reg[1]_i_1_0 ;
  input button_ok_IBUF;
  input \caracter_reg[5] ;
  input [0:0]CO;
  input clr_refresco_r;
  input clk_BUFG;
  input [3:0]ref_option_IBUF;

  wire [0:0]CO;
  wire [1:0]D;
  wire [0:0]E;
  wire \FSM_onehot_current_refresco[0]_i_1_n_0 ;
  wire \FSM_onehot_current_refresco[1]_i_1_n_0 ;
  wire \FSM_onehot_current_refresco[2]_i_1_n_0 ;
  wire \FSM_onehot_current_refresco[3]_i_1_n_0 ;
  wire \FSM_onehot_current_refresco[4]_i_1_n_0 ;
  wire \FSM_onehot_current_refresco[4]_i_2_n_0 ;
  wire \FSM_onehot_current_refresco_reg[0]_0 ;
  wire \FSM_onehot_current_refresco_reg[0]_1 ;
  wire \FSM_onehot_current_refresco_reg[0]_2 ;
  wire \FSM_onehot_current_refresco_reg[1]_0 ;
  wire \FSM_onehot_current_refresco_reg[1]_1 ;
  wire \FSM_onehot_current_refresco_reg[1]_2 ;
  wire \FSM_onehot_current_refresco_reg[2]_0 ;
  wire \FSM_onehot_current_refresco_reg[2]_1 ;
  wire [1:0]\FSM_onehot_current_refresco_reg[2]_2 ;
  wire \FSM_onehot_current_refresco_reg[3]_0 ;
  wire \FSM_onehot_current_refresco_reg[3]_1 ;
  wire \FSM_onehot_current_refresco_reg[3]_2 ;
  wire \FSM_onehot_current_refresco_reg[3]_3 ;
  wire \FSM_onehot_current_refresco_reg[4]_0 ;
  wire \FSM_onehot_current_refresco_reg[4]_1 ;
  wire \FSM_onehot_current_refresco_reg_n_0_[2] ;
  wire \FSM_onehot_current_refresco_reg_n_0_[4] ;
  wire [2:0]Q;
  wire button_ok_IBUF;
  wire \caracter[0]_i_4_0 ;
  wire \caracter[0]_i_8_n_0 ;
  wire \caracter[1]_i_2_n_0 ;
  wire \caracter[1]_i_3_n_0 ;
  wire \caracter[6]_i_7_n_0 ;
  wire \caracter_reg[0] ;
  wire \caracter_reg[1] ;
  wire \caracter_reg[1]_0 ;
  wire \caracter_reg[1]_1 ;
  wire \caracter_reg[1]_2 ;
  wire \caracter_reg[1]_3 ;
  wire \caracter_reg[1]_4 ;
  wire \caracter_reg[1]_5 ;
  wire \caracter_reg[1]_6 ;
  wire \caracter_reg[2] ;
  wire \caracter_reg[2]_0 ;
  wire \caracter_reg[4] ;
  wire \caracter_reg[5] ;
  wire [2:0]\caracter_reg[6] ;
  wire \caracter_reg[6]_0 ;
  wire clk_BUFG;
  wire clr_refresco_r;
  wire \current_state_reg[0] ;
  wire \current_state_reg[0]_0 ;
  wire \current_state_reg[1] ;
  wire [0:0]\disp_dinero[1]_0 ;
  wire next_refresco_n_0;
  wire \next_state_reg[1]_i_1 ;
  wire [0:0]\next_state_reg[1]_i_1_0 ;
  wire [0:0]plusOp;
  wire [3:0]ref_option_IBUF;
  wire \reg_reg[0] ;
  wire \v_reg[6] ;

  LUT4 #(
    .INIT(16'hFEE8)) 
    \FSM_onehot_current_refresco[0]_i_1 
       (.I0(ref_option_IBUF[2]),
        .I1(ref_option_IBUF[3]),
        .I2(ref_option_IBUF[1]),
        .I3(ref_option_IBUF[0]),
        .O(\FSM_onehot_current_refresco[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT3 #(
    .INIT(8'h01)) 
    \FSM_onehot_current_refresco[1]_i_1 
       (.I0(ref_option_IBUF[2]),
        .I1(ref_option_IBUF[1]),
        .I2(ref_option_IBUF[3]),
        .O(\FSM_onehot_current_refresco[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair78" *) 
  LUT3 #(
    .INIT(8'h01)) 
    \FSM_onehot_current_refresco[2]_i_1 
       (.I0(ref_option_IBUF[2]),
        .I1(ref_option_IBUF[0]),
        .I2(ref_option_IBUF[3]),
        .O(\FSM_onehot_current_refresco[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT4 #(
    .INIT(16'h0010)) 
    \FSM_onehot_current_refresco[3]_i_1 
       (.I0(ref_option_IBUF[3]),
        .I1(ref_option_IBUF[0]),
        .I2(ref_option_IBUF[2]),
        .I3(ref_option_IBUF[1]),
        .O(\FSM_onehot_current_refresco[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT4 #(
    .INIT(16'h0010)) 
    \FSM_onehot_current_refresco[4]_i_1 
       (.I0(ref_option_IBUF[0]),
        .I1(ref_option_IBUF[2]),
        .I2(ref_option_IBUF[3]),
        .I3(ref_option_IBUF[1]),
        .O(\FSM_onehot_current_refresco[4]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h6F)) 
    \FSM_onehot_current_refresco[4]_i_2 
       (.I0(\FSM_onehot_current_refresco_reg[2]_2 [0]),
        .I1(\FSM_onehot_current_refresco_reg[2]_2 [1]),
        .I2(clr_refresco_r),
        .O(\FSM_onehot_current_refresco[4]_i_2_n_0 ));
  (* FSM_ENCODED_STATES = "inicio:00001,agua:00010,fanta_n:00100,nestea:01000,cocacola:10000" *) 
  FDPE #(
    .INIT(1'b1)) 
    \FSM_onehot_current_refresco_reg[0] 
       (.C(clk_BUFG),
        .CE(next_refresco_n_0),
        .D(\FSM_onehot_current_refresco[0]_i_1_n_0 ),
        .PRE(\FSM_onehot_current_refresco[4]_i_2_n_0 ),
        .Q(Q[0]));
  (* FSM_ENCODED_STATES = "inicio:00001,agua:00010,fanta_n:00100,nestea:01000,cocacola:10000" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_refresco_reg[1] 
       (.C(clk_BUFG),
        .CE(next_refresco_n_0),
        .CLR(\FSM_onehot_current_refresco[4]_i_2_n_0 ),
        .D(\FSM_onehot_current_refresco[1]_i_1_n_0 ),
        .Q(Q[1]));
  (* FSM_ENCODED_STATES = "inicio:00001,agua:00010,fanta_n:00100,nestea:01000,cocacola:10000" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_refresco_reg[2] 
       (.C(clk_BUFG),
        .CE(next_refresco_n_0),
        .CLR(\FSM_onehot_current_refresco[4]_i_2_n_0 ),
        .D(\FSM_onehot_current_refresco[2]_i_1_n_0 ),
        .Q(\FSM_onehot_current_refresco_reg_n_0_[2] ));
  (* FSM_ENCODED_STATES = "inicio:00001,agua:00010,fanta_n:00100,nestea:01000,cocacola:10000" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_refresco_reg[3] 
       (.C(clk_BUFG),
        .CE(next_refresco_n_0),
        .CLR(\FSM_onehot_current_refresco[4]_i_2_n_0 ),
        .D(\FSM_onehot_current_refresco[3]_i_1_n_0 ),
        .Q(Q[2]));
  (* FSM_ENCODED_STATES = "inicio:00001,agua:00010,fanta_n:00100,nestea:01000,cocacola:10000" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_current_refresco_reg[4] 
       (.C(clk_BUFG),
        .CE(next_refresco_n_0),
        .CLR(\FSM_onehot_current_refresco[4]_i_2_n_0 ),
        .D(\FSM_onehot_current_refresco[4]_i_1_n_0 ),
        .Q(\FSM_onehot_current_refresco_reg_n_0_[4] ));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT5 #(
    .INIT(32'hAAAAAABA)) 
    \caracter[0]_i_4 
       (.I0(\caracter[0]_i_8_n_0 ),
        .I1(\caracter_reg[0] ),
        .I2(\FSM_onehot_current_refresco_reg_n_0_[4] ),
        .I3(\FSM_onehot_current_refresco_reg[2]_2 [1]),
        .I4(\FSM_onehot_current_refresco_reg[2]_2 [0]),
        .O(\FSM_onehot_current_refresco_reg[4]_0 ));
  LUT6 #(
    .INIT(64'h000A00080A000A00)) 
    \caracter[0]_i_8 
       (.I0(\caracter[0]_i_4_0 ),
        .I1(Q[0]),
        .I2(\FSM_onehot_current_refresco_reg[2]_2 [1]),
        .I3(\FSM_onehot_current_refresco_reg[2]_2 [0]),
        .I4(Q[1]),
        .I5(\caracter_reg[2] ),
        .O(\caracter[0]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \caracter[1]_i_1 
       (.I0(\caracter[1]_i_2_n_0 ),
        .I1(\caracter[1]_i_3_n_0 ),
        .I2(\caracter_reg[1] ),
        .I3(\caracter_reg[1]_0 ),
        .I4(\caracter_reg[1]_1 ),
        .I5(\caracter_reg[1]_2 ),
        .O(D[0]));
  LUT6 #(
    .INIT(64'hFFFFFFFFAAAAABAA)) 
    \caracter[1]_i_2 
       (.I0(\FSM_onehot_current_refresco_reg[2]_1 ),
        .I1(\FSM_onehot_current_refresco_reg[2]_2 [0]),
        .I2(\FSM_onehot_current_refresco_reg[2]_2 [1]),
        .I3(\FSM_onehot_current_refresco_reg_n_0_[4] ),
        .I4(\caracter_reg[1]_3 ),
        .I5(\caracter_reg[1]_4 ),
        .O(\caracter[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88888888000000F0)) 
    \caracter[1]_i_3 
       (.I0(\FSM_onehot_current_refresco_reg[4]_1 ),
        .I1(\caracter_reg[1]_6 ),
        .I2(\current_state_reg[0]_0 ),
        .I3(Q[2]),
        .I4(plusOp),
        .I5(\caracter_reg[1]_5 ),
        .O(\caracter[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0088008000888880)) 
    \caracter[2]_i_4 
       (.I0(\caracter_reg[2] ),
        .I1(\caracter_reg[2]_0 ),
        .I2(Q[0]),
        .I3(\FSM_onehot_current_refresco_reg[2]_2 [1]),
        .I4(\FSM_onehot_current_refresco_reg[2]_2 [0]),
        .I5(\disp_dinero[1]_0 ),
        .O(\FSM_onehot_current_refresco_reg[0]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT4 #(
    .INIT(16'h1000)) 
    \caracter[3]_i_12 
       (.I0(\FSM_onehot_current_refresco_reg[2]_2 [0]),
        .I1(\FSM_onehot_current_refresco_reg[2]_2 [1]),
        .I2(\FSM_onehot_current_refresco_reg_n_0_[2] ),
        .I3(\caracter_reg[6] [0]),
        .O(\current_state_reg[0]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \caracter[3]_i_13 
       (.I0(Q[2]),
        .I1(Q[1]),
        .I2(Q[0]),
        .O(\FSM_onehot_current_refresco_reg[3]_2 ));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT5 #(
    .INIT(32'hFF200020)) 
    \caracter[3]_i_9 
       (.I0(\caracter_reg[2]_0 ),
        .I1(\caracter_reg[1]_5 ),
        .I2(\FSM_onehot_current_refresco_reg_n_0_[2] ),
        .I3(\FSM_onehot_current_refresco_reg[2]_2 [1]),
        .I4(\FSM_onehot_current_refresco_reg[2]_2 [0]),
        .O(\FSM_onehot_current_refresco_reg[2]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \caracter[4]_i_14 
       (.I0(Q[2]),
        .I1(\FSM_onehot_current_refresco_reg[2]_2 [1]),
        .I2(\FSM_onehot_current_refresco_reg[2]_2 [0]),
        .O(\FSM_onehot_current_refresco_reg[3]_1 ));
  LUT6 #(
    .INIT(64'h0E000000000E0E0E)) 
    \caracter[4]_i_16 
       (.I0(Q[1]),
        .I1(\FSM_onehot_current_refresco_reg[2]_2 [0]),
        .I2(\FSM_onehot_current_refresco_reg[2]_2 [1]),
        .I3(\caracter_reg[6] [1]),
        .I4(\caracter_reg[6] [0]),
        .I5(\caracter_reg[6] [2]),
        .O(\FSM_onehot_current_refresco_reg[1]_1 ));
  LUT6 #(
    .INIT(64'h0110001001100000)) 
    \caracter[4]_i_4 
       (.I0(\FSM_onehot_current_refresco_reg[2]_2 [0]),
        .I1(\caracter_reg[4] ),
        .I2(plusOp),
        .I3(\caracter_reg[1]_6 ),
        .I4(Q[2]),
        .I5(Q[0]),
        .O(\current_state_reg[0] ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \caracter[5]_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(\FSM_onehot_current_refresco_reg_n_0_[4] ),
        .I4(\FSM_onehot_current_refresco_reg_n_0_[2] ),
        .I5(\caracter_reg[5] ),
        .O(D[1]));
  LUT2 #(
    .INIT(4'hB)) 
    \caracter[6]_i_1 
       (.I0(\reg_reg[0] ),
        .I1(CO),
        .O(E));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \caracter[6]_i_16 
       (.I0(Q[0]),
        .I1(\FSM_onehot_current_refresco_reg[2]_2 [1]),
        .I2(\FSM_onehot_current_refresco_reg[2]_2 [0]),
        .O(\FSM_onehot_current_refresco_reg[0]_1 ));
  LUT6 #(
    .INIT(64'h000020FF00002020)) 
    \caracter[6]_i_17 
       (.I0(\caracter_reg[0] ),
        .I1(\caracter_reg[1]_5 ),
        .I2(Q[1]),
        .I3(\FSM_onehot_current_refresco_reg[2]_2 [0]),
        .I4(\FSM_onehot_current_refresco_reg[2]_2 [1]),
        .I5(\FSM_onehot_current_refresco_reg_n_0_[4] ),
        .O(\FSM_onehot_current_refresco_reg[1]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \caracter[6]_i_20 
       (.I0(\FSM_onehot_current_refresco_reg[2]_2 [1]),
        .I1(\FSM_onehot_current_refresco_reg_n_0_[2] ),
        .O(\current_state_reg[1] ));
  (* SOFT_HLUTNM = "soft_lutpair77" *) 
  LUT3 #(
    .INIT(8'h32)) 
    \caracter[6]_i_22 
       (.I0(Q[2]),
        .I1(\FSM_onehot_current_refresco_reg[2]_2 [1]),
        .I2(\FSM_onehot_current_refresco_reg[2]_2 [0]),
        .O(\FSM_onehot_current_refresco_reg[3]_3 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF80000000)) 
    \caracter[6]_i_3 
       (.I0(\FSM_onehot_current_refresco_reg[2]_0 ),
        .I1(\caracter[6]_i_7_n_0 ),
        .I2(\caracter_reg[6] [0]),
        .I3(\caracter_reg[6] [1]),
        .I4(\caracter_reg[6] [2]),
        .I5(\caracter_reg[6]_0 ),
        .O(\reg_reg[0] ));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \caracter[6]_i_6 
       (.I0(\FSM_onehot_current_refresco_reg_n_0_[2] ),
        .I1(\FSM_onehot_current_refresco_reg[2]_2 [1]),
        .I2(\FSM_onehot_current_refresco_reg[2]_2 [0]),
        .O(\FSM_onehot_current_refresco_reg[2]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \caracter[6]_i_7 
       (.I0(\FSM_onehot_current_refresco_reg_n_0_[4] ),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(Q[0]),
        .O(\caracter[6]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    dot_i_4
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(\FSM_onehot_current_refresco_reg_n_0_[4] ),
        .O(\FSM_onehot_current_refresco_reg[0]_2 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    next_refresco
       (.I0(ref_option_IBUF[3]),
        .I1(ref_option_IBUF[2]),
        .I2(ref_option_IBUF[0]),
        .I3(ref_option_IBUF[1]),
        .O(next_refresco_n_0));
  LUT6 #(
    .INIT(64'hFFFFC0C0FFEAC0C0)) 
    \next_state_reg[1]_i_2 
       (.I0(\FSM_onehot_current_refresco_reg[3]_0 ),
        .I1(\next_state_reg[1]_i_1 ),
        .I2(\next_state_reg[1]_i_1_0 ),
        .I3(\FSM_onehot_current_refresco_reg[4]_1 ),
        .I4(button_ok_IBUF),
        .I5(\FSM_onehot_current_refresco_reg[1]_2 ),
        .O(\v_reg[6] ));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT4 #(
    .INIT(16'h0302)) 
    \next_state_reg[1]_i_4 
       (.I0(Q[2]),
        .I1(\FSM_onehot_current_refresco_reg[2]_2 [0]),
        .I2(\FSM_onehot_current_refresco_reg[2]_2 [1]),
        .I3(\FSM_onehot_current_refresco_reg_n_0_[2] ),
        .O(\FSM_onehot_current_refresco_reg[3]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \next_state_reg[1]_i_6 
       (.I0(\FSM_onehot_current_refresco_reg_n_0_[4] ),
        .I1(\FSM_onehot_current_refresco_reg[2]_2 [1]),
        .I2(\FSM_onehot_current_refresco_reg[2]_2 [0]),
        .O(\FSM_onehot_current_refresco_reg[4]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair79" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \next_state_reg[1]_i_7 
       (.I0(Q[1]),
        .I1(\FSM_onehot_current_refresco_reg[2]_2 [1]),
        .I2(\FSM_onehot_current_refresco_reg[2]_2 [0]),
        .O(\FSM_onehot_current_refresco_reg[1]_2 ));
endmodule

module int_to_string
   (\total_reg[0] ,
    \current_state_reg[1] ,
    \reg_reg[1] ,
    \current_state_reg[1]_0 ,
    \reg_reg[1]_0 ,
    \total_reg[15] ,
    \disp_dinero[3]_1 ,
    \string_cent_decenas[1]5__186_carry_0 ,
    \total_reg[20] ,
    \disp_dinero[5]_3 ,
    \total_reg[29] ,
    \total_reg[26] ,
    Q,
    \caracter_reg[1] ,
    \caracter_reg[1]_0 ,
    \caracter_reg[2] ,
    \caracter[1]_i_6 ,
    \caracter[1]_i_6_0 ,
    \caracter_reg[2]_0 );
  output \total_reg[0] ;
  output \current_state_reg[1] ;
  output \reg_reg[1] ;
  output \current_state_reg[1]_0 ;
  output \reg_reg[1]_0 ;
  output \total_reg[15] ;
  output [1:0]\disp_dinero[3]_1 ;
  output \string_cent_decenas[1]5__186_carry_0 ;
  output \total_reg[20] ;
  output [0:0]\disp_dinero[5]_3 ;
  output \total_reg[29] ;
  output \total_reg[26] ;
  input [30:0]Q;
  input \caracter_reg[1] ;
  input \caracter_reg[1]_0 ;
  input \caracter_reg[2] ;
  input \caracter[1]_i_6 ;
  input [0:0]\caracter[1]_i_6_0 ;
  input \caracter_reg[2]_0 ;

  wire [30:0]Q;
  wire \caracter[1]_i_10_n_0 ;
  wire \caracter[1]_i_6 ;
  wire [0:0]\caracter[1]_i_6_0 ;
  wire \caracter[3]_i_14_n_0 ;
  wire \caracter[3]_i_15_n_0 ;
  wire \caracter[3]_i_16_n_0 ;
  wire \caracter[3]_i_17_n_0 ;
  wire \caracter[3]_i_18_n_0 ;
  wire \caracter[3]_i_21_n_0 ;
  wire \caracter[3]_i_22_n_0 ;
  wire \caracter[3]_i_23_n_0 ;
  wire \caracter[3]_i_24_n_0 ;
  wire \caracter[3]_i_25_n_0 ;
  wire \caracter[3]_i_26_n_0 ;
  wire \caracter[3]_i_27_n_0 ;
  wire \caracter[3]_i_28_n_0 ;
  wire \caracter[3]_i_29_n_0 ;
  wire \caracter[3]_i_30_n_0 ;
  wire \caracter[3]_i_31_n_0 ;
  wire \caracter[3]_i_32_n_0 ;
  wire \caracter[3]_i_33_n_0 ;
  wire \caracter[3]_i_34_n_0 ;
  wire \caracter[3]_i_35_n_0 ;
  wire \caracter[3]_i_36_n_0 ;
  wire \caracter[3]_i_37_n_0 ;
  wire \caracter[3]_i_38_n_0 ;
  wire \caracter[3]_i_39_n_0 ;
  wire \caracter[3]_i_40_n_0 ;
  wire \caracter[3]_i_41_n_0 ;
  wire \caracter[3]_i_42_n_0 ;
  wire \caracter[3]_i_43_n_0 ;
  wire \caracter_reg[1] ;
  wire \caracter_reg[1]_0 ;
  wire \caracter_reg[2] ;
  wire \caracter_reg[2]_0 ;
  wire [6:1]cent;
  wire \current_state_reg[1] ;
  wire \current_state_reg[1]_0 ;
  wire [1:0]\disp_dinero[3]_1 ;
  wire [0:0]\disp_dinero[5]_3 ;
  wire euros1__169_carry__0_i_1_n_0;
  wire euros1__169_carry__0_i_2_n_0;
  wire euros1__169_carry__0_i_3_n_0;
  wire euros1__169_carry__0_i_4_n_0;
  wire euros1__169_carry__0_n_0;
  wire euros1__169_carry__0_n_4;
  wire euros1__169_carry__0_n_5;
  wire euros1__169_carry__0_n_6;
  wire euros1__169_carry__0_n_7;
  wire euros1__169_carry__1_i_1_n_0;
  wire euros1__169_carry__1_i_2_n_0;
  wire euros1__169_carry__1_i_3_n_0;
  wire euros1__169_carry__1_i_4_n_0;
  wire euros1__169_carry__1_n_0;
  wire euros1__169_carry__1_n_4;
  wire euros1__169_carry__1_n_5;
  wire euros1__169_carry__1_n_6;
  wire euros1__169_carry__1_n_7;
  wire euros1__169_carry__2_i_1_n_0;
  wire euros1__169_carry__2_i_2_n_0;
  wire euros1__169_carry__2_i_3_n_0;
  wire euros1__169_carry__2_i_4_n_0;
  wire euros1__169_carry__2_n_0;
  wire euros1__169_carry__2_n_4;
  wire euros1__169_carry__2_n_5;
  wire euros1__169_carry__2_n_6;
  wire euros1__169_carry__2_n_7;
  wire euros1__169_carry__3_i_1_n_0;
  wire euros1__169_carry__3_i_2_n_0;
  wire euros1__169_carry__3_i_3_n_0;
  wire euros1__169_carry__3_i_4_n_0;
  wire euros1__169_carry__3_n_0;
  wire euros1__169_carry__3_n_4;
  wire euros1__169_carry__3_n_5;
  wire euros1__169_carry__3_n_6;
  wire euros1__169_carry__3_n_7;
  wire euros1__169_carry__4_i_1_n_0;
  wire euros1__169_carry__4_i_2_n_0;
  wire euros1__169_carry__4_n_6;
  wire euros1__169_carry__4_n_7;
  wire euros1__169_carry_i_1_n_0;
  wire euros1__169_carry_i_2_n_0;
  wire euros1__169_carry_i_3_n_0;
  wire euros1__169_carry_n_0;
  wire euros1__169_carry_n_4;
  wire euros1__169_carry_n_5;
  wire euros1__169_carry_n_6;
  wire euros1__1_carry__0_i_1_n_0;
  wire euros1__1_carry__0_i_2_n_0;
  wire euros1__1_carry__0_i_3_n_0;
  wire euros1__1_carry__0_i_4_n_0;
  wire euros1__1_carry__0_i_5_n_0;
  wire euros1__1_carry__0_n_0;
  wire euros1__1_carry__0_n_4;
  wire euros1__1_carry__1_i_1_n_0;
  wire euros1__1_carry__1_i_2_n_0;
  wire euros1__1_carry__1_i_3_n_0;
  wire euros1__1_carry__1_i_4_n_0;
  wire euros1__1_carry__1_i_5_n_0;
  wire euros1__1_carry__1_i_6_n_0;
  wire euros1__1_carry__1_i_7_n_0;
  wire euros1__1_carry__1_i_8_n_0;
  wire euros1__1_carry__1_n_0;
  wire euros1__1_carry__1_n_4;
  wire euros1__1_carry__1_n_5;
  wire euros1__1_carry__1_n_6;
  wire euros1__1_carry__1_n_7;
  wire euros1__1_carry__2_i_1_n_0;
  wire euros1__1_carry__2_i_2_n_0;
  wire euros1__1_carry__2_i_3_n_0;
  wire euros1__1_carry__2_i_4_n_0;
  wire euros1__1_carry__2_i_5_n_0;
  wire euros1__1_carry__2_i_6_n_0;
  wire euros1__1_carry__2_i_7_n_0;
  wire euros1__1_carry__2_i_8_n_0;
  wire euros1__1_carry__2_n_0;
  wire euros1__1_carry__2_n_4;
  wire euros1__1_carry__2_n_5;
  wire euros1__1_carry__2_n_6;
  wire euros1__1_carry__2_n_7;
  wire euros1__1_carry__3_i_1_n_0;
  wire euros1__1_carry__3_i_2_n_0;
  wire euros1__1_carry__3_i_3_n_0;
  wire euros1__1_carry__3_i_4_n_0;
  wire euros1__1_carry__3_i_5_n_0;
  wire euros1__1_carry__3_i_6_n_0;
  wire euros1__1_carry__3_i_7_n_0;
  wire euros1__1_carry__3_i_8_n_0;
  wire euros1__1_carry__3_n_0;
  wire euros1__1_carry__3_n_4;
  wire euros1__1_carry__3_n_5;
  wire euros1__1_carry__3_n_6;
  wire euros1__1_carry__3_n_7;
  wire euros1__1_carry__4_i_1_n_0;
  wire euros1__1_carry__4_i_2_n_0;
  wire euros1__1_carry__4_i_3_n_0;
  wire euros1__1_carry__4_i_4_n_0;
  wire euros1__1_carry__4_i_5_n_0;
  wire euros1__1_carry__4_i_6_n_0;
  wire euros1__1_carry__4_i_7_n_0;
  wire euros1__1_carry__4_i_8_n_0;
  wire euros1__1_carry__4_n_0;
  wire euros1__1_carry__4_n_4;
  wire euros1__1_carry__4_n_5;
  wire euros1__1_carry__4_n_6;
  wire euros1__1_carry__4_n_7;
  wire euros1__1_carry__5_i_1_n_0;
  wire euros1__1_carry__5_i_2_n_0;
  wire euros1__1_carry__5_i_3_n_0;
  wire euros1__1_carry__5_i_4_n_0;
  wire euros1__1_carry__5_i_5_n_0;
  wire euros1__1_carry__5_i_6_n_0;
  wire euros1__1_carry__5_i_7_n_0;
  wire euros1__1_carry__5_i_8_n_0;
  wire euros1__1_carry__5_n_0;
  wire euros1__1_carry__5_n_4;
  wire euros1__1_carry__5_n_5;
  wire euros1__1_carry__5_n_6;
  wire euros1__1_carry__5_n_7;
  wire euros1__1_carry__6_i_1_n_0;
  wire euros1__1_carry__6_i_2_n_0;
  wire euros1__1_carry__6_i_3_n_0;
  wire euros1__1_carry__6_i_4_n_0;
  wire euros1__1_carry__6_i_5_n_0;
  wire euros1__1_carry__6_i_6_n_0;
  wire euros1__1_carry__6_i_7_n_0;
  wire euros1__1_carry__6_i_8_n_0;
  wire euros1__1_carry__6_n_0;
  wire euros1__1_carry__6_n_4;
  wire euros1__1_carry__6_n_5;
  wire euros1__1_carry__6_n_6;
  wire euros1__1_carry__6_n_7;
  wire euros1__1_carry__7_i_1_n_0;
  wire euros1__1_carry__7_n_6;
  wire euros1__1_carry__7_n_7;
  wire euros1__1_carry_i_1_n_0;
  wire euros1__1_carry_i_2_n_0;
  wire euros1__1_carry_i_3_n_0;
  wire euros1__1_carry_n_0;
  wire euros1__231_carry__0_i_1_n_0;
  wire euros1__231_carry__0_i_2_n_0;
  wire euros1__231_carry__0_i_3_n_0;
  wire euros1__231_carry__0_i_4_n_0;
  wire euros1__231_carry__0_i_5_n_0;
  wire euros1__231_carry__0_i_6_n_0;
  wire euros1__231_carry__0_i_7_n_0;
  wire euros1__231_carry__0_i_8_n_0;
  wire euros1__231_carry__0_n_0;
  wire euros1__231_carry__0_n_4;
  wire euros1__231_carry__0_n_5;
  wire euros1__231_carry__0_n_6;
  wire euros1__231_carry__0_n_7;
  wire euros1__231_carry__1_i_1_n_0;
  wire euros1__231_carry__1_i_2_n_0;
  wire euros1__231_carry__1_i_3_n_0;
  wire euros1__231_carry__1_i_4_n_0;
  wire euros1__231_carry__1_i_5_n_0;
  wire euros1__231_carry__1_i_6_n_0;
  wire euros1__231_carry__1_i_7_n_0;
  wire euros1__231_carry__1_i_8_n_0;
  wire euros1__231_carry__1_n_0;
  wire euros1__231_carry__1_n_4;
  wire euros1__231_carry__1_n_5;
  wire euros1__231_carry__1_n_6;
  wire euros1__231_carry__1_n_7;
  wire euros1__231_carry__2_i_1_n_0;
  wire euros1__231_carry__2_i_2_n_0;
  wire euros1__231_carry__2_i_3_n_0;
  wire euros1__231_carry__2_i_4_n_0;
  wire euros1__231_carry__2_i_5_n_0;
  wire euros1__231_carry__2_i_6_n_0;
  wire euros1__231_carry__2_i_7_n_0;
  wire euros1__231_carry__2_n_4;
  wire euros1__231_carry__2_n_5;
  wire euros1__231_carry__2_n_6;
  wire euros1__231_carry__2_n_7;
  wire euros1__231_carry_i_1_n_0;
  wire euros1__231_carry_i_2_n_0;
  wire euros1__231_carry_i_3_n_0;
  wire euros1__231_carry_n_0;
  wire euros1__231_carry_n_4;
  wire euros1__231_carry_n_5;
  wire euros1__231_carry_n_6;
  wire euros1__231_carry_n_7;
  wire euros1__276_carry__0_i_1_n_0;
  wire euros1__276_carry__0_i_2_n_0;
  wire euros1__276_carry__0_i_3_n_0;
  wire euros1__276_carry__0_i_4_n_0;
  wire euros1__276_carry__0_i_5_n_0;
  wire euros1__276_carry__0_i_6_n_0;
  wire euros1__276_carry__0_i_7_n_0;
  wire euros1__276_carry__0_i_8_n_0;
  wire euros1__276_carry__0_i_9_n_0;
  wire euros1__276_carry__0_n_0;
  wire euros1__276_carry__1_i_10_n_0;
  wire euros1__276_carry__1_i_11_n_0;
  wire euros1__276_carry__1_i_12_n_0;
  wire euros1__276_carry__1_i_13_n_0;
  wire euros1__276_carry__1_i_14_n_0;
  wire euros1__276_carry__1_i_15_n_0;
  wire euros1__276_carry__1_i_1_n_0;
  wire euros1__276_carry__1_i_2_n_0;
  wire euros1__276_carry__1_i_3_n_0;
  wire euros1__276_carry__1_i_4_n_0;
  wire euros1__276_carry__1_i_5_n_0;
  wire euros1__276_carry__1_i_6_n_0;
  wire euros1__276_carry__1_i_7_n_0;
  wire euros1__276_carry__1_i_8_n_0;
  wire euros1__276_carry__1_i_9_n_0;
  wire euros1__276_carry__1_n_0;
  wire euros1__276_carry__2_i_10_n_0;
  wire euros1__276_carry__2_i_11_n_0;
  wire euros1__276_carry__2_i_12_n_0;
  wire euros1__276_carry__2_i_13_n_0;
  wire euros1__276_carry__2_i_14_n_0;
  wire euros1__276_carry__2_i_1_n_0;
  wire euros1__276_carry__2_i_2_n_0;
  wire euros1__276_carry__2_i_3_n_0;
  wire euros1__276_carry__2_i_4_n_0;
  wire euros1__276_carry__2_i_5_n_0;
  wire euros1__276_carry__2_i_6_n_0;
  wire euros1__276_carry__2_i_7_n_0;
  wire euros1__276_carry__2_i_8_n_0;
  wire euros1__276_carry__2_i_9_n_0;
  wire euros1__276_carry__2_n_0;
  wire euros1__276_carry__3_i_10_n_0;
  wire euros1__276_carry__3_i_11_n_0;
  wire euros1__276_carry__3_i_12_n_0;
  wire euros1__276_carry__3_i_13_n_0;
  wire euros1__276_carry__3_i_14_n_0;
  wire euros1__276_carry__3_i_15_n_0;
  wire euros1__276_carry__3_i_16_n_0;
  wire euros1__276_carry__3_i_1_n_0;
  wire euros1__276_carry__3_i_2_n_0;
  wire euros1__276_carry__3_i_3_n_0;
  wire euros1__276_carry__3_i_4_n_0;
  wire euros1__276_carry__3_i_5_n_0;
  wire euros1__276_carry__3_i_6_n_0;
  wire euros1__276_carry__3_i_7_n_0;
  wire euros1__276_carry__3_i_8_n_0;
  wire euros1__276_carry__3_i_9_n_0;
  wire euros1__276_carry__3_n_0;
  wire euros1__276_carry__4_i_10_n_0;
  wire euros1__276_carry__4_i_11_n_0;
  wire euros1__276_carry__4_i_12_n_0;
  wire euros1__276_carry__4_i_13_n_0;
  wire euros1__276_carry__4_i_14_n_0;
  wire euros1__276_carry__4_i_1_n_0;
  wire euros1__276_carry__4_i_2_n_0;
  wire euros1__276_carry__4_i_3_n_0;
  wire euros1__276_carry__4_i_4_n_0;
  wire euros1__276_carry__4_i_5_n_0;
  wire euros1__276_carry__4_i_6_n_0;
  wire euros1__276_carry__4_i_7_n_0;
  wire euros1__276_carry__4_i_8_n_0;
  wire euros1__276_carry__4_i_9_n_0;
  wire euros1__276_carry__4_n_0;
  wire euros1__276_carry__4_n_4;
  wire euros1__276_carry__4_n_5;
  wire euros1__276_carry__4_n_6;
  wire euros1__276_carry__4_n_7;
  wire euros1__276_carry__5_i_1_n_0;
  wire euros1__276_carry__5_i_2_n_0;
  wire euros1__276_carry__5_i_3_n_0;
  wire euros1__276_carry__5_i_4_n_0;
  wire euros1__276_carry__5_i_5_n_0;
  wire euros1__276_carry__5_i_6_n_0;
  wire euros1__276_carry__5_i_7_n_0;
  wire euros1__276_carry__5_n_6;
  wire euros1__276_carry__5_n_7;
  wire euros1__276_carry_i_1_n_0;
  wire euros1__276_carry_i_2_n_0;
  wire euros1__276_carry_i_3_n_0;
  wire euros1__276_carry_i_4_n_0;
  wire euros1__276_carry_i_5_n_0;
  wire euros1__276_carry_i_6_n_0;
  wire euros1__276_carry_i_7_n_0;
  wire euros1__276_carry_i_8_n_0;
  wire euros1__276_carry_n_0;
  wire euros1__333_carry_i_1_n_0;
  wire euros1__333_carry_i_2_n_0;
  wire euros1__333_carry_i_3_n_0;
  wire euros1__333_carry_i_4_n_0;
  wire euros1__333_carry_n_5;
  wire euros1__333_carry_n_6;
  wire euros1__333_carry_n_7;
  wire euros1__339_carry__0_i_1_n_0;
  wire euros1__339_carry__0_i_2_n_0;
  wire euros1__339_carry__0_i_3_n_0;
  wire euros1__339_carry__0_i_4_n_0;
  wire euros1__339_carry__0_n_4;
  wire euros1__339_carry__0_n_5;
  wire euros1__339_carry__0_n_6;
  wire euros1__339_carry__0_n_7;
  wire euros1__339_carry_i_1_n_0;
  wire euros1__339_carry_i_2_n_0;
  wire euros1__339_carry_i_3_n_0;
  wire euros1__339_carry_i_4_n_0;
  wire euros1__339_carry_n_0;
  wire euros1__339_carry_n_4;
  wire euros1__339_carry_n_5;
  wire euros1__92_carry__0_i_1_n_0;
  wire euros1__92_carry__0_i_2_n_0;
  wire euros1__92_carry__0_i_3_n_0;
  wire euros1__92_carry__0_i_4_n_0;
  wire euros1__92_carry__0_i_5_n_0;
  wire euros1__92_carry__0_i_6_n_0;
  wire euros1__92_carry__0_i_7_n_0;
  wire euros1__92_carry__0_i_8_n_0;
  wire euros1__92_carry__0_n_0;
  wire euros1__92_carry__0_n_4;
  wire euros1__92_carry__0_n_5;
  wire euros1__92_carry__0_n_6;
  wire euros1__92_carry__0_n_7;
  wire euros1__92_carry__1_i_1_n_0;
  wire euros1__92_carry__1_i_2_n_0;
  wire euros1__92_carry__1_i_3_n_0;
  wire euros1__92_carry__1_i_4_n_0;
  wire euros1__92_carry__1_i_5_n_0;
  wire euros1__92_carry__1_i_6_n_0;
  wire euros1__92_carry__1_i_7_n_0;
  wire euros1__92_carry__1_i_8_n_0;
  wire euros1__92_carry__1_n_0;
  wire euros1__92_carry__1_n_4;
  wire euros1__92_carry__1_n_5;
  wire euros1__92_carry__1_n_6;
  wire euros1__92_carry__1_n_7;
  wire euros1__92_carry__2_i_1_n_0;
  wire euros1__92_carry__2_i_2_n_0;
  wire euros1__92_carry__2_i_3_n_0;
  wire euros1__92_carry__2_i_4_n_0;
  wire euros1__92_carry__2_i_5_n_0;
  wire euros1__92_carry__2_i_6_n_0;
  wire euros1__92_carry__2_i_7_n_0;
  wire euros1__92_carry__2_i_8_n_0;
  wire euros1__92_carry__2_n_0;
  wire euros1__92_carry__2_n_4;
  wire euros1__92_carry__2_n_5;
  wire euros1__92_carry__2_n_6;
  wire euros1__92_carry__2_n_7;
  wire euros1__92_carry__3_i_1_n_0;
  wire euros1__92_carry__3_i_2_n_0;
  wire euros1__92_carry__3_i_3_n_0;
  wire euros1__92_carry__3_i_4_n_0;
  wire euros1__92_carry__3_i_5_n_0;
  wire euros1__92_carry__3_i_6_n_0;
  wire euros1__92_carry__3_i_7_n_0;
  wire euros1__92_carry__3_i_8_n_0;
  wire euros1__92_carry__3_n_0;
  wire euros1__92_carry__3_n_4;
  wire euros1__92_carry__3_n_5;
  wire euros1__92_carry__3_n_6;
  wire euros1__92_carry__3_n_7;
  wire euros1__92_carry__4_i_1_n_0;
  wire euros1__92_carry__4_i_2_n_0;
  wire euros1__92_carry__4_i_3_n_0;
  wire euros1__92_carry__4_i_4_n_0;
  wire euros1__92_carry__4_i_5_n_0;
  wire euros1__92_carry__4_i_6_n_0;
  wire euros1__92_carry__4_i_7_n_0;
  wire euros1__92_carry__4_i_8_n_0;
  wire euros1__92_carry__4_n_0;
  wire euros1__92_carry__4_n_4;
  wire euros1__92_carry__4_n_5;
  wire euros1__92_carry__4_n_6;
  wire euros1__92_carry__4_n_7;
  wire euros1__92_carry__5_i_1_n_0;
  wire euros1__92_carry__5_i_2_n_0;
  wire euros1__92_carry__5_i_3_n_0;
  wire euros1__92_carry__5_i_4_n_0;
  wire euros1__92_carry__5_i_5_n_0;
  wire euros1__92_carry__5_n_5;
  wire euros1__92_carry__5_n_6;
  wire euros1__92_carry__5_n_7;
  wire euros1__92_carry_i_1_n_0;
  wire euros1__92_carry_i_2_n_0;
  wire euros1__92_carry_i_3_n_0;
  wire euros1__92_carry_i_4_n_0;
  wire euros1__92_carry_n_0;
  wire euros1__92_carry_n_4;
  wire euros1__92_carry_n_5;
  wire euros1__92_carry_n_6;
  wire euros1__92_carry_n_7;
  wire i___0_carry__0_i_1_n_0;
  wire i___0_carry__0_i_2_n_0;
  wire i___0_carry__0_i_4_n_0;
  wire i___0_carry__0_i_5_n_0;
  wire i___0_carry__0_i_6_n_0;
  wire i___0_carry__0_i_7_n_0;
  wire i___0_carry__1_i_1_n_0;
  wire i___0_carry__1_i_2_n_0;
  wire i___0_carry__1_i_3_n_0;
  wire i___0_carry__1_i_4_n_0;
  wire i___0_carry__1_i_5_n_0;
  wire i___0_carry__2_i_1_n_0;
  wire i___0_carry_i_2_n_0;
  wire i___0_carry_i_3_n_0;
  wire i___0_carry_i_4_n_0;
  wire i___0_carry_i_5_n_0;
  wire i___0_carry_i_6_n_0;
  wire i___106_carry__0_i_1_n_0;
  wire i___106_carry__0_i_2_n_0;
  wire i___106_carry__0_i_3_n_0;
  wire i___106_carry__0_i_4_n_0;
  wire i___106_carry__0_i_5_n_0;
  wire i___106_carry__0_i_6_n_0;
  wire i___106_carry__0_i_7_n_0;
  wire i___106_carry__0_i_8_n_0;
  wire i___106_carry__0_i_9_n_0;
  wire i___106_carry__1_i_10_n_0;
  wire i___106_carry__1_i_11_n_0;
  wire i___106_carry__1_i_12_n_0;
  wire i___106_carry__1_i_1_n_0;
  wire i___106_carry__1_i_2_n_0;
  wire i___106_carry__1_i_3_n_0;
  wire i___106_carry__1_i_4_n_0;
  wire i___106_carry__1_i_5_n_0;
  wire i___106_carry__1_i_6_n_0;
  wire i___106_carry__1_i_7_n_0;
  wire i___106_carry__1_i_8_n_0;
  wire i___106_carry__1_i_9_n_0;
  wire i___106_carry__2_i_10_n_0;
  wire i___106_carry__2_i_1_n_0;
  wire i___106_carry__2_i_2_n_0;
  wire i___106_carry__2_i_3_n_0;
  wire i___106_carry__2_i_4_n_0;
  wire i___106_carry__2_i_5_n_0;
  wire i___106_carry__2_i_6_n_0;
  wire i___106_carry__2_i_7_n_0;
  wire i___106_carry__2_i_8_n_0;
  wire i___106_carry__2_i_9_n_0;
  wire i___106_carry__3_i_1_n_0;
  wire i___106_carry__3_i_2_n_0;
  wire i___106_carry__3_i_3_n_0;
  wire i___106_carry__3_i_4_n_0;
  wire i___106_carry__3_i_5_n_0;
  wire i___106_carry__3_i_6_n_0;
  wire i___106_carry__3_i_7_n_0;
  wire i___106_carry__3_i_8_n_0;
  wire i___106_carry__4_i_1_n_0;
  wire i___106_carry__4_i_2_n_0;
  wire i___106_carry__4_i_3_n_0;
  wire i___106_carry__4_i_4_n_0;
  wire i___106_carry__4_i_5_n_0;
  wire i___106_carry__4_i_6_n_0;
  wire i___106_carry__4_i_7_n_0;
  wire i___106_carry__5_i_1_n_0;
  wire i___106_carry__5_i_2_n_0;
  wire i___106_carry_i_1_n_0;
  wire i___106_carry_i_2_n_0;
  wire i___106_carry_i_3_n_0;
  wire i___106_carry_i_4_n_0;
  wire i___106_carry_i_5_n_0;
  wire i___106_carry_i_6_n_0;
  wire i___106_carry_i_7_n_0;
  wire i___106_carry_i_8_n_0;
  wire i___163_carry_i_1_n_0;
  wire i___163_carry_i_2_n_0;
  wire i___163_carry_i_3_n_0;
  wire i___163_carry_i_4_n_0;
  wire i___169_carry__0_i_1_n_0;
  wire i___169_carry__0_i_2_n_0;
  wire i___169_carry__0_i_3_n_0;
  wire i___169_carry__0_i_4_n_0;
  wire i___169_carry_i_2_n_0;
  wire i___169_carry_i_3_n_0;
  wire i___169_carry_i_4_n_0;
  wire i___169_carry_i_5_n_0;
  wire i___169_carry_i_6_n_0;
  wire i___27_carry__0_i_1_n_0;
  wire i___27_carry__0_i_2_n_0;
  wire i___27_carry__0_i_3_n_0;
  wire i___27_carry__0_i_4_n_0;
  wire i___27_carry__0_i_5_n_0;
  wire i___27_carry__0_i_6_n_0;
  wire i___27_carry__0_i_7_n_0;
  wire i___27_carry__0_i_8_n_0;
  wire i___27_carry__1_i_1_n_0;
  wire i___27_carry__1_i_2_n_0;
  wire i___27_carry__1_i_3_n_0;
  wire i___27_carry__1_i_4_n_0;
  wire i___27_carry__1_i_5_n_0;
  wire i___27_carry_i_1_n_0;
  wire i___27_carry_i_2_n_0;
  wire i___27_carry_i_3_n_0;
  wire i___27_carry_i_5_n_0;
  wire i___56_carry__0_i_2_n_0;
  wire i___56_carry__0_i_3_n_0;
  wire i___56_carry__0_i_4_n_0;
  wire i___56_carry__0_i_5_n_0;
  wire i___56_carry__0_i_6_n_0;
  wire i___56_carry__1_i_1_n_0;
  wire i___56_carry__1_i_2_n_0;
  wire i___56_carry_i_1_n_0;
  wire i___56_carry_i_2_n_0;
  wire i___56_carry_i_4_n_0;
  wire i___80_carry__0_i_1_n_0;
  wire i___80_carry__0_i_2_n_0;
  wire i___80_carry__0_i_3_n_0;
  wire i___80_carry__0_i_4_n_0;
  wire i___80_carry__0_i_5_n_0;
  wire i___80_carry__0_i_6_n_0;
  wire i___80_carry__0_i_7_n_0;
  wire i___80_carry__0_i_8_n_0;
  wire i___80_carry__1_i_1_n_0;
  wire i___80_carry__1_i_2_n_0;
  wire i___80_carry__1_i_3_n_0;
  wire i___80_carry_i_1_n_0;
  wire i___80_carry_i_2_n_0;
  wire i___80_carry_i_3_n_0;
  wire i___80_carry_i_4_n_0;
  wire [1:0]p_1_in;
  wire \reg_reg[1] ;
  wire \reg_reg[1]_0 ;
  wire [1:0]\string_cent_decenas[1]5 ;
  wire \string_cent_decenas[1]5__100_carry__0_i_1_n_0 ;
  wire \string_cent_decenas[1]5__100_carry__0_i_2_n_0 ;
  wire \string_cent_decenas[1]5__100_carry__0_i_3_n_0 ;
  wire \string_cent_decenas[1]5__100_carry__0_i_4_n_0 ;
  wire \string_cent_decenas[1]5__100_carry__0_i_5_n_0 ;
  wire \string_cent_decenas[1]5__100_carry__0_i_6_n_0 ;
  wire \string_cent_decenas[1]5__100_carry__0_i_7_n_0 ;
  wire \string_cent_decenas[1]5__100_carry__0_i_8_n_0 ;
  wire \string_cent_decenas[1]5__100_carry__0_n_0 ;
  wire \string_cent_decenas[1]5__100_carry__0_n_4 ;
  wire \string_cent_decenas[1]5__100_carry__0_n_5 ;
  wire \string_cent_decenas[1]5__100_carry__0_n_6 ;
  wire \string_cent_decenas[1]5__100_carry__0_n_7 ;
  wire \string_cent_decenas[1]5__100_carry__1_i_1_n_0 ;
  wire \string_cent_decenas[1]5__100_carry__1_n_7 ;
  wire \string_cent_decenas[1]5__100_carry_i_1_n_0 ;
  wire \string_cent_decenas[1]5__100_carry_i_2_n_0 ;
  wire \string_cent_decenas[1]5__100_carry_i_3_n_0 ;
  wire \string_cent_decenas[1]5__100_carry_i_4_n_0 ;
  wire \string_cent_decenas[1]5__100_carry_i_5_n_0 ;
  wire \string_cent_decenas[1]5__100_carry_i_6_n_0 ;
  wire \string_cent_decenas[1]5__100_carry_i_7_n_0 ;
  wire \string_cent_decenas[1]5__100_carry_i_8_n_3 ;
  wire \string_cent_decenas[1]5__100_carry_n_0 ;
  wire \string_cent_decenas[1]5__100_carry_n_4 ;
  wire \string_cent_decenas[1]5__100_carry_n_5 ;
  wire \string_cent_decenas[1]5__100_carry_n_6 ;
  wire \string_cent_decenas[1]5__100_carry_n_7 ;
  wire \string_cent_decenas[1]5__124_carry__0_i_1_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__0_i_2_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__0_i_3_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__0_i_4_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__0_i_5_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__0_i_6_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__0_i_7_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__0_i_8_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__0_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__1_i_1_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__1_i_2_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__1_i_3_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__1_i_4_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__1_i_5_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__1_i_6_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__1_i_7_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__1_i_8_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__1_i_9_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__1_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__2_i_10_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__2_i_11_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__2_i_12_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__2_i_13_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__2_i_14_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__2_i_1_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__2_i_2_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__2_i_3_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__2_i_4_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__2_i_5_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__2_i_6_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__2_i_7_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__2_i_8_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__2_i_9_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__2_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__3_i_10_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__3_i_11_n_3 ;
  wire \string_cent_decenas[1]5__124_carry__3_i_12_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__3_i_13_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__3_i_14_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__3_i_1_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__3_i_2_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__3_i_3_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__3_i_4_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__3_i_5_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__3_i_6_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__3_i_7_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__3_i_8_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__3_i_9_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__3_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__4_i_10_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__4_i_11_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__4_i_12_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__4_i_1_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__4_i_2_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__4_i_3_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__4_i_4_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__4_i_5_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__4_i_6_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__4_i_7_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__4_i_8_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__4_i_9_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__4_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__4_n_4 ;
  wire \string_cent_decenas[1]5__124_carry__5_i_1_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__5_i_2_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__5_i_3_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__5_i_4_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__5_i_5_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__5_i_6_n_0 ;
  wire \string_cent_decenas[1]5__124_carry__5_n_5 ;
  wire \string_cent_decenas[1]5__124_carry__5_n_6 ;
  wire \string_cent_decenas[1]5__124_carry__5_n_7 ;
  wire \string_cent_decenas[1]5__124_carry_i_1_n_0 ;
  wire \string_cent_decenas[1]5__124_carry_i_2_n_0 ;
  wire \string_cent_decenas[1]5__124_carry_i_3_n_0 ;
  wire \string_cent_decenas[1]5__124_carry_i_4_n_0 ;
  wire \string_cent_decenas[1]5__124_carry_i_5_n_0 ;
  wire \string_cent_decenas[1]5__124_carry_i_6_n_0 ;
  wire \string_cent_decenas[1]5__124_carry_i_7_n_0 ;
  wire \string_cent_decenas[1]5__124_carry_i_8_n_0 ;
  wire \string_cent_decenas[1]5__124_carry_n_0 ;
  wire \string_cent_decenas[1]5__180_carry_i_1_n_0 ;
  wire \string_cent_decenas[1]5__180_carry_i_2_n_0 ;
  wire \string_cent_decenas[1]5__180_carry_n_5 ;
  wire \string_cent_decenas[1]5__180_carry_n_6 ;
  wire \string_cent_decenas[1]5__180_carry_n_7 ;
  wire \string_cent_decenas[1]5__186_carry_0 ;
  wire \string_cent_decenas[1]5__186_carry__0_i_1_n_0 ;
  wire \string_cent_decenas[1]5__186_carry__0_n_7 ;
  wire \string_cent_decenas[1]5__186_carry_i_3_n_0 ;
  wire \string_cent_decenas[1]5__186_carry_i_4_n_0 ;
  wire \string_cent_decenas[1]5__186_carry_i_5_n_0 ;
  wire \string_cent_decenas[1]5__186_carry_i_6_n_0 ;
  wire \string_cent_decenas[1]5__186_carry_i_7_n_0 ;
  wire \string_cent_decenas[1]5__186_carry_i_8_n_0 ;
  wire \string_cent_decenas[1]5__186_carry_n_0 ;
  wire \string_cent_decenas[1]5__186_carry_n_4 ;
  wire \string_cent_decenas[1]5__186_carry_n_5 ;
  wire \string_cent_decenas[1]5__186_carry_n_6 ;
  wire \string_cent_decenas[1]5__27_carry__0_i_1_n_0 ;
  wire \string_cent_decenas[1]5__27_carry__0_i_2_n_0 ;
  wire \string_cent_decenas[1]5__27_carry__0_i_3_n_0 ;
  wire \string_cent_decenas[1]5__27_carry__0_i_4_n_0 ;
  wire \string_cent_decenas[1]5__27_carry__0_i_5_n_0 ;
  wire \string_cent_decenas[1]5__27_carry__0_i_6_n_0 ;
  wire \string_cent_decenas[1]5__27_carry__0_i_7_n_0 ;
  wire \string_cent_decenas[1]5__27_carry__0_i_8_n_0 ;
  wire \string_cent_decenas[1]5__27_carry__0_n_0 ;
  wire \string_cent_decenas[1]5__27_carry__0_n_4 ;
  wire \string_cent_decenas[1]5__27_carry__0_n_5 ;
  wire \string_cent_decenas[1]5__27_carry__0_n_6 ;
  wire \string_cent_decenas[1]5__27_carry__0_n_7 ;
  wire \string_cent_decenas[1]5__27_carry__1_i_2_n_0 ;
  wire \string_cent_decenas[1]5__27_carry__1_i_3_n_0 ;
  wire \string_cent_decenas[1]5__27_carry__1_i_4_n_0 ;
  wire \string_cent_decenas[1]5__27_carry__1_n_1 ;
  wire \string_cent_decenas[1]5__27_carry__1_n_6 ;
  wire \string_cent_decenas[1]5__27_carry__1_n_7 ;
  wire \string_cent_decenas[1]5__27_carry_i_1_n_0 ;
  wire \string_cent_decenas[1]5__27_carry_i_2_n_0 ;
  wire \string_cent_decenas[1]5__27_carry_i_3_n_0 ;
  wire \string_cent_decenas[1]5__27_carry_i_4_n_0 ;
  wire \string_cent_decenas[1]5__27_carry_i_5_n_0 ;
  wire \string_cent_decenas[1]5__27_carry_i_6_n_0 ;
  wire \string_cent_decenas[1]5__27_carry_i_7_n_0 ;
  wire \string_cent_decenas[1]5__27_carry_n_0 ;
  wire \string_cent_decenas[1]5__27_carry_n_4 ;
  wire \string_cent_decenas[1]5__27_carry_n_5 ;
  wire \string_cent_decenas[1]5__27_carry_n_6 ;
  wire \string_cent_decenas[1]5__27_carry_n_7 ;
  wire \string_cent_decenas[1]5__55_carry__0_i_1_n_0 ;
  wire \string_cent_decenas[1]5__55_carry__0_i_2_n_0 ;
  wire \string_cent_decenas[1]5__55_carry__0_i_4_n_0 ;
  wire \string_cent_decenas[1]5__55_carry__0_i_5_n_0 ;
  wire \string_cent_decenas[1]5__55_carry__0_i_6_n_0 ;
  wire \string_cent_decenas[1]5__55_carry__0_i_7_n_0 ;
  wire \string_cent_decenas[1]5__55_carry__0_n_0 ;
  wire \string_cent_decenas[1]5__55_carry__0_n_4 ;
  wire \string_cent_decenas[1]5__55_carry__0_n_5 ;
  wire \string_cent_decenas[1]5__55_carry__0_n_6 ;
  wire \string_cent_decenas[1]5__55_carry__0_n_7 ;
  wire \string_cent_decenas[1]5__55_carry__1_i_1_n_0 ;
  wire \string_cent_decenas[1]5__55_carry__1_i_2_n_0 ;
  wire \string_cent_decenas[1]5__55_carry__1_i_3_n_0 ;
  wire \string_cent_decenas[1]5__55_carry__1_i_4_n_0 ;
  wire \string_cent_decenas[1]5__55_carry__1_i_5_n_0 ;
  wire \string_cent_decenas[1]5__55_carry__1_i_6_n_0 ;
  wire \string_cent_decenas[1]5__55_carry__1_i_7_n_0 ;
  wire \string_cent_decenas[1]5__55_carry__1_n_0 ;
  wire \string_cent_decenas[1]5__55_carry__1_n_4 ;
  wire \string_cent_decenas[1]5__55_carry__1_n_5 ;
  wire \string_cent_decenas[1]5__55_carry__1_n_6 ;
  wire \string_cent_decenas[1]5__55_carry__1_n_7 ;
  wire \string_cent_decenas[1]5__55_carry_i_1_n_0 ;
  wire \string_cent_decenas[1]5__55_carry_i_2_n_0 ;
  wire \string_cent_decenas[1]5__55_carry_i_3_n_0 ;
  wire \string_cent_decenas[1]5__55_carry_i_5_n_0 ;
  wire \string_cent_decenas[1]5__55_carry_n_0 ;
  wire \string_cent_decenas[1]5__55_carry_n_4 ;
  wire \string_cent_decenas[1]5__55_carry_n_5 ;
  wire \string_cent_decenas[1]5__55_carry_n_6 ;
  wire \string_cent_decenas[1]5__79_carry__0_i_1_n_0 ;
  wire \string_cent_decenas[1]5__79_carry__0_i_2_n_0 ;
  wire \string_cent_decenas[1]5__79_carry__0_i_3_n_0 ;
  wire \string_cent_decenas[1]5__79_carry__0_i_4_n_0 ;
  wire \string_cent_decenas[1]5__79_carry__0_i_5_n_0 ;
  wire \string_cent_decenas[1]5__79_carry__0_i_6_n_0 ;
  wire \string_cent_decenas[1]5__79_carry__0_i_7_n_0 ;
  wire \string_cent_decenas[1]5__79_carry__0_i_8_n_0 ;
  wire \string_cent_decenas[1]5__79_carry__0_n_0 ;
  wire \string_cent_decenas[1]5__79_carry__0_n_4 ;
  wire \string_cent_decenas[1]5__79_carry__0_n_5 ;
  wire \string_cent_decenas[1]5__79_carry__0_n_6 ;
  wire \string_cent_decenas[1]5__79_carry__0_n_7 ;
  wire \string_cent_decenas[1]5__79_carry__1_i_1_n_0 ;
  wire \string_cent_decenas[1]5__79_carry__1_i_2_n_0 ;
  wire \string_cent_decenas[1]5__79_carry__1_n_1 ;
  wire \string_cent_decenas[1]5__79_carry__1_n_6 ;
  wire \string_cent_decenas[1]5__79_carry__1_n_7 ;
  wire \string_cent_decenas[1]5__79_carry_i_1_n_0 ;
  wire \string_cent_decenas[1]5__79_carry_i_2_n_0 ;
  wire \string_cent_decenas[1]5__79_carry_i_3_n_0 ;
  wire \string_cent_decenas[1]5__79_carry_i_4_n_0 ;
  wire \string_cent_decenas[1]5__79_carry_i_5_n_0 ;
  wire \string_cent_decenas[1]5__79_carry_i_6_n_0 ;
  wire \string_cent_decenas[1]5__79_carry_n_0 ;
  wire \string_cent_decenas[1]5__79_carry_n_4 ;
  wire \string_cent_decenas[1]5__79_carry_n_5 ;
  wire \string_cent_decenas[1]5__79_carry_n_6 ;
  wire \string_cent_decenas[1]5_carry__0_i_1_n_0 ;
  wire \string_cent_decenas[1]5_carry__0_i_2_n_0 ;
  wire \string_cent_decenas[1]5_carry__0_i_3_n_0 ;
  wire \string_cent_decenas[1]5_carry__0_i_5_n_0 ;
  wire \string_cent_decenas[1]5_carry__0_i_6_n_0 ;
  wire \string_cent_decenas[1]5_carry__0_i_7_n_0 ;
  wire \string_cent_decenas[1]5_carry__0_i_8_n_0 ;
  wire \string_cent_decenas[1]5_carry__0_n_0 ;
  wire \string_cent_decenas[1]5_carry__0_n_4 ;
  wire \string_cent_decenas[1]5_carry__0_n_5 ;
  wire \string_cent_decenas[1]5_carry__0_n_6 ;
  wire \string_cent_decenas[1]5_carry__1_i_1_n_0 ;
  wire \string_cent_decenas[1]5_carry__1_i_2_n_0 ;
  wire \string_cent_decenas[1]5_carry__1_i_3_n_0 ;
  wire \string_cent_decenas[1]5_carry__1_i_4_n_0 ;
  wire \string_cent_decenas[1]5_carry__1_i_5_n_0 ;
  wire \string_cent_decenas[1]5_carry__1_i_6_n_0 ;
  wire \string_cent_decenas[1]5_carry__1_i_7_n_0 ;
  wire \string_cent_decenas[1]5_carry__1_n_0 ;
  wire \string_cent_decenas[1]5_carry__1_n_4 ;
  wire \string_cent_decenas[1]5_carry__1_n_5 ;
  wire \string_cent_decenas[1]5_carry__1_n_6 ;
  wire \string_cent_decenas[1]5_carry__1_n_7 ;
  wire \string_cent_decenas[1]5_carry_i_11_n_0 ;
  wire \string_cent_decenas[1]5_carry_i_14_n_0 ;
  wire \string_cent_decenas[1]5_carry_i_16_n_0 ;
  wire \string_cent_decenas[1]5_carry_i_17_n_0 ;
  wire \string_cent_decenas[1]5_carry_i_18_n_0 ;
  wire \string_cent_decenas[1]5_carry_i_19_n_0 ;
  wire \string_cent_decenas[1]5_carry_i_1_n_0 ;
  wire \string_cent_decenas[1]5_carry_i_3_n_0 ;
  wire \string_cent_decenas[1]5_carry_i_4_n_0 ;
  wire \string_cent_decenas[1]5_carry_i_6_n_0 ;
  wire \string_cent_decenas[1]5_carry_i_8_n_0 ;
  wire \string_cent_decenas[1]5_carry_i_9_n_0 ;
  wire \string_cent_decenas[1]5_carry_n_0 ;
  wire \string_cent_decenas[1]5_carry_n_7 ;
  wire \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_0 ;
  wire \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_4 ;
  wire \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_0 ;
  wire \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_4 ;
  wire \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_5 ;
  wire \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_6 ;
  wire \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_7 ;
  wire \string_cent_decenas[1]5_inferred__0/i___0_carry__2_n_2 ;
  wire \string_cent_decenas[1]5_inferred__0/i___0_carry__2_n_7 ;
  wire \string_cent_decenas[1]5_inferred__0/i___0_carry_n_0 ;
  wire \string_cent_decenas[1]5_inferred__0/i___106_carry__0_n_0 ;
  wire \string_cent_decenas[1]5_inferred__0/i___106_carry__1_n_0 ;
  wire \string_cent_decenas[1]5_inferred__0/i___106_carry__2_n_0 ;
  wire \string_cent_decenas[1]5_inferred__0/i___106_carry__3_n_0 ;
  wire \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_0 ;
  wire \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_4 ;
  wire \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_5 ;
  wire \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_6 ;
  wire \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_7 ;
  wire \string_cent_decenas[1]5_inferred__0/i___106_carry__5_n_6 ;
  wire \string_cent_decenas[1]5_inferred__0/i___106_carry__5_n_7 ;
  wire \string_cent_decenas[1]5_inferred__0/i___106_carry_n_0 ;
  wire \string_cent_decenas[1]5_inferred__0/i___163_carry_n_5 ;
  wire \string_cent_decenas[1]5_inferred__0/i___163_carry_n_6 ;
  wire \string_cent_decenas[1]5_inferred__0/i___163_carry_n_7 ;
  wire \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_4 ;
  wire \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_5 ;
  wire \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_6 ;
  wire \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_7 ;
  wire \string_cent_decenas[1]5_inferred__0/i___169_carry_n_0 ;
  wire \string_cent_decenas[1]5_inferred__0/i___169_carry_n_4 ;
  wire \string_cent_decenas[1]5_inferred__0/i___169_carry_n_5 ;
  wire \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_0 ;
  wire \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_4 ;
  wire \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_5 ;
  wire \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_6 ;
  wire \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_7 ;
  wire \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0 ;
  wire \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_5 ;
  wire \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_6 ;
  wire \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_7 ;
  wire \string_cent_decenas[1]5_inferred__0/i___27_carry_n_0 ;
  wire \string_cent_decenas[1]5_inferred__0/i___27_carry_n_4 ;
  wire \string_cent_decenas[1]5_inferred__0/i___27_carry_n_5 ;
  wire \string_cent_decenas[1]5_inferred__0/i___27_carry_n_6 ;
  wire \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_0 ;
  wire \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_4 ;
  wire \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_5 ;
  wire \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_6 ;
  wire \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_7 ;
  wire \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1 ;
  wire \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_6 ;
  wire \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_7 ;
  wire \string_cent_decenas[1]5_inferred__0/i___56_carry_n_0 ;
  wire \string_cent_decenas[1]5_inferred__0/i___56_carry_n_4 ;
  wire \string_cent_decenas[1]5_inferred__0/i___56_carry_n_5 ;
  wire \string_cent_decenas[1]5_inferred__0/i___56_carry_n_6 ;
  wire \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_0 ;
  wire \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_4 ;
  wire \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_5 ;
  wire \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_6 ;
  wire \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_7 ;
  wire \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_1 ;
  wire \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_6 ;
  wire \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_7 ;
  wire \string_cent_decenas[1]5_inferred__0/i___80_carry_n_0 ;
  wire \string_cent_decenas[1]5_inferred__0/i___80_carry_n_4 ;
  wire \string_cent_decenas[1]5_inferred__0/i___80_carry_n_5 ;
  wire \string_cent_decenas[1]5_inferred__0/i___80_carry_n_6 ;
  wire \string_cent_decenas[1]5_inferred__0/i___80_carry_n_7 ;
  wire \total_reg[0] ;
  wire \total_reg[15] ;
  wire \total_reg[20] ;
  wire \total_reg[26] ;
  wire \total_reg[29] ;
  wire [2:0]NLW_euros1__169_carry_CO_UNCONNECTED;
  wire [0:0]NLW_euros1__169_carry_O_UNCONNECTED;
  wire [2:0]NLW_euros1__169_carry__0_CO_UNCONNECTED;
  wire [2:0]NLW_euros1__169_carry__1_CO_UNCONNECTED;
  wire [2:0]NLW_euros1__169_carry__2_CO_UNCONNECTED;
  wire [2:0]NLW_euros1__169_carry__3_CO_UNCONNECTED;
  wire [3:0]NLW_euros1__169_carry__4_CO_UNCONNECTED;
  wire [3:2]NLW_euros1__169_carry__4_O_UNCONNECTED;
  wire [2:0]NLW_euros1__1_carry_CO_UNCONNECTED;
  wire [3:0]NLW_euros1__1_carry_O_UNCONNECTED;
  wire [2:0]NLW_euros1__1_carry__0_CO_UNCONNECTED;
  wire [2:0]NLW_euros1__1_carry__0_O_UNCONNECTED;
  wire [2:0]NLW_euros1__1_carry__1_CO_UNCONNECTED;
  wire [2:0]NLW_euros1__1_carry__2_CO_UNCONNECTED;
  wire [2:0]NLW_euros1__1_carry__3_CO_UNCONNECTED;
  wire [2:0]NLW_euros1__1_carry__4_CO_UNCONNECTED;
  wire [2:0]NLW_euros1__1_carry__5_CO_UNCONNECTED;
  wire [2:0]NLW_euros1__1_carry__6_CO_UNCONNECTED;
  wire [3:0]NLW_euros1__1_carry__7_CO_UNCONNECTED;
  wire [3:2]NLW_euros1__1_carry__7_O_UNCONNECTED;
  wire [2:0]NLW_euros1__231_carry_CO_UNCONNECTED;
  wire [2:0]NLW_euros1__231_carry__0_CO_UNCONNECTED;
  wire [2:0]NLW_euros1__231_carry__1_CO_UNCONNECTED;
  wire [3:0]NLW_euros1__231_carry__2_CO_UNCONNECTED;
  wire [2:0]NLW_euros1__276_carry_CO_UNCONNECTED;
  wire [3:0]NLW_euros1__276_carry_O_UNCONNECTED;
  wire [2:0]NLW_euros1__276_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_euros1__276_carry__0_O_UNCONNECTED;
  wire [2:0]NLW_euros1__276_carry__1_CO_UNCONNECTED;
  wire [3:0]NLW_euros1__276_carry__1_O_UNCONNECTED;
  wire [2:0]NLW_euros1__276_carry__2_CO_UNCONNECTED;
  wire [3:0]NLW_euros1__276_carry__2_O_UNCONNECTED;
  wire [2:0]NLW_euros1__276_carry__3_CO_UNCONNECTED;
  wire [3:0]NLW_euros1__276_carry__3_O_UNCONNECTED;
  wire [2:0]NLW_euros1__276_carry__4_CO_UNCONNECTED;
  wire [3:0]NLW_euros1__276_carry__5_CO_UNCONNECTED;
  wire [3:2]NLW_euros1__276_carry__5_O_UNCONNECTED;
  wire [3:0]NLW_euros1__333_carry_CO_UNCONNECTED;
  wire [3:3]NLW_euros1__333_carry_O_UNCONNECTED;
  wire [2:0]NLW_euros1__339_carry_CO_UNCONNECTED;
  wire [3:0]NLW_euros1__339_carry__0_CO_UNCONNECTED;
  wire [2:0]NLW_euros1__92_carry_CO_UNCONNECTED;
  wire [2:0]NLW_euros1__92_carry__0_CO_UNCONNECTED;
  wire [2:0]NLW_euros1__92_carry__1_CO_UNCONNECTED;
  wire [2:0]NLW_euros1__92_carry__2_CO_UNCONNECTED;
  wire [2:0]NLW_euros1__92_carry__3_CO_UNCONNECTED;
  wire [2:0]NLW_euros1__92_carry__4_CO_UNCONNECTED;
  wire [3:0]NLW_euros1__92_carry__5_CO_UNCONNECTED;
  wire [3:3]NLW_euros1__92_carry__5_O_UNCONNECTED;
  wire [2:0]\NLW_string_cent_decenas[1]5__100_carry_CO_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5__100_carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_string_cent_decenas[1]5__100_carry__1_CO_UNCONNECTED ;
  wire [3:1]\NLW_string_cent_decenas[1]5__100_carry__1_O_UNCONNECTED ;
  wire [3:1]\NLW_string_cent_decenas[1]5__100_carry_i_8_CO_UNCONNECTED ;
  wire [3:0]\NLW_string_cent_decenas[1]5__100_carry_i_8_O_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5__124_carry_CO_UNCONNECTED ;
  wire [3:0]\NLW_string_cent_decenas[1]5__124_carry_O_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5__124_carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_string_cent_decenas[1]5__124_carry__0_O_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5__124_carry__1_CO_UNCONNECTED ;
  wire [3:0]\NLW_string_cent_decenas[1]5__124_carry__1_O_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5__124_carry__2_CO_UNCONNECTED ;
  wire [3:0]\NLW_string_cent_decenas[1]5__124_carry__2_O_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5__124_carry__3_CO_UNCONNECTED ;
  wire [3:0]\NLW_string_cent_decenas[1]5__124_carry__3_O_UNCONNECTED ;
  wire [3:1]\NLW_string_cent_decenas[1]5__124_carry__3_i_11_CO_UNCONNECTED ;
  wire [3:0]\NLW_string_cent_decenas[1]5__124_carry__3_i_11_O_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5__124_carry__4_CO_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5__124_carry__4_O_UNCONNECTED ;
  wire [3:0]\NLW_string_cent_decenas[1]5__124_carry__5_CO_UNCONNECTED ;
  wire [3:3]\NLW_string_cent_decenas[1]5__124_carry__5_O_UNCONNECTED ;
  wire [3:0]\NLW_string_cent_decenas[1]5__180_carry_CO_UNCONNECTED ;
  wire [3:3]\NLW_string_cent_decenas[1]5__180_carry_O_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5__186_carry_CO_UNCONNECTED ;
  wire [3:0]\NLW_string_cent_decenas[1]5__186_carry__0_CO_UNCONNECTED ;
  wire [3:1]\NLW_string_cent_decenas[1]5__186_carry__0_O_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5__27_carry_CO_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5__27_carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_string_cent_decenas[1]5__27_carry__1_CO_UNCONNECTED ;
  wire [3:2]\NLW_string_cent_decenas[1]5__27_carry__1_O_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5__55_carry_CO_UNCONNECTED ;
  wire [0:0]\NLW_string_cent_decenas[1]5__55_carry_O_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5__55_carry__0_CO_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5__55_carry__1_CO_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5__79_carry_CO_UNCONNECTED ;
  wire [0:0]\NLW_string_cent_decenas[1]5__79_carry_O_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5__79_carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_string_cent_decenas[1]5__79_carry__1_CO_UNCONNECTED ;
  wire [3:2]\NLW_string_cent_decenas[1]5__79_carry__1_O_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5_carry_CO_UNCONNECTED ;
  wire [3:1]\NLW_string_cent_decenas[1]5_carry_O_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5_carry__0_CO_UNCONNECTED ;
  wire [0:0]\NLW_string_cent_decenas[1]5_carry__0_O_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5_carry__1_CO_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5_inferred__0/i___0_carry_CO_UNCONNECTED ;
  wire [3:0]\NLW_string_cent_decenas[1]5_inferred__0/i___0_carry_O_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__0_CO_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__0_O_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__1_CO_UNCONNECTED ;
  wire [3:0]\NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__2_CO_UNCONNECTED ;
  wire [3:1]\NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__2_O_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5_inferred__0/i___106_carry_CO_UNCONNECTED ;
  wire [3:0]\NLW_string_cent_decenas[1]5_inferred__0/i___106_carry_O_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__0_O_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__1_CO_UNCONNECTED ;
  wire [3:0]\NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__1_O_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__2_CO_UNCONNECTED ;
  wire [3:0]\NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__2_O_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__3_CO_UNCONNECTED ;
  wire [3:0]\NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__3_O_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__4_CO_UNCONNECTED ;
  wire [3:0]\NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__5_CO_UNCONNECTED ;
  wire [3:2]\NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__5_O_UNCONNECTED ;
  wire [3:0]\NLW_string_cent_decenas[1]5_inferred__0/i___163_carry_CO_UNCONNECTED ;
  wire [3:3]\NLW_string_cent_decenas[1]5_inferred__0/i___163_carry_O_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5_inferred__0/i___169_carry_CO_UNCONNECTED ;
  wire [0:0]\NLW_string_cent_decenas[1]5_inferred__0/i___169_carry_O_UNCONNECTED ;
  wire [3:0]\NLW_string_cent_decenas[1]5_inferred__0/i___169_carry__0_CO_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5_inferred__0/i___27_carry_CO_UNCONNECTED ;
  wire [0:0]\NLW_string_cent_decenas[1]5_inferred__0/i___27_carry_O_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5_inferred__0/i___27_carry__0_CO_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5_inferred__0/i___27_carry__1_CO_UNCONNECTED ;
  wire [3:3]\NLW_string_cent_decenas[1]5_inferred__0/i___27_carry__1_O_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5_inferred__0/i___56_carry_CO_UNCONNECTED ;
  wire [0:0]\NLW_string_cent_decenas[1]5_inferred__0/i___56_carry_O_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5_inferred__0/i___56_carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_string_cent_decenas[1]5_inferred__0/i___56_carry__1_CO_UNCONNECTED ;
  wire [3:2]\NLW_string_cent_decenas[1]5_inferred__0/i___56_carry__1_O_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5_inferred__0/i___80_carry_CO_UNCONNECTED ;
  wire [2:0]\NLW_string_cent_decenas[1]5_inferred__0/i___80_carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_string_cent_decenas[1]5_inferred__0/i___80_carry__1_CO_UNCONNECTED ;
  wire [3:2]\NLW_string_cent_decenas[1]5_inferred__0/i___80_carry__1_O_UNCONNECTED ;

  LUT4 #(
    .INIT(16'hFFFE)) 
    \FSM_sequential_next_state_reg[1]_i_5 
       (.I0(Q[29]),
        .I1(Q[28]),
        .I2(Q[25]),
        .I3(Q[24]),
        .O(\total_reg[29] ));
  LUT3 #(
    .INIT(8'h06)) 
    \caracter[0]_i_13 
       (.I0(p_1_in[0]),
        .I1(Q[0]),
        .I2(\total_reg[15] ),
        .O(\disp_dinero[5]_3 ));
  LUT6 #(
    .INIT(64'hFA3F83ACCA3E03A0)) 
    \caracter[0]_i_7 
       (.I0(\caracter[3]_i_29_n_0 ),
        .I1(\caracter[3]_i_26_n_0 ),
        .I2(\caracter[3]_i_14_n_0 ),
        .I3(\caracter[3]_i_27_n_0 ),
        .I4(\caracter[3]_i_30_n_0 ),
        .I5(\caracter[3]_i_28_n_0 ),
        .O(\disp_dinero[3]_1 [0]));
  LUT6 #(
    .INIT(64'h0040505030405050)) 
    \caracter[1]_i_10 
       (.I0(\caracter[3]_i_29_n_0 ),
        .I1(\caracter[3]_i_26_n_0 ),
        .I2(\caracter[3]_i_14_n_0 ),
        .I3(\caracter[3]_i_27_n_0 ),
        .I4(\caracter[3]_i_30_n_0 ),
        .I5(\caracter[3]_i_28_n_0 ),
        .O(\caracter[1]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h0044000004000440)) 
    \caracter[1]_i_12 
       (.I0(\caracter[1]_i_6 ),
        .I1(\caracter[1]_i_6_0 ),
        .I2(\string_cent_decenas[1]5__186_carry__0_n_7 ),
        .I3(\string_cent_decenas[1]5__186_carry_n_6 ),
        .I4(\string_cent_decenas[1]5__186_carry_n_5 ),
        .I5(\string_cent_decenas[1]5__186_carry_n_4 ),
        .O(\current_state_reg[1]_0 ));
  LUT6 #(
    .INIT(64'h4404000000000000)) 
    \caracter[1]_i_4 
       (.I0(\caracter[3]_i_15_n_0 ),
        .I1(\caracter[3]_i_18_n_0 ),
        .I2(\caracter[3]_i_17_n_0 ),
        .I3(\caracter[1]_i_10_n_0 ),
        .I4(\caracter_reg[1] ),
        .I5(\caracter_reg[1]_0 ),
        .O(\current_state_reg[1] ));
  LUT6 #(
    .INIT(64'h2028280820080808)) 
    \caracter[2]_i_11 
       (.I0(\caracter_reg[2] ),
        .I1(\caracter[3]_i_25_n_0 ),
        .I2(\caracter[3]_i_22_n_0 ),
        .I3(\caracter[3]_i_24_n_0 ),
        .I4(\caracter[3]_i_23_n_0 ),
        .I5(\caracter[3]_i_27_n_0 ),
        .O(\reg_reg[1] ));
  LUT6 #(
    .INIT(64'hD102000000000000)) 
    \caracter[2]_i_3 
       (.I0(\string_cent_decenas[1]5__186_carry__0_n_7 ),
        .I1(\string_cent_decenas[1]5__186_carry_n_4 ),
        .I2(\string_cent_decenas[1]5__186_carry_n_6 ),
        .I3(\string_cent_decenas[1]5__186_carry_n_5 ),
        .I4(\caracter_reg[2] ),
        .I5(\caracter_reg[2]_0 ),
        .O(\reg_reg[1]_0 ));
  LUT5 #(
    .INIT(32'h7CCC377F)) 
    \caracter[3]_i_14 
       (.I0(\caracter[3]_i_21_n_0 ),
        .I1(\caracter[3]_i_22_n_0 ),
        .I2(\caracter[3]_i_23_n_0 ),
        .I3(\caracter[3]_i_24_n_0 ),
        .I4(\caracter[3]_i_25_n_0 ),
        .O(\caracter[3]_i_14_n_0 ));
  LUT6 #(
    .INIT(64'hDC3333C73D4343DF)) 
    \caracter[3]_i_15 
       (.I0(\caracter[3]_i_26_n_0 ),
        .I1(\caracter[3]_i_25_n_0 ),
        .I2(\caracter[3]_i_22_n_0 ),
        .I3(\caracter[3]_i_23_n_0 ),
        .I4(\caracter[3]_i_24_n_0 ),
        .I5(\caracter[3]_i_27_n_0 ),
        .O(\caracter[3]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'hDBD39B9224240404)) 
    \caracter[3]_i_16 
       (.I0(\caracter[3]_i_14_n_0 ),
        .I1(\caracter[3]_i_27_n_0 ),
        .I2(\caracter[3]_i_26_n_0 ),
        .I3(\caracter[3]_i_28_n_0 ),
        .I4(\caracter[3]_i_29_n_0 ),
        .I5(\caracter[3]_i_30_n_0 ),
        .O(\caracter[3]_i_16_n_0 ));
  LUT6 #(
    .INIT(64'h0000000010001110)) 
    \caracter[3]_i_17 
       (.I0(\caracter[3]_i_22_n_0 ),
        .I1(\caracter[3]_i_23_n_0 ),
        .I2(\string_cent_decenas[1]5__186_carry_0 ),
        .I3(\caracter[3]_i_31_n_0 ),
        .I4(\caracter[3]_i_32_n_0 ),
        .I5(\caracter[3]_i_25_n_0 ),
        .O(\caracter[3]_i_17_n_0 ));
  LUT6 #(
    .INIT(64'h7757575777777757)) 
    \caracter[3]_i_18 
       (.I0(\caracter[3]_i_25_n_0 ),
        .I1(\caracter[3]_i_22_n_0 ),
        .I2(\caracter[3]_i_23_n_0 ),
        .I3(\string_cent_decenas[1]5__186_carry_0 ),
        .I4(\caracter[3]_i_31_n_0 ),
        .I5(\caracter[3]_i_32_n_0 ),
        .O(\caracter[3]_i_18_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT4 #(
    .INIT(16'h444A)) 
    \caracter[3]_i_20 
       (.I0(\string_cent_decenas[1]5__186_carry_n_4 ),
        .I1(\string_cent_decenas[1]5__186_carry__0_n_7 ),
        .I2(\string_cent_decenas[1]5__186_carry_n_6 ),
        .I3(\string_cent_decenas[1]5__186_carry_n_5 ),
        .O(\string_cent_decenas[1]5__186_carry_0 ));
  LUT6 #(
    .INIT(64'hD24BB4D22DB44B2D)) 
    \caracter[3]_i_21 
       (.I0(\caracter[3]_i_33_n_0 ),
        .I1(\caracter[3]_i_34_n_0 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___169_carry_n_4 ),
        .I3(\caracter[3]_i_35_n_0 ),
        .I4(\string_cent_decenas[1]5_inferred__0/i___169_carry_n_5 ),
        .I5(\string_cent_decenas[1]5__186_carry_0 ),
        .O(\caracter[3]_i_21_n_0 ));
  LUT6 #(
    .INIT(64'h010101FFFE00FE00)) 
    \caracter[3]_i_22 
       (.I0(\string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_7 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___169_carry_n_5 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___169_carry_n_4 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_4 ),
        .I4(\string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_5 ),
        .I5(\string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_6 ),
        .O(\caracter[3]_i_22_n_0 ));
  LUT6 #(
    .INIT(64'hFFF1000AFFF5000A)) 
    \caracter[3]_i_23 
       (.I0(\string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_4 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_5 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___169_carry_n_4 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___169_carry_n_5 ),
        .I4(\string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_7 ),
        .I5(\string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_6 ),
        .O(\caracter[3]_i_23_n_0 ));
  LUT6 #(
    .INIT(64'h40D00D40F4FDDFF4)) 
    \caracter[3]_i_24 
       (.I0(\caracter[3]_i_34_n_0 ),
        .I1(\caracter[3]_i_33_n_0 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___169_carry_n_4 ),
        .I3(\caracter[3]_i_35_n_0 ),
        .I4(\string_cent_decenas[1]5_inferred__0/i___169_carry_n_5 ),
        .I5(\string_cent_decenas[1]5__186_carry_0 ),
        .O(\caracter[3]_i_24_n_0 ));
  LUT6 #(
    .INIT(64'h4A4A4A4A4A4A4AAA)) 
    \caracter[3]_i_25 
       (.I0(\string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_5 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_4 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_6 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_7 ),
        .I4(\string_cent_decenas[1]5_inferred__0/i___169_carry_n_5 ),
        .I5(\string_cent_decenas[1]5_inferred__0/i___169_carry_n_4 ),
        .O(\caracter[3]_i_25_n_0 ));
  LUT6 #(
    .INIT(64'h65569559AAAAAAAA)) 
    \caracter[3]_i_26 
       (.I0(\caracter[3]_i_36_n_0 ),
        .I1(\string_cent_decenas[1]5 [1]),
        .I2(\string_cent_decenas[1]5 [0]),
        .I3(\total_reg[0] ),
        .I4(\caracter[3]_i_37_n_0 ),
        .I5(\caracter[3]_i_17_n_0 ),
        .O(\caracter[3]_i_26_n_0 ));
  LUT6 #(
    .INIT(64'h6969966996969696)) 
    \caracter[3]_i_27 
       (.I0(\string_cent_decenas[1]5__186_carry_0 ),
        .I1(\caracter[3]_i_31_n_0 ),
        .I2(\caracter[3]_i_32_n_0 ),
        .I3(\caracter[3]_i_38_n_0 ),
        .I4(\caracter[3]_i_36_n_0 ),
        .I5(\caracter[3]_i_17_n_0 ),
        .O(\caracter[3]_i_27_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT5 #(
    .INIT(32'h99699699)) 
    \caracter[3]_i_28 
       (.I0(\string_cent_decenas[1]5 [1]),
        .I1(\caracter[3]_i_37_n_0 ),
        .I2(\string_cent_decenas[1]5 [0]),
        .I3(\total_reg[0] ),
        .I4(\caracter[3]_i_17_n_0 ),
        .O(\caracter[3]_i_28_n_0 ));
  LUT6 #(
    .INIT(64'h5FFAAAA0055FFAA9)) 
    \caracter[3]_i_29 
       (.I0(\caracter[3]_i_21_n_0 ),
        .I1(\caracter[3]_i_39_n_0 ),
        .I2(\caracter[3]_i_23_n_0 ),
        .I3(\caracter[3]_i_24_n_0 ),
        .I4(\caracter[3]_i_22_n_0 ),
        .I5(\caracter[3]_i_25_n_0 ),
        .O(\caracter[3]_i_29_n_0 ));
  LUT6 #(
    .INIT(64'h9969666666969999)) 
    \caracter[3]_i_30 
       (.I0(\caracter[3]_i_40_n_0 ),
        .I1(\caracter[3]_i_21_n_0 ),
        .I2(\caracter[3]_i_38_n_0 ),
        .I3(\caracter[3]_i_36_n_0 ),
        .I4(\caracter[3]_i_17_n_0 ),
        .I5(\caracter[3]_i_18_n_0 ),
        .O(\caracter[3]_i_30_n_0 ));
  LUT6 #(
    .INIT(64'h0FE10FE50FA50FA5)) 
    \caracter[3]_i_31 
       (.I0(\string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_4 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_5 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___169_carry_n_4 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___169_carry_n_5 ),
        .I4(\string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_7 ),
        .I5(\string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_6 ),
        .O(\caracter[3]_i_31_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFEF8AEF8A0000)) 
    \caracter[3]_i_32 
       (.I0(\string_cent_decenas[1]5 [1]),
        .I1(\string_cent_decenas[1]5 [0]),
        .I2(\total_reg[0] ),
        .I3(\caracter[3]_i_37_n_0 ),
        .I4(\caracter[3]_i_33_n_0 ),
        .I5(\caracter[3]_i_41_n_0 ),
        .O(\caracter[3]_i_32_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT4 #(
    .INIT(16'h6675)) 
    \caracter[3]_i_33 
       (.I0(\string_cent_decenas[1]5__186_carry_n_5 ),
        .I1(\string_cent_decenas[1]5__186_carry_n_6 ),
        .I2(\string_cent_decenas[1]5__186_carry_n_4 ),
        .I3(\string_cent_decenas[1]5__186_carry__0_n_7 ),
        .O(\caracter[3]_i_33_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT4 #(
    .INIT(16'h045D)) 
    \caracter[3]_i_34 
       (.I0(\caracter[3]_i_37_n_0 ),
        .I1(\total_reg[0] ),
        .I2(\string_cent_decenas[1]5 [0]),
        .I3(\string_cent_decenas[1]5 [1]),
        .O(\caracter[3]_i_34_n_0 ));
  LUT6 #(
    .INIT(64'hEEEEEEEAAAAAAAAA)) 
    \caracter[3]_i_35 
       (.I0(\string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_4 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_5 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___169_carry_n_4 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___169_carry_n_5 ),
        .I4(\string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_7 ),
        .I5(\string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_6 ),
        .O(\caracter[3]_i_35_n_0 ));
  LUT6 #(
    .INIT(64'hA6656559599A9AA6)) 
    \caracter[3]_i_36 
       (.I0(\caracter[3]_i_41_n_0 ),
        .I1(\string_cent_decenas[1]5 [1]),
        .I2(\caracter[3]_i_42_n_0 ),
        .I3(\caracter[3]_i_43_n_0 ),
        .I4(\string_cent_decenas[1]5__186_carry_n_6 ),
        .I5(\string_cent_decenas[1]5__186_carry_n_5 ),
        .O(\caracter[3]_i_36_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT4 #(
    .INIT(16'hCD99)) 
    \caracter[3]_i_37 
       (.I0(\string_cent_decenas[1]5__186_carry__0_n_7 ),
        .I1(\string_cent_decenas[1]5__186_carry_n_6 ),
        .I2(\string_cent_decenas[1]5__186_carry_n_5 ),
        .I3(\string_cent_decenas[1]5__186_carry_n_4 ),
        .O(\caracter[3]_i_37_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT4 #(
    .INIT(16'h4182)) 
    \caracter[3]_i_38 
       (.I0(\string_cent_decenas[1]5 [1]),
        .I1(\string_cent_decenas[1]5 [0]),
        .I2(\total_reg[0] ),
        .I3(\caracter[3]_i_37_n_0 ),
        .O(\caracter[3]_i_38_n_0 ));
  LUT6 #(
    .INIT(64'h1001400420028008)) 
    \caracter[3]_i_39 
       (.I0(\caracter[3]_i_33_n_0 ),
        .I1(\caracter[3]_i_37_n_0 ),
        .I2(\total_reg[0] ),
        .I3(\string_cent_decenas[1]5 [0]),
        .I4(\string_cent_decenas[1]5 [1]),
        .I5(\caracter[3]_i_41_n_0 ),
        .O(\caracter[3]_i_39_n_0 ));
  LUT6 #(
    .INIT(64'h00F0F0FFEE0E0E00)) 
    \caracter[3]_i_40 
       (.I0(\caracter[3]_i_25_n_0 ),
        .I1(\caracter[3]_i_22_n_0 ),
        .I2(\caracter[3]_i_32_n_0 ),
        .I3(\caracter[3]_i_31_n_0 ),
        .I4(\string_cent_decenas[1]5__186_carry_0 ),
        .I5(\caracter[3]_i_23_n_0 ),
        .O(\caracter[3]_i_40_n_0 ));
  LUT6 #(
    .INIT(64'h0F0F0F0F5A58F0F0)) 
    \caracter[3]_i_41 
       (.I0(\string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_6 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_7 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___169_carry_n_5 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___169_carry_n_4 ),
        .I4(\string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_5 ),
        .I5(\string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_4 ),
        .O(\caracter[3]_i_41_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \caracter[3]_i_42 
       (.I0(\total_reg[0] ),
        .I1(\string_cent_decenas[1]5 [0]),
        .O(\caracter[3]_i_42_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT4 #(
    .INIT(16'h0057)) 
    \caracter[3]_i_43 
       (.I0(\string_cent_decenas[1]5__186_carry_n_4 ),
        .I1(\string_cent_decenas[1]5__186_carry_n_5 ),
        .I2(\string_cent_decenas[1]5__186_carry_n_6 ),
        .I3(\string_cent_decenas[1]5__186_carry__0_n_7 ),
        .O(\caracter[3]_i_43_n_0 ));
  LUT5 #(
    .INIT(32'hF700FFFF)) 
    \caracter[3]_i_6 
       (.I0(\caracter[3]_i_14_n_0 ),
        .I1(\caracter[3]_i_15_n_0 ),
        .I2(\caracter[3]_i_16_n_0 ),
        .I3(\caracter[3]_i_17_n_0 ),
        .I4(\caracter[3]_i_18_n_0 ),
        .O(\disp_dinero[3]_1 [1]));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 euros1__169_carry
       (.CI(1'b0),
        .CO({euros1__169_carry_n_0,NLW_euros1__169_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({Q[0],1'b0,1'b0,1'b1}),
        .O({euros1__169_carry_n_4,euros1__169_carry_n_5,euros1__169_carry_n_6,NLW_euros1__169_carry_O_UNCONNECTED[0]}),
        .S({euros1__169_carry_i_1_n_0,euros1__169_carry_i_2_n_0,euros1__169_carry_i_3_n_0,Q[0]}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 euros1__169_carry__0
       (.CI(euros1__169_carry_n_0),
        .CO({euros1__169_carry__0_n_0,NLW_euros1__169_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[4:1]),
        .O({euros1__169_carry__0_n_4,euros1__169_carry__0_n_5,euros1__169_carry__0_n_6,euros1__169_carry__0_n_7}),
        .S({euros1__169_carry__0_i_1_n_0,euros1__169_carry__0_i_2_n_0,euros1__169_carry__0_i_3_n_0,euros1__169_carry__0_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    euros1__169_carry__0_i_1
       (.I0(Q[4]),
        .I1(Q[7]),
        .O(euros1__169_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    euros1__169_carry__0_i_2
       (.I0(Q[3]),
        .I1(Q[6]),
        .O(euros1__169_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    euros1__169_carry__0_i_3
       (.I0(Q[2]),
        .I1(Q[5]),
        .O(euros1__169_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    euros1__169_carry__0_i_4
       (.I0(Q[1]),
        .I1(Q[4]),
        .O(euros1__169_carry__0_i_4_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 euros1__169_carry__1
       (.CI(euros1__169_carry__0_n_0),
        .CO({euros1__169_carry__1_n_0,NLW_euros1__169_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[8:5]),
        .O({euros1__169_carry__1_n_4,euros1__169_carry__1_n_5,euros1__169_carry__1_n_6,euros1__169_carry__1_n_7}),
        .S({euros1__169_carry__1_i_1_n_0,euros1__169_carry__1_i_2_n_0,euros1__169_carry__1_i_3_n_0,euros1__169_carry__1_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    euros1__169_carry__1_i_1
       (.I0(Q[8]),
        .I1(Q[11]),
        .O(euros1__169_carry__1_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    euros1__169_carry__1_i_2
       (.I0(Q[7]),
        .I1(Q[10]),
        .O(euros1__169_carry__1_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    euros1__169_carry__1_i_3
       (.I0(Q[6]),
        .I1(Q[9]),
        .O(euros1__169_carry__1_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    euros1__169_carry__1_i_4
       (.I0(Q[5]),
        .I1(Q[8]),
        .O(euros1__169_carry__1_i_4_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 euros1__169_carry__2
       (.CI(euros1__169_carry__1_n_0),
        .CO({euros1__169_carry__2_n_0,NLW_euros1__169_carry__2_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[12:9]),
        .O({euros1__169_carry__2_n_4,euros1__169_carry__2_n_5,euros1__169_carry__2_n_6,euros1__169_carry__2_n_7}),
        .S({euros1__169_carry__2_i_1_n_0,euros1__169_carry__2_i_2_n_0,euros1__169_carry__2_i_3_n_0,euros1__169_carry__2_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    euros1__169_carry__2_i_1
       (.I0(Q[12]),
        .I1(Q[15]),
        .O(euros1__169_carry__2_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    euros1__169_carry__2_i_2
       (.I0(Q[11]),
        .I1(Q[14]),
        .O(euros1__169_carry__2_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    euros1__169_carry__2_i_3
       (.I0(Q[10]),
        .I1(Q[13]),
        .O(euros1__169_carry__2_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    euros1__169_carry__2_i_4
       (.I0(Q[9]),
        .I1(Q[12]),
        .O(euros1__169_carry__2_i_4_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 euros1__169_carry__3
       (.CI(euros1__169_carry__2_n_0),
        .CO({euros1__169_carry__3_n_0,NLW_euros1__169_carry__3_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[16:13]),
        .O({euros1__169_carry__3_n_4,euros1__169_carry__3_n_5,euros1__169_carry__3_n_6,euros1__169_carry__3_n_7}),
        .S({euros1__169_carry__3_i_1_n_0,euros1__169_carry__3_i_2_n_0,euros1__169_carry__3_i_3_n_0,euros1__169_carry__3_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    euros1__169_carry__3_i_1
       (.I0(Q[16]),
        .I1(Q[19]),
        .O(euros1__169_carry__3_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    euros1__169_carry__3_i_2
       (.I0(Q[15]),
        .I1(Q[18]),
        .O(euros1__169_carry__3_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    euros1__169_carry__3_i_3
       (.I0(Q[14]),
        .I1(Q[17]),
        .O(euros1__169_carry__3_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    euros1__169_carry__3_i_4
       (.I0(Q[13]),
        .I1(Q[16]),
        .O(euros1__169_carry__3_i_4_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 euros1__169_carry__4
       (.CI(euros1__169_carry__3_n_0),
        .CO(NLW_euros1__169_carry__4_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,Q[17]}),
        .O({NLW_euros1__169_carry__4_O_UNCONNECTED[3:2],euros1__169_carry__4_n_6,euros1__169_carry__4_n_7}),
        .S({1'b0,1'b0,euros1__169_carry__4_i_1_n_0,euros1__169_carry__4_i_2_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    euros1__169_carry__4_i_1
       (.I0(Q[18]),
        .I1(Q[21]),
        .O(euros1__169_carry__4_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    euros1__169_carry__4_i_2
       (.I0(Q[17]),
        .I1(Q[20]),
        .O(euros1__169_carry__4_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    euros1__169_carry_i_1
       (.I0(Q[0]),
        .I1(Q[3]),
        .O(euros1__169_carry_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    euros1__169_carry_i_2
       (.I0(Q[2]),
        .O(euros1__169_carry_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    euros1__169_carry_i_3
       (.I0(Q[1]),
        .O(euros1__169_carry_i_3_n_0));
  (* OPT_MODIFIED = "PROPCONST SWEEP" *) 
  CARRY4 euros1__1_carry
       (.CI(1'b0),
        .CO({euros1__1_carry_n_0,NLW_euros1__1_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({Q[4:2],1'b0}),
        .O(NLW_euros1__1_carry_O_UNCONNECTED[3:0]),
        .S({euros1__1_carry_i_1_n_0,euros1__1_carry_i_2_n_0,euros1__1_carry_i_3_n_0,Q[1]}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 euros1__1_carry__0
       (.CI(euros1__1_carry_n_0),
        .CO({euros1__1_carry__0_n_0,NLW_euros1__1_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({euros1__1_carry__0_i_1_n_0,Q[7:5]}),
        .O({euros1__1_carry__0_n_4,NLW_euros1__1_carry__0_O_UNCONNECTED[2:0]}),
        .S({euros1__1_carry__0_i_2_n_0,euros1__1_carry__0_i_3_n_0,euros1__1_carry__0_i_4_n_0,euros1__1_carry__0_i_5_n_0}));
  LUT3 #(
    .INIT(8'h96)) 
    euros1__1_carry__0_i_1
       (.I0(Q[8]),
        .I1(Q[6]),
        .I2(Q[1]),
        .O(euros1__1_carry__0_i_1_n_0));
  (* HLUTNM = "lutpair0" *) 
  LUT5 #(
    .INIT(32'h69969696)) 
    euros1__1_carry__0_i_2
       (.I0(Q[6]),
        .I1(Q[8]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(Q[5]),
        .O(euros1__1_carry__0_i_2_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    euros1__1_carry__0_i_3
       (.I0(Q[0]),
        .I1(Q[5]),
        .I2(Q[7]),
        .O(euros1__1_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    euros1__1_carry__0_i_4
       (.I0(Q[6]),
        .I1(Q[4]),
        .O(euros1__1_carry__0_i_4_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    euros1__1_carry__0_i_5
       (.I0(Q[5]),
        .I1(Q[3]),
        .O(euros1__1_carry__0_i_5_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 euros1__1_carry__1
       (.CI(euros1__1_carry__0_n_0),
        .CO({euros1__1_carry__1_n_0,NLW_euros1__1_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({euros1__1_carry__1_i_1_n_0,euros1__1_carry__1_i_2_n_0,euros1__1_carry__1_i_3_n_0,euros1__1_carry__1_i_4_n_0}),
        .O({euros1__1_carry__1_n_4,euros1__1_carry__1_n_5,euros1__1_carry__1_n_6,euros1__1_carry__1_n_7}),
        .S({euros1__1_carry__1_i_5_n_0,euros1__1_carry__1_i_6_n_0,euros1__1_carry__1_i_7_n_0,euros1__1_carry__1_i_8_n_0}));
  (* HLUTNM = "lutpair3" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__1_carry__1_i_1
       (.I0(Q[9]),
        .I1(Q[11]),
        .I2(Q[4]),
        .O(euros1__1_carry__1_i_1_n_0));
  (* HLUTNM = "lutpair2" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__1_carry__1_i_2
       (.I0(Q[8]),
        .I1(Q[10]),
        .I2(Q[3]),
        .O(euros1__1_carry__1_i_2_n_0));
  (* HLUTNM = "lutpair1" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__1_carry__1_i_3
       (.I0(Q[7]),
        .I1(Q[9]),
        .I2(Q[2]),
        .O(euros1__1_carry__1_i_3_n_0));
  (* HLUTNM = "lutpair0" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__1_carry__1_i_4
       (.I0(Q[6]),
        .I1(Q[8]),
        .I2(Q[1]),
        .O(euros1__1_carry__1_i_4_n_0));
  (* HLUTNM = "lutpair4" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__1_carry__1_i_5
       (.I0(Q[10]),
        .I1(Q[12]),
        .I2(Q[5]),
        .I3(euros1__1_carry__1_i_1_n_0),
        .O(euros1__1_carry__1_i_5_n_0));
  (* HLUTNM = "lutpair3" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__1_carry__1_i_6
       (.I0(Q[9]),
        .I1(Q[11]),
        .I2(Q[4]),
        .I3(euros1__1_carry__1_i_2_n_0),
        .O(euros1__1_carry__1_i_6_n_0));
  (* HLUTNM = "lutpair2" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__1_carry__1_i_7
       (.I0(Q[8]),
        .I1(Q[10]),
        .I2(Q[3]),
        .I3(euros1__1_carry__1_i_3_n_0),
        .O(euros1__1_carry__1_i_7_n_0));
  (* HLUTNM = "lutpair1" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__1_carry__1_i_8
       (.I0(Q[7]),
        .I1(Q[9]),
        .I2(Q[2]),
        .I3(euros1__1_carry__1_i_4_n_0),
        .O(euros1__1_carry__1_i_8_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 euros1__1_carry__2
       (.CI(euros1__1_carry__1_n_0),
        .CO({euros1__1_carry__2_n_0,NLW_euros1__1_carry__2_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({euros1__1_carry__2_i_1_n_0,euros1__1_carry__2_i_2_n_0,euros1__1_carry__2_i_3_n_0,euros1__1_carry__2_i_4_n_0}),
        .O({euros1__1_carry__2_n_4,euros1__1_carry__2_n_5,euros1__1_carry__2_n_6,euros1__1_carry__2_n_7}),
        .S({euros1__1_carry__2_i_5_n_0,euros1__1_carry__2_i_6_n_0,euros1__1_carry__2_i_7_n_0,euros1__1_carry__2_i_8_n_0}));
  (* HLUTNM = "lutpair7" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__1_carry__2_i_1
       (.I0(Q[8]),
        .I1(Q[15]),
        .I2(Q[13]),
        .O(euros1__1_carry__2_i_1_n_0));
  (* HLUTNM = "lutpair6" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__1_carry__2_i_2
       (.I0(Q[12]),
        .I1(Q[14]),
        .I2(Q[7]),
        .O(euros1__1_carry__2_i_2_n_0));
  (* HLUTNM = "lutpair5" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__1_carry__2_i_3
       (.I0(Q[6]),
        .I1(Q[13]),
        .I2(Q[11]),
        .O(euros1__1_carry__2_i_3_n_0));
  (* HLUTNM = "lutpair4" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__1_carry__2_i_4
       (.I0(Q[10]),
        .I1(Q[12]),
        .I2(Q[5]),
        .O(euros1__1_carry__2_i_4_n_0));
  (* HLUTNM = "lutpair8" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__1_carry__2_i_5
       (.I0(Q[14]),
        .I1(Q[16]),
        .I2(Q[9]),
        .I3(euros1__1_carry__2_i_1_n_0),
        .O(euros1__1_carry__2_i_5_n_0));
  (* HLUTNM = "lutpair7" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__1_carry__2_i_6
       (.I0(Q[8]),
        .I1(Q[15]),
        .I2(Q[13]),
        .I3(euros1__1_carry__2_i_2_n_0),
        .O(euros1__1_carry__2_i_6_n_0));
  (* HLUTNM = "lutpair6" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__1_carry__2_i_7
       (.I0(Q[12]),
        .I1(Q[14]),
        .I2(Q[7]),
        .I3(euros1__1_carry__2_i_3_n_0),
        .O(euros1__1_carry__2_i_7_n_0));
  (* HLUTNM = "lutpair5" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__1_carry__2_i_8
       (.I0(Q[6]),
        .I1(Q[13]),
        .I2(Q[11]),
        .I3(euros1__1_carry__2_i_4_n_0),
        .O(euros1__1_carry__2_i_8_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 euros1__1_carry__3
       (.CI(euros1__1_carry__2_n_0),
        .CO({euros1__1_carry__3_n_0,NLW_euros1__1_carry__3_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({euros1__1_carry__3_i_1_n_0,euros1__1_carry__3_i_2_n_0,euros1__1_carry__3_i_3_n_0,euros1__1_carry__3_i_4_n_0}),
        .O({euros1__1_carry__3_n_4,euros1__1_carry__3_n_5,euros1__1_carry__3_n_6,euros1__1_carry__3_n_7}),
        .S({euros1__1_carry__3_i_5_n_0,euros1__1_carry__3_i_6_n_0,euros1__1_carry__3_i_7_n_0,euros1__1_carry__3_i_8_n_0}));
  (* HLUTNM = "lutpair11" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__1_carry__3_i_1
       (.I0(Q[12]),
        .I1(Q[19]),
        .I2(Q[17]),
        .O(euros1__1_carry__3_i_1_n_0));
  (* HLUTNM = "lutpair10" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__1_carry__3_i_2
       (.I0(Q[16]),
        .I1(Q[18]),
        .I2(Q[11]),
        .O(euros1__1_carry__3_i_2_n_0));
  (* HLUTNM = "lutpair9" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__1_carry__3_i_3
       (.I0(Q[10]),
        .I1(Q[17]),
        .I2(Q[15]),
        .O(euros1__1_carry__3_i_3_n_0));
  (* HLUTNM = "lutpair8" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__1_carry__3_i_4
       (.I0(Q[14]),
        .I1(Q[16]),
        .I2(Q[9]),
        .O(euros1__1_carry__3_i_4_n_0));
  (* HLUTNM = "lutpair12" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__1_carry__3_i_5
       (.I0(Q[18]),
        .I1(Q[20]),
        .I2(Q[13]),
        .I3(euros1__1_carry__3_i_1_n_0),
        .O(euros1__1_carry__3_i_5_n_0));
  (* HLUTNM = "lutpair11" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__1_carry__3_i_6
       (.I0(Q[12]),
        .I1(Q[19]),
        .I2(Q[17]),
        .I3(euros1__1_carry__3_i_2_n_0),
        .O(euros1__1_carry__3_i_6_n_0));
  (* HLUTNM = "lutpair10" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__1_carry__3_i_7
       (.I0(Q[16]),
        .I1(Q[18]),
        .I2(Q[11]),
        .I3(euros1__1_carry__3_i_3_n_0),
        .O(euros1__1_carry__3_i_7_n_0));
  (* HLUTNM = "lutpair9" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__1_carry__3_i_8
       (.I0(Q[10]),
        .I1(Q[17]),
        .I2(Q[15]),
        .I3(euros1__1_carry__3_i_4_n_0),
        .O(euros1__1_carry__3_i_8_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 euros1__1_carry__4
       (.CI(euros1__1_carry__3_n_0),
        .CO({euros1__1_carry__4_n_0,NLW_euros1__1_carry__4_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({euros1__1_carry__4_i_1_n_0,euros1__1_carry__4_i_2_n_0,euros1__1_carry__4_i_3_n_0,euros1__1_carry__4_i_4_n_0}),
        .O({euros1__1_carry__4_n_4,euros1__1_carry__4_n_5,euros1__1_carry__4_n_6,euros1__1_carry__4_n_7}),
        .S({euros1__1_carry__4_i_5_n_0,euros1__1_carry__4_i_6_n_0,euros1__1_carry__4_i_7_n_0,euros1__1_carry__4_i_8_n_0}));
  (* HLUTNM = "lutpair15" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__1_carry__4_i_1
       (.I0(Q[16]),
        .I1(Q[23]),
        .I2(Q[21]),
        .O(euros1__1_carry__4_i_1_n_0));
  (* HLUTNM = "lutpair14" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__1_carry__4_i_2
       (.I0(Q[20]),
        .I1(Q[22]),
        .I2(Q[15]),
        .O(euros1__1_carry__4_i_2_n_0));
  (* HLUTNM = "lutpair13" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__1_carry__4_i_3
       (.I0(Q[14]),
        .I1(Q[21]),
        .I2(Q[19]),
        .O(euros1__1_carry__4_i_3_n_0));
  (* HLUTNM = "lutpair12" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__1_carry__4_i_4
       (.I0(Q[18]),
        .I1(Q[20]),
        .I2(Q[13]),
        .O(euros1__1_carry__4_i_4_n_0));
  (* HLUTNM = "lutpair16" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__1_carry__4_i_5
       (.I0(Q[17]),
        .I1(Q[24]),
        .I2(Q[22]),
        .I3(euros1__1_carry__4_i_1_n_0),
        .O(euros1__1_carry__4_i_5_n_0));
  (* HLUTNM = "lutpair15" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__1_carry__4_i_6
       (.I0(Q[16]),
        .I1(Q[23]),
        .I2(Q[21]),
        .I3(euros1__1_carry__4_i_2_n_0),
        .O(euros1__1_carry__4_i_6_n_0));
  (* HLUTNM = "lutpair14" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__1_carry__4_i_7
       (.I0(Q[20]),
        .I1(Q[22]),
        .I2(Q[15]),
        .I3(euros1__1_carry__4_i_3_n_0),
        .O(euros1__1_carry__4_i_7_n_0));
  (* HLUTNM = "lutpair13" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__1_carry__4_i_8
       (.I0(Q[14]),
        .I1(Q[21]),
        .I2(Q[19]),
        .I3(euros1__1_carry__4_i_4_n_0),
        .O(euros1__1_carry__4_i_8_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 euros1__1_carry__5
       (.CI(euros1__1_carry__4_n_0),
        .CO({euros1__1_carry__5_n_0,NLW_euros1__1_carry__5_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({euros1__1_carry__5_i_1_n_0,euros1__1_carry__5_i_2_n_0,euros1__1_carry__5_i_3_n_0,euros1__1_carry__5_i_4_n_0}),
        .O({euros1__1_carry__5_n_4,euros1__1_carry__5_n_5,euros1__1_carry__5_n_6,euros1__1_carry__5_n_7}),
        .S({euros1__1_carry__5_i_5_n_0,euros1__1_carry__5_i_6_n_0,euros1__1_carry__5_i_7_n_0,euros1__1_carry__5_i_8_n_0}));
  (* HLUTNM = "lutpair19" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__1_carry__5_i_1
       (.I0(Q[20]),
        .I1(Q[27]),
        .I2(Q[25]),
        .O(euros1__1_carry__5_i_1_n_0));
  (* HLUTNM = "lutpair18" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__1_carry__5_i_2
       (.I0(Q[19]),
        .I1(Q[26]),
        .I2(Q[24]),
        .O(euros1__1_carry__5_i_2_n_0));
  (* HLUTNM = "lutpair17" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__1_carry__5_i_3
       (.I0(Q[18]),
        .I1(Q[25]),
        .I2(Q[23]),
        .O(euros1__1_carry__5_i_3_n_0));
  (* HLUTNM = "lutpair16" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__1_carry__5_i_4
       (.I0(Q[17]),
        .I1(Q[24]),
        .I2(Q[22]),
        .O(euros1__1_carry__5_i_4_n_0));
  (* HLUTNM = "lutpair20" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__1_carry__5_i_5
       (.I0(Q[21]),
        .I1(Q[28]),
        .I2(Q[26]),
        .I3(euros1__1_carry__5_i_1_n_0),
        .O(euros1__1_carry__5_i_5_n_0));
  (* HLUTNM = "lutpair19" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__1_carry__5_i_6
       (.I0(Q[20]),
        .I1(Q[27]),
        .I2(Q[25]),
        .I3(euros1__1_carry__5_i_2_n_0),
        .O(euros1__1_carry__5_i_6_n_0));
  (* HLUTNM = "lutpair18" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__1_carry__5_i_7
       (.I0(Q[19]),
        .I1(Q[26]),
        .I2(Q[24]),
        .I3(euros1__1_carry__5_i_3_n_0),
        .O(euros1__1_carry__5_i_7_n_0));
  (* HLUTNM = "lutpair17" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__1_carry__5_i_8
       (.I0(Q[18]),
        .I1(Q[25]),
        .I2(Q[23]),
        .I3(euros1__1_carry__5_i_4_n_0),
        .O(euros1__1_carry__5_i_8_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 euros1__1_carry__6
       (.CI(euros1__1_carry__5_n_0),
        .CO({euros1__1_carry__6_n_0,NLW_euros1__1_carry__6_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({euros1__1_carry__6_i_1_n_0,euros1__1_carry__6_i_2_n_0,euros1__1_carry__6_i_3_n_0,euros1__1_carry__6_i_4_n_0}),
        .O({euros1__1_carry__6_n_4,euros1__1_carry__6_n_5,euros1__1_carry__6_n_6,euros1__1_carry__6_n_7}),
        .S({euros1__1_carry__6_i_5_n_0,euros1__1_carry__6_i_6_n_0,euros1__1_carry__6_i_7_n_0,euros1__1_carry__6_i_8_n_0}));
  LUT2 #(
    .INIT(4'h8)) 
    euros1__1_carry__6_i_1
       (.I0(Q[24]),
        .I1(Q[29]),
        .O(euros1__1_carry__6_i_1_n_0));
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__1_carry__6_i_2
       (.I0(Q[23]),
        .I1(Q[30]),
        .I2(Q[28]),
        .O(euros1__1_carry__6_i_2_n_0));
  (* HLUTNM = "lutpair21" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__1_carry__6_i_3
       (.I0(Q[22]),
        .I1(Q[29]),
        .I2(Q[27]),
        .O(euros1__1_carry__6_i_3_n_0));
  (* HLUTNM = "lutpair20" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__1_carry__6_i_4
       (.I0(Q[21]),
        .I1(Q[28]),
        .I2(Q[26]),
        .O(euros1__1_carry__6_i_4_n_0));
  LUT4 #(
    .INIT(16'h8778)) 
    euros1__1_carry__6_i_5
       (.I0(Q[29]),
        .I1(Q[24]),
        .I2(Q[30]),
        .I3(Q[25]),
        .O(euros1__1_carry__6_i_5_n_0));
  LUT5 #(
    .INIT(32'hE81717E8)) 
    euros1__1_carry__6_i_6
       (.I0(Q[28]),
        .I1(Q[30]),
        .I2(Q[23]),
        .I3(Q[29]),
        .I4(Q[24]),
        .O(euros1__1_carry__6_i_6_n_0));
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__1_carry__6_i_7
       (.I0(euros1__1_carry__6_i_3_n_0),
        .I1(Q[23]),
        .I2(Q[30]),
        .I3(Q[28]),
        .O(euros1__1_carry__6_i_7_n_0));
  (* HLUTNM = "lutpair21" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__1_carry__6_i_8
       (.I0(Q[22]),
        .I1(Q[29]),
        .I2(Q[27]),
        .I3(euros1__1_carry__6_i_4_n_0),
        .O(euros1__1_carry__6_i_8_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 euros1__1_carry__7
       (.CI(euros1__1_carry__6_n_0),
        .CO(NLW_euros1__1_carry__7_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,Q[26]}),
        .O({NLW_euros1__1_carry__7_O_UNCONNECTED[3:2],euros1__1_carry__7_n_6,euros1__1_carry__7_n_7}),
        .S({1'b0,1'b0,Q[27],euros1__1_carry__7_i_1_n_0}));
  LUT3 #(
    .INIT(8'h78)) 
    euros1__1_carry__7_i_1
       (.I0(Q[30]),
        .I1(Q[25]),
        .I2(Q[26]),
        .O(euros1__1_carry__7_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    euros1__1_carry_i_1
       (.I0(Q[4]),
        .I1(Q[2]),
        .O(euros1__1_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    euros1__1_carry_i_2
       (.I0(Q[3]),
        .I1(Q[1]),
        .O(euros1__1_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    euros1__1_carry_i_3
       (.I0(Q[2]),
        .I1(Q[0]),
        .O(euros1__1_carry_i_3_n_0));
  (* OPT_MODIFIED = "PROPCONST SWEEP" *) 
  CARRY4 euros1__231_carry
       (.CI(1'b0),
        .CO({euros1__231_carry_n_0,NLW_euros1__231_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({Q[6:4],1'b0}),
        .O({euros1__231_carry_n_4,euros1__231_carry_n_5,euros1__231_carry_n_6,euros1__231_carry_n_7}),
        .S({euros1__231_carry_i_1_n_0,euros1__231_carry_i_2_n_0,euros1__231_carry_i_3_n_0,Q[3]}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 euros1__231_carry__0
       (.CI(euros1__231_carry_n_0),
        .CO({euros1__231_carry__0_n_0,NLW_euros1__231_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({euros1__231_carry__0_i_1_n_0,euros1__231_carry__0_i_2_n_0,euros1__231_carry__0_i_3_n_0,euros1__231_carry__0_i_4_n_0}),
        .O({euros1__231_carry__0_n_4,euros1__231_carry__0_n_5,euros1__231_carry__0_n_6,euros1__231_carry__0_n_7}),
        .S({euros1__231_carry__0_i_5_n_0,euros1__231_carry__0_i_6_n_0,euros1__231_carry__0_i_7_n_0,euros1__231_carry__0_i_8_n_0}));
  (* HLUTNM = "lutpair41" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__231_carry__0_i_1
       (.I0(Q[5]),
        .I1(Q[3]),
        .I2(Q[9]),
        .O(euros1__231_carry__0_i_1_n_0));
  (* HLUTNM = "lutpair40" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__231_carry__0_i_2
       (.I0(Q[2]),
        .I1(Q[4]),
        .I2(Q[8]),
        .O(euros1__231_carry__0_i_2_n_0));
  (* HLUTNM = "lutpair39" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__231_carry__0_i_3
       (.I0(Q[1]),
        .I1(Q[3]),
        .I2(Q[7]),
        .O(euros1__231_carry__0_i_3_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    euros1__231_carry__0_i_4
       (.I0(Q[7]),
        .I1(Q[1]),
        .I2(Q[3]),
        .O(euros1__231_carry__0_i_4_n_0));
  (* HLUTNM = "lutpair42" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__231_carry__0_i_5
       (.I0(Q[6]),
        .I1(Q[4]),
        .I2(Q[10]),
        .I3(euros1__231_carry__0_i_1_n_0),
        .O(euros1__231_carry__0_i_5_n_0));
  (* HLUTNM = "lutpair41" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__231_carry__0_i_6
       (.I0(Q[5]),
        .I1(Q[3]),
        .I2(Q[9]),
        .I3(euros1__231_carry__0_i_2_n_0),
        .O(euros1__231_carry__0_i_6_n_0));
  (* HLUTNM = "lutpair40" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__231_carry__0_i_7
       (.I0(Q[2]),
        .I1(Q[4]),
        .I2(Q[8]),
        .I3(euros1__231_carry__0_i_3_n_0),
        .O(euros1__231_carry__0_i_7_n_0));
  (* HLUTNM = "lutpair39" *) 
  LUT5 #(
    .INIT(32'h69969696)) 
    euros1__231_carry__0_i_8
       (.I0(Q[1]),
        .I1(Q[3]),
        .I2(Q[7]),
        .I3(Q[2]),
        .I4(Q[0]),
        .O(euros1__231_carry__0_i_8_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 euros1__231_carry__1
       (.CI(euros1__231_carry__0_n_0),
        .CO({euros1__231_carry__1_n_0,NLW_euros1__231_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({euros1__231_carry__1_i_1_n_0,euros1__231_carry__1_i_2_n_0,euros1__231_carry__1_i_3_n_0,euros1__231_carry__1_i_4_n_0}),
        .O({euros1__231_carry__1_n_4,euros1__231_carry__1_n_5,euros1__231_carry__1_n_6,euros1__231_carry__1_n_7}),
        .S({euros1__231_carry__1_i_5_n_0,euros1__231_carry__1_i_6_n_0,euros1__231_carry__1_i_7_n_0,euros1__231_carry__1_i_8_n_0}));
  (* HLUTNM = "lutpair45" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__231_carry__1_i_1
       (.I0(Q[7]),
        .I1(Q[9]),
        .I2(Q[13]),
        .O(euros1__231_carry__1_i_1_n_0));
  (* HLUTNM = "lutpair44" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__231_carry__1_i_2
       (.I0(Q[6]),
        .I1(Q[8]),
        .I2(Q[12]),
        .O(euros1__231_carry__1_i_2_n_0));
  (* HLUTNM = "lutpair43" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__231_carry__1_i_3
       (.I0(Q[7]),
        .I1(Q[11]),
        .I2(Q[5]),
        .O(euros1__231_carry__1_i_3_n_0));
  (* HLUTNM = "lutpair42" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__231_carry__1_i_4
       (.I0(Q[6]),
        .I1(Q[4]),
        .I2(Q[10]),
        .O(euros1__231_carry__1_i_4_n_0));
  (* HLUTNM = "lutpair46" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__231_carry__1_i_5
       (.I0(Q[14]),
        .I1(Q[10]),
        .I2(Q[8]),
        .I3(euros1__231_carry__1_i_1_n_0),
        .O(euros1__231_carry__1_i_5_n_0));
  (* HLUTNM = "lutpair45" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__231_carry__1_i_6
       (.I0(Q[7]),
        .I1(Q[9]),
        .I2(Q[13]),
        .I3(euros1__231_carry__1_i_2_n_0),
        .O(euros1__231_carry__1_i_6_n_0));
  (* HLUTNM = "lutpair44" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__231_carry__1_i_7
       (.I0(Q[6]),
        .I1(Q[8]),
        .I2(Q[12]),
        .I3(euros1__231_carry__1_i_3_n_0),
        .O(euros1__231_carry__1_i_7_n_0));
  (* HLUTNM = "lutpair43" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__231_carry__1_i_8
       (.I0(Q[7]),
        .I1(Q[11]),
        .I2(Q[5]),
        .I3(euros1__231_carry__1_i_4_n_0),
        .O(euros1__231_carry__1_i_8_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 euros1__231_carry__2
       (.CI(euros1__231_carry__1_n_0),
        .CO(NLW_euros1__231_carry__2_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,euros1__231_carry__2_i_1_n_0,euros1__231_carry__2_i_2_n_0,euros1__231_carry__2_i_3_n_0}),
        .O({euros1__231_carry__2_n_4,euros1__231_carry__2_n_5,euros1__231_carry__2_n_6,euros1__231_carry__2_n_7}),
        .S({euros1__231_carry__2_i_4_n_0,euros1__231_carry__2_i_5_n_0,euros1__231_carry__2_i_6_n_0,euros1__231_carry__2_i_7_n_0}));
  (* HLUTNM = "lutpair48" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__231_carry__2_i_1
       (.I0(Q[10]),
        .I1(Q[12]),
        .I2(Q[16]),
        .O(euros1__231_carry__2_i_1_n_0));
  (* HLUTNM = "lutpair47" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__231_carry__2_i_2
       (.I0(Q[15]),
        .I1(Q[11]),
        .I2(Q[9]),
        .O(euros1__231_carry__2_i_2_n_0));
  (* HLUTNM = "lutpair46" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__231_carry__2_i_3
       (.I0(Q[14]),
        .I1(Q[10]),
        .I2(Q[8]),
        .O(euros1__231_carry__2_i_3_n_0));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    euros1__231_carry__2_i_4
       (.I0(Q[17]),
        .I1(Q[13]),
        .I2(Q[11]),
        .I3(Q[12]),
        .I4(Q[14]),
        .I5(Q[18]),
        .O(euros1__231_carry__2_i_4_n_0));
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__231_carry__2_i_5
       (.I0(euros1__231_carry__2_i_1_n_0),
        .I1(Q[11]),
        .I2(Q[13]),
        .I3(Q[17]),
        .O(euros1__231_carry__2_i_5_n_0));
  (* HLUTNM = "lutpair48" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__231_carry__2_i_6
       (.I0(Q[10]),
        .I1(Q[12]),
        .I2(Q[16]),
        .I3(euros1__231_carry__2_i_2_n_0),
        .O(euros1__231_carry__2_i_6_n_0));
  (* HLUTNM = "lutpair47" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__231_carry__2_i_7
       (.I0(Q[15]),
        .I1(Q[11]),
        .I2(Q[9]),
        .I3(euros1__231_carry__2_i_3_n_0),
        .O(euros1__231_carry__2_i_7_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    euros1__231_carry_i_1
       (.I0(Q[0]),
        .I1(Q[2]),
        .I2(Q[6]),
        .O(euros1__231_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    euros1__231_carry_i_2
       (.I0(Q[5]),
        .I1(Q[1]),
        .O(euros1__231_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    euros1__231_carry_i_3
       (.I0(Q[4]),
        .I1(Q[0]),
        .O(euros1__231_carry_i_3_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 euros1__276_carry
       (.CI(1'b0),
        .CO({euros1__276_carry_n_0,NLW_euros1__276_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({euros1__276_carry_i_1_n_0,euros1__276_carry_i_2_n_0,euros1__276_carry_i_3_n_0,euros1__276_carry_i_4_n_0}),
        .O(NLW_euros1__276_carry_O_UNCONNECTED[3:0]),
        .S({euros1__276_carry_i_5_n_0,euros1__276_carry_i_6_n_0,euros1__276_carry_i_7_n_0,euros1__276_carry_i_8_n_0}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 euros1__276_carry__0
       (.CI(euros1__276_carry_n_0),
        .CO({euros1__276_carry__0_n_0,NLW_euros1__276_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({euros1__276_carry__0_i_1_n_0,euros1__276_carry__0_i_2_n_0,euros1__276_carry__0_i_3_n_0,euros1__276_carry__0_i_4_n_0}),
        .O(NLW_euros1__276_carry__0_O_UNCONNECTED[3:0]),
        .S({euros1__276_carry__0_i_5_n_0,euros1__276_carry__0_i_6_n_0,euros1__276_carry__0_i_7_n_0,euros1__276_carry__0_i_8_n_0}));
  LUT5 #(
    .INIT(32'hBE282828)) 
    euros1__276_carry__0_i_1
       (.I0(euros1__1_carry__2_n_5),
        .I1(euros1__92_carry__0_n_4),
        .I2(euros1__169_carry_n_5),
        .I3(euros1__169_carry_n_6),
        .I4(euros1__92_carry__0_n_5),
        .O(euros1__276_carry__0_i_1_n_0));
  LUT5 #(
    .INIT(32'hBE282828)) 
    euros1__276_carry__0_i_2
       (.I0(euros1__1_carry__2_n_6),
        .I1(euros1__92_carry__0_n_5),
        .I2(euros1__169_carry_n_6),
        .I3(euros1__92_carry_n_7),
        .I4(euros1__92_carry__0_n_6),
        .O(euros1__276_carry__0_i_2_n_0));
  (* HLUTNM = "lutpair50" *) 
  LUT3 #(
    .INIT(8'h28)) 
    euros1__276_carry__0_i_3
       (.I0(euros1__1_carry__2_n_7),
        .I1(euros1__92_carry__0_n_6),
        .I2(euros1__92_carry_n_7),
        .O(euros1__276_carry__0_i_3_n_0));
  (* HLUTNM = "lutpair49" *) 
  LUT2 #(
    .INIT(4'h8)) 
    euros1__276_carry__0_i_4
       (.I0(euros1__1_carry__1_n_4),
        .I1(euros1__92_carry__0_n_7),
        .O(euros1__276_carry__0_i_4_n_0));
  LUT5 #(
    .INIT(32'h69969696)) 
    euros1__276_carry__0_i_5
       (.I0(euros1__276_carry__0_i_1_n_0),
        .I1(euros1__1_carry__2_n_4),
        .I2(euros1__276_carry__0_i_9_n_0),
        .I3(euros1__169_carry_n_5),
        .I4(euros1__92_carry__0_n_4),
        .O(euros1__276_carry__0_i_5_n_0));
  LUT6 #(
    .INIT(64'h9669699669966996)) 
    euros1__276_carry__0_i_6
       (.I0(euros1__276_carry__0_i_2_n_0),
        .I1(euros1__1_carry__2_n_5),
        .I2(euros1__92_carry__0_n_4),
        .I3(euros1__169_carry_n_5),
        .I4(euros1__169_carry_n_6),
        .I5(euros1__92_carry__0_n_5),
        .O(euros1__276_carry__0_i_6_n_0));
  LUT6 #(
    .INIT(64'h9669699669966996)) 
    euros1__276_carry__0_i_7
       (.I0(euros1__276_carry__0_i_3_n_0),
        .I1(euros1__1_carry__2_n_6),
        .I2(euros1__92_carry__0_n_5),
        .I3(euros1__169_carry_n_6),
        .I4(euros1__92_carry_n_7),
        .I5(euros1__92_carry__0_n_6),
        .O(euros1__276_carry__0_i_7_n_0));
  (* HLUTNM = "lutpair50" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__276_carry__0_i_8
       (.I0(euros1__1_carry__2_n_7),
        .I1(euros1__92_carry__0_n_6),
        .I2(euros1__92_carry_n_7),
        .I3(euros1__276_carry__0_i_4_n_0),
        .O(euros1__276_carry__0_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'h96)) 
    euros1__276_carry__0_i_9
       (.I0(euros1__169_carry_n_4),
        .I1(euros1__92_carry__1_n_7),
        .I2(Q[0]),
        .O(euros1__276_carry__0_i_9_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 euros1__276_carry__1
       (.CI(euros1__276_carry__0_n_0),
        .CO({euros1__276_carry__1_n_0,NLW_euros1__276_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({euros1__276_carry__1_i_1_n_0,euros1__276_carry__1_i_2_n_0,euros1__276_carry__1_i_3_n_0,euros1__276_carry__1_i_4_n_0}),
        .O(NLW_euros1__276_carry__1_O_UNCONNECTED[3:0]),
        .S({euros1__276_carry__1_i_5_n_0,euros1__276_carry__1_i_6_n_0,euros1__276_carry__1_i_7_n_0,euros1__276_carry__1_i_8_n_0}));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    euros1__276_carry__1_i_1
       (.I0(Q[2]),
        .I1(euros1__92_carry__1_n_5),
        .I2(euros1__169_carry__0_n_6),
        .I3(euros1__1_carry__3_n_5),
        .I4(euros1__276_carry__1_i_9_n_0),
        .O(euros1__276_carry__1_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'h96)) 
    euros1__276_carry__1_i_10
       (.I0(euros1__169_carry__0_n_6),
        .I1(euros1__92_carry__1_n_5),
        .I2(Q[2]),
        .O(euros1__276_carry__1_i_10_n_0));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'h96)) 
    euros1__276_carry__1_i_11
       (.I0(euros1__169_carry__0_n_7),
        .I1(euros1__92_carry__1_n_6),
        .I2(Q[1]),
        .O(euros1__276_carry__1_i_11_n_0));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT3 #(
    .INIT(8'h17)) 
    euros1__276_carry__1_i_12
       (.I0(euros1__231_carry_n_7),
        .I1(euros1__92_carry__1_n_4),
        .I2(euros1__169_carry__0_n_5),
        .O(euros1__276_carry__1_i_12_n_0));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'h17)) 
    euros1__276_carry__1_i_13
       (.I0(Q[2]),
        .I1(euros1__92_carry__1_n_5),
        .I2(euros1__169_carry__0_n_6),
        .O(euros1__276_carry__1_i_13_n_0));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'h17)) 
    euros1__276_carry__1_i_14
       (.I0(Q[1]),
        .I1(euros1__92_carry__1_n_6),
        .I2(euros1__169_carry__0_n_7),
        .O(euros1__276_carry__1_i_14_n_0));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'h17)) 
    euros1__276_carry__1_i_15
       (.I0(Q[0]),
        .I1(euros1__92_carry__1_n_7),
        .I2(euros1__169_carry_n_4),
        .O(euros1__276_carry__1_i_15_n_0));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    euros1__276_carry__1_i_2
       (.I0(Q[1]),
        .I1(euros1__92_carry__1_n_6),
        .I2(euros1__169_carry__0_n_7),
        .I3(euros1__1_carry__3_n_6),
        .I4(euros1__276_carry__1_i_10_n_0),
        .O(euros1__276_carry__1_i_2_n_0));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    euros1__276_carry__1_i_3
       (.I0(Q[0]),
        .I1(euros1__92_carry__1_n_7),
        .I2(euros1__169_carry_n_4),
        .I3(euros1__1_carry__3_n_7),
        .I4(euros1__276_carry__1_i_11_n_0),
        .O(euros1__276_carry__1_i_3_n_0));
  LUT6 #(
    .INIT(64'hEBBE822882288228)) 
    euros1__276_carry__1_i_4
       (.I0(euros1__1_carry__2_n_4),
        .I1(Q[0]),
        .I2(euros1__92_carry__1_n_7),
        .I3(euros1__169_carry_n_4),
        .I4(euros1__169_carry_n_5),
        .I5(euros1__92_carry__0_n_4),
        .O(euros1__276_carry__1_i_4_n_0));
  LUT6 #(
    .INIT(64'h9669699669969669)) 
    euros1__276_carry__1_i_5
       (.I0(euros1__276_carry__1_i_1_n_0),
        .I1(euros1__276_carry__1_i_12_n_0),
        .I2(euros1__169_carry__0_n_4),
        .I3(euros1__92_carry__2_n_7),
        .I4(euros1__231_carry_n_6),
        .I5(euros1__1_carry__3_n_4),
        .O(euros1__276_carry__1_i_5_n_0));
  LUT6 #(
    .INIT(64'h9669699669969669)) 
    euros1__276_carry__1_i_6
       (.I0(euros1__276_carry__1_i_2_n_0),
        .I1(euros1__276_carry__1_i_13_n_0),
        .I2(euros1__169_carry__0_n_5),
        .I3(euros1__92_carry__1_n_4),
        .I4(euros1__231_carry_n_7),
        .I5(euros1__1_carry__3_n_5),
        .O(euros1__276_carry__1_i_6_n_0));
  LUT6 #(
    .INIT(64'h9669699669969669)) 
    euros1__276_carry__1_i_7
       (.I0(euros1__276_carry__1_i_3_n_0),
        .I1(euros1__276_carry__1_i_14_n_0),
        .I2(euros1__169_carry__0_n_6),
        .I3(euros1__92_carry__1_n_5),
        .I4(Q[2]),
        .I5(euros1__1_carry__3_n_6),
        .O(euros1__276_carry__1_i_7_n_0));
  LUT6 #(
    .INIT(64'h9669699669969669)) 
    euros1__276_carry__1_i_8
       (.I0(euros1__276_carry__1_i_4_n_0),
        .I1(euros1__276_carry__1_i_15_n_0),
        .I2(euros1__169_carry__0_n_7),
        .I3(euros1__92_carry__1_n_6),
        .I4(Q[1]),
        .I5(euros1__1_carry__3_n_7),
        .O(euros1__276_carry__1_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT3 #(
    .INIT(8'h96)) 
    euros1__276_carry__1_i_9
       (.I0(euros1__169_carry__0_n_5),
        .I1(euros1__92_carry__1_n_4),
        .I2(euros1__231_carry_n_7),
        .O(euros1__276_carry__1_i_9_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 euros1__276_carry__2
       (.CI(euros1__276_carry__1_n_0),
        .CO({euros1__276_carry__2_n_0,NLW_euros1__276_carry__2_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({euros1__276_carry__2_i_1_n_0,euros1__276_carry__2_i_2_n_0,euros1__276_carry__2_i_3_n_0,euros1__276_carry__2_i_4_n_0}),
        .O(NLW_euros1__276_carry__2_O_UNCONNECTED[3:0]),
        .S({euros1__276_carry__2_i_5_n_0,euros1__276_carry__2_i_6_n_0,euros1__276_carry__2_i_7_n_0,euros1__276_carry__2_i_8_n_0}));
  LUT5 #(
    .INIT(32'hEEE8E888)) 
    euros1__276_carry__2_i_1
       (.I0(euros1__1_carry__4_n_5),
        .I1(euros1__276_carry__2_i_9_n_0),
        .I2(euros1__231_carry_n_4),
        .I3(euros1__92_carry__2_n_5),
        .I4(euros1__169_carry__1_n_6),
        .O(euros1__276_carry__2_i_1_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    euros1__276_carry__2_i_10
       (.I0(euros1__169_carry__1_n_6),
        .I1(euros1__92_carry__2_n_5),
        .I2(euros1__231_carry_n_4),
        .O(euros1__276_carry__2_i_10_n_0));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT3 #(
    .INIT(8'h96)) 
    euros1__276_carry__2_i_11
       (.I0(euros1__169_carry__1_n_7),
        .I1(euros1__92_carry__2_n_6),
        .I2(euros1__231_carry_n_5),
        .O(euros1__276_carry__2_i_11_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    euros1__276_carry__2_i_12
       (.I0(euros1__169_carry__0_n_4),
        .I1(euros1__92_carry__2_n_7),
        .I2(euros1__231_carry_n_6),
        .O(euros1__276_carry__2_i_12_n_0));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT3 #(
    .INIT(8'h17)) 
    euros1__276_carry__2_i_13
       (.I0(euros1__231_carry__0_n_7),
        .I1(euros1__92_carry__2_n_4),
        .I2(euros1__169_carry__1_n_5),
        .O(euros1__276_carry__2_i_13_n_0));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT3 #(
    .INIT(8'h17)) 
    euros1__276_carry__2_i_14
       (.I0(euros1__231_carry_n_5),
        .I1(euros1__92_carry__2_n_6),
        .I2(euros1__169_carry__1_n_7),
        .O(euros1__276_carry__2_i_14_n_0));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    euros1__276_carry__2_i_2
       (.I0(euros1__231_carry_n_5),
        .I1(euros1__92_carry__2_n_6),
        .I2(euros1__169_carry__1_n_7),
        .I3(euros1__1_carry__4_n_6),
        .I4(euros1__276_carry__2_i_10_n_0),
        .O(euros1__276_carry__2_i_2_n_0));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    euros1__276_carry__2_i_3
       (.I0(euros1__231_carry_n_6),
        .I1(euros1__92_carry__2_n_7),
        .I2(euros1__169_carry__0_n_4),
        .I3(euros1__276_carry__2_i_11_n_0),
        .I4(euros1__1_carry__4_n_7),
        .O(euros1__276_carry__2_i_3_n_0));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    euros1__276_carry__2_i_4
       (.I0(euros1__231_carry_n_7),
        .I1(euros1__92_carry__1_n_4),
        .I2(euros1__169_carry__0_n_5),
        .I3(euros1__1_carry__3_n_4),
        .I4(euros1__276_carry__2_i_12_n_0),
        .O(euros1__276_carry__2_i_4_n_0));
  LUT6 #(
    .INIT(64'h9669699669969669)) 
    euros1__276_carry__2_i_5
       (.I0(euros1__276_carry__2_i_1_n_0),
        .I1(euros1__276_carry__2_i_13_n_0),
        .I2(euros1__169_carry__1_n_4),
        .I3(euros1__92_carry__3_n_7),
        .I4(euros1__231_carry__0_n_6),
        .I5(euros1__1_carry__4_n_4),
        .O(euros1__276_carry__2_i_5_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    euros1__276_carry__2_i_6
       (.I0(euros1__276_carry__2_i_2_n_0),
        .I1(euros1__1_carry__4_n_5),
        .I2(euros1__276_carry__2_i_9_n_0),
        .I3(euros1__231_carry_n_4),
        .I4(euros1__92_carry__2_n_5),
        .I5(euros1__169_carry__1_n_6),
        .O(euros1__276_carry__2_i_6_n_0));
  LUT6 #(
    .INIT(64'h9669699669969669)) 
    euros1__276_carry__2_i_7
       (.I0(euros1__276_carry__2_i_3_n_0),
        .I1(euros1__276_carry__2_i_14_n_0),
        .I2(euros1__169_carry__1_n_6),
        .I3(euros1__92_carry__2_n_5),
        .I4(euros1__231_carry_n_4),
        .I5(euros1__1_carry__4_n_6),
        .O(euros1__276_carry__2_i_7_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    euros1__276_carry__2_i_8
       (.I0(euros1__276_carry__2_i_4_n_0),
        .I1(euros1__1_carry__4_n_7),
        .I2(euros1__276_carry__2_i_11_n_0),
        .I3(euros1__231_carry_n_6),
        .I4(euros1__92_carry__2_n_7),
        .I5(euros1__169_carry__0_n_4),
        .O(euros1__276_carry__2_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT3 #(
    .INIT(8'h96)) 
    euros1__276_carry__2_i_9
       (.I0(euros1__169_carry__1_n_5),
        .I1(euros1__92_carry__2_n_4),
        .I2(euros1__231_carry__0_n_7),
        .O(euros1__276_carry__2_i_9_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 euros1__276_carry__3
       (.CI(euros1__276_carry__2_n_0),
        .CO({euros1__276_carry__3_n_0,NLW_euros1__276_carry__3_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({euros1__276_carry__3_i_1_n_0,euros1__276_carry__3_i_2_n_0,euros1__276_carry__3_i_3_n_0,euros1__276_carry__3_i_4_n_0}),
        .O(NLW_euros1__276_carry__3_O_UNCONNECTED[3:0]),
        .S({euros1__276_carry__3_i_5_n_0,euros1__276_carry__3_i_6_n_0,euros1__276_carry__3_i_7_n_0,euros1__276_carry__3_i_8_n_0}));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    euros1__276_carry__3_i_1
       (.I0(euros1__231_carry__0_n_4),
        .I1(euros1__92_carry__3_n_5),
        .I2(euros1__169_carry__2_n_6),
        .I3(euros1__1_carry__5_n_5),
        .I4(euros1__276_carry__3_i_9_n_0),
        .O(euros1__276_carry__3_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT3 #(
    .INIT(8'h96)) 
    euros1__276_carry__3_i_10
       (.I0(euros1__169_carry__2_n_6),
        .I1(euros1__92_carry__3_n_5),
        .I2(euros1__231_carry__0_n_4),
        .O(euros1__276_carry__3_i_10_n_0));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT3 #(
    .INIT(8'h96)) 
    euros1__276_carry__3_i_11
       (.I0(euros1__169_carry__2_n_7),
        .I1(euros1__92_carry__3_n_6),
        .I2(euros1__231_carry__0_n_5),
        .O(euros1__276_carry__3_i_11_n_0));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT3 #(
    .INIT(8'h96)) 
    euros1__276_carry__3_i_12
       (.I0(euros1__169_carry__1_n_4),
        .I1(euros1__92_carry__3_n_7),
        .I2(euros1__231_carry__0_n_6),
        .O(euros1__276_carry__3_i_12_n_0));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'h17)) 
    euros1__276_carry__3_i_13
       (.I0(euros1__231_carry__1_n_7),
        .I1(euros1__92_carry__3_n_4),
        .I2(euros1__169_carry__2_n_5),
        .O(euros1__276_carry__3_i_13_n_0));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT3 #(
    .INIT(8'h17)) 
    euros1__276_carry__3_i_14
       (.I0(euros1__231_carry__0_n_4),
        .I1(euros1__92_carry__3_n_5),
        .I2(euros1__169_carry__2_n_6),
        .O(euros1__276_carry__3_i_14_n_0));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT3 #(
    .INIT(8'h17)) 
    euros1__276_carry__3_i_15
       (.I0(euros1__231_carry__0_n_5),
        .I1(euros1__92_carry__3_n_6),
        .I2(euros1__169_carry__2_n_7),
        .O(euros1__276_carry__3_i_15_n_0));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT3 #(
    .INIT(8'h17)) 
    euros1__276_carry__3_i_16
       (.I0(euros1__231_carry__0_n_6),
        .I1(euros1__92_carry__3_n_7),
        .I2(euros1__169_carry__1_n_4),
        .O(euros1__276_carry__3_i_16_n_0));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    euros1__276_carry__3_i_2
       (.I0(euros1__231_carry__0_n_5),
        .I1(euros1__92_carry__3_n_6),
        .I2(euros1__169_carry__2_n_7),
        .I3(euros1__1_carry__5_n_6),
        .I4(euros1__276_carry__3_i_10_n_0),
        .O(euros1__276_carry__3_i_2_n_0));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    euros1__276_carry__3_i_3
       (.I0(euros1__231_carry__0_n_6),
        .I1(euros1__92_carry__3_n_7),
        .I2(euros1__169_carry__1_n_4),
        .I3(euros1__1_carry__5_n_7),
        .I4(euros1__276_carry__3_i_11_n_0),
        .O(euros1__276_carry__3_i_3_n_0));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    euros1__276_carry__3_i_4
       (.I0(euros1__231_carry__0_n_7),
        .I1(euros1__92_carry__2_n_4),
        .I2(euros1__169_carry__1_n_5),
        .I3(euros1__1_carry__4_n_4),
        .I4(euros1__276_carry__3_i_12_n_0),
        .O(euros1__276_carry__3_i_4_n_0));
  LUT6 #(
    .INIT(64'h9669699669969669)) 
    euros1__276_carry__3_i_5
       (.I0(euros1__276_carry__3_i_1_n_0),
        .I1(euros1__276_carry__3_i_13_n_0),
        .I2(euros1__169_carry__2_n_4),
        .I3(euros1__92_carry__4_n_7),
        .I4(euros1__231_carry__1_n_6),
        .I5(euros1__1_carry__5_n_4),
        .O(euros1__276_carry__3_i_5_n_0));
  LUT6 #(
    .INIT(64'h9669699669969669)) 
    euros1__276_carry__3_i_6
       (.I0(euros1__276_carry__3_i_2_n_0),
        .I1(euros1__276_carry__3_i_14_n_0),
        .I2(euros1__169_carry__2_n_5),
        .I3(euros1__92_carry__3_n_4),
        .I4(euros1__231_carry__1_n_7),
        .I5(euros1__1_carry__5_n_5),
        .O(euros1__276_carry__3_i_6_n_0));
  LUT6 #(
    .INIT(64'h9669699669969669)) 
    euros1__276_carry__3_i_7
       (.I0(euros1__276_carry__3_i_3_n_0),
        .I1(euros1__276_carry__3_i_15_n_0),
        .I2(euros1__169_carry__2_n_6),
        .I3(euros1__92_carry__3_n_5),
        .I4(euros1__231_carry__0_n_4),
        .I5(euros1__1_carry__5_n_6),
        .O(euros1__276_carry__3_i_7_n_0));
  LUT6 #(
    .INIT(64'h9669699669969669)) 
    euros1__276_carry__3_i_8
       (.I0(euros1__276_carry__3_i_4_n_0),
        .I1(euros1__276_carry__3_i_16_n_0),
        .I2(euros1__169_carry__2_n_7),
        .I3(euros1__92_carry__3_n_6),
        .I4(euros1__231_carry__0_n_5),
        .I5(euros1__1_carry__5_n_7),
        .O(euros1__276_carry__3_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'h96)) 
    euros1__276_carry__3_i_9
       (.I0(euros1__169_carry__2_n_5),
        .I1(euros1__92_carry__3_n_4),
        .I2(euros1__231_carry__1_n_7),
        .O(euros1__276_carry__3_i_9_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 euros1__276_carry__4
       (.CI(euros1__276_carry__3_n_0),
        .CO({euros1__276_carry__4_n_0,NLW_euros1__276_carry__4_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({euros1__276_carry__4_i_1_n_0,euros1__276_carry__4_i_2_n_0,euros1__276_carry__4_i_3_n_0,euros1__276_carry__4_i_4_n_0}),
        .O({euros1__276_carry__4_n_4,euros1__276_carry__4_n_5,euros1__276_carry__4_n_6,euros1__276_carry__4_n_7}),
        .S({euros1__276_carry__4_i_5_n_0,euros1__276_carry__4_i_6_n_0,euros1__276_carry__4_i_7_n_0,euros1__276_carry__4_i_8_n_0}));
  LUT5 #(
    .INIT(32'hEEE8E888)) 
    euros1__276_carry__4_i_1
       (.I0(euros1__1_carry__6_n_5),
        .I1(euros1__276_carry__4_i_9_n_0),
        .I2(euros1__231_carry__1_n_4),
        .I3(euros1__92_carry__4_n_5),
        .I4(euros1__169_carry__3_n_6),
        .O(euros1__276_carry__4_i_1_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    euros1__276_carry__4_i_10
       (.I0(euros1__169_carry__3_n_6),
        .I1(euros1__92_carry__4_n_5),
        .I2(euros1__231_carry__1_n_4),
        .O(euros1__276_carry__4_i_10_n_0));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'h96)) 
    euros1__276_carry__4_i_11
       (.I0(euros1__169_carry__3_n_7),
        .I1(euros1__92_carry__4_n_6),
        .I2(euros1__231_carry__1_n_5),
        .O(euros1__276_carry__4_i_11_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    euros1__276_carry__4_i_12
       (.I0(euros1__169_carry__2_n_4),
        .I1(euros1__92_carry__4_n_7),
        .I2(euros1__231_carry__1_n_6),
        .O(euros1__276_carry__4_i_12_n_0));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT3 #(
    .INIT(8'h17)) 
    euros1__276_carry__4_i_13
       (.I0(euros1__231_carry__2_n_7),
        .I1(euros1__92_carry__4_n_4),
        .I2(euros1__169_carry__3_n_5),
        .O(euros1__276_carry__4_i_13_n_0));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'h17)) 
    euros1__276_carry__4_i_14
       (.I0(euros1__231_carry__1_n_5),
        .I1(euros1__92_carry__4_n_6),
        .I2(euros1__169_carry__3_n_7),
        .O(euros1__276_carry__4_i_14_n_0));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    euros1__276_carry__4_i_2
       (.I0(euros1__231_carry__1_n_5),
        .I1(euros1__92_carry__4_n_6),
        .I2(euros1__169_carry__3_n_7),
        .I3(euros1__1_carry__6_n_6),
        .I4(euros1__276_carry__4_i_10_n_0),
        .O(euros1__276_carry__4_i_2_n_0));
  LUT5 #(
    .INIT(32'hEEE8E888)) 
    euros1__276_carry__4_i_3
       (.I0(euros1__1_carry__6_n_7),
        .I1(euros1__276_carry__4_i_11_n_0),
        .I2(euros1__231_carry__1_n_6),
        .I3(euros1__92_carry__4_n_7),
        .I4(euros1__169_carry__2_n_4),
        .O(euros1__276_carry__4_i_3_n_0));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    euros1__276_carry__4_i_4
       (.I0(euros1__231_carry__1_n_7),
        .I1(euros1__92_carry__3_n_4),
        .I2(euros1__169_carry__2_n_5),
        .I3(euros1__1_carry__5_n_4),
        .I4(euros1__276_carry__4_i_12_n_0),
        .O(euros1__276_carry__4_i_4_n_0));
  LUT6 #(
    .INIT(64'h9669699669969669)) 
    euros1__276_carry__4_i_5
       (.I0(euros1__276_carry__4_i_1_n_0),
        .I1(euros1__276_carry__4_i_13_n_0),
        .I2(euros1__169_carry__3_n_4),
        .I3(euros1__92_carry__5_n_7),
        .I4(euros1__231_carry__2_n_6),
        .I5(euros1__1_carry__6_n_4),
        .O(euros1__276_carry__4_i_5_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    euros1__276_carry__4_i_6
       (.I0(euros1__276_carry__4_i_2_n_0),
        .I1(euros1__1_carry__6_n_5),
        .I2(euros1__276_carry__4_i_9_n_0),
        .I3(euros1__231_carry__1_n_4),
        .I4(euros1__92_carry__4_n_5),
        .I5(euros1__169_carry__3_n_6),
        .O(euros1__276_carry__4_i_6_n_0));
  LUT6 #(
    .INIT(64'h9669699669969669)) 
    euros1__276_carry__4_i_7
       (.I0(euros1__276_carry__4_i_3_n_0),
        .I1(euros1__276_carry__4_i_14_n_0),
        .I2(euros1__169_carry__3_n_6),
        .I3(euros1__92_carry__4_n_5),
        .I4(euros1__231_carry__1_n_4),
        .I5(euros1__1_carry__6_n_6),
        .O(euros1__276_carry__4_i_7_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    euros1__276_carry__4_i_8
       (.I0(euros1__276_carry__4_i_4_n_0),
        .I1(euros1__1_carry__6_n_7),
        .I2(euros1__276_carry__4_i_11_n_0),
        .I3(euros1__231_carry__1_n_6),
        .I4(euros1__92_carry__4_n_7),
        .I5(euros1__169_carry__2_n_4),
        .O(euros1__276_carry__4_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT3 #(
    .INIT(8'h96)) 
    euros1__276_carry__4_i_9
       (.I0(euros1__169_carry__3_n_5),
        .I1(euros1__92_carry__4_n_4),
        .I2(euros1__231_carry__2_n_7),
        .O(euros1__276_carry__4_i_9_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 euros1__276_carry__5
       (.CI(euros1__276_carry__4_n_0),
        .CO(NLW_euros1__276_carry__5_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,euros1__276_carry__5_i_1_n_0}),
        .O({NLW_euros1__276_carry__5_O_UNCONNECTED[3:2],euros1__276_carry__5_n_6,euros1__276_carry__5_n_7}),
        .S({1'b0,1'b0,euros1__276_carry__5_i_2_n_0,euros1__276_carry__5_i_3_n_0}));
  LUT5 #(
    .INIT(32'hFFE8E800)) 
    euros1__276_carry__5_i_1
       (.I0(euros1__231_carry__2_n_7),
        .I1(euros1__92_carry__4_n_4),
        .I2(euros1__169_carry__3_n_5),
        .I3(euros1__1_carry__6_n_4),
        .I4(euros1__276_carry__5_i_4_n_0),
        .O(euros1__276_carry__5_i_1_n_0));
  LUT6 #(
    .INIT(64'hE8818117177E7EE8)) 
    euros1__276_carry__5_i_2
       (.I0(euros1__276_carry__5_i_5_n_0),
        .I1(euros1__1_carry__7_n_7),
        .I2(euros1__231_carry__2_n_5),
        .I3(euros1__92_carry__5_n_6),
        .I4(euros1__169_carry__4_n_7),
        .I5(euros1__276_carry__5_i_6_n_0),
        .O(euros1__276_carry__5_i_2_n_0));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    euros1__276_carry__5_i_3
       (.I0(euros1__276_carry__5_i_1_n_0),
        .I1(euros1__1_carry__7_n_7),
        .I2(euros1__276_carry__5_i_7_n_0),
        .I3(euros1__231_carry__2_n_6),
        .I4(euros1__92_carry__5_n_7),
        .I5(euros1__169_carry__3_n_4),
        .O(euros1__276_carry__5_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT3 #(
    .INIT(8'h96)) 
    euros1__276_carry__5_i_4
       (.I0(euros1__169_carry__3_n_4),
        .I1(euros1__92_carry__5_n_7),
        .I2(euros1__231_carry__2_n_6),
        .O(euros1__276_carry__5_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    euros1__276_carry__5_i_5
       (.I0(euros1__231_carry__2_n_6),
        .I1(euros1__92_carry__5_n_7),
        .I2(euros1__169_carry__3_n_4),
        .O(euros1__276_carry__5_i_5_n_0));
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__276_carry__5_i_6
       (.I0(euros1__169_carry__4_n_6),
        .I1(euros1__231_carry__2_n_4),
        .I2(euros1__1_carry__7_n_6),
        .I3(euros1__92_carry__5_n_5),
        .O(euros1__276_carry__5_i_6_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    euros1__276_carry__5_i_7
       (.I0(euros1__169_carry__4_n_7),
        .I1(euros1__92_carry__5_n_6),
        .I2(euros1__231_carry__2_n_5),
        .O(euros1__276_carry__5_i_7_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    euros1__276_carry_i_1
       (.I0(euros1__1_carry__1_n_5),
        .I1(euros1__92_carry_n_4),
        .O(euros1__276_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    euros1__276_carry_i_2
       (.I0(euros1__1_carry__1_n_6),
        .I1(euros1__92_carry_n_5),
        .O(euros1__276_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    euros1__276_carry_i_3
       (.I0(euros1__1_carry__1_n_7),
        .I1(euros1__92_carry_n_6),
        .O(euros1__276_carry_i_3_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    euros1__276_carry_i_4
       (.I0(Q[0]),
        .I1(euros1__1_carry__0_n_4),
        .O(euros1__276_carry_i_4_n_0));
  (* HLUTNM = "lutpair49" *) 
  LUT4 #(
    .INIT(16'h9666)) 
    euros1__276_carry_i_5
       (.I0(euros1__1_carry__1_n_4),
        .I1(euros1__92_carry__0_n_7),
        .I2(euros1__92_carry_n_4),
        .I3(euros1__1_carry__1_n_5),
        .O(euros1__276_carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h8778)) 
    euros1__276_carry_i_6
       (.I0(euros1__92_carry_n_5),
        .I1(euros1__1_carry__1_n_6),
        .I2(euros1__92_carry_n_4),
        .I3(euros1__1_carry__1_n_5),
        .O(euros1__276_carry_i_6_n_0));
  LUT4 #(
    .INIT(16'h8778)) 
    euros1__276_carry_i_7
       (.I0(euros1__92_carry_n_6),
        .I1(euros1__1_carry__1_n_7),
        .I2(euros1__92_carry_n_5),
        .I3(euros1__1_carry__1_n_6),
        .O(euros1__276_carry_i_7_n_0));
  LUT4 #(
    .INIT(16'h8778)) 
    euros1__276_carry_i_8
       (.I0(euros1__1_carry__0_n_4),
        .I1(Q[0]),
        .I2(euros1__92_carry_n_6),
        .I3(euros1__1_carry__1_n_7),
        .O(euros1__276_carry_i_8_n_0));
  (* OPT_MODIFIED = "PROPCONST SWEEP" *) 
  CARRY4 euros1__333_carry
       (.CI(1'b0),
        .CO(NLW_euros1__333_carry_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,euros1__333_carry_i_1_n_0,1'b0}),
        .O({NLW_euros1__333_carry_O_UNCONNECTED[3],euros1__333_carry_n_5,euros1__333_carry_n_6,euros1__333_carry_n_7}),
        .S({1'b0,euros1__333_carry_i_2_n_0,euros1__333_carry_i_3_n_0,euros1__333_carry_i_4_n_0}));
  LUT2 #(
    .INIT(4'hB)) 
    euros1__333_carry_i_1
       (.I0(euros1__276_carry__4_n_4),
        .I1(euros1__276_carry__4_n_7),
        .O(euros1__333_carry_i_1_n_0));
  LUT5 #(
    .INIT(32'h66969969)) 
    euros1__333_carry_i_2
       (.I0(euros1__276_carry__5_n_6),
        .I1(euros1__276_carry__4_n_5),
        .I2(euros1__276_carry__5_n_7),
        .I3(euros1__276_carry__4_n_6),
        .I4(euros1__276_carry__4_n_7),
        .O(euros1__333_carry_i_2_n_0));
  LUT4 #(
    .INIT(16'h2DD2)) 
    euros1__333_carry_i_3
       (.I0(euros1__276_carry__4_n_7),
        .I1(euros1__276_carry__4_n_4),
        .I2(euros1__276_carry__5_n_7),
        .I3(euros1__276_carry__4_n_6),
        .O(euros1__333_carry_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    euros1__333_carry_i_4
       (.I0(euros1__276_carry__4_n_4),
        .I1(euros1__276_carry__4_n_7),
        .O(euros1__333_carry_i_4_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 euros1__339_carry
       (.CI(1'b0),
        .CO({euros1__339_carry_n_0,NLW_euros1__339_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b1),
        .DI(Q[3:0]),
        .O({euros1__339_carry_n_4,euros1__339_carry_n_5,p_1_in}),
        .S({euros1__339_carry_i_1_n_0,euros1__339_carry_i_2_n_0,euros1__339_carry_i_3_n_0,euros1__339_carry_i_4_n_0}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 euros1__339_carry__0
       (.CI(euros1__339_carry_n_0),
        .CO(NLW_euros1__339_carry__0_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,Q[6:4]}),
        .O({euros1__339_carry__0_n_4,euros1__339_carry__0_n_5,euros1__339_carry__0_n_6,euros1__339_carry__0_n_7}),
        .S({euros1__339_carry__0_i_1_n_0,euros1__339_carry__0_i_2_n_0,euros1__339_carry__0_i_3_n_0,euros1__339_carry__0_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    euros1__339_carry__0_i_1
       (.I0(Q[7]),
        .I1(euros1__333_carry_n_5),
        .O(euros1__339_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    euros1__339_carry__0_i_2
       (.I0(Q[6]),
        .I1(euros1__333_carry_n_6),
        .O(euros1__339_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    euros1__339_carry__0_i_3
       (.I0(Q[5]),
        .I1(euros1__333_carry_n_7),
        .O(euros1__339_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    euros1__339_carry__0_i_4
       (.I0(Q[4]),
        .I1(euros1__276_carry__4_n_5),
        .O(euros1__339_carry__0_i_4_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    euros1__339_carry_i_1
       (.I0(Q[3]),
        .I1(euros1__276_carry__4_n_6),
        .O(euros1__339_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    euros1__339_carry_i_2
       (.I0(Q[2]),
        .I1(euros1__276_carry__4_n_7),
        .O(euros1__339_carry_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    euros1__339_carry_i_3
       (.I0(Q[1]),
        .O(euros1__339_carry_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    euros1__339_carry_i_4
       (.I0(Q[0]),
        .O(euros1__339_carry_i_4_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 euros1__92_carry
       (.CI(1'b0),
        .CO({euros1__92_carry_n_0,NLW_euros1__92_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({euros1__92_carry_i_1_n_0,Q[0],1'b0,1'b1}),
        .O({euros1__92_carry_n_4,euros1__92_carry_n_5,euros1__92_carry_n_6,euros1__92_carry_n_7}),
        .S({euros1__92_carry_i_2_n_0,euros1__92_carry_i_3_n_0,euros1__92_carry_i_4_n_0,Q[0]}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 euros1__92_carry__0
       (.CI(euros1__92_carry_n_0),
        .CO({euros1__92_carry__0_n_0,NLW_euros1__92_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({euros1__92_carry__0_i_1_n_0,euros1__92_carry__0_i_2_n_0,euros1__92_carry__0_i_3_n_0,euros1__92_carry__0_i_4_n_0}),
        .O({euros1__92_carry__0_n_4,euros1__92_carry__0_n_5,euros1__92_carry__0_n_6,euros1__92_carry__0_n_7}),
        .S({euros1__92_carry__0_i_5_n_0,euros1__92_carry__0_i_6_n_0,euros1__92_carry__0_i_7_n_0,euros1__92_carry__0_i_8_n_0}));
  (* HLUTNM = "lutpair22" *) 
  LUT3 #(
    .INIT(8'h2B)) 
    euros1__92_carry__0_i_1
       (.I0(Q[2]),
        .I1(Q[4]),
        .I2(Q[6]),
        .O(euros1__92_carry__0_i_1_n_0));
  LUT3 #(
    .INIT(8'h2B)) 
    euros1__92_carry__0_i_2
       (.I0(Q[1]),
        .I1(Q[3]),
        .I2(Q[5]),
        .O(euros1__92_carry__0_i_2_n_0));
  LUT3 #(
    .INIT(8'h2B)) 
    euros1__92_carry__0_i_3
       (.I0(Q[0]),
        .I1(Q[4]),
        .I2(Q[2]),
        .O(euros1__92_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    euros1__92_carry__0_i_4
       (.I0(Q[1]),
        .I1(Q[3]),
        .O(euros1__92_carry__0_i_4_n_0));
  (* HLUTNM = "lutpair23" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__92_carry__0_i_5
       (.I0(Q[5]),
        .I1(Q[3]),
        .I2(Q[7]),
        .I3(euros1__92_carry__0_i_1_n_0),
        .O(euros1__92_carry__0_i_5_n_0));
  (* HLUTNM = "lutpair22" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__92_carry__0_i_6
       (.I0(Q[2]),
        .I1(Q[4]),
        .I2(Q[6]),
        .I3(euros1__92_carry__0_i_2_n_0),
        .O(euros1__92_carry__0_i_6_n_0));
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__92_carry__0_i_7
       (.I0(Q[1]),
        .I1(Q[3]),
        .I2(Q[5]),
        .I3(euros1__92_carry__0_i_3_n_0),
        .O(euros1__92_carry__0_i_7_n_0));
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__92_carry__0_i_8
       (.I0(Q[0]),
        .I1(Q[4]),
        .I2(Q[2]),
        .I3(euros1__92_carry__0_i_4_n_0),
        .O(euros1__92_carry__0_i_8_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 euros1__92_carry__1
       (.CI(euros1__92_carry__0_n_0),
        .CO({euros1__92_carry__1_n_0,NLW_euros1__92_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({euros1__92_carry__1_i_1_n_0,euros1__92_carry__1_i_2_n_0,euros1__92_carry__1_i_3_n_0,euros1__92_carry__1_i_4_n_0}),
        .O({euros1__92_carry__1_n_4,euros1__92_carry__1_n_5,euros1__92_carry__1_n_6,euros1__92_carry__1_n_7}),
        .S({euros1__92_carry__1_i_5_n_0,euros1__92_carry__1_i_6_n_0,euros1__92_carry__1_i_7_n_0,euros1__92_carry__1_i_8_n_0}));
  (* HLUTNM = "lutpair24" *) 
  LUT3 #(
    .INIT(8'h2B)) 
    euros1__92_carry__1_i_1
       (.I0(Q[6]),
        .I1(Q[10]),
        .I2(Q[8]),
        .O(euros1__92_carry__1_i_1_n_0));
  LUT3 #(
    .INIT(8'h4D)) 
    euros1__92_carry__1_i_2
       (.I0(Q[7]),
        .I1(Q[5]),
        .I2(Q[9]),
        .O(euros1__92_carry__1_i_2_n_0));
  LUT3 #(
    .INIT(8'h2B)) 
    euros1__92_carry__1_i_3
       (.I0(Q[4]),
        .I1(Q[8]),
        .I2(Q[6]),
        .O(euros1__92_carry__1_i_3_n_0));
  (* HLUTNM = "lutpair23" *) 
  LUT3 #(
    .INIT(8'h4D)) 
    euros1__92_carry__1_i_4
       (.I0(Q[5]),
        .I1(Q[3]),
        .I2(Q[7]),
        .O(euros1__92_carry__1_i_4_n_0));
  (* HLUTNM = "lutpair25" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__92_carry__1_i_5
       (.I0(Q[9]),
        .I1(Q[11]),
        .I2(Q[7]),
        .I3(euros1__92_carry__1_i_1_n_0),
        .O(euros1__92_carry__1_i_5_n_0));
  (* HLUTNM = "lutpair24" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__92_carry__1_i_6
       (.I0(Q[6]),
        .I1(Q[10]),
        .I2(Q[8]),
        .I3(euros1__92_carry__1_i_2_n_0),
        .O(euros1__92_carry__1_i_6_n_0));
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__92_carry__1_i_7
       (.I0(Q[7]),
        .I1(Q[5]),
        .I2(Q[9]),
        .I3(euros1__92_carry__1_i_3_n_0),
        .O(euros1__92_carry__1_i_7_n_0));
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__92_carry__1_i_8
       (.I0(Q[4]),
        .I1(Q[8]),
        .I2(Q[6]),
        .I3(euros1__92_carry__1_i_4_n_0),
        .O(euros1__92_carry__1_i_8_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 euros1__92_carry__2
       (.CI(euros1__92_carry__1_n_0),
        .CO({euros1__92_carry__2_n_0,NLW_euros1__92_carry__2_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({euros1__92_carry__2_i_1_n_0,euros1__92_carry__2_i_2_n_0,euros1__92_carry__2_i_3_n_0,euros1__92_carry__2_i_4_n_0}),
        .O({euros1__92_carry__2_n_4,euros1__92_carry__2_n_5,euros1__92_carry__2_n_6,euros1__92_carry__2_n_7}),
        .S({euros1__92_carry__2_i_5_n_0,euros1__92_carry__2_i_6_n_0,euros1__92_carry__2_i_7_n_0,euros1__92_carry__2_i_8_n_0}));
  (* HLUTNM = "lutpair28" *) 
  LUT3 #(
    .INIT(8'h2B)) 
    euros1__92_carry__2_i_1
       (.I0(Q[10]),
        .I1(Q[14]),
        .I2(Q[12]),
        .O(euros1__92_carry__2_i_1_n_0));
  (* HLUTNM = "lutpair27" *) 
  LUT3 #(
    .INIT(8'h2B)) 
    euros1__92_carry__2_i_2
       (.I0(Q[9]),
        .I1(Q[13]),
        .I2(Q[11]),
        .O(euros1__92_carry__2_i_2_n_0));
  (* HLUTNM = "lutpair26" *) 
  LUT3 #(
    .INIT(8'h2B)) 
    euros1__92_carry__2_i_3
       (.I0(Q[8]),
        .I1(Q[12]),
        .I2(Q[10]),
        .O(euros1__92_carry__2_i_3_n_0));
  (* HLUTNM = "lutpair25" *) 
  LUT3 #(
    .INIT(8'h71)) 
    euros1__92_carry__2_i_4
       (.I0(Q[9]),
        .I1(Q[11]),
        .I2(Q[7]),
        .O(euros1__92_carry__2_i_4_n_0));
  (* HLUTNM = "lutpair29" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__92_carry__2_i_5
       (.I0(Q[11]),
        .I1(Q[15]),
        .I2(Q[13]),
        .I3(euros1__92_carry__2_i_1_n_0),
        .O(euros1__92_carry__2_i_5_n_0));
  (* HLUTNM = "lutpair28" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__92_carry__2_i_6
       (.I0(Q[10]),
        .I1(Q[14]),
        .I2(Q[12]),
        .I3(euros1__92_carry__2_i_2_n_0),
        .O(euros1__92_carry__2_i_6_n_0));
  (* HLUTNM = "lutpair27" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__92_carry__2_i_7
       (.I0(Q[9]),
        .I1(Q[13]),
        .I2(Q[11]),
        .I3(euros1__92_carry__2_i_3_n_0),
        .O(euros1__92_carry__2_i_7_n_0));
  (* HLUTNM = "lutpair26" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__92_carry__2_i_8
       (.I0(Q[8]),
        .I1(Q[12]),
        .I2(Q[10]),
        .I3(euros1__92_carry__2_i_4_n_0),
        .O(euros1__92_carry__2_i_8_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 euros1__92_carry__3
       (.CI(euros1__92_carry__2_n_0),
        .CO({euros1__92_carry__3_n_0,NLW_euros1__92_carry__3_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({euros1__92_carry__3_i_1_n_0,euros1__92_carry__3_i_2_n_0,euros1__92_carry__3_i_3_n_0,euros1__92_carry__3_i_4_n_0}),
        .O({euros1__92_carry__3_n_4,euros1__92_carry__3_n_5,euros1__92_carry__3_n_6,euros1__92_carry__3_n_7}),
        .S({euros1__92_carry__3_i_5_n_0,euros1__92_carry__3_i_6_n_0,euros1__92_carry__3_i_7_n_0,euros1__92_carry__3_i_8_n_0}));
  (* HLUTNM = "lutpair32" *) 
  LUT3 #(
    .INIT(8'h2B)) 
    euros1__92_carry__3_i_1
       (.I0(Q[14]),
        .I1(Q[18]),
        .I2(Q[16]),
        .O(euros1__92_carry__3_i_1_n_0));
  (* HLUTNM = "lutpair31" *) 
  LUT3 #(
    .INIT(8'h2B)) 
    euros1__92_carry__3_i_2
       (.I0(Q[13]),
        .I1(Q[17]),
        .I2(Q[15]),
        .O(euros1__92_carry__3_i_2_n_0));
  (* HLUTNM = "lutpair30" *) 
  LUT3 #(
    .INIT(8'h2B)) 
    euros1__92_carry__3_i_3
       (.I0(Q[12]),
        .I1(Q[16]),
        .I2(Q[14]),
        .O(euros1__92_carry__3_i_3_n_0));
  (* HLUTNM = "lutpair29" *) 
  LUT3 #(
    .INIT(8'h2B)) 
    euros1__92_carry__3_i_4
       (.I0(Q[11]),
        .I1(Q[15]),
        .I2(Q[13]),
        .O(euros1__92_carry__3_i_4_n_0));
  (* HLUTNM = "lutpair33" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__92_carry__3_i_5
       (.I0(Q[15]),
        .I1(Q[19]),
        .I2(Q[17]),
        .I3(euros1__92_carry__3_i_1_n_0),
        .O(euros1__92_carry__3_i_5_n_0));
  (* HLUTNM = "lutpair32" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__92_carry__3_i_6
       (.I0(Q[14]),
        .I1(Q[18]),
        .I2(Q[16]),
        .I3(euros1__92_carry__3_i_2_n_0),
        .O(euros1__92_carry__3_i_6_n_0));
  (* HLUTNM = "lutpair31" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__92_carry__3_i_7
       (.I0(Q[13]),
        .I1(Q[17]),
        .I2(Q[15]),
        .I3(euros1__92_carry__3_i_3_n_0),
        .O(euros1__92_carry__3_i_7_n_0));
  (* HLUTNM = "lutpair30" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__92_carry__3_i_8
       (.I0(Q[12]),
        .I1(Q[16]),
        .I2(Q[14]),
        .I3(euros1__92_carry__3_i_4_n_0),
        .O(euros1__92_carry__3_i_8_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 euros1__92_carry__4
       (.CI(euros1__92_carry__3_n_0),
        .CO({euros1__92_carry__4_n_0,NLW_euros1__92_carry__4_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({euros1__92_carry__4_i_1_n_0,euros1__92_carry__4_i_2_n_0,euros1__92_carry__4_i_3_n_0,euros1__92_carry__4_i_4_n_0}),
        .O({euros1__92_carry__4_n_4,euros1__92_carry__4_n_5,euros1__92_carry__4_n_6,euros1__92_carry__4_n_7}),
        .S({euros1__92_carry__4_i_5_n_0,euros1__92_carry__4_i_6_n_0,euros1__92_carry__4_i_7_n_0,euros1__92_carry__4_i_8_n_0}));
  (* HLUTNM = "lutpair36" *) 
  LUT3 #(
    .INIT(8'h71)) 
    euros1__92_carry__4_i_1
       (.I0(Q[20]),
        .I1(Q[22]),
        .I2(Q[18]),
        .O(euros1__92_carry__4_i_1_n_0));
  (* HLUTNM = "lutpair35" *) 
  LUT3 #(
    .INIT(8'h2B)) 
    euros1__92_carry__4_i_2
       (.I0(Q[17]),
        .I1(Q[21]),
        .I2(Q[19]),
        .O(euros1__92_carry__4_i_2_n_0));
  (* HLUTNM = "lutpair34" *) 
  LUT3 #(
    .INIT(8'h2B)) 
    euros1__92_carry__4_i_3
       (.I0(Q[16]),
        .I1(Q[20]),
        .I2(Q[18]),
        .O(euros1__92_carry__4_i_3_n_0));
  (* HLUTNM = "lutpair33" *) 
  LUT3 #(
    .INIT(8'h2B)) 
    euros1__92_carry__4_i_4
       (.I0(Q[15]),
        .I1(Q[19]),
        .I2(Q[17]),
        .O(euros1__92_carry__4_i_4_n_0));
  (* HLUTNM = "lutpair37" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__92_carry__4_i_5
       (.I0(Q[21]),
        .I1(Q[23]),
        .I2(Q[19]),
        .I3(euros1__92_carry__4_i_1_n_0),
        .O(euros1__92_carry__4_i_5_n_0));
  (* HLUTNM = "lutpair36" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__92_carry__4_i_6
       (.I0(Q[20]),
        .I1(Q[22]),
        .I2(Q[18]),
        .I3(euros1__92_carry__4_i_2_n_0),
        .O(euros1__92_carry__4_i_6_n_0));
  (* HLUTNM = "lutpair35" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__92_carry__4_i_7
       (.I0(Q[17]),
        .I1(Q[21]),
        .I2(Q[19]),
        .I3(euros1__92_carry__4_i_3_n_0),
        .O(euros1__92_carry__4_i_7_n_0));
  (* HLUTNM = "lutpair34" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__92_carry__4_i_8
       (.I0(Q[16]),
        .I1(Q[20]),
        .I2(Q[18]),
        .I3(euros1__92_carry__4_i_4_n_0),
        .O(euros1__92_carry__4_i_8_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 euros1__92_carry__5
       (.CI(euros1__92_carry__4_n_0),
        .CO(NLW_euros1__92_carry__5_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,euros1__92_carry__5_i_1_n_0,euros1__92_carry__5_i_2_n_0}),
        .O({NLW_euros1__92_carry__5_O_UNCONNECTED[3],euros1__92_carry__5_n_5,euros1__92_carry__5_n_6,euros1__92_carry__5_n_7}),
        .S({1'b0,euros1__92_carry__5_i_3_n_0,euros1__92_carry__5_i_4_n_0,euros1__92_carry__5_i_5_n_0}));
  (* HLUTNM = "lutpair38" *) 
  LUT3 #(
    .INIT(8'h2B)) 
    euros1__92_carry__5_i_1
       (.I0(Q[20]),
        .I1(Q[24]),
        .I2(Q[22]),
        .O(euros1__92_carry__5_i_1_n_0));
  (* HLUTNM = "lutpair37" *) 
  LUT3 #(
    .INIT(8'h71)) 
    euros1__92_carry__5_i_2
       (.I0(Q[21]),
        .I1(Q[23]),
        .I2(Q[19]),
        .O(euros1__92_carry__5_i_2_n_0));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    euros1__92_carry__5_i_3
       (.I0(Q[23]),
        .I1(Q[21]),
        .I2(Q[25]),
        .I3(Q[22]),
        .I4(Q[24]),
        .I5(Q[26]),
        .O(euros1__92_carry__5_i_3_n_0));
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__92_carry__5_i_4
       (.I0(euros1__92_carry__5_i_1_n_0),
        .I1(Q[21]),
        .I2(Q[23]),
        .I3(Q[25]),
        .O(euros1__92_carry__5_i_4_n_0));
  (* HLUTNM = "lutpair38" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    euros1__92_carry__5_i_5
       (.I0(Q[20]),
        .I1(Q[24]),
        .I2(Q[22]),
        .I3(euros1__92_carry__5_i_2_n_0),
        .O(euros1__92_carry__5_i_5_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    euros1__92_carry_i_1
       (.I0(Q[0]),
        .O(euros1__92_carry_i_1_n_0));
  LUT3 #(
    .INIT(8'h69)) 
    euros1__92_carry_i_2
       (.I0(Q[1]),
        .I1(Q[3]),
        .I2(Q[0]),
        .O(euros1__92_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    euros1__92_carry_i_3
       (.I0(Q[0]),
        .I1(Q[2]),
        .O(euros1__92_carry_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    euros1__92_carry_i_4
       (.I0(Q[1]),
        .O(euros1__92_carry_i_4_n_0));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT2 #(
    .INIT(4'h9)) 
    i___0_carry__0_i_1
       (.I0(i___169_carry_i_5_n_0),
        .I1(cent[6]),
        .O(i___0_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i___0_carry__0_i_2
       (.I0(cent[5]),
        .I1(\total_reg[0] ),
        .O(i___0_carry__0_i_2_n_0));
  LUT6 #(
    .INIT(64'hBBBB8B8B8888B888)) 
    i___0_carry__0_i_3
       (.I0(Q[6]),
        .I1(\total_reg[15] ),
        .I2(euros1__339_carry__0_n_6),
        .I3(euros1__339_carry__0_n_4),
        .I4(\string_cent_decenas[1]5__27_carry__1_i_4_n_0 ),
        .I5(euros1__339_carry__0_n_5),
        .O(cent[6]));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT4 #(
    .INIT(16'h6999)) 
    i___0_carry__0_i_4
       (.I0(cent[6]),
        .I1(i___169_carry_i_5_n_0),
        .I2(\total_reg[0] ),
        .I3(cent[5]),
        .O(i___0_carry__0_i_4_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i___0_carry__0_i_5
       (.I0(cent[5]),
        .I1(\total_reg[0] ),
        .O(i___0_carry__0_i_5_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i___0_carry__0_i_6
       (.I0(cent[6]),
        .I1(cent[4]),
        .O(i___0_carry__0_i_6_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i___0_carry__0_i_7
       (.I0(cent[5]),
        .I1(cent[3]),
        .O(i___0_carry__0_i_7_n_0));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT2 #(
    .INIT(4'h2)) 
    i___0_carry__1_i_1
       (.I0(cent[6]),
        .I1(i___169_carry_i_5_n_0),
        .O(i___0_carry__1_i_1_n_0));
  LUT6 #(
    .INIT(64'hBB88BB8B88BB8888)) 
    i___0_carry__1_i_2
       (.I0(Q[5]),
        .I1(\total_reg[15] ),
        .I2(euros1__339_carry__0_n_5),
        .I3(\string_cent_decenas[1]5__27_carry__1_i_4_n_0 ),
        .I4(euros1__339_carry__0_n_4),
        .I5(euros1__339_carry__0_n_6),
        .O(i___0_carry__1_i_2_n_0));
  LUT6 #(
    .INIT(64'hB8B8B8B8B8B8B88B)) 
    i___0_carry__1_i_3
       (.I0(Q[4]),
        .I1(\total_reg[15] ),
        .I2(euros1__339_carry__0_n_7),
        .I3(\string_cent_decenas[1]5__186_carry_i_8_n_0 ),
        .I4(euros1__339_carry_n_4),
        .I5(euros1__339_carry_n_5),
        .O(i___0_carry__1_i_3_n_0));
  LUT5 #(
    .INIT(32'hAAAACCC3)) 
    i___0_carry__1_i_4
       (.I0(Q[3]),
        .I1(euros1__339_carry_n_4),
        .I2(\string_cent_decenas[1]5__186_carry_i_8_n_0 ),
        .I3(euros1__339_carry_n_5),
        .I4(\total_reg[15] ),
        .O(i___0_carry__1_i_4_n_0));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT3 #(
    .INIT(8'hB4)) 
    i___0_carry__1_i_5
       (.I0(i___169_carry_i_5_n_0),
        .I1(cent[6]),
        .I2(i___0_carry_i_2_n_0),
        .O(i___0_carry__1_i_5_n_0));
  LUT6 #(
    .INIT(64'hBBBB8B8B8888B888)) 
    i___0_carry__2_i_1
       (.I0(Q[6]),
        .I1(\total_reg[15] ),
        .I2(euros1__339_carry__0_n_6),
        .I3(euros1__339_carry__0_n_4),
        .I4(\string_cent_decenas[1]5__27_carry__1_i_4_n_0 ),
        .I5(euros1__339_carry__0_n_5),
        .O(i___0_carry__2_i_1_n_0));
  LUT6 #(
    .INIT(64'hB8B8B8B8B8B8B88B)) 
    i___0_carry_i_1
       (.I0(Q[4]),
        .I1(\total_reg[15] ),
        .I2(euros1__339_carry__0_n_7),
        .I3(\string_cent_decenas[1]5__186_carry_i_8_n_0 ),
        .I4(euros1__339_carry_n_4),
        .I5(euros1__339_carry_n_5),
        .O(cent[4]));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT2 #(
    .INIT(4'h6)) 
    i___0_carry_i_3
       (.I0(i___0_carry_i_2_n_0),
        .I1(cent[4]),
        .O(i___0_carry_i_3_n_0));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT2 #(
    .INIT(4'h9)) 
    i___0_carry_i_4
       (.I0(i___169_carry_i_5_n_0),
        .I1(cent[3]),
        .O(i___0_carry_i_4_n_0));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT2 #(
    .INIT(4'h6)) 
    i___0_carry_i_5
       (.I0(\total_reg[0] ),
        .I1(i___0_carry_i_2_n_0),
        .O(i___0_carry_i_5_n_0));
  LUT3 #(
    .INIT(8'hB8)) 
    i___0_carry_i_6
       (.I0(Q[1]),
        .I1(\total_reg[15] ),
        .I2(p_1_in[1]),
        .O(i___0_carry_i_6_n_0));
  LUT4 #(
    .INIT(16'h0880)) 
    i___106_carry__0_i_1
       (.I0(\string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_5 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___56_carry_n_6 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_4 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___56_carry_n_5 ),
        .O(i___106_carry__0_i_1_n_0));
  LUT5 #(
    .INIT(32'hBE282828)) 
    i___106_carry__0_i_2
       (.I0(\string_cent_decenas[1]5_inferred__0/i___0_carry__2_n_2 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_5 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___56_carry_n_6 ),
        .I3(\total_reg[0] ),
        .I4(\string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_6 ),
        .O(i___106_carry__0_i_2_n_0));
  (* HLUTNM = "lutpair58" *) 
  LUT3 #(
    .INIT(8'h28)) 
    i___106_carry__0_i_3
       (.I0(\string_cent_decenas[1]5_inferred__0/i___0_carry__2_n_7 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_6 ),
        .I2(\total_reg[0] ),
        .O(i___106_carry__0_i_3_n_0));
  (* HLUTNM = "lutpair57" *) 
  LUT2 #(
    .INIT(4'h8)) 
    i___106_carry__0_i_4
       (.I0(\string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_4 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_7 ),
        .O(i___106_carry__0_i_4_n_0));
  LUT5 #(
    .INIT(32'hF087870F)) 
    i___106_carry__0_i_5
       (.I0(\string_cent_decenas[1]5_inferred__0/i___56_carry_n_6 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_5 ),
        .I2(i___106_carry__0_i_9_n_0),
        .I3(\string_cent_decenas[1]5_inferred__0/i___56_carry_n_5 ),
        .I4(\string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_4 ),
        .O(i___106_carry__0_i_5_n_0));
  LUT5 #(
    .INIT(32'h69969696)) 
    i___106_carry__0_i_6
       (.I0(i___106_carry__0_i_2_n_0),
        .I1(\string_cent_decenas[1]5_inferred__0/i___56_carry_n_5 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_4 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___56_carry_n_6 ),
        .I4(\string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_5 ),
        .O(i___106_carry__0_i_6_n_0));
  LUT6 #(
    .INIT(64'h9669699669966996)) 
    i___106_carry__0_i_7
       (.I0(i___106_carry__0_i_3_n_0),
        .I1(\string_cent_decenas[1]5_inferred__0/i___0_carry__2_n_2 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_5 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___56_carry_n_6 ),
        .I4(\total_reg[0] ),
        .I5(\string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_6 ),
        .O(i___106_carry__0_i_7_n_0));
  (* HLUTNM = "lutpair58" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    i___106_carry__0_i_8
       (.I0(\string_cent_decenas[1]5_inferred__0/i___0_carry__2_n_7 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_6 ),
        .I2(\total_reg[0] ),
        .I3(i___106_carry__0_i_4_n_0),
        .O(i___106_carry__0_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'h69)) 
    i___106_carry__0_i_9
       (.I0(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_7 ),
        .I1(\total_reg[0] ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___56_carry_n_4 ),
        .O(i___106_carry__0_i_9_n_0));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT6 #(
    .INIT(64'h6969690069000000)) 
    i___106_carry__1_i_1
       (.I0(\string_cent_decenas[1]5_inferred__0/i___80_carry_n_7 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_5 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_5 ),
        .I4(i___0_carry_i_2_n_0),
        .I5(\string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_6 ),
        .O(i___106_carry__1_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    i___106_carry__1_i_10
       (.I0(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_7 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___56_carry_n_4 ),
        .I2(\total_reg[0] ),
        .O(i___106_carry__1_i_10_n_0));
  (* OPT_MODIFIED = "RETARGET" *) 
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'h69)) 
    i___106_carry__1_i_11
       (.I0(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_5 ),
        .I1(i___0_carry_i_2_n_0),
        .I2(\string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_6 ),
        .O(i___106_carry__1_i_11_n_0));
  (* OPT_MODIFIED = "RETARGET" *) 
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'h96)) 
    i___106_carry__1_i_12
       (.I0(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_6 ),
        .I1(i___169_carry_i_5_n_0),
        .I2(\string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_7 ),
        .O(i___106_carry__1_i_12_n_0));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT6 #(
    .INIT(64'hD40000D400D4D400)) 
    i___106_carry__1_i_2
       (.I0(i___169_carry_i_5_n_0),
        .I1(\string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_7 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_6 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_6 ),
        .I4(i___0_carry_i_2_n_0),
        .I5(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_5 ),
        .O(i___106_carry__1_i_2_n_0));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT6 #(
    .INIT(64'h00E8E800E80000E8)) 
    i___106_carry__1_i_3
       (.I0(\total_reg[0] ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___56_carry_n_4 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_7 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_7 ),
        .I4(i___169_carry_i_5_n_0),
        .I5(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_6 ),
        .O(i___106_carry__1_i_3_n_0));
  LUT5 #(
    .INIT(32'h80080880)) 
    i___106_carry__1_i_4
       (.I0(\string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_4 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___56_carry_n_5 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___56_carry_n_4 ),
        .I3(\total_reg[0] ),
        .I4(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_7 ),
        .O(i___106_carry__1_i_4_n_0));
  LUT6 #(
    .INIT(64'h6AA9955695566AA9)) 
    i___106_carry__1_i_5
       (.I0(i___106_carry__1_i_1_n_0),
        .I1(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___80_carry_n_7 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_5 ),
        .I4(\string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_4 ),
        .I5(\string_cent_decenas[1]5_inferred__0/i___80_carry_n_6 ),
        .O(i___106_carry__1_i_5_n_0));
  LUT5 #(
    .INIT(32'h69969669)) 
    i___106_carry__1_i_6
       (.I0(i___106_carry__1_i_2_n_0),
        .I1(i___106_carry__1_i_9_n_0),
        .I2(\string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_5 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0 ),
        .I4(\string_cent_decenas[1]5_inferred__0/i___80_carry_n_7 ),
        .O(i___106_carry__1_i_6_n_0));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT5 #(
    .INIT(32'hE87E1781)) 
    i___106_carry__1_i_7
       (.I0(i___106_carry__1_i_10_n_0),
        .I1(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_6 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_7 ),
        .I3(i___169_carry_i_5_n_0),
        .I4(i___106_carry__1_i_11_n_0),
        .O(i___106_carry__1_i_7_n_0));
  LUT6 #(
    .INIT(64'h7FF8F8808007077F)) 
    i___106_carry__1_i_8
       (.I0(\string_cent_decenas[1]5_inferred__0/i___56_carry_n_5 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_4 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_7 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___56_carry_n_4 ),
        .I4(\total_reg[0] ),
        .I5(i___106_carry__1_i_12_n_0),
        .O(i___106_carry__1_i_8_n_0));
  (* OPT_MODIFIED = "RETARGET" *) 
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'hE8)) 
    i___106_carry__1_i_9
       (.I0(\string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_6 ),
        .I1(i___0_carry_i_2_n_0),
        .I2(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_5 ),
        .O(i___106_carry__1_i_9_n_0));
  LUT5 #(
    .INIT(32'h90006660)) 
    i___106_carry__2_i_1
       (.I0(\string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_7 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_6 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___80_carry_n_4 ),
        .I4(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0 ),
        .O(i___106_carry__2_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'h69)) 
    i___106_carry__2_i_10
       (.I0(\string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_7 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___80_carry_n_5 ),
        .O(i___106_carry__2_i_10_n_0));
  LUT5 #(
    .INIT(32'h08E0800E)) 
    i___106_carry__2_i_2
       (.I0(\string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_7 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___80_carry_n_5 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___80_carry_n_4 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0 ),
        .I4(\string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_6 ),
        .O(i___106_carry__2_i_2_n_0));
  LUT5 #(
    .INIT(32'h60009990)) 
    i___106_carry__2_i_3
       (.I0(\string_cent_decenas[1]5_inferred__0/i___80_carry_n_5 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_7 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_4 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___80_carry_n_6 ),
        .I4(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0 ),
        .O(i___106_carry__2_i_3_n_0));
  LUT5 #(
    .INIT(32'h60009990)) 
    i___106_carry__2_i_4
       (.I0(\string_cent_decenas[1]5_inferred__0/i___80_carry_n_6 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_4 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_5 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___80_carry_n_7 ),
        .I4(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0 ),
        .O(i___106_carry__2_i_4_n_0));
  LUT6 #(
    .INIT(64'h7FFE8001F8E0071F)) 
    i___106_carry__2_i_5
       (.I0(\string_cent_decenas[1]5_inferred__0/i___80_carry_n_4 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_6 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0 ),
        .I4(\string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_6 ),
        .I5(\string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_7 ),
        .O(i___106_carry__2_i_5_n_0));
  LUT6 #(
    .INIT(64'h0E7070F1F18F8F0E)) 
    i___106_carry__2_i_6
       (.I0(\string_cent_decenas[1]5_inferred__0/i___80_carry_n_5 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_7 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___80_carry_n_4 ),
        .I4(\string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_6 ),
        .I5(i___106_carry__2_i_9_n_0),
        .O(i___106_carry__2_i_6_n_0));
  LUT6 #(
    .INIT(64'h6996996699669669)) 
    i___106_carry__2_i_7
       (.I0(i___106_carry__2_i_3_n_0),
        .I1(\string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_6 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___80_carry_n_4 ),
        .I4(\string_cent_decenas[1]5_inferred__0/i___80_carry_n_5 ),
        .I5(\string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_7 ),
        .O(i___106_carry__2_i_7_n_0));
  LUT6 #(
    .INIT(64'h0E7070F1F18F8F0E)) 
    i___106_carry__2_i_8
       (.I0(\string_cent_decenas[1]5_inferred__0/i___80_carry_n_7 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_5 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___80_carry_n_6 ),
        .I4(\string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_4 ),
        .I5(i___106_carry__2_i_10_n_0),
        .O(i___106_carry__2_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'h96)) 
    i___106_carry__2_i_9
       (.I0(\string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_7 ),
        .O(i___106_carry__2_i_9_n_0));
  LUT4 #(
    .INIT(16'h1062)) 
    i___106_carry__3_i_1
       (.I0(\string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_7 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_4 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0 ),
        .O(i___106_carry__3_i_1_n_0));
  LUT4 #(
    .INIT(16'h1062)) 
    i___106_carry__3_i_2
       (.I0(\string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_4 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_5 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0 ),
        .O(i___106_carry__3_i_2_n_0));
  LUT4 #(
    .INIT(16'h1062)) 
    i___106_carry__3_i_3
       (.I0(\string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_5 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_6 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0 ),
        .O(i___106_carry__3_i_3_n_0));
  LUT4 #(
    .INIT(16'h1062)) 
    i___106_carry__3_i_4
       (.I0(\string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_6 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_7 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0 ),
        .O(i___106_carry__3_i_4_n_0));
  LUT5 #(
    .INIT(32'hFEF80107)) 
    i___106_carry__3_i_5
       (.I0(\string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_4 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_7 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1 ),
        .I4(\string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_6 ),
        .O(i___106_carry__3_i_5_n_0));
  LUT5 #(
    .INIT(32'hFEF80107)) 
    i___106_carry__3_i_6
       (.I0(\string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_5 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_4 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1 ),
        .I4(\string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_7 ),
        .O(i___106_carry__3_i_6_n_0));
  LUT5 #(
    .INIT(32'hFF00E817)) 
    i___106_carry__3_i_7
       (.I0(\string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_6 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_4 ),
        .I4(\string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_5 ),
        .O(i___106_carry__3_i_7_n_0));
  LUT5 #(
    .INIT(32'hFEF80107)) 
    i___106_carry__3_i_8
       (.I0(\string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_7 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_6 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1 ),
        .I4(\string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_5 ),
        .O(i___106_carry__3_i_8_n_0));
  LUT3 #(
    .INIT(8'h48)) 
    i___106_carry__4_i_1
       (.I0(\string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_1 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0 ),
        .O(i___106_carry__4_i_1_n_0));
  LUT4 #(
    .INIT(16'h1062)) 
    i___106_carry__4_i_2
       (.I0(\string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_1 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_6 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0 ),
        .O(i___106_carry__4_i_2_n_0));
  LUT4 #(
    .INIT(16'h1062)) 
    i___106_carry__4_i_3
       (.I0(\string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_6 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_7 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0 ),
        .O(i___106_carry__4_i_3_n_0));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT2 #(
    .INIT(4'h7)) 
    i___106_carry__4_i_4
       (.I0(\string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0 ),
        .O(i___106_carry__4_i_4_n_0));
  LUT3 #(
    .INIT(8'h17)) 
    i___106_carry__4_i_5
       (.I0(\string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_1 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0 ),
        .O(i___106_carry__4_i_5_n_0));
  LUT4 #(
    .INIT(16'h0107)) 
    i___106_carry__4_i_6
       (.I0(\string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_6 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_1 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1 ),
        .O(i___106_carry__4_i_6_n_0));
  LUT5 #(
    .INIT(32'hFEF80107)) 
    i___106_carry__4_i_7
       (.I0(\string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_7 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_6 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1 ),
        .I4(\string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_1 ),
        .O(i___106_carry__4_i_7_n_0));
  LUT2 #(
    .INIT(4'h7)) 
    i___106_carry__5_i_1
       (.I0(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1 ),
        .O(i___106_carry__5_i_1_n_0));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT2 #(
    .INIT(4'h7)) 
    i___106_carry__5_i_2
       (.I0(\string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0 ),
        .O(i___106_carry__5_i_2_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    i___106_carry_i_1
       (.I0(\string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_5 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___27_carry_n_4 ),
        .O(i___106_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    i___106_carry_i_2
       (.I0(\string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_6 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___27_carry_n_5 ),
        .O(i___106_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    i___106_carry_i_3
       (.I0(\string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_7 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___27_carry_n_6 ),
        .O(i___106_carry_i_3_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    i___106_carry_i_4
       (.I0(\total_reg[0] ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_4 ),
        .O(i___106_carry_i_4_n_0));
  (* HLUTNM = "lutpair57" *) 
  LUT4 #(
    .INIT(16'h9666)) 
    i___106_carry_i_5
       (.I0(\string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_4 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_7 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___27_carry_n_4 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_5 ),
        .O(i___106_carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h8778)) 
    i___106_carry_i_6
       (.I0(\string_cent_decenas[1]5_inferred__0/i___27_carry_n_5 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_6 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___27_carry_n_4 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_5 ),
        .O(i___106_carry_i_6_n_0));
  LUT4 #(
    .INIT(16'h8778)) 
    i___106_carry_i_7
       (.I0(\string_cent_decenas[1]5_inferred__0/i___27_carry_n_6 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_7 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___27_carry_n_5 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_6 ),
        .O(i___106_carry_i_7_n_0));
  LUT4 #(
    .INIT(16'h8778)) 
    i___106_carry_i_8
       (.I0(\string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_4 ),
        .I1(\total_reg[0] ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___27_carry_n_6 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_7 ),
        .O(i___106_carry_i_8_n_0));
  LUT2 #(
    .INIT(4'hB)) 
    i___163_carry_i_1
       (.I0(\string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_4 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_7 ),
        .O(i___163_carry_i_1_n_0));
  LUT5 #(
    .INIT(32'h66969969)) 
    i___163_carry_i_2
       (.I0(\string_cent_decenas[1]5_inferred__0/i___106_carry__5_n_6 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_5 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___106_carry__5_n_7 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_6 ),
        .I4(\string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_7 ),
        .O(i___163_carry_i_2_n_0));
  LUT4 #(
    .INIT(16'h2DD2)) 
    i___163_carry_i_3
       (.I0(\string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_7 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_4 ),
        .I2(\string_cent_decenas[1]5_inferred__0/i___106_carry__5_n_7 ),
        .I3(\string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_6 ),
        .O(i___163_carry_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i___163_carry_i_4
       (.I0(\string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_4 ),
        .I1(\string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_7 ),
        .O(i___163_carry_i_4_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i___169_carry__0_i_1
       (.I0(\string_cent_decenas[1]5_inferred__0/i___163_carry_n_5 ),
        .O(i___169_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    i___169_carry__0_i_2
       (.I0(cent[6]),
        .I1(\string_cent_decenas[1]5_inferred__0/i___163_carry_n_6 ),
        .O(i___169_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    i___169_carry__0_i_3
       (.I0(cent[5]),
        .I1(\string_cent_decenas[1]5_inferred__0/i___163_carry_n_7 ),
        .O(i___169_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    i___169_carry__0_i_4
       (.I0(cent[4]),
        .I1(\string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_5 ),
        .O(i___169_carry__0_i_4_n_0));
  LUT3 #(
    .INIT(8'hB8)) 
    i___169_carry_i_2
       (.I0(Q[1]),
        .I1(\total_reg[15] ),
        .I2(p_1_in[1]),
        .O(i___169_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    i___169_carry_i_3
       (.I0(cent[3]),
        .I1(\string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_6 ),
        .O(i___169_carry_i_3_n_0));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT2 #(
    .INIT(4'h9)) 
    i___169_carry_i_4
       (.I0(i___0_carry_i_2_n_0),
        .I1(\string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_7 ),
        .O(i___169_carry_i_4_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i___169_carry_i_6
       (.I0(\total_reg[0] ),
        .O(i___169_carry_i_6_n_0));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT3 #(
    .INIT(8'h4D)) 
    i___27_carry__0_i_1
       (.I0(cent[4]),
        .I1(i___0_carry_i_2_n_0),
        .I2(cent[6]),
        .O(i___27_carry__0_i_1_n_0));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT3 #(
    .INIT(8'h17)) 
    i___27_carry__0_i_2
       (.I0(cent[3]),
        .I1(cent[5]),
        .I2(i___169_carry_i_5_n_0),
        .O(i___27_carry__0_i_2_n_0));
  (* HLUTNM = "lutpair60" *) 
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT3 #(
    .INIT(8'h2B)) 
    i___27_carry__0_i_3
       (.I0(\total_reg[0] ),
        .I1(i___0_carry_i_2_n_0),
        .I2(cent[4]),
        .O(i___27_carry__0_i_3_n_0));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT2 #(
    .INIT(4'h2)) 
    i___27_carry__0_i_4
       (.I0(i___169_carry_i_5_n_0),
        .I1(cent[3]),
        .O(i___27_carry__0_i_4_n_0));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT5 #(
    .INIT(32'h4DB2B24D)) 
    i___27_carry__0_i_5
       (.I0(cent[6]),
        .I1(i___0_carry_i_2_n_0),
        .I2(cent[4]),
        .I3(cent[5]),
        .I4(cent[3]),
        .O(i___27_carry__0_i_5_n_0));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT6 #(
    .INIT(64'hE81717E817E8E817)) 
    i___27_carry__0_i_6
       (.I0(i___169_carry_i_5_n_0),
        .I1(cent[5]),
        .I2(cent[3]),
        .I3(cent[4]),
        .I4(i___0_carry_i_2_n_0),
        .I5(cent[6]),
        .O(i___27_carry__0_i_6_n_0));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT4 #(
    .INIT(16'h9669)) 
    i___27_carry__0_i_7
       (.I0(i___27_carry__0_i_3_n_0),
        .I1(cent[3]),
        .I2(i___169_carry_i_5_n_0),
        .I3(cent[5]),
        .O(i___27_carry__0_i_7_n_0));
  (* HLUTNM = "lutpair60" *) 
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT5 #(
    .INIT(32'h96699696)) 
    i___27_carry__0_i_8
       (.I0(\total_reg[0] ),
        .I1(i___0_carry_i_2_n_0),
        .I2(cent[4]),
        .I3(cent[3]),
        .I4(i___169_carry_i_5_n_0),
        .O(i___27_carry__0_i_8_n_0));
  LUT2 #(
    .INIT(4'hB)) 
    i___27_carry__1_i_1
       (.I0(cent[4]),
        .I1(cent[6]),
        .O(i___27_carry__1_i_1_n_0));
  LUT2 #(
    .INIT(4'hB)) 
    i___27_carry__1_i_2
       (.I0(cent[3]),
        .I1(cent[5]),
        .O(i___27_carry__1_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i___27_carry__1_i_3
       (.I0(cent[6]),
        .O(i___27_carry__1_i_3_n_0));
  LUT3 #(
    .INIT(8'h4B)) 
    i___27_carry__1_i_4
       (.I0(cent[4]),
        .I1(cent[6]),
        .I2(cent[5]),
        .O(i___27_carry__1_i_4_n_0));
  LUT4 #(
    .INIT(16'hD22D)) 
    i___27_carry__1_i_5
       (.I0(cent[5]),
        .I1(cent[3]),
        .I2(cent[6]),
        .I3(cent[4]),
        .O(i___27_carry__1_i_5_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i___27_carry_i_1
       (.I0(\total_reg[0] ),
        .O(i___27_carry_i_1_n_0));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT3 #(
    .INIT(8'h96)) 
    i___27_carry_i_2
       (.I0(\total_reg[0] ),
        .I1(cent[3]),
        .I2(i___169_carry_i_5_n_0),
        .O(i___27_carry_i_2_n_0));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT2 #(
    .INIT(4'h9)) 
    i___27_carry_i_3
       (.I0(\total_reg[0] ),
        .I1(i___0_carry_i_2_n_0),
        .O(i___27_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFEF00000020)) 
    i___27_carry_i_5
       (.I0(Q[0]),
        .I1(\string_cent_decenas[1]5_carry_i_8_n_0 ),
        .I2(\string_cent_decenas[1]5_carry_i_9_n_0 ),
        .I3(\total_reg[20] ),
        .I4(\string_cent_decenas[1]5_carry_i_11_n_0 ),
        .I5(p_1_in[0]),
        .O(i___27_carry_i_5_n_0));
  LUT3 #(
    .INIT(8'hB8)) 
    i___56_carry__0_i_2
       (.I0(Q[1]),
        .I1(\total_reg[15] ),
        .I2(p_1_in[1]),
        .O(i___56_carry__0_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i___56_carry__0_i_3
       (.I0(cent[4]),
        .O(i___56_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    i___56_carry__0_i_4
       (.I0(cent[3]),
        .I1(cent[6]),
        .O(i___56_carry__0_i_4_n_0));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT2 #(
    .INIT(4'h9)) 
    i___56_carry__0_i_5
       (.I0(i___0_carry_i_2_n_0),
        .I1(cent[5]),
        .O(i___56_carry__0_i_5_n_0));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT2 #(
    .INIT(4'h6)) 
    i___56_carry__0_i_6
       (.I0(i___169_carry_i_5_n_0),
        .I1(cent[4]),
        .O(i___56_carry__0_i_6_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i___56_carry__1_i_1
       (.I0(cent[6]),
        .O(i___56_carry__1_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i___56_carry__1_i_2
       (.I0(cent[5]),
        .O(i___56_carry__1_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    i___56_carry_i_1
       (.I0(\total_reg[0] ),
        .I1(cent[3]),
        .O(i___56_carry_i_1_n_0));
  LUT4 #(
    .INIT(16'h553C)) 
    i___56_carry_i_2
       (.I0(Q[2]),
        .I1(euros1__339_carry_n_5),
        .I2(\string_cent_decenas[1]5__186_carry_i_8_n_0 ),
        .I3(\total_reg[15] ),
        .O(i___56_carry_i_2_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFEF00000020)) 
    i___56_carry_i_4
       (.I0(Q[0]),
        .I1(\string_cent_decenas[1]5_carry_i_8_n_0 ),
        .I2(\string_cent_decenas[1]5_carry_i_9_n_0 ),
        .I3(\total_reg[20] ),
        .I4(\string_cent_decenas[1]5_carry_i_11_n_0 ),
        .I5(p_1_in[0]),
        .O(i___56_carry_i_4_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    i___80_carry__0_i_1
       (.I0(cent[3]),
        .I1(cent[5]),
        .O(i___80_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i___80_carry__0_i_2
       (.I0(cent[5]),
        .I1(cent[3]),
        .O(i___80_carry__0_i_2_n_0));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT2 #(
    .INIT(4'h2)) 
    i___80_carry__0_i_3
       (.I0(cent[3]),
        .I1(i___169_carry_i_5_n_0),
        .O(i___80_carry__0_i_3_n_0));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT2 #(
    .INIT(4'h9)) 
    i___80_carry__0_i_4
       (.I0(i___169_carry_i_5_n_0),
        .I1(cent[3]),
        .O(i___80_carry__0_i_4_n_0));
  LUT4 #(
    .INIT(16'h8778)) 
    i___80_carry__0_i_5
       (.I0(cent[5]),
        .I1(cent[3]),
        .I2(cent[4]),
        .I3(cent[6]),
        .O(i___80_carry__0_i_5_n_0));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT4 #(
    .INIT(16'h8778)) 
    i___80_carry__0_i_6
       (.I0(i___0_carry_i_2_n_0),
        .I1(cent[4]),
        .I2(cent[3]),
        .I3(cent[5]),
        .O(i___80_carry__0_i_6_n_0));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT4 #(
    .INIT(16'h4BB4)) 
    i___80_carry__0_i_7
       (.I0(i___169_carry_i_5_n_0),
        .I1(cent[3]),
        .I2(cent[4]),
        .I3(i___0_carry_i_2_n_0),
        .O(i___80_carry__0_i_7_n_0));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT4 #(
    .INIT(16'h6999)) 
    i___80_carry__0_i_8
       (.I0(cent[3]),
        .I1(i___169_carry_i_5_n_0),
        .I2(i___0_carry_i_2_n_0),
        .I3(\total_reg[0] ),
        .O(i___80_carry__0_i_8_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    i___80_carry__1_i_1
       (.I0(cent[4]),
        .I1(cent[6]),
        .O(i___80_carry__1_i_1_n_0));
  LUT6 #(
    .INIT(64'hBBBB8B8B8888B888)) 
    i___80_carry__1_i_2
       (.I0(Q[6]),
        .I1(\total_reg[15] ),
        .I2(euros1__339_carry__0_n_6),
        .I3(euros1__339_carry__0_n_4),
        .I4(\string_cent_decenas[1]5__27_carry__1_i_4_n_0 ),
        .I5(euros1__339_carry__0_n_5),
        .O(i___80_carry__1_i_2_n_0));
  LUT3 #(
    .INIT(8'h78)) 
    i___80_carry__1_i_3
       (.I0(cent[6]),
        .I1(cent[4]),
        .I2(cent[5]),
        .O(i___80_carry__1_i_3_n_0));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT3 #(
    .INIT(8'h96)) 
    i___80_carry_i_1
       (.I0(\total_reg[0] ),
        .I1(i___0_carry_i_2_n_0),
        .I2(cent[6]),
        .O(i___80_carry_i_1_n_0));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT2 #(
    .INIT(4'h9)) 
    i___80_carry_i_2
       (.I0(cent[5]),
        .I1(i___169_carry_i_5_n_0),
        .O(i___80_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    i___80_carry_i_3
       (.I0(cent[4]),
        .I1(\total_reg[0] ),
        .O(i___80_carry_i_3_n_0));
  LUT5 #(
    .INIT(32'hAAAACCC3)) 
    i___80_carry_i_4
       (.I0(Q[3]),
        .I1(euros1__339_carry_n_4),
        .I2(\string_cent_decenas[1]5__186_carry_i_8_n_0 ),
        .I3(euros1__339_carry_n_5),
        .I4(\total_reg[15] ),
        .O(i___80_carry_i_4_n_0));
  (* OPT_MODIFIED = "PROPCONST SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5__100_carry 
       (.CI(1'b0),
        .CO({\string_cent_decenas[1]5__100_carry_n_0 ,\NLW_string_cent_decenas[1]5__100_carry_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\string_cent_decenas[1]5__100_carry_i_1_n_0 ,\string_cent_decenas[1]5__100_carry_i_2_n_0 ,\string_cent_decenas[1]5__100_carry_i_3_n_0 ,1'b0}),
        .O({\string_cent_decenas[1]5__100_carry_n_4 ,\string_cent_decenas[1]5__100_carry_n_5 ,\string_cent_decenas[1]5__100_carry_n_6 ,\string_cent_decenas[1]5__100_carry_n_7 }),
        .S({\string_cent_decenas[1]5__100_carry_i_4_n_0 ,\string_cent_decenas[1]5__100_carry_i_5_n_0 ,\string_cent_decenas[1]5__100_carry_i_6_n_0 ,\string_cent_decenas[1]5__100_carry_i_7_n_0 }));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5__100_carry__0 
       (.CI(\string_cent_decenas[1]5__100_carry_n_0 ),
        .CO({\string_cent_decenas[1]5__100_carry__0_n_0 ,\NLW_string_cent_decenas[1]5__100_carry__0_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\string_cent_decenas[1]5__100_carry__0_i_1_n_0 ,\string_cent_decenas[1]5__100_carry__0_i_2_n_0 ,\string_cent_decenas[1]5__100_carry__0_i_3_n_0 ,\string_cent_decenas[1]5__100_carry__0_i_4_n_0 }),
        .O({\string_cent_decenas[1]5__100_carry__0_n_4 ,\string_cent_decenas[1]5__100_carry__0_n_5 ,\string_cent_decenas[1]5__100_carry__0_n_6 ,\string_cent_decenas[1]5__100_carry__0_n_7 }),
        .S({\string_cent_decenas[1]5__100_carry__0_i_5_n_0 ,\string_cent_decenas[1]5__100_carry__0_i_6_n_0 ,\string_cent_decenas[1]5__100_carry__0_i_7_n_0 ,\string_cent_decenas[1]5__100_carry__0_i_8_n_0 }));
  LUT3 #(
    .INIT(8'h4D)) 
    \string_cent_decenas[1]5__100_carry__0_i_1 
       (.I0(cent[6]),
        .I1(cent[4]),
        .I2(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .O(\string_cent_decenas[1]5__100_carry__0_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h2B)) 
    \string_cent_decenas[1]5__100_carry__0_i_2 
       (.I0(cent[3]),
        .I1(cent[5]),
        .I2(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .O(\string_cent_decenas[1]5__100_carry__0_i_2_n_0 ));
  (* HLUTNM = "lutpair51" *) 
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT3 #(
    .INIT(8'h4D)) 
    \string_cent_decenas[1]5__100_carry__0_i_3 
       (.I0(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .I1(i___0_carry_i_2_n_0),
        .I2(cent[4]),
        .O(\string_cent_decenas[1]5__100_carry__0_i_3_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT3 #(
    .INIT(8'h17)) 
    \string_cent_decenas[1]5__100_carry__0_i_4 
       (.I0(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .I1(i___169_carry_i_5_n_0),
        .I2(cent[3]),
        .O(\string_cent_decenas[1]5__100_carry__0_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h2D4B)) 
    \string_cent_decenas[1]5__100_carry__0_i_5 
       (.I0(cent[4]),
        .I1(cent[6]),
        .I2(cent[5]),
        .I3(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .O(\string_cent_decenas[1]5__100_carry__0_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hB44BD22D)) 
    \string_cent_decenas[1]5__100_carry__0_i_6 
       (.I0(cent[5]),
        .I1(cent[3]),
        .I2(cent[4]),
        .I3(cent[6]),
        .I4(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .O(\string_cent_decenas[1]5__100_carry__0_i_6_n_0 ));
  LUT4 #(
    .INIT(16'h6996)) 
    \string_cent_decenas[1]5__100_carry__0_i_7 
       (.I0(\string_cent_decenas[1]5__100_carry__0_i_3_n_0 ),
        .I1(cent[3]),
        .I2(cent[5]),
        .I3(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .O(\string_cent_decenas[1]5__100_carry__0_i_7_n_0 ));
  (* HLUTNM = "lutpair51" *) 
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    \string_cent_decenas[1]5__100_carry__0_i_8 
       (.I0(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .I1(i___0_carry_i_2_n_0),
        .I2(cent[4]),
        .I3(\string_cent_decenas[1]5__100_carry__0_i_4_n_0 ),
        .O(\string_cent_decenas[1]5__100_carry__0_i_8_n_0 ));
  CARRY4 \string_cent_decenas[1]5__100_carry__1 
       (.CI(\string_cent_decenas[1]5__100_carry__0_n_0 ),
        .CO(\NLW_string_cent_decenas[1]5__100_carry__1_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_string_cent_decenas[1]5__100_carry__1_O_UNCONNECTED [3:1],\string_cent_decenas[1]5__100_carry__1_n_7 }),
        .S({1'b0,1'b0,1'b0,\string_cent_decenas[1]5__100_carry__1_i_1_n_0 }));
  LUT3 #(
    .INIT(8'h93)) 
    \string_cent_decenas[1]5__100_carry__1_i_1 
       (.I0(cent[5]),
        .I1(cent[6]),
        .I2(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .O(\string_cent_decenas[1]5__100_carry__1_i_1_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT3 #(
    .INIT(8'h2B)) 
    \string_cent_decenas[1]5__100_carry_i_1 
       (.I0(\total_reg[0] ),
        .I1(i___0_carry_i_2_n_0),
        .I2(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .O(\string_cent_decenas[1]5__100_carry_i_1_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \string_cent_decenas[1]5__100_carry_i_2 
       (.I0(\total_reg[0] ),
        .I1(i___0_carry_i_2_n_0),
        .I2(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .O(\string_cent_decenas[1]5__100_carry_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h7)) 
    \string_cent_decenas[1]5__100_carry_i_3 
       (.I0(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .I1(\total_reg[0] ),
        .O(\string_cent_decenas[1]5__100_carry_i_3_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT4 #(
    .INIT(16'h9669)) 
    \string_cent_decenas[1]5__100_carry_i_4 
       (.I0(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .I1(i___169_carry_i_5_n_0),
        .I2(cent[3]),
        .I3(\string_cent_decenas[1]5__100_carry_i_1_n_0 ),
        .O(\string_cent_decenas[1]5__100_carry_i_4_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT4 #(
    .INIT(16'h9996)) 
    \string_cent_decenas[1]5__100_carry_i_5 
       (.I0(\total_reg[0] ),
        .I1(i___0_carry_i_2_n_0),
        .I2(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .I3(i___169_carry_i_5_n_0),
        .O(\string_cent_decenas[1]5__100_carry_i_5_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT3 #(
    .INIT(8'h9C)) 
    \string_cent_decenas[1]5__100_carry_i_6 
       (.I0(\total_reg[0] ),
        .I1(i___169_carry_i_5_n_0),
        .I2(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .O(\string_cent_decenas[1]5__100_carry_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \string_cent_decenas[1]5__100_carry_i_7 
       (.I0(\total_reg[0] ),
        .I1(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .O(\string_cent_decenas[1]5__100_carry_i_7_n_0 ));
  CARRY4 \string_cent_decenas[1]5__100_carry_i_8 
       (.CI(\string_cent_decenas[1]5_carry__1_n_0 ),
        .CO({\NLW_string_cent_decenas[1]5__100_carry_i_8_CO_UNCONNECTED [3:1],\string_cent_decenas[1]5__100_carry_i_8_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_string_cent_decenas[1]5__100_carry_i_8_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,1'b1}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5__124_carry 
       (.CI(1'b0),
        .CO({\string_cent_decenas[1]5__124_carry_n_0 ,\NLW_string_cent_decenas[1]5__124_carry_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\string_cent_decenas[1]5__124_carry_i_1_n_0 ,\string_cent_decenas[1]5__124_carry_i_2_n_0 ,\string_cent_decenas[1]5__124_carry_i_3_n_0 ,\string_cent_decenas[1]5__124_carry_i_4_n_0 }),
        .O(\NLW_string_cent_decenas[1]5__124_carry_O_UNCONNECTED [3:0]),
        .S({\string_cent_decenas[1]5__124_carry_i_5_n_0 ,\string_cent_decenas[1]5__124_carry_i_6_n_0 ,\string_cent_decenas[1]5__124_carry_i_7_n_0 ,\string_cent_decenas[1]5__124_carry_i_8_n_0 }));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5__124_carry__0 
       (.CI(\string_cent_decenas[1]5__124_carry_n_0 ),
        .CO({\string_cent_decenas[1]5__124_carry__0_n_0 ,\NLW_string_cent_decenas[1]5__124_carry__0_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\string_cent_decenas[1]5__124_carry__0_i_1_n_0 ,\string_cent_decenas[1]5__124_carry__0_i_2_n_0 ,\string_cent_decenas[1]5__124_carry__0_i_3_n_0 ,\string_cent_decenas[1]5__124_carry__0_i_4_n_0 }),
        .O(\NLW_string_cent_decenas[1]5__124_carry__0_O_UNCONNECTED [3:0]),
        .S({\string_cent_decenas[1]5__124_carry__0_i_5_n_0 ,\string_cent_decenas[1]5__124_carry__0_i_6_n_0 ,\string_cent_decenas[1]5__124_carry__0_i_7_n_0 ,\string_cent_decenas[1]5__124_carry__0_i_8_n_0 }));
  (* HLUTNM = "lutpair53" *) 
  LUT3 #(
    .INIT(8'h82)) 
    \string_cent_decenas[1]5__124_carry__0_i_1 
       (.I0(\string_cent_decenas[1]5__27_carry__0_n_7 ),
        .I1(\string_cent_decenas[1]5_carry_n_7 ),
        .I2(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .O(\string_cent_decenas[1]5__124_carry__0_i_1_n_0 ));
  (* HLUTNM = "lutpair52" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \string_cent_decenas[1]5__124_carry__0_i_2 
       (.I0(\string_cent_decenas[1]5__27_carry_n_4 ),
        .I1(\string_cent_decenas[1]5_carry__1_n_4 ),
        .O(\string_cent_decenas[1]5__124_carry__0_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \string_cent_decenas[1]5__124_carry__0_i_3 
       (.I0(\string_cent_decenas[1]5__27_carry_n_5 ),
        .I1(\string_cent_decenas[1]5_carry__1_n_5 ),
        .O(\string_cent_decenas[1]5__124_carry__0_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \string_cent_decenas[1]5__124_carry__0_i_4 
       (.I0(\string_cent_decenas[1]5__27_carry_n_6 ),
        .I1(\string_cent_decenas[1]5_carry__1_n_6 ),
        .O(\string_cent_decenas[1]5__124_carry__0_i_4_n_0 ));
  (* HLUTNM = "lutpair54" *) 
  LUT5 #(
    .INIT(32'h99966669)) 
    \string_cent_decenas[1]5__124_carry__0_i_5 
       (.I0(\string_cent_decenas[1]5__27_carry__0_n_6 ),
        .I1(\string_cent_decenas[1]5__55_carry_n_6 ),
        .I2(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .I3(\string_cent_decenas[1]5_carry_n_7 ),
        .I4(\string_cent_decenas[1]5__124_carry__0_i_1_n_0 ),
        .O(\string_cent_decenas[1]5__124_carry__0_i_5_n_0 ));
  (* HLUTNM = "lutpair53" *) 
  LUT4 #(
    .INIT(16'h9669)) 
    \string_cent_decenas[1]5__124_carry__0_i_6 
       (.I0(\string_cent_decenas[1]5__27_carry__0_n_7 ),
        .I1(\string_cent_decenas[1]5_carry_n_7 ),
        .I2(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .I3(\string_cent_decenas[1]5__124_carry__0_i_2_n_0 ),
        .O(\string_cent_decenas[1]5__124_carry__0_i_6_n_0 ));
  (* HLUTNM = "lutpair52" *) 
  LUT4 #(
    .INIT(16'h9666)) 
    \string_cent_decenas[1]5__124_carry__0_i_7 
       (.I0(\string_cent_decenas[1]5__27_carry_n_4 ),
        .I1(\string_cent_decenas[1]5_carry__1_n_4 ),
        .I2(\string_cent_decenas[1]5_carry__1_n_5 ),
        .I3(\string_cent_decenas[1]5__27_carry_n_5 ),
        .O(\string_cent_decenas[1]5__124_carry__0_i_7_n_0 ));
  LUT4 #(
    .INIT(16'h8778)) 
    \string_cent_decenas[1]5__124_carry__0_i_8 
       (.I0(\string_cent_decenas[1]5_carry__1_n_6 ),
        .I1(\string_cent_decenas[1]5__27_carry_n_6 ),
        .I2(\string_cent_decenas[1]5_carry__1_n_5 ),
        .I3(\string_cent_decenas[1]5__27_carry_n_5 ),
        .O(\string_cent_decenas[1]5__124_carry__0_i_8_n_0 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5__124_carry__1 
       (.CI(\string_cent_decenas[1]5__124_carry__0_n_0 ),
        .CO({\string_cent_decenas[1]5__124_carry__1_n_0 ,\NLW_string_cent_decenas[1]5__124_carry__1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\string_cent_decenas[1]5__124_carry__1_i_1_n_0 ,\string_cent_decenas[1]5__124_carry__1_i_2_n_0 ,\string_cent_decenas[1]5__124_carry__1_i_3_n_0 ,\string_cent_decenas[1]5__124_carry__1_i_4_n_0 }),
        .O(\NLW_string_cent_decenas[1]5__124_carry__1_O_UNCONNECTED [3:0]),
        .S({\string_cent_decenas[1]5__124_carry__1_i_5_n_0 ,\string_cent_decenas[1]5__124_carry__1_i_6_n_0 ,\string_cent_decenas[1]5__124_carry__1_i_7_n_0 ,\string_cent_decenas[1]5__124_carry__1_i_8_n_0 }));
  LUT4 #(
    .INIT(16'h8B82)) 
    \string_cent_decenas[1]5__124_carry__1_i_1 
       (.I0(\string_cent_decenas[1]5__27_carry__1_n_7 ),
        .I1(\string_cent_decenas[1]5__55_carry__0_n_7 ),
        .I2(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .I3(\string_cent_decenas[1]5__55_carry_n_4 ),
        .O(\string_cent_decenas[1]5__124_carry__1_i_1_n_0 ));
  (* HLUTNM = "lutpair56" *) 
  LUT4 #(
    .INIT(16'h8B82)) 
    \string_cent_decenas[1]5__124_carry__1_i_2 
       (.I0(\string_cent_decenas[1]5__27_carry__0_n_4 ),
        .I1(\string_cent_decenas[1]5__55_carry_n_4 ),
        .I2(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .I3(\string_cent_decenas[1]5__55_carry_n_5 ),
        .O(\string_cent_decenas[1]5__124_carry__1_i_2_n_0 ));
  (* HLUTNM = "lutpair55" *) 
  LUT4 #(
    .INIT(16'h8B82)) 
    \string_cent_decenas[1]5__124_carry__1_i_3 
       (.I0(\string_cent_decenas[1]5__27_carry__0_n_5 ),
        .I1(\string_cent_decenas[1]5__55_carry_n_5 ),
        .I2(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .I3(\string_cent_decenas[1]5__55_carry_n_6 ),
        .O(\string_cent_decenas[1]5__124_carry__1_i_3_n_0 ));
  (* HLUTNM = "lutpair54" *) 
  LUT4 #(
    .INIT(16'h8B82)) 
    \string_cent_decenas[1]5__124_carry__1_i_4 
       (.I0(\string_cent_decenas[1]5__27_carry__0_n_6 ),
        .I1(\string_cent_decenas[1]5__55_carry_n_6 ),
        .I2(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .I3(\string_cent_decenas[1]5_carry_n_7 ),
        .O(\string_cent_decenas[1]5__124_carry__1_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h3CC3F00F87781EE1)) 
    \string_cent_decenas[1]5__124_carry__1_i_5 
       (.I0(\string_cent_decenas[1]5__55_carry_n_4 ),
        .I1(\string_cent_decenas[1]5__27_carry__1_n_7 ),
        .I2(\string_cent_decenas[1]5__27_carry__1_n_6 ),
        .I3(\string_cent_decenas[1]5__124_carry__1_i_9_n_0 ),
        .I4(\string_cent_decenas[1]5__55_carry__0_n_7 ),
        .I5(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .O(\string_cent_decenas[1]5__124_carry__1_i_5_n_0 ));
  LUT5 #(
    .INIT(32'h96969669)) 
    \string_cent_decenas[1]5__124_carry__1_i_6 
       (.I0(\string_cent_decenas[1]5__124_carry__1_i_2_n_0 ),
        .I1(\string_cent_decenas[1]5__27_carry__1_n_7 ),
        .I2(\string_cent_decenas[1]5__55_carry__0_n_7 ),
        .I3(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .I4(\string_cent_decenas[1]5__55_carry_n_4 ),
        .O(\string_cent_decenas[1]5__124_carry__1_i_6_n_0 ));
  (* HLUTNM = "lutpair56" *) 
  LUT5 #(
    .INIT(32'h99966669)) 
    \string_cent_decenas[1]5__124_carry__1_i_7 
       (.I0(\string_cent_decenas[1]5__27_carry__0_n_4 ),
        .I1(\string_cent_decenas[1]5__55_carry_n_4 ),
        .I2(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .I3(\string_cent_decenas[1]5__55_carry_n_5 ),
        .I4(\string_cent_decenas[1]5__124_carry__1_i_3_n_0 ),
        .O(\string_cent_decenas[1]5__124_carry__1_i_7_n_0 ));
  (* HLUTNM = "lutpair55" *) 
  LUT5 #(
    .INIT(32'h99966669)) 
    \string_cent_decenas[1]5__124_carry__1_i_8 
       (.I0(\string_cent_decenas[1]5__27_carry__0_n_5 ),
        .I1(\string_cent_decenas[1]5__55_carry_n_5 ),
        .I2(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .I3(\string_cent_decenas[1]5__55_carry_n_6 ),
        .I4(\string_cent_decenas[1]5__124_carry__1_i_4_n_0 ),
        .O(\string_cent_decenas[1]5__124_carry__1_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \string_cent_decenas[1]5__124_carry__1_i_9 
       (.I0(\string_cent_decenas[1]5__55_carry__0_n_6 ),
        .I1(\total_reg[0] ),
        .I2(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .O(\string_cent_decenas[1]5__124_carry__1_i_9_n_0 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5__124_carry__2 
       (.CI(\string_cent_decenas[1]5__124_carry__1_n_0 ),
        .CO({\string_cent_decenas[1]5__124_carry__2_n_0 ,\NLW_string_cent_decenas[1]5__124_carry__2_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\string_cent_decenas[1]5__124_carry__2_i_1_n_0 ,\string_cent_decenas[1]5__124_carry__2_i_2_n_0 ,\string_cent_decenas[1]5__124_carry__2_i_3_n_0 ,\string_cent_decenas[1]5__124_carry__2_i_4_n_0 }),
        .O(\NLW_string_cent_decenas[1]5__124_carry__2_O_UNCONNECTED [3:0]),
        .S({\string_cent_decenas[1]5__124_carry__2_i_5_n_0 ,\string_cent_decenas[1]5__124_carry__2_i_6_n_0 ,\string_cent_decenas[1]5__124_carry__2_i_7_n_0 ,\string_cent_decenas[1]5__124_carry__2_i_8_n_0 }));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT6 #(
    .INIT(64'h54D580FE80FE54D5)) 
    \string_cent_decenas[1]5__124_carry__2_i_1 
       (.I0(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .I1(i___0_carry_i_2_n_0),
        .I2(\string_cent_decenas[1]5__55_carry__0_n_4 ),
        .I3(\string_cent_decenas[1]5__27_carry__1_n_1 ),
        .I4(\string_cent_decenas[1]5__55_carry__1_n_7 ),
        .I5(\string_cent_decenas[1]5__124_carry__2_i_9_n_0 ),
        .O(\string_cent_decenas[1]5__124_carry__2_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT4 #(
    .INIT(16'hB22B)) 
    \string_cent_decenas[1]5__124_carry__2_i_10 
       (.I0(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .I1(\string_cent_decenas[1]5__55_carry__1_n_7 ),
        .I2(cent[3]),
        .I3(\total_reg[0] ),
        .O(\string_cent_decenas[1]5__124_carry__2_i_10_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT4 #(
    .INIT(16'h9669)) 
    \string_cent_decenas[1]5__124_carry__2_i_11 
       (.I0(\total_reg[0] ),
        .I1(cent[3]),
        .I2(\string_cent_decenas[1]5__55_carry__1_n_7 ),
        .I3(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .O(\string_cent_decenas[1]5__124_carry__2_i_11_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'hB2)) 
    \string_cent_decenas[1]5__124_carry__2_i_12 
       (.I0(\total_reg[0] ),
        .I1(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .I2(\string_cent_decenas[1]5__55_carry__0_n_6 ),
        .O(\string_cent_decenas[1]5__124_carry__2_i_12_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \string_cent_decenas[1]5__124_carry__2_i_13 
       (.I0(\string_cent_decenas[1]5__55_carry__0_n_4 ),
        .I1(i___0_carry_i_2_n_0),
        .I2(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .O(\string_cent_decenas[1]5__124_carry__2_i_13_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'h69)) 
    \string_cent_decenas[1]5__124_carry__2_i_14 
       (.I0(\string_cent_decenas[1]5__55_carry__0_n_5 ),
        .I1(i___169_carry_i_5_n_0),
        .I2(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .O(\string_cent_decenas[1]5__124_carry__2_i_14_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT6 #(
    .INIT(64'h417D006969FF417D)) 
    \string_cent_decenas[1]5__124_carry__2_i_2 
       (.I0(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .I1(i___0_carry_i_2_n_0),
        .I2(\string_cent_decenas[1]5__55_carry__0_n_4 ),
        .I3(\string_cent_decenas[1]5__27_carry__1_n_1 ),
        .I4(\string_cent_decenas[1]5__55_carry__0_n_5 ),
        .I5(i___169_carry_i_5_n_0),
        .O(\string_cent_decenas[1]5__124_carry__2_i_2_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT6 #(
    .INIT(64'h96FF14D714D70096)) 
    \string_cent_decenas[1]5__124_carry__2_i_3 
       (.I0(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .I1(i___169_carry_i_5_n_0),
        .I2(\string_cent_decenas[1]5__55_carry__0_n_5 ),
        .I3(\string_cent_decenas[1]5__27_carry__1_n_1 ),
        .I4(\total_reg[0] ),
        .I5(\string_cent_decenas[1]5__55_carry__0_n_6 ),
        .O(\string_cent_decenas[1]5__124_carry__2_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h7D416900)) 
    \string_cent_decenas[1]5__124_carry__2_i_4 
       (.I0(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .I1(\total_reg[0] ),
        .I2(\string_cent_decenas[1]5__55_carry__0_n_6 ),
        .I3(\string_cent_decenas[1]5__27_carry__1_n_6 ),
        .I4(\string_cent_decenas[1]5__55_carry__0_n_7 ),
        .O(\string_cent_decenas[1]5__124_carry__2_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h9669699669969669)) 
    \string_cent_decenas[1]5__124_carry__2_i_5 
       (.I0(\string_cent_decenas[1]5__124_carry__2_i_1_n_0 ),
        .I1(\string_cent_decenas[1]5__124_carry__2_i_10_n_0 ),
        .I2(\string_cent_decenas[1]5__27_carry__1_n_1 ),
        .I3(\string_cent_decenas[1]5__79_carry_n_6 ),
        .I4(\string_cent_decenas[1]5__55_carry__1_n_6 ),
        .I5(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .O(\string_cent_decenas[1]5__124_carry__2_i_5_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT6 #(
    .INIT(64'hA665599A599AA665)) 
    \string_cent_decenas[1]5__124_carry__2_i_6 
       (.I0(\string_cent_decenas[1]5__124_carry__2_i_2_n_0 ),
        .I1(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .I2(i___0_carry_i_2_n_0),
        .I3(\string_cent_decenas[1]5__55_carry__0_n_4 ),
        .I4(\string_cent_decenas[1]5__27_carry__1_n_1 ),
        .I5(\string_cent_decenas[1]5__124_carry__2_i_11_n_0 ),
        .O(\string_cent_decenas[1]5__124_carry__2_i_6_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT6 #(
    .INIT(64'h9336C993C9936CC9)) 
    \string_cent_decenas[1]5__124_carry__2_i_7 
       (.I0(\string_cent_decenas[1]5__124_carry__2_i_12_n_0 ),
        .I1(\string_cent_decenas[1]5__124_carry__2_i_13_n_0 ),
        .I2(\string_cent_decenas[1]5__27_carry__1_n_1 ),
        .I3(\string_cent_decenas[1]5__55_carry__0_n_5 ),
        .I4(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .I5(i___169_carry_i_5_n_0),
        .O(\string_cent_decenas[1]5__124_carry__2_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h65A69A599A5965A6)) 
    \string_cent_decenas[1]5__124_carry__2_i_8 
       (.I0(\string_cent_decenas[1]5__124_carry__2_i_4_n_0 ),
        .I1(\total_reg[0] ),
        .I2(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .I3(\string_cent_decenas[1]5__55_carry__0_n_6 ),
        .I4(\string_cent_decenas[1]5__27_carry__1_n_1 ),
        .I5(\string_cent_decenas[1]5__124_carry__2_i_14_n_0 ),
        .O(\string_cent_decenas[1]5__124_carry__2_i_8_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \string_cent_decenas[1]5__124_carry__2_i_9 
       (.I0(cent[3]),
        .I1(\total_reg[0] ),
        .O(\string_cent_decenas[1]5__124_carry__2_i_9_n_0 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5__124_carry__3 
       (.CI(\string_cent_decenas[1]5__124_carry__2_n_0 ),
        .CO({\string_cent_decenas[1]5__124_carry__3_n_0 ,\NLW_string_cent_decenas[1]5__124_carry__3_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\string_cent_decenas[1]5__124_carry__3_i_1_n_0 ,\string_cent_decenas[1]5__124_carry__3_i_2_n_0 ,\string_cent_decenas[1]5__124_carry__3_i_3_n_0 ,\string_cent_decenas[1]5__124_carry__3_i_4_n_0 }),
        .O(\NLW_string_cent_decenas[1]5__124_carry__3_O_UNCONNECTED [3:0]),
        .S({\string_cent_decenas[1]5__124_carry__3_i_5_n_0 ,\string_cent_decenas[1]5__124_carry__3_i_6_n_0 ,\string_cent_decenas[1]5__124_carry__3_i_7_n_0 ,\string_cent_decenas[1]5__124_carry__3_i_8_n_0 }));
  LUT5 #(
    .INIT(32'hD400FFD4)) 
    \string_cent_decenas[1]5__124_carry__3_i_1 
       (.I0(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .I1(\string_cent_decenas[1]5__55_carry__1_n_4 ),
        .I2(\string_cent_decenas[1]5__79_carry_n_4 ),
        .I3(\string_cent_decenas[1]5__124_carry__3_i_9_n_0 ),
        .I4(\string_cent_decenas[1]5__27_carry__1_n_1 ),
        .O(\string_cent_decenas[1]5__124_carry__3_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'h2B)) 
    \string_cent_decenas[1]5__124_carry__3_i_10 
       (.I0(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .I1(\string_cent_decenas[1]5__55_carry__1_n_4 ),
        .I2(\string_cent_decenas[1]5__79_carry_n_4 ),
        .O(\string_cent_decenas[1]5__124_carry__3_i_10_n_0 ));
  CARRY4 \string_cent_decenas[1]5__124_carry__3_i_11 
       (.CI(\string_cent_decenas[1]5__55_carry__1_n_0 ),
        .CO({\NLW_string_cent_decenas[1]5__124_carry__3_i_11_CO_UNCONNECTED [3:1],\string_cent_decenas[1]5__124_carry__3_i_11_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_string_cent_decenas[1]5__124_carry__3_i_11_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,1'b1}));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT3 #(
    .INIT(8'h69)) 
    \string_cent_decenas[1]5__124_carry__3_i_12 
       (.I0(\string_cent_decenas[1]5__79_carry__0_n_6 ),
        .I1(\string_cent_decenas[1]5__124_carry__3_i_11_n_3 ),
        .I2(\string_cent_decenas[1]5__100_carry_n_6 ),
        .O(\string_cent_decenas[1]5__124_carry__3_i_12_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT3 #(
    .INIT(8'h2B)) 
    \string_cent_decenas[1]5__124_carry__3_i_13 
       (.I0(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .I1(\string_cent_decenas[1]5__55_carry__1_n_5 ),
        .I2(\string_cent_decenas[1]5__79_carry_n_5 ),
        .O(\string_cent_decenas[1]5__124_carry__3_i_13_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT3 #(
    .INIT(8'h69)) 
    \string_cent_decenas[1]5__124_carry__3_i_14 
       (.I0(\string_cent_decenas[1]5__79_carry_n_5 ),
        .I1(\string_cent_decenas[1]5__55_carry__1_n_5 ),
        .I2(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .O(\string_cent_decenas[1]5__124_carry__3_i_14_n_0 ));
  LUT6 #(
    .INIT(64'h54D580FE80FE54D5)) 
    \string_cent_decenas[1]5__124_carry__3_i_2 
       (.I0(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .I1(\string_cent_decenas[1]5__55_carry__1_n_5 ),
        .I2(\string_cent_decenas[1]5__79_carry_n_5 ),
        .I3(\string_cent_decenas[1]5__27_carry__1_n_1 ),
        .I4(\string_cent_decenas[1]5__55_carry__1_n_4 ),
        .I5(\string_cent_decenas[1]5__79_carry_n_4 ),
        .O(\string_cent_decenas[1]5__124_carry__3_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h54D580FE80FE54D5)) 
    \string_cent_decenas[1]5__124_carry__3_i_3 
       (.I0(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .I1(\string_cent_decenas[1]5__55_carry__1_n_6 ),
        .I2(\string_cent_decenas[1]5__79_carry_n_6 ),
        .I3(\string_cent_decenas[1]5__27_carry__1_n_1 ),
        .I4(\string_cent_decenas[1]5__55_carry__1_n_5 ),
        .I5(\string_cent_decenas[1]5__79_carry_n_5 ),
        .O(\string_cent_decenas[1]5__124_carry__3_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h54D580FE80FE54D5)) 
    \string_cent_decenas[1]5__124_carry__3_i_4 
       (.I0(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .I1(\string_cent_decenas[1]5__55_carry__1_n_7 ),
        .I2(\string_cent_decenas[1]5__124_carry__2_i_9_n_0 ),
        .I3(\string_cent_decenas[1]5__27_carry__1_n_1 ),
        .I4(\string_cent_decenas[1]5__55_carry__1_n_6 ),
        .I5(\string_cent_decenas[1]5__79_carry_n_6 ),
        .O(\string_cent_decenas[1]5__124_carry__3_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h188E8EE7E7717118)) 
    \string_cent_decenas[1]5__124_carry__3_i_5 
       (.I0(\string_cent_decenas[1]5__124_carry__3_i_10_n_0 ),
        .I1(\string_cent_decenas[1]5__124_carry__3_i_11_n_3 ),
        .I2(\string_cent_decenas[1]5__100_carry_n_7 ),
        .I3(\string_cent_decenas[1]5__79_carry__0_n_7 ),
        .I4(\string_cent_decenas[1]5__27_carry__1_n_1 ),
        .I5(\string_cent_decenas[1]5__124_carry__3_i_12_n_0 ),
        .O(\string_cent_decenas[1]5__124_carry__3_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h9696699669966969)) 
    \string_cent_decenas[1]5__124_carry__3_i_6 
       (.I0(\string_cent_decenas[1]5__124_carry__3_i_2_n_0 ),
        .I1(\string_cent_decenas[1]5__27_carry__1_n_1 ),
        .I2(\string_cent_decenas[1]5__124_carry__3_i_9_n_0 ),
        .I3(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .I4(\string_cent_decenas[1]5__55_carry__1_n_4 ),
        .I5(\string_cent_decenas[1]5__79_carry_n_4 ),
        .O(\string_cent_decenas[1]5__124_carry__3_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h9669699669969669)) 
    \string_cent_decenas[1]5__124_carry__3_i_7 
       (.I0(\string_cent_decenas[1]5__124_carry__3_i_3_n_0 ),
        .I1(\string_cent_decenas[1]5__124_carry__3_i_13_n_0 ),
        .I2(\string_cent_decenas[1]5__27_carry__1_n_1 ),
        .I3(\string_cent_decenas[1]5__79_carry_n_4 ),
        .I4(\string_cent_decenas[1]5__55_carry__1_n_4 ),
        .I5(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .O(\string_cent_decenas[1]5__124_carry__3_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h188E8EE7E7717118)) 
    \string_cent_decenas[1]5__124_carry__3_i_8 
       (.I0(\string_cent_decenas[1]5__124_carry__2_i_10_n_0 ),
        .I1(\string_cent_decenas[1]5__100_carry_i_8_n_3 ),
        .I2(\string_cent_decenas[1]5__55_carry__1_n_6 ),
        .I3(\string_cent_decenas[1]5__79_carry_n_6 ),
        .I4(\string_cent_decenas[1]5__27_carry__1_n_1 ),
        .I5(\string_cent_decenas[1]5__124_carry__3_i_14_n_0 ),
        .O(\string_cent_decenas[1]5__124_carry__3_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT3 #(
    .INIT(8'h69)) 
    \string_cent_decenas[1]5__124_carry__3_i_9 
       (.I0(\string_cent_decenas[1]5__79_carry__0_n_7 ),
        .I1(\string_cent_decenas[1]5__124_carry__3_i_11_n_3 ),
        .I2(\string_cent_decenas[1]5__100_carry_n_7 ),
        .O(\string_cent_decenas[1]5__124_carry__3_i_9_n_0 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5__124_carry__4 
       (.CI(\string_cent_decenas[1]5__124_carry__3_n_0 ),
        .CO({\string_cent_decenas[1]5__124_carry__4_n_0 ,\NLW_string_cent_decenas[1]5__124_carry__4_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\string_cent_decenas[1]5__124_carry__4_i_1_n_0 ,\string_cent_decenas[1]5__124_carry__4_i_2_n_0 ,\string_cent_decenas[1]5__124_carry__4_i_3_n_0 ,\string_cent_decenas[1]5__124_carry__4_i_4_n_0 }),
        .O({\string_cent_decenas[1]5__124_carry__4_n_4 ,\NLW_string_cent_decenas[1]5__124_carry__4_O_UNCONNECTED [2:0]}),
        .S({\string_cent_decenas[1]5__124_carry__4_i_5_n_0 ,\string_cent_decenas[1]5__124_carry__4_i_6_n_0 ,\string_cent_decenas[1]5__124_carry__4_i_7_n_0 ,\string_cent_decenas[1]5__124_carry__4_i_8_n_0 }));
  LUT6 #(
    .INIT(64'h54D580FE80FE54D5)) 
    \string_cent_decenas[1]5__124_carry__4_i_1 
       (.I0(\string_cent_decenas[1]5__124_carry__3_i_11_n_3 ),
        .I1(\string_cent_decenas[1]5__100_carry_n_4 ),
        .I2(\string_cent_decenas[1]5__79_carry__0_n_4 ),
        .I3(\string_cent_decenas[1]5__27_carry__1_n_1 ),
        .I4(\string_cent_decenas[1]5__100_carry__0_n_7 ),
        .I5(\string_cent_decenas[1]5__79_carry__1_n_7 ),
        .O(\string_cent_decenas[1]5__124_carry__4_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT3 #(
    .INIT(8'h2B)) 
    \string_cent_decenas[1]5__124_carry__4_i_10 
       (.I0(\string_cent_decenas[1]5__124_carry__3_i_11_n_3 ),
        .I1(\string_cent_decenas[1]5__100_carry_n_4 ),
        .I2(\string_cent_decenas[1]5__79_carry__0_n_4 ),
        .O(\string_cent_decenas[1]5__124_carry__4_i_10_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT3 #(
    .INIT(8'h2B)) 
    \string_cent_decenas[1]5__124_carry__4_i_11 
       (.I0(\string_cent_decenas[1]5__124_carry__3_i_11_n_3 ),
        .I1(\string_cent_decenas[1]5__100_carry_n_5 ),
        .I2(\string_cent_decenas[1]5__79_carry__0_n_5 ),
        .O(\string_cent_decenas[1]5__124_carry__4_i_11_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT3 #(
    .INIT(8'h69)) 
    \string_cent_decenas[1]5__124_carry__4_i_12 
       (.I0(\string_cent_decenas[1]5__79_carry__0_n_5 ),
        .I1(\string_cent_decenas[1]5__124_carry__3_i_11_n_3 ),
        .I2(\string_cent_decenas[1]5__100_carry_n_5 ),
        .O(\string_cent_decenas[1]5__124_carry__4_i_12_n_0 ));
  LUT6 #(
    .INIT(64'h54D580FE80FE54D5)) 
    \string_cent_decenas[1]5__124_carry__4_i_2 
       (.I0(\string_cent_decenas[1]5__124_carry__3_i_11_n_3 ),
        .I1(\string_cent_decenas[1]5__100_carry_n_5 ),
        .I2(\string_cent_decenas[1]5__79_carry__0_n_5 ),
        .I3(\string_cent_decenas[1]5__27_carry__1_n_1 ),
        .I4(\string_cent_decenas[1]5__100_carry_n_4 ),
        .I5(\string_cent_decenas[1]5__79_carry__0_n_4 ),
        .O(\string_cent_decenas[1]5__124_carry__4_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h7DD71D471D471441)) 
    \string_cent_decenas[1]5__124_carry__4_i_3 
       (.I0(\string_cent_decenas[1]5__27_carry__1_n_1 ),
        .I1(\string_cent_decenas[1]5__100_carry_n_5 ),
        .I2(\string_cent_decenas[1]5__124_carry__3_i_11_n_3 ),
        .I3(\string_cent_decenas[1]5__79_carry__0_n_5 ),
        .I4(\string_cent_decenas[1]5__100_carry_n_6 ),
        .I5(\string_cent_decenas[1]5__79_carry__0_n_6 ),
        .O(\string_cent_decenas[1]5__124_carry__4_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h54D580FE80FE54D5)) 
    \string_cent_decenas[1]5__124_carry__4_i_4 
       (.I0(\string_cent_decenas[1]5__124_carry__3_i_11_n_3 ),
        .I1(\string_cent_decenas[1]5__100_carry_n_7 ),
        .I2(\string_cent_decenas[1]5__79_carry__0_n_7 ),
        .I3(\string_cent_decenas[1]5__27_carry__1_n_1 ),
        .I4(\string_cent_decenas[1]5__100_carry_n_6 ),
        .I5(\string_cent_decenas[1]5__79_carry__0_n_6 ),
        .O(\string_cent_decenas[1]5__124_carry__4_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h9669699669969669)) 
    \string_cent_decenas[1]5__124_carry__4_i_5 
       (.I0(\string_cent_decenas[1]5__124_carry__4_i_1_n_0 ),
        .I1(\string_cent_decenas[1]5__124_carry__4_i_9_n_0 ),
        .I2(\string_cent_decenas[1]5__27_carry__1_n_1 ),
        .I3(\string_cent_decenas[1]5__79_carry__1_n_6 ),
        .I4(\string_cent_decenas[1]5__124_carry__3_i_11_n_3 ),
        .I5(\string_cent_decenas[1]5__100_carry__0_n_6 ),
        .O(\string_cent_decenas[1]5__124_carry__4_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h9669699669969669)) 
    \string_cent_decenas[1]5__124_carry__4_i_6 
       (.I0(\string_cent_decenas[1]5__124_carry__4_i_2_n_0 ),
        .I1(\string_cent_decenas[1]5__124_carry__4_i_10_n_0 ),
        .I2(\string_cent_decenas[1]5__27_carry__1_n_1 ),
        .I3(\string_cent_decenas[1]5__79_carry__1_n_7 ),
        .I4(\string_cent_decenas[1]5__124_carry__3_i_11_n_3 ),
        .I5(\string_cent_decenas[1]5__100_carry__0_n_7 ),
        .O(\string_cent_decenas[1]5__124_carry__4_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h9669699669969669)) 
    \string_cent_decenas[1]5__124_carry__4_i_7 
       (.I0(\string_cent_decenas[1]5__124_carry__4_i_3_n_0 ),
        .I1(\string_cent_decenas[1]5__124_carry__4_i_11_n_0 ),
        .I2(\string_cent_decenas[1]5__27_carry__1_n_1 ),
        .I3(\string_cent_decenas[1]5__79_carry__0_n_4 ),
        .I4(\string_cent_decenas[1]5__124_carry__3_i_11_n_3 ),
        .I5(\string_cent_decenas[1]5__100_carry_n_4 ),
        .O(\string_cent_decenas[1]5__124_carry__4_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h9696699669966969)) 
    \string_cent_decenas[1]5__124_carry__4_i_8 
       (.I0(\string_cent_decenas[1]5__124_carry__4_i_4_n_0 ),
        .I1(\string_cent_decenas[1]5__27_carry__1_n_1 ),
        .I2(\string_cent_decenas[1]5__124_carry__4_i_12_n_0 ),
        .I3(\string_cent_decenas[1]5__124_carry__3_i_11_n_3 ),
        .I4(\string_cent_decenas[1]5__100_carry_n_6 ),
        .I5(\string_cent_decenas[1]5__79_carry__0_n_6 ),
        .O(\string_cent_decenas[1]5__124_carry__4_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT3 #(
    .INIT(8'h2B)) 
    \string_cent_decenas[1]5__124_carry__4_i_9 
       (.I0(\string_cent_decenas[1]5__124_carry__3_i_11_n_3 ),
        .I1(\string_cent_decenas[1]5__100_carry__0_n_7 ),
        .I2(\string_cent_decenas[1]5__79_carry__1_n_7 ),
        .O(\string_cent_decenas[1]5__124_carry__4_i_9_n_0 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5__124_carry__5 
       (.CI(\string_cent_decenas[1]5__124_carry__4_n_0 ),
        .CO(\NLW_string_cent_decenas[1]5__124_carry__5_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,\string_cent_decenas[1]5__124_carry__5_i_1_n_0 ,\string_cent_decenas[1]5__124_carry__5_i_2_n_0 }),
        .O({\NLW_string_cent_decenas[1]5__124_carry__5_O_UNCONNECTED [3],\string_cent_decenas[1]5__124_carry__5_n_5 ,\string_cent_decenas[1]5__124_carry__5_n_6 ,\string_cent_decenas[1]5__124_carry__5_n_7 }),
        .S({1'b0,\string_cent_decenas[1]5__124_carry__5_i_3_n_0 ,\string_cent_decenas[1]5__124_carry__5_i_4_n_0 ,\string_cent_decenas[1]5__124_carry__5_i_5_n_0 }));
  LUT6 #(
    .INIT(64'h80FE54D554D580FE)) 
    \string_cent_decenas[1]5__124_carry__5_i_1 
       (.I0(\string_cent_decenas[1]5__124_carry__3_i_11_n_3 ),
        .I1(\string_cent_decenas[1]5__100_carry__0_n_6 ),
        .I2(\string_cent_decenas[1]5__79_carry__1_n_6 ),
        .I3(\string_cent_decenas[1]5__27_carry__1_n_1 ),
        .I4(\string_cent_decenas[1]5__100_carry__0_n_5 ),
        .I5(\string_cent_decenas[1]5__79_carry__1_n_1 ),
        .O(\string_cent_decenas[1]5__124_carry__5_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h54D580FE80FE54D5)) 
    \string_cent_decenas[1]5__124_carry__5_i_2 
       (.I0(\string_cent_decenas[1]5__124_carry__3_i_11_n_3 ),
        .I1(\string_cent_decenas[1]5__100_carry__0_n_7 ),
        .I2(\string_cent_decenas[1]5__79_carry__1_n_7 ),
        .I3(\string_cent_decenas[1]5__27_carry__1_n_1 ),
        .I4(\string_cent_decenas[1]5__100_carry__0_n_6 ),
        .I5(\string_cent_decenas[1]5__79_carry__1_n_6 ),
        .O(\string_cent_decenas[1]5__124_carry__5_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFEF80107E0801F7F)) 
    \string_cent_decenas[1]5__124_carry__5_i_3 
       (.I0(\string_cent_decenas[1]5__100_carry__0_n_5 ),
        .I1(\string_cent_decenas[1]5__124_carry__3_i_11_n_3 ),
        .I2(\string_cent_decenas[1]5__100_carry__0_n_4 ),
        .I3(\string_cent_decenas[1]5__79_carry__1_n_1 ),
        .I4(\string_cent_decenas[1]5__100_carry__1_n_7 ),
        .I5(\string_cent_decenas[1]5__27_carry__1_n_1 ),
        .O(\string_cent_decenas[1]5__124_carry__5_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h6969699669969696)) 
    \string_cent_decenas[1]5__124_carry__5_i_4 
       (.I0(\string_cent_decenas[1]5__124_carry__5_i_1_n_0 ),
        .I1(\string_cent_decenas[1]5__27_carry__1_n_1 ),
        .I2(\string_cent_decenas[1]5__100_carry__0_n_4 ),
        .I3(\string_cent_decenas[1]5__124_carry__3_i_11_n_3 ),
        .I4(\string_cent_decenas[1]5__79_carry__1_n_1 ),
        .I5(\string_cent_decenas[1]5__100_carry__0_n_5 ),
        .O(\string_cent_decenas[1]5__124_carry__5_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h6996966996696996)) 
    \string_cent_decenas[1]5__124_carry__5_i_5 
       (.I0(\string_cent_decenas[1]5__124_carry__5_i_2_n_0 ),
        .I1(\string_cent_decenas[1]5__124_carry__5_i_6_n_0 ),
        .I2(\string_cent_decenas[1]5__27_carry__1_n_1 ),
        .I3(\string_cent_decenas[1]5__79_carry__1_n_1 ),
        .I4(\string_cent_decenas[1]5__124_carry__3_i_11_n_3 ),
        .I5(\string_cent_decenas[1]5__100_carry__0_n_5 ),
        .O(\string_cent_decenas[1]5__124_carry__5_i_5_n_0 ));
  LUT3 #(
    .INIT(8'h2B)) 
    \string_cent_decenas[1]5__124_carry__5_i_6 
       (.I0(\string_cent_decenas[1]5__124_carry__3_i_11_n_3 ),
        .I1(\string_cent_decenas[1]5__100_carry__0_n_6 ),
        .I2(\string_cent_decenas[1]5__79_carry__1_n_6 ),
        .O(\string_cent_decenas[1]5__124_carry__5_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \string_cent_decenas[1]5__124_carry_i_1 
       (.I0(\string_cent_decenas[1]5__27_carry_n_7 ),
        .I1(\string_cent_decenas[1]5_carry__1_n_7 ),
        .O(\string_cent_decenas[1]5__124_carry_i_1_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \string_cent_decenas[1]5__124_carry_i_2 
       (.I0(\string_cent_decenas[1]5_carry__0_n_4 ),
        .I1(i___0_carry_i_2_n_0),
        .O(\string_cent_decenas[1]5__124_carry_i_2_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \string_cent_decenas[1]5__124_carry_i_3 
       (.I0(\string_cent_decenas[1]5_carry__0_n_5 ),
        .I1(i___169_carry_i_5_n_0),
        .O(\string_cent_decenas[1]5__124_carry_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \string_cent_decenas[1]5__124_carry_i_4 
       (.I0(\string_cent_decenas[1]5_carry__0_n_6 ),
        .I1(\total_reg[0] ),
        .O(\string_cent_decenas[1]5__124_carry_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h8778)) 
    \string_cent_decenas[1]5__124_carry_i_5 
       (.I0(\string_cent_decenas[1]5_carry__1_n_7 ),
        .I1(\string_cent_decenas[1]5__27_carry_n_7 ),
        .I2(\string_cent_decenas[1]5_carry__1_n_6 ),
        .I3(\string_cent_decenas[1]5__27_carry_n_6 ),
        .O(\string_cent_decenas[1]5__124_carry_i_5_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT4 #(
    .INIT(16'h8778)) 
    \string_cent_decenas[1]5__124_carry_i_6 
       (.I0(i___0_carry_i_2_n_0),
        .I1(\string_cent_decenas[1]5_carry__0_n_4 ),
        .I2(\string_cent_decenas[1]5_carry__1_n_7 ),
        .I3(\string_cent_decenas[1]5__27_carry_n_7 ),
        .O(\string_cent_decenas[1]5__124_carry_i_6_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT4 #(
    .INIT(16'h4BB4)) 
    \string_cent_decenas[1]5__124_carry_i_7 
       (.I0(i___169_carry_i_5_n_0),
        .I1(\string_cent_decenas[1]5_carry__0_n_5 ),
        .I2(i___0_carry_i_2_n_0),
        .I3(\string_cent_decenas[1]5_carry__0_n_4 ),
        .O(\string_cent_decenas[1]5__124_carry_i_7_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT4 #(
    .INIT(16'h7887)) 
    \string_cent_decenas[1]5__124_carry_i_8 
       (.I0(\total_reg[0] ),
        .I1(\string_cent_decenas[1]5_carry__0_n_6 ),
        .I2(i___169_carry_i_5_n_0),
        .I3(\string_cent_decenas[1]5_carry__0_n_5 ),
        .O(\string_cent_decenas[1]5__124_carry_i_8_n_0 ));
  (* OPT_MODIFIED = "PROPCONST SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5__180_carry 
       (.CI(1'b0),
        .CO(\NLW_string_cent_decenas[1]5__180_carry_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,\string_cent_decenas[1]5__124_carry__5_n_6 ,1'b0}),
        .O({\NLW_string_cent_decenas[1]5__180_carry_O_UNCONNECTED [3],\string_cent_decenas[1]5__180_carry_n_5 ,\string_cent_decenas[1]5__180_carry_n_6 ,\string_cent_decenas[1]5__180_carry_n_7 }),
        .S({1'b0,\string_cent_decenas[1]5__180_carry_i_1_n_0 ,\string_cent_decenas[1]5__180_carry_i_2_n_0 ,\string_cent_decenas[1]5__124_carry__5_n_7 }));
  LUT2 #(
    .INIT(4'h6)) 
    \string_cent_decenas[1]5__180_carry_i_1 
       (.I0(\string_cent_decenas[1]5__124_carry__5_n_7 ),
        .I1(\string_cent_decenas[1]5__124_carry__5_n_5 ),
        .O(\string_cent_decenas[1]5__180_carry_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \string_cent_decenas[1]5__180_carry_i_2 
       (.I0(\string_cent_decenas[1]5__124_carry__5_n_6 ),
        .I1(\string_cent_decenas[1]5__124_carry__4_n_4 ),
        .O(\string_cent_decenas[1]5__180_carry_i_2_n_0 ));
  (* OPT_MODIFIED = "RETARGET SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5__186_carry 
       (.CI(1'b0),
        .CO({\string_cent_decenas[1]5__186_carry_n_0 ,\NLW_string_cent_decenas[1]5__186_carry_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b1),
        .DI({cent[3],i___0_carry_i_2_n_0,\string_cent_decenas[1]5__186_carry_i_3_n_0 ,\total_reg[0] }),
        .O({\string_cent_decenas[1]5__186_carry_n_4 ,\string_cent_decenas[1]5__186_carry_n_5 ,\string_cent_decenas[1]5__186_carry_n_6 ,\string_cent_decenas[1]5 [0]}),
        .S({\string_cent_decenas[1]5__186_carry_i_4_n_0 ,\string_cent_decenas[1]5__186_carry_i_5_n_0 ,\string_cent_decenas[1]5__186_carry_i_6_n_0 ,\string_cent_decenas[1]5__186_carry_i_7_n_0 }));
  CARRY4 \string_cent_decenas[1]5__186_carry__0 
       (.CI(\string_cent_decenas[1]5__186_carry_n_0 ),
        .CO(\NLW_string_cent_decenas[1]5__186_carry__0_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_string_cent_decenas[1]5__186_carry__0_O_UNCONNECTED [3:1],\string_cent_decenas[1]5__186_carry__0_n_7 }),
        .S({1'b0,1'b0,1'b0,\string_cent_decenas[1]5__186_carry__0_i_1_n_0 }));
  LUT2 #(
    .INIT(4'h9)) 
    \string_cent_decenas[1]5__186_carry__0_i_1 
       (.I0(\string_cent_decenas[1]5__180_carry_n_5 ),
        .I1(cent[4]),
        .O(\string_cent_decenas[1]5__186_carry__0_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAAAACCC3)) 
    \string_cent_decenas[1]5__186_carry_i_1 
       (.I0(Q[3]),
        .I1(euros1__339_carry_n_4),
        .I2(\string_cent_decenas[1]5__186_carry_i_8_n_0 ),
        .I3(euros1__339_carry_n_5),
        .I4(\total_reg[15] ),
        .O(cent[3]));
  LUT3 #(
    .INIT(8'hB8)) 
    \string_cent_decenas[1]5__186_carry_i_3 
       (.I0(Q[1]),
        .I1(\total_reg[15] ),
        .I2(p_1_in[1]),
        .O(\string_cent_decenas[1]5__186_carry_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \string_cent_decenas[1]5__186_carry_i_4 
       (.I0(cent[3]),
        .I1(\string_cent_decenas[1]5__180_carry_n_6 ),
        .O(\string_cent_decenas[1]5__186_carry_i_4_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \string_cent_decenas[1]5__186_carry_i_5 
       (.I0(i___0_carry_i_2_n_0),
        .I1(\string_cent_decenas[1]5__180_carry_n_7 ),
        .O(\string_cent_decenas[1]5__186_carry_i_5_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \string_cent_decenas[1]5__186_carry_i_6 
       (.I0(i___169_carry_i_5_n_0),
        .I1(\string_cent_decenas[1]5__124_carry__4_n_4 ),
        .O(\string_cent_decenas[1]5__186_carry_i_6_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \string_cent_decenas[1]5__186_carry_i_7 
       (.I0(\total_reg[0] ),
        .O(\string_cent_decenas[1]5__186_carry_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h0001555555555555)) 
    \string_cent_decenas[1]5__186_carry_i_8 
       (.I0(euros1__339_carry__0_n_4),
        .I1(euros1__339_carry_n_5),
        .I2(euros1__339_carry_n_4),
        .I3(euros1__339_carry__0_n_7),
        .I4(euros1__339_carry__0_n_6),
        .I5(euros1__339_carry__0_n_5),
        .O(\string_cent_decenas[1]5__186_carry_i_8_n_0 ));
  (* OPT_MODIFIED = "PROPCONST SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5__27_carry 
       (.CI(1'b0),
        .CO({\string_cent_decenas[1]5__27_carry_n_0 ,\NLW_string_cent_decenas[1]5__27_carry_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\string_cent_decenas[1]5__27_carry_i_1_n_0 ,\string_cent_decenas[1]5__27_carry_i_2_n_0 ,\string_cent_decenas[1]5__27_carry_i_3_n_0 ,1'b0}),
        .O({\string_cent_decenas[1]5__27_carry_n_4 ,\string_cent_decenas[1]5__27_carry_n_5 ,\string_cent_decenas[1]5__27_carry_n_6 ,\string_cent_decenas[1]5__27_carry_n_7 }),
        .S({\string_cent_decenas[1]5__27_carry_i_4_n_0 ,\string_cent_decenas[1]5__27_carry_i_5_n_0 ,\string_cent_decenas[1]5__27_carry_i_6_n_0 ,\string_cent_decenas[1]5__27_carry_i_7_n_0 }));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5__27_carry__0 
       (.CI(\string_cent_decenas[1]5__27_carry_n_0 ),
        .CO({\string_cent_decenas[1]5__27_carry__0_n_0 ,\NLW_string_cent_decenas[1]5__27_carry__0_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\string_cent_decenas[1]5__27_carry__0_i_1_n_0 ,\string_cent_decenas[1]5__27_carry__0_i_2_n_0 ,\string_cent_decenas[1]5__27_carry__0_i_3_n_0 ,\string_cent_decenas[1]5__27_carry__0_i_4_n_0 }),
        .O({\string_cent_decenas[1]5__27_carry__0_n_4 ,\string_cent_decenas[1]5__27_carry__0_n_5 ,\string_cent_decenas[1]5__27_carry__0_n_6 ,\string_cent_decenas[1]5__27_carry__0_n_7 }),
        .S({\string_cent_decenas[1]5__27_carry__0_i_5_n_0 ,\string_cent_decenas[1]5__27_carry__0_i_6_n_0 ,\string_cent_decenas[1]5__27_carry__0_i_7_n_0 ,\string_cent_decenas[1]5__27_carry__0_i_8_n_0 }));
  LUT2 #(
    .INIT(4'h2)) 
    \string_cent_decenas[1]5__27_carry__0_i_1 
       (.I0(cent[4]),
        .I1(cent[6]),
        .O(\string_cent_decenas[1]5__27_carry__0_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \string_cent_decenas[1]5__27_carry__0_i_2 
       (.I0(cent[3]),
        .I1(cent[5]),
        .O(\string_cent_decenas[1]5__27_carry__0_i_2_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT2 #(
    .INIT(4'h4)) 
    \string_cent_decenas[1]5__27_carry__0_i_3 
       (.I0(cent[4]),
        .I1(i___0_carry_i_2_n_0),
        .O(\string_cent_decenas[1]5__27_carry__0_i_3_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \string_cent_decenas[1]5__27_carry__0_i_4 
       (.I0(cent[4]),
        .I1(i___0_carry_i_2_n_0),
        .O(\string_cent_decenas[1]5__27_carry__0_i_4_n_0 ));
  LUT3 #(
    .INIT(8'h4B)) 
    \string_cent_decenas[1]5__27_carry__0_i_5 
       (.I0(cent[6]),
        .I1(cent[4]),
        .I2(cent[5]),
        .O(\string_cent_decenas[1]5__27_carry__0_i_5_n_0 ));
  LUT4 #(
    .INIT(16'hB44B)) 
    \string_cent_decenas[1]5__27_carry__0_i_6 
       (.I0(cent[5]),
        .I1(cent[3]),
        .I2(cent[6]),
        .I3(cent[4]),
        .O(\string_cent_decenas[1]5__27_carry__0_i_6_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT4 #(
    .INIT(16'hD22D)) 
    \string_cent_decenas[1]5__27_carry__0_i_7 
       (.I0(i___0_carry_i_2_n_0),
        .I1(cent[4]),
        .I2(cent[5]),
        .I3(cent[3]),
        .O(\string_cent_decenas[1]5__27_carry__0_i_7_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT5 #(
    .INIT(32'hD42B2BD4)) 
    \string_cent_decenas[1]5__27_carry__0_i_8 
       (.I0(cent[6]),
        .I1(i___169_carry_i_5_n_0),
        .I2(cent[3]),
        .I3(i___0_carry_i_2_n_0),
        .I4(cent[4]),
        .O(\string_cent_decenas[1]5__27_carry__0_i_8_n_0 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5__27_carry__1 
       (.CI(\string_cent_decenas[1]5__27_carry__0_n_0 ),
        .CO({\NLW_string_cent_decenas[1]5__27_carry__1_CO_UNCONNECTED [3],\string_cent_decenas[1]5__27_carry__1_n_1 ,\NLW_string_cent_decenas[1]5__27_carry__1_CO_UNCONNECTED [1:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b1,cent[5]}),
        .O({\NLW_string_cent_decenas[1]5__27_carry__1_O_UNCONNECTED [3:2],\string_cent_decenas[1]5__27_carry__1_n_6 ,\string_cent_decenas[1]5__27_carry__1_n_7 }),
        .S({1'b0,1'b1,\string_cent_decenas[1]5__27_carry__1_i_2_n_0 ,\string_cent_decenas[1]5__27_carry__1_i_3_n_0 }));
  LUT6 #(
    .INIT(64'hBB88BB8B88BB8888)) 
    \string_cent_decenas[1]5__27_carry__1_i_1 
       (.I0(Q[5]),
        .I1(\total_reg[15] ),
        .I2(euros1__339_carry__0_n_5),
        .I3(\string_cent_decenas[1]5__27_carry__1_i_4_n_0 ),
        .I4(euros1__339_carry__0_n_4),
        .I5(euros1__339_carry__0_n_6),
        .O(cent[5]));
  LUT1 #(
    .INIT(2'h1)) 
    \string_cent_decenas[1]5__27_carry__1_i_2 
       (.I0(cent[6]),
        .O(\string_cent_decenas[1]5__27_carry__1_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \string_cent_decenas[1]5__27_carry__1_i_3 
       (.I0(cent[5]),
        .I1(cent[6]),
        .O(\string_cent_decenas[1]5__27_carry__1_i_3_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \string_cent_decenas[1]5__27_carry__1_i_4 
       (.I0(euros1__339_carry_n_5),
        .I1(euros1__339_carry_n_4),
        .I2(euros1__339_carry__0_n_7),
        .O(\string_cent_decenas[1]5__27_carry__1_i_4_n_0 ));
  (* HLUTNM = "lutpair59" *) 
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT3 #(
    .INIT(8'hB2)) 
    \string_cent_decenas[1]5__27_carry_i_1 
       (.I0(\total_reg[0] ),
        .I1(i___0_carry_i_2_n_0),
        .I2(cent[5]),
        .O(\string_cent_decenas[1]5__27_carry_i_1_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT3 #(
    .INIT(8'h69)) 
    \string_cent_decenas[1]5__27_carry_i_2 
       (.I0(\total_reg[0] ),
        .I1(i___0_carry_i_2_n_0),
        .I2(cent[5]),
        .O(\string_cent_decenas[1]5__27_carry_i_2_n_0 ));
  LUT2 #(
    .INIT(4'hB)) 
    \string_cent_decenas[1]5__27_carry_i_3 
       (.I0(cent[3]),
        .I1(\total_reg[0] ),
        .O(\string_cent_decenas[1]5__27_carry_i_3_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    \string_cent_decenas[1]5__27_carry_i_4 
       (.I0(\string_cent_decenas[1]5__27_carry_i_1_n_0 ),
        .I1(cent[3]),
        .I2(i___169_carry_i_5_n_0),
        .I3(cent[6]),
        .O(\string_cent_decenas[1]5__27_carry_i_4_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT5 #(
    .INIT(32'h96696969)) 
    \string_cent_decenas[1]5__27_carry_i_5 
       (.I0(\total_reg[0] ),
        .I1(i___0_carry_i_2_n_0),
        .I2(cent[5]),
        .I3(i___169_carry_i_5_n_0),
        .I4(cent[4]),
        .O(\string_cent_decenas[1]5__27_carry_i_5_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT4 #(
    .INIT(16'hD22D)) 
    \string_cent_decenas[1]5__27_carry_i_6 
       (.I0(\total_reg[0] ),
        .I1(cent[3]),
        .I2(i___169_carry_i_5_n_0),
        .I3(cent[4]),
        .O(\string_cent_decenas[1]5__27_carry_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \string_cent_decenas[1]5__27_carry_i_7 
       (.I0(cent[3]),
        .I1(\total_reg[0] ),
        .O(\string_cent_decenas[1]5__27_carry_i_7_n_0 ));
  (* OPT_MODIFIED = "RETARGET SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5__55_carry 
       (.CI(1'b0),
        .CO({\string_cent_decenas[1]5__55_carry_n_0 ,\NLW_string_cent_decenas[1]5__55_carry_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\string_cent_decenas[1]5__55_carry_i_1_n_0 ,\total_reg[0] ,1'b0,1'b1}),
        .O({\string_cent_decenas[1]5__55_carry_n_4 ,\string_cent_decenas[1]5__55_carry_n_5 ,\string_cent_decenas[1]5__55_carry_n_6 ,\NLW_string_cent_decenas[1]5__55_carry_O_UNCONNECTED [0]}),
        .S({\string_cent_decenas[1]5__55_carry_i_2_n_0 ,\string_cent_decenas[1]5__55_carry_i_3_n_0 ,i___169_carry_i_5_n_0,\string_cent_decenas[1]5__55_carry_i_5_n_0 }));
  (* OPT_MODIFIED = "RETARGET SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5__55_carry__0 
       (.CI(\string_cent_decenas[1]5__55_carry_n_0 ),
        .CO({\string_cent_decenas[1]5__55_carry__0_n_0 ,\NLW_string_cent_decenas[1]5__55_carry__0_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\string_cent_decenas[1]5__55_carry__0_i_1_n_0 ,\string_cent_decenas[1]5_carry__0_i_2_n_0 ,\string_cent_decenas[1]5__55_carry__0_i_2_n_0 ,i___0_carry_i_2_n_0}),
        .O({\string_cent_decenas[1]5__55_carry__0_n_4 ,\string_cent_decenas[1]5__55_carry__0_n_5 ,\string_cent_decenas[1]5__55_carry__0_n_6 ,\string_cent_decenas[1]5__55_carry__0_n_7 }),
        .S({\string_cent_decenas[1]5__55_carry__0_i_4_n_0 ,\string_cent_decenas[1]5__55_carry__0_i_5_n_0 ,\string_cent_decenas[1]5__55_carry__0_i_6_n_0 ,\string_cent_decenas[1]5__55_carry__0_i_7_n_0 }));
  LUT2 #(
    .INIT(4'h9)) 
    \string_cent_decenas[1]5__55_carry__0_i_1 
       (.I0(cent[3]),
        .I1(cent[5]),
        .O(\string_cent_decenas[1]5__55_carry__0_i_1_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \string_cent_decenas[1]5__55_carry__0_i_2 
       (.I0(i___169_carry_i_5_n_0),
        .I1(cent[5]),
        .I2(cent[3]),
        .O(\string_cent_decenas[1]5__55_carry__0_i_2_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT5 #(
    .INIT(32'h4DB2B24D)) 
    \string_cent_decenas[1]5__55_carry__0_i_4 
       (.I0(i___0_carry_i_2_n_0),
        .I1(cent[6]),
        .I2(cent[4]),
        .I3(cent[5]),
        .I4(cent[3]),
        .O(\string_cent_decenas[1]5__55_carry__0_i_4_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT6 #(
    .INIT(64'h4DB2B24DB24D4DB2)) 
    \string_cent_decenas[1]5__55_carry__0_i_5 
       (.I0(cent[5]),
        .I1(cent[3]),
        .I2(i___169_carry_i_5_n_0),
        .I3(cent[4]),
        .I4(i___0_carry_i_2_n_0),
        .I5(cent[6]),
        .O(\string_cent_decenas[1]5__55_carry__0_i_5_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT5 #(
    .INIT(32'h96699696)) 
    \string_cent_decenas[1]5__55_carry__0_i_6 
       (.I0(cent[3]),
        .I1(cent[5]),
        .I2(i___169_carry_i_5_n_0),
        .I3(cent[4]),
        .I4(\total_reg[0] ),
        .O(\string_cent_decenas[1]5__55_carry__0_i_6_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT3 #(
    .INIT(8'h69)) 
    \string_cent_decenas[1]5__55_carry__0_i_7 
       (.I0(\total_reg[0] ),
        .I1(cent[4]),
        .I2(i___0_carry_i_2_n_0),
        .O(\string_cent_decenas[1]5__55_carry__0_i_7_n_0 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5__55_carry__1 
       (.CI(\string_cent_decenas[1]5__55_carry__0_n_0 ),
        .CO({\string_cent_decenas[1]5__55_carry__1_n_0 ,\NLW_string_cent_decenas[1]5__55_carry__1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b1,\string_cent_decenas[1]5__55_carry__1_i_1_n_0 ,\string_cent_decenas[1]5__55_carry__1_i_2_n_0 ,\string_cent_decenas[1]5__55_carry__1_i_3_n_0 }),
        .O({\string_cent_decenas[1]5__55_carry__1_n_4 ,\string_cent_decenas[1]5__55_carry__1_n_5 ,\string_cent_decenas[1]5__55_carry__1_n_6 ,\string_cent_decenas[1]5__55_carry__1_n_7 }),
        .S({\string_cent_decenas[1]5__55_carry__1_i_4_n_0 ,\string_cent_decenas[1]5__55_carry__1_i_5_n_0 ,\string_cent_decenas[1]5__55_carry__1_i_6_n_0 ,\string_cent_decenas[1]5__55_carry__1_i_7_n_0 }));
  LUT1 #(
    .INIT(2'h1)) 
    \string_cent_decenas[1]5__55_carry__1_i_1 
       (.I0(cent[6]),
        .O(\string_cent_decenas[1]5__55_carry__1_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \string_cent_decenas[1]5__55_carry__1_i_2 
       (.I0(cent[5]),
        .O(\string_cent_decenas[1]5__55_carry__1_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \string_cent_decenas[1]5__55_carry__1_i_3 
       (.I0(cent[6]),
        .I1(cent[4]),
        .O(\string_cent_decenas[1]5__55_carry__1_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \string_cent_decenas[1]5__55_carry__1_i_4 
       (.I0(cent[6]),
        .O(\string_cent_decenas[1]5__55_carry__1_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \string_cent_decenas[1]5__55_carry__1_i_5 
       (.I0(cent[5]),
        .I1(cent[6]),
        .O(\string_cent_decenas[1]5__55_carry__1_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hE1)) 
    \string_cent_decenas[1]5__55_carry__1_i_6 
       (.I0(cent[6]),
        .I1(cent[4]),
        .I2(cent[5]),
        .O(\string_cent_decenas[1]5__55_carry__1_i_6_n_0 ));
  LUT4 #(
    .INIT(16'h1EE1)) 
    \string_cent_decenas[1]5__55_carry__1_i_7 
       (.I0(cent[5]),
        .I1(cent[3]),
        .I2(cent[6]),
        .I3(cent[4]),
        .O(\string_cent_decenas[1]5__55_carry__1_i_7_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \string_cent_decenas[1]5__55_carry_i_1 
       (.I0(Q[1]),
        .I1(\total_reg[15] ),
        .I2(p_1_in[1]),
        .O(\string_cent_decenas[1]5__55_carry_i_1_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \string_cent_decenas[1]5__55_carry_i_2 
       (.I0(i___169_carry_i_5_n_0),
        .I1(cent[3]),
        .O(\string_cent_decenas[1]5__55_carry_i_2_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \string_cent_decenas[1]5__55_carry_i_3 
       (.I0(\total_reg[0] ),
        .I1(i___0_carry_i_2_n_0),
        .O(\string_cent_decenas[1]5__55_carry_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFEF00000020)) 
    \string_cent_decenas[1]5__55_carry_i_5 
       (.I0(Q[0]),
        .I1(\string_cent_decenas[1]5_carry_i_8_n_0 ),
        .I2(\string_cent_decenas[1]5_carry_i_9_n_0 ),
        .I3(\total_reg[20] ),
        .I4(\string_cent_decenas[1]5_carry_i_11_n_0 ),
        .I5(p_1_in[0]),
        .O(\string_cent_decenas[1]5__55_carry_i_5_n_0 ));
  (* OPT_MODIFIED = "PROPCONST SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5__79_carry 
       (.CI(1'b0),
        .CO({\string_cent_decenas[1]5__79_carry_n_0 ,\NLW_string_cent_decenas[1]5__79_carry_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\string_cent_decenas[1]5__27_carry_i_1_n_0 ,\string_cent_decenas[1]5__79_carry_i_1_n_0 ,\string_cent_decenas[1]5__79_carry_i_2_n_0 ,1'b0}),
        .O({\string_cent_decenas[1]5__79_carry_n_4 ,\string_cent_decenas[1]5__79_carry_n_5 ,\string_cent_decenas[1]5__79_carry_n_6 ,\NLW_string_cent_decenas[1]5__79_carry_O_UNCONNECTED [0]}),
        .S({\string_cent_decenas[1]5__79_carry_i_3_n_0 ,\string_cent_decenas[1]5__79_carry_i_4_n_0 ,\string_cent_decenas[1]5__79_carry_i_5_n_0 ,\string_cent_decenas[1]5__79_carry_i_6_n_0 }));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5__79_carry__0 
       (.CI(\string_cent_decenas[1]5__79_carry_n_0 ),
        .CO({\string_cent_decenas[1]5__79_carry__0_n_0 ,\NLW_string_cent_decenas[1]5__79_carry__0_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\string_cent_decenas[1]5__79_carry__0_i_1_n_0 ,\string_cent_decenas[1]5__79_carry__0_i_2_n_0 ,\string_cent_decenas[1]5__79_carry__0_i_3_n_0 ,\string_cent_decenas[1]5__79_carry__0_i_4_n_0 }),
        .O({\string_cent_decenas[1]5__79_carry__0_n_4 ,\string_cent_decenas[1]5__79_carry__0_n_5 ,\string_cent_decenas[1]5__79_carry__0_n_6 ,\string_cent_decenas[1]5__79_carry__0_n_7 }),
        .S({\string_cent_decenas[1]5__79_carry__0_i_5_n_0 ,\string_cent_decenas[1]5__79_carry__0_i_6_n_0 ,\string_cent_decenas[1]5__79_carry__0_i_7_n_0 ,\string_cent_decenas[1]5__79_carry__0_i_8_n_0 }));
  LUT2 #(
    .INIT(4'h2)) 
    \string_cent_decenas[1]5__79_carry__0_i_1 
       (.I0(cent[4]),
        .I1(cent[6]),
        .O(\string_cent_decenas[1]5__79_carry__0_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \string_cent_decenas[1]5__79_carry__0_i_2 
       (.I0(cent[3]),
        .I1(cent[5]),
        .O(\string_cent_decenas[1]5__79_carry__0_i_2_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT2 #(
    .INIT(4'h4)) 
    \string_cent_decenas[1]5__79_carry__0_i_3 
       (.I0(cent[4]),
        .I1(i___0_carry_i_2_n_0),
        .O(\string_cent_decenas[1]5__79_carry__0_i_3_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \string_cent_decenas[1]5__79_carry__0_i_4 
       (.I0(cent[4]),
        .I1(i___0_carry_i_2_n_0),
        .O(\string_cent_decenas[1]5__79_carry__0_i_4_n_0 ));
  LUT3 #(
    .INIT(8'h4B)) 
    \string_cent_decenas[1]5__79_carry__0_i_5 
       (.I0(cent[6]),
        .I1(cent[4]),
        .I2(cent[5]),
        .O(\string_cent_decenas[1]5__79_carry__0_i_5_n_0 ));
  LUT4 #(
    .INIT(16'hB44B)) 
    \string_cent_decenas[1]5__79_carry__0_i_6 
       (.I0(cent[5]),
        .I1(cent[3]),
        .I2(cent[6]),
        .I3(cent[4]),
        .O(\string_cent_decenas[1]5__79_carry__0_i_6_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT4 #(
    .INIT(16'hD22D)) 
    \string_cent_decenas[1]5__79_carry__0_i_7 
       (.I0(i___0_carry_i_2_n_0),
        .I1(cent[4]),
        .I2(cent[5]),
        .I3(cent[3]),
        .O(\string_cent_decenas[1]5__79_carry__0_i_7_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT5 #(
    .INIT(32'hD42B2BD4)) 
    \string_cent_decenas[1]5__79_carry__0_i_8 
       (.I0(cent[6]),
        .I1(i___169_carry_i_5_n_0),
        .I2(cent[3]),
        .I3(i___0_carry_i_2_n_0),
        .I4(cent[4]),
        .O(\string_cent_decenas[1]5__79_carry__0_i_8_n_0 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5__79_carry__1 
       (.CI(\string_cent_decenas[1]5__79_carry__0_n_0 ),
        .CO({\NLW_string_cent_decenas[1]5__79_carry__1_CO_UNCONNECTED [3],\string_cent_decenas[1]5__79_carry__1_n_1 ,\NLW_string_cent_decenas[1]5__79_carry__1_CO_UNCONNECTED [1:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b1,cent[5]}),
        .O({\NLW_string_cent_decenas[1]5__79_carry__1_O_UNCONNECTED [3:2],\string_cent_decenas[1]5__79_carry__1_n_6 ,\string_cent_decenas[1]5__79_carry__1_n_7 }),
        .S({1'b0,1'b1,\string_cent_decenas[1]5__79_carry__1_i_1_n_0 ,\string_cent_decenas[1]5__79_carry__1_i_2_n_0 }));
  LUT1 #(
    .INIT(2'h1)) 
    \string_cent_decenas[1]5__79_carry__1_i_1 
       (.I0(cent[6]),
        .O(\string_cent_decenas[1]5__79_carry__1_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \string_cent_decenas[1]5__79_carry__1_i_2 
       (.I0(cent[5]),
        .I1(cent[6]),
        .O(\string_cent_decenas[1]5__79_carry__1_i_2_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT3 #(
    .INIT(8'h69)) 
    \string_cent_decenas[1]5__79_carry_i_1 
       (.I0(\total_reg[0] ),
        .I1(i___0_carry_i_2_n_0),
        .I2(cent[5]),
        .O(\string_cent_decenas[1]5__79_carry_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hB)) 
    \string_cent_decenas[1]5__79_carry_i_2 
       (.I0(cent[3]),
        .I1(\total_reg[0] ),
        .O(\string_cent_decenas[1]5__79_carry_i_2_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    \string_cent_decenas[1]5__79_carry_i_3 
       (.I0(\string_cent_decenas[1]5__27_carry_i_1_n_0 ),
        .I1(cent[3]),
        .I2(i___169_carry_i_5_n_0),
        .I3(cent[6]),
        .O(\string_cent_decenas[1]5__79_carry_i_3_n_0 ));
  (* HLUTNM = "lutpair59" *) 
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT5 #(
    .INIT(32'h96696969)) 
    \string_cent_decenas[1]5__79_carry_i_4 
       (.I0(\total_reg[0] ),
        .I1(i___0_carry_i_2_n_0),
        .I2(cent[5]),
        .I3(i___169_carry_i_5_n_0),
        .I4(cent[4]),
        .O(\string_cent_decenas[1]5__79_carry_i_4_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT4 #(
    .INIT(16'hD22D)) 
    \string_cent_decenas[1]5__79_carry_i_5 
       (.I0(\total_reg[0] ),
        .I1(cent[3]),
        .I2(i___169_carry_i_5_n_0),
        .I3(cent[4]),
        .O(\string_cent_decenas[1]5__79_carry_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \string_cent_decenas[1]5__79_carry_i_6 
       (.I0(cent[3]),
        .I1(\total_reg[0] ),
        .O(\string_cent_decenas[1]5__79_carry_i_6_n_0 ));
  (* OPT_MODIFIED = "RETARGET SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5_carry 
       (.CI(1'b0),
        .CO({\string_cent_decenas[1]5_carry_n_0 ,\NLW_string_cent_decenas[1]5_carry_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\string_cent_decenas[1]5_carry_i_1_n_0 ,\total_reg[0] ,1'b0,1'b1}),
        .O({\NLW_string_cent_decenas[1]5_carry_O_UNCONNECTED [3:1],\string_cent_decenas[1]5_carry_n_7 }),
        .S({\string_cent_decenas[1]5_carry_i_3_n_0 ,\string_cent_decenas[1]5_carry_i_4_n_0 ,i___169_carry_i_5_n_0,\string_cent_decenas[1]5_carry_i_6_n_0 }));
  (* OPT_MODIFIED = "RETARGET SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5_carry__0 
       (.CI(\string_cent_decenas[1]5_carry_n_0 ),
        .CO({\string_cent_decenas[1]5_carry__0_n_0 ,\NLW_string_cent_decenas[1]5_carry__0_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\string_cent_decenas[1]5_carry__0_i_1_n_0 ,\string_cent_decenas[1]5_carry__0_i_2_n_0 ,\string_cent_decenas[1]5_carry__0_i_3_n_0 ,i___0_carry_i_2_n_0}),
        .O({\string_cent_decenas[1]5_carry__0_n_4 ,\string_cent_decenas[1]5_carry__0_n_5 ,\string_cent_decenas[1]5_carry__0_n_6 ,\NLW_string_cent_decenas[1]5_carry__0_O_UNCONNECTED [0]}),
        .S({\string_cent_decenas[1]5_carry__0_i_5_n_0 ,\string_cent_decenas[1]5_carry__0_i_6_n_0 ,\string_cent_decenas[1]5_carry__0_i_7_n_0 ,\string_cent_decenas[1]5_carry__0_i_8_n_0 }));
  LUT2 #(
    .INIT(4'h9)) 
    \string_cent_decenas[1]5_carry__0_i_1 
       (.I0(cent[3]),
        .I1(cent[5]),
        .O(\string_cent_decenas[1]5_carry__0_i_1_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT3 #(
    .INIT(8'h69)) 
    \string_cent_decenas[1]5_carry__0_i_2 
       (.I0(cent[6]),
        .I1(i___0_carry_i_2_n_0),
        .I2(cent[4]),
        .O(\string_cent_decenas[1]5_carry__0_i_2_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT3 #(
    .INIT(8'h96)) 
    \string_cent_decenas[1]5_carry__0_i_3 
       (.I0(i___169_carry_i_5_n_0),
        .I1(cent[5]),
        .I2(cent[3]),
        .O(\string_cent_decenas[1]5_carry__0_i_3_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT5 #(
    .INIT(32'h4DB2B24D)) 
    \string_cent_decenas[1]5_carry__0_i_5 
       (.I0(i___0_carry_i_2_n_0),
        .I1(cent[6]),
        .I2(cent[4]),
        .I3(cent[5]),
        .I4(cent[3]),
        .O(\string_cent_decenas[1]5_carry__0_i_5_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT6 #(
    .INIT(64'h4DB2B24DB24D4DB2)) 
    \string_cent_decenas[1]5_carry__0_i_6 
       (.I0(cent[5]),
        .I1(cent[3]),
        .I2(i___169_carry_i_5_n_0),
        .I3(cent[4]),
        .I4(i___0_carry_i_2_n_0),
        .I5(cent[6]),
        .O(\string_cent_decenas[1]5_carry__0_i_6_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT5 #(
    .INIT(32'h96699696)) 
    \string_cent_decenas[1]5_carry__0_i_7 
       (.I0(cent[3]),
        .I1(cent[5]),
        .I2(i___169_carry_i_5_n_0),
        .I3(cent[4]),
        .I4(\total_reg[0] ),
        .O(\string_cent_decenas[1]5_carry__0_i_7_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT3 #(
    .INIT(8'h69)) 
    \string_cent_decenas[1]5_carry__0_i_8 
       (.I0(\total_reg[0] ),
        .I1(cent[4]),
        .I2(i___0_carry_i_2_n_0),
        .O(\string_cent_decenas[1]5_carry__0_i_8_n_0 ));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5_carry__1 
       (.CI(\string_cent_decenas[1]5_carry__0_n_0 ),
        .CO({\string_cent_decenas[1]5_carry__1_n_0 ,\NLW_string_cent_decenas[1]5_carry__1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b1,\string_cent_decenas[1]5_carry__1_i_1_n_0 ,\string_cent_decenas[1]5_carry__1_i_2_n_0 ,\string_cent_decenas[1]5_carry__1_i_3_n_0 }),
        .O({\string_cent_decenas[1]5_carry__1_n_4 ,\string_cent_decenas[1]5_carry__1_n_5 ,\string_cent_decenas[1]5_carry__1_n_6 ,\string_cent_decenas[1]5_carry__1_n_7 }),
        .S({\string_cent_decenas[1]5_carry__1_i_4_n_0 ,\string_cent_decenas[1]5_carry__1_i_5_n_0 ,\string_cent_decenas[1]5_carry__1_i_6_n_0 ,\string_cent_decenas[1]5_carry__1_i_7_n_0 }));
  LUT1 #(
    .INIT(2'h1)) 
    \string_cent_decenas[1]5_carry__1_i_1 
       (.I0(cent[6]),
        .O(\string_cent_decenas[1]5_carry__1_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \string_cent_decenas[1]5_carry__1_i_2 
       (.I0(cent[5]),
        .O(\string_cent_decenas[1]5_carry__1_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \string_cent_decenas[1]5_carry__1_i_3 
       (.I0(cent[6]),
        .I1(cent[4]),
        .O(\string_cent_decenas[1]5_carry__1_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \string_cent_decenas[1]5_carry__1_i_4 
       (.I0(cent[6]),
        .O(\string_cent_decenas[1]5_carry__1_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \string_cent_decenas[1]5_carry__1_i_5 
       (.I0(cent[5]),
        .I1(cent[6]),
        .O(\string_cent_decenas[1]5_carry__1_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hE1)) 
    \string_cent_decenas[1]5_carry__1_i_6 
       (.I0(cent[6]),
        .I1(cent[4]),
        .I2(cent[5]),
        .O(\string_cent_decenas[1]5_carry__1_i_6_n_0 ));
  LUT4 #(
    .INIT(16'h1EE1)) 
    \string_cent_decenas[1]5_carry__1_i_7 
       (.I0(cent[5]),
        .I1(cent[3]),
        .I2(cent[6]),
        .I3(cent[4]),
        .O(\string_cent_decenas[1]5_carry__1_i_7_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \string_cent_decenas[1]5_carry_i_1 
       (.I0(Q[1]),
        .I1(\total_reg[15] ),
        .I2(p_1_in[1]),
        .O(\string_cent_decenas[1]5_carry_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFEFFF)) 
    \string_cent_decenas[1]5_carry_i_10 
       (.I0(Q[20]),
        .I1(Q[22]),
        .I2(\string_cent_decenas[1]5_carry_i_18_n_0 ),
        .I3(\string_cent_decenas[1]5_carry_i_19_n_0 ),
        .I4(Q[12]),
        .I5(Q[14]),
        .O(\total_reg[20] ));
  LUT5 #(
    .INIT(32'h88888880)) 
    \string_cent_decenas[1]5_carry_i_11 
       (.I0(Q[6]),
        .I1(Q[5]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(Q[4]),
        .O(\string_cent_decenas[1]5_carry_i_11_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT3 #(
    .INIT(8'h47)) 
    \string_cent_decenas[1]5_carry_i_12 
       (.I0(Q[1]),
        .I1(\total_reg[15] ),
        .I2(p_1_in[1]),
        .O(i___169_carry_i_5_n_0));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT4 #(
    .INIT(16'hAAC3)) 
    \string_cent_decenas[1]5_carry_i_13 
       (.I0(Q[2]),
        .I1(euros1__339_carry_n_5),
        .I2(\string_cent_decenas[1]5__186_carry_i_8_n_0 ),
        .I3(\total_reg[15] ),
        .O(i___0_carry_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \string_cent_decenas[1]5_carry_i_14 
       (.I0(Q[23]),
        .I1(Q[30]),
        .O(\string_cent_decenas[1]5_carry_i_14_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \string_cent_decenas[1]5_carry_i_15 
       (.I0(Q[26]),
        .I1(Q[27]),
        .O(\total_reg[26] ));
  LUT2 #(
    .INIT(4'h1)) 
    \string_cent_decenas[1]5_carry_i_16 
       (.I0(Q[11]),
        .I1(Q[13]),
        .O(\string_cent_decenas[1]5_carry_i_16_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \string_cent_decenas[1]5_carry_i_17 
       (.I0(Q[7]),
        .I1(Q[9]),
        .O(\string_cent_decenas[1]5_carry_i_17_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \string_cent_decenas[1]5_carry_i_18 
       (.I0(Q[8]),
        .I1(Q[10]),
        .O(\string_cent_decenas[1]5_carry_i_18_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \string_cent_decenas[1]5_carry_i_19 
       (.I0(Q[16]),
        .I1(Q[18]),
        .O(\string_cent_decenas[1]5_carry_i_19_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFEF00000020)) 
    \string_cent_decenas[1]5_carry_i_2 
       (.I0(Q[0]),
        .I1(\string_cent_decenas[1]5_carry_i_8_n_0 ),
        .I2(\string_cent_decenas[1]5_carry_i_9_n_0 ),
        .I3(\total_reg[20] ),
        .I4(\string_cent_decenas[1]5_carry_i_11_n_0 ),
        .I5(p_1_in[0]),
        .O(\total_reg[0] ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \string_cent_decenas[1]5_carry_i_3 
       (.I0(i___169_carry_i_5_n_0),
        .I1(cent[3]),
        .O(\string_cent_decenas[1]5_carry_i_3_n_0 ));
  (* OPT_MODIFIED = "RETARGET" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \string_cent_decenas[1]5_carry_i_4 
       (.I0(\total_reg[0] ),
        .I1(i___0_carry_i_2_n_0),
        .O(\string_cent_decenas[1]5_carry_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFEF00000020)) 
    \string_cent_decenas[1]5_carry_i_6 
       (.I0(Q[0]),
        .I1(\string_cent_decenas[1]5_carry_i_8_n_0 ),
        .I2(\string_cent_decenas[1]5_carry_i_9_n_0 ),
        .I3(\total_reg[20] ),
        .I4(\string_cent_decenas[1]5_carry_i_11_n_0 ),
        .I5(p_1_in[0]),
        .O(\string_cent_decenas[1]5_carry_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000001000)) 
    \string_cent_decenas[1]5_carry_i_7 
       (.I0(\string_cent_decenas[1]5_carry_i_8_n_0 ),
        .I1(\total_reg[29] ),
        .I2(\string_cent_decenas[1]5_carry_i_14_n_0 ),
        .I3(\total_reg[26] ),
        .I4(\total_reg[20] ),
        .I5(\string_cent_decenas[1]5_carry_i_11_n_0 ),
        .O(\total_reg[15] ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFFFF)) 
    \string_cent_decenas[1]5_carry_i_8 
       (.I0(Q[15]),
        .I1(Q[17]),
        .I2(Q[19]),
        .I3(Q[21]),
        .I4(\string_cent_decenas[1]5_carry_i_16_n_0 ),
        .I5(\string_cent_decenas[1]5_carry_i_17_n_0 ),
        .O(\string_cent_decenas[1]5_carry_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    \string_cent_decenas[1]5_carry_i_9 
       (.I0(\total_reg[29] ),
        .I1(Q[23]),
        .I2(Q[30]),
        .I3(Q[26]),
        .I4(Q[27]),
        .O(\string_cent_decenas[1]5_carry_i_9_n_0 ));
  (* OPT_MODIFIED = "PROPCONST SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5_inferred__0/i___0_carry 
       (.CI(1'b0),
        .CO({\string_cent_decenas[1]5_inferred__0/i___0_carry_n_0 ,\NLW_string_cent_decenas[1]5_inferred__0/i___0_carry_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({cent[4:3],i___0_carry_i_2_n_0,1'b0}),
        .O(\NLW_string_cent_decenas[1]5_inferred__0/i___0_carry_O_UNCONNECTED [3:0]),
        .S({i___0_carry_i_3_n_0,i___0_carry_i_4_n_0,i___0_carry_i_5_n_0,i___0_carry_i_6_n_0}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5_inferred__0/i___0_carry__0 
       (.CI(\string_cent_decenas[1]5_inferred__0/i___0_carry_n_0 ),
        .CO({\string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_0 ,\NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__0_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({i___0_carry__0_i_1_n_0,i___0_carry__0_i_2_n_0,cent[6:5]}),
        .O({\string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_4 ,\NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__0_O_UNCONNECTED [2:0]}),
        .S({i___0_carry__0_i_4_n_0,i___0_carry__0_i_5_n_0,i___0_carry__0_i_6_n_0,i___0_carry__0_i_7_n_0}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5_inferred__0/i___0_carry__1 
       (.CI(\string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_0 ),
        .CO({\string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_0 ,\NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,i___0_carry__1_i_1_n_0}),
        .O({\string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_4 ,\string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_5 ,\string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_6 ,\string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_7 }),
        .S({i___0_carry__1_i_2_n_0,i___0_carry__1_i_3_n_0,i___0_carry__1_i_4_n_0,i___0_carry__1_i_5_n_0}));
  CARRY4 \string_cent_decenas[1]5_inferred__0/i___0_carry__2 
       (.CI(\string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_0 ),
        .CO({\NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__2_CO_UNCONNECTED [3:2],\string_cent_decenas[1]5_inferred__0/i___0_carry__2_n_2 ,\NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__2_CO_UNCONNECTED [0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__2_O_UNCONNECTED [3:1],\string_cent_decenas[1]5_inferred__0/i___0_carry__2_n_7 }),
        .S({1'b0,1'b0,1'b1,i___0_carry__2_i_1_n_0}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5_inferred__0/i___106_carry 
       (.CI(1'b0),
        .CO({\string_cent_decenas[1]5_inferred__0/i___106_carry_n_0 ,\NLW_string_cent_decenas[1]5_inferred__0/i___106_carry_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({i___106_carry_i_1_n_0,i___106_carry_i_2_n_0,i___106_carry_i_3_n_0,i___106_carry_i_4_n_0}),
        .O(\NLW_string_cent_decenas[1]5_inferred__0/i___106_carry_O_UNCONNECTED [3:0]),
        .S({i___106_carry_i_5_n_0,i___106_carry_i_6_n_0,i___106_carry_i_7_n_0,i___106_carry_i_8_n_0}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5_inferred__0/i___106_carry__0 
       (.CI(\string_cent_decenas[1]5_inferred__0/i___106_carry_n_0 ),
        .CO({\string_cent_decenas[1]5_inferred__0/i___106_carry__0_n_0 ,\NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__0_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({i___106_carry__0_i_1_n_0,i___106_carry__0_i_2_n_0,i___106_carry__0_i_3_n_0,i___106_carry__0_i_4_n_0}),
        .O(\NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__0_O_UNCONNECTED [3:0]),
        .S({i___106_carry__0_i_5_n_0,i___106_carry__0_i_6_n_0,i___106_carry__0_i_7_n_0,i___106_carry__0_i_8_n_0}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5_inferred__0/i___106_carry__1 
       (.CI(\string_cent_decenas[1]5_inferred__0/i___106_carry__0_n_0 ),
        .CO({\string_cent_decenas[1]5_inferred__0/i___106_carry__1_n_0 ,\NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({i___106_carry__1_i_1_n_0,i___106_carry__1_i_2_n_0,i___106_carry__1_i_3_n_0,i___106_carry__1_i_4_n_0}),
        .O(\NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__1_O_UNCONNECTED [3:0]),
        .S({i___106_carry__1_i_5_n_0,i___106_carry__1_i_6_n_0,i___106_carry__1_i_7_n_0,i___106_carry__1_i_8_n_0}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5_inferred__0/i___106_carry__2 
       (.CI(\string_cent_decenas[1]5_inferred__0/i___106_carry__1_n_0 ),
        .CO({\string_cent_decenas[1]5_inferred__0/i___106_carry__2_n_0 ,\NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__2_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({i___106_carry__2_i_1_n_0,i___106_carry__2_i_2_n_0,i___106_carry__2_i_3_n_0,i___106_carry__2_i_4_n_0}),
        .O(\NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__2_O_UNCONNECTED [3:0]),
        .S({i___106_carry__2_i_5_n_0,i___106_carry__2_i_6_n_0,i___106_carry__2_i_7_n_0,i___106_carry__2_i_8_n_0}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5_inferred__0/i___106_carry__3 
       (.CI(\string_cent_decenas[1]5_inferred__0/i___106_carry__2_n_0 ),
        .CO({\string_cent_decenas[1]5_inferred__0/i___106_carry__3_n_0 ,\NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__3_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({i___106_carry__3_i_1_n_0,i___106_carry__3_i_2_n_0,i___106_carry__3_i_3_n_0,i___106_carry__3_i_4_n_0}),
        .O(\NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__3_O_UNCONNECTED [3:0]),
        .S({i___106_carry__3_i_5_n_0,i___106_carry__3_i_6_n_0,i___106_carry__3_i_7_n_0,i___106_carry__3_i_8_n_0}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5_inferred__0/i___106_carry__4 
       (.CI(\string_cent_decenas[1]5_inferred__0/i___106_carry__3_n_0 ),
        .CO({\string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_0 ,\NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__4_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,i___106_carry__4_i_1_n_0,i___106_carry__4_i_2_n_0,i___106_carry__4_i_3_n_0}),
        .O({\string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_4 ,\string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_5 ,\string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_6 ,\string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_7 }),
        .S({i___106_carry__4_i_4_n_0,i___106_carry__4_i_5_n_0,i___106_carry__4_i_6_n_0,i___106_carry__4_i_7_n_0}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5_inferred__0/i___106_carry__5 
       (.CI(\string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_0 ),
        .CO(\NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__5_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__5_O_UNCONNECTED [3:2],\string_cent_decenas[1]5_inferred__0/i___106_carry__5_n_6 ,\string_cent_decenas[1]5_inferred__0/i___106_carry__5_n_7 }),
        .S({1'b0,1'b0,i___106_carry__5_i_1_n_0,i___106_carry__5_i_2_n_0}));
  (* OPT_MODIFIED = "PROPCONST SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5_inferred__0/i___163_carry 
       (.CI(1'b0),
        .CO(\NLW_string_cent_decenas[1]5_inferred__0/i___163_carry_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,i___163_carry_i_1_n_0,1'b0}),
        .O({\NLW_string_cent_decenas[1]5_inferred__0/i___163_carry_O_UNCONNECTED [3],\string_cent_decenas[1]5_inferred__0/i___163_carry_n_5 ,\string_cent_decenas[1]5_inferred__0/i___163_carry_n_6 ,\string_cent_decenas[1]5_inferred__0/i___163_carry_n_7 }),
        .S({1'b0,i___163_carry_i_2_n_0,i___163_carry_i_3_n_0,i___163_carry_i_4_n_0}));
  (* OPT_MODIFIED = "RETARGET SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5_inferred__0/i___169_carry 
       (.CI(1'b0),
        .CO({\string_cent_decenas[1]5_inferred__0/i___169_carry_n_0 ,\NLW_string_cent_decenas[1]5_inferred__0/i___169_carry_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b1),
        .DI({cent[3],i___0_carry_i_2_n_0,i___169_carry_i_2_n_0,\total_reg[0] }),
        .O({\string_cent_decenas[1]5_inferred__0/i___169_carry_n_4 ,\string_cent_decenas[1]5_inferred__0/i___169_carry_n_5 ,\string_cent_decenas[1]5 [1],\NLW_string_cent_decenas[1]5_inferred__0/i___169_carry_O_UNCONNECTED [0]}),
        .S({i___169_carry_i_3_n_0,i___169_carry_i_4_n_0,i___169_carry_i_5_n_0,i___169_carry_i_6_n_0}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5_inferred__0/i___169_carry__0 
       (.CI(\string_cent_decenas[1]5_inferred__0/i___169_carry_n_0 ),
        .CO(\NLW_string_cent_decenas[1]5_inferred__0/i___169_carry__0_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,cent[6:4]}),
        .O({\string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_4 ,\string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_5 ,\string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_6 ,\string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_7 }),
        .S({i___169_carry__0_i_1_n_0,i___169_carry__0_i_2_n_0,i___169_carry__0_i_3_n_0,i___169_carry__0_i_4_n_0}));
  (* OPT_MODIFIED = "RETARGET SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5_inferred__0/i___27_carry 
       (.CI(1'b0),
        .CO({\string_cent_decenas[1]5_inferred__0/i___27_carry_n_0 ,\NLW_string_cent_decenas[1]5_inferred__0/i___27_carry_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({i___27_carry_i_1_n_0,\total_reg[0] ,1'b0,1'b1}),
        .O({\string_cent_decenas[1]5_inferred__0/i___27_carry_n_4 ,\string_cent_decenas[1]5_inferred__0/i___27_carry_n_5 ,\string_cent_decenas[1]5_inferred__0/i___27_carry_n_6 ,\NLW_string_cent_decenas[1]5_inferred__0/i___27_carry_O_UNCONNECTED [0]}),
        .S({i___27_carry_i_2_n_0,i___27_carry_i_3_n_0,i___169_carry_i_5_n_0,i___27_carry_i_5_n_0}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5_inferred__0/i___27_carry__0 
       (.CI(\string_cent_decenas[1]5_inferred__0/i___27_carry_n_0 ),
        .CO({\string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_0 ,\NLW_string_cent_decenas[1]5_inferred__0/i___27_carry__0_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({i___27_carry__0_i_1_n_0,i___27_carry__0_i_2_n_0,i___27_carry__0_i_3_n_0,i___27_carry__0_i_4_n_0}),
        .O({\string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_4 ,\string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_5 ,\string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_6 ,\string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_7 }),
        .S({i___27_carry__0_i_5_n_0,i___27_carry__0_i_6_n_0,i___27_carry__0_i_7_n_0,i___27_carry__0_i_8_n_0}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5_inferred__0/i___27_carry__1 
       (.CI(\string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_0 ),
        .CO({\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0 ,\NLW_string_cent_decenas[1]5_inferred__0/i___27_carry__1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b1,i___27_carry__1_i_1_n_0,i___27_carry__1_i_2_n_0}),
        .O({\NLW_string_cent_decenas[1]5_inferred__0/i___27_carry__1_O_UNCONNECTED [3],\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_5 ,\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_6 ,\string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_7 }),
        .S({1'b1,i___27_carry__1_i_3_n_0,i___27_carry__1_i_4_n_0,i___27_carry__1_i_5_n_0}));
  (* OPT_MODIFIED = "RETARGET SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5_inferred__0/i___56_carry 
       (.CI(1'b0),
        .CO({\string_cent_decenas[1]5_inferred__0/i___56_carry_n_0 ,\NLW_string_cent_decenas[1]5_inferred__0/i___56_carry_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\total_reg[0] ,1'b0,1'b0,1'b1}),
        .O({\string_cent_decenas[1]5_inferred__0/i___56_carry_n_4 ,\string_cent_decenas[1]5_inferred__0/i___56_carry_n_5 ,\string_cent_decenas[1]5_inferred__0/i___56_carry_n_6 ,\NLW_string_cent_decenas[1]5_inferred__0/i___56_carry_O_UNCONNECTED [0]}),
        .S({i___56_carry_i_1_n_0,i___56_carry_i_2_n_0,i___169_carry_i_5_n_0,i___56_carry_i_4_n_0}));
  (* OPT_MODIFIED = "RETARGET SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5_inferred__0/i___56_carry__0 
       (.CI(\string_cent_decenas[1]5_inferred__0/i___56_carry_n_0 ),
        .CO({\string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_0 ,\NLW_string_cent_decenas[1]5_inferred__0/i___56_carry__0_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({cent[4:3],i___0_carry_i_2_n_0,i___56_carry__0_i_2_n_0}),
        .O({\string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_4 ,\string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_5 ,\string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_6 ,\string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_7 }),
        .S({i___56_carry__0_i_3_n_0,i___56_carry__0_i_4_n_0,i___56_carry__0_i_5_n_0,i___56_carry__0_i_6_n_0}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5_inferred__0/i___56_carry__1 
       (.CI(\string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_0 ),
        .CO({\NLW_string_cent_decenas[1]5_inferred__0/i___56_carry__1_CO_UNCONNECTED [3],\string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1 ,\NLW_string_cent_decenas[1]5_inferred__0/i___56_carry__1_CO_UNCONNECTED [1:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,cent[6:5]}),
        .O({\NLW_string_cent_decenas[1]5_inferred__0/i___56_carry__1_O_UNCONNECTED [3:2],\string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_6 ,\string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_7 }),
        .S({1'b0,1'b1,i___56_carry__1_i_1_n_0,i___56_carry__1_i_2_n_0}));
  (* OPT_MODIFIED = "PROPCONST SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5_inferred__0/i___80_carry 
       (.CI(1'b0),
        .CO({\string_cent_decenas[1]5_inferred__0/i___80_carry_n_0 ,\NLW_string_cent_decenas[1]5_inferred__0/i___80_carry_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({cent[6:4],1'b0}),
        .O({\string_cent_decenas[1]5_inferred__0/i___80_carry_n_4 ,\string_cent_decenas[1]5_inferred__0/i___80_carry_n_5 ,\string_cent_decenas[1]5_inferred__0/i___80_carry_n_6 ,\string_cent_decenas[1]5_inferred__0/i___80_carry_n_7 }),
        .S({i___80_carry_i_1_n_0,i___80_carry_i_2_n_0,i___80_carry_i_3_n_0,i___80_carry_i_4_n_0}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5_inferred__0/i___80_carry__0 
       (.CI(\string_cent_decenas[1]5_inferred__0/i___80_carry_n_0 ),
        .CO({\string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_0 ,\NLW_string_cent_decenas[1]5_inferred__0/i___80_carry__0_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({i___80_carry__0_i_1_n_0,i___80_carry__0_i_2_n_0,i___80_carry__0_i_3_n_0,i___80_carry__0_i_4_n_0}),
        .O({\string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_4 ,\string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_5 ,\string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_6 ,\string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_7 }),
        .S({i___80_carry__0_i_5_n_0,i___80_carry__0_i_6_n_0,i___80_carry__0_i_7_n_0,i___80_carry__0_i_8_n_0}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \string_cent_decenas[1]5_inferred__0/i___80_carry__1 
       (.CI(\string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_0 ),
        .CO({\NLW_string_cent_decenas[1]5_inferred__0/i___80_carry__1_CO_UNCONNECTED [3],\string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_1 ,\NLW_string_cent_decenas[1]5_inferred__0/i___80_carry__1_CO_UNCONNECTED [1:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,i___80_carry__1_i_1_n_0}),
        .O({\NLW_string_cent_decenas[1]5_inferred__0/i___80_carry__1_O_UNCONNECTED [3:2],\string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_6 ,\string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_7 }),
        .S({1'b0,1'b1,i___80_carry__1_i_2_n_0,i___80_carry__1_i_3_n_0}));
endmodule

module prescaler
   (output0,
    clk,
    clk_100Mh_IBUF_BUFG,
    reset_IBUF);
  output output0;
  output clk;
  input clk_100Mh_IBUF_BUFG;
  input reset_IBUF;

  wire clk;
  wire clk_100Mh_IBUF_BUFG;
  wire output0;
  wire reset_IBUF;
  wire uut_n_1;

  FDRE #(
    .INIT(1'b0)) 
    clk_pre_reg
       (.C(clk_100Mh_IBUF_BUFG),
        .CE(1'b1),
        .D(uut_n_1),
        .Q(clk),
        .R(1'b0));
  contador uut
       (.clk(clk),
        .clk_100Mh_IBUF_BUFG(clk_100Mh_IBUF_BUFG),
        .clk_pre_reg(uut_n_1),
        .reset(output0),
        .reset_IBUF(reset_IBUF));
endmodule

module sincronizador
   (\sreg_reg[0] ,
    \sreg_reg[0]_0 ,
    \sreg_reg[0]_1 ,
    \sreg_reg[0]_2 ,
    \sreg_reg[0]_3 ,
    clk_BUFG,
    monedas_IBUF);
  output \sreg_reg[0] ;
  output \sreg_reg[0]_0 ;
  output \sreg_reg[0]_1 ;
  output \sreg_reg[0]_2 ;
  output \sreg_reg[0]_3 ;
  input clk_BUFG;
  input [4:0]monedas_IBUF;

  wire clk_BUFG;
  wire [4:0]monedas_IBUF;
  wire \sreg_reg[0] ;
  wire \sreg_reg[0]_0 ;
  wire \sreg_reg[0]_1 ;
  wire \sreg_reg[0]_2 ;
  wire \sreg_reg[0]_3 ;

  synchrnzr inst_synchrnzr0
       (.clk_BUFG(clk_BUFG),
        .monedas_IBUF(monedas_IBUF[0]),
        .\sreg_reg[0]_0 (\sreg_reg[0] ));
  synchrnzr_0 inst_synchrnzr1
       (.clk_BUFG(clk_BUFG),
        .monedas_IBUF(monedas_IBUF[1]),
        .\sreg_reg[0]_0 (\sreg_reg[0]_0 ));
  synchrnzr_1 inst_synchrnzr2
       (.clk_BUFG(clk_BUFG),
        .monedas_IBUF(monedas_IBUF[2]),
        .\sreg_reg[0]_0 (\sreg_reg[0]_1 ));
  synchrnzr_2 inst_synchrnzr3
       (.clk_BUFG(clk_BUFG),
        .monedas_IBUF(monedas_IBUF[3]),
        .\sreg_reg[0]_0 (\sreg_reg[0]_2 ));
  synchrnzr_3 inst_synchrnzr4
       (.clk_BUFG(clk_BUFG),
        .monedas_IBUF(monedas_IBUF[4]),
        .\sreg_reg[0]_0 (\sreg_reg[0]_3 ));
endmodule

module synchrnzr
   (\sreg_reg[0]_0 ,
    clk_BUFG,
    monedas_IBUF);
  output \sreg_reg[0]_0 ;
  input clk_BUFG;
  input [0:0]monedas_IBUF;

  wire clk_BUFG;
  wire [0:0]monedas_IBUF;
  wire \sreg_reg[0]_0 ;
  wire \sreg_reg_n_0_[0] ;

  FDRE #(
    .INIT(1'b0)) 
    \sreg_reg[0] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .D(monedas_IBUF),
        .Q(\sreg_reg_n_0_[0] ),
        .R(1'b0));
  (* srl_name = "\inst_sincronizador/inst_synchrnzr0/sync_out_reg_srl2 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    sync_out_reg_srl2
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(1'b1),
        .CLK(clk_BUFG),
        .D(\sreg_reg_n_0_[0] ),
        .Q(\sreg_reg[0]_0 ));
endmodule

(* ORIG_REF_NAME = "synchrnzr" *) 
module synchrnzr_0
   (\sreg_reg[0]_0 ,
    clk_BUFG,
    monedas_IBUF);
  output \sreg_reg[0]_0 ;
  input clk_BUFG;
  input [0:0]monedas_IBUF;

  wire clk_BUFG;
  wire [0:0]monedas_IBUF;
  wire \sreg_reg[0]_0 ;
  wire \sreg_reg_n_0_[0] ;

  FDRE #(
    .INIT(1'b0)) 
    \sreg_reg[0] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .D(monedas_IBUF),
        .Q(\sreg_reg_n_0_[0] ),
        .R(1'b0));
  (* srl_name = "\inst_sincronizador/inst_synchrnzr1/sync_out_reg_srl2 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    sync_out_reg_srl2
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(1'b1),
        .CLK(clk_BUFG),
        .D(\sreg_reg_n_0_[0] ),
        .Q(\sreg_reg[0]_0 ));
endmodule

(* ORIG_REF_NAME = "synchrnzr" *) 
module synchrnzr_1
   (\sreg_reg[0]_0 ,
    clk_BUFG,
    monedas_IBUF);
  output \sreg_reg[0]_0 ;
  input clk_BUFG;
  input [0:0]monedas_IBUF;

  wire clk_BUFG;
  wire [0:0]monedas_IBUF;
  wire \sreg_reg[0]_0 ;
  wire \sreg_reg_n_0_[0] ;

  FDRE #(
    .INIT(1'b0)) 
    \sreg_reg[0] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .D(monedas_IBUF),
        .Q(\sreg_reg_n_0_[0] ),
        .R(1'b0));
  (* srl_name = "\inst_sincronizador/inst_synchrnzr2/sync_out_reg_srl2 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    sync_out_reg_srl2
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(1'b1),
        .CLK(clk_BUFG),
        .D(\sreg_reg_n_0_[0] ),
        .Q(\sreg_reg[0]_0 ));
endmodule

(* ORIG_REF_NAME = "synchrnzr" *) 
module synchrnzr_2
   (\sreg_reg[0]_0 ,
    clk_BUFG,
    monedas_IBUF);
  output \sreg_reg[0]_0 ;
  input clk_BUFG;
  input [0:0]monedas_IBUF;

  wire clk_BUFG;
  wire [0:0]monedas_IBUF;
  wire \sreg_reg[0]_0 ;
  wire \sreg_reg_n_0_[0] ;

  FDRE #(
    .INIT(1'b0)) 
    \sreg_reg[0] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .D(monedas_IBUF),
        .Q(\sreg_reg_n_0_[0] ),
        .R(1'b0));
  (* srl_name = "\inst_sincronizador/inst_synchrnzr3/sync_out_reg_srl2 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    sync_out_reg_srl2
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(1'b1),
        .CLK(clk_BUFG),
        .D(\sreg_reg_n_0_[0] ),
        .Q(\sreg_reg[0]_0 ));
endmodule

(* ORIG_REF_NAME = "synchrnzr" *) 
module synchrnzr_3
   (\sreg_reg[0]_0 ,
    clk_BUFG,
    monedas_IBUF);
  output \sreg_reg[0]_0 ;
  input clk_BUFG;
  input [0:0]monedas_IBUF;

  wire clk_BUFG;
  wire [0:0]monedas_IBUF;
  wire \sreg_reg[0]_0 ;
  wire \sreg_reg_n_0_[0] ;

  FDRE #(
    .INIT(1'b0)) 
    \sreg_reg[0] 
       (.C(clk_BUFG),
        .CE(1'b1),
        .D(monedas_IBUF),
        .Q(\sreg_reg_n_0_[0] ),
        .R(1'b0));
  (* srl_name = "\inst_sincronizador/inst_synchrnzr4/sync_out_reg_srl2 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    sync_out_reg_srl2
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(1'b1),
        .CLK(clk_BUFG),
        .D(\sreg_reg_n_0_[0] ),
        .Q(\sreg_reg[0]_0 ));
endmodule

(* ECO_CHECKSUM = "fba6a574" *) (* digit_number = "8" *) (* division_prescaler = "250000" *) 
(* num_refrescos = "4" *) (* size_counter = "3" *) (* size_prescaler = "17" *) 
(* width_word = "8" *) 
(* NotValidForBitStream *)
module top
   (clk_100Mh,
    monedas,
    ref_option,
    button_ok,
    reset,
    devolver,
    refresco,
    digit,
    dot,
    elem,
    letra);
  input clk_100Mh;
  input [4:0]monedas;
  input [3:0]ref_option;
  input button_ok;
  input reset;
  output devolver;
  output refresco;
  output [6:0]digit;
  output dot;
  output [7:0]elem;
  output [7:0]letra;

  wire [0:0]\Inst_gestion_dinero/cent ;
  wire button_ok;
  wire button_ok_IBUF;
  wire caracter;
  wire [3:3]caracter1;
  wire clk;
  wire clk_100Mh;
  wire clk_100Mh_IBUF;
  wire clk_100Mh_IBUF_BUFG;
  wire clk_BUFG;
  wire [1:0]current_state;
  wire devolver;
  wire devolver_OBUF;
  wire [6:0]digit;
  wire [6:0]digit_OBUF;
  wire [1:1]\disp_dinero[1]_3 ;
  wire [3:3]\disp_dinero[2]_1 ;
  wire [3:0]\disp_dinero[3]_2 ;
  wire [0:0]\disp_dinero[5]_0 ;
  wire dot;
  wire dot_OBUF;
  wire [7:0]elem;
  wire [7:0]elem_OBUF;
  wire inst_decodmonedas_n_0;
  wire inst_detectorflancos_n_0;
  wire inst_detectorflancos_n_1;
  wire inst_detectorflancos_n_2;
  wire inst_detectorflancos_n_3;
  wire inst_detectorflancos_n_4;
  wire inst_detectorflancos_n_5;
  wire inst_detectorflancos_n_6;
  wire inst_display_n_1;
  wire inst_display_n_10;
  wire inst_display_n_11;
  wire inst_display_n_12;
  wire inst_display_n_13;
  wire inst_display_n_15;
  wire inst_display_n_16;
  wire inst_display_n_17;
  wire inst_display_n_18;
  wire inst_display_n_19;
  wire inst_display_n_2;
  wire inst_display_n_28;
  wire inst_display_n_3;
  wire inst_display_n_4;
  wire inst_display_n_8;
  wire inst_display_n_9;
  wire inst_fsm_n_0;
  wire inst_fsm_n_1;
  wire inst_fsm_n_10;
  wire inst_fsm_n_11;
  wire inst_fsm_n_12;
  wire inst_fsm_n_13;
  wire inst_fsm_n_14;
  wire inst_fsm_n_15;
  wire inst_fsm_n_16;
  wire inst_fsm_n_17;
  wire inst_fsm_n_18;
  wire inst_fsm_n_19;
  wire inst_fsm_n_2;
  wire inst_fsm_n_20;
  wire inst_fsm_n_21;
  wire inst_fsm_n_22;
  wire inst_fsm_n_23;
  wire inst_fsm_n_24;
  wire inst_fsm_n_25;
  wire inst_fsm_n_26;
  wire inst_fsm_n_27;
  wire inst_fsm_n_28;
  wire inst_fsm_n_29;
  wire inst_fsm_n_3;
  wire inst_fsm_n_30;
  wire inst_fsm_n_31;
  wire inst_fsm_n_34;
  wire inst_fsm_n_35;
  wire inst_fsm_n_4;
  wire inst_fsm_n_5;
  wire inst_fsm_n_9;
  wire inst_sincronizador_n_0;
  wire inst_sincronizador_n_1;
  wire inst_sincronizador_n_2;
  wire inst_sincronizador_n_3;
  wire inst_sincronizador_n_4;
  wire [7:0]letra;
  wire lopt;
  wire lopt_1;
  wire lopt_2;
  wire lopt_3;
  wire lopt_4;
  wire lopt_5;
  wire lopt_6;
  wire [4:0]monedas;
  wire [4:0]monedas_IBUF;
  wire output0;
  wire [1:1]plusOp;
  wire [3:0]ref_option;
  wire [3:0]ref_option_IBUF;
  wire refresco;
  wire refresco_OBUF;
  wire [2:0]\reg ;
  wire reset;
  wire reset_IBUF;
  wire [6:0]valor_moneda;
  wire [6:0]\NLW_inst_display_caracter_reg[6]_0_UNCONNECTED ;

initial begin
 $sdf_annotate("top_tb_time_impl.sdf",,,,"tool_control");
end
  IBUF button_ok_IBUF_inst
       (.I(button_ok),
        .O(button_ok_IBUF));
  BUFG clk_100Mh_IBUF_BUFG_inst
       (.I(clk_100Mh_IBUF),
        .O(clk_100Mh_IBUF_BUFG));
  IBUF clk_100Mh_IBUF_inst
       (.I(clk_100Mh),
        .O(clk_100Mh_IBUF));
  BUFG clk_BUFG_inst
       (.I(clk),
        .O(clk_BUFG));
  OBUF devolver_OBUF_inst
       (.I(devolver_OBUF),
        .O(devolver));
  OBUF \digit_OBUF[0]_inst 
       (.I(digit_OBUF[0]),
        .O(digit[0]));
  OBUF \digit_OBUF[1]_inst 
       (.I(digit_OBUF[1]),
        .O(digit[1]));
  OBUF \digit_OBUF[2]_inst 
       (.I(digit_OBUF[2]),
        .O(digit[2]));
  OBUF \digit_OBUF[3]_inst 
       (.I(digit_OBUF[3]),
        .O(digit[3]));
  OBUF \digit_OBUF[4]_inst 
       (.I(digit_OBUF[4]),
        .O(digit[4]));
  OBUF \digit_OBUF[5]_inst 
       (.I(digit_OBUF[5]),
        .O(digit[5]));
  OBUF \digit_OBUF[6]_inst 
       (.I(digit_OBUF[6]),
        .O(digit[6]));
  OBUF dot_OBUF_inst
       (.I(dot_OBUF),
        .O(dot));
  OBUF \elem_OBUF[0]_inst 
       (.I(elem_OBUF[0]),
        .O(elem[0]));
  OBUF \elem_OBUF[1]_inst 
       (.I(elem_OBUF[1]),
        .O(elem[1]));
  OBUF \elem_OBUF[2]_inst 
       (.I(elem_OBUF[2]),
        .O(elem[2]));
  OBUF \elem_OBUF[3]_inst 
       (.I(elem_OBUF[3]),
        .O(elem[3]));
  OBUF \elem_OBUF[4]_inst 
       (.I(elem_OBUF[4]),
        .O(elem[4]));
  OBUF \elem_OBUF[5]_inst 
       (.I(elem_OBUF[5]),
        .O(elem[5]));
  OBUF \elem_OBUF[6]_inst 
       (.I(elem_OBUF[6]),
        .O(elem[6]));
  OBUF \elem_OBUF[7]_inst 
       (.I(elem_OBUF[7]),
        .O(elem[7]));
  decod_monedas inst_decodmonedas
       (.D({inst_detectorflancos_n_0,inst_detectorflancos_n_1,inst_detectorflancos_n_2,inst_detectorflancos_n_3,inst_detectorflancos_n_4,inst_detectorflancos_n_5,inst_detectorflancos_n_6}),
        .Q(valor_moneda),
        .clk_BUFG(clk_BUFG),
        .\next_state_reg[1]_i_1 (inst_fsm_n_24),
        .\v_reg[1]_0 (inst_decodmonedas_n_0));
  detectorflancos inst_detectorflancos
       (.D({inst_detectorflancos_n_0,inst_detectorflancos_n_1,inst_detectorflancos_n_2,inst_detectorflancos_n_3,inst_detectorflancos_n_4,inst_detectorflancos_n_5,inst_detectorflancos_n_6}),
        .clk_BUFG(clk_BUFG),
        .\sreg_reg[0] (inst_sincronizador_n_0),
        .\sreg_reg[0]_0 (inst_sincronizador_n_1),
        .\sreg_reg[0]_1 (inst_sincronizador_n_2),
        .\sreg_reg[0]_2 (inst_sincronizador_n_3),
        .\sreg_reg[0]_3 (inst_sincronizador_n_4));
  display inst_display
       (.CO(caracter1),
        .D({inst_fsm_n_0,inst_fsm_n_1}),
        .E(caracter),
        .Q(current_state),
        .\caracter[2]_i_5 (inst_fsm_n_25),
        .\caracter[2]_i_6 (inst_fsm_n_23),
        .\caracter[4]_i_2 (inst_fsm_n_9),
        .\caracter[4]_i_2_0 (inst_fsm_n_21),
        .\caracter[6]_i_5 (inst_fsm_n_30),
        .\caracter_reg[0]_0 (inst_fsm_n_15),
        .\caracter_reg[0]_1 (inst_fsm_n_19),
        .\caracter_reg[1]_0 (inst_fsm_n_13),
        .\caracter_reg[2]_0 (inst_fsm_n_14),
        .\caracter_reg[2]_1 (inst_fsm_n_2),
        .\caracter_reg[2]_2 (inst_fsm_n_12),
        .\caracter_reg[3]_0 (inst_fsm_n_16),
        .\caracter_reg[3]_1 ({inst_fsm_n_3,inst_fsm_n_4,inst_fsm_n_5}),
        .\caracter_reg[3]_2 (inst_fsm_n_20),
        .\caracter_reg[3]_3 (inst_fsm_n_29),
        .\caracter_reg[3]_4 (inst_fsm_n_27),
        .\caracter_reg[4]_0 (inst_fsm_n_35),
        .\caracter_reg[4]_1 (inst_fsm_n_18),
        .\caracter_reg[4]_2 (inst_fsm_n_26),
        .\caracter_reg[6]_0 (\NLW_inst_display_caracter_reg[6]_0_UNCONNECTED [6:0]),
        .\caracter_reg[6]_1 (inst_fsm_n_11),
        .\caracter_reg[6]_2 (inst_fsm_n_22),
        .\caracter_reg[6]_3 (inst_fsm_n_17),
        .cent(\Inst_gestion_dinero/cent ),
        .\charact_dot_reg[0]_0 (inst_display_n_2),
        .\charact_dot_reg[0]_1 (inst_display_n_9),
        .\charact_dot_reg[0]_2 (inst_display_n_12),
        .\charact_dot_reg[0]_3 (inst_display_n_15),
        .\charact_dot_reg[0]_4 (inst_display_n_16),
        .\charact_dot_reg[0]_5 (inst_display_n_19),
        .\charact_dot_reg[0]_6 (inst_display_n_28),
        .\charact_dot_reg[0]_7 (inst_fsm_n_10),
        .clk_BUFG(clk_BUFG),
        .\current_state_reg[0] (inst_display_n_1),
        .\current_state_reg[1] (inst_display_n_3),
        .digit_OBUF(digit_OBUF),
        .\disp_dinero[1]_0 (\disp_dinero[1]_3 ),
        .\disp_dinero[2]_3 (\disp_dinero[2]_1 ),
        .\disp_dinero[3]_1 ({\disp_dinero[3]_2 [3],\disp_dinero[3]_2 [0]}),
        .\disp_dinero[5]_2 (\disp_dinero[5]_0 ),
        .dot_OBUF(dot_OBUF),
        .dot_reg_0(inst_fsm_n_31),
        .dot_reg_1(inst_fsm_n_28),
        .dot_reg_2(inst_fsm_n_34),
        .elem_OBUF(elem_OBUF),
        .lopt(lopt),
        .lopt_1(lopt_1),
        .lopt_2(lopt_2),
        .lopt_3(lopt_3),
        .lopt_4(lopt_4),
        .lopt_5(lopt_5),
        .lopt_6(lopt_6),
        .output0(output0),
        .plusOp(plusOp),
        .\reg_reg[0] (inst_display_n_11),
        .\reg_reg[1] (inst_display_n_4),
        .\reg_reg[1]_0 (inst_display_n_8),
        .\reg_reg[1]_1 (inst_display_n_10),
        .\reg_reg[1]_2 (inst_display_n_18),
        .\reg_reg[2] (\reg ),
        .\reg_reg[2]_0 (inst_display_n_13),
        .\reg_reg[2]_1 (inst_display_n_17),
        .reset_IBUF(reset_IBUF));
  fsm inst_fsm
       (.CO(caracter1),
        .D({inst_fsm_n_0,inst_fsm_n_1}),
        .E(caracter),
        .\FSM_onehot_current_refresco_reg[0] (inst_fsm_n_2),
        .\FSM_onehot_current_refresco_reg[0]_0 (inst_fsm_n_22),
        .\FSM_onehot_current_refresco_reg[0]_1 (inst_fsm_n_28),
        .\FSM_onehot_current_refresco_reg[1] (inst_fsm_n_17),
        .\FSM_onehot_current_refresco_reg[1]_0 (inst_fsm_n_21),
        .\FSM_onehot_current_refresco_reg[1]_1 (inst_fsm_n_25),
        .\FSM_onehot_current_refresco_reg[2] (inst_fsm_n_11),
        .\FSM_onehot_current_refresco_reg[2]_0 (inst_fsm_n_16),
        .\FSM_onehot_current_refresco_reg[3] (inst_fsm_n_23),
        .\FSM_onehot_current_refresco_reg[3]_0 (inst_fsm_n_26),
        .\FSM_onehot_current_refresco_reg[3]_1 (inst_fsm_n_29),
        .\FSM_onehot_current_refresco_reg[3]_2 (inst_fsm_n_30),
        .\FSM_onehot_current_refresco_reg[4] (inst_fsm_n_15),
        .\FSM_onehot_current_refresco_reg[4]_0 (inst_fsm_n_19),
        .Q({inst_fsm_n_3,inst_fsm_n_4,inst_fsm_n_5}),
        .button_ok_IBUF(button_ok_IBUF),
        .\caracter[0]_i_4 (inst_display_n_15),
        .\caracter_reg[0] (inst_display_n_9),
        .\caracter_reg[1] (inst_display_n_17),
        .\caracter_reg[1]_0 (inst_display_n_12),
        .\caracter_reg[1]_1 (inst_display_n_1),
        .\caracter_reg[1]_2 (inst_display_n_3),
        .\caracter_reg[1]_3 (inst_display_n_4),
        .\caracter_reg[1]_4 (inst_display_n_8),
        .\caracter_reg[1]_5 (inst_display_n_16),
        .\caracter_reg[1]_6 (inst_display_n_13),
        .\caracter_reg[1]_7 (inst_display_n_28),
        .\caracter_reg[2] (inst_display_n_11),
        .\caracter_reg[2]_0 (inst_display_n_2),
        .\caracter_reg[2]_1 (inst_display_n_10),
        .\caracter_reg[2]_2 (inst_display_n_18),
        .\caracter_reg[4] (inst_display_n_19),
        .\caracter_reg[6] (\reg ),
        .clk_BUFG(clk_BUFG),
        .\current_state_reg[0]_0 (inst_fsm_n_18),
        .\current_state_reg[0]_1 (inst_fsm_n_20),
        .\current_state_reg[0]_2 (inst_fsm_n_24),
        .\current_state_reg[0]_3 (inst_fsm_n_27),
        .\current_state_reg[0]_4 (inst_fsm_n_35),
        .\current_state_reg[0]_5 (inst_decodmonedas_n_0),
        .\current_state_reg[1]_0 (current_state),
        .\current_state_reg[1]_1 (inst_fsm_n_9),
        .\current_state_reg[1]_2 (inst_fsm_n_13),
        .\current_state_reg[1]_3 (inst_fsm_n_31),
        .\current_state_reg[1]_4 (inst_fsm_n_34),
        .devolver_OBUF(devolver_OBUF),
        .\disp_dinero[1]_0 (\disp_dinero[1]_3 ),
        .\disp_dinero[2]_2 (\disp_dinero[2]_1 ),
        .\disp_dinero[3]_1 ({\disp_dinero[3]_2 [3],\disp_dinero[3]_2 [0]}),
        .\disp_dinero[5]_3 (\disp_dinero[5]_0 ),
        .output0(output0),
        .plusOp(plusOp),
        .ref_option_IBUF(ref_option_IBUF),
        .refresco_OBUF(refresco_OBUF),
        .\reg_reg[0] (inst_fsm_n_10),
        .\reg_reg[1] (inst_fsm_n_12),
        .\reg_reg[1]_0 (inst_fsm_n_14),
        .\total_reg[0] (\Inst_gestion_dinero/cent ),
        .\total_reg[7]_i_2 (valor_moneda));
  prescaler inst_prescaler
       (.clk(clk),
        .clk_100Mh_IBUF_BUFG(clk_100Mh_IBUF_BUFG),
        .output0(output0),
        .reset_IBUF(reset_IBUF));
  sincronizador inst_sincronizador
       (.clk_BUFG(clk_BUFG),
        .monedas_IBUF(monedas_IBUF),
        .\sreg_reg[0] (inst_sincronizador_n_0),
        .\sreg_reg[0]_0 (inst_sincronizador_n_1),
        .\sreg_reg[0]_1 (inst_sincronizador_n_2),
        .\sreg_reg[0]_2 (inst_sincronizador_n_3),
        .\sreg_reg[0]_3 (inst_sincronizador_n_4));
  (* OPT_MODIFIED = "SWEEP" *) 
  OBUF \letra_OBUF[0]_inst 
       (.I(lopt),
        .O(letra[0]));
  (* OPT_MODIFIED = "SWEEP" *) 
  OBUF \letra_OBUF[1]_inst 
       (.I(lopt_1),
        .O(letra[1]));
  (* OPT_MODIFIED = "SWEEP" *) 
  OBUF \letra_OBUF[2]_inst 
       (.I(lopt_2),
        .O(letra[2]));
  (* OPT_MODIFIED = "SWEEP" *) 
  OBUF \letra_OBUF[3]_inst 
       (.I(lopt_3),
        .O(letra[3]));
  (* OPT_MODIFIED = "SWEEP" *) 
  OBUF \letra_OBUF[4]_inst 
       (.I(lopt_4),
        .O(letra[4]));
  (* OPT_MODIFIED = "SWEEP" *) 
  OBUF \letra_OBUF[5]_inst 
       (.I(lopt_5),
        .O(letra[5]));
  (* OPT_MODIFIED = "SWEEP" *) 
  OBUF \letra_OBUF[6]_inst 
       (.I(lopt_6),
        .O(letra[6]));
  OBUF \letra_OBUF[7]_inst 
       (.I(1'b0),
        .O(letra[7]));
  IBUF \monedas_IBUF[0]_inst 
       (.I(monedas[0]),
        .O(monedas_IBUF[0]));
  IBUF \monedas_IBUF[1]_inst 
       (.I(monedas[1]),
        .O(monedas_IBUF[1]));
  IBUF \monedas_IBUF[2]_inst 
       (.I(monedas[2]),
        .O(monedas_IBUF[2]));
  IBUF \monedas_IBUF[3]_inst 
       (.I(monedas[3]),
        .O(monedas_IBUF[3]));
  IBUF \monedas_IBUF[4]_inst 
       (.I(monedas[4]),
        .O(monedas_IBUF[4]));
  IBUF \ref_option_IBUF[0]_inst 
       (.I(ref_option[0]),
        .O(ref_option_IBUF[0]));
  IBUF \ref_option_IBUF[1]_inst 
       (.I(ref_option[1]),
        .O(ref_option_IBUF[1]));
  IBUF \ref_option_IBUF[2]_inst 
       (.I(ref_option[2]),
        .O(ref_option_IBUF[2]));
  IBUF \ref_option_IBUF[3]_inst 
       (.I(ref_option[3]),
        .O(ref_option_IBUF[3]));
  OBUF refresco_OBUF_inst
       (.I(refresco_OBUF),
        .O(refresco));
  IBUF reset_IBUF_inst
       (.I(reset),
        .O(reset_IBUF));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
