-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Sun Dec 27 17:43:41 2020
-- Host        : LAPTOP-7BL7BHFF running 64-bit major release  (build 9200)
-- Command     : write_vhdl -mode funcsim -nolib -force -file
--               C:/Users/Inees/Desktop/Trabajo_SED/MaquinaExpendedora_aux/MaquinaExpendedora_aux.sim/sim_1/impl/func/xsim/top_tb_func_impl.vhd
-- Design      : top
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tcsg324-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity contador is
  port (
    reset : out STD_LOGIC;
    clk_pre_reg : out STD_LOGIC;
    clk_100Mh_IBUF_BUFG : in STD_LOGIC;
    reset_IBUF : in STD_LOGIC;
    clk : in STD_LOGIC
  );
end contador;

architecture STRUCTURE of contador is
  signal clk_pre_i_2_n_0 : STD_LOGIC;
  signal clk_pre_i_3_n_0 : STD_LOGIC;
  signal clk_pre_i_4_n_0 : STD_LOGIC;
  signal \reg[0]_i_2_n_0\ : STD_LOGIC;
  signal reg_reg : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal \reg_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \reg_reg[0]_i_1_n_4\ : STD_LOGIC;
  signal \reg_reg[0]_i_1_n_5\ : STD_LOGIC;
  signal \reg_reg[0]_i_1_n_6\ : STD_LOGIC;
  signal \reg_reg[0]_i_1_n_7\ : STD_LOGIC;
  signal \reg_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \reg_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \reg_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \reg_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \reg_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \reg_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \reg_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \reg_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \reg_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \reg_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \reg_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \reg_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \reg_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \reg_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \reg_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \reg_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal \^reset\ : STD_LOGIC;
  signal \NLW_reg_reg[0]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_reg_reg[12]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_reg_reg[16]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_reg_reg[16]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_reg_reg[4]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_reg_reg[8]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  attribute OPT_MODIFIED : string;
  attribute OPT_MODIFIED of \reg_reg[0]_i_1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \reg_reg[12]_i_1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \reg_reg[4]_i_1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \reg_reg[8]_i_1\ : label is "SWEEP";
begin
  reset <= \^reset\;
clk_pre_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => clk_pre_i_2_n_0,
      I1 => clk_pre_i_3_n_0,
      I2 => clk_pre_i_4_n_0,
      I3 => clk,
      O => clk_pre_reg
    );
clk_pre_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0080000000000000"
    )
        port map (
      I0 => reg_reg(13),
      I1 => reg_reg(14),
      I2 => reg_reg(11),
      I3 => reg_reg(12),
      I4 => reg_reg(16),
      I5 => reg_reg(15),
      O => clk_pre_i_2_n_0
    );
clk_pre_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000080"
    )
        port map (
      I0 => reg_reg(0),
      I1 => reg_reg(1),
      I2 => reg_reg(2),
      I3 => reg_reg(4),
      I4 => reg_reg(3),
      O => clk_pre_i_3_n_0
    );
clk_pre_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000010"
    )
        port map (
      I0 => reg_reg(7),
      I1 => reg_reg(8),
      I2 => reg_reg(6),
      I3 => reg_reg(5),
      I4 => reg_reg(10),
      I5 => reg_reg(9),
      O => clk_pre_i_4_n_0
    );
\current_state[1]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => reset_IBUF,
      O => \^reset\
    );
\reg[0]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => reg_reg(0),
      O => \reg[0]_i_2_n_0\
    );
\reg_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[0]_i_1_n_7\,
      Q => reg_reg(0)
    );
\reg_reg[0]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \reg_reg[0]_i_1_n_0\,
      CO(2 downto 0) => \NLW_reg_reg[0]_i_1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \reg_reg[0]_i_1_n_4\,
      O(2) => \reg_reg[0]_i_1_n_5\,
      O(1) => \reg_reg[0]_i_1_n_6\,
      O(0) => \reg_reg[0]_i_1_n_7\,
      S(3 downto 1) => reg_reg(3 downto 1),
      S(0) => \reg[0]_i_2_n_0\
    );
\reg_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[8]_i_1_n_5\,
      Q => reg_reg(10)
    );
\reg_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[8]_i_1_n_4\,
      Q => reg_reg(11)
    );
\reg_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[12]_i_1_n_7\,
      Q => reg_reg(12)
    );
\reg_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \reg_reg[8]_i_1_n_0\,
      CO(3) => \reg_reg[12]_i_1_n_0\,
      CO(2 downto 0) => \NLW_reg_reg[12]_i_1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \reg_reg[12]_i_1_n_4\,
      O(2) => \reg_reg[12]_i_1_n_5\,
      O(1) => \reg_reg[12]_i_1_n_6\,
      O(0) => \reg_reg[12]_i_1_n_7\,
      S(3 downto 0) => reg_reg(15 downto 12)
    );
\reg_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[12]_i_1_n_6\,
      Q => reg_reg(13)
    );
\reg_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[12]_i_1_n_5\,
      Q => reg_reg(14)
    );
\reg_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[12]_i_1_n_4\,
      Q => reg_reg(15)
    );
\reg_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[16]_i_1_n_7\,
      Q => reg_reg(16)
    );
\reg_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \reg_reg[12]_i_1_n_0\,
      CO(3 downto 0) => \NLW_reg_reg[16]_i_1_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_reg_reg[16]_i_1_O_UNCONNECTED\(3 downto 1),
      O(0) => \reg_reg[16]_i_1_n_7\,
      S(3 downto 1) => B"000",
      S(0) => reg_reg(16)
    );
\reg_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[0]_i_1_n_6\,
      Q => reg_reg(1)
    );
\reg_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[0]_i_1_n_5\,
      Q => reg_reg(2)
    );
\reg_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[0]_i_1_n_4\,
      Q => reg_reg(3)
    );
\reg_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[4]_i_1_n_7\,
      Q => reg_reg(4)
    );
\reg_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \reg_reg[0]_i_1_n_0\,
      CO(3) => \reg_reg[4]_i_1_n_0\,
      CO(2 downto 0) => \NLW_reg_reg[4]_i_1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \reg_reg[4]_i_1_n_4\,
      O(2) => \reg_reg[4]_i_1_n_5\,
      O(1) => \reg_reg[4]_i_1_n_6\,
      O(0) => \reg_reg[4]_i_1_n_7\,
      S(3 downto 0) => reg_reg(7 downto 4)
    );
\reg_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[4]_i_1_n_6\,
      Q => reg_reg(5)
    );
\reg_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[4]_i_1_n_5\,
      Q => reg_reg(6)
    );
\reg_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[4]_i_1_n_4\,
      Q => reg_reg(7)
    );
\reg_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[8]_i_1_n_7\,
      Q => reg_reg(8)
    );
\reg_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \reg_reg[4]_i_1_n_0\,
      CO(3) => \reg_reg[8]_i_1_n_0\,
      CO(2 downto 0) => \NLW_reg_reg[8]_i_1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \reg_reg[8]_i_1_n_4\,
      O(2) => \reg_reg[8]_i_1_n_5\,
      O(1) => \reg_reg[8]_i_1_n_6\,
      O(0) => \reg_reg[8]_i_1_n_7\,
      S(3 downto 0) => reg_reg(11 downto 8)
    );
\reg_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[8]_i_1_n_6\,
      Q => reg_reg(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \contador__parameterized1\ is
  port (
    \charact_dot_reg[0]\ : out STD_LOGIC;
    \charact_dot_reg[0]_0\ : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \charact_dot_reg[0]_1\ : out STD_LOGIC;
    \reg_reg[0]_0\ : out STD_LOGIC;
    \charact_dot_reg[0]_2\ : out STD_LOGIC;
    \FSM_onehot_current_refresco_reg[4]\ : out STD_LOGIC;
    \reg_reg[1]_0\ : out STD_LOGIC;
    \reg_reg[2]_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \current_state_reg[1]\ : out STD_LOGIC;
    \current_state_reg[0]\ : out STD_LOGIC;
    \charact_dot_reg[0]_3\ : out STD_LOGIC;
    \current_state_reg[0]_0\ : out STD_LOGIC;
    \reg_reg[2]_1\ : out STD_LOGIC;
    \FSM_onehot_current_refresco_reg[1]\ : out STD_LOGIC;
    \FSM_onehot_current_refresco_reg[2]\ : out STD_LOGIC;
    \charact_dot_reg[0]_4\ : out STD_LOGIC;
    \FSM_onehot_current_refresco_reg[0]\ : out STD_LOGIC;
    \reg_reg[1]_1\ : out STD_LOGIC;
    \reg_reg[1]_2\ : out STD_LOGIC;
    elem_OBUF : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \current_state_reg[1]_0\ : out STD_LOGIC;
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    dot : out STD_LOGIC;
    \charact_dot_reg[0]_5\ : out STD_LOGIC;
    \current_state_reg[1]_1\ : out STD_LOGIC;
    \disp_dinero[1]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter_reg[3]\ : in STD_LOGIC;
    \caracter_reg[1]\ : in STD_LOGIC;
    \caracter_reg[1]_0\ : in STD_LOGIC;
    \caracter_reg[1]_1\ : in STD_LOGIC;
    \caracter_reg[1]_2\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 4 downto 0 );
    \caracter_reg[4]\ : in STD_LOGIC;
    \charact_dot_reg[0]_6\ : in STD_LOGIC;
    dot_reg : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \caracter_reg[0]\ : in STD_LOGIC;
    \caracter_reg[0]_0\ : in STD_LOGIC;
    \disp_dinero[5]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    cent : in STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter_reg[0]_1\ : in STD_LOGIC;
    \caracter_reg[0]_2\ : in STD_LOGIC;
    \caracter_reg[0]_3\ : in STD_LOGIC;
    \caracter[2]_i_2_0\ : in STD_LOGIC;
    \caracter_reg[2]\ : in STD_LOGIC;
    \caracter_reg[6]\ : in STD_LOGIC;
    \caracter[2]_i_6\ : in STD_LOGIC;
    \caracter[2]_i_6_0\ : in STD_LOGIC;
    reset_IBUF : in STD_LOGIC;
    dot_reg_0 : in STD_LOGIC;
    \charact_dot_reg[0]_7\ : in STD_LOGIC;
    \caracter_reg[2]_0\ : in STD_LOGIC;
    \caracter_reg[2]_1\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    output0 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \contador__parameterized1\ : entity is "contador";
end \contador__parameterized1\;

architecture STRUCTURE of \contador__parameterized1\ is
  signal \^co\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \caracter[0]_i_13_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_14_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_15_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_16_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_17_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_2_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_3_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_5_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_6_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_7_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_8_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_2_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_3_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_4_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_8_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_9_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_2_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_3_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_5_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_7_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_8_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_15_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_16_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_18_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_33_n_0\ : STD_LOGIC;
  signal \caracter[4]_i_10_n_0\ : STD_LOGIC;
  signal \caracter[4]_i_2_n_0\ : STD_LOGIC;
  signal \caracter[4]_i_3_n_0\ : STD_LOGIC;
  signal \caracter[4]_i_4_n_0\ : STD_LOGIC;
  signal \caracter[4]_i_5_n_0\ : STD_LOGIC;
  signal \caracter[4]_i_6_n_0\ : STD_LOGIC;
  signal \caracter[4]_i_7_n_0\ : STD_LOGIC;
  signal \caracter[4]_i_8_n_0\ : STD_LOGIC;
  signal \caracter[4]_i_9_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_13_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_15_n_0\ : STD_LOGIC;
  signal \^charact_dot_reg[0]\ : STD_LOGIC;
  signal \^charact_dot_reg[0]_0\ : STD_LOGIC;
  signal \^charact_dot_reg[0]_1\ : STD_LOGIC;
  signal \^charact_dot_reg[0]_2\ : STD_LOGIC;
  signal \^charact_dot_reg[0]_3\ : STD_LOGIC;
  signal \^charact_dot_reg[0]_4\ : STD_LOGIC;
  signal \^current_state_reg[0]\ : STD_LOGIC;
  signal \^current_state_reg[0]_0\ : STD_LOGIC;
  signal \^current_state_reg[1]\ : STD_LOGIC;
  signal dot_i_10_n_0 : STD_LOGIC;
  signal dot_i_11_n_0 : STD_LOGIC;
  signal dot_i_12_n_0 : STD_LOGIC;
  signal dot_i_13_n_0 : STD_LOGIC;
  signal dot_i_14_n_0 : STD_LOGIC;
  signal dot_i_15_n_0 : STD_LOGIC;
  signal dot_i_7_n_0 : STD_LOGIC;
  signal dot_i_8_n_0 : STD_LOGIC;
  signal plusOp : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \reg[1]_i_1_n_0\ : STD_LOGIC;
  signal \reg[2]_i_1_n_0\ : STD_LOGIC;
  signal \^reg_reg[0]_0\ : STD_LOGIC;
  signal \^reg_reg[1]_0\ : STD_LOGIC;
  signal \^reg_reg[2]_0\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_dot_reg_i_5_CO_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_dot_reg_i_5_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \caracter[0]_i_13\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \caracter[0]_i_16\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \caracter[0]_i_3\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \caracter[0]_i_7\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \caracter[0]_i_8\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \caracter[1]_i_12\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \caracter[1]_i_8\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \caracter[2]_i_16\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \caracter[2]_i_7\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \caracter[3]_i_18\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \caracter[3]_i_3\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \caracter[3]_i_8\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \caracter[3]_i_9\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \caracter[4]_i_10\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \caracter[4]_i_11\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \caracter[4]_i_12\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \caracter[4]_i_13\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \caracter[4]_i_9\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \caracter[6]_i_12\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \caracter[6]_i_13\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \caracter[6]_i_14\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \caracter[6]_i_15\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \caracter[6]_i_4\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \caracter[6]_i_8\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of dot_i_1 : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of dot_i_2 : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of dot_i_3 : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of dot_i_7 : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of dot_i_8 : label is "soft_lutpair12";
  attribute OPT_MODIFIED : string;
  attribute OPT_MODIFIED of dot_reg_i_5 : label is "SWEEP";
  attribute SOFT_HLUTNM of \elem_OBUF[0]_inst_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \elem_OBUF[1]_inst_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \elem_OBUF[2]_inst_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \elem_OBUF[3]_inst_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \elem_OBUF[4]_inst_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \elem_OBUF[5]_inst_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \elem_OBUF[6]_inst_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \elem_OBUF[7]_inst_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \reg[0]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \reg[1]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \reg[2]_i_1\ : label is "soft_lutpair9";
begin
  CO(0) <= \^co\(0);
  \charact_dot_reg[0]\ <= \^charact_dot_reg[0]\;
  \charact_dot_reg[0]_0\ <= \^charact_dot_reg[0]_0\;
  \charact_dot_reg[0]_1\ <= \^charact_dot_reg[0]_1\;
  \charact_dot_reg[0]_2\ <= \^charact_dot_reg[0]_2\;
  \charact_dot_reg[0]_3\ <= \^charact_dot_reg[0]_3\;
  \charact_dot_reg[0]_4\ <= \^charact_dot_reg[0]_4\;
  \current_state_reg[0]\ <= \^current_state_reg[0]\;
  \current_state_reg[0]_0\ <= \^current_state_reg[0]_0\;
  \current_state_reg[1]\ <= \^current_state_reg[1]\;
  \reg_reg[0]_0\ <= \^reg_reg[0]_0\;
  \reg_reg[1]_0\ <= \^reg_reg[1]_0\;
  \reg_reg[2]_0\(2 downto 0) <= \^reg_reg[2]_0\(2 downto 0);
\caracter[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFEA"
    )
        port map (
      I0 => \caracter[0]_i_2_n_0\,
      I1 => \caracter[0]_i_3_n_0\,
      I2 => \caracter_reg[0]\,
      I3 => \caracter_reg[0]_0\,
      I4 => \caracter[0]_i_5_n_0\,
      I5 => \caracter[0]_i_6_n_0\,
      O => D(0)
    );
\caracter[0]_i_13\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF2A80"
    )
        port map (
      I0 => Q(2),
      I1 => \^reg_reg[2]_0\(1),
      I2 => \^reg_reg[2]_0\(0),
      I3 => \^reg_reg[2]_0\(2),
      I4 => Q(0),
      O => \caracter[0]_i_13_n_0\
    );
\caracter[0]_i_14\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F000808000008080"
    )
        port map (
      I0 => \caracter[6]_i_13_n_0\,
      I1 => Q(1),
      I2 => \^charact_dot_reg[0]_3\,
      I3 => cent(0),
      I4 => \^reg_reg[0]_0\,
      I5 => \caracter_reg[0]_1\,
      O => \caracter[0]_i_14_n_0\
    );
\caracter[0]_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"222200002F220000"
    )
        port map (
      I0 => \^current_state_reg[0]\,
      I1 => \^charact_dot_reg[0]_3\,
      I2 => dot_i_7_n_0,
      I3 => dot_reg(1),
      I4 => \disp_dinero[5]_1\(0),
      I5 => dot_i_8_n_0,
      O => \caracter[0]_i_15_n_0\
    );
\caracter[0]_i_16\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000009"
    )
        port map (
      I0 => \charact_dot_reg[0]_6\,
      I1 => \^reg_reg[2]_0\(0),
      I2 => dot_reg(0),
      I3 => dot_reg(1),
      I4 => \^co\(0),
      O => \caracter[0]_i_16_n_0\
    );
\caracter[0]_i_17\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8F888FFF88888888"
    )
        port map (
      I0 => Q(4),
      I1 => \^current_state_reg[0]_0\,
      I2 => \^charact_dot_reg[0]\,
      I3 => \^reg_reg[1]_0\,
      I4 => \^charact_dot_reg[0]_3\,
      I5 => \caracter_reg[2]\,
      O => \caracter[0]_i_17_n_0\
    );
\caracter[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFF88F8"
    )
        port map (
      I0 => \caracter[0]_i_7_n_0\,
      I1 => \caracter[0]_i_8_n_0\,
      I2 => \caracter_reg[0]_1\,
      I3 => \^charact_dot_reg[0]\,
      I4 => \caracter_reg[0]_2\,
      I5 => \caracter_reg[0]_3\,
      O => \caracter[0]_i_2_n_0\
    );
\caracter[0]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7E7A"
    )
        port map (
      I0 => \^reg_reg[2]_0\(2),
      I1 => \^reg_reg[2]_0\(0),
      I2 => \^reg_reg[2]_0\(1),
      I3 => \charact_dot_reg[0]_6\,
      O => \caracter[0]_i_3_n_0\
    );
\caracter[0]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF40404040404040"
    )
        port map (
      I0 => \^charact_dot_reg[0]\,
      I1 => \caracter[6]_i_13_n_0\,
      I2 => dot_reg(0),
      I3 => \^current_state_reg[0]_0\,
      I4 => \^charact_dot_reg[0]_0\,
      I5 => \caracter[0]_i_13_n_0\,
      O => \caracter[0]_i_5_n_0\
    );
\caracter[0]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFEEEEFEEE"
    )
        port map (
      I0 => \caracter[0]_i_14_n_0\,
      I1 => \caracter[0]_i_15_n_0\,
      I2 => Q(3),
      I3 => \caracter[0]_i_16_n_0\,
      I4 => \^reg_reg[1]_0\,
      I5 => \caracter[0]_i_17_n_0\,
      O => \caracter[0]_i_6_n_0\
    );
\caracter[0]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00010100"
    )
        port map (
      I0 => \^reg_reg[1]_0\,
      I1 => dot_reg(1),
      I2 => \^co\(0),
      I3 => \charact_dot_reg[0]_6\,
      I4 => \^reg_reg[2]_0\(0),
      O => \caracter[0]_i_7_n_0\
    );
\caracter[0]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"2EF02200"
    )
        port map (
      I0 => Q(0),
      I1 => \charact_dot_reg[0]_6\,
      I2 => \^reg_reg[2]_0\(1),
      I3 => \^reg_reg[2]_0\(0),
      I4 => Q(3),
      O => \caracter[0]_i_8_n_0\
    );
\caracter[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFEFE"
    )
        port map (
      I0 => \caracter[1]_i_2_n_0\,
      I1 => \caracter[1]_i_3_n_0\,
      I2 => \caracter[1]_i_4_n_0\,
      I3 => \^charact_dot_reg[0]_1\,
      I4 => \caracter_reg[1]\,
      I5 => \caracter_reg[1]_0\,
      O => D(1)
    );
\caracter[1]_i_12\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^reg_reg[2]_0\(0),
      I1 => \^reg_reg[2]_0\(1),
      O => plusOp(1)
    );
\caracter[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFEAAA"
    )
        port map (
      I0 => \caracter_reg[1]_1\,
      I1 => \^reg_reg[0]_0\,
      I2 => \caracter[3]_i_18_n_0\,
      I3 => \^charact_dot_reg[0]_2\,
      I4 => \caracter[3]_i_16_n_0\,
      I5 => \caracter_reg[1]_2\,
      O => \caracter[1]_i_2_n_0\
    );
\caracter[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFF8808"
    )
        port map (
      I0 => Q(0),
      I1 => \^current_state_reg[0]_0\,
      I2 => \^charact_dot_reg[0]_3\,
      I3 => \^charact_dot_reg[0]_2\,
      I4 => \caracter[1]_i_8_n_0\,
      I5 => \caracter[1]_i_9_n_0\,
      O => \caracter[1]_i_3_n_0\
    );
\caracter[1]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0101010001000100"
    )
        port map (
      I0 => \^co\(0),
      I1 => \^reg_reg[1]_0\,
      I2 => \^charact_dot_reg[0]\,
      I3 => dot_reg(0),
      I4 => dot_reg(1),
      I5 => \disp_dinero[1]_0\(0),
      O => \caracter[1]_i_4_n_0\
    );
\caracter[1]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00800800"
    )
        port map (
      I0 => \caracter[6]_i_13_n_0\,
      I1 => Q(3),
      I2 => \^reg_reg[2]_0\(0),
      I3 => \^reg_reg[2]_0\(1),
      I4 => \charact_dot_reg[0]_6\,
      O => \caracter[1]_i_8_n_0\
    );
\caracter[1]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EAFAFAAAAAAAAAAA"
    )
        port map (
      I0 => \caracter[2]_i_2_0\,
      I1 => \^reg_reg[1]_0\,
      I2 => \caracter[0]_i_16_n_0\,
      I3 => plusOp(1),
      I4 => \^charact_dot_reg[0]_4\,
      I5 => Q(0),
      O => \caracter[1]_i_9_n_0\
    );
\caracter[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \caracter[2]_i_2_n_0\,
      I1 => \caracter[2]_i_3_n_0\,
      I2 => \caracter_reg[2]_0\,
      I3 => \caracter[2]_i_5_n_0\,
      I4 => \caracter_reg[2]_1\,
      O => D(2)
    );
\caracter[2]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"20F0202020202020"
    )
        port map (
      I0 => Q(2),
      I1 => \^charact_dot_reg[0]_3\,
      I2 => \^current_state_reg[0]_0\,
      I3 => \^reg_reg[1]_0\,
      I4 => Q(3),
      I5 => \^charact_dot_reg[0]_4\,
      O => \FSM_onehot_current_refresco_reg[2]\
    );
\caracter[2]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000088006640"
    )
        port map (
      I0 => \^charact_dot_reg[0]_3\,
      I1 => \^reg_reg[0]_0\,
      I2 => Q(0),
      I3 => dot_reg(0),
      I4 => \^reg_reg[1]_0\,
      I5 => \caracter[2]_i_6\,
      O => \FSM_onehot_current_refresco_reg[0]\
    );
\caracter[2]_i_14\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"080808080808FF08"
    )
        port map (
      I0 => \^charact_dot_reg[0]_1\,
      I1 => \caracter[2]_i_6_0\,
      I2 => \caracter_reg[4]\,
      I3 => \caracter_reg[2]\,
      I4 => \^reg_reg[2]_0\(1),
      I5 => \^reg_reg[2]_0\(0),
      O => \reg_reg[1]_2\
    );
\caracter[2]_i_16\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \charact_dot_reg[0]_6\,
      I1 => \^reg_reg[2]_0\(0),
      O => \^charact_dot_reg[0]_4\
    );
\caracter[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFABAAABAAABAA"
    )
        port map (
      I0 => \caracter[3]_i_15_n_0\,
      I1 => \disp_dinero[1]_0\(0),
      I2 => \^charact_dot_reg[0]\,
      I3 => \caracter[2]_i_7_n_0\,
      I4 => \^charact_dot_reg[0]_0\,
      I5 => \caracter_reg[3]\,
      O => \caracter[2]_i_2_n_0\
    );
\caracter[2]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000F4444444"
    )
        port map (
      I0 => \^reg_reg[0]_0\,
      I1 => \caracter[2]_i_8_n_0\,
      I2 => \^charact_dot_reg[0]_0\,
      I3 => Q(0),
      I4 => \^reg_reg[1]_0\,
      I5 => \caracter_reg[4]\,
      O => \caracter[2]_i_3_n_0\
    );
\caracter[2]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0050004000400040"
    )
        port map (
      I0 => \^reg_reg[2]_0\(1),
      I1 => \caracter_reg[2]\,
      I2 => \^reg_reg[2]_0\(0),
      I3 => \charact_dot_reg[0]_6\,
      I4 => \caracter[6]_i_13_n_0\,
      I5 => Q(3),
      O => \caracter[2]_i_5_n_0\
    );
\caracter[2]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000B0000"
    )
        port map (
      I0 => \^reg_reg[0]_0\,
      I1 => \^reg_reg[1]_0\,
      I2 => dot_reg(0),
      I3 => \^co\(0),
      I4 => dot_reg(1),
      O => \caracter[2]_i_7_n_0\
    );
\caracter[2]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000BA000000"
    )
        port map (
      I0 => Q(2),
      I1 => \^reg_reg[2]_0\(2),
      I2 => Q(3),
      I3 => \^reg_reg[2]_0\(0),
      I4 => \^reg_reg[2]_0\(1),
      I5 => \charact_dot_reg[0]_6\,
      O => \caracter[2]_i_8_n_0\
    );
\caracter[3]_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF40000000"
    )
        port map (
      I0 => \charact_dot_reg[0]_6\,
      I1 => \^reg_reg[2]_0\(1),
      I2 => \^reg_reg[2]_0\(0),
      I3 => \caracter[6]_i_13_n_0\,
      I4 => Q(0),
      I5 => \caracter[2]_i_2_0\,
      O => \caracter[3]_i_15_n_0\
    );
\caracter[3]_i_16\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BAAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \caracter[3]_i_33_n_0\,
      I1 => \charact_dot_reg[0]_6\,
      I2 => \^reg_reg[2]_0\(1),
      I3 => \^reg_reg[2]_0\(0),
      I4 => \caracter[6]_i_13_n_0\,
      I5 => Q(2),
      O => \caracter[3]_i_16_n_0\
    );
\caracter[3]_i_18\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \^co\(0),
      I1 => dot_reg(0),
      O => \caracter[3]_i_18_n_0\
    );
\caracter[3]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8013"
    )
        port map (
      I0 => \charact_dot_reg[0]_6\,
      I1 => \^reg_reg[2]_0\(2),
      I2 => \^reg_reg[2]_0\(0),
      I3 => \^reg_reg[2]_0\(1),
      O => \^charact_dot_reg[0]_1\
    );
\caracter[3]_i_33\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000020000000000"
    )
        port map (
      I0 => \^current_state_reg[1]\,
      I1 => \disp_dinero[1]_0\(0),
      I2 => \^reg_reg[2]_0\(2),
      I3 => \^reg_reg[2]_0\(0),
      I4 => \^reg_reg[2]_0\(1),
      I5 => \charact_dot_reg[0]_6\,
      O => \caracter[3]_i_33_n_0\
    );
\caracter[3]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEEEEFEEEEE"
    )
        port map (
      I0 => \caracter[3]_i_15_n_0\,
      I1 => \caracter[3]_i_16_n_0\,
      I2 => \^charact_dot_reg[0]_0\,
      I3 => \^reg_reg[1]_0\,
      I4 => Q(4),
      I5 => \caracter_reg[4]\,
      O => \FSM_onehot_current_refresco_reg[4]\
    );
\caracter[3]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F58558840080088"
    )
        port map (
      I0 => \^reg_reg[2]_0\(2),
      I1 => \caracter_reg[3]\,
      I2 => \charact_dot_reg[0]_6\,
      I3 => \^reg_reg[2]_0\(1),
      I4 => \^reg_reg[2]_0\(0),
      I5 => \caracter[3]_i_18_n_0\,
      O => \reg_reg[2]_1\
    );
\caracter[3]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \^co\(0),
      I1 => dot_reg(1),
      O => \^current_state_reg[1]\
    );
\caracter[3]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0220"
    )
        port map (
      I0 => dot_reg(1),
      I1 => \^co\(0),
      I2 => \charact_dot_reg[0]_6\,
      I3 => \^reg_reg[2]_0\(0),
      O => \current_state_reg[1]_1\
    );
\caracter[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \caracter[4]_i_2_n_0\,
      I1 => \caracter[4]_i_3_n_0\,
      I2 => \caracter[4]_i_4_n_0\,
      I3 => \caracter[4]_i_5_n_0\,
      I4 => \caracter[4]_i_6_n_0\,
      I5 => \caracter[4]_i_7_n_0\,
      O => D(3)
    );
\caracter[4]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"D"
    )
        port map (
      I0 => \^reg_reg[2]_0\(1),
      I1 => \^reg_reg[2]_0\(0),
      O => \caracter[4]_i_10_n_0\
    );
\caracter[4]_i_11\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000100"
    )
        port map (
      I0 => \^reg_reg[1]_0\,
      I1 => dot_reg(0),
      I2 => \^co\(0),
      I3 => dot_reg(1),
      I4 => \^reg_reg[0]_0\,
      O => \^current_state_reg[0]\
    );
\caracter[4]_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"93"
    )
        port map (
      I0 => \charact_dot_reg[0]_6\,
      I1 => \^reg_reg[2]_0\(1),
      I2 => \^reg_reg[2]_0\(0),
      O => \^charact_dot_reg[0]_0\
    );
\caracter[4]_i_13\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00010100"
    )
        port map (
      I0 => dot_reg(0),
      I1 => dot_reg(1),
      I2 => \^co\(0),
      I3 => \charact_dot_reg[0]_6\,
      I4 => \^reg_reg[2]_0\(0),
      O => \^current_state_reg[0]_0\
    );
\caracter[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABBBAAAABAAAAAAA"
    )
        port map (
      I0 => \caracter[4]_i_8_n_0\,
      I1 => \caracter_reg[4]\,
      I2 => \charact_dot_reg[0]_6\,
      I3 => \^reg_reg[2]_0\(0),
      I4 => \caracter[4]_i_9_n_0\,
      I5 => \^reg_reg[2]_0\(1),
      O => \caracter[4]_i_2_n_0\
    );
\caracter[4]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000FF040404"
    )
        port map (
      I0 => \caracter[4]_i_10_n_0\,
      I1 => dot_reg(1),
      I2 => dot_i_8_n_0,
      I3 => \^current_state_reg[0]\,
      I4 => \charact_dot_reg[0]_6\,
      I5 => \disp_dinero[1]_0\(0),
      O => \caracter[4]_i_3_n_0\
    );
\caracter[4]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000040504040"
    )
        port map (
      I0 => \^co\(0),
      I1 => dot_reg(1),
      I2 => \^charact_dot_reg[0]_0\,
      I3 => \^reg_reg[0]_0\,
      I4 => Q(3),
      I5 => dot_i_8_n_0,
      O => \caracter[4]_i_4_n_0\
    );
\caracter[4]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00F00040C040C040"
    )
        port map (
      I0 => \^charact_dot_reg[0]_3\,
      I1 => Q(2),
      I2 => \^current_state_reg[0]_0\,
      I3 => \^reg_reg[1]_0\,
      I4 => Q(0),
      I5 => \^charact_dot_reg[0]\,
      O => \caracter[4]_i_5_n_0\
    );
\caracter[4]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200200000"
    )
        port map (
      I0 => Q(3),
      I1 => \caracter_reg[4]\,
      I2 => \charact_dot_reg[0]_6\,
      I3 => \^reg_reg[2]_0\(2),
      I4 => \^reg_reg[2]_0\(0),
      I5 => \^reg_reg[2]_0\(1),
      O => \caracter[4]_i_6_n_0\
    );
\caracter[4]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A8000000000000A8"
    )
        port map (
      I0 => \caracter[6]_i_13_n_0\,
      I1 => dot_reg(0),
      I2 => Q(1),
      I3 => \^reg_reg[2]_0\(0),
      I4 => \^reg_reg[2]_0\(1),
      I5 => \charact_dot_reg[0]_6\,
      O => \caracter[4]_i_7_n_0\
    );
\caracter[4]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000010000000000"
    )
        port map (
      I0 => \^charact_dot_reg[0]\,
      I1 => dot_reg(0),
      I2 => \disp_dinero[1]_0\(0),
      I3 => \^reg_reg[1]_0\,
      I4 => \^reg_reg[0]_0\,
      I5 => \^current_state_reg[1]\,
      O => \caracter[4]_i_8_n_0\
    );
\caracter[4]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6A00"
    )
        port map (
      I0 => \^reg_reg[2]_0\(2),
      I1 => \^reg_reg[2]_0\(0),
      I2 => \^reg_reg[2]_0\(1),
      I3 => Q(0),
      O => \caracter[4]_i_9_n_0\
    );
\caracter[6]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFF0000E020"
    )
        port map (
      I0 => \^reg_reg[2]_0\(1),
      I1 => \^reg_reg[2]_0\(0),
      I2 => Q(2),
      I3 => \charact_dot_reg[0]_6\,
      I4 => \caracter_reg[4]\,
      I5 => \caracter[0]_i_13_n_0\,
      O => \reg_reg[1]_1\
    );
\caracter[6]_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D3"
    )
        port map (
      I0 => \charact_dot_reg[0]_6\,
      I1 => \^reg_reg[2]_0\(1),
      I2 => \^reg_reg[2]_0\(0),
      O => \^charact_dot_reg[0]_3\
    );
\caracter[6]_i_13\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"10010101"
    )
        port map (
      I0 => \^co\(0),
      I1 => dot_reg(1),
      I2 => \^reg_reg[2]_0\(2),
      I3 => \^reg_reg[2]_0\(0),
      I4 => \^reg_reg[2]_0\(1),
      O => \caracter[6]_i_13_n_0\
    );
\caracter[6]_i_14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"D333"
    )
        port map (
      I0 => \charact_dot_reg[0]_6\,
      I1 => \^reg_reg[2]_0\(2),
      I2 => \^reg_reg[2]_0\(0),
      I3 => \^reg_reg[2]_0\(1),
      O => \^charact_dot_reg[0]_2\
    );
\caracter[6]_i_15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0009"
    )
        port map (
      I0 => \charact_dot_reg[0]_6\,
      I1 => \^reg_reg[2]_0\(0),
      I2 => \^co\(0),
      I3 => dot_reg(1),
      O => \caracter[6]_i_15_n_0\
    );
\caracter[6]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BF"
    )
        port map (
      I0 => \charact_dot_reg[0]_6\,
      I1 => \^reg_reg[2]_0\(1),
      I2 => \^reg_reg[2]_0\(0),
      O => \^charact_dot_reg[0]\
    );
\caracter[6]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFC8C0C088880000"
    )
        port map (
      I0 => \caracter_reg[6]\,
      I1 => \^charact_dot_reg[0]_3\,
      I2 => \caracter[6]_i_13_n_0\,
      I3 => \^charact_dot_reg[0]_2\,
      I4 => \caracter[6]_i_15_n_0\,
      I5 => Q(1),
      O => \FSM_onehot_current_refresco_reg[1]\
    );
\caracter[6]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^reg_reg[2]_0\(0),
      I1 => \charact_dot_reg[0]_6\,
      O => \^reg_reg[0]_0\
    );
\charact_dot[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"2FFF2020"
    )
        port map (
      I0 => \charact_dot_reg[0]_7\,
      I1 => dot_i_8_n_0,
      I2 => reset_IBUF,
      I3 => \^co\(0),
      I4 => \charact_dot_reg[0]_6\,
      O => \charact_dot_reg[0]_5\
    );
dot_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"10FF"
    )
        port map (
      I0 => \^reg_reg[1]_0\,
      I1 => dot_reg(0),
      I2 => \charact_dot_reg[0]_7\,
      I3 => \^co\(0),
      O => dot
    );
dot_i_10: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \^reg_reg[2]_0\(1),
      I1 => \^reg_reg[2]_0\(0),
      I2 => \^reg_reg[2]_0\(2),
      O => dot_i_10_n_0
    );
dot_i_11: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^reg_reg[2]_0\(0),
      I1 => \^reg_reg[2]_0\(1),
      O => dot_i_11_n_0
    );
dot_i_12: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^reg_reg[2]_0\(0),
      O => dot_i_12_n_0
    );
dot_i_13: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \^reg_reg[2]_0\(1),
      I1 => \^reg_reg[2]_0\(0),
      I2 => \^reg_reg[2]_0\(2),
      O => dot_i_13_n_0
    );
dot_i_14: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^reg_reg[2]_0\(0),
      I1 => \^reg_reg[2]_0\(1),
      O => dot_i_14_n_0
    );
dot_i_15: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^reg_reg[2]_0\(0),
      I1 => \charact_dot_reg[0]_6\,
      O => dot_i_15_n_0
    );
dot_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00FF00E2"
    )
        port map (
      I0 => dot_reg_0,
      I1 => dot_reg(1),
      I2 => dot_i_7_n_0,
      I3 => \^co\(0),
      I4 => dot_i_8_n_0,
      O => \current_state_reg[1]_0\
    );
dot_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \^reg_reg[2]_0\(1),
      I1 => \^reg_reg[2]_0\(0),
      I2 => \^reg_reg[2]_0\(2),
      O => \^reg_reg[1]_0\
    );
dot_i_7: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => \^reg_reg[2]_0\(0),
      I1 => \^reg_reg[2]_0\(1),
      I2 => \disp_dinero[1]_0\(0),
      O => dot_i_7_n_0
    );
dot_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF6A"
    )
        port map (
      I0 => \^reg_reg[2]_0\(2),
      I1 => \^reg_reg[2]_0\(0),
      I2 => \^reg_reg[2]_0\(1),
      I3 => dot_reg(0),
      O => dot_i_8_n_0
    );
dot_reg_i_5: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \^co\(0),
      CO(2 downto 0) => NLW_dot_reg_i_5_CO_UNCONNECTED(2 downto 0),
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => dot_i_10_n_0,
      DI(1) => dot_i_11_n_0,
      DI(0) => dot_i_12_n_0,
      O(3 downto 0) => NLW_dot_reg_i_5_O_UNCONNECTED(3 downto 0),
      S(3) => '1',
      S(2) => dot_i_13_n_0,
      S(1) => dot_i_14_n_0,
      S(0) => dot_i_15_n_0
    );
\elem_OBUF[0]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFB"
    )
        port map (
      I0 => \^reg_reg[2]_0\(2),
      I1 => reset_IBUF,
      I2 => \^reg_reg[2]_0\(1),
      I3 => \^reg_reg[2]_0\(0),
      O => elem_OBUF(0)
    );
\elem_OBUF[1]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FBFF"
    )
        port map (
      I0 => \^reg_reg[2]_0\(2),
      I1 => reset_IBUF,
      I2 => \^reg_reg[2]_0\(1),
      I3 => \^reg_reg[2]_0\(0),
      O => elem_OBUF(1)
    );
\elem_OBUF[2]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFDF"
    )
        port map (
      I0 => \^reg_reg[2]_0\(1),
      I1 => \^reg_reg[2]_0\(2),
      I2 => reset_IBUF,
      I3 => \^reg_reg[2]_0\(0),
      O => elem_OBUF(2)
    );
\elem_OBUF[3]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => reset_IBUF,
      I1 => \^reg_reg[2]_0\(2),
      I2 => \^reg_reg[2]_0\(1),
      I3 => \^reg_reg[2]_0\(0),
      O => elem_OBUF(3)
    );
\elem_OBUF[4]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFF7"
    )
        port map (
      I0 => reset_IBUF,
      I1 => \^reg_reg[2]_0\(2),
      I2 => \^reg_reg[2]_0\(1),
      I3 => \^reg_reg[2]_0\(0),
      O => elem_OBUF(4)
    );
\elem_OBUF[5]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F7FF"
    )
        port map (
      I0 => reset_IBUF,
      I1 => \^reg_reg[2]_0\(2),
      I2 => \^reg_reg[2]_0\(1),
      I3 => \^reg_reg[2]_0\(0),
      O => elem_OBUF(5)
    );
\elem_OBUF[6]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF7F"
    )
        port map (
      I0 => \^reg_reg[2]_0\(1),
      I1 => reset_IBUF,
      I2 => \^reg_reg[2]_0\(2),
      I3 => \^reg_reg[2]_0\(0),
      O => elem_OBUF(6)
    );
\elem_OBUF[7]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => \^reg_reg[2]_0\(2),
      I1 => \^reg_reg[2]_0\(0),
      I2 => \^reg_reg[2]_0\(1),
      I3 => reset_IBUF,
      O => elem_OBUF(7)
    );
\reg[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^reg_reg[2]_0\(0),
      O => \reg[0]_i_1_n_0\
    );
\reg[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^reg_reg[2]_0\(1),
      I1 => \^reg_reg[2]_0\(0),
      O => \reg[1]_i_1_n_0\
    );
\reg[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \^reg_reg[2]_0\(2),
      I1 => \^reg_reg[2]_0\(0),
      I2 => \^reg_reg[2]_0\(1),
      O => \reg[2]_i_1_n_0\
    );
\reg_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => \reg[0]_i_1_n_0\,
      Q => \^reg_reg[2]_0\(0)
    );
\reg_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => \reg[1]_i_1_n_0\,
      Q => \^reg_reg[2]_0\(1)
    );
\reg_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => \reg[2]_i_1_n_0\,
      Q => \^reg_reg[2]_0\(2)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decod_monedas is
  port (
    \v_reg[1]_0\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 6 downto 0 );
    \next_state_reg[1]_i_1\ : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 6 downto 0 );
    clk_BUFG : in STD_LOGIC;
    lopt : out STD_LOGIC;
    lopt_1 : out STD_LOGIC;
    lopt_2 : out STD_LOGIC;
    lopt_3 : out STD_LOGIC;
    lopt_4 : out STD_LOGIC;
    lopt_5 : out STD_LOGIC;
    lopt_6 : out STD_LOGIC
  );
end decod_monedas;

architecture STRUCTURE of decod_monedas is
  signal \^q\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \v_reg[0]_lopt_replica_1\ : STD_LOGIC;
  signal \v_reg[1]_lopt_replica_1\ : STD_LOGIC;
  signal \v_reg[2]_lopt_replica_1\ : STD_LOGIC;
  signal \v_reg[3]_lopt_replica_1\ : STD_LOGIC;
  signal \v_reg[4]_lopt_replica_1\ : STD_LOGIC;
  signal \v_reg[5]_lopt_replica_1\ : STD_LOGIC;
  signal \v_reg[6]_lopt_replica_1\ : STD_LOGIC;
  attribute OPT_INSERTED_REPDRIVER : boolean;
  attribute OPT_INSERTED_REPDRIVER of \v_reg[0]_lopt_replica\ : label is std.standard.true;
  attribute OPT_MODIFIED : string;
  attribute OPT_MODIFIED of \v_reg[0]_lopt_replica\ : label is "SWEEP";
  attribute OPT_INSERTED_REPDRIVER of \v_reg[1]_lopt_replica\ : label is std.standard.true;
  attribute OPT_MODIFIED of \v_reg[1]_lopt_replica\ : label is "SWEEP";
  attribute OPT_INSERTED_REPDRIVER of \v_reg[2]_lopt_replica\ : label is std.standard.true;
  attribute OPT_MODIFIED of \v_reg[2]_lopt_replica\ : label is "SWEEP";
  attribute OPT_INSERTED_REPDRIVER of \v_reg[3]_lopt_replica\ : label is std.standard.true;
  attribute OPT_MODIFIED of \v_reg[3]_lopt_replica\ : label is "SWEEP";
  attribute OPT_INSERTED_REPDRIVER of \v_reg[4]_lopt_replica\ : label is std.standard.true;
  attribute OPT_MODIFIED of \v_reg[4]_lopt_replica\ : label is "SWEEP";
  attribute OPT_INSERTED_REPDRIVER of \v_reg[5]_lopt_replica\ : label is std.standard.true;
  attribute OPT_MODIFIED of \v_reg[5]_lopt_replica\ : label is "SWEEP";
  attribute OPT_INSERTED_REPDRIVER of \v_reg[6]_lopt_replica\ : label is std.standard.true;
  attribute OPT_MODIFIED of \v_reg[6]_lopt_replica\ : label is "SWEEP";
begin
  Q(6 downto 0) <= \^q\(6 downto 0);
  lopt <= \v_reg[0]_lopt_replica_1\;
  lopt_1 <= \v_reg[1]_lopt_replica_1\;
  lopt_2 <= \v_reg[2]_lopt_replica_1\;
  lopt_3 <= \v_reg[3]_lopt_replica_1\;
  lopt_4 <= \v_reg[4]_lopt_replica_1\;
  lopt_5 <= \v_reg[5]_lopt_replica_1\;
  lopt_6 <= \v_reg[6]_lopt_replica_1\;
\next_state_reg[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF0000FFFE0000"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(4),
      I2 => \^q\(5),
      I3 => \^q\(3),
      I4 => \next_state_reg[1]_i_1\,
      I5 => \^q\(2),
      O => \v_reg[1]_0\
    );
\v_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => D(0),
      Q => \^q\(0),
      R => '0'
    );
\v_reg[0]_lopt_replica\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => D(0),
      Q => \v_reg[0]_lopt_replica_1\,
      R => '0'
    );
\v_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => D(1),
      Q => \^q\(1),
      R => '0'
    );
\v_reg[1]_lopt_replica\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => D(1),
      Q => \v_reg[1]_lopt_replica_1\,
      R => '0'
    );
\v_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => D(2),
      Q => \^q\(2),
      R => '0'
    );
\v_reg[2]_lopt_replica\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => D(2),
      Q => \v_reg[2]_lopt_replica_1\,
      R => '0'
    );
\v_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => D(3),
      Q => \^q\(3),
      R => '0'
    );
\v_reg[3]_lopt_replica\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => D(3),
      Q => \v_reg[3]_lopt_replica_1\,
      R => '0'
    );
\v_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => D(4),
      Q => \^q\(4),
      R => '0'
    );
\v_reg[4]_lopt_replica\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => D(4),
      Q => \v_reg[4]_lopt_replica_1\,
      R => '0'
    );
\v_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => D(5),
      Q => \^q\(5),
      R => '0'
    );
\v_reg[5]_lopt_replica\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => D(5),
      Q => \v_reg[5]_lopt_replica_1\,
      R => '0'
    );
\v_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => D(6),
      Q => \^q\(6),
      R => '0'
    );
\v_reg[6]_lopt_replica\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => D(6),
      Q => \v_reg[6]_lopt_replica_1\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity digito is
  port (
    digit_OBUF : out STD_LOGIC_VECTOR ( 6 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 6 downto 0 )
  );
end digito;

architecture STRUCTURE of digito is
  signal \digit_OBUF[0]_inst_i_2_n_0\ : STD_LOGIC;
  signal \digit_OBUF[1]_inst_i_2_n_0\ : STD_LOGIC;
  signal \digit_OBUF[2]_inst_i_2_n_0\ : STD_LOGIC;
  signal \digit_OBUF[3]_inst_i_2_n_0\ : STD_LOGIC;
  signal \digit_OBUF[4]_inst_i_2_n_0\ : STD_LOGIC;
  signal \digit_OBUF[5]_inst_i_2_n_0\ : STD_LOGIC;
  signal \digit_OBUF[6]_inst_i_2_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \digit_OBUF[0]_inst_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \digit_OBUF[3]_inst_i_1\ : label is "soft_lutpair22";
begin
\digit_OBUF[0]_inst_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => Q(5),
      I1 => \digit_OBUF[0]_inst_i_2_n_0\,
      O => digit_OBUF(0)
    );
\digit_OBUF[0]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0008000390102081"
    )
        port map (
      I0 => Q(0),
      I1 => Q(1),
      I2 => Q(6),
      I3 => Q(3),
      I4 => Q(2),
      I5 => Q(4),
      O => \digit_OBUF[0]_inst_i_2_n_0\
    );
\digit_OBUF[1]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88A2FFFFFFFFFFFF"
    )
        port map (
      I0 => Q(3),
      I1 => Q(4),
      I2 => Q(1),
      I3 => Q(2),
      I4 => Q(5),
      I5 => \digit_OBUF[1]_inst_i_2_n_0\,
      O => digit_OBUF(1)
    );
\digit_OBUF[1]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"761C882A365C088A"
    )
        port map (
      I0 => Q(4),
      I1 => Q(2),
      I2 => Q(0),
      I3 => Q(1),
      I4 => Q(6),
      I5 => Q(3),
      O => \digit_OBUF[1]_inst_i_2_n_0\
    );
\digit_OBUF[2]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A808FFFFFFFFFFFF"
    )
        port map (
      I0 => Q(3),
      I1 => Q(1),
      I2 => Q(2),
      I3 => Q(4),
      I4 => Q(5),
      I5 => \digit_OBUF[2]_inst_i_2_n_0\,
      O => digit_OBUF(2)
    );
\digit_OBUF[2]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0555D0A050057020"
    )
        port map (
      I0 => Q(0),
      I1 => Q(3),
      I2 => Q(6),
      I3 => Q(2),
      I4 => Q(4),
      I5 => Q(1),
      O => \digit_OBUF[2]_inst_i_2_n_0\
    );
\digit_OBUF[3]_inst_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => Q(5),
      I1 => \digit_OBUF[3]_inst_i_2_n_0\,
      O => digit_OBUF(3)
    );
\digit_OBUF[3]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1004904C15630000"
    )
        port map (
      I0 => Q(3),
      I1 => Q(2),
      I2 => Q(0),
      I3 => Q(1),
      I4 => Q(4),
      I5 => Q(6),
      O => \digit_OBUF[3]_inst_i_2_n_0\
    );
\digit_OBUF[4]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFB48FFFFF"
    )
        port map (
      I0 => Q(3),
      I1 => Q(1),
      I2 => Q(4),
      I3 => Q(6),
      I4 => Q(5),
      I5 => \digit_OBUF[4]_inst_i_2_n_0\,
      O => digit_OBUF(4)
    );
\digit_OBUF[4]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5B0BA0005A0AB3F3"
    )
        port map (
      I0 => Q(3),
      I1 => Q(4),
      I2 => Q(1),
      I3 => Q(6),
      I4 => Q(2),
      I5 => Q(0),
      O => \digit_OBUF[4]_inst_i_2_n_0\
    );
\digit_OBUF[5]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF2808FFFF"
    )
        port map (
      I0 => Q(2),
      I1 => Q(3),
      I2 => Q(1),
      I3 => Q(6),
      I4 => Q(5),
      I5 => \digit_OBUF[5]_inst_i_2_n_0\,
      O => digit_OBUF(5)
    );
\digit_OBUF[5]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B3EEBDBBF5DDFD55"
    )
        port map (
      I0 => Q(4),
      I1 => Q(2),
      I2 => Q(3),
      I3 => Q(1),
      I4 => Q(0),
      I5 => Q(6),
      O => \digit_OBUF[5]_inst_i_2_n_0\
    );
\digit_OBUF[6]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAABEAFEEFBAAF"
    )
        port map (
      I0 => \digit_OBUF[6]_inst_i_2_n_0\,
      I1 => Q(1),
      I2 => Q(2),
      I3 => Q(4),
      I4 => Q(3),
      I5 => Q(0),
      O => digit_OBUF(6)
    );
\digit_OBUF[6]_inst_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F7FFD55FFFDFF55F"
    )
        port map (
      I0 => Q(5),
      I1 => Q(2),
      I2 => Q(4),
      I3 => Q(6),
      I4 => Q(3),
      I5 => Q(1),
      O => \digit_OBUF[6]_inst_i_2_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity edgedtctr is
  port (
    D : out STD_LOGIC_VECTOR ( 0 to 0 );
    \sreg_reg[2]_0\ : out STD_LOGIC;
    \sreg_reg[0]_0\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    \v_reg[0]\ : in STD_LOGIC;
    \v_reg[0]_0\ : in STD_LOGIC
  );
end edgedtctr;

architecture STRUCTURE of edgedtctr is
  signal sreg : STD_LOGIC_VECTOR ( 2 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \v[0]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \v[6]_i_3\ : label is "soft_lutpair0";
begin
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \sreg_reg[0]_0\,
      Q => sreg(0),
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => sreg(0),
      Q => sreg(1),
      R => '0'
    );
\sreg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => sreg(1),
      Q => sreg(2),
      R => '0'
    );
\v[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => sreg(2),
      I1 => sreg(1),
      I2 => sreg(0),
      I3 => \v_reg[0]\,
      I4 => \v_reg[0]_0\,
      O => D(0)
    );
\v[6]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => sreg(2),
      I1 => sreg(1),
      I2 => sreg(0),
      O => \sreg_reg[2]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity edgedtctr_4 is
  port (
    sreg : out STD_LOGIC_VECTOR ( 2 downto 0 );
    D : out STD_LOGIC_VECTOR ( 0 to 0 );
    \sreg_reg[2]_0\ : out STD_LOGIC;
    \sreg_reg[0]_0\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    \v_reg[3]\ : in STD_LOGIC;
    \v_reg[3]_0\ : in STD_LOGIC;
    \v_reg[3]_1\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of edgedtctr_4 : entity is "edgedtctr";
end edgedtctr_4;

architecture STRUCTURE of edgedtctr_4 is
  signal \^sreg\ : STD_LOGIC_VECTOR ( 2 downto 0 );
begin
  sreg(2 downto 0) <= \^sreg\(2 downto 0);
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \sreg_reg[0]_0\,
      Q => \^sreg\(0),
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \^sreg\(0),
      Q => \^sreg\(1),
      R => '0'
    );
\sreg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \^sreg\(1),
      Q => \^sreg\(2),
      R => '0'
    );
\v[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0008000000000000"
    )
        port map (
      I0 => \v_reg[3]\,
      I1 => \v_reg[3]_0\,
      I2 => \^sreg\(0),
      I3 => \^sreg\(1),
      I4 => \^sreg\(2),
      I5 => \v_reg[3]_1\,
      O => D(0)
    );
\v[4]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => \^sreg\(2),
      I1 => \^sreg\(1),
      I2 => \^sreg\(0),
      O => \sreg_reg[2]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity edgedtctr_5 is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    \sreg_reg[2]_0\ : out STD_LOGIC;
    \sreg_reg[0]_1\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    sreg : in STD_LOGIC_VECTOR ( 2 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of edgedtctr_5 : entity is "edgedtctr";
end edgedtctr_5;

architecture STRUCTURE of edgedtctr_5 is
  signal sreg_0 : STD_LOGIC_VECTOR ( 2 downto 0 );
begin
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \sreg_reg[0]_1\,
      Q => sreg_0(0),
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => sreg_0(0),
      Q => sreg_0(1),
      R => '0'
    );
\sreg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => sreg_0(1),
      Q => sreg_0(2),
      R => '0'
    );
\v[4]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => sreg_0(2),
      I1 => sreg_0(1),
      I2 => sreg_0(0),
      O => \sreg_reg[2]_0\
    );
\v[6]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFEFEF00EFEFEFEF"
    )
        port map (
      I0 => sreg_0(0),
      I1 => sreg_0(1),
      I2 => sreg_0(2),
      I3 => sreg(0),
      I4 => sreg(1),
      I5 => sreg(2),
      O => \sreg_reg[0]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity edgedtctr_6 is
  port (
    sreg : out STD_LOGIC_VECTOR ( 2 downto 0 );
    D : out STD_LOGIC_VECTOR ( 0 to 0 );
    \sreg_reg[2]_0\ : out STD_LOGIC;
    \sreg_reg[0]_0\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    \v_reg[6]\ : in STD_LOGIC;
    \v_reg[6]_0\ : in STD_LOGIC;
    \v_reg[6]_1\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of edgedtctr_6 : entity is "edgedtctr";
end edgedtctr_6;

architecture STRUCTURE of edgedtctr_6 is
  signal \^sreg\ : STD_LOGIC_VECTOR ( 2 downto 0 );
begin
  sreg(2 downto 0) <= \^sreg\(2 downto 0);
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \sreg_reg[0]_0\,
      Q => \^sreg\(0),
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \^sreg\(0),
      Q => \^sreg\(1),
      R => '0'
    );
\sreg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \^sreg\(1),
      Q => \^sreg\(2),
      R => '0'
    );
\v[5]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => \^sreg\(2),
      I1 => \^sreg\(1),
      I2 => \^sreg\(0),
      O => \sreg_reg[2]_0\
    );
\v[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5551000000000000"
    )
        port map (
      I0 => \v_reg[6]\,
      I1 => \^sreg\(2),
      I2 => \^sreg\(1),
      I3 => \^sreg\(0),
      I4 => \v_reg[6]_0\,
      I5 => \v_reg[6]_1\,
      O => D(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity edgedtctr_7 is
  port (
    D : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \sreg_reg[2]_0\ : out STD_LOGIC;
    \sreg_reg[0]_0\ : out STD_LOGIC;
    \sreg_reg[0]_1\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    \v_reg[2]\ : in STD_LOGIC;
    \v_reg[2]_0\ : in STD_LOGIC;
    \v_reg[2]_1\ : in STD_LOGIC;
    \v_reg[2]_2\ : in STD_LOGIC;
    \v_reg[5]\ : in STD_LOGIC;
    sreg : in STD_LOGIC_VECTOR ( 2 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of edgedtctr_7 : entity is "edgedtctr";
end edgedtctr_7;

architecture STRUCTURE of edgedtctr_7 is
  signal sreg_0 : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \^sreg_reg[2]_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \v[1]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \v[2]_i_1\ : label is "soft_lutpair1";
begin
  \sreg_reg[2]_0\ <= \^sreg_reg[2]_0\;
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \sreg_reg[0]_1\,
      Q => sreg_0(0),
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => sreg_0(0),
      Q => sreg_0(1),
      R => '0'
    );
\sreg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => sreg_0(1),
      Q => sreg_0(2),
      R => '0'
    );
\v[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"28000000"
    )
        port map (
      I0 => \^sreg_reg[2]_0\,
      I1 => \v_reg[2]\,
      I2 => \v_reg[2]_1\,
      I3 => \v_reg[2]_0\,
      I4 => \v_reg[2]_2\,
      O => D(0)
    );
\v[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"48008000"
    )
        port map (
      I0 => \^sreg_reg[2]_0\,
      I1 => \v_reg[2]\,
      I2 => \v_reg[2]_0\,
      I3 => \v_reg[2]_1\,
      I4 => \v_reg[2]_2\,
      O => D(1)
    );
\v[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFEFEF00EFEFEFEF"
    )
        port map (
      I0 => sreg_0(0),
      I1 => sreg_0(1),
      I2 => sreg_0(2),
      I3 => sreg(0),
      I4 => sreg(1),
      I5 => sreg(2),
      O => \sreg_reg[0]_0\
    );
\v[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20008000"
    )
        port map (
      I0 => \^sreg_reg[2]_0\,
      I1 => \v_reg[2]\,
      I2 => \v_reg[2]_0\,
      I3 => \v_reg[2]_1\,
      I4 => \v_reg[2]_2\,
      O => D(2)
    );
\v[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0008000088808888"
    )
        port map (
      I0 => \v_reg[5]\,
      I1 => \v_reg[2]_0\,
      I2 => sreg_0(0),
      I3 => sreg_0(1),
      I4 => sreg_0(2),
      I5 => \v_reg[2]\,
      O => D(3)
    );
\v[6]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => sreg_0(2),
      I1 => sreg_0(1),
      I2 => sreg_0(0),
      O => \^sreg_reg[2]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gestion_refresco is
  port (
    \reg_reg[1]\ : out STD_LOGIC;
    \FSM_onehot_current_refresco_reg[3]_0\ : out STD_LOGIC;
    \FSM_onehot_current_refresco_reg[4]_0\ : out STD_LOGIC_VECTOR ( 4 downto 0 );
    \FSM_onehot_current_refresco_reg[1]_0\ : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \FSM_onehot_current_refresco_reg[4]_1\ : out STD_LOGIC;
    \current_state_reg[1]\ : out STD_LOGIC;
    \FSM_onehot_current_refresco_reg[1]_1\ : out STD_LOGIC;
    \FSM_onehot_current_refresco_reg[2]_0\ : out STD_LOGIC;
    \FSM_onehot_current_refresco_reg[4]_2\ : out STD_LOGIC;
    \FSM_onehot_current_refresco_reg[0]_0\ : out STD_LOGIC;
    \charact_dot_reg[0]\ : out STD_LOGIC;
    \v_reg[6]\ : out STD_LOGIC;
    \FSM_onehot_current_refresco_reg[1]_2\ : out STD_LOGIC;
    \FSM_onehot_current_refresco_reg[3]_1\ : out STD_LOGIC;
    \current_state_reg[1]_0\ : out STD_LOGIC;
    \current_state_reg[0]\ : out STD_LOGIC;
    \current_state_reg[1]_1\ : out STD_LOGIC;
    \caracter[3]_i_32_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_dinero[1]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter_reg[5]\ : in STD_LOGIC;
    \caracter_reg[6]\ : in STD_LOGIC;
    \disp_dinero[5]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter[0]_i_2\ : in STD_LOGIC;
    \caracter_reg[1]\ : in STD_LOGIC;
    \caracter_reg[1]_0\ : in STD_LOGIC;
    \caracter_reg[1]_1\ : in STD_LOGIC;
    \caracter_reg[1]_2\ : in STD_LOGIC;
    \caracter_reg[6]_0\ : in STD_LOGIC;
    \caracter_reg[6]_1\ : in STD_LOGIC;
    \caracter[0]_i_2_0\ : in STD_LOGIC;
    \caracter_reg[6]_2\ : in STD_LOGIC;
    \caracter[0]_i_2_1\ : in STD_LOGIC;
    \caracter_reg[6]_3\ : in STD_LOGIC;
    \caracter_reg[6]_4\ : in STD_LOGIC;
    \caracter[1]_i_2\ : in STD_LOGIC;
    \caracter[1]_i_2_0\ : in STD_LOGIC;
    \caracter_reg[2]\ : in STD_LOGIC;
    \caracter_reg[2]_0\ : in STD_LOGIC;
    \caracter_reg[2]_1\ : in STD_LOGIC;
    \caracter[3]_i_15\ : in STD_LOGIC;
    \next_state_reg[1]_i_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    button_ok_IBUF : in STD_LOGIC;
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter_reg[3]\ : in STD_LOGIC;
    clr_refresco_r : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    ref_option_IBUF : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
end gestion_refresco;

architecture STRUCTURE of gestion_refresco is
  signal \FSM_onehot_current_refresco[0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_refresco[1]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_refresco[2]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_refresco[3]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_refresco[4]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_refresco[4]_i_2_n_0\ : STD_LOGIC;
  signal \^fsm_onehot_current_refresco_reg[2]_0\ : STD_LOGIC;
  signal \^fsm_onehot_current_refresco_reg[4]_0\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \^fsm_onehot_current_refresco_reg[4]_1\ : STD_LOGIC;
  signal \caracter[1]_i_10_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_12_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_42_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_45_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_6_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_9_n_0\ : STD_LOGIC;
  signal \^current_state_reg[1]\ : STD_LOGIC;
  signal dot_i_9_n_0 : STD_LOGIC;
  signal next_refresco_n_0 : STD_LOGIC;
  signal \next_state_reg[1]_i_4_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_onehot_current_refresco[1]_i_1\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \FSM_onehot_current_refresco[2]_i_1\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \FSM_onehot_current_refresco[3]_i_1\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \FSM_onehot_current_refresco[4]_i_1\ : label is "soft_lutpair72";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_refresco_reg[0]\ : label is "inicio:00001,agua:00010,fanta_n:00100,nestea:01000,cocacola:10000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_refresco_reg[1]\ : label is "inicio:00001,agua:00010,fanta_n:00100,nestea:01000,cocacola:10000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_refresco_reg[2]\ : label is "inicio:00001,agua:00010,fanta_n:00100,nestea:01000,cocacola:10000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_refresco_reg[3]\ : label is "inicio:00001,agua:00010,fanta_n:00100,nestea:01000,cocacola:10000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_refresco_reg[4]\ : label is "inicio:00001,agua:00010,fanta_n:00100,nestea:01000,cocacola:10000";
  attribute SOFT_HLUTNM of \caracter[0]_i_11\ : label is "soft_lutpair71";
  attribute SOFT_HLUTNM of \caracter[2]_i_18\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \caracter[3]_i_17\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \caracter[6]_i_11\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \caracter[6]_i_2\ : label is "soft_lutpair71";
  attribute SOFT_HLUTNM of \next_state_reg[1]_i_4\ : label is "soft_lutpair73";
begin
  \FSM_onehot_current_refresco_reg[2]_0\ <= \^fsm_onehot_current_refresco_reg[2]_0\;
  \FSM_onehot_current_refresco_reg[4]_0\(4 downto 0) <= \^fsm_onehot_current_refresco_reg[4]_0\(4 downto 0);
  \FSM_onehot_current_refresco_reg[4]_1\ <= \^fsm_onehot_current_refresco_reg[4]_1\;
  \current_state_reg[1]\ <= \^current_state_reg[1]\;
\FSM_onehot_current_refresco[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEE8"
    )
        port map (
      I0 => ref_option_IBUF(2),
      I1 => ref_option_IBUF(3),
      I2 => ref_option_IBUF(1),
      I3 => ref_option_IBUF(0),
      O => \FSM_onehot_current_refresco[0]_i_1_n_0\
    );
\FSM_onehot_current_refresco[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => ref_option_IBUF(2),
      I1 => ref_option_IBUF(1),
      I2 => ref_option_IBUF(3),
      O => \FSM_onehot_current_refresco[1]_i_1_n_0\
    );
\FSM_onehot_current_refresco[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => ref_option_IBUF(2),
      I1 => ref_option_IBUF(0),
      I2 => ref_option_IBUF(3),
      O => \FSM_onehot_current_refresco[2]_i_1_n_0\
    );
\FSM_onehot_current_refresco[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0010"
    )
        port map (
      I0 => ref_option_IBUF(3),
      I1 => ref_option_IBUF(0),
      I2 => ref_option_IBUF(2),
      I3 => ref_option_IBUF(1),
      O => \FSM_onehot_current_refresco[3]_i_1_n_0\
    );
\FSM_onehot_current_refresco[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0010"
    )
        port map (
      I0 => ref_option_IBUF(0),
      I1 => ref_option_IBUF(2),
      I2 => ref_option_IBUF(3),
      I3 => ref_option_IBUF(1),
      O => \FSM_onehot_current_refresco[4]_i_1_n_0\
    );
\FSM_onehot_current_refresco[4]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EF"
    )
        port map (
      I0 => Q(1),
      I1 => Q(0),
      I2 => clr_refresco_r,
      O => \FSM_onehot_current_refresco[4]_i_2_n_0\
    );
\FSM_onehot_current_refresco_reg[0]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_BUFG,
      CE => next_refresco_n_0,
      D => \FSM_onehot_current_refresco[0]_i_1_n_0\,
      PRE => \FSM_onehot_current_refresco[4]_i_2_n_0\,
      Q => \^fsm_onehot_current_refresco_reg[4]_0\(0)
    );
\FSM_onehot_current_refresco_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => next_refresco_n_0,
      CLR => \FSM_onehot_current_refresco[4]_i_2_n_0\,
      D => \FSM_onehot_current_refresco[1]_i_1_n_0\,
      Q => \^fsm_onehot_current_refresco_reg[4]_0\(1)
    );
\FSM_onehot_current_refresco_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => next_refresco_n_0,
      CLR => \FSM_onehot_current_refresco[4]_i_2_n_0\,
      D => \FSM_onehot_current_refresco[2]_i_1_n_0\,
      Q => \^fsm_onehot_current_refresco_reg[4]_0\(2)
    );
\FSM_onehot_current_refresco_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => next_refresco_n_0,
      CLR => \FSM_onehot_current_refresco[4]_i_2_n_0\,
      D => \FSM_onehot_current_refresco[3]_i_1_n_0\,
      Q => \^fsm_onehot_current_refresco_reg[4]_0\(3)
    );
\FSM_onehot_current_refresco_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => next_refresco_n_0,
      CLR => \FSM_onehot_current_refresco[4]_i_2_n_0\,
      D => \FSM_onehot_current_refresco[4]_i_1_n_0\,
      Q => \^fsm_onehot_current_refresco_reg[4]_0\(4)
    );
\caracter[0]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"020F020202020202"
    )
        port map (
      I0 => \^fsm_onehot_current_refresco_reg[4]_0\(3),
      I1 => \caracter_reg[5]\,
      I2 => \caracter_reg[6]\,
      I3 => Q(0),
      I4 => \disp_dinero[5]_1\(0),
      I5 => \caracter[0]_i_2\,
      O => \FSM_onehot_current_refresco_reg[3]_0\
    );
\caracter[0]_i_11\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000100"
    )
        port map (
      I0 => CO(0),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \^fsm_onehot_current_refresco_reg[4]_0\(4),
      I4 => \caracter_reg[6]\,
      O => \current_state_reg[1]_0\
    );
\caracter[0]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000F0E000"
    )
        port map (
      I0 => \^fsm_onehot_current_refresco_reg[4]_0\(1),
      I1 => \^fsm_onehot_current_refresco_reg[4]_0\(0),
      I2 => \caracter[0]_i_2_0\,
      I3 => \caracter_reg[6]_2\,
      I4 => Q(0),
      I5 => \caracter[0]_i_2_1\,
      O => \FSM_onehot_current_refresco_reg[1]_1\
    );
\caracter[1]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00CF000000800000"
    )
        port map (
      I0 => \^fsm_onehot_current_refresco_reg[4]_0\(3),
      I1 => \caracter_reg[6]_3\,
      I2 => \caracter[1]_i_2_0\,
      I3 => \caracter_reg[5]\,
      I4 => \caracter_reg[6]_2\,
      I5 => \^fsm_onehot_current_refresco_reg[4]_0\(4),
      O => \caracter[1]_i_10_n_0\
    );
\caracter[1]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFAAEAAAEAAAEAAA"
    )
        port map (
      I0 => \caracter[1]_i_10_n_0\,
      I1 => \caracter_reg[1]\,
      I2 => \caracter_reg[1]_0\,
      I3 => \caracter_reg[1]_1\,
      I4 => \^fsm_onehot_current_refresco_reg[4]_0\(1),
      I5 => \caracter_reg[1]_2\,
      O => \FSM_onehot_current_refresco_reg[1]_0\
    );
\caracter[1]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"080800000F080000"
    )
        port map (
      I0 => \^fsm_onehot_current_refresco_reg[4]_0\(4),
      I1 => \caracter[1]_i_2\,
      I2 => \caracter_reg[5]\,
      I3 => \^fsm_onehot_current_refresco_reg[4]_0\(2),
      I4 => \caracter_reg[6]_3\,
      I5 => \caracter[1]_i_2_0\,
      O => \FSM_onehot_current_refresco_reg[4]_2\
    );
\caracter[2]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4100100041000000"
    )
        port map (
      I0 => \caracter_reg[5]\,
      I1 => \caracter[3]_i_15\,
      I2 => \caracter[3]_i_32_0\(0),
      I3 => \^fsm_onehot_current_refresco_reg[4]_0\(2),
      I4 => \caracter_reg[6]_3\,
      I5 => \next_state_reg[1]_i_4_n_0\,
      O => \caracter[2]_i_12_n_0\
    );
\caracter[2]_i_18\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^fsm_onehot_current_refresco_reg[4]_0\(1),
      I1 => \^fsm_onehot_current_refresco_reg[4]_0\(4),
      O => \FSM_onehot_current_refresco_reg[1]_2\
    );
\caracter[2]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFF8"
    )
        port map (
      I0 => \^current_state_reg[1]\,
      I1 => \^fsm_onehot_current_refresco_reg[4]_0\(0),
      I2 => \caracter_reg[2]\,
      I3 => \caracter[2]_i_12_n_0\,
      I4 => \caracter_reg[2]_0\,
      I5 => \caracter_reg[2]_1\,
      O => \FSM_onehot_current_refresco_reg[0]_0\
    );
\caracter[3]_i_17\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"01000000"
    )
        port map (
      I0 => CO(0),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \caracter_reg[6]_2\,
      I4 => \^fsm_onehot_current_refresco_reg[4]_0\(3),
      O => \current_state_reg[1]_1\
    );
\caracter[3]_i_32\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAABAEAAAA"
    )
        port map (
      I0 => \caracter[3]_i_42_n_0\,
      I1 => \caracter[3]_i_15\,
      I2 => \caracter[3]_i_32_0\(1),
      I3 => \caracter[3]_i_32_0\(0),
      I4 => \^fsm_onehot_current_refresco_reg[4]_0\(4),
      I5 => \caracter_reg[5]\,
      O => \charact_dot_reg[0]\
    );
\caracter[3]_i_42\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008002"
    )
        port map (
      I0 => \caracter[3]_i_45_n_0\,
      I1 => \caracter[3]_i_32_0\(2),
      I2 => \caracter[3]_i_32_0\(0),
      I3 => \caracter[3]_i_32_0\(1),
      I4 => \^fsm_onehot_current_refresco_reg[4]_0\(0),
      I5 => \^fsm_onehot_current_refresco_reg[4]_0\(4),
      O => \caracter[3]_i_42_n_0\
    );
\caracter[3]_i_45\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000020"
    )
        port map (
      I0 => \^fsm_onehot_current_refresco_reg[4]_0\(2),
      I1 => \^fsm_onehot_current_refresco_reg[4]_0\(1),
      I2 => \caracter[3]_i_32_0\(0),
      I3 => \^fsm_onehot_current_refresco_reg[4]_0\(3),
      I4 => Q(1),
      I5 => Q(0),
      O => \caracter[3]_i_45_n_0\
    );
\caracter[3]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"88F88888"
    )
        port map (
      I0 => \caracter_reg[3]\,
      I1 => \^current_state_reg[1]\,
      I2 => Q(0),
      I3 => CO(0),
      I4 => Q(1),
      O => \current_state_reg[0]\
    );
\caracter[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \caracter_reg[5]\,
      I1 => \^fsm_onehot_current_refresco_reg[4]_0\(0),
      I2 => \^fsm_onehot_current_refresco_reg[4]_0\(2),
      I3 => \^fsm_onehot_current_refresco_reg[4]_0\(4),
      I4 => \^fsm_onehot_current_refresco_reg[4]_0\(1),
      I5 => \^fsm_onehot_current_refresco_reg[4]_0\(3),
      O => D(0)
    );
\caracter[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFAE"
    )
        port map (
      I0 => \^fsm_onehot_current_refresco_reg[4]_1\,
      I1 => \caracter_reg[6]_0\,
      I2 => \caracter_reg[6]\,
      I3 => \^current_state_reg[1]\,
      I4 => \caracter[6]_i_6_n_0\,
      I5 => \caracter_reg[6]_1\,
      O => D(1)
    );
\caracter[6]_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^fsm_onehot_current_refresco_reg[4]_0\(3),
      I1 => Q(0),
      O => \FSM_onehot_current_refresco_reg[3]_1\
    );
\caracter[6]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0002"
    )
        port map (
      I0 => \^fsm_onehot_current_refresco_reg[4]_0\(4),
      I1 => Q(0),
      I2 => Q(1),
      I3 => CO(0),
      O => \^fsm_onehot_current_refresco_reg[4]_1\
    );
\caracter[6]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"01000000"
    )
        port map (
      I0 => CO(0),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \caracter_reg[6]_2\,
      I4 => \^fsm_onehot_current_refresco_reg[4]_0\(2),
      O => \^current_state_reg[1]\
    );
\caracter[6]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFF0001"
    )
        port map (
      I0 => Q(1),
      I1 => \caracter_reg[6]_3\,
      I2 => Q(0),
      I3 => \^fsm_onehot_current_refresco_reg[2]_0\,
      I4 => \caracter[6]_i_9_n_0\,
      I5 => \caracter_reg[6]_4\,
      O => \caracter[6]_i_6_n_0\
    );
\caracter[6]_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"11100000"
    )
        port map (
      I0 => Q(1),
      I1 => CO(0),
      I2 => \^fsm_onehot_current_refresco_reg[4]_0\(3),
      I3 => Q(0),
      I4 => \caracter[0]_i_2_0\,
      O => \caracter[6]_i_9_n_0\
    );
dot_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAAAEAA"
    )
        port map (
      I0 => dot_i_9_n_0,
      I1 => \caracter[3]_i_32_0\(1),
      I2 => \caracter[3]_i_32_0\(0),
      I3 => Q(1),
      I4 => \disp_dinero[1]_0\(0),
      O => \reg_reg[1]\
    );
dot_i_6: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFF7F"
    )
        port map (
      I0 => \^fsm_onehot_current_refresco_reg[4]_0\(2),
      I1 => \caracter[3]_i_32_0\(0),
      I2 => \caracter[3]_i_32_0\(1),
      I3 => \^fsm_onehot_current_refresco_reg[4]_0\(0),
      I4 => \next_state_reg[1]_i_4_n_0\,
      O => \^fsm_onehot_current_refresco_reg[2]_0\
    );
dot_i_9: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000002000000"
    )
        port map (
      I0 => \^fsm_onehot_current_refresco_reg[4]_0\(2),
      I1 => \^fsm_onehot_current_refresco_reg[4]_0\(0),
      I2 => Q(1),
      I3 => \caracter[3]_i_32_0\(1),
      I4 => \caracter[3]_i_32_0\(0),
      I5 => \next_state_reg[1]_i_4_n_0\,
      O => dot_i_9_n_0
    );
next_refresco: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => ref_option_IBUF(3),
      I1 => ref_option_IBUF(2),
      I2 => ref_option_IBUF(0),
      I3 => ref_option_IBUF(1),
      O => next_refresco_n_0
    );
\next_state_reg[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00AA00FC00AA0000"
    )
        port map (
      I0 => \next_state_reg[1]_i_1\(0),
      I1 => \next_state_reg[1]_i_4_n_0\,
      I2 => \^fsm_onehot_current_refresco_reg[4]_0\(2),
      I3 => Q(1),
      I4 => Q(0),
      I5 => button_ok_IBUF,
      O => \v_reg[6]\
    );
\next_state_reg[1]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \^fsm_onehot_current_refresco_reg[4]_0\(4),
      I1 => \^fsm_onehot_current_refresco_reg[4]_0\(1),
      I2 => \^fsm_onehot_current_refresco_reg[4]_0\(3),
      O => \next_state_reg[1]_i_4_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity int_to_string is
  port (
    \total_reg[0]\ : out STD_LOGIC;
    \charact_dot_reg[0]\ : out STD_LOGIC;
    \total_reg[29]\ : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 0 to 0 );
    \current_state_reg[1]\ : out STD_LOGIC;
    \charact_dot_reg[0]_0\ : out STD_LOGIC;
    \current_state_reg[1]_0\ : out STD_LOGIC;
    \total_reg[20]\ : out STD_LOGIC;
    \disp_dinero[5]_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \total_reg[29]_0\ : out STD_LOGIC;
    \total_reg[26]\ : out STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 30 downto 0 );
    \caracter_reg[2]\ : in STD_LOGIC;
    \caracter_reg[2]_0\ : in STD_LOGIC;
    \caracter_reg[2]_1\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \caracter_reg[3]\ : in STD_LOGIC;
    \caracter_reg[3]_0\ : in STD_LOGIC;
    \caracter_reg[3]_1\ : in STD_LOGIC;
    \caracter_reg[3]_2\ : in STD_LOGIC;
    \caracter_reg[0]\ : in STD_LOGIC;
    \caracter_reg[0]_0\ : in STD_LOGIC;
    \caracter_reg[0]_1\ : in STD_LOGIC;
    \caracter_reg[3]_3\ : in STD_LOGIC;
    \caracter_reg[3]_4\ : in STD_LOGIC;
    \caracter_reg[3]_5\ : in STD_LOGIC
  );
end int_to_string;

architecture STRUCTURE of int_to_string is
  signal \caracter[0]_i_12_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_15_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_9_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_10_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_11_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_12_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_13_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_14_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_19_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_20_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_21_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_23_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_24_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_25_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_26_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_27_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_28_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_29_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_2_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_30_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_31_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_34_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_35_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_36_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_37_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_38_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_39_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_40_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_41_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_43_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_44_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_4_n_0\ : STD_LOGIC;
  signal cent : STD_LOGIC_VECTOR ( 6 downto 1 );
  signal \disp_dinero[2]_1\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \disp_dinero[3]_0\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \euros1__169_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__0_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__0_n_4\ : STD_LOGIC;
  signal \euros1__169_carry__0_n_5\ : STD_LOGIC;
  signal \euros1__169_carry__0_n_6\ : STD_LOGIC;
  signal \euros1__169_carry__0_n_7\ : STD_LOGIC;
  signal \euros1__169_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__1_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__1_n_4\ : STD_LOGIC;
  signal \euros1__169_carry__1_n_5\ : STD_LOGIC;
  signal \euros1__169_carry__1_n_6\ : STD_LOGIC;
  signal \euros1__169_carry__1_n_7\ : STD_LOGIC;
  signal \euros1__169_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__2_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__2_n_4\ : STD_LOGIC;
  signal \euros1__169_carry__2_n_5\ : STD_LOGIC;
  signal \euros1__169_carry__2_n_6\ : STD_LOGIC;
  signal \euros1__169_carry__2_n_7\ : STD_LOGIC;
  signal \euros1__169_carry__3_i_1_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__3_i_2_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__3_i_3_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__3_i_4_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__3_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__3_n_4\ : STD_LOGIC;
  signal \euros1__169_carry__3_n_5\ : STD_LOGIC;
  signal \euros1__169_carry__3_n_6\ : STD_LOGIC;
  signal \euros1__169_carry__3_n_7\ : STD_LOGIC;
  signal \euros1__169_carry__4_i_1_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__4_i_2_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__4_n_6\ : STD_LOGIC;
  signal \euros1__169_carry__4_n_7\ : STD_LOGIC;
  signal \euros1__169_carry_i_1_n_0\ : STD_LOGIC;
  signal \euros1__169_carry_i_2_n_0\ : STD_LOGIC;
  signal \euros1__169_carry_i_3_n_0\ : STD_LOGIC;
  signal \euros1__169_carry_n_0\ : STD_LOGIC;
  signal \euros1__169_carry_n_4\ : STD_LOGIC;
  signal \euros1__169_carry_n_5\ : STD_LOGIC;
  signal \euros1__169_carry_n_6\ : STD_LOGIC;
  signal \euros1__1_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__0_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__0_n_4\ : STD_LOGIC;
  signal \euros1__1_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__1_i_7_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__1_i_8_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__1_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__1_n_4\ : STD_LOGIC;
  signal \euros1__1_carry__1_n_5\ : STD_LOGIC;
  signal \euros1__1_carry__1_n_6\ : STD_LOGIC;
  signal \euros1__1_carry__1_n_7\ : STD_LOGIC;
  signal \euros1__1_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__2_i_5_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__2_i_6_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__2_i_7_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__2_i_8_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__2_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__2_n_4\ : STD_LOGIC;
  signal \euros1__1_carry__2_n_5\ : STD_LOGIC;
  signal \euros1__1_carry__2_n_6\ : STD_LOGIC;
  signal \euros1__1_carry__2_n_7\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_1_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_2_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_3_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_4_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_5_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_6_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_7_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_8_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__3_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__3_n_4\ : STD_LOGIC;
  signal \euros1__1_carry__3_n_5\ : STD_LOGIC;
  signal \euros1__1_carry__3_n_6\ : STD_LOGIC;
  signal \euros1__1_carry__3_n_7\ : STD_LOGIC;
  signal \euros1__1_carry__4_i_1_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__4_i_2_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__4_i_3_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__4_i_4_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__4_i_5_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__4_i_6_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__4_i_7_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__4_i_8_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__4_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__4_n_4\ : STD_LOGIC;
  signal \euros1__1_carry__4_n_5\ : STD_LOGIC;
  signal \euros1__1_carry__4_n_6\ : STD_LOGIC;
  signal \euros1__1_carry__4_n_7\ : STD_LOGIC;
  signal \euros1__1_carry__5_i_1_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__5_i_2_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__5_i_3_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__5_i_4_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__5_i_5_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__5_i_6_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__5_i_7_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__5_i_8_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__5_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__5_n_4\ : STD_LOGIC;
  signal \euros1__1_carry__5_n_5\ : STD_LOGIC;
  signal \euros1__1_carry__5_n_6\ : STD_LOGIC;
  signal \euros1__1_carry__5_n_7\ : STD_LOGIC;
  signal \euros1__1_carry__6_i_1_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__6_i_2_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__6_i_3_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__6_i_4_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__6_i_5_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__6_i_6_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__6_i_7_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__6_i_8_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__6_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__6_n_4\ : STD_LOGIC;
  signal \euros1__1_carry__6_n_5\ : STD_LOGIC;
  signal \euros1__1_carry__6_n_6\ : STD_LOGIC;
  signal \euros1__1_carry__6_n_7\ : STD_LOGIC;
  signal \euros1__1_carry__7_i_1_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__7_n_6\ : STD_LOGIC;
  signal \euros1__1_carry__7_n_7\ : STD_LOGIC;
  signal \euros1__1_carry_i_1_n_0\ : STD_LOGIC;
  signal \euros1__1_carry_i_2_n_0\ : STD_LOGIC;
  signal \euros1__1_carry_i_3_n_0\ : STD_LOGIC;
  signal \euros1__1_carry_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__0_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__0_n_4\ : STD_LOGIC;
  signal \euros1__231_carry__0_n_5\ : STD_LOGIC;
  signal \euros1__231_carry__0_n_6\ : STD_LOGIC;
  signal \euros1__231_carry__0_n_7\ : STD_LOGIC;
  signal \euros1__231_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__1_i_7_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__1_i_8_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__1_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__1_n_4\ : STD_LOGIC;
  signal \euros1__231_carry__1_n_5\ : STD_LOGIC;
  signal \euros1__231_carry__1_n_6\ : STD_LOGIC;
  signal \euros1__231_carry__1_n_7\ : STD_LOGIC;
  signal \euros1__231_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__2_i_5_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__2_i_6_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__2_i_7_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__2_n_4\ : STD_LOGIC;
  signal \euros1__231_carry__2_n_5\ : STD_LOGIC;
  signal \euros1__231_carry__2_n_6\ : STD_LOGIC;
  signal \euros1__231_carry__2_n_7\ : STD_LOGIC;
  signal \euros1__231_carry_i_1_n_0\ : STD_LOGIC;
  signal \euros1__231_carry_i_2_n_0\ : STD_LOGIC;
  signal \euros1__231_carry_i_3_n_0\ : STD_LOGIC;
  signal \euros1__231_carry_n_0\ : STD_LOGIC;
  signal \euros1__231_carry_n_4\ : STD_LOGIC;
  signal \euros1__231_carry_n_5\ : STD_LOGIC;
  signal \euros1__231_carry_n_6\ : STD_LOGIC;
  signal \euros1__231_carry_n_7\ : STD_LOGIC;
  signal \euros1__276_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__0_i_9_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__0_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__1_i_10_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__1_i_11_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__1_i_12_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__1_i_13_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__1_i_14_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__1_i_15_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__1_i_7_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__1_i_8_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__1_i_9_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__1_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__2_i_10_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__2_i_11_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__2_i_12_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__2_i_13_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__2_i_14_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__2_i_5_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__2_i_6_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__2_i_7_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__2_i_8_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__2_i_9_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__2_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_i_10_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_i_11_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_i_12_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_i_13_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_i_14_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_i_15_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_i_16_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_i_1_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_i_2_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_i_3_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_i_4_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_i_5_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_i_6_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_i_7_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_i_8_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_i_9_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__4_i_10_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__4_i_11_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__4_i_12_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__4_i_13_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__4_i_14_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__4_i_1_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__4_i_2_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__4_i_3_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__4_i_4_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__4_i_5_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__4_i_6_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__4_i_7_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__4_i_8_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__4_i_9_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__4_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__4_n_4\ : STD_LOGIC;
  signal \euros1__276_carry__4_n_5\ : STD_LOGIC;
  signal \euros1__276_carry__4_n_6\ : STD_LOGIC;
  signal \euros1__276_carry__4_n_7\ : STD_LOGIC;
  signal \euros1__276_carry__5_i_1_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__5_i_2_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__5_i_3_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__5_i_4_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__5_i_5_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__5_i_6_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__5_i_7_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__5_n_6\ : STD_LOGIC;
  signal \euros1__276_carry__5_n_7\ : STD_LOGIC;
  signal \euros1__276_carry_i_1_n_0\ : STD_LOGIC;
  signal \euros1__276_carry_i_2_n_0\ : STD_LOGIC;
  signal \euros1__276_carry_i_3_n_0\ : STD_LOGIC;
  signal \euros1__276_carry_i_4_n_0\ : STD_LOGIC;
  signal \euros1__276_carry_i_5_n_0\ : STD_LOGIC;
  signal \euros1__276_carry_i_6_n_0\ : STD_LOGIC;
  signal \euros1__276_carry_i_7_n_0\ : STD_LOGIC;
  signal \euros1__276_carry_i_8_n_0\ : STD_LOGIC;
  signal \euros1__276_carry_n_0\ : STD_LOGIC;
  signal \euros1__333_carry_i_1_n_0\ : STD_LOGIC;
  signal \euros1__333_carry_i_2_n_0\ : STD_LOGIC;
  signal \euros1__333_carry_i_3_n_0\ : STD_LOGIC;
  signal \euros1__333_carry_i_4_n_0\ : STD_LOGIC;
  signal \euros1__333_carry_n_5\ : STD_LOGIC;
  signal \euros1__333_carry_n_6\ : STD_LOGIC;
  signal \euros1__333_carry_n_7\ : STD_LOGIC;
  signal \euros1__339_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \euros1__339_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \euros1__339_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \euros1__339_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \euros1__339_carry__0_n_4\ : STD_LOGIC;
  signal \euros1__339_carry__0_n_5\ : STD_LOGIC;
  signal \euros1__339_carry__0_n_6\ : STD_LOGIC;
  signal \euros1__339_carry__0_n_7\ : STD_LOGIC;
  signal \euros1__339_carry_i_1_n_0\ : STD_LOGIC;
  signal \euros1__339_carry_i_2_n_0\ : STD_LOGIC;
  signal \euros1__339_carry_i_3_n_0\ : STD_LOGIC;
  signal \euros1__339_carry_i_4_n_0\ : STD_LOGIC;
  signal \euros1__339_carry_n_0\ : STD_LOGIC;
  signal \euros1__339_carry_n_4\ : STD_LOGIC;
  signal \euros1__339_carry_n_5\ : STD_LOGIC;
  signal \euros1__92_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__0_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__0_n_4\ : STD_LOGIC;
  signal \euros1__92_carry__0_n_5\ : STD_LOGIC;
  signal \euros1__92_carry__0_n_6\ : STD_LOGIC;
  signal \euros1__92_carry__0_n_7\ : STD_LOGIC;
  signal \euros1__92_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__1_i_7_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__1_i_8_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__1_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__1_n_4\ : STD_LOGIC;
  signal \euros1__92_carry__1_n_5\ : STD_LOGIC;
  signal \euros1__92_carry__1_n_6\ : STD_LOGIC;
  signal \euros1__92_carry__1_n_7\ : STD_LOGIC;
  signal \euros1__92_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__2_i_5_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__2_i_6_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__2_i_7_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__2_i_8_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__2_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__2_n_4\ : STD_LOGIC;
  signal \euros1__92_carry__2_n_5\ : STD_LOGIC;
  signal \euros1__92_carry__2_n_6\ : STD_LOGIC;
  signal \euros1__92_carry__2_n_7\ : STD_LOGIC;
  signal \euros1__92_carry__3_i_1_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__3_i_2_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__3_i_3_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__3_i_4_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__3_i_5_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__3_i_6_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__3_i_7_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__3_i_8_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__3_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__3_n_4\ : STD_LOGIC;
  signal \euros1__92_carry__3_n_5\ : STD_LOGIC;
  signal \euros1__92_carry__3_n_6\ : STD_LOGIC;
  signal \euros1__92_carry__3_n_7\ : STD_LOGIC;
  signal \euros1__92_carry__4_i_1_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__4_i_2_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__4_i_3_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__4_i_4_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__4_i_5_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__4_i_6_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__4_i_7_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__4_i_8_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__4_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__4_n_4\ : STD_LOGIC;
  signal \euros1__92_carry__4_n_5\ : STD_LOGIC;
  signal \euros1__92_carry__4_n_6\ : STD_LOGIC;
  signal \euros1__92_carry__4_n_7\ : STD_LOGIC;
  signal \euros1__92_carry__5_i_1_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__5_i_2_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__5_i_3_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__5_i_4_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__5_i_5_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__5_n_5\ : STD_LOGIC;
  signal \euros1__92_carry__5_n_6\ : STD_LOGIC;
  signal \euros1__92_carry__5_n_7\ : STD_LOGIC;
  signal \euros1__92_carry_i_1_n_0\ : STD_LOGIC;
  signal \euros1__92_carry_i_2_n_0\ : STD_LOGIC;
  signal \euros1__92_carry_i_3_n_0\ : STD_LOGIC;
  signal \euros1__92_carry_i_4_n_0\ : STD_LOGIC;
  signal \euros1__92_carry_n_0\ : STD_LOGIC;
  signal \euros1__92_carry_n_4\ : STD_LOGIC;
  signal \euros1__92_carry_n_5\ : STD_LOGIC;
  signal \euros1__92_carry_n_6\ : STD_LOGIC;
  signal \euros1__92_carry_n_7\ : STD_LOGIC;
  signal \i___0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \i___0_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \i___0_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \i___0_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \i___0_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \i___0_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \i___0_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \i___0_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \i___0_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \i___0_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \i___0_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \i___0_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \i___0_carry_i_2_n_0\ : STD_LOGIC;
  signal \i___0_carry_i_3_n_0\ : STD_LOGIC;
  signal \i___0_carry_i_4_n_0\ : STD_LOGIC;
  signal \i___0_carry_i_5_n_0\ : STD_LOGIC;
  signal \i___0_carry_i_6_n_0\ : STD_LOGIC;
  signal \i___106_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \i___106_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \i___106_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \i___106_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \i___106_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \i___106_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \i___106_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \i___106_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \i___106_carry__0_i_9_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_10_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_11_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_12_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_7_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_8_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_9_n_0\ : STD_LOGIC;
  signal \i___106_carry__2_i_10_n_0\ : STD_LOGIC;
  signal \i___106_carry__2_i_11_n_0\ : STD_LOGIC;
  signal \i___106_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \i___106_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \i___106_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \i___106_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \i___106_carry__2_i_5_n_0\ : STD_LOGIC;
  signal \i___106_carry__2_i_6_n_0\ : STD_LOGIC;
  signal \i___106_carry__2_i_7_n_0\ : STD_LOGIC;
  signal \i___106_carry__2_i_8_n_0\ : STD_LOGIC;
  signal \i___106_carry__2_i_9_n_0\ : STD_LOGIC;
  signal \i___106_carry__3_i_1_n_0\ : STD_LOGIC;
  signal \i___106_carry__3_i_2_n_0\ : STD_LOGIC;
  signal \i___106_carry__3_i_3_n_0\ : STD_LOGIC;
  signal \i___106_carry__3_i_4_n_0\ : STD_LOGIC;
  signal \i___106_carry__3_i_5_n_0\ : STD_LOGIC;
  signal \i___106_carry__3_i_6_n_0\ : STD_LOGIC;
  signal \i___106_carry__3_i_7_n_0\ : STD_LOGIC;
  signal \i___106_carry__3_i_8_n_0\ : STD_LOGIC;
  signal \i___106_carry__4_i_1_n_0\ : STD_LOGIC;
  signal \i___106_carry__4_i_2_n_0\ : STD_LOGIC;
  signal \i___106_carry__4_i_3_n_0\ : STD_LOGIC;
  signal \i___106_carry__4_i_4_n_0\ : STD_LOGIC;
  signal \i___106_carry__4_i_5_n_0\ : STD_LOGIC;
  signal \i___106_carry__4_i_6_n_0\ : STD_LOGIC;
  signal \i___106_carry__4_i_7_n_0\ : STD_LOGIC;
  signal \i___106_carry__5_i_1_n_0\ : STD_LOGIC;
  signal \i___106_carry__5_i_2_n_0\ : STD_LOGIC;
  signal \i___106_carry_i_1_n_0\ : STD_LOGIC;
  signal \i___106_carry_i_2_n_0\ : STD_LOGIC;
  signal \i___106_carry_i_3_n_0\ : STD_LOGIC;
  signal \i___106_carry_i_4_n_0\ : STD_LOGIC;
  signal \i___106_carry_i_5_n_0\ : STD_LOGIC;
  signal \i___106_carry_i_6_n_0\ : STD_LOGIC;
  signal \i___106_carry_i_7_n_0\ : STD_LOGIC;
  signal \i___106_carry_i_8_n_0\ : STD_LOGIC;
  signal \i___163_carry_i_1_n_0\ : STD_LOGIC;
  signal \i___163_carry_i_2_n_0\ : STD_LOGIC;
  signal \i___163_carry_i_3_n_0\ : STD_LOGIC;
  signal \i___163_carry_i_4_n_0\ : STD_LOGIC;
  signal \i___169_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \i___169_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \i___169_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \i___169_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \i___169_carry_i_2_n_0\ : STD_LOGIC;
  signal \i___169_carry_i_3_n_0\ : STD_LOGIC;
  signal \i___169_carry_i_4_n_0\ : STD_LOGIC;
  signal \i___169_carry_i_5_n_0\ : STD_LOGIC;
  signal \i___169_carry_i_6_n_0\ : STD_LOGIC;
  signal \i___27_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \i___27_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \i___27_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \i___27_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \i___27_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \i___27_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \i___27_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \i___27_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \i___27_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \i___27_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \i___27_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \i___27_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \i___27_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \i___27_carry_i_1_n_0\ : STD_LOGIC;
  signal \i___27_carry_i_2_n_0\ : STD_LOGIC;
  signal \i___27_carry_i_3_n_0\ : STD_LOGIC;
  signal \i___27_carry_i_5_n_0\ : STD_LOGIC;
  signal \i___56_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \i___56_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \i___56_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \i___56_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \i___56_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \i___56_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \i___56_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \i___56_carry_i_1_n_0\ : STD_LOGIC;
  signal \i___56_carry_i_2_n_0\ : STD_LOGIC;
  signal \i___56_carry_i_4_n_0\ : STD_LOGIC;
  signal \i___80_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \i___80_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \i___80_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \i___80_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \i___80_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \i___80_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \i___80_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \i___80_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \i___80_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \i___80_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \i___80_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \i___80_carry_i_1_n_0\ : STD_LOGIC;
  signal \i___80_carry_i_2_n_0\ : STD_LOGIC;
  signal \i___80_carry_i_3_n_0\ : STD_LOGIC;
  signal \i___80_carry_i_4_n_0\ : STD_LOGIC;
  signal p_1_in : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \string_cent_decenas[1]5\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \string_cent_decenas[1]5__100_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry__0_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry__0_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry__0_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry__0_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry__1_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry_i_8_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__1_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__1_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__1_i_9_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__2_i_10_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__2_i_11_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__2_i_12_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__2_i_13_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__2_i_14_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__2_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__2_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__2_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__2_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__2_i_9_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__3_i_10_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__3_i_11_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__3_i_12_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__3_i_13_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__3_i_14_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__3_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__3_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__3_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__3_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__3_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__3_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__3_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__3_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__3_i_9_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__4_i_10_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__4_i_11_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__4_i_12_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__4_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__4_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__4_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__4_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__4_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__4_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__4_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__4_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__4_i_9_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__4_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__5_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__5_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__5_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__5_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__5_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__5_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__5_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__5_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__5_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__180_carry_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__180_carry_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__180_carry_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__180_carry_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__180_carry_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__186_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__186_carry__0_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__186_carry_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__186_carry_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__186_carry_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__186_carry_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__186_carry_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__186_carry_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__186_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__186_carry_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__186_carry_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__186_carry_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__1_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__1_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__1_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__0_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__0_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__0_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__0_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__1_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__1_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__1_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__1_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__1_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__0_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__0_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__0_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__0_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__1_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__1_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__1_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_11_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_15_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_16_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_17_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_18_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_19_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_9_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__2_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__2_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__5_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__5_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___163_carry_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___163_carry_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___163_carry_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___169_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___169_carry_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___169_carry_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry_n_7\ : STD_LOGIC;
  signal \^total_reg[0]\ : STD_LOGIC;
  signal \^total_reg[20]\ : STD_LOGIC;
  signal \^total_reg[26]\ : STD_LOGIC;
  signal \^total_reg[29]\ : STD_LOGIC;
  signal \^total_reg[29]_0\ : STD_LOGIC;
  signal \NLW_euros1__169_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_euros1__169_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_euros1__169_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_euros1__169_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_euros1__169_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_euros1__169_carry__3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_euros1__169_carry__4_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_euros1__169_carry__4_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_euros1__1_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_euros1__1_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_euros1__1_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_euros1__1_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_euros1__1_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_euros1__1_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_euros1__1_carry__3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_euros1__1_carry__4_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_euros1__1_carry__5_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_euros1__1_carry__6_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_euros1__1_carry__7_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_euros1__1_carry__7_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_euros1__231_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_euros1__231_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_euros1__231_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_euros1__231_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_euros1__276_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_euros1__276_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_euros1__276_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_euros1__276_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_euros1__276_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_euros1__276_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_euros1__276_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_euros1__276_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_euros1__276_carry__3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_euros1__276_carry__3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_euros1__276_carry__4_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_euros1__276_carry__5_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_euros1__276_carry__5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_euros1__333_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_euros1__333_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_euros1__339_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_euros1__339_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_euros1__92_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_euros1__92_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_euros1__92_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_euros1__92_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_euros1__92_carry__3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_euros1__92_carry__4_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_euros1__92_carry__5_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_euros1__92_carry__5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_string_cent_decenas[1]5__100_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5__100_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5__100_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5__100_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_string_cent_decenas[1]5__100_carry_i_8_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_string_cent_decenas[1]5__100_carry_i_8_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5__124_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5__124_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5__124_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5__124_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5__124_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5__124_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5__124_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5__124_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5__124_carry__3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5__124_carry__3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5__124_carry__3_i_11_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_string_cent_decenas[1]5__124_carry__3_i_11_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5__124_carry__4_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5__124_carry__4_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5__124_carry__5_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5__124_carry__5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_string_cent_decenas[1]5__180_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5__180_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_string_cent_decenas[1]5__186_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5__186_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5__186_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_string_cent_decenas[1]5__27_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5__27_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5__27_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5__27_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_string_cent_decenas[1]5__55_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5__55_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_string_cent_decenas[1]5__55_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5__55_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5__79_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5__79_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_string_cent_decenas[1]5__79_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5__79_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5__79_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_string_cent_decenas[1]5_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_string_cent_decenas[1]5_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_string_cent_decenas[1]5_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___0_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___0_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__4_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__5_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___163_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___163_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___169_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___169_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___169_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___27_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___27_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___27_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___27_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___27_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___56_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___56_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___56_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___56_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___56_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___80_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___80_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___80_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___80_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \caracter[2]_i_15\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \caracter[3]_i_22\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \caracter[3]_i_27\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \caracter[3]_i_34\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \caracter[3]_i_36\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \caracter[3]_i_40\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \caracter[3]_i_43\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \caracter[3]_i_44\ : label is "soft_lutpair27";
  attribute OPT_MODIFIED : string;
  attribute OPT_MODIFIED of \euros1__169_carry\ : label is "SWEEP";
  attribute OPT_MODIFIED of \euros1__169_carry__0\ : label is "SWEEP";
  attribute OPT_MODIFIED of \euros1__169_carry__1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \euros1__169_carry__2\ : label is "SWEEP";
  attribute OPT_MODIFIED of \euros1__169_carry__3\ : label is "SWEEP";
  attribute OPT_MODIFIED of \euros1__169_carry__4\ : label is "SWEEP";
  attribute OPT_MODIFIED of \euros1__1_carry\ : label is "PROPCONST SWEEP";
  attribute OPT_MODIFIED of \euros1__1_carry__0\ : label is "SWEEP";
  attribute HLUTNM : string;
  attribute HLUTNM of \euros1__1_carry__0_i_2\ : label is "lutpair0";
  attribute OPT_MODIFIED of \euros1__1_carry__1\ : label is "SWEEP";
  attribute HLUTNM of \euros1__1_carry__1_i_1\ : label is "lutpair3";
  attribute HLUTNM of \euros1__1_carry__1_i_2\ : label is "lutpair2";
  attribute HLUTNM of \euros1__1_carry__1_i_3\ : label is "lutpair1";
  attribute HLUTNM of \euros1__1_carry__1_i_4\ : label is "lutpair0";
  attribute HLUTNM of \euros1__1_carry__1_i_5\ : label is "lutpair4";
  attribute HLUTNM of \euros1__1_carry__1_i_6\ : label is "lutpair3";
  attribute HLUTNM of \euros1__1_carry__1_i_7\ : label is "lutpair2";
  attribute HLUTNM of \euros1__1_carry__1_i_8\ : label is "lutpair1";
  attribute OPT_MODIFIED of \euros1__1_carry__2\ : label is "SWEEP";
  attribute HLUTNM of \euros1__1_carry__2_i_1\ : label is "lutpair7";
  attribute HLUTNM of \euros1__1_carry__2_i_2\ : label is "lutpair6";
  attribute HLUTNM of \euros1__1_carry__2_i_3\ : label is "lutpair5";
  attribute HLUTNM of \euros1__1_carry__2_i_4\ : label is "lutpair4";
  attribute HLUTNM of \euros1__1_carry__2_i_5\ : label is "lutpair8";
  attribute HLUTNM of \euros1__1_carry__2_i_6\ : label is "lutpair7";
  attribute HLUTNM of \euros1__1_carry__2_i_7\ : label is "lutpair6";
  attribute HLUTNM of \euros1__1_carry__2_i_8\ : label is "lutpair5";
  attribute OPT_MODIFIED of \euros1__1_carry__3\ : label is "SWEEP";
  attribute HLUTNM of \euros1__1_carry__3_i_1\ : label is "lutpair11";
  attribute HLUTNM of \euros1__1_carry__3_i_2\ : label is "lutpair10";
  attribute HLUTNM of \euros1__1_carry__3_i_3\ : label is "lutpair9";
  attribute HLUTNM of \euros1__1_carry__3_i_4\ : label is "lutpair8";
  attribute HLUTNM of \euros1__1_carry__3_i_5\ : label is "lutpair12";
  attribute HLUTNM of \euros1__1_carry__3_i_6\ : label is "lutpair11";
  attribute HLUTNM of \euros1__1_carry__3_i_7\ : label is "lutpair10";
  attribute HLUTNM of \euros1__1_carry__3_i_8\ : label is "lutpair9";
  attribute OPT_MODIFIED of \euros1__1_carry__4\ : label is "SWEEP";
  attribute HLUTNM of \euros1__1_carry__4_i_1\ : label is "lutpair15";
  attribute HLUTNM of \euros1__1_carry__4_i_2\ : label is "lutpair14";
  attribute HLUTNM of \euros1__1_carry__4_i_3\ : label is "lutpair13";
  attribute HLUTNM of \euros1__1_carry__4_i_4\ : label is "lutpair12";
  attribute HLUTNM of \euros1__1_carry__4_i_5\ : label is "lutpair16";
  attribute HLUTNM of \euros1__1_carry__4_i_6\ : label is "lutpair15";
  attribute HLUTNM of \euros1__1_carry__4_i_7\ : label is "lutpair14";
  attribute HLUTNM of \euros1__1_carry__4_i_8\ : label is "lutpair13";
  attribute OPT_MODIFIED of \euros1__1_carry__5\ : label is "SWEEP";
  attribute HLUTNM of \euros1__1_carry__5_i_1\ : label is "lutpair19";
  attribute HLUTNM of \euros1__1_carry__5_i_2\ : label is "lutpair18";
  attribute HLUTNM of \euros1__1_carry__5_i_3\ : label is "lutpair17";
  attribute HLUTNM of \euros1__1_carry__5_i_4\ : label is "lutpair16";
  attribute HLUTNM of \euros1__1_carry__5_i_5\ : label is "lutpair20";
  attribute HLUTNM of \euros1__1_carry__5_i_6\ : label is "lutpair19";
  attribute HLUTNM of \euros1__1_carry__5_i_7\ : label is "lutpair18";
  attribute HLUTNM of \euros1__1_carry__5_i_8\ : label is "lutpair17";
  attribute OPT_MODIFIED of \euros1__1_carry__6\ : label is "SWEEP";
  attribute HLUTNM of \euros1__1_carry__6_i_3\ : label is "lutpair21";
  attribute HLUTNM of \euros1__1_carry__6_i_4\ : label is "lutpair20";
  attribute HLUTNM of \euros1__1_carry__6_i_8\ : label is "lutpair21";
  attribute OPT_MODIFIED of \euros1__1_carry__7\ : label is "SWEEP";
  attribute OPT_MODIFIED of \euros1__231_carry\ : label is "PROPCONST SWEEP";
  attribute OPT_MODIFIED of \euros1__231_carry__0\ : label is "SWEEP";
  attribute HLUTNM of \euros1__231_carry__0_i_1\ : label is "lutpair41";
  attribute HLUTNM of \euros1__231_carry__0_i_2\ : label is "lutpair40";
  attribute HLUTNM of \euros1__231_carry__0_i_3\ : label is "lutpair39";
  attribute HLUTNM of \euros1__231_carry__0_i_5\ : label is "lutpair42";
  attribute HLUTNM of \euros1__231_carry__0_i_6\ : label is "lutpair41";
  attribute HLUTNM of \euros1__231_carry__0_i_7\ : label is "lutpair40";
  attribute HLUTNM of \euros1__231_carry__0_i_8\ : label is "lutpair39";
  attribute OPT_MODIFIED of \euros1__231_carry__1\ : label is "SWEEP";
  attribute HLUTNM of \euros1__231_carry__1_i_1\ : label is "lutpair45";
  attribute HLUTNM of \euros1__231_carry__1_i_2\ : label is "lutpair44";
  attribute HLUTNM of \euros1__231_carry__1_i_3\ : label is "lutpair43";
  attribute HLUTNM of \euros1__231_carry__1_i_4\ : label is "lutpair42";
  attribute HLUTNM of \euros1__231_carry__1_i_5\ : label is "lutpair46";
  attribute HLUTNM of \euros1__231_carry__1_i_6\ : label is "lutpair45";
  attribute HLUTNM of \euros1__231_carry__1_i_7\ : label is "lutpair44";
  attribute HLUTNM of \euros1__231_carry__1_i_8\ : label is "lutpair43";
  attribute OPT_MODIFIED of \euros1__231_carry__2\ : label is "SWEEP";
  attribute HLUTNM of \euros1__231_carry__2_i_1\ : label is "lutpair48";
  attribute HLUTNM of \euros1__231_carry__2_i_2\ : label is "lutpair47";
  attribute HLUTNM of \euros1__231_carry__2_i_3\ : label is "lutpair46";
  attribute HLUTNM of \euros1__231_carry__2_i_6\ : label is "lutpair48";
  attribute HLUTNM of \euros1__231_carry__2_i_7\ : label is "lutpair47";
  attribute OPT_MODIFIED of \euros1__276_carry\ : label is "SWEEP";
  attribute OPT_MODIFIED of \euros1__276_carry__0\ : label is "SWEEP";
  attribute HLUTNM of \euros1__276_carry__0_i_3\ : label is "lutpair50";
  attribute HLUTNM of \euros1__276_carry__0_i_4\ : label is "lutpair49";
  attribute HLUTNM of \euros1__276_carry__0_i_8\ : label is "lutpair50";
  attribute SOFT_HLUTNM of \euros1__276_carry__0_i_9\ : label is "soft_lutpair33";
  attribute OPT_MODIFIED of \euros1__276_carry__1\ : label is "SWEEP";
  attribute SOFT_HLUTNM of \euros1__276_carry__1_i_10\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \euros1__276_carry__1_i_11\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \euros1__276_carry__1_i_12\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \euros1__276_carry__1_i_13\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \euros1__276_carry__1_i_14\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \euros1__276_carry__1_i_15\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \euros1__276_carry__1_i_9\ : label is "soft_lutpair37";
  attribute OPT_MODIFIED of \euros1__276_carry__2\ : label is "SWEEP";
  attribute SOFT_HLUTNM of \euros1__276_carry__2_i_11\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \euros1__276_carry__2_i_13\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \euros1__276_carry__2_i_14\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \euros1__276_carry__2_i_9\ : label is "soft_lutpair39";
  attribute OPT_MODIFIED of \euros1__276_carry__3\ : label is "SWEEP";
  attribute SOFT_HLUTNM of \euros1__276_carry__3_i_10\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \euros1__276_carry__3_i_11\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \euros1__276_carry__3_i_12\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \euros1__276_carry__3_i_13\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \euros1__276_carry__3_i_14\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \euros1__276_carry__3_i_15\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \euros1__276_carry__3_i_16\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \euros1__276_carry__3_i_9\ : label is "soft_lutpair43";
  attribute OPT_MODIFIED of \euros1__276_carry__4\ : label is "SWEEP";
  attribute SOFT_HLUTNM of \euros1__276_carry__4_i_11\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \euros1__276_carry__4_i_13\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \euros1__276_carry__4_i_14\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \euros1__276_carry__4_i_9\ : label is "soft_lutpair45";
  attribute OPT_MODIFIED of \euros1__276_carry__5\ : label is "SWEEP";
  attribute SOFT_HLUTNM of \euros1__276_carry__5_i_4\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \euros1__276_carry__5_i_5\ : label is "soft_lutpair46";
  attribute HLUTNM of \euros1__276_carry_i_5\ : label is "lutpair49";
  attribute OPT_MODIFIED of \euros1__333_carry\ : label is "PROPCONST SWEEP";
  attribute OPT_MODIFIED of \euros1__339_carry\ : label is "SWEEP";
  attribute OPT_MODIFIED of \euros1__339_carry__0\ : label is "SWEEP";
  attribute OPT_MODIFIED of \euros1__92_carry\ : label is "SWEEP";
  attribute OPT_MODIFIED of \euros1__92_carry__0\ : label is "SWEEP";
  attribute HLUTNM of \euros1__92_carry__0_i_1\ : label is "lutpair22";
  attribute HLUTNM of \euros1__92_carry__0_i_5\ : label is "lutpair23";
  attribute HLUTNM of \euros1__92_carry__0_i_6\ : label is "lutpair22";
  attribute OPT_MODIFIED of \euros1__92_carry__1\ : label is "SWEEP";
  attribute HLUTNM of \euros1__92_carry__1_i_1\ : label is "lutpair24";
  attribute HLUTNM of \euros1__92_carry__1_i_4\ : label is "lutpair23";
  attribute HLUTNM of \euros1__92_carry__1_i_5\ : label is "lutpair25";
  attribute HLUTNM of \euros1__92_carry__1_i_6\ : label is "lutpair24";
  attribute OPT_MODIFIED of \euros1__92_carry__2\ : label is "SWEEP";
  attribute HLUTNM of \euros1__92_carry__2_i_1\ : label is "lutpair28";
  attribute HLUTNM of \euros1__92_carry__2_i_2\ : label is "lutpair27";
  attribute HLUTNM of \euros1__92_carry__2_i_3\ : label is "lutpair26";
  attribute HLUTNM of \euros1__92_carry__2_i_4\ : label is "lutpair25";
  attribute HLUTNM of \euros1__92_carry__2_i_5\ : label is "lutpair29";
  attribute HLUTNM of \euros1__92_carry__2_i_6\ : label is "lutpair28";
  attribute HLUTNM of \euros1__92_carry__2_i_7\ : label is "lutpair27";
  attribute HLUTNM of \euros1__92_carry__2_i_8\ : label is "lutpair26";
  attribute OPT_MODIFIED of \euros1__92_carry__3\ : label is "SWEEP";
  attribute HLUTNM of \euros1__92_carry__3_i_1\ : label is "lutpair32";
  attribute HLUTNM of \euros1__92_carry__3_i_2\ : label is "lutpair31";
  attribute HLUTNM of \euros1__92_carry__3_i_3\ : label is "lutpair30";
  attribute HLUTNM of \euros1__92_carry__3_i_4\ : label is "lutpair29";
  attribute HLUTNM of \euros1__92_carry__3_i_5\ : label is "lutpair33";
  attribute HLUTNM of \euros1__92_carry__3_i_6\ : label is "lutpair32";
  attribute HLUTNM of \euros1__92_carry__3_i_7\ : label is "lutpair31";
  attribute HLUTNM of \euros1__92_carry__3_i_8\ : label is "lutpair30";
  attribute OPT_MODIFIED of \euros1__92_carry__4\ : label is "SWEEP";
  attribute HLUTNM of \euros1__92_carry__4_i_1\ : label is "lutpair36";
  attribute HLUTNM of \euros1__92_carry__4_i_2\ : label is "lutpair35";
  attribute HLUTNM of \euros1__92_carry__4_i_3\ : label is "lutpair34";
  attribute HLUTNM of \euros1__92_carry__4_i_4\ : label is "lutpair33";
  attribute HLUTNM of \euros1__92_carry__4_i_5\ : label is "lutpair37";
  attribute HLUTNM of \euros1__92_carry__4_i_6\ : label is "lutpair36";
  attribute HLUTNM of \euros1__92_carry__4_i_7\ : label is "lutpair35";
  attribute HLUTNM of \euros1__92_carry__4_i_8\ : label is "lutpair34";
  attribute OPT_MODIFIED of \euros1__92_carry__5\ : label is "SWEEP";
  attribute HLUTNM of \euros1__92_carry__5_i_1\ : label is "lutpair38";
  attribute HLUTNM of \euros1__92_carry__5_i_2\ : label is "lutpair37";
  attribute HLUTNM of \euros1__92_carry__5_i_5\ : label is "lutpair38";
  attribute OPT_MODIFIED of \i___0_carry__0_i_1\ : label is "RETARGET";
  attribute OPT_MODIFIED of \i___0_carry__0_i_4\ : label is "RETARGET";
  attribute OPT_MODIFIED of \i___0_carry__1_i_1\ : label is "RETARGET";
  attribute OPT_MODIFIED of \i___0_carry__1_i_5\ : label is "RETARGET";
  attribute OPT_MODIFIED of \i___0_carry_i_3\ : label is "RETARGET";
  attribute OPT_MODIFIED of \i___0_carry_i_4\ : label is "RETARGET";
  attribute OPT_MODIFIED of \i___0_carry_i_5\ : label is "RETARGET";
  attribute HLUTNM of \i___106_carry__0_i_3\ : label is "lutpair59";
  attribute HLUTNM of \i___106_carry__0_i_4\ : label is "lutpair58";
  attribute HLUTNM of \i___106_carry__0_i_8\ : label is "lutpair59";
  attribute SOFT_HLUTNM of \i___106_carry__0_i_9\ : label is "soft_lutpair29";
  attribute OPT_MODIFIED of \i___106_carry__1_i_1\ : label is "RETARGET";
  attribute SOFT_HLUTNM of \i___106_carry__1_i_10\ : label is "soft_lutpair29";
  attribute OPT_MODIFIED of \i___106_carry__1_i_11\ : label is "RETARGET";
  attribute SOFT_HLUTNM of \i___106_carry__1_i_11\ : label is "soft_lutpair32";
  attribute OPT_MODIFIED of \i___106_carry__1_i_12\ : label is "RETARGET";
  attribute OPT_MODIFIED of \i___106_carry__1_i_2\ : label is "RETARGET";
  attribute OPT_MODIFIED of \i___106_carry__1_i_3\ : label is "RETARGET";
  attribute OPT_MODIFIED of \i___106_carry__1_i_7\ : label is "RETARGET";
  attribute OPT_MODIFIED of \i___106_carry__1_i_9\ : label is "RETARGET";
  attribute SOFT_HLUTNM of \i___106_carry__1_i_9\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \i___106_carry__2_i_11\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \i___106_carry__2_i_9\ : label is "soft_lutpair31";
  attribute HLUTNM of \i___106_carry__3_i_3\ : label is "lutpair60";
  attribute HLUTNM of \i___106_carry__3_i_8\ : label is "lutpair60";
  attribute OPT_MODIFIED of \i___106_carry__4_i_4\ : label is "RETARGET";
  attribute OPT_MODIFIED of \i___106_carry__5_i_2\ : label is "RETARGET";
  attribute HLUTNM of \i___106_carry_i_5\ : label is "lutpair58";
  attribute OPT_MODIFIED of \i___169_carry_i_4\ : label is "RETARGET";
  attribute OPT_MODIFIED of \i___27_carry__0_i_2\ : label is "RETARGET";
  attribute HLUTNM of \i___27_carry__0_i_3\ : label is "lutpair62";
  attribute OPT_MODIFIED of \i___27_carry__0_i_3\ : label is "RETARGET";
  attribute OPT_MODIFIED of \i___27_carry__0_i_4\ : label is "RETARGET";
  attribute OPT_MODIFIED of \i___27_carry__0_i_5\ : label is "RETARGET";
  attribute OPT_MODIFIED of \i___27_carry__0_i_6\ : label is "RETARGET";
  attribute OPT_MODIFIED of \i___27_carry__0_i_7\ : label is "RETARGET";
  attribute HLUTNM of \i___27_carry__0_i_8\ : label is "lutpair62";
  attribute OPT_MODIFIED of \i___27_carry__0_i_8\ : label is "RETARGET";
  attribute OPT_MODIFIED of \i___27_carry_i_2\ : label is "RETARGET";
  attribute OPT_MODIFIED of \i___27_carry_i_3\ : label is "RETARGET";
  attribute OPT_MODIFIED of \i___56_carry__0_i_5\ : label is "RETARGET";
  attribute OPT_MODIFIED of \i___56_carry__0_i_6\ : label is "RETARGET";
  attribute OPT_MODIFIED of \i___80_carry__0_i_2\ : label is "RETARGET";
  attribute OPT_MODIFIED of \i___80_carry__0_i_3\ : label is "RETARGET";
  attribute OPT_MODIFIED of \i___80_carry__0_i_4\ : label is "RETARGET";
  attribute OPT_MODIFIED of \i___80_carry__0_i_6\ : label is "RETARGET";
  attribute OPT_MODIFIED of \i___80_carry__0_i_7\ : label is "RETARGET";
  attribute OPT_MODIFIED of \i___80_carry__0_i_8\ : label is "RETARGET";
  attribute OPT_MODIFIED of \i___80_carry_i_1\ : label is "RETARGET";
  attribute OPT_MODIFIED of \i___80_carry_i_2\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__100_carry\ : label is "PROPCONST SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__100_carry__0\ : label is "SWEEP";
  attribute HLUTNM of \string_cent_decenas[1]5__100_carry__0_i_2\ : label is "lutpair52";
  attribute HLUTNM of \string_cent_decenas[1]5__100_carry__0_i_3\ : label is "lutpair51";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__100_carry__0_i_3\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__100_carry__0_i_4\ : label is "RETARGET";
  attribute HLUTNM of \string_cent_decenas[1]5__100_carry__0_i_7\ : label is "lutpair52";
  attribute HLUTNM of \string_cent_decenas[1]5__100_carry__0_i_8\ : label is "lutpair51";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__100_carry__0_i_8\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__100_carry_i_1\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__100_carry_i_2\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__100_carry_i_4\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__100_carry_i_5\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__100_carry_i_6\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__124_carry\ : label is "SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__124_carry__0\ : label is "SWEEP";
  attribute HLUTNM of \string_cent_decenas[1]5__124_carry__0_i_1\ : label is "lutpair54";
  attribute HLUTNM of \string_cent_decenas[1]5__124_carry__0_i_2\ : label is "lutpair53";
  attribute HLUTNM of \string_cent_decenas[1]5__124_carry__0_i_5\ : label is "lutpair55";
  attribute HLUTNM of \string_cent_decenas[1]5__124_carry__0_i_6\ : label is "lutpair54";
  attribute HLUTNM of \string_cent_decenas[1]5__124_carry__0_i_7\ : label is "lutpair53";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__124_carry__1\ : label is "SWEEP";
  attribute HLUTNM of \string_cent_decenas[1]5__124_carry__1_i_2\ : label is "lutpair57";
  attribute HLUTNM of \string_cent_decenas[1]5__124_carry__1_i_3\ : label is "lutpair56";
  attribute HLUTNM of \string_cent_decenas[1]5__124_carry__1_i_4\ : label is "lutpair55";
  attribute HLUTNM of \string_cent_decenas[1]5__124_carry__1_i_7\ : label is "lutpair57";
  attribute HLUTNM of \string_cent_decenas[1]5__124_carry__1_i_8\ : label is "lutpair56";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__124_carry__1_i_9\ : label is "soft_lutpair30";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__124_carry__2\ : label is "SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__124_carry__2_i_1\ : label is "RETARGET";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__124_carry__2_i_10\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__124_carry__2_i_11\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__124_carry__2_i_12\ : label is "soft_lutpair30";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__124_carry__2_i_13\ : label is "RETARGET";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__124_carry__2_i_13\ : label is "soft_lutpair34";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__124_carry__2_i_14\ : label is "RETARGET";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__124_carry__2_i_14\ : label is "soft_lutpair34";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__124_carry__2_i_2\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__124_carry__2_i_3\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__124_carry__2_i_6\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__124_carry__2_i_7\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__124_carry__3\ : label is "SWEEP";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__124_carry__3_i_12\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__124_carry__3_i_13\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__124_carry__3_i_14\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__124_carry__3_i_9\ : label is "soft_lutpair48";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__124_carry__4\ : label is "SWEEP";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__124_carry__4_i_10\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__124_carry__4_i_11\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__124_carry__4_i_12\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__124_carry__4_i_9\ : label is "soft_lutpair50";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__124_carry__5\ : label is "SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__124_carry_i_2\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__124_carry_i_3\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__124_carry_i_6\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__124_carry_i_7\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__124_carry_i_8\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__180_carry\ : label is "PROPCONST SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__186_carry\ : label is "RETARGET SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__186_carry_i_5\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__186_carry_i_6\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__27_carry\ : label is "PROPCONST SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__27_carry__0\ : label is "SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__27_carry__0_i_3\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__27_carry__0_i_4\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__27_carry__0_i_7\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__27_carry__0_i_8\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__27_carry__1\ : label is "SWEEP";
  attribute HLUTNM of \string_cent_decenas[1]5__27_carry_i_1\ : label is "lutpair61";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__27_carry_i_1\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__27_carry_i_2\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__27_carry_i_4\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__27_carry_i_5\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__27_carry_i_6\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__55_carry\ : label is "RETARGET SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__55_carry__0\ : label is "RETARGET SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__55_carry__0_i_1\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__55_carry__0_i_2\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__55_carry__0_i_4\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__55_carry__0_i_5\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__55_carry__0_i_6\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__55_carry__0_i_7\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__55_carry__1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__55_carry_i_2\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__55_carry_i_3\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__79_carry\ : label is "PROPCONST SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__79_carry__0\ : label is "SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__79_carry__0_i_3\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__79_carry__0_i_4\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__79_carry__0_i_7\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__79_carry__0_i_8\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__79_carry__1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__79_carry_i_1\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__79_carry_i_3\ : label is "RETARGET";
  attribute HLUTNM of \string_cent_decenas[1]5__79_carry_i_4\ : label is "lutpair61";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__79_carry_i_4\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5__79_carry_i_5\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_carry\ : label is "RETARGET SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_carry__0\ : label is "RETARGET SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_carry__0_i_1\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_carry__0_i_2\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_carry__0_i_3\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_carry__0_i_5\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_carry__0_i_6\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_carry__0_i_7\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_carry__0_i_8\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_carry__1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_carry_i_12\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_carry_i_13\ : label is "RETARGET";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5_carry_i_14\ : label is "soft_lutpair24";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_carry_i_3\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_carry_i_4\ : label is "RETARGET";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5_carry_i_8\ : label is "soft_lutpair24";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_inferred__0/i___0_carry\ : label is "PROPCONST SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_inferred__0/i___0_carry__0\ : label is "SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_inferred__0/i___0_carry__1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_inferred__0/i___106_carry\ : label is "SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_inferred__0/i___106_carry__0\ : label is "SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_inferred__0/i___106_carry__1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_inferred__0/i___106_carry__2\ : label is "SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_inferred__0/i___106_carry__3\ : label is "SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_inferred__0/i___106_carry__4\ : label is "SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_inferred__0/i___106_carry__5\ : label is "SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_inferred__0/i___163_carry\ : label is "PROPCONST SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_inferred__0/i___169_carry\ : label is "RETARGET SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_inferred__0/i___169_carry__0\ : label is "SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_inferred__0/i___27_carry\ : label is "RETARGET SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_inferred__0/i___27_carry__0\ : label is "SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_inferred__0/i___27_carry__1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_inferred__0/i___56_carry\ : label is "RETARGET SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_inferred__0/i___56_carry__0\ : label is "RETARGET SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_inferred__0/i___56_carry__1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_inferred__0/i___80_carry\ : label is "PROPCONST SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_inferred__0/i___80_carry__0\ : label is "SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_inferred__0/i___80_carry__1\ : label is "SWEEP";
begin
  \total_reg[0]\ <= \^total_reg[0]\;
  \total_reg[20]\ <= \^total_reg[20]\;
  \total_reg[26]\ <= \^total_reg[26]\;
  \total_reg[29]\ <= \^total_reg[29]\;
  \total_reg[29]_0\ <= \^total_reg[29]_0\;
\FSM_sequential_next_state_reg[1]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => Q(29),
      I1 => Q(28),
      I2 => Q(25),
      I3 => Q(24),
      O => \^total_reg[29]_0\
    );
\caracter[0]_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9066"
    )
        port map (
      I0 => \caracter[3]_i_14_n_0\,
      I1 => \caracter[3]_i_25_n_0\,
      I2 => \caracter[3]_i_27_n_0\,
      I3 => \caracter[3]_i_26_n_0\,
      O => \caracter[0]_i_12_n_0\
    );
\caracter[0]_i_18\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"14"
    )
        port map (
      I0 => \^total_reg[29]\,
      I1 => p_1_in(0),
      I2 => Q(0),
      O => \disp_dinero[5]_1\(0)
    );
\caracter[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EAEAEAEAAAAAEAAA"
    )
        port map (
      I0 => \caracter_reg[0]\,
      I1 => \caracter_reg[0]_0\,
      I2 => \caracter_reg[0]_1\,
      I3 => \caracter[0]_i_12_n_0\,
      I4 => \caracter[3]_i_13_n_0\,
      I5 => \caracter[3]_i_12_n_0\,
      O => \current_state_reg[1]\
    );
\caracter[1]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0099000009000990"
    )
        port map (
      I0 => \caracter_reg[2]_0\,
      I1 => \caracter_reg[2]_1\(0),
      I2 => \string_cent_decenas[1]5__186_carry__0_n_7\,
      I3 => \string_cent_decenas[1]5__186_carry_n_6\,
      I4 => \string_cent_decenas[1]5__186_carry_n_5\,
      I5 => \string_cent_decenas[1]5__186_carry_n_4\,
      O => \charact_dot_reg[0]_0\
    );
\caracter[1]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0002"
    )
        port map (
      I0 => \caracter_reg[3]_3\,
      I1 => \caracter[3]_i_13_n_0\,
      I2 => \caracter[3]_i_11_n_0\,
      I3 => \caracter[3]_i_10_n_0\,
      O => \current_state_reg[1]_0\
    );
\caracter[2]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"003FE800"
    )
        port map (
      I0 => \caracter[3]_i_25_n_0\,
      I1 => \caracter[3]_i_21_n_0\,
      I2 => \caracter[3]_i_30_n_0\,
      I3 => \caracter[3]_i_20_n_0\,
      I4 => \caracter[3]_i_19_n_0\,
      O => \disp_dinero[3]_0\(2)
    );
\caracter[2]_i_15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6675"
    )
        port map (
      I0 => \string_cent_decenas[1]5__186_carry_n_5\,
      I1 => \string_cent_decenas[1]5__186_carry_n_6\,
      I2 => \string_cent_decenas[1]5__186_carry_n_4\,
      I3 => \string_cent_decenas[1]5__186_carry__0_n_7\,
      O => \caracter[2]_i_15_n_0\
    );
\caracter[2]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"80088CC880088008"
    )
        port map (
      I0 => \caracter[2]_i_9_n_0\,
      I1 => \caracter_reg[2]\,
      I2 => \caracter_reg[2]_0\,
      I3 => \caracter_reg[2]_1\(0),
      I4 => \caracter_reg[2]_1\(1),
      I5 => \disp_dinero[3]_0\(2),
      O => \charact_dot_reg[0]\
    );
\caracter[2]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0300550300000303"
    )
        port map (
      I0 => \^total_reg[29]\,
      I1 => \disp_dinero[2]_1\(3),
      I2 => \caracter[2]_i_15_n_0\,
      I3 => \caracter_reg[2]_0\,
      I4 => \caracter_reg[2]_1\(1),
      I5 => \caracter_reg[2]_1\(0),
      O => \caracter[2]_i_9_n_0\
    );
\caracter[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFC8"
    )
        port map (
      I0 => \caracter[3]_i_2_n_0\,
      I1 => \caracter_reg[3]\,
      I2 => \caracter[3]_i_4_n_0\,
      I3 => \caracter_reg[3]_0\,
      I4 => \caracter_reg[3]_1\,
      I5 => \caracter_reg[3]_2\,
      O => D(0)
    );
\caracter[3]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88A8A8A8888888A8"
    )
        port map (
      I0 => \caracter[3]_i_19_n_0\,
      I1 => \caracter[3]_i_20_n_0\,
      I2 => \caracter[3]_i_21_n_0\,
      I3 => \disp_dinero[2]_1\(3),
      I4 => \caracter[3]_i_23_n_0\,
      I5 => \caracter[3]_i_24_n_0\,
      O => \caracter[3]_i_10_n_0\
    );
\caracter[3]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000010001110"
    )
        port map (
      I0 => \caracter[3]_i_20_n_0\,
      I1 => \caracter[3]_i_21_n_0\,
      I2 => \disp_dinero[2]_1\(3),
      I3 => \caracter[3]_i_23_n_0\,
      I4 => \caracter[3]_i_24_n_0\,
      I5 => \caracter[3]_i_19_n_0\,
      O => \caracter[3]_i_11_n_0\
    );
\caracter[3]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DBD39B9224240404"
    )
        port map (
      I0 => \caracter[3]_i_14_n_0\,
      I1 => \caracter[3]_i_25_n_0\,
      I2 => \caracter[3]_i_26_n_0\,
      I3 => \caracter[3]_i_27_n_0\,
      I4 => \caracter[3]_i_28_n_0\,
      I5 => \caracter[3]_i_29_n_0\,
      O => \caracter[3]_i_12_n_0\
    );
\caracter[3]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"83C1683F97C3E97F"
    )
        port map (
      I0 => \caracter[3]_i_25_n_0\,
      I1 => \caracter[3]_i_30_n_0\,
      I2 => \caracter[3]_i_21_n_0\,
      I3 => \caracter[3]_i_20_n_0\,
      I4 => \caracter[3]_i_19_n_0\,
      I5 => \caracter[3]_i_26_n_0\,
      O => \caracter[3]_i_13_n_0\
    );
\caracter[3]_i_14\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7CCC377F"
    )
        port map (
      I0 => \caracter[3]_i_31_n_0\,
      I1 => \caracter[3]_i_20_n_0\,
      I2 => \caracter[3]_i_21_n_0\,
      I3 => \caracter[3]_i_30_n_0\,
      I4 => \caracter[3]_i_19_n_0\,
      O => \caracter[3]_i_14_n_0\
    );
\caracter[3]_i_19\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4A4A4A4A4A4A4AAA"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_4\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_6\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_7\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_5\,
      I5 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_4\,
      O => \caracter[3]_i_19_n_0\
    );
\caracter[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"444A000000000000"
    )
        port map (
      I0 => \string_cent_decenas[1]5__186_carry_n_4\,
      I1 => \string_cent_decenas[1]5__186_carry__0_n_7\,
      I2 => \string_cent_decenas[1]5__186_carry_n_6\,
      I3 => \string_cent_decenas[1]5__186_carry_n_5\,
      I4 => \caracter_reg[3]_4\,
      I5 => \caracter_reg[3]_5\,
      O => \caracter[3]_i_2_n_0\
    );
\caracter[3]_i_20\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"010101FFFE00FE00"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_5\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_4\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_5\,
      I5 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_6\,
      O => \caracter[3]_i_20_n_0\
    );
\caracter[3]_i_21\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFF1000AFFF5000A"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_4\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_5\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_5\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_7\,
      I5 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_6\,
      O => \caracter[3]_i_21_n_0\
    );
\caracter[3]_i_22\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"444A"
    )
        port map (
      I0 => \string_cent_decenas[1]5__186_carry_n_4\,
      I1 => \string_cent_decenas[1]5__186_carry__0_n_7\,
      I2 => \string_cent_decenas[1]5__186_carry_n_6\,
      I3 => \string_cent_decenas[1]5__186_carry_n_5\,
      O => \disp_dinero[2]_1\(3)
    );
\caracter[3]_i_23\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0FE10FE50FA50FA5"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_4\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_5\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_5\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_7\,
      I5 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_6\,
      O => \caracter[3]_i_23_n_0\
    );
\caracter[3]_i_24\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFEF8AEF8A0000"
    )
        port map (
      I0 => \string_cent_decenas[1]5\(1),
      I1 => \string_cent_decenas[1]5\(0),
      I2 => \^total_reg[0]\,
      I3 => \caracter[3]_i_34_n_0\,
      I4 => \caracter[2]_i_15_n_0\,
      I5 => \caracter[3]_i_35_n_0\,
      O => \caracter[3]_i_24_n_0\
    );
\caracter[3]_i_25\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969966996969696"
    )
        port map (
      I0 => \disp_dinero[2]_1\(3),
      I1 => \caracter[3]_i_23_n_0\,
      I2 => \caracter[3]_i_24_n_0\,
      I3 => \caracter[3]_i_36_n_0\,
      I4 => \caracter[3]_i_37_n_0\,
      I5 => \caracter[3]_i_11_n_0\,
      O => \caracter[3]_i_25_n_0\
    );
\caracter[3]_i_26\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"65569559AAAAAAAA"
    )
        port map (
      I0 => \caracter[3]_i_37_n_0\,
      I1 => \string_cent_decenas[1]5\(1),
      I2 => \string_cent_decenas[1]5\(0),
      I3 => \^total_reg[0]\,
      I4 => \caracter[3]_i_34_n_0\,
      I5 => \caracter[3]_i_11_n_0\,
      O => \caracter[3]_i_26_n_0\
    );
\caracter[3]_i_27\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99699699"
    )
        port map (
      I0 => \string_cent_decenas[1]5\(1),
      I1 => \caracter[3]_i_34_n_0\,
      I2 => \string_cent_decenas[1]5\(0),
      I3 => \^total_reg[0]\,
      I4 => \caracter[3]_i_11_n_0\,
      O => \caracter[3]_i_27_n_0\
    );
\caracter[3]_i_28\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5FFAAAA0055FFAA9"
    )
        port map (
      I0 => \caracter[3]_i_31_n_0\,
      I1 => \caracter[3]_i_38_n_0\,
      I2 => \caracter[3]_i_21_n_0\,
      I3 => \caracter[3]_i_30_n_0\,
      I4 => \caracter[3]_i_20_n_0\,
      I5 => \caracter[3]_i_19_n_0\,
      O => \caracter[3]_i_28_n_0\
    );
\caracter[3]_i_29\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6696999999696666"
    )
        port map (
      I0 => \caracter[3]_i_39_n_0\,
      I1 => \caracter[3]_i_31_n_0\,
      I2 => \caracter[3]_i_36_n_0\,
      I3 => \caracter[3]_i_37_n_0\,
      I4 => \caracter[3]_i_11_n_0\,
      I5 => \caracter[3]_i_10_n_0\,
      O => \caracter[3]_i_29_n_0\
    );
\caracter[3]_i_30\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"40D00D40F4FDDFF4"
    )
        port map (
      I0 => \caracter[3]_i_40_n_0\,
      I1 => \caracter[2]_i_15_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_4\,
      I3 => \caracter[3]_i_41_n_0\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_5\,
      I5 => \disp_dinero[2]_1\(3),
      O => \caracter[3]_i_30_n_0\
    );
\caracter[3]_i_31\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"D24BB4D22DB44B2D"
    )
        port map (
      I0 => \caracter[2]_i_15_n_0\,
      I1 => \caracter[3]_i_40_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_4\,
      I3 => \caracter[3]_i_41_n_0\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_5\,
      I5 => \disp_dinero[2]_1\(3),
      O => \caracter[3]_i_31_n_0\
    );
\caracter[3]_i_34\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CD99"
    )
        port map (
      I0 => \string_cent_decenas[1]5__186_carry__0_n_7\,
      I1 => \string_cent_decenas[1]5__186_carry_n_6\,
      I2 => \string_cent_decenas[1]5__186_carry_n_5\,
      I3 => \string_cent_decenas[1]5__186_carry_n_4\,
      O => \caracter[3]_i_34_n_0\
    );
\caracter[3]_i_35\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0F0F0F0F5A58F0F0"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_7\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_5\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_4\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_5\,
      I5 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_4\,
      O => \caracter[3]_i_35_n_0\
    );
\caracter[3]_i_36\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4182"
    )
        port map (
      I0 => \string_cent_decenas[1]5\(1),
      I1 => \string_cent_decenas[1]5\(0),
      I2 => \^total_reg[0]\,
      I3 => \caracter[3]_i_34_n_0\,
      O => \caracter[3]_i_36_n_0\
    );
\caracter[3]_i_37\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A6656559599A9AA6"
    )
        port map (
      I0 => \caracter[3]_i_35_n_0\,
      I1 => \string_cent_decenas[1]5\(1),
      I2 => \caracter[3]_i_43_n_0\,
      I3 => \caracter[3]_i_44_n_0\,
      I4 => \string_cent_decenas[1]5__186_carry_n_6\,
      I5 => \string_cent_decenas[1]5__186_carry_n_5\,
      O => \caracter[3]_i_37_n_0\
    );
\caracter[3]_i_38\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1001400420028008"
    )
        port map (
      I0 => \caracter[2]_i_15_n_0\,
      I1 => \caracter[3]_i_34_n_0\,
      I2 => \^total_reg[0]\,
      I3 => \string_cent_decenas[1]5\(0),
      I4 => \string_cent_decenas[1]5\(1),
      I5 => \caracter[3]_i_35_n_0\,
      O => \caracter[3]_i_38_n_0\
    );
\caracter[3]_i_39\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00F0F0FFEE0E0E00"
    )
        port map (
      I0 => \caracter[3]_i_19_n_0\,
      I1 => \caracter[3]_i_20_n_0\,
      I2 => \caracter[3]_i_24_n_0\,
      I3 => \caracter[3]_i_23_n_0\,
      I4 => \disp_dinero[2]_1\(3),
      I5 => \caracter[3]_i_21_n_0\,
      O => \caracter[3]_i_39_n_0\
    );
\caracter[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A888A8A8A8A8A8A8"
    )
        port map (
      I0 => \caracter_reg[3]_3\,
      I1 => \caracter[3]_i_10_n_0\,
      I2 => \caracter[3]_i_11_n_0\,
      I3 => \caracter[3]_i_12_n_0\,
      I4 => \caracter[3]_i_13_n_0\,
      I5 => \caracter[3]_i_14_n_0\,
      O => \caracter[3]_i_4_n_0\
    );
\caracter[3]_i_40\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"045D"
    )
        port map (
      I0 => \caracter[3]_i_34_n_0\,
      I1 => \^total_reg[0]\,
      I2 => \string_cent_decenas[1]5\(0),
      I3 => \string_cent_decenas[1]5\(1),
      O => \caracter[3]_i_40_n_0\
    );
\caracter[3]_i_41\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAAAAAAAAA"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_4\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_5\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_5\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_7\,
      I5 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_6\,
      O => \caracter[3]_i_41_n_0\
    );
\caracter[3]_i_43\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^total_reg[0]\,
      I1 => \string_cent_decenas[1]5\(0),
      O => \caracter[3]_i_43_n_0\
    );
\caracter[3]_i_44\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0057"
    )
        port map (
      I0 => \string_cent_decenas[1]5__186_carry_n_4\,
      I1 => \string_cent_decenas[1]5__186_carry_n_5\,
      I2 => \string_cent_decenas[1]5__186_carry_n_6\,
      I3 => \string_cent_decenas[1]5__186_carry__0_n_7\,
      O => \caracter[3]_i_44_n_0\
    );
\euros1__169_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \euros1__169_carry_n_0\,
      CO(2 downto 0) => \NLW_euros1__169_carry_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => Q(0),
      DI(2 downto 0) => B"001",
      O(3) => \euros1__169_carry_n_4\,
      O(2) => \euros1__169_carry_n_5\,
      O(1) => \euros1__169_carry_n_6\,
      O(0) => \NLW_euros1__169_carry_O_UNCONNECTED\(0),
      S(3) => \euros1__169_carry_i_1_n_0\,
      S(2) => \euros1__169_carry_i_2_n_0\,
      S(1) => \euros1__169_carry_i_3_n_0\,
      S(0) => Q(0)
    );
\euros1__169_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__169_carry_n_0\,
      CO(3) => \euros1__169_carry__0_n_0\,
      CO(2 downto 0) => \NLW_euros1__169_carry__0_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => Q(4 downto 1),
      O(3) => \euros1__169_carry__0_n_4\,
      O(2) => \euros1__169_carry__0_n_5\,
      O(1) => \euros1__169_carry__0_n_6\,
      O(0) => \euros1__169_carry__0_n_7\,
      S(3) => \euros1__169_carry__0_i_1_n_0\,
      S(2) => \euros1__169_carry__0_i_2_n_0\,
      S(1) => \euros1__169_carry__0_i_3_n_0\,
      S(0) => \euros1__169_carry__0_i_4_n_0\
    );
\euros1__169_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(4),
      I1 => Q(7),
      O => \euros1__169_carry__0_i_1_n_0\
    );
\euros1__169_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(3),
      I1 => Q(6),
      O => \euros1__169_carry__0_i_2_n_0\
    );
\euros1__169_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(2),
      I1 => Q(5),
      O => \euros1__169_carry__0_i_3_n_0\
    );
\euros1__169_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(1),
      I1 => Q(4),
      O => \euros1__169_carry__0_i_4_n_0\
    );
\euros1__169_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__169_carry__0_n_0\,
      CO(3) => \euros1__169_carry__1_n_0\,
      CO(2 downto 0) => \NLW_euros1__169_carry__1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => Q(8 downto 5),
      O(3) => \euros1__169_carry__1_n_4\,
      O(2) => \euros1__169_carry__1_n_5\,
      O(1) => \euros1__169_carry__1_n_6\,
      O(0) => \euros1__169_carry__1_n_7\,
      S(3) => \euros1__169_carry__1_i_1_n_0\,
      S(2) => \euros1__169_carry__1_i_2_n_0\,
      S(1) => \euros1__169_carry__1_i_3_n_0\,
      S(0) => \euros1__169_carry__1_i_4_n_0\
    );
\euros1__169_carry__1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(8),
      I1 => Q(11),
      O => \euros1__169_carry__1_i_1_n_0\
    );
\euros1__169_carry__1_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(7),
      I1 => Q(10),
      O => \euros1__169_carry__1_i_2_n_0\
    );
\euros1__169_carry__1_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(6),
      I1 => Q(9),
      O => \euros1__169_carry__1_i_3_n_0\
    );
\euros1__169_carry__1_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(5),
      I1 => Q(8),
      O => \euros1__169_carry__1_i_4_n_0\
    );
\euros1__169_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__169_carry__1_n_0\,
      CO(3) => \euros1__169_carry__2_n_0\,
      CO(2 downto 0) => \NLW_euros1__169_carry__2_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => Q(12 downto 9),
      O(3) => \euros1__169_carry__2_n_4\,
      O(2) => \euros1__169_carry__2_n_5\,
      O(1) => \euros1__169_carry__2_n_6\,
      O(0) => \euros1__169_carry__2_n_7\,
      S(3) => \euros1__169_carry__2_i_1_n_0\,
      S(2) => \euros1__169_carry__2_i_2_n_0\,
      S(1) => \euros1__169_carry__2_i_3_n_0\,
      S(0) => \euros1__169_carry__2_i_4_n_0\
    );
\euros1__169_carry__2_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(12),
      I1 => Q(15),
      O => \euros1__169_carry__2_i_1_n_0\
    );
\euros1__169_carry__2_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(11),
      I1 => Q(14),
      O => \euros1__169_carry__2_i_2_n_0\
    );
\euros1__169_carry__2_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(10),
      I1 => Q(13),
      O => \euros1__169_carry__2_i_3_n_0\
    );
\euros1__169_carry__2_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(9),
      I1 => Q(12),
      O => \euros1__169_carry__2_i_4_n_0\
    );
\euros1__169_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__169_carry__2_n_0\,
      CO(3) => \euros1__169_carry__3_n_0\,
      CO(2 downto 0) => \NLW_euros1__169_carry__3_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => Q(16 downto 13),
      O(3) => \euros1__169_carry__3_n_4\,
      O(2) => \euros1__169_carry__3_n_5\,
      O(1) => \euros1__169_carry__3_n_6\,
      O(0) => \euros1__169_carry__3_n_7\,
      S(3) => \euros1__169_carry__3_i_1_n_0\,
      S(2) => \euros1__169_carry__3_i_2_n_0\,
      S(1) => \euros1__169_carry__3_i_3_n_0\,
      S(0) => \euros1__169_carry__3_i_4_n_0\
    );
\euros1__169_carry__3_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(16),
      I1 => Q(19),
      O => \euros1__169_carry__3_i_1_n_0\
    );
\euros1__169_carry__3_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(15),
      I1 => Q(18),
      O => \euros1__169_carry__3_i_2_n_0\
    );
\euros1__169_carry__3_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(14),
      I1 => Q(17),
      O => \euros1__169_carry__3_i_3_n_0\
    );
\euros1__169_carry__3_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(13),
      I1 => Q(16),
      O => \euros1__169_carry__3_i_4_n_0\
    );
\euros1__169_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__169_carry__3_n_0\,
      CO(3 downto 0) => \NLW_euros1__169_carry__4_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => Q(17),
      O(3 downto 2) => \NLW_euros1__169_carry__4_O_UNCONNECTED\(3 downto 2),
      O(1) => \euros1__169_carry__4_n_6\,
      O(0) => \euros1__169_carry__4_n_7\,
      S(3 downto 2) => B"00",
      S(1) => \euros1__169_carry__4_i_1_n_0\,
      S(0) => \euros1__169_carry__4_i_2_n_0\
    );
\euros1__169_carry__4_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(18),
      I1 => Q(21),
      O => \euros1__169_carry__4_i_1_n_0\
    );
\euros1__169_carry__4_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(17),
      I1 => Q(20),
      O => \euros1__169_carry__4_i_2_n_0\
    );
\euros1__169_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(0),
      I1 => Q(3),
      O => \euros1__169_carry_i_1_n_0\
    );
\euros1__169_carry_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => Q(2),
      O => \euros1__169_carry_i_2_n_0\
    );
\euros1__169_carry_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => Q(1),
      O => \euros1__169_carry_i_3_n_0\
    );
\euros1__1_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \euros1__1_carry_n_0\,
      CO(2 downto 0) => \NLW_euros1__1_carry_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 1) => Q(4 downto 2),
      DI(0) => '0',
      O(3 downto 0) => \NLW_euros1__1_carry_O_UNCONNECTED\(3 downto 0),
      S(3) => \euros1__1_carry_i_1_n_0\,
      S(2) => \euros1__1_carry_i_2_n_0\,
      S(1) => \euros1__1_carry_i_3_n_0\,
      S(0) => Q(1)
    );
\euros1__1_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__1_carry_n_0\,
      CO(3) => \euros1__1_carry__0_n_0\,
      CO(2 downto 0) => \NLW_euros1__1_carry__0_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \euros1__1_carry__0_i_1_n_0\,
      DI(2 downto 0) => Q(7 downto 5),
      O(3) => \euros1__1_carry__0_n_4\,
      O(2 downto 0) => \NLW_euros1__1_carry__0_O_UNCONNECTED\(2 downto 0),
      S(3) => \euros1__1_carry__0_i_2_n_0\,
      S(2) => \euros1__1_carry__0_i_3_n_0\,
      S(1) => \euros1__1_carry__0_i_4_n_0\,
      S(0) => \euros1__1_carry__0_i_5_n_0\
    );
\euros1__1_carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(8),
      I1 => Q(6),
      I2 => Q(1),
      O => \euros1__1_carry__0_i_1_n_0\
    );
\euros1__1_carry__0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => Q(6),
      I1 => Q(8),
      I2 => Q(1),
      I3 => Q(0),
      I4 => Q(5),
      O => \euros1__1_carry__0_i_2_n_0\
    );
\euros1__1_carry__0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(0),
      I1 => Q(5),
      I2 => Q(7),
      O => \euros1__1_carry__0_i_3_n_0\
    );
\euros1__1_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(6),
      I1 => Q(4),
      O => \euros1__1_carry__0_i_4_n_0\
    );
\euros1__1_carry__0_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(5),
      I1 => Q(3),
      O => \euros1__1_carry__0_i_5_n_0\
    );
\euros1__1_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__1_carry__0_n_0\,
      CO(3) => \euros1__1_carry__1_n_0\,
      CO(2 downto 0) => \NLW_euros1__1_carry__1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \euros1__1_carry__1_i_1_n_0\,
      DI(2) => \euros1__1_carry__1_i_2_n_0\,
      DI(1) => \euros1__1_carry__1_i_3_n_0\,
      DI(0) => \euros1__1_carry__1_i_4_n_0\,
      O(3) => \euros1__1_carry__1_n_4\,
      O(2) => \euros1__1_carry__1_n_5\,
      O(1) => \euros1__1_carry__1_n_6\,
      O(0) => \euros1__1_carry__1_n_7\,
      S(3) => \euros1__1_carry__1_i_5_n_0\,
      S(2) => \euros1__1_carry__1_i_6_n_0\,
      S(1) => \euros1__1_carry__1_i_7_n_0\,
      S(0) => \euros1__1_carry__1_i_8_n_0\
    );
\euros1__1_carry__1_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => Q(9),
      I1 => Q(11),
      I2 => Q(4),
      O => \euros1__1_carry__1_i_1_n_0\
    );
\euros1__1_carry__1_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => Q(8),
      I1 => Q(10),
      I2 => Q(3),
      O => \euros1__1_carry__1_i_2_n_0\
    );
\euros1__1_carry__1_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => Q(7),
      I1 => Q(9),
      I2 => Q(2),
      O => \euros1__1_carry__1_i_3_n_0\
    );
\euros1__1_carry__1_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => Q(6),
      I1 => Q(8),
      I2 => Q(1),
      O => \euros1__1_carry__1_i_4_n_0\
    );
\euros1__1_carry__1_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(10),
      I1 => Q(12),
      I2 => Q(5),
      I3 => \euros1__1_carry__1_i_1_n_0\,
      O => \euros1__1_carry__1_i_5_n_0\
    );
\euros1__1_carry__1_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(9),
      I1 => Q(11),
      I2 => Q(4),
      I3 => \euros1__1_carry__1_i_2_n_0\,
      O => \euros1__1_carry__1_i_6_n_0\
    );
\euros1__1_carry__1_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(8),
      I1 => Q(10),
      I2 => Q(3),
      I3 => \euros1__1_carry__1_i_3_n_0\,
      O => \euros1__1_carry__1_i_7_n_0\
    );
\euros1__1_carry__1_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(7),
      I1 => Q(9),
      I2 => Q(2),
      I3 => \euros1__1_carry__1_i_4_n_0\,
      O => \euros1__1_carry__1_i_8_n_0\
    );
\euros1__1_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__1_carry__1_n_0\,
      CO(3) => \euros1__1_carry__2_n_0\,
      CO(2 downto 0) => \NLW_euros1__1_carry__2_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \euros1__1_carry__2_i_1_n_0\,
      DI(2) => \euros1__1_carry__2_i_2_n_0\,
      DI(1) => \euros1__1_carry__2_i_3_n_0\,
      DI(0) => \euros1__1_carry__2_i_4_n_0\,
      O(3) => \euros1__1_carry__2_n_4\,
      O(2) => \euros1__1_carry__2_n_5\,
      O(1) => \euros1__1_carry__2_n_6\,
      O(0) => \euros1__1_carry__2_n_7\,
      S(3) => \euros1__1_carry__2_i_5_n_0\,
      S(2) => \euros1__1_carry__2_i_6_n_0\,
      S(1) => \euros1__1_carry__2_i_7_n_0\,
      S(0) => \euros1__1_carry__2_i_8_n_0\
    );
\euros1__1_carry__2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => Q(8),
      I1 => Q(15),
      I2 => Q(13),
      O => \euros1__1_carry__2_i_1_n_0\
    );
\euros1__1_carry__2_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => Q(12),
      I1 => Q(14),
      I2 => Q(7),
      O => \euros1__1_carry__2_i_2_n_0\
    );
\euros1__1_carry__2_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => Q(6),
      I1 => Q(13),
      I2 => Q(11),
      O => \euros1__1_carry__2_i_3_n_0\
    );
\euros1__1_carry__2_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => Q(10),
      I1 => Q(12),
      I2 => Q(5),
      O => \euros1__1_carry__2_i_4_n_0\
    );
\euros1__1_carry__2_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(14),
      I1 => Q(16),
      I2 => Q(9),
      I3 => \euros1__1_carry__2_i_1_n_0\,
      O => \euros1__1_carry__2_i_5_n_0\
    );
\euros1__1_carry__2_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(8),
      I1 => Q(15),
      I2 => Q(13),
      I3 => \euros1__1_carry__2_i_2_n_0\,
      O => \euros1__1_carry__2_i_6_n_0\
    );
\euros1__1_carry__2_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(12),
      I1 => Q(14),
      I2 => Q(7),
      I3 => \euros1__1_carry__2_i_3_n_0\,
      O => \euros1__1_carry__2_i_7_n_0\
    );
\euros1__1_carry__2_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(6),
      I1 => Q(13),
      I2 => Q(11),
      I3 => \euros1__1_carry__2_i_4_n_0\,
      O => \euros1__1_carry__2_i_8_n_0\
    );
\euros1__1_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__1_carry__2_n_0\,
      CO(3) => \euros1__1_carry__3_n_0\,
      CO(2 downto 0) => \NLW_euros1__1_carry__3_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \euros1__1_carry__3_i_1_n_0\,
      DI(2) => \euros1__1_carry__3_i_2_n_0\,
      DI(1) => \euros1__1_carry__3_i_3_n_0\,
      DI(0) => \euros1__1_carry__3_i_4_n_0\,
      O(3) => \euros1__1_carry__3_n_4\,
      O(2) => \euros1__1_carry__3_n_5\,
      O(1) => \euros1__1_carry__3_n_6\,
      O(0) => \euros1__1_carry__3_n_7\,
      S(3) => \euros1__1_carry__3_i_5_n_0\,
      S(2) => \euros1__1_carry__3_i_6_n_0\,
      S(1) => \euros1__1_carry__3_i_7_n_0\,
      S(0) => \euros1__1_carry__3_i_8_n_0\
    );
\euros1__1_carry__3_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => Q(12),
      I1 => Q(19),
      I2 => Q(17),
      O => \euros1__1_carry__3_i_1_n_0\
    );
\euros1__1_carry__3_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => Q(16),
      I1 => Q(18),
      I2 => Q(11),
      O => \euros1__1_carry__3_i_2_n_0\
    );
\euros1__1_carry__3_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => Q(10),
      I1 => Q(17),
      I2 => Q(15),
      O => \euros1__1_carry__3_i_3_n_0\
    );
\euros1__1_carry__3_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => Q(14),
      I1 => Q(16),
      I2 => Q(9),
      O => \euros1__1_carry__3_i_4_n_0\
    );
\euros1__1_carry__3_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(18),
      I1 => Q(20),
      I2 => Q(13),
      I3 => \euros1__1_carry__3_i_1_n_0\,
      O => \euros1__1_carry__3_i_5_n_0\
    );
\euros1__1_carry__3_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(12),
      I1 => Q(19),
      I2 => Q(17),
      I3 => \euros1__1_carry__3_i_2_n_0\,
      O => \euros1__1_carry__3_i_6_n_0\
    );
\euros1__1_carry__3_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(16),
      I1 => Q(18),
      I2 => Q(11),
      I3 => \euros1__1_carry__3_i_3_n_0\,
      O => \euros1__1_carry__3_i_7_n_0\
    );
\euros1__1_carry__3_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(10),
      I1 => Q(17),
      I2 => Q(15),
      I3 => \euros1__1_carry__3_i_4_n_0\,
      O => \euros1__1_carry__3_i_8_n_0\
    );
\euros1__1_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__1_carry__3_n_0\,
      CO(3) => \euros1__1_carry__4_n_0\,
      CO(2 downto 0) => \NLW_euros1__1_carry__4_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \euros1__1_carry__4_i_1_n_0\,
      DI(2) => \euros1__1_carry__4_i_2_n_0\,
      DI(1) => \euros1__1_carry__4_i_3_n_0\,
      DI(0) => \euros1__1_carry__4_i_4_n_0\,
      O(3) => \euros1__1_carry__4_n_4\,
      O(2) => \euros1__1_carry__4_n_5\,
      O(1) => \euros1__1_carry__4_n_6\,
      O(0) => \euros1__1_carry__4_n_7\,
      S(3) => \euros1__1_carry__4_i_5_n_0\,
      S(2) => \euros1__1_carry__4_i_6_n_0\,
      S(1) => \euros1__1_carry__4_i_7_n_0\,
      S(0) => \euros1__1_carry__4_i_8_n_0\
    );
\euros1__1_carry__4_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => Q(16),
      I1 => Q(23),
      I2 => Q(21),
      O => \euros1__1_carry__4_i_1_n_0\
    );
\euros1__1_carry__4_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => Q(20),
      I1 => Q(22),
      I2 => Q(15),
      O => \euros1__1_carry__4_i_2_n_0\
    );
\euros1__1_carry__4_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => Q(14),
      I1 => Q(21),
      I2 => Q(19),
      O => \euros1__1_carry__4_i_3_n_0\
    );
\euros1__1_carry__4_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => Q(18),
      I1 => Q(20),
      I2 => Q(13),
      O => \euros1__1_carry__4_i_4_n_0\
    );
\euros1__1_carry__4_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(17),
      I1 => Q(24),
      I2 => Q(22),
      I3 => \euros1__1_carry__4_i_1_n_0\,
      O => \euros1__1_carry__4_i_5_n_0\
    );
\euros1__1_carry__4_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(16),
      I1 => Q(23),
      I2 => Q(21),
      I3 => \euros1__1_carry__4_i_2_n_0\,
      O => \euros1__1_carry__4_i_6_n_0\
    );
\euros1__1_carry__4_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(20),
      I1 => Q(22),
      I2 => Q(15),
      I3 => \euros1__1_carry__4_i_3_n_0\,
      O => \euros1__1_carry__4_i_7_n_0\
    );
\euros1__1_carry__4_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(14),
      I1 => Q(21),
      I2 => Q(19),
      I3 => \euros1__1_carry__4_i_4_n_0\,
      O => \euros1__1_carry__4_i_8_n_0\
    );
\euros1__1_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__1_carry__4_n_0\,
      CO(3) => \euros1__1_carry__5_n_0\,
      CO(2 downto 0) => \NLW_euros1__1_carry__5_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \euros1__1_carry__5_i_1_n_0\,
      DI(2) => \euros1__1_carry__5_i_2_n_0\,
      DI(1) => \euros1__1_carry__5_i_3_n_0\,
      DI(0) => \euros1__1_carry__5_i_4_n_0\,
      O(3) => \euros1__1_carry__5_n_4\,
      O(2) => \euros1__1_carry__5_n_5\,
      O(1) => \euros1__1_carry__5_n_6\,
      O(0) => \euros1__1_carry__5_n_7\,
      S(3) => \euros1__1_carry__5_i_5_n_0\,
      S(2) => \euros1__1_carry__5_i_6_n_0\,
      S(1) => \euros1__1_carry__5_i_7_n_0\,
      S(0) => \euros1__1_carry__5_i_8_n_0\
    );
\euros1__1_carry__5_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => Q(20),
      I1 => Q(27),
      I2 => Q(25),
      O => \euros1__1_carry__5_i_1_n_0\
    );
\euros1__1_carry__5_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => Q(19),
      I1 => Q(26),
      I2 => Q(24),
      O => \euros1__1_carry__5_i_2_n_0\
    );
\euros1__1_carry__5_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => Q(18),
      I1 => Q(25),
      I2 => Q(23),
      O => \euros1__1_carry__5_i_3_n_0\
    );
\euros1__1_carry__5_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => Q(17),
      I1 => Q(24),
      I2 => Q(22),
      O => \euros1__1_carry__5_i_4_n_0\
    );
\euros1__1_carry__5_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(21),
      I1 => Q(28),
      I2 => Q(26),
      I3 => \euros1__1_carry__5_i_1_n_0\,
      O => \euros1__1_carry__5_i_5_n_0\
    );
\euros1__1_carry__5_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(20),
      I1 => Q(27),
      I2 => Q(25),
      I3 => \euros1__1_carry__5_i_2_n_0\,
      O => \euros1__1_carry__5_i_6_n_0\
    );
\euros1__1_carry__5_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(19),
      I1 => Q(26),
      I2 => Q(24),
      I3 => \euros1__1_carry__5_i_3_n_0\,
      O => \euros1__1_carry__5_i_7_n_0\
    );
\euros1__1_carry__5_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(18),
      I1 => Q(25),
      I2 => Q(23),
      I3 => \euros1__1_carry__5_i_4_n_0\,
      O => \euros1__1_carry__5_i_8_n_0\
    );
\euros1__1_carry__6\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__1_carry__5_n_0\,
      CO(3) => \euros1__1_carry__6_n_0\,
      CO(2 downto 0) => \NLW_euros1__1_carry__6_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \euros1__1_carry__6_i_1_n_0\,
      DI(2) => \euros1__1_carry__6_i_2_n_0\,
      DI(1) => \euros1__1_carry__6_i_3_n_0\,
      DI(0) => \euros1__1_carry__6_i_4_n_0\,
      O(3) => \euros1__1_carry__6_n_4\,
      O(2) => \euros1__1_carry__6_n_5\,
      O(1) => \euros1__1_carry__6_n_6\,
      O(0) => \euros1__1_carry__6_n_7\,
      S(3) => \euros1__1_carry__6_i_5_n_0\,
      S(2) => \euros1__1_carry__6_i_6_n_0\,
      S(1) => \euros1__1_carry__6_i_7_n_0\,
      S(0) => \euros1__1_carry__6_i_8_n_0\
    );
\euros1__1_carry__6_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => Q(24),
      I1 => Q(29),
      O => \euros1__1_carry__6_i_1_n_0\
    );
\euros1__1_carry__6_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => Q(23),
      I1 => Q(30),
      I2 => Q(28),
      O => \euros1__1_carry__6_i_2_n_0\
    );
\euros1__1_carry__6_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => Q(22),
      I1 => Q(29),
      I2 => Q(27),
      O => \euros1__1_carry__6_i_3_n_0\
    );
\euros1__1_carry__6_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => Q(21),
      I1 => Q(28),
      I2 => Q(26),
      O => \euros1__1_carry__6_i_4_n_0\
    );
\euros1__1_carry__6_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => Q(29),
      I1 => Q(24),
      I2 => Q(30),
      I3 => Q(25),
      O => \euros1__1_carry__6_i_5_n_0\
    );
\euros1__1_carry__6_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E81717E8"
    )
        port map (
      I0 => Q(28),
      I1 => Q(30),
      I2 => Q(23),
      I3 => Q(29),
      I4 => Q(24),
      O => \euros1__1_carry__6_i_6_n_0\
    );
\euros1__1_carry__6_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__1_carry__6_i_3_n_0\,
      I1 => Q(23),
      I2 => Q(30),
      I3 => Q(28),
      O => \euros1__1_carry__6_i_7_n_0\
    );
\euros1__1_carry__6_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(22),
      I1 => Q(29),
      I2 => Q(27),
      I3 => \euros1__1_carry__6_i_4_n_0\,
      O => \euros1__1_carry__6_i_8_n_0\
    );
\euros1__1_carry__7\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__1_carry__6_n_0\,
      CO(3 downto 0) => \NLW_euros1__1_carry__7_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => Q(26),
      O(3 downto 2) => \NLW_euros1__1_carry__7_O_UNCONNECTED\(3 downto 2),
      O(1) => \euros1__1_carry__7_n_6\,
      O(0) => \euros1__1_carry__7_n_7\,
      S(3 downto 2) => B"00",
      S(1) => Q(27),
      S(0) => \euros1__1_carry__7_i_1_n_0\
    );
\euros1__1_carry__7_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => Q(30),
      I1 => Q(25),
      I2 => Q(26),
      O => \euros1__1_carry__7_i_1_n_0\
    );
\euros1__1_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(4),
      I1 => Q(2),
      O => \euros1__1_carry_i_1_n_0\
    );
\euros1__1_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(3),
      I1 => Q(1),
      O => \euros1__1_carry_i_2_n_0\
    );
\euros1__1_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(2),
      I1 => Q(0),
      O => \euros1__1_carry_i_3_n_0\
    );
\euros1__231_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \euros1__231_carry_n_0\,
      CO(2 downto 0) => \NLW_euros1__231_carry_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 1) => Q(6 downto 4),
      DI(0) => '0',
      O(3) => \euros1__231_carry_n_4\,
      O(2) => \euros1__231_carry_n_5\,
      O(1) => \euros1__231_carry_n_6\,
      O(0) => \euros1__231_carry_n_7\,
      S(3) => \euros1__231_carry_i_1_n_0\,
      S(2) => \euros1__231_carry_i_2_n_0\,
      S(1) => \euros1__231_carry_i_3_n_0\,
      S(0) => Q(3)
    );
\euros1__231_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__231_carry_n_0\,
      CO(3) => \euros1__231_carry__0_n_0\,
      CO(2 downto 0) => \NLW_euros1__231_carry__0_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \euros1__231_carry__0_i_1_n_0\,
      DI(2) => \euros1__231_carry__0_i_2_n_0\,
      DI(1) => \euros1__231_carry__0_i_3_n_0\,
      DI(0) => \euros1__231_carry__0_i_4_n_0\,
      O(3) => \euros1__231_carry__0_n_4\,
      O(2) => \euros1__231_carry__0_n_5\,
      O(1) => \euros1__231_carry__0_n_6\,
      O(0) => \euros1__231_carry__0_n_7\,
      S(3) => \euros1__231_carry__0_i_5_n_0\,
      S(2) => \euros1__231_carry__0_i_6_n_0\,
      S(1) => \euros1__231_carry__0_i_7_n_0\,
      S(0) => \euros1__231_carry__0_i_8_n_0\
    );
\euros1__231_carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => Q(5),
      I1 => Q(3),
      I2 => Q(9),
      O => \euros1__231_carry__0_i_1_n_0\
    );
\euros1__231_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => Q(2),
      I1 => Q(4),
      I2 => Q(8),
      O => \euros1__231_carry__0_i_2_n_0\
    );
\euros1__231_carry__0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => Q(1),
      I1 => Q(3),
      I2 => Q(7),
      O => \euros1__231_carry__0_i_3_n_0\
    );
\euros1__231_carry__0_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(7),
      I1 => Q(1),
      I2 => Q(3),
      O => \euros1__231_carry__0_i_4_n_0\
    );
\euros1__231_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(6),
      I1 => Q(4),
      I2 => Q(10),
      I3 => \euros1__231_carry__0_i_1_n_0\,
      O => \euros1__231_carry__0_i_5_n_0\
    );
\euros1__231_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(5),
      I1 => Q(3),
      I2 => Q(9),
      I3 => \euros1__231_carry__0_i_2_n_0\,
      O => \euros1__231_carry__0_i_6_n_0\
    );
\euros1__231_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(2),
      I1 => Q(4),
      I2 => Q(8),
      I3 => \euros1__231_carry__0_i_3_n_0\,
      O => \euros1__231_carry__0_i_7_n_0\
    );
\euros1__231_carry__0_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => Q(1),
      I1 => Q(3),
      I2 => Q(7),
      I3 => Q(2),
      I4 => Q(0),
      O => \euros1__231_carry__0_i_8_n_0\
    );
\euros1__231_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__231_carry__0_n_0\,
      CO(3) => \euros1__231_carry__1_n_0\,
      CO(2 downto 0) => \NLW_euros1__231_carry__1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \euros1__231_carry__1_i_1_n_0\,
      DI(2) => \euros1__231_carry__1_i_2_n_0\,
      DI(1) => \euros1__231_carry__1_i_3_n_0\,
      DI(0) => \euros1__231_carry__1_i_4_n_0\,
      O(3) => \euros1__231_carry__1_n_4\,
      O(2) => \euros1__231_carry__1_n_5\,
      O(1) => \euros1__231_carry__1_n_6\,
      O(0) => \euros1__231_carry__1_n_7\,
      S(3) => \euros1__231_carry__1_i_5_n_0\,
      S(2) => \euros1__231_carry__1_i_6_n_0\,
      S(1) => \euros1__231_carry__1_i_7_n_0\,
      S(0) => \euros1__231_carry__1_i_8_n_0\
    );
\euros1__231_carry__1_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => Q(7),
      I1 => Q(9),
      I2 => Q(13),
      O => \euros1__231_carry__1_i_1_n_0\
    );
\euros1__231_carry__1_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => Q(6),
      I1 => Q(8),
      I2 => Q(12),
      O => \euros1__231_carry__1_i_2_n_0\
    );
\euros1__231_carry__1_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => Q(7),
      I1 => Q(11),
      I2 => Q(5),
      O => \euros1__231_carry__1_i_3_n_0\
    );
\euros1__231_carry__1_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => Q(6),
      I1 => Q(4),
      I2 => Q(10),
      O => \euros1__231_carry__1_i_4_n_0\
    );
\euros1__231_carry__1_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(14),
      I1 => Q(10),
      I2 => Q(8),
      I3 => \euros1__231_carry__1_i_1_n_0\,
      O => \euros1__231_carry__1_i_5_n_0\
    );
\euros1__231_carry__1_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(7),
      I1 => Q(9),
      I2 => Q(13),
      I3 => \euros1__231_carry__1_i_2_n_0\,
      O => \euros1__231_carry__1_i_6_n_0\
    );
\euros1__231_carry__1_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(6),
      I1 => Q(8),
      I2 => Q(12),
      I3 => \euros1__231_carry__1_i_3_n_0\,
      O => \euros1__231_carry__1_i_7_n_0\
    );
\euros1__231_carry__1_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(7),
      I1 => Q(11),
      I2 => Q(5),
      I3 => \euros1__231_carry__1_i_4_n_0\,
      O => \euros1__231_carry__1_i_8_n_0\
    );
\euros1__231_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__231_carry__1_n_0\,
      CO(3 downto 0) => \NLW_euros1__231_carry__2_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \euros1__231_carry__2_i_1_n_0\,
      DI(1) => \euros1__231_carry__2_i_2_n_0\,
      DI(0) => \euros1__231_carry__2_i_3_n_0\,
      O(3) => \euros1__231_carry__2_n_4\,
      O(2) => \euros1__231_carry__2_n_5\,
      O(1) => \euros1__231_carry__2_n_6\,
      O(0) => \euros1__231_carry__2_n_7\,
      S(3) => \euros1__231_carry__2_i_4_n_0\,
      S(2) => \euros1__231_carry__2_i_5_n_0\,
      S(1) => \euros1__231_carry__2_i_6_n_0\,
      S(0) => \euros1__231_carry__2_i_7_n_0\
    );
\euros1__231_carry__2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => Q(10),
      I1 => Q(12),
      I2 => Q(16),
      O => \euros1__231_carry__2_i_1_n_0\
    );
\euros1__231_carry__2_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => Q(15),
      I1 => Q(11),
      I2 => Q(9),
      O => \euros1__231_carry__2_i_2_n_0\
    );
\euros1__231_carry__2_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => Q(14),
      I1 => Q(10),
      I2 => Q(8),
      O => \euros1__231_carry__2_i_3_n_0\
    );
\euros1__231_carry__2_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => Q(17),
      I1 => Q(13),
      I2 => Q(11),
      I3 => Q(12),
      I4 => Q(14),
      I5 => Q(18),
      O => \euros1__231_carry__2_i_4_n_0\
    );
\euros1__231_carry__2_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__231_carry__2_i_1_n_0\,
      I1 => Q(11),
      I2 => Q(13),
      I3 => Q(17),
      O => \euros1__231_carry__2_i_5_n_0\
    );
\euros1__231_carry__2_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(10),
      I1 => Q(12),
      I2 => Q(16),
      I3 => \euros1__231_carry__2_i_2_n_0\,
      O => \euros1__231_carry__2_i_6_n_0\
    );
\euros1__231_carry__2_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(15),
      I1 => Q(11),
      I2 => Q(9),
      I3 => \euros1__231_carry__2_i_3_n_0\,
      O => \euros1__231_carry__2_i_7_n_0\
    );
\euros1__231_carry_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => Q(0),
      I1 => Q(2),
      I2 => Q(6),
      O => \euros1__231_carry_i_1_n_0\
    );
\euros1__231_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(5),
      I1 => Q(1),
      O => \euros1__231_carry_i_2_n_0\
    );
\euros1__231_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(4),
      I1 => Q(0),
      O => \euros1__231_carry_i_3_n_0\
    );
\euros1__276_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \euros1__276_carry_n_0\,
      CO(2 downto 0) => \NLW_euros1__276_carry_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \euros1__276_carry_i_1_n_0\,
      DI(2) => \euros1__276_carry_i_2_n_0\,
      DI(1) => \euros1__276_carry_i_3_n_0\,
      DI(0) => \euros1__276_carry_i_4_n_0\,
      O(3 downto 0) => \NLW_euros1__276_carry_O_UNCONNECTED\(3 downto 0),
      S(3) => \euros1__276_carry_i_5_n_0\,
      S(2) => \euros1__276_carry_i_6_n_0\,
      S(1) => \euros1__276_carry_i_7_n_0\,
      S(0) => \euros1__276_carry_i_8_n_0\
    );
\euros1__276_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__276_carry_n_0\,
      CO(3) => \euros1__276_carry__0_n_0\,
      CO(2 downto 0) => \NLW_euros1__276_carry__0_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \euros1__276_carry__0_i_1_n_0\,
      DI(2) => \euros1__276_carry__0_i_2_n_0\,
      DI(1) => \euros1__276_carry__0_i_3_n_0\,
      DI(0) => \euros1__276_carry__0_i_4_n_0\,
      O(3 downto 0) => \NLW_euros1__276_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \euros1__276_carry__0_i_5_n_0\,
      S(2) => \euros1__276_carry__0_i_6_n_0\,
      S(1) => \euros1__276_carry__0_i_7_n_0\,
      S(0) => \euros1__276_carry__0_i_8_n_0\
    );
\euros1__276_carry__0_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BE282828"
    )
        port map (
      I0 => \euros1__1_carry__2_n_5\,
      I1 => \euros1__92_carry__0_n_4\,
      I2 => \euros1__169_carry_n_5\,
      I3 => \euros1__169_carry_n_6\,
      I4 => \euros1__92_carry__0_n_5\,
      O => \euros1__276_carry__0_i_1_n_0\
    );
\euros1__276_carry__0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BE282828"
    )
        port map (
      I0 => \euros1__1_carry__2_n_6\,
      I1 => \euros1__92_carry__0_n_5\,
      I2 => \euros1__169_carry_n_6\,
      I3 => \euros1__92_carry_n_7\,
      I4 => \euros1__92_carry__0_n_6\,
      O => \euros1__276_carry__0_i_2_n_0\
    );
\euros1__276_carry__0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"28"
    )
        port map (
      I0 => \euros1__1_carry__2_n_7\,
      I1 => \euros1__92_carry__0_n_6\,
      I2 => \euros1__92_carry_n_7\,
      O => \euros1__276_carry__0_i_3_n_0\
    );
\euros1__276_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \euros1__1_carry__1_n_4\,
      I1 => \euros1__92_carry__0_n_7\,
      O => \euros1__276_carry__0_i_4_n_0\
    );
\euros1__276_carry__0_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => \euros1__276_carry__0_i_1_n_0\,
      I1 => \euros1__1_carry__2_n_4\,
      I2 => \euros1__276_carry__0_i_9_n_0\,
      I3 => \euros1__169_carry_n_5\,
      I4 => \euros1__92_carry__0_n_4\,
      O => \euros1__276_carry__0_i_5_n_0\
    );
\euros1__276_carry__0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669966996"
    )
        port map (
      I0 => \euros1__276_carry__0_i_2_n_0\,
      I1 => \euros1__1_carry__2_n_5\,
      I2 => \euros1__92_carry__0_n_4\,
      I3 => \euros1__169_carry_n_5\,
      I4 => \euros1__169_carry_n_6\,
      I5 => \euros1__92_carry__0_n_5\,
      O => \euros1__276_carry__0_i_6_n_0\
    );
\euros1__276_carry__0_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669966996"
    )
        port map (
      I0 => \euros1__276_carry__0_i_3_n_0\,
      I1 => \euros1__1_carry__2_n_6\,
      I2 => \euros1__92_carry__0_n_5\,
      I3 => \euros1__169_carry_n_6\,
      I4 => \euros1__92_carry_n_7\,
      I5 => \euros1__92_carry__0_n_6\,
      O => \euros1__276_carry__0_i_7_n_0\
    );
\euros1__276_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__1_carry__2_n_7\,
      I1 => \euros1__92_carry__0_n_6\,
      I2 => \euros1__92_carry_n_7\,
      I3 => \euros1__276_carry__0_i_4_n_0\,
      O => \euros1__276_carry__0_i_8_n_0\
    );
\euros1__276_carry__0_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry_n_4\,
      I1 => \euros1__92_carry__1_n_7\,
      I2 => Q(0),
      O => \euros1__276_carry__0_i_9_n_0\
    );
\euros1__276_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__276_carry__0_n_0\,
      CO(3) => \euros1__276_carry__1_n_0\,
      CO(2 downto 0) => \NLW_euros1__276_carry__1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \euros1__276_carry__1_i_1_n_0\,
      DI(2) => \euros1__276_carry__1_i_2_n_0\,
      DI(1) => \euros1__276_carry__1_i_3_n_0\,
      DI(0) => \euros1__276_carry__1_i_4_n_0\,
      O(3 downto 0) => \NLW_euros1__276_carry__1_O_UNCONNECTED\(3 downto 0),
      S(3) => \euros1__276_carry__1_i_5_n_0\,
      S(2) => \euros1__276_carry__1_i_6_n_0\,
      S(1) => \euros1__276_carry__1_i_7_n_0\,
      S(0) => \euros1__276_carry__1_i_8_n_0\
    );
\euros1__276_carry__1_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => Q(2),
      I1 => \euros1__92_carry__1_n_5\,
      I2 => \euros1__169_carry__0_n_6\,
      I3 => \euros1__1_carry__3_n_5\,
      I4 => \euros1__276_carry__1_i_9_n_0\,
      O => \euros1__276_carry__1_i_1_n_0\
    );
\euros1__276_carry__1_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__0_n_6\,
      I1 => \euros1__92_carry__1_n_5\,
      I2 => Q(2),
      O => \euros1__276_carry__1_i_10_n_0\
    );
\euros1__276_carry__1_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__0_n_7\,
      I1 => \euros1__92_carry__1_n_6\,
      I2 => Q(1),
      O => \euros1__276_carry__1_i_11_n_0\
    );
\euros1__276_carry__1_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \euros1__231_carry_n_7\,
      I1 => \euros1__92_carry__1_n_4\,
      I2 => \euros1__169_carry__0_n_5\,
      O => \euros1__276_carry__1_i_12_n_0\
    );
\euros1__276_carry__1_i_13\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => Q(2),
      I1 => \euros1__92_carry__1_n_5\,
      I2 => \euros1__169_carry__0_n_6\,
      O => \euros1__276_carry__1_i_13_n_0\
    );
\euros1__276_carry__1_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => Q(1),
      I1 => \euros1__92_carry__1_n_6\,
      I2 => \euros1__169_carry__0_n_7\,
      O => \euros1__276_carry__1_i_14_n_0\
    );
\euros1__276_carry__1_i_15\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => Q(0),
      I1 => \euros1__92_carry__1_n_7\,
      I2 => \euros1__169_carry_n_4\,
      O => \euros1__276_carry__1_i_15_n_0\
    );
\euros1__276_carry__1_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => Q(1),
      I1 => \euros1__92_carry__1_n_6\,
      I2 => \euros1__169_carry__0_n_7\,
      I3 => \euros1__1_carry__3_n_6\,
      I4 => \euros1__276_carry__1_i_10_n_0\,
      O => \euros1__276_carry__1_i_2_n_0\
    );
\euros1__276_carry__1_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => Q(0),
      I1 => \euros1__92_carry__1_n_7\,
      I2 => \euros1__169_carry_n_4\,
      I3 => \euros1__1_carry__3_n_7\,
      I4 => \euros1__276_carry__1_i_11_n_0\,
      O => \euros1__276_carry__1_i_3_n_0\
    );
\euros1__276_carry__1_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EBBE822882288228"
    )
        port map (
      I0 => \euros1__1_carry__2_n_4\,
      I1 => Q(0),
      I2 => \euros1__92_carry__1_n_7\,
      I3 => \euros1__169_carry_n_4\,
      I4 => \euros1__169_carry_n_5\,
      I5 => \euros1__92_carry__0_n_4\,
      O => \euros1__276_carry__1_i_4_n_0\
    );
\euros1__276_carry__1_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__276_carry__1_i_1_n_0\,
      I1 => \euros1__276_carry__1_i_12_n_0\,
      I2 => \euros1__169_carry__0_n_4\,
      I3 => \euros1__92_carry__2_n_7\,
      I4 => \euros1__231_carry_n_6\,
      I5 => \euros1__1_carry__3_n_4\,
      O => \euros1__276_carry__1_i_5_n_0\
    );
\euros1__276_carry__1_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__276_carry__1_i_2_n_0\,
      I1 => \euros1__276_carry__1_i_13_n_0\,
      I2 => \euros1__169_carry__0_n_5\,
      I3 => \euros1__92_carry__1_n_4\,
      I4 => \euros1__231_carry_n_7\,
      I5 => \euros1__1_carry__3_n_5\,
      O => \euros1__276_carry__1_i_6_n_0\
    );
\euros1__276_carry__1_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__276_carry__1_i_3_n_0\,
      I1 => \euros1__276_carry__1_i_14_n_0\,
      I2 => \euros1__169_carry__0_n_6\,
      I3 => \euros1__92_carry__1_n_5\,
      I4 => Q(2),
      I5 => \euros1__1_carry__3_n_6\,
      O => \euros1__276_carry__1_i_7_n_0\
    );
\euros1__276_carry__1_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__276_carry__1_i_4_n_0\,
      I1 => \euros1__276_carry__1_i_15_n_0\,
      I2 => \euros1__169_carry__0_n_7\,
      I3 => \euros1__92_carry__1_n_6\,
      I4 => Q(1),
      I5 => \euros1__1_carry__3_n_7\,
      O => \euros1__276_carry__1_i_8_n_0\
    );
\euros1__276_carry__1_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__0_n_5\,
      I1 => \euros1__92_carry__1_n_4\,
      I2 => \euros1__231_carry_n_7\,
      O => \euros1__276_carry__1_i_9_n_0\
    );
\euros1__276_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__276_carry__1_n_0\,
      CO(3) => \euros1__276_carry__2_n_0\,
      CO(2 downto 0) => \NLW_euros1__276_carry__2_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \euros1__276_carry__2_i_1_n_0\,
      DI(2) => \euros1__276_carry__2_i_2_n_0\,
      DI(1) => \euros1__276_carry__2_i_3_n_0\,
      DI(0) => \euros1__276_carry__2_i_4_n_0\,
      O(3 downto 0) => \NLW_euros1__276_carry__2_O_UNCONNECTED\(3 downto 0),
      S(3) => \euros1__276_carry__2_i_5_n_0\,
      S(2) => \euros1__276_carry__2_i_6_n_0\,
      S(1) => \euros1__276_carry__2_i_7_n_0\,
      S(0) => \euros1__276_carry__2_i_8_n_0\
    );
\euros1__276_carry__2_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \euros1__1_carry__4_n_5\,
      I1 => \euros1__276_carry__2_i_9_n_0\,
      I2 => \euros1__231_carry_n_4\,
      I3 => \euros1__92_carry__2_n_5\,
      I4 => \euros1__169_carry__1_n_6\,
      O => \euros1__276_carry__2_i_1_n_0\
    );
\euros1__276_carry__2_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__1_n_6\,
      I1 => \euros1__92_carry__2_n_5\,
      I2 => \euros1__231_carry_n_4\,
      O => \euros1__276_carry__2_i_10_n_0\
    );
\euros1__276_carry__2_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__1_n_7\,
      I1 => \euros1__92_carry__2_n_6\,
      I2 => \euros1__231_carry_n_5\,
      O => \euros1__276_carry__2_i_11_n_0\
    );
\euros1__276_carry__2_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__0_n_4\,
      I1 => \euros1__92_carry__2_n_7\,
      I2 => \euros1__231_carry_n_6\,
      O => \euros1__276_carry__2_i_12_n_0\
    );
\euros1__276_carry__2_i_13\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \euros1__231_carry__0_n_7\,
      I1 => \euros1__92_carry__2_n_4\,
      I2 => \euros1__169_carry__1_n_5\,
      O => \euros1__276_carry__2_i_13_n_0\
    );
\euros1__276_carry__2_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \euros1__231_carry_n_5\,
      I1 => \euros1__92_carry__2_n_6\,
      I2 => \euros1__169_carry__1_n_7\,
      O => \euros1__276_carry__2_i_14_n_0\
    );
\euros1__276_carry__2_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \euros1__231_carry_n_5\,
      I1 => \euros1__92_carry__2_n_6\,
      I2 => \euros1__169_carry__1_n_7\,
      I3 => \euros1__1_carry__4_n_6\,
      I4 => \euros1__276_carry__2_i_10_n_0\,
      O => \euros1__276_carry__2_i_2_n_0\
    );
\euros1__276_carry__2_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \euros1__231_carry_n_6\,
      I1 => \euros1__92_carry__2_n_7\,
      I2 => \euros1__169_carry__0_n_4\,
      I3 => \euros1__276_carry__2_i_11_n_0\,
      I4 => \euros1__1_carry__4_n_7\,
      O => \euros1__276_carry__2_i_3_n_0\
    );
\euros1__276_carry__2_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \euros1__231_carry_n_7\,
      I1 => \euros1__92_carry__1_n_4\,
      I2 => \euros1__169_carry__0_n_5\,
      I3 => \euros1__1_carry__3_n_4\,
      I4 => \euros1__276_carry__2_i_12_n_0\,
      O => \euros1__276_carry__2_i_4_n_0\
    );
\euros1__276_carry__2_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__276_carry__2_i_1_n_0\,
      I1 => \euros1__276_carry__2_i_13_n_0\,
      I2 => \euros1__169_carry__1_n_4\,
      I3 => \euros1__92_carry__3_n_7\,
      I4 => \euros1__231_carry__0_n_6\,
      I5 => \euros1__1_carry__4_n_4\,
      O => \euros1__276_carry__2_i_5_n_0\
    );
\euros1__276_carry__2_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \euros1__276_carry__2_i_2_n_0\,
      I1 => \euros1__1_carry__4_n_5\,
      I2 => \euros1__276_carry__2_i_9_n_0\,
      I3 => \euros1__231_carry_n_4\,
      I4 => \euros1__92_carry__2_n_5\,
      I5 => \euros1__169_carry__1_n_6\,
      O => \euros1__276_carry__2_i_6_n_0\
    );
\euros1__276_carry__2_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__276_carry__2_i_3_n_0\,
      I1 => \euros1__276_carry__2_i_14_n_0\,
      I2 => \euros1__169_carry__1_n_6\,
      I3 => \euros1__92_carry__2_n_5\,
      I4 => \euros1__231_carry_n_4\,
      I5 => \euros1__1_carry__4_n_6\,
      O => \euros1__276_carry__2_i_7_n_0\
    );
\euros1__276_carry__2_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \euros1__276_carry__2_i_4_n_0\,
      I1 => \euros1__1_carry__4_n_7\,
      I2 => \euros1__276_carry__2_i_11_n_0\,
      I3 => \euros1__231_carry_n_6\,
      I4 => \euros1__92_carry__2_n_7\,
      I5 => \euros1__169_carry__0_n_4\,
      O => \euros1__276_carry__2_i_8_n_0\
    );
\euros1__276_carry__2_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__1_n_5\,
      I1 => \euros1__92_carry__2_n_4\,
      I2 => \euros1__231_carry__0_n_7\,
      O => \euros1__276_carry__2_i_9_n_0\
    );
\euros1__276_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__276_carry__2_n_0\,
      CO(3) => \euros1__276_carry__3_n_0\,
      CO(2 downto 0) => \NLW_euros1__276_carry__3_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \euros1__276_carry__3_i_1_n_0\,
      DI(2) => \euros1__276_carry__3_i_2_n_0\,
      DI(1) => \euros1__276_carry__3_i_3_n_0\,
      DI(0) => \euros1__276_carry__3_i_4_n_0\,
      O(3 downto 0) => \NLW_euros1__276_carry__3_O_UNCONNECTED\(3 downto 0),
      S(3) => \euros1__276_carry__3_i_5_n_0\,
      S(2) => \euros1__276_carry__3_i_6_n_0\,
      S(1) => \euros1__276_carry__3_i_7_n_0\,
      S(0) => \euros1__276_carry__3_i_8_n_0\
    );
\euros1__276_carry__3_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \euros1__231_carry__0_n_4\,
      I1 => \euros1__92_carry__3_n_5\,
      I2 => \euros1__169_carry__2_n_6\,
      I3 => \euros1__1_carry__5_n_5\,
      I4 => \euros1__276_carry__3_i_9_n_0\,
      O => \euros1__276_carry__3_i_1_n_0\
    );
\euros1__276_carry__3_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__2_n_6\,
      I1 => \euros1__92_carry__3_n_5\,
      I2 => \euros1__231_carry__0_n_4\,
      O => \euros1__276_carry__3_i_10_n_0\
    );
\euros1__276_carry__3_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__2_n_7\,
      I1 => \euros1__92_carry__3_n_6\,
      I2 => \euros1__231_carry__0_n_5\,
      O => \euros1__276_carry__3_i_11_n_0\
    );
\euros1__276_carry__3_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__1_n_4\,
      I1 => \euros1__92_carry__3_n_7\,
      I2 => \euros1__231_carry__0_n_6\,
      O => \euros1__276_carry__3_i_12_n_0\
    );
\euros1__276_carry__3_i_13\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \euros1__231_carry__1_n_7\,
      I1 => \euros1__92_carry__3_n_4\,
      I2 => \euros1__169_carry__2_n_5\,
      O => \euros1__276_carry__3_i_13_n_0\
    );
\euros1__276_carry__3_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \euros1__231_carry__0_n_4\,
      I1 => \euros1__92_carry__3_n_5\,
      I2 => \euros1__169_carry__2_n_6\,
      O => \euros1__276_carry__3_i_14_n_0\
    );
\euros1__276_carry__3_i_15\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \euros1__231_carry__0_n_5\,
      I1 => \euros1__92_carry__3_n_6\,
      I2 => \euros1__169_carry__2_n_7\,
      O => \euros1__276_carry__3_i_15_n_0\
    );
\euros1__276_carry__3_i_16\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \euros1__231_carry__0_n_6\,
      I1 => \euros1__92_carry__3_n_7\,
      I2 => \euros1__169_carry__1_n_4\,
      O => \euros1__276_carry__3_i_16_n_0\
    );
\euros1__276_carry__3_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \euros1__231_carry__0_n_5\,
      I1 => \euros1__92_carry__3_n_6\,
      I2 => \euros1__169_carry__2_n_7\,
      I3 => \euros1__1_carry__5_n_6\,
      I4 => \euros1__276_carry__3_i_10_n_0\,
      O => \euros1__276_carry__3_i_2_n_0\
    );
\euros1__276_carry__3_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \euros1__231_carry__0_n_6\,
      I1 => \euros1__92_carry__3_n_7\,
      I2 => \euros1__169_carry__1_n_4\,
      I3 => \euros1__1_carry__5_n_7\,
      I4 => \euros1__276_carry__3_i_11_n_0\,
      O => \euros1__276_carry__3_i_3_n_0\
    );
\euros1__276_carry__3_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \euros1__231_carry__0_n_7\,
      I1 => \euros1__92_carry__2_n_4\,
      I2 => \euros1__169_carry__1_n_5\,
      I3 => \euros1__1_carry__4_n_4\,
      I4 => \euros1__276_carry__3_i_12_n_0\,
      O => \euros1__276_carry__3_i_4_n_0\
    );
\euros1__276_carry__3_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__276_carry__3_i_1_n_0\,
      I1 => \euros1__276_carry__3_i_13_n_0\,
      I2 => \euros1__169_carry__2_n_4\,
      I3 => \euros1__92_carry__4_n_7\,
      I4 => \euros1__231_carry__1_n_6\,
      I5 => \euros1__1_carry__5_n_4\,
      O => \euros1__276_carry__3_i_5_n_0\
    );
\euros1__276_carry__3_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__276_carry__3_i_2_n_0\,
      I1 => \euros1__276_carry__3_i_14_n_0\,
      I2 => \euros1__169_carry__2_n_5\,
      I3 => \euros1__92_carry__3_n_4\,
      I4 => \euros1__231_carry__1_n_7\,
      I5 => \euros1__1_carry__5_n_5\,
      O => \euros1__276_carry__3_i_6_n_0\
    );
\euros1__276_carry__3_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__276_carry__3_i_3_n_0\,
      I1 => \euros1__276_carry__3_i_15_n_0\,
      I2 => \euros1__169_carry__2_n_6\,
      I3 => \euros1__92_carry__3_n_5\,
      I4 => \euros1__231_carry__0_n_4\,
      I5 => \euros1__1_carry__5_n_6\,
      O => \euros1__276_carry__3_i_7_n_0\
    );
\euros1__276_carry__3_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__276_carry__3_i_4_n_0\,
      I1 => \euros1__276_carry__3_i_16_n_0\,
      I2 => \euros1__169_carry__2_n_7\,
      I3 => \euros1__92_carry__3_n_6\,
      I4 => \euros1__231_carry__0_n_5\,
      I5 => \euros1__1_carry__5_n_7\,
      O => \euros1__276_carry__3_i_8_n_0\
    );
\euros1__276_carry__3_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__2_n_5\,
      I1 => \euros1__92_carry__3_n_4\,
      I2 => \euros1__231_carry__1_n_7\,
      O => \euros1__276_carry__3_i_9_n_0\
    );
\euros1__276_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__276_carry__3_n_0\,
      CO(3) => \euros1__276_carry__4_n_0\,
      CO(2 downto 0) => \NLW_euros1__276_carry__4_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \euros1__276_carry__4_i_1_n_0\,
      DI(2) => \euros1__276_carry__4_i_2_n_0\,
      DI(1) => \euros1__276_carry__4_i_3_n_0\,
      DI(0) => \euros1__276_carry__4_i_4_n_0\,
      O(3) => \euros1__276_carry__4_n_4\,
      O(2) => \euros1__276_carry__4_n_5\,
      O(1) => \euros1__276_carry__4_n_6\,
      O(0) => \euros1__276_carry__4_n_7\,
      S(3) => \euros1__276_carry__4_i_5_n_0\,
      S(2) => \euros1__276_carry__4_i_6_n_0\,
      S(1) => \euros1__276_carry__4_i_7_n_0\,
      S(0) => \euros1__276_carry__4_i_8_n_0\
    );
\euros1__276_carry__4_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \euros1__1_carry__6_n_5\,
      I1 => \euros1__276_carry__4_i_9_n_0\,
      I2 => \euros1__231_carry__1_n_4\,
      I3 => \euros1__92_carry__4_n_5\,
      I4 => \euros1__169_carry__3_n_6\,
      O => \euros1__276_carry__4_i_1_n_0\
    );
\euros1__276_carry__4_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__3_n_6\,
      I1 => \euros1__92_carry__4_n_5\,
      I2 => \euros1__231_carry__1_n_4\,
      O => \euros1__276_carry__4_i_10_n_0\
    );
\euros1__276_carry__4_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__3_n_7\,
      I1 => \euros1__92_carry__4_n_6\,
      I2 => \euros1__231_carry__1_n_5\,
      O => \euros1__276_carry__4_i_11_n_0\
    );
\euros1__276_carry__4_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__2_n_4\,
      I1 => \euros1__92_carry__4_n_7\,
      I2 => \euros1__231_carry__1_n_6\,
      O => \euros1__276_carry__4_i_12_n_0\
    );
\euros1__276_carry__4_i_13\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \euros1__231_carry__2_n_7\,
      I1 => \euros1__92_carry__4_n_4\,
      I2 => \euros1__169_carry__3_n_5\,
      O => \euros1__276_carry__4_i_13_n_0\
    );
\euros1__276_carry__4_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \euros1__231_carry__1_n_5\,
      I1 => \euros1__92_carry__4_n_6\,
      I2 => \euros1__169_carry__3_n_7\,
      O => \euros1__276_carry__4_i_14_n_0\
    );
\euros1__276_carry__4_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \euros1__231_carry__1_n_5\,
      I1 => \euros1__92_carry__4_n_6\,
      I2 => \euros1__169_carry__3_n_7\,
      I3 => \euros1__1_carry__6_n_6\,
      I4 => \euros1__276_carry__4_i_10_n_0\,
      O => \euros1__276_carry__4_i_2_n_0\
    );
\euros1__276_carry__4_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \euros1__1_carry__6_n_7\,
      I1 => \euros1__276_carry__4_i_11_n_0\,
      I2 => \euros1__231_carry__1_n_6\,
      I3 => \euros1__92_carry__4_n_7\,
      I4 => \euros1__169_carry__2_n_4\,
      O => \euros1__276_carry__4_i_3_n_0\
    );
\euros1__276_carry__4_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \euros1__231_carry__1_n_7\,
      I1 => \euros1__92_carry__3_n_4\,
      I2 => \euros1__169_carry__2_n_5\,
      I3 => \euros1__1_carry__5_n_4\,
      I4 => \euros1__276_carry__4_i_12_n_0\,
      O => \euros1__276_carry__4_i_4_n_0\
    );
\euros1__276_carry__4_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__276_carry__4_i_1_n_0\,
      I1 => \euros1__276_carry__4_i_13_n_0\,
      I2 => \euros1__169_carry__3_n_4\,
      I3 => \euros1__92_carry__5_n_7\,
      I4 => \euros1__231_carry__2_n_6\,
      I5 => \euros1__1_carry__6_n_4\,
      O => \euros1__276_carry__4_i_5_n_0\
    );
\euros1__276_carry__4_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \euros1__276_carry__4_i_2_n_0\,
      I1 => \euros1__1_carry__6_n_5\,
      I2 => \euros1__276_carry__4_i_9_n_0\,
      I3 => \euros1__231_carry__1_n_4\,
      I4 => \euros1__92_carry__4_n_5\,
      I5 => \euros1__169_carry__3_n_6\,
      O => \euros1__276_carry__4_i_6_n_0\
    );
\euros1__276_carry__4_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__276_carry__4_i_3_n_0\,
      I1 => \euros1__276_carry__4_i_14_n_0\,
      I2 => \euros1__169_carry__3_n_6\,
      I3 => \euros1__92_carry__4_n_5\,
      I4 => \euros1__231_carry__1_n_4\,
      I5 => \euros1__1_carry__6_n_6\,
      O => \euros1__276_carry__4_i_7_n_0\
    );
\euros1__276_carry__4_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \euros1__276_carry__4_i_4_n_0\,
      I1 => \euros1__1_carry__6_n_7\,
      I2 => \euros1__276_carry__4_i_11_n_0\,
      I3 => \euros1__231_carry__1_n_6\,
      I4 => \euros1__92_carry__4_n_7\,
      I5 => \euros1__169_carry__2_n_4\,
      O => \euros1__276_carry__4_i_8_n_0\
    );
\euros1__276_carry__4_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__3_n_5\,
      I1 => \euros1__92_carry__4_n_4\,
      I2 => \euros1__231_carry__2_n_7\,
      O => \euros1__276_carry__4_i_9_n_0\
    );
\euros1__276_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__276_carry__4_n_0\,
      CO(3 downto 0) => \NLW_euros1__276_carry__5_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \euros1__276_carry__5_i_1_n_0\,
      O(3 downto 2) => \NLW_euros1__276_carry__5_O_UNCONNECTED\(3 downto 2),
      O(1) => \euros1__276_carry__5_n_6\,
      O(0) => \euros1__276_carry__5_n_7\,
      S(3 downto 2) => B"00",
      S(1) => \euros1__276_carry__5_i_2_n_0\,
      S(0) => \euros1__276_carry__5_i_3_n_0\
    );
\euros1__276_carry__5_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \euros1__231_carry__2_n_7\,
      I1 => \euros1__92_carry__4_n_4\,
      I2 => \euros1__169_carry__3_n_5\,
      I3 => \euros1__1_carry__6_n_4\,
      I4 => \euros1__276_carry__5_i_4_n_0\,
      O => \euros1__276_carry__5_i_1_n_0\
    );
\euros1__276_carry__5_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E8818117177E7EE8"
    )
        port map (
      I0 => \euros1__276_carry__5_i_5_n_0\,
      I1 => \euros1__1_carry__7_n_7\,
      I2 => \euros1__169_carry__4_n_7\,
      I3 => \euros1__92_carry__5_n_6\,
      I4 => \euros1__231_carry__2_n_5\,
      I5 => \euros1__276_carry__5_i_6_n_0\,
      O => \euros1__276_carry__5_i_2_n_0\
    );
\euros1__276_carry__5_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \euros1__276_carry__5_i_1_n_0\,
      I1 => \euros1__1_carry__7_n_7\,
      I2 => \euros1__276_carry__5_i_7_n_0\,
      I3 => \euros1__231_carry__2_n_6\,
      I4 => \euros1__92_carry__5_n_7\,
      I5 => \euros1__169_carry__3_n_4\,
      O => \euros1__276_carry__5_i_3_n_0\
    );
\euros1__276_carry__5_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__3_n_4\,
      I1 => \euros1__92_carry__5_n_7\,
      I2 => \euros1__231_carry__2_n_6\,
      O => \euros1__276_carry__5_i_4_n_0\
    );
\euros1__276_carry__5_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \euros1__231_carry__2_n_6\,
      I1 => \euros1__92_carry__5_n_7\,
      I2 => \euros1__169_carry__3_n_4\,
      O => \euros1__276_carry__5_i_5_n_0\
    );
\euros1__276_carry__5_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__169_carry__4_n_6\,
      I1 => \euros1__231_carry__2_n_4\,
      I2 => \euros1__1_carry__7_n_6\,
      I3 => \euros1__92_carry__5_n_5\,
      O => \euros1__276_carry__5_i_6_n_0\
    );
\euros1__276_carry__5_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__4_n_7\,
      I1 => \euros1__92_carry__5_n_6\,
      I2 => \euros1__231_carry__2_n_5\,
      O => \euros1__276_carry__5_i_7_n_0\
    );
\euros1__276_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \euros1__1_carry__1_n_5\,
      I1 => \euros1__92_carry_n_4\,
      O => \euros1__276_carry_i_1_n_0\
    );
\euros1__276_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \euros1__1_carry__1_n_6\,
      I1 => \euros1__92_carry_n_5\,
      O => \euros1__276_carry_i_2_n_0\
    );
\euros1__276_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \euros1__1_carry__1_n_7\,
      I1 => \euros1__92_carry_n_6\,
      O => \euros1__276_carry_i_3_n_0\
    );
\euros1__276_carry_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => Q(0),
      I1 => \euros1__1_carry__0_n_4\,
      O => \euros1__276_carry_i_4_n_0\
    );
\euros1__276_carry_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => \euros1__1_carry__1_n_4\,
      I1 => \euros1__92_carry__0_n_7\,
      I2 => \euros1__92_carry_n_4\,
      I3 => \euros1__1_carry__1_n_5\,
      O => \euros1__276_carry_i_5_n_0\
    );
\euros1__276_carry_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \euros1__92_carry_n_5\,
      I1 => \euros1__1_carry__1_n_6\,
      I2 => \euros1__92_carry_n_4\,
      I3 => \euros1__1_carry__1_n_5\,
      O => \euros1__276_carry_i_6_n_0\
    );
\euros1__276_carry_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \euros1__92_carry_n_6\,
      I1 => \euros1__1_carry__1_n_7\,
      I2 => \euros1__92_carry_n_5\,
      I3 => \euros1__1_carry__1_n_6\,
      O => \euros1__276_carry_i_7_n_0\
    );
\euros1__276_carry_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \euros1__1_carry__0_n_4\,
      I1 => Q(0),
      I2 => \euros1__92_carry_n_6\,
      I3 => \euros1__1_carry__1_n_7\,
      O => \euros1__276_carry_i_8_n_0\
    );
\euros1__333_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3 downto 0) => \NLW_euros1__333_carry_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \euros1__333_carry_i_1_n_0\,
      DI(0) => '0',
      O(3) => \NLW_euros1__333_carry_O_UNCONNECTED\(3),
      O(2) => \euros1__333_carry_n_5\,
      O(1) => \euros1__333_carry_n_6\,
      O(0) => \euros1__333_carry_n_7\,
      S(3) => '0',
      S(2) => \euros1__333_carry_i_2_n_0\,
      S(1) => \euros1__333_carry_i_3_n_0\,
      S(0) => \euros1__333_carry_i_4_n_0\
    );
\euros1__333_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \euros1__276_carry__4_n_4\,
      I1 => \euros1__276_carry__4_n_7\,
      O => \euros1__333_carry_i_1_n_0\
    );
\euros1__333_carry_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66969969"
    )
        port map (
      I0 => \euros1__276_carry__5_n_6\,
      I1 => \euros1__276_carry__4_n_5\,
      I2 => \euros1__276_carry__5_n_7\,
      I3 => \euros1__276_carry__4_n_6\,
      I4 => \euros1__276_carry__4_n_7\,
      O => \euros1__333_carry_i_2_n_0\
    );
\euros1__333_carry_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => \euros1__276_carry__4_n_7\,
      I1 => \euros1__276_carry__4_n_4\,
      I2 => \euros1__276_carry__5_n_7\,
      I3 => \euros1__276_carry__4_n_6\,
      O => \euros1__333_carry_i_3_n_0\
    );
\euros1__333_carry_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \euros1__276_carry__4_n_4\,
      I1 => \euros1__276_carry__4_n_7\,
      O => \euros1__333_carry_i_4_n_0\
    );
\euros1__339_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \euros1__339_carry_n_0\,
      CO(2 downto 0) => \NLW_euros1__339_carry_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '1',
      DI(3 downto 0) => Q(3 downto 0),
      O(3) => \euros1__339_carry_n_4\,
      O(2) => \euros1__339_carry_n_5\,
      O(1 downto 0) => p_1_in(1 downto 0),
      S(3) => \euros1__339_carry_i_1_n_0\,
      S(2) => \euros1__339_carry_i_2_n_0\,
      S(1) => \euros1__339_carry_i_3_n_0\,
      S(0) => \euros1__339_carry_i_4_n_0\
    );
\euros1__339_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__339_carry_n_0\,
      CO(3 downto 0) => \NLW_euros1__339_carry__0_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => Q(6 downto 4),
      O(3) => \euros1__339_carry__0_n_4\,
      O(2) => \euros1__339_carry__0_n_5\,
      O(1) => \euros1__339_carry__0_n_6\,
      O(0) => \euros1__339_carry__0_n_7\,
      S(3) => \euros1__339_carry__0_i_1_n_0\,
      S(2) => \euros1__339_carry__0_i_2_n_0\,
      S(1) => \euros1__339_carry__0_i_3_n_0\,
      S(0) => \euros1__339_carry__0_i_4_n_0\
    );
\euros1__339_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(7),
      I1 => \euros1__333_carry_n_5\,
      O => \euros1__339_carry__0_i_1_n_0\
    );
\euros1__339_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(6),
      I1 => \euros1__333_carry_n_6\,
      O => \euros1__339_carry__0_i_2_n_0\
    );
\euros1__339_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(5),
      I1 => \euros1__333_carry_n_7\,
      O => \euros1__339_carry__0_i_3_n_0\
    );
\euros1__339_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(4),
      I1 => \euros1__276_carry__4_n_5\,
      O => \euros1__339_carry__0_i_4_n_0\
    );
\euros1__339_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(3),
      I1 => \euros1__276_carry__4_n_6\,
      O => \euros1__339_carry_i_1_n_0\
    );
\euros1__339_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(2),
      I1 => \euros1__276_carry__4_n_7\,
      O => \euros1__339_carry_i_2_n_0\
    );
\euros1__339_carry_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => Q(1),
      O => \euros1__339_carry_i_3_n_0\
    );
\euros1__339_carry_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => Q(0),
      O => \euros1__339_carry_i_4_n_0\
    );
\euros1__92_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \euros1__92_carry_n_0\,
      CO(2 downto 0) => \NLW_euros1__92_carry_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \euros1__92_carry_i_1_n_0\,
      DI(2) => Q(0),
      DI(1 downto 0) => B"01",
      O(3) => \euros1__92_carry_n_4\,
      O(2) => \euros1__92_carry_n_5\,
      O(1) => \euros1__92_carry_n_6\,
      O(0) => \euros1__92_carry_n_7\,
      S(3) => \euros1__92_carry_i_2_n_0\,
      S(2) => \euros1__92_carry_i_3_n_0\,
      S(1) => \euros1__92_carry_i_4_n_0\,
      S(0) => Q(0)
    );
\euros1__92_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__92_carry_n_0\,
      CO(3) => \euros1__92_carry__0_n_0\,
      CO(2 downto 0) => \NLW_euros1__92_carry__0_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \euros1__92_carry__0_i_1_n_0\,
      DI(2) => \euros1__92_carry__0_i_2_n_0\,
      DI(1) => \euros1__92_carry__0_i_3_n_0\,
      DI(0) => \euros1__92_carry__0_i_4_n_0\,
      O(3) => \euros1__92_carry__0_n_4\,
      O(2) => \euros1__92_carry__0_n_5\,
      O(1) => \euros1__92_carry__0_n_6\,
      O(0) => \euros1__92_carry__0_n_7\,
      S(3) => \euros1__92_carry__0_i_5_n_0\,
      S(2) => \euros1__92_carry__0_i_6_n_0\,
      S(1) => \euros1__92_carry__0_i_7_n_0\,
      S(0) => \euros1__92_carry__0_i_8_n_0\
    );
\euros1__92_carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => Q(2),
      I1 => Q(4),
      I2 => Q(6),
      O => \euros1__92_carry__0_i_1_n_0\
    );
\euros1__92_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => Q(1),
      I1 => Q(3),
      I2 => Q(5),
      O => \euros1__92_carry__0_i_2_n_0\
    );
\euros1__92_carry__0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => Q(0),
      I1 => Q(4),
      I2 => Q(2),
      O => \euros1__92_carry__0_i_3_n_0\
    );
\euros1__92_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => Q(1),
      I1 => Q(3),
      O => \euros1__92_carry__0_i_4_n_0\
    );
\euros1__92_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(5),
      I1 => Q(3),
      I2 => Q(7),
      I3 => \euros1__92_carry__0_i_1_n_0\,
      O => \euros1__92_carry__0_i_5_n_0\
    );
\euros1__92_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(2),
      I1 => Q(4),
      I2 => Q(6),
      I3 => \euros1__92_carry__0_i_2_n_0\,
      O => \euros1__92_carry__0_i_6_n_0\
    );
\euros1__92_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(1),
      I1 => Q(3),
      I2 => Q(5),
      I3 => \euros1__92_carry__0_i_3_n_0\,
      O => \euros1__92_carry__0_i_7_n_0\
    );
\euros1__92_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(0),
      I1 => Q(4),
      I2 => Q(2),
      I3 => \euros1__92_carry__0_i_4_n_0\,
      O => \euros1__92_carry__0_i_8_n_0\
    );
\euros1__92_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__92_carry__0_n_0\,
      CO(3) => \euros1__92_carry__1_n_0\,
      CO(2 downto 0) => \NLW_euros1__92_carry__1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \euros1__92_carry__1_i_1_n_0\,
      DI(2) => \euros1__92_carry__1_i_2_n_0\,
      DI(1) => \euros1__92_carry__1_i_3_n_0\,
      DI(0) => \euros1__92_carry__1_i_4_n_0\,
      O(3) => \euros1__92_carry__1_n_4\,
      O(2) => \euros1__92_carry__1_n_5\,
      O(1) => \euros1__92_carry__1_n_6\,
      O(0) => \euros1__92_carry__1_n_7\,
      S(3) => \euros1__92_carry__1_i_5_n_0\,
      S(2) => \euros1__92_carry__1_i_6_n_0\,
      S(1) => \euros1__92_carry__1_i_7_n_0\,
      S(0) => \euros1__92_carry__1_i_8_n_0\
    );
\euros1__92_carry__1_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => Q(6),
      I1 => Q(10),
      I2 => Q(8),
      O => \euros1__92_carry__1_i_1_n_0\
    );
\euros1__92_carry__1_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4D"
    )
        port map (
      I0 => Q(7),
      I1 => Q(5),
      I2 => Q(9),
      O => \euros1__92_carry__1_i_2_n_0\
    );
\euros1__92_carry__1_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => Q(4),
      I1 => Q(8),
      I2 => Q(6),
      O => \euros1__92_carry__1_i_3_n_0\
    );
\euros1__92_carry__1_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4D"
    )
        port map (
      I0 => Q(5),
      I1 => Q(3),
      I2 => Q(7),
      O => \euros1__92_carry__1_i_4_n_0\
    );
\euros1__92_carry__1_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(9),
      I1 => Q(11),
      I2 => Q(7),
      I3 => \euros1__92_carry__1_i_1_n_0\,
      O => \euros1__92_carry__1_i_5_n_0\
    );
\euros1__92_carry__1_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(6),
      I1 => Q(10),
      I2 => Q(8),
      I3 => \euros1__92_carry__1_i_2_n_0\,
      O => \euros1__92_carry__1_i_6_n_0\
    );
\euros1__92_carry__1_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(7),
      I1 => Q(5),
      I2 => Q(9),
      I3 => \euros1__92_carry__1_i_3_n_0\,
      O => \euros1__92_carry__1_i_7_n_0\
    );
\euros1__92_carry__1_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(4),
      I1 => Q(8),
      I2 => Q(6),
      I3 => \euros1__92_carry__1_i_4_n_0\,
      O => \euros1__92_carry__1_i_8_n_0\
    );
\euros1__92_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__92_carry__1_n_0\,
      CO(3) => \euros1__92_carry__2_n_0\,
      CO(2 downto 0) => \NLW_euros1__92_carry__2_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \euros1__92_carry__2_i_1_n_0\,
      DI(2) => \euros1__92_carry__2_i_2_n_0\,
      DI(1) => \euros1__92_carry__2_i_3_n_0\,
      DI(0) => \euros1__92_carry__2_i_4_n_0\,
      O(3) => \euros1__92_carry__2_n_4\,
      O(2) => \euros1__92_carry__2_n_5\,
      O(1) => \euros1__92_carry__2_n_6\,
      O(0) => \euros1__92_carry__2_n_7\,
      S(3) => \euros1__92_carry__2_i_5_n_0\,
      S(2) => \euros1__92_carry__2_i_6_n_0\,
      S(1) => \euros1__92_carry__2_i_7_n_0\,
      S(0) => \euros1__92_carry__2_i_8_n_0\
    );
\euros1__92_carry__2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => Q(10),
      I1 => Q(14),
      I2 => Q(12),
      O => \euros1__92_carry__2_i_1_n_0\
    );
\euros1__92_carry__2_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => Q(9),
      I1 => Q(13),
      I2 => Q(11),
      O => \euros1__92_carry__2_i_2_n_0\
    );
\euros1__92_carry__2_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => Q(8),
      I1 => Q(12),
      I2 => Q(10),
      O => \euros1__92_carry__2_i_3_n_0\
    );
\euros1__92_carry__2_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => Q(9),
      I1 => Q(11),
      I2 => Q(7),
      O => \euros1__92_carry__2_i_4_n_0\
    );
\euros1__92_carry__2_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(11),
      I1 => Q(15),
      I2 => Q(13),
      I3 => \euros1__92_carry__2_i_1_n_0\,
      O => \euros1__92_carry__2_i_5_n_0\
    );
\euros1__92_carry__2_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(10),
      I1 => Q(14),
      I2 => Q(12),
      I3 => \euros1__92_carry__2_i_2_n_0\,
      O => \euros1__92_carry__2_i_6_n_0\
    );
\euros1__92_carry__2_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(9),
      I1 => Q(13),
      I2 => Q(11),
      I3 => \euros1__92_carry__2_i_3_n_0\,
      O => \euros1__92_carry__2_i_7_n_0\
    );
\euros1__92_carry__2_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(8),
      I1 => Q(12),
      I2 => Q(10),
      I3 => \euros1__92_carry__2_i_4_n_0\,
      O => \euros1__92_carry__2_i_8_n_0\
    );
\euros1__92_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__92_carry__2_n_0\,
      CO(3) => \euros1__92_carry__3_n_0\,
      CO(2 downto 0) => \NLW_euros1__92_carry__3_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \euros1__92_carry__3_i_1_n_0\,
      DI(2) => \euros1__92_carry__3_i_2_n_0\,
      DI(1) => \euros1__92_carry__3_i_3_n_0\,
      DI(0) => \euros1__92_carry__3_i_4_n_0\,
      O(3) => \euros1__92_carry__3_n_4\,
      O(2) => \euros1__92_carry__3_n_5\,
      O(1) => \euros1__92_carry__3_n_6\,
      O(0) => \euros1__92_carry__3_n_7\,
      S(3) => \euros1__92_carry__3_i_5_n_0\,
      S(2) => \euros1__92_carry__3_i_6_n_0\,
      S(1) => \euros1__92_carry__3_i_7_n_0\,
      S(0) => \euros1__92_carry__3_i_8_n_0\
    );
\euros1__92_carry__3_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => Q(14),
      I1 => Q(18),
      I2 => Q(16),
      O => \euros1__92_carry__3_i_1_n_0\
    );
\euros1__92_carry__3_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => Q(13),
      I1 => Q(17),
      I2 => Q(15),
      O => \euros1__92_carry__3_i_2_n_0\
    );
\euros1__92_carry__3_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => Q(12),
      I1 => Q(16),
      I2 => Q(14),
      O => \euros1__92_carry__3_i_3_n_0\
    );
\euros1__92_carry__3_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => Q(11),
      I1 => Q(15),
      I2 => Q(13),
      O => \euros1__92_carry__3_i_4_n_0\
    );
\euros1__92_carry__3_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(15),
      I1 => Q(19),
      I2 => Q(17),
      I3 => \euros1__92_carry__3_i_1_n_0\,
      O => \euros1__92_carry__3_i_5_n_0\
    );
\euros1__92_carry__3_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(14),
      I1 => Q(18),
      I2 => Q(16),
      I3 => \euros1__92_carry__3_i_2_n_0\,
      O => \euros1__92_carry__3_i_6_n_0\
    );
\euros1__92_carry__3_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(13),
      I1 => Q(17),
      I2 => Q(15),
      I3 => \euros1__92_carry__3_i_3_n_0\,
      O => \euros1__92_carry__3_i_7_n_0\
    );
\euros1__92_carry__3_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(12),
      I1 => Q(16),
      I2 => Q(14),
      I3 => \euros1__92_carry__3_i_4_n_0\,
      O => \euros1__92_carry__3_i_8_n_0\
    );
\euros1__92_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__92_carry__3_n_0\,
      CO(3) => \euros1__92_carry__4_n_0\,
      CO(2 downto 0) => \NLW_euros1__92_carry__4_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \euros1__92_carry__4_i_1_n_0\,
      DI(2) => \euros1__92_carry__4_i_2_n_0\,
      DI(1) => \euros1__92_carry__4_i_3_n_0\,
      DI(0) => \euros1__92_carry__4_i_4_n_0\,
      O(3) => \euros1__92_carry__4_n_4\,
      O(2) => \euros1__92_carry__4_n_5\,
      O(1) => \euros1__92_carry__4_n_6\,
      O(0) => \euros1__92_carry__4_n_7\,
      S(3) => \euros1__92_carry__4_i_5_n_0\,
      S(2) => \euros1__92_carry__4_i_6_n_0\,
      S(1) => \euros1__92_carry__4_i_7_n_0\,
      S(0) => \euros1__92_carry__4_i_8_n_0\
    );
\euros1__92_carry__4_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => Q(20),
      I1 => Q(22),
      I2 => Q(18),
      O => \euros1__92_carry__4_i_1_n_0\
    );
\euros1__92_carry__4_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => Q(17),
      I1 => Q(21),
      I2 => Q(19),
      O => \euros1__92_carry__4_i_2_n_0\
    );
\euros1__92_carry__4_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => Q(16),
      I1 => Q(20),
      I2 => Q(18),
      O => \euros1__92_carry__4_i_3_n_0\
    );
\euros1__92_carry__4_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => Q(15),
      I1 => Q(19),
      I2 => Q(17),
      O => \euros1__92_carry__4_i_4_n_0\
    );
\euros1__92_carry__4_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(21),
      I1 => Q(23),
      I2 => Q(19),
      I3 => \euros1__92_carry__4_i_1_n_0\,
      O => \euros1__92_carry__4_i_5_n_0\
    );
\euros1__92_carry__4_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(20),
      I1 => Q(22),
      I2 => Q(18),
      I3 => \euros1__92_carry__4_i_2_n_0\,
      O => \euros1__92_carry__4_i_6_n_0\
    );
\euros1__92_carry__4_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(17),
      I1 => Q(21),
      I2 => Q(19),
      I3 => \euros1__92_carry__4_i_3_n_0\,
      O => \euros1__92_carry__4_i_7_n_0\
    );
\euros1__92_carry__4_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(16),
      I1 => Q(20),
      I2 => Q(18),
      I3 => \euros1__92_carry__4_i_4_n_0\,
      O => \euros1__92_carry__4_i_8_n_0\
    );
\euros1__92_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__92_carry__4_n_0\,
      CO(3 downto 0) => \NLW_euros1__92_carry__5_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \euros1__92_carry__5_i_1_n_0\,
      DI(0) => \euros1__92_carry__5_i_2_n_0\,
      O(3) => \NLW_euros1__92_carry__5_O_UNCONNECTED\(3),
      O(2) => \euros1__92_carry__5_n_5\,
      O(1) => \euros1__92_carry__5_n_6\,
      O(0) => \euros1__92_carry__5_n_7\,
      S(3) => '0',
      S(2) => \euros1__92_carry__5_i_3_n_0\,
      S(1) => \euros1__92_carry__5_i_4_n_0\,
      S(0) => \euros1__92_carry__5_i_5_n_0\
    );
\euros1__92_carry__5_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => Q(20),
      I1 => Q(24),
      I2 => Q(22),
      O => \euros1__92_carry__5_i_1_n_0\
    );
\euros1__92_carry__5_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => Q(21),
      I1 => Q(23),
      I2 => Q(19),
      O => \euros1__92_carry__5_i_2_n_0\
    );
\euros1__92_carry__5_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B24D4DB24DB2B24D"
    )
        port map (
      I0 => Q(23),
      I1 => Q(21),
      I2 => Q(25),
      I3 => Q(22),
      I4 => Q(24),
      I5 => Q(26),
      O => \euros1__92_carry__5_i_3_n_0\
    );
\euros1__92_carry__5_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__92_carry__5_i_1_n_0\,
      I1 => Q(21),
      I2 => Q(23),
      I3 => Q(25),
      O => \euros1__92_carry__5_i_4_n_0\
    );
\euros1__92_carry__5_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => Q(20),
      I1 => Q(24),
      I2 => Q(22),
      I3 => \euros1__92_carry__5_i_2_n_0\,
      O => \euros1__92_carry__5_i_5_n_0\
    );
\euros1__92_carry_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => Q(0),
      O => \euros1__92_carry_i_1_n_0\
    );
\euros1__92_carry_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => Q(1),
      I1 => Q(3),
      I2 => Q(0),
      O => \euros1__92_carry_i_2_n_0\
    );
\euros1__92_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(0),
      I1 => Q(2),
      O => \euros1__92_carry_i_3_n_0\
    );
\euros1__92_carry_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => Q(1),
      O => \euros1__92_carry_i_4_n_0\
    );
\i___0_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \i___169_carry_i_5_n_0\,
      I1 => cent(6),
      O => \i___0_carry__0_i_1_n_0\
    );
\i___0_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(5),
      I1 => \^total_reg[0]\,
      O => \i___0_carry__0_i_2_n_0\
    );
\i___0_carry__0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBB8B8B8888B888"
    )
        port map (
      I0 => Q(6),
      I1 => \^total_reg[29]\,
      I2 => \euros1__339_carry__0_n_6\,
      I3 => \euros1__339_carry__0_n_4\,
      I4 => \string_cent_decenas[1]5__27_carry__1_i_4_n_0\,
      I5 => \euros1__339_carry__0_n_5\,
      O => cent(6)
    );
\i___0_carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6999"
    )
        port map (
      I0 => cent(6),
      I1 => \i___169_carry_i_5_n_0\,
      I2 => \^total_reg[0]\,
      I3 => cent(5),
      O => \i___0_carry__0_i_4_n_0\
    );
\i___0_carry__0_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(5),
      I1 => \^total_reg[0]\,
      O => \i___0_carry__0_i_5_n_0\
    );
\i___0_carry__0_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(6),
      I1 => cent(4),
      O => \i___0_carry__0_i_6_n_0\
    );
\i___0_carry__0_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(5),
      I1 => cent(3),
      O => \i___0_carry__0_i_7_n_0\
    );
\i___0_carry__1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => cent(6),
      I1 => \i___169_carry_i_5_n_0\,
      O => \i___0_carry__1_i_1_n_0\
    );
\i___0_carry__1_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BB88BB8B88BB8888"
    )
        port map (
      I0 => Q(5),
      I1 => \^total_reg[29]\,
      I2 => \euros1__339_carry__0_n_5\,
      I3 => \string_cent_decenas[1]5__27_carry__1_i_4_n_0\,
      I4 => \euros1__339_carry__0_n_4\,
      I5 => \euros1__339_carry__0_n_6\,
      O => \i___0_carry__1_i_2_n_0\
    );
\i___0_carry__1_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8B8B8B8B88B"
    )
        port map (
      I0 => Q(4),
      I1 => \^total_reg[29]\,
      I2 => \euros1__339_carry__0_n_7\,
      I3 => \string_cent_decenas[1]5__186_carry_i_8_n_0\,
      I4 => \euros1__339_carry_n_4\,
      I5 => \euros1__339_carry_n_5\,
      O => \i___0_carry__1_i_3_n_0\
    );
\i___0_carry__1_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCC3"
    )
        port map (
      I0 => Q(3),
      I1 => \euros1__339_carry_n_4\,
      I2 => \string_cent_decenas[1]5__186_carry_i_8_n_0\,
      I3 => \euros1__339_carry_n_5\,
      I4 => \^total_reg[29]\,
      O => \i___0_carry__1_i_4_n_0\
    );
\i___0_carry__1_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B4"
    )
        port map (
      I0 => \i___169_carry_i_5_n_0\,
      I1 => cent(6),
      I2 => \i___0_carry_i_2_n_0\,
      O => \i___0_carry__1_i_5_n_0\
    );
\i___0_carry__2_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBB8B8B8888B888"
    )
        port map (
      I0 => Q(6),
      I1 => \^total_reg[29]\,
      I2 => \euros1__339_carry__0_n_6\,
      I3 => \euros1__339_carry__0_n_4\,
      I4 => \string_cent_decenas[1]5__27_carry__1_i_4_n_0\,
      I5 => \euros1__339_carry__0_n_5\,
      O => \i___0_carry__2_i_1_n_0\
    );
\i___0_carry_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B8B8B8B8B88B"
    )
        port map (
      I0 => Q(4),
      I1 => \^total_reg[29]\,
      I2 => \euros1__339_carry__0_n_7\,
      I3 => \string_cent_decenas[1]5__186_carry_i_8_n_0\,
      I4 => \euros1__339_carry_n_4\,
      I5 => \euros1__339_carry_n_5\,
      O => cent(4)
    );
\i___0_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \i___0_carry_i_2_n_0\,
      I1 => cent(4),
      O => \i___0_carry_i_3_n_0\
    );
\i___0_carry_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(3),
      I1 => \i___169_carry_i_5_n_0\,
      O => \i___0_carry_i_4_n_0\
    );
\i___0_carry_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^total_reg[0]\,
      I1 => \i___0_carry_i_2_n_0\,
      O => \i___0_carry_i_5_n_0\
    );
\i___0_carry_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(1),
      I1 => \^total_reg[29]\,
      I2 => p_1_in(1),
      O => \i___0_carry_i_6_n_0\
    );
\i___106_carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0880"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_6\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_5\,
      O => \i___106_carry__0_i_1_n_0\
    );
\i___106_carry__0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BE282828"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___0_carry__2_n_2\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_5\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_6\,
      I3 => \^total_reg[0]\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_6\,
      O => \i___106_carry__0_i_2_n_0\
    );
\i___106_carry__0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"28"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___0_carry__2_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_6\,
      I2 => \^total_reg[0]\,
      O => \i___106_carry__0_i_3_n_0\
    );
\i___106_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_4\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_7\,
      O => \i___106_carry__0_i_4_n_0\
    );
\i___106_carry__0_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F087870F"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_5\,
      I2 => \i___106_carry__0_i_9_n_0\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_5\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_4\,
      O => \i___106_carry__0_i_5_n_0\
    );
\i___106_carry__0_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => \i___106_carry__0_i_2_n_0\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_5\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_6\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_5\,
      O => \i___106_carry__0_i_6_n_0\
    );
\i___106_carry__0_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669966996"
    )
        port map (
      I0 => \i___106_carry__0_i_3_n_0\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___0_carry__2_n_2\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_5\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_6\,
      I4 => \^total_reg[0]\,
      I5 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_6\,
      O => \i___106_carry__0_i_7_n_0\
    );
\i___106_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___0_carry__2_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_6\,
      I2 => \^total_reg[0]\,
      I3 => \i___106_carry__0_i_4_n_0\,
      O => \i___106_carry__0_i_8_n_0\
    );
\i___106_carry__0_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_7\,
      I1 => \^total_reg[0]\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_4\,
      O => \i___106_carry__0_i_9_n_0\
    );
\i___106_carry__1_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969690069000000"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_5\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_5\,
      I4 => \i___0_carry_i_2_n_0\,
      I5 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_6\,
      O => \i___106_carry__1_i_1_n_0\
    );
\i___106_carry__1_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_4\,
      I2 => \^total_reg[0]\,
      O => \i___106_carry__1_i_10_n_0\
    );
\i___106_carry__1_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_5\,
      I1 => \i___0_carry_i_2_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_6\,
      O => \i___106_carry__1_i_11_n_0\
    );
\i___106_carry__1_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_6\,
      I1 => \i___169_carry_i_5_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_7\,
      O => \i___106_carry__1_i_12_n_0\
    );
\i___106_carry__1_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"D40000D400D4D400"
    )
        port map (
      I0 => \i___169_carry_i_5_n_0\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_7\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_6\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_6\,
      I4 => \i___0_carry_i_2_n_0\,
      I5 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_5\,
      O => \i___106_carry__1_i_2_n_0\
    );
\i___106_carry__1_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E8E800E80000E8"
    )
        port map (
      I0 => \^total_reg[0]\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_4\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_7\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_7\,
      I4 => \i___169_carry_i_5_n_0\,
      I5 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_6\,
      O => \i___106_carry__1_i_3_n_0\
    );
\i___106_carry__1_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80080880"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_4\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_5\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_4\,
      I3 => \^total_reg[0]\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_7\,
      O => \i___106_carry__1_i_4_n_0\
    );
\i___106_carry__1_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AA9955695566AA9"
    )
        port map (
      I0 => \i___106_carry__1_i_1_n_0\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_7\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_5\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_4\,
      I5 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_6\,
      O => \i___106_carry__1_i_5_n_0\
    );
\i___106_carry__1_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969669"
    )
        port map (
      I0 => \i___106_carry__1_i_2_n_0\,
      I1 => \i___106_carry__1_i_9_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_5\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_7\,
      O => \i___106_carry__1_i_6_n_0\
    );
\i___106_carry__1_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E87E1781"
    )
        port map (
      I0 => \i___106_carry__1_i_10_n_0\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_6\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_7\,
      I3 => \i___169_carry_i_5_n_0\,
      I4 => \i___106_carry__1_i_11_n_0\,
      O => \i___106_carry__1_i_7_n_0\
    );
\i___106_carry__1_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FF8F8808007077F"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_4\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_7\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_4\,
      I4 => \^total_reg[0]\,
      I5 => \i___106_carry__1_i_12_n_0\,
      O => \i___106_carry__1_i_8_n_0\
    );
\i___106_carry__1_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_6\,
      I1 => \i___0_carry_i_2_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_5\,
      O => \i___106_carry__1_i_9_n_0\
    );
\i___106_carry__2_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"90006660"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_6\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_4\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__2_i_1_n_0\
    );
\i___106_carry__2_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_4\,
      O => \i___106_carry__2_i_10_n_0\
    );
\i___106_carry__2_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_5\,
      O => \i___106_carry__2_i_11_n_0\
    );
\i___106_carry__2_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60009990"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_4\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_6\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_7\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_5\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__2_i_2_n_0\
    );
\i___106_carry__2_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60009990"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_7\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_6\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__2_i_3_n_0\
    );
\i___106_carry__2_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60009990"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_4\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_5\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_7\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__2_i_4_n_0\
    );
\i___106_carry__2_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"56A96A95"
    )
        port map (
      I0 => \i___106_carry__2_i_1_n_0\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_6\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_7\,
      O => \i___106_carry__2_i_5_n_0\
    );
\i___106_carry__2_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0E7070F1F18F8F0E"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_7\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_4\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_6\,
      I5 => \i___106_carry__2_i_9_n_0\,
      O => \i___106_carry__2_i_6_n_0\
    );
\i___106_carry__2_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0FE1780F780FF01E"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_4\,
      I2 => \i___106_carry__2_i_10_n_0\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_5\,
      I5 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_7\,
      O => \i___106_carry__2_i_7_n_0\
    );
\i___106_carry__2_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0E7070F1F18F8F0E"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_5\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_6\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_4\,
      I5 => \i___106_carry__2_i_11_n_0\,
      O => \i___106_carry__2_i_8_n_0\
    );
\i___106_carry__2_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_7\,
      O => \i___106_carry__2_i_9_n_0\
    );
\i___106_carry__3_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1062"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__3_i_1_n_0\
    );
\i___106_carry__3_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_4\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      O => \i___106_carry__3_i_2_n_0\
    );
\i___106_carry__3_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1062"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_6\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__3_i_3_n_0\
    );
\i___106_carry__3_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_6\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      O => \i___106_carry__3_i_4_n_0\
    );
\i___106_carry__3_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEF80107"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_4\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_7\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_6\,
      O => \i___106_carry__3_i_5_n_0\
    );
\i___106_carry__3_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEF80107"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_7\,
      O => \i___106_carry__3_i_6_n_0\
    );
\i___106_carry__3_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"56A96A95"
    )
        port map (
      I0 => \i___106_carry__3_i_3_n_0\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_4\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_5\,
      O => \i___106_carry__3_i_7_n_0\
    );
\i___106_carry__3_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAA9A9A5"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_6\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_7\,
      O => \i___106_carry__3_i_8_n_0\
    );
\i___106_carry__4_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"48"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__4_i_1_n_0\
    );
\i___106_carry__4_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1062"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_1\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_6\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__4_i_2_n_0\
    );
\i___106_carry__4_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1062"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_7\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__4_i_3_n_0\
    );
\i___106_carry__4_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__4_i_4_n_0\
    );
\i___106_carry__4_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_1\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__4_i_5_n_0\
    );
\i___106_carry__4_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0107"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_1\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      O => \i___106_carry__4_i_6_n_0\
    );
\i___106_carry__4_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEF80107"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_6\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_1\,
      O => \i___106_carry__4_i_7_n_0\
    );
\i___106_carry__5_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      O => \i___106_carry__5_i_1_n_0\
    );
\i___106_carry__5_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__5_i_2_n_0\
    );
\i___106_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_4\,
      O => \i___106_carry_i_1_n_0\
    );
\i___106_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_5\,
      O => \i___106_carry_i_2_n_0\
    );
\i___106_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_6\,
      O => \i___106_carry_i_3_n_0\
    );
\i___106_carry_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^total_reg[0]\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_4\,
      O => \i___106_carry_i_4_n_0\
    );
\i___106_carry_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_4\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_7\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_5\,
      O => \i___106_carry_i_5_n_0\
    );
\i___106_carry_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_6\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_5\,
      O => \i___106_carry_i_6_n_0\
    );
\i___106_carry_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_7\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_5\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_6\,
      O => \i___106_carry_i_7_n_0\
    );
\i___106_carry_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_4\,
      I1 => \^total_reg[0]\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_6\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_7\,
      O => \i___106_carry_i_8_n_0\
    );
\i___163_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_4\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_7\,
      O => \i___163_carry_i_1_n_0\
    );
\i___163_carry_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66969969"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___106_carry__5_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_5\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___106_carry__5_n_7\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_6\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_7\,
      O => \i___163_carry_i_2_n_0\
    );
\i___163_carry_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_4\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___106_carry__5_n_7\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_6\,
      O => \i___163_carry_i_3_n_0\
    );
\i___163_carry_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_4\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_7\,
      O => \i___163_carry_i_4_n_0\
    );
\i___169_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___163_carry_n_5\,
      O => \i___169_carry__0_i_1_n_0\
    );
\i___169_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(6),
      I1 => \string_cent_decenas[1]5_inferred__0/i___163_carry_n_6\,
      O => \i___169_carry__0_i_2_n_0\
    );
\i___169_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(5),
      I1 => \string_cent_decenas[1]5_inferred__0/i___163_carry_n_7\,
      O => \i___169_carry__0_i_3_n_0\
    );
\i___169_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(4),
      I1 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_5\,
      O => \i___169_carry__0_i_4_n_0\
    );
\i___169_carry_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(1),
      I1 => \^total_reg[29]\,
      I2 => p_1_in(1),
      O => \i___169_carry_i_2_n_0\
    );
\i___169_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(3),
      I1 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_6\,
      O => \i___169_carry_i_3_n_0\
    );
\i___169_carry_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \i___0_carry_i_2_n_0\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_7\,
      O => \i___169_carry_i_4_n_0\
    );
\i___169_carry_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^total_reg[0]\,
      O => \i___169_carry_i_6_n_0\
    );
\i___27_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(3),
      I1 => cent(5),
      O => \i___27_carry__0_i_1_n_0\
    );
\i___27_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => cent(6),
      I1 => \i___0_carry_i_2_n_0\,
      I2 => cent(4),
      O => \i___27_carry__0_i_2_n_0\
    );
\i___27_carry__0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \^total_reg[0]\,
      I1 => \i___0_carry_i_2_n_0\,
      I2 => cent(4),
      O => \i___27_carry__0_i_3_n_0\
    );
\i___27_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \i___169_carry_i_5_n_0\,
      I1 => cent(3),
      O => \i___27_carry__0_i_4_n_0\
    );
\i___27_carry__0_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"718E8E71"
    )
        port map (
      I0 => cent(4),
      I1 => cent(6),
      I2 => \i___0_carry_i_2_n_0\,
      I3 => cent(5),
      I4 => cent(3),
      O => \i___27_carry__0_i_5_n_0\
    );
\i___27_carry__0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E81717E817E8E817"
    )
        port map (
      I0 => cent(5),
      I1 => cent(3),
      I2 => \i___169_carry_i_5_n_0\,
      I3 => cent(4),
      I4 => \i___0_carry_i_2_n_0\,
      I5 => cent(6),
      O => \i___27_carry__0_i_6_n_0\
    );
\i___27_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \i___27_carry__0_i_3_n_0\,
      I1 => cent(3),
      I2 => cent(5),
      I3 => \i___169_carry_i_5_n_0\,
      O => \i___27_carry__0_i_7_n_0\
    );
\i___27_carry__0_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96699696"
    )
        port map (
      I0 => \^total_reg[0]\,
      I1 => \i___0_carry_i_2_n_0\,
      I2 => cent(4),
      I3 => cent(3),
      I4 => \i___169_carry_i_5_n_0\,
      O => \i___27_carry__0_i_8_n_0\
    );
\i___27_carry__1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => cent(4),
      I1 => cent(6),
      O => \i___27_carry__1_i_1_n_0\
    );
\i___27_carry__1_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => cent(3),
      I1 => cent(5),
      O => \i___27_carry__1_i_2_n_0\
    );
\i___27_carry__1_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(6),
      O => \i___27_carry__1_i_3_n_0\
    );
\i___27_carry__1_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4B"
    )
        port map (
      I0 => cent(4),
      I1 => cent(6),
      I2 => cent(5),
      O => \i___27_carry__1_i_4_n_0\
    );
\i___27_carry__1_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"D22D"
    )
        port map (
      I0 => cent(5),
      I1 => cent(3),
      I2 => cent(6),
      I3 => cent(4),
      O => \i___27_carry__1_i_5_n_0\
    );
\i___27_carry_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^total_reg[0]\,
      O => \i___27_carry_i_1_n_0\
    );
\i___27_carry_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \^total_reg[0]\,
      I1 => \i___169_carry_i_5_n_0\,
      I2 => cent(3),
      O => \i___27_carry_i_2_n_0\
    );
\i___27_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^total_reg[0]\,
      I1 => \i___0_carry_i_2_n_0\,
      O => \i___27_carry_i_3_n_0\
    );
\i___27_carry_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEF00000020"
    )
        port map (
      I0 => Q(0),
      I1 => \string_cent_decenas[1]5_carry_i_8_n_0\,
      I2 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I3 => \^total_reg[20]\,
      I4 => \string_cent_decenas[1]5_carry_i_11_n_0\,
      I5 => p_1_in(0),
      O => \i___27_carry_i_5_n_0\
    );
\i___56_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(1),
      I1 => \^total_reg[29]\,
      I2 => p_1_in(1),
      O => \i___56_carry__0_i_2_n_0\
    );
\i___56_carry__0_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(4),
      O => \i___56_carry__0_i_3_n_0\
    );
\i___56_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(3),
      I1 => cent(6),
      O => \i___56_carry__0_i_4_n_0\
    );
\i___56_carry__0_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \i___0_carry_i_2_n_0\,
      I1 => cent(5),
      O => \i___56_carry__0_i_5_n_0\
    );
\i___56_carry__0_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \i___169_carry_i_5_n_0\,
      I1 => cent(4),
      O => \i___56_carry__0_i_6_n_0\
    );
\i___56_carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(6),
      O => \i___56_carry__1_i_1_n_0\
    );
\i___56_carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(5),
      O => \i___56_carry__1_i_2_n_0\
    );
\i___56_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^total_reg[0]\,
      I1 => cent(3),
      O => \i___56_carry_i_1_n_0\
    );
\i___56_carry_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"553C"
    )
        port map (
      I0 => Q(2),
      I1 => \euros1__339_carry_n_5\,
      I2 => \string_cent_decenas[1]5__186_carry_i_8_n_0\,
      I3 => \^total_reg[29]\,
      O => \i___56_carry_i_2_n_0\
    );
\i___56_carry_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEF00000020"
    )
        port map (
      I0 => Q(0),
      I1 => \string_cent_decenas[1]5_carry_i_8_n_0\,
      I2 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I3 => \^total_reg[20]\,
      I4 => \string_cent_decenas[1]5_carry_i_11_n_0\,
      I5 => p_1_in(0),
      O => \i___56_carry_i_4_n_0\
    );
\i___80_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(6),
      I1 => cent(4),
      O => \i___80_carry__0_i_1_n_0\
    );
\i___80_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => cent(4),
      I1 => \i___0_carry_i_2_n_0\,
      O => \i___80_carry__0_i_2_n_0\
    );
\i___80_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => cent(3),
      I1 => \i___169_carry_i_5_n_0\,
      O => \i___80_carry__0_i_3_n_0\
    );
\i___80_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(3),
      I1 => \i___169_carry_i_5_n_0\,
      O => \i___80_carry__0_i_4_n_0\
    );
\i___80_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => cent(5),
      I1 => cent(3),
      I2 => cent(4),
      I3 => cent(6),
      O => \i___80_carry__0_i_5_n_0\
    );
\i___80_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \i___0_carry_i_2_n_0\,
      I1 => cent(4),
      I2 => cent(3),
      I3 => cent(5),
      O => \i___80_carry__0_i_6_n_0\
    );
\i___80_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4BB4"
    )
        port map (
      I0 => \i___169_carry_i_5_n_0\,
      I1 => cent(3),
      I2 => cent(4),
      I3 => \i___0_carry_i_2_n_0\,
      O => \i___80_carry__0_i_7_n_0\
    );
\i___80_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6999"
    )
        port map (
      I0 => \i___169_carry_i_5_n_0\,
      I1 => cent(3),
      I2 => \i___0_carry_i_2_n_0\,
      I3 => \^total_reg[0]\,
      O => \i___80_carry__0_i_8_n_0\
    );
\i___80_carry__1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => cent(4),
      I1 => cent(6),
      O => \i___80_carry__1_i_1_n_0\
    );
\i___80_carry__1_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBB8B8B8888B888"
    )
        port map (
      I0 => Q(6),
      I1 => \^total_reg[29]\,
      I2 => \euros1__339_carry__0_n_6\,
      I3 => \euros1__339_carry__0_n_4\,
      I4 => \string_cent_decenas[1]5__27_carry__1_i_4_n_0\,
      I5 => \euros1__339_carry__0_n_5\,
      O => \i___80_carry__1_i_2_n_0\
    );
\i___80_carry__1_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => cent(6),
      I1 => cent(4),
      I2 => cent(5),
      O => \i___80_carry__1_i_3_n_0\
    );
\i___80_carry_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \^total_reg[0]\,
      I1 => \i___0_carry_i_2_n_0\,
      I2 => cent(6),
      O => \i___80_carry_i_1_n_0\
    );
\i___80_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(5),
      I1 => \i___169_carry_i_5_n_0\,
      O => \i___80_carry_i_2_n_0\
    );
\i___80_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(4),
      I1 => \^total_reg[0]\,
      O => \i___80_carry_i_3_n_0\
    );
\i___80_carry_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCC3"
    )
        port map (
      I0 => Q(3),
      I1 => \euros1__339_carry_n_4\,
      I2 => \string_cent_decenas[1]5__186_carry_i_8_n_0\,
      I3 => \euros1__339_carry_n_5\,
      I4 => \^total_reg[29]\,
      O => \i___80_carry_i_4_n_0\
    );
\string_cent_decenas[1]5__100_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5__100_carry_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5__100_carry_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__100_carry_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__100_carry_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__100_carry_i_3_n_0\,
      DI(0) => '0',
      O(3) => \string_cent_decenas[1]5__100_carry_n_4\,
      O(2) => \string_cent_decenas[1]5__100_carry_n_5\,
      O(1) => \string_cent_decenas[1]5__100_carry_n_6\,
      O(0) => \string_cent_decenas[1]5__100_carry_n_7\,
      S(3) => \string_cent_decenas[1]5__100_carry_i_4_n_0\,
      S(2) => \string_cent_decenas[1]5__100_carry_i_5_n_0\,
      S(1) => \string_cent_decenas[1]5__100_carry_i_6_n_0\,
      S(0) => \string_cent_decenas[1]5__100_carry_i_7_n_0\
    );
\string_cent_decenas[1]5__100_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__100_carry_n_0\,
      CO(3) => \string_cent_decenas[1]5__100_carry__0_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5__100_carry__0_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__100_carry__0_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__100_carry__0_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__100_carry__0_i_3_n_0\,
      DI(0) => \string_cent_decenas[1]5__100_carry__0_i_4_n_0\,
      O(3) => \string_cent_decenas[1]5__100_carry__0_n_4\,
      O(2) => \string_cent_decenas[1]5__100_carry__0_n_5\,
      O(1) => \string_cent_decenas[1]5__100_carry__0_n_6\,
      O(0) => \string_cent_decenas[1]5__100_carry__0_n_7\,
      S(3) => \string_cent_decenas[1]5__100_carry__0_i_5_n_0\,
      S(2) => \string_cent_decenas[1]5__100_carry__0_i_6_n_0\,
      S(1) => \string_cent_decenas[1]5__100_carry__0_i_7_n_0\,
      S(0) => \string_cent_decenas[1]5__100_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5__100_carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4D"
    )
        port map (
      I0 => cent(6),
      I1 => cent(4),
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__100_carry__0_i_1_n_0\
    );
\string_cent_decenas[1]5__100_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => cent(3),
      I1 => cent(5),
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__100_carry__0_i_2_n_0\
    );
\string_cent_decenas[1]5__100_carry__0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => cent(4),
      I1 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I2 => \i___0_carry_i_2_n_0\,
      O => \string_cent_decenas[1]5__100_carry__0_i_3_n_0\
    );
\string_cent_decenas[1]5__100_carry__0_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \i___169_carry_i_5_n_0\,
      I1 => cent(3),
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__100_carry__0_i_4_n_0\
    );
\string_cent_decenas[1]5__100_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2D4B"
    )
        port map (
      I0 => cent(4),
      I1 => cent(6),
      I2 => cent(5),
      I3 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__100_carry__0_i_5_n_0\
    );
\string_cent_decenas[1]5__100_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \string_cent_decenas[1]5__100_carry__0_i_2_n_0\,
      I1 => cent(4),
      I2 => cent(6),
      I3 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__100_carry__0_i_6_n_0\
    );
\string_cent_decenas[1]5__100_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => cent(3),
      I1 => cent(5),
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I3 => \string_cent_decenas[1]5__100_carry__0_i_3_n_0\,
      O => \string_cent_decenas[1]5__100_carry__0_i_7_n_0\
    );
\string_cent_decenas[1]5__100_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => cent(4),
      I1 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I2 => \i___0_carry_i_2_n_0\,
      I3 => \string_cent_decenas[1]5__100_carry__0_i_4_n_0\,
      O => \string_cent_decenas[1]5__100_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5__100_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__100_carry__0_n_0\,
      CO(3 downto 0) => \NLW_string_cent_decenas[1]5__100_carry__1_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_string_cent_decenas[1]5__100_carry__1_O_UNCONNECTED\(3 downto 1),
      O(0) => \string_cent_decenas[1]5__100_carry__1_n_7\,
      S(3 downto 1) => B"000",
      S(0) => \string_cent_decenas[1]5__100_carry__1_i_1_n_0\
    );
\string_cent_decenas[1]5__100_carry__1_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"93"
    )
        port map (
      I0 => cent(5),
      I1 => cent(6),
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__100_carry__1_i_1_n_0\
    );
\string_cent_decenas[1]5__100_carry_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \^total_reg[0]\,
      I1 => \i___0_carry_i_2_n_0\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__100_carry_i_1_n_0\
    );
\string_cent_decenas[1]5__100_carry_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \^total_reg[0]\,
      I1 => \i___0_carry_i_2_n_0\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__100_carry_i_2_n_0\
    );
\string_cent_decenas[1]5__100_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I1 => \^total_reg[0]\,
      O => \string_cent_decenas[1]5__100_carry_i_3_n_0\
    );
\string_cent_decenas[1]5__100_carry_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \i___169_carry_i_5_n_0\,
      I1 => cent(3),
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I3 => \string_cent_decenas[1]5__100_carry_i_1_n_0\,
      O => \string_cent_decenas[1]5__100_carry_i_4_n_0\
    );
\string_cent_decenas[1]5__100_carry_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9996"
    )
        port map (
      I0 => \^total_reg[0]\,
      I1 => \i___0_carry_i_2_n_0\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I3 => \i___169_carry_i_5_n_0\,
      O => \string_cent_decenas[1]5__100_carry_i_5_n_0\
    );
\string_cent_decenas[1]5__100_carry_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"9C"
    )
        port map (
      I0 => \^total_reg[0]\,
      I1 => \i___169_carry_i_5_n_0\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__100_carry_i_6_n_0\
    );
\string_cent_decenas[1]5__100_carry_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^total_reg[0]\,
      I1 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__100_carry_i_7_n_0\
    );
\string_cent_decenas[1]5__100_carry_i_8\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_carry__1_n_0\,
      CO(3 downto 1) => \NLW_string_cent_decenas[1]5__100_carry_i_8_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_string_cent_decenas[1]5__100_carry_i_8_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
\string_cent_decenas[1]5__124_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5__124_carry_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5__124_carry_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__124_carry_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__124_carry_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__124_carry_i_3_n_0\,
      DI(0) => \string_cent_decenas[1]5__124_carry_i_4_n_0\,
      O(3 downto 0) => \NLW_string_cent_decenas[1]5__124_carry_O_UNCONNECTED\(3 downto 0),
      S(3) => \string_cent_decenas[1]5__124_carry_i_5_n_0\,
      S(2) => \string_cent_decenas[1]5__124_carry_i_6_n_0\,
      S(1) => \string_cent_decenas[1]5__124_carry_i_7_n_0\,
      S(0) => \string_cent_decenas[1]5__124_carry_i_8_n_0\
    );
\string_cent_decenas[1]5__124_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__124_carry_n_0\,
      CO(3) => \string_cent_decenas[1]5__124_carry__0_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5__124_carry__0_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__124_carry__0_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__124_carry__0_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__124_carry__0_i_3_n_0\,
      DI(0) => \string_cent_decenas[1]5__124_carry__0_i_4_n_0\,
      O(3 downto 0) => \NLW_string_cent_decenas[1]5__124_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \string_cent_decenas[1]5__124_carry__0_i_5_n_0\,
      S(2) => \string_cent_decenas[1]5__124_carry__0_i_6_n_0\,
      S(1) => \string_cent_decenas[1]5__124_carry__0_i_7_n_0\,
      S(0) => \string_cent_decenas[1]5__124_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5__124_carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"82"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry__0_n_7\,
      I1 => \string_cent_decenas[1]5_carry_n_7\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__124_carry__0_i_1_n_0\
    );
\string_cent_decenas[1]5__124_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry_n_4\,
      I1 => \string_cent_decenas[1]5_carry__1_n_4\,
      O => \string_cent_decenas[1]5__124_carry__0_i_2_n_0\
    );
\string_cent_decenas[1]5__124_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry_n_5\,
      I1 => \string_cent_decenas[1]5_carry__1_n_5\,
      O => \string_cent_decenas[1]5__124_carry__0_i_3_n_0\
    );
\string_cent_decenas[1]5__124_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry_n_6\,
      I1 => \string_cent_decenas[1]5_carry__1_n_6\,
      O => \string_cent_decenas[1]5__124_carry__0_i_4_n_0\
    );
\string_cent_decenas[1]5__124_carry__0_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99966669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry__0_n_6\,
      I1 => \string_cent_decenas[1]5__55_carry_n_6\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I3 => \string_cent_decenas[1]5_carry_n_7\,
      I4 => \string_cent_decenas[1]5__124_carry__0_i_1_n_0\,
      O => \string_cent_decenas[1]5__124_carry__0_i_5_n_0\
    );
\string_cent_decenas[1]5__124_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry__0_n_7\,
      I1 => \string_cent_decenas[1]5_carry_n_7\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I3 => \string_cent_decenas[1]5__124_carry__0_i_2_n_0\,
      O => \string_cent_decenas[1]5__124_carry__0_i_6_n_0\
    );
\string_cent_decenas[1]5__124_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry_n_4\,
      I1 => \string_cent_decenas[1]5_carry__1_n_4\,
      I2 => \string_cent_decenas[1]5_carry__1_n_5\,
      I3 => \string_cent_decenas[1]5__27_carry_n_5\,
      O => \string_cent_decenas[1]5__124_carry__0_i_7_n_0\
    );
\string_cent_decenas[1]5__124_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__1_n_6\,
      I1 => \string_cent_decenas[1]5__27_carry_n_6\,
      I2 => \string_cent_decenas[1]5_carry__1_n_5\,
      I3 => \string_cent_decenas[1]5__27_carry_n_5\,
      O => \string_cent_decenas[1]5__124_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5__124_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__124_carry__0_n_0\,
      CO(3) => \string_cent_decenas[1]5__124_carry__1_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5__124_carry__1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__124_carry__1_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__124_carry__1_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__124_carry__1_i_3_n_0\,
      DI(0) => \string_cent_decenas[1]5__124_carry__1_i_4_n_0\,
      O(3 downto 0) => \NLW_string_cent_decenas[1]5__124_carry__1_O_UNCONNECTED\(3 downto 0),
      S(3) => \string_cent_decenas[1]5__124_carry__1_i_5_n_0\,
      S(2) => \string_cent_decenas[1]5__124_carry__1_i_6_n_0\,
      S(1) => \string_cent_decenas[1]5__124_carry__1_i_7_n_0\,
      S(0) => \string_cent_decenas[1]5__124_carry__1_i_8_n_0\
    );
\string_cent_decenas[1]5__124_carry__1_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8B82"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry__1_n_7\,
      I1 => \string_cent_decenas[1]5__55_carry__0_n_7\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I3 => \string_cent_decenas[1]5__55_carry_n_4\,
      O => \string_cent_decenas[1]5__124_carry__1_i_1_n_0\
    );
\string_cent_decenas[1]5__124_carry__1_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8B82"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry__0_n_4\,
      I1 => \string_cent_decenas[1]5__55_carry_n_4\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I3 => \string_cent_decenas[1]5__55_carry_n_5\,
      O => \string_cent_decenas[1]5__124_carry__1_i_2_n_0\
    );
\string_cent_decenas[1]5__124_carry__1_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8B82"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5__55_carry_n_5\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I3 => \string_cent_decenas[1]5__55_carry_n_6\,
      O => \string_cent_decenas[1]5__124_carry__1_i_3_n_0\
    );
\string_cent_decenas[1]5__124_carry__1_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8B82"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry__0_n_6\,
      I1 => \string_cent_decenas[1]5__55_carry_n_6\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I3 => \string_cent_decenas[1]5_carry_n_7\,
      O => \string_cent_decenas[1]5__124_carry__1_i_4_n_0\
    );
\string_cent_decenas[1]5__124_carry__1_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3CC3F00F87781EE1"
    )
        port map (
      I0 => \string_cent_decenas[1]5__55_carry_n_4\,
      I1 => \string_cent_decenas[1]5__27_carry__1_n_7\,
      I2 => \string_cent_decenas[1]5__27_carry__1_n_6\,
      I3 => \string_cent_decenas[1]5__124_carry__1_i_9_n_0\,
      I4 => \string_cent_decenas[1]5__55_carry__0_n_7\,
      I5 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__124_carry__1_i_5_n_0\
    );
\string_cent_decenas[1]5__124_carry__1_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96969669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__1_i_2_n_0\,
      I1 => \string_cent_decenas[1]5__27_carry__1_n_7\,
      I2 => \string_cent_decenas[1]5__55_carry__0_n_7\,
      I3 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I4 => \string_cent_decenas[1]5__55_carry_n_4\,
      O => \string_cent_decenas[1]5__124_carry__1_i_6_n_0\
    );
\string_cent_decenas[1]5__124_carry__1_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99966669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry__0_n_4\,
      I1 => \string_cent_decenas[1]5__55_carry_n_4\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I3 => \string_cent_decenas[1]5__55_carry_n_5\,
      I4 => \string_cent_decenas[1]5__124_carry__1_i_3_n_0\,
      O => \string_cent_decenas[1]5__124_carry__1_i_7_n_0\
    );
\string_cent_decenas[1]5__124_carry__1_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99966669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5__55_carry_n_5\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I3 => \string_cent_decenas[1]5__55_carry_n_6\,
      I4 => \string_cent_decenas[1]5__124_carry__1_i_4_n_0\,
      O => \string_cent_decenas[1]5__124_carry__1_i_8_n_0\
    );
\string_cent_decenas[1]5__124_carry__1_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \string_cent_decenas[1]5__55_carry__0_n_6\,
      I1 => \^total_reg[0]\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__124_carry__1_i_9_n_0\
    );
\string_cent_decenas[1]5__124_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__124_carry__1_n_0\,
      CO(3) => \string_cent_decenas[1]5__124_carry__2_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5__124_carry__2_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__124_carry__2_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__124_carry__2_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__124_carry__2_i_3_n_0\,
      DI(0) => \string_cent_decenas[1]5__124_carry__2_i_4_n_0\,
      O(3 downto 0) => \NLW_string_cent_decenas[1]5__124_carry__2_O_UNCONNECTED\(3 downto 0),
      S(3) => \string_cent_decenas[1]5__124_carry__2_i_5_n_0\,
      S(2) => \string_cent_decenas[1]5__124_carry__2_i_6_n_0\,
      S(1) => \string_cent_decenas[1]5__124_carry__2_i_7_n_0\,
      S(0) => \string_cent_decenas[1]5__124_carry__2_i_8_n_0\
    );
\string_cent_decenas[1]5__124_carry__2_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"54D580FE80FE54D5"
    )
        port map (
      I0 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I1 => \i___0_carry_i_2_n_0\,
      I2 => \string_cent_decenas[1]5__55_carry__0_n_4\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__55_carry__1_n_7\,
      I5 => \string_cent_decenas[1]5__124_carry__2_i_9_n_0\,
      O => \string_cent_decenas[1]5__124_carry__2_i_1_n_0\
    );
\string_cent_decenas[1]5__124_carry__2_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B22B"
    )
        port map (
      I0 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I1 => \string_cent_decenas[1]5__55_carry__1_n_7\,
      I2 => cent(3),
      I3 => \^total_reg[0]\,
      O => \string_cent_decenas[1]5__124_carry__2_i_10_n_0\
    );
\string_cent_decenas[1]5__124_carry__2_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \^total_reg[0]\,
      I1 => cent(3),
      I2 => \string_cent_decenas[1]5__55_carry__1_n_7\,
      I3 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__124_carry__2_i_11_n_0\
    );
\string_cent_decenas[1]5__124_carry__2_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \^total_reg[0]\,
      I1 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I2 => \string_cent_decenas[1]5__55_carry__0_n_6\,
      O => \string_cent_decenas[1]5__124_carry__2_i_12_n_0\
    );
\string_cent_decenas[1]5__124_carry__2_i_13\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \string_cent_decenas[1]5__55_carry__0_n_4\,
      I1 => \i___0_carry_i_2_n_0\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__124_carry__2_i_13_n_0\
    );
\string_cent_decenas[1]5__124_carry__2_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \string_cent_decenas[1]5__55_carry__0_n_5\,
      I1 => \i___169_carry_i_5_n_0\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__124_carry__2_i_14_n_0\
    );
\string_cent_decenas[1]5__124_carry__2_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"417D006969FF417D"
    )
        port map (
      I0 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I1 => \i___0_carry_i_2_n_0\,
      I2 => \string_cent_decenas[1]5__55_carry__0_n_4\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__55_carry__0_n_5\,
      I5 => \i___169_carry_i_5_n_0\,
      O => \string_cent_decenas[1]5__124_carry__2_i_2_n_0\
    );
\string_cent_decenas[1]5__124_carry__2_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"96FF14D714D70096"
    )
        port map (
      I0 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I1 => \i___169_carry_i_5_n_0\,
      I2 => \string_cent_decenas[1]5__55_carry__0_n_5\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => \^total_reg[0]\,
      I5 => \string_cent_decenas[1]5__55_carry__0_n_6\,
      O => \string_cent_decenas[1]5__124_carry__2_i_3_n_0\
    );
\string_cent_decenas[1]5__124_carry__2_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7D416900"
    )
        port map (
      I0 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I1 => \^total_reg[0]\,
      I2 => \string_cent_decenas[1]5__55_carry__0_n_6\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_6\,
      I4 => \string_cent_decenas[1]5__55_carry__0_n_7\,
      O => \string_cent_decenas[1]5__124_carry__2_i_4_n_0\
    );
\string_cent_decenas[1]5__124_carry__2_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__2_i_1_n_0\,
      I1 => \string_cent_decenas[1]5__124_carry__2_i_10_n_0\,
      I2 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I3 => \string_cent_decenas[1]5__79_carry_n_6\,
      I4 => \string_cent_decenas[1]5__55_carry__1_n_6\,
      I5 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__124_carry__2_i_5_n_0\
    );
\string_cent_decenas[1]5__124_carry__2_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A665599A599AA665"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__2_i_2_n_0\,
      I1 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I2 => \i___0_carry_i_2_n_0\,
      I3 => \string_cent_decenas[1]5__55_carry__0_n_4\,
      I4 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I5 => \string_cent_decenas[1]5__124_carry__2_i_11_n_0\,
      O => \string_cent_decenas[1]5__124_carry__2_i_6_n_0\
    );
\string_cent_decenas[1]5__124_carry__2_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9336C993C9936CC9"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__2_i_12_n_0\,
      I1 => \string_cent_decenas[1]5__124_carry__2_i_13_n_0\,
      I2 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I3 => \string_cent_decenas[1]5__55_carry__0_n_5\,
      I4 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I5 => \i___169_carry_i_5_n_0\,
      O => \string_cent_decenas[1]5__124_carry__2_i_7_n_0\
    );
\string_cent_decenas[1]5__124_carry__2_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"65A69A599A5965A6"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__2_i_4_n_0\,
      I1 => \^total_reg[0]\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I3 => \string_cent_decenas[1]5__55_carry__0_n_6\,
      I4 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I5 => \string_cent_decenas[1]5__124_carry__2_i_14_n_0\,
      O => \string_cent_decenas[1]5__124_carry__2_i_8_n_0\
    );
\string_cent_decenas[1]5__124_carry__2_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(3),
      I1 => \^total_reg[0]\,
      O => \string_cent_decenas[1]5__124_carry__2_i_9_n_0\
    );
\string_cent_decenas[1]5__124_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__124_carry__2_n_0\,
      CO(3) => \string_cent_decenas[1]5__124_carry__3_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5__124_carry__3_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__124_carry__3_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__124_carry__3_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__124_carry__3_i_3_n_0\,
      DI(0) => \string_cent_decenas[1]5__124_carry__3_i_4_n_0\,
      O(3 downto 0) => \NLW_string_cent_decenas[1]5__124_carry__3_O_UNCONNECTED\(3 downto 0),
      S(3) => \string_cent_decenas[1]5__124_carry__3_i_5_n_0\,
      S(2) => \string_cent_decenas[1]5__124_carry__3_i_6_n_0\,
      S(1) => \string_cent_decenas[1]5__124_carry__3_i_7_n_0\,
      S(0) => \string_cent_decenas[1]5__124_carry__3_i_8_n_0\
    );
\string_cent_decenas[1]5__124_carry__3_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"D400FFD4"
    )
        port map (
      I0 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I1 => \string_cent_decenas[1]5__55_carry__1_n_4\,
      I2 => \string_cent_decenas[1]5__79_carry_n_4\,
      I3 => \string_cent_decenas[1]5__124_carry__3_i_9_n_0\,
      I4 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      O => \string_cent_decenas[1]5__124_carry__3_i_1_n_0\
    );
\string_cent_decenas[1]5__124_carry__3_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I1 => \string_cent_decenas[1]5__55_carry__1_n_4\,
      I2 => \string_cent_decenas[1]5__79_carry_n_4\,
      O => \string_cent_decenas[1]5__124_carry__3_i_10_n_0\
    );
\string_cent_decenas[1]5__124_carry__3_i_11\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__55_carry__1_n_0\,
      CO(3 downto 1) => \NLW_string_cent_decenas[1]5__124_carry__3_i_11_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_string_cent_decenas[1]5__124_carry__3_i_11_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
\string_cent_decenas[1]5__124_carry__3_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \string_cent_decenas[1]5__79_carry__0_n_6\,
      I1 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I2 => \string_cent_decenas[1]5__100_carry_n_6\,
      O => \string_cent_decenas[1]5__124_carry__3_i_12_n_0\
    );
\string_cent_decenas[1]5__124_carry__3_i_13\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I1 => \string_cent_decenas[1]5__55_carry__1_n_5\,
      I2 => \string_cent_decenas[1]5__79_carry_n_5\,
      O => \string_cent_decenas[1]5__124_carry__3_i_13_n_0\
    );
\string_cent_decenas[1]5__124_carry__3_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \string_cent_decenas[1]5__79_carry_n_5\,
      I1 => \string_cent_decenas[1]5__55_carry__1_n_5\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__124_carry__3_i_14_n_0\
    );
\string_cent_decenas[1]5__124_carry__3_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"54D580FE80FE54D5"
    )
        port map (
      I0 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I1 => \string_cent_decenas[1]5__55_carry__1_n_5\,
      I2 => \string_cent_decenas[1]5__79_carry_n_5\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__55_carry__1_n_4\,
      I5 => \string_cent_decenas[1]5__79_carry_n_4\,
      O => \string_cent_decenas[1]5__124_carry__3_i_2_n_0\
    );
\string_cent_decenas[1]5__124_carry__3_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"54D580FE80FE54D5"
    )
        port map (
      I0 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I1 => \string_cent_decenas[1]5__55_carry__1_n_6\,
      I2 => \string_cent_decenas[1]5__79_carry_n_6\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__55_carry__1_n_5\,
      I5 => \string_cent_decenas[1]5__79_carry_n_5\,
      O => \string_cent_decenas[1]5__124_carry__3_i_3_n_0\
    );
\string_cent_decenas[1]5__124_carry__3_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"54D580FE80FE54D5"
    )
        port map (
      I0 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I1 => \string_cent_decenas[1]5__55_carry__1_n_7\,
      I2 => \string_cent_decenas[1]5__124_carry__2_i_9_n_0\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__55_carry__1_n_6\,
      I5 => \string_cent_decenas[1]5__79_carry_n_6\,
      O => \string_cent_decenas[1]5__124_carry__3_i_4_n_0\
    );
\string_cent_decenas[1]5__124_carry__3_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"188E8EE7E7717118"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__3_i_10_n_0\,
      I1 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I2 => \string_cent_decenas[1]5__100_carry_n_7\,
      I3 => \string_cent_decenas[1]5__79_carry__0_n_7\,
      I4 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I5 => \string_cent_decenas[1]5__124_carry__3_i_12_n_0\,
      O => \string_cent_decenas[1]5__124_carry__3_i_5_n_0\
    );
\string_cent_decenas[1]5__124_carry__3_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9696699669966969"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__3_i_2_n_0\,
      I1 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5__124_carry__3_i_9_n_0\,
      I3 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I4 => \string_cent_decenas[1]5__55_carry__1_n_4\,
      I5 => \string_cent_decenas[1]5__79_carry_n_4\,
      O => \string_cent_decenas[1]5__124_carry__3_i_6_n_0\
    );
\string_cent_decenas[1]5__124_carry__3_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__3_i_3_n_0\,
      I1 => \string_cent_decenas[1]5__124_carry__3_i_13_n_0\,
      I2 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I3 => \string_cent_decenas[1]5__79_carry_n_4\,
      I4 => \string_cent_decenas[1]5__55_carry__1_n_4\,
      I5 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__124_carry__3_i_7_n_0\
    );
\string_cent_decenas[1]5__124_carry__3_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"188E8EE7E7717118"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__2_i_10_n_0\,
      I1 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I2 => \string_cent_decenas[1]5__55_carry__1_n_6\,
      I3 => \string_cent_decenas[1]5__79_carry_n_6\,
      I4 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I5 => \string_cent_decenas[1]5__124_carry__3_i_14_n_0\,
      O => \string_cent_decenas[1]5__124_carry__3_i_8_n_0\
    );
\string_cent_decenas[1]5__124_carry__3_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \string_cent_decenas[1]5__79_carry__0_n_7\,
      I1 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I2 => \string_cent_decenas[1]5__100_carry_n_7\,
      O => \string_cent_decenas[1]5__124_carry__3_i_9_n_0\
    );
\string_cent_decenas[1]5__124_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__124_carry__3_n_0\,
      CO(3) => \string_cent_decenas[1]5__124_carry__4_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5__124_carry__4_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__124_carry__4_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__124_carry__4_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__124_carry__4_i_3_n_0\,
      DI(0) => \string_cent_decenas[1]5__124_carry__4_i_4_n_0\,
      O(3) => \string_cent_decenas[1]5__124_carry__4_n_4\,
      O(2 downto 0) => \NLW_string_cent_decenas[1]5__124_carry__4_O_UNCONNECTED\(2 downto 0),
      S(3) => \string_cent_decenas[1]5__124_carry__4_i_5_n_0\,
      S(2) => \string_cent_decenas[1]5__124_carry__4_i_6_n_0\,
      S(1) => \string_cent_decenas[1]5__124_carry__4_i_7_n_0\,
      S(0) => \string_cent_decenas[1]5__124_carry__4_i_8_n_0\
    );
\string_cent_decenas[1]5__124_carry__4_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"54D580FE80FE54D5"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I1 => \string_cent_decenas[1]5__100_carry_n_4\,
      I2 => \string_cent_decenas[1]5__79_carry__0_n_4\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__100_carry__0_n_7\,
      I5 => \string_cent_decenas[1]5__79_carry__1_n_7\,
      O => \string_cent_decenas[1]5__124_carry__4_i_1_n_0\
    );
\string_cent_decenas[1]5__124_carry__4_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I1 => \string_cent_decenas[1]5__100_carry_n_4\,
      I2 => \string_cent_decenas[1]5__79_carry__0_n_4\,
      O => \string_cent_decenas[1]5__124_carry__4_i_10_n_0\
    );
\string_cent_decenas[1]5__124_carry__4_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I1 => \string_cent_decenas[1]5__100_carry_n_5\,
      I2 => \string_cent_decenas[1]5__79_carry__0_n_5\,
      O => \string_cent_decenas[1]5__124_carry__4_i_11_n_0\
    );
\string_cent_decenas[1]5__124_carry__4_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \string_cent_decenas[1]5__79_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I2 => \string_cent_decenas[1]5__100_carry_n_5\,
      O => \string_cent_decenas[1]5__124_carry__4_i_12_n_0\
    );
\string_cent_decenas[1]5__124_carry__4_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"54D580FE80FE54D5"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I1 => \string_cent_decenas[1]5__100_carry_n_5\,
      I2 => \string_cent_decenas[1]5__79_carry__0_n_5\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__100_carry_n_4\,
      I5 => \string_cent_decenas[1]5__79_carry__0_n_4\,
      O => \string_cent_decenas[1]5__124_carry__4_i_2_n_0\
    );
\string_cent_decenas[1]5__124_carry__4_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"08E0800E8FFEF8EF"
    )
        port map (
      I0 => \string_cent_decenas[1]5__100_carry_n_6\,
      I1 => \string_cent_decenas[1]5__79_carry__0_n_6\,
      I2 => \string_cent_decenas[1]5__79_carry__0_n_5\,
      I3 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I4 => \string_cent_decenas[1]5__100_carry_n_5\,
      I5 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      O => \string_cent_decenas[1]5__124_carry__4_i_3_n_0\
    );
\string_cent_decenas[1]5__124_carry__4_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"54D580FE80FE54D5"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I1 => \string_cent_decenas[1]5__100_carry_n_7\,
      I2 => \string_cent_decenas[1]5__79_carry__0_n_7\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__100_carry_n_6\,
      I5 => \string_cent_decenas[1]5__79_carry__0_n_6\,
      O => \string_cent_decenas[1]5__124_carry__4_i_4_n_0\
    );
\string_cent_decenas[1]5__124_carry__4_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__4_i_1_n_0\,
      I1 => \string_cent_decenas[1]5__124_carry__4_i_9_n_0\,
      I2 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I3 => \string_cent_decenas[1]5__79_carry__1_n_6\,
      I4 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I5 => \string_cent_decenas[1]5__100_carry__0_n_6\,
      O => \string_cent_decenas[1]5__124_carry__4_i_5_n_0\
    );
\string_cent_decenas[1]5__124_carry__4_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__4_i_2_n_0\,
      I1 => \string_cent_decenas[1]5__124_carry__4_i_10_n_0\,
      I2 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I3 => \string_cent_decenas[1]5__79_carry__1_n_7\,
      I4 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I5 => \string_cent_decenas[1]5__100_carry__0_n_7\,
      O => \string_cent_decenas[1]5__124_carry__4_i_6_n_0\
    );
\string_cent_decenas[1]5__124_carry__4_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__4_i_3_n_0\,
      I1 => \string_cent_decenas[1]5__124_carry__4_i_11_n_0\,
      I2 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I3 => \string_cent_decenas[1]5__79_carry__0_n_4\,
      I4 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I5 => \string_cent_decenas[1]5__100_carry_n_4\,
      O => \string_cent_decenas[1]5__124_carry__4_i_7_n_0\
    );
\string_cent_decenas[1]5__124_carry__4_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9696699669966969"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__4_i_4_n_0\,
      I1 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5__124_carry__4_i_12_n_0\,
      I3 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I4 => \string_cent_decenas[1]5__100_carry_n_6\,
      I5 => \string_cent_decenas[1]5__79_carry__0_n_6\,
      O => \string_cent_decenas[1]5__124_carry__4_i_8_n_0\
    );
\string_cent_decenas[1]5__124_carry__4_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I1 => \string_cent_decenas[1]5__100_carry__0_n_7\,
      I2 => \string_cent_decenas[1]5__79_carry__1_n_7\,
      O => \string_cent_decenas[1]5__124_carry__4_i_9_n_0\
    );
\string_cent_decenas[1]5__124_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__124_carry__4_n_0\,
      CO(3 downto 0) => \NLW_string_cent_decenas[1]5__124_carry__5_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \string_cent_decenas[1]5__124_carry__5_i_1_n_0\,
      DI(0) => \string_cent_decenas[1]5__124_carry__5_i_2_n_0\,
      O(3) => \NLW_string_cent_decenas[1]5__124_carry__5_O_UNCONNECTED\(3),
      O(2) => \string_cent_decenas[1]5__124_carry__5_n_5\,
      O(1) => \string_cent_decenas[1]5__124_carry__5_n_6\,
      O(0) => \string_cent_decenas[1]5__124_carry__5_n_7\,
      S(3) => '0',
      S(2) => \string_cent_decenas[1]5__124_carry__5_i_3_n_0\,
      S(1) => \string_cent_decenas[1]5__124_carry__5_i_4_n_0\,
      S(0) => \string_cent_decenas[1]5__124_carry__5_i_5_n_0\
    );
\string_cent_decenas[1]5__124_carry__5_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"80FE54D554D580FE"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I1 => \string_cent_decenas[1]5__100_carry__0_n_6\,
      I2 => \string_cent_decenas[1]5__79_carry__1_n_6\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__100_carry__0_n_5\,
      I5 => \string_cent_decenas[1]5__79_carry__1_n_1\,
      O => \string_cent_decenas[1]5__124_carry__5_i_1_n_0\
    );
\string_cent_decenas[1]5__124_carry__5_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"54D580FE80FE54D5"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I1 => \string_cent_decenas[1]5__100_carry__0_n_7\,
      I2 => \string_cent_decenas[1]5__79_carry__1_n_7\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__100_carry__0_n_6\,
      I5 => \string_cent_decenas[1]5__79_carry__1_n_6\,
      O => \string_cent_decenas[1]5__124_carry__5_i_2_n_0\
    );
\string_cent_decenas[1]5__124_carry__5_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEF80107E0801F7F"
    )
        port map (
      I0 => \string_cent_decenas[1]5__100_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I2 => \string_cent_decenas[1]5__100_carry__0_n_4\,
      I3 => \string_cent_decenas[1]5__79_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__100_carry__1_n_7\,
      I5 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      O => \string_cent_decenas[1]5__124_carry__5_i_3_n_0\
    );
\string_cent_decenas[1]5__124_carry__5_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__5_i_1_n_0\,
      I1 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5__100_carry__0_n_4\,
      I3 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I4 => \string_cent_decenas[1]5__79_carry__1_n_1\,
      I5 => \string_cent_decenas[1]5__100_carry__0_n_5\,
      O => \string_cent_decenas[1]5__124_carry__5_i_4_n_0\
    );
\string_cent_decenas[1]5__124_carry__5_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6996966996696996"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__5_i_2_n_0\,
      I1 => \string_cent_decenas[1]5__124_carry__5_i_6_n_0\,
      I2 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I3 => \string_cent_decenas[1]5__79_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I5 => \string_cent_decenas[1]5__100_carry__0_n_5\,
      O => \string_cent_decenas[1]5__124_carry__5_i_5_n_0\
    );
\string_cent_decenas[1]5__124_carry__5_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I1 => \string_cent_decenas[1]5__100_carry__0_n_6\,
      I2 => \string_cent_decenas[1]5__79_carry__1_n_6\,
      O => \string_cent_decenas[1]5__124_carry__5_i_6_n_0\
    );
\string_cent_decenas[1]5__124_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry_n_7\,
      I1 => \string_cent_decenas[1]5_carry__1_n_7\,
      O => \string_cent_decenas[1]5__124_carry_i_1_n_0\
    );
\string_cent_decenas[1]5__124_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__0_n_4\,
      I1 => \i___0_carry_i_2_n_0\,
      O => \string_cent_decenas[1]5__124_carry_i_2_n_0\
    );
\string_cent_decenas[1]5__124_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__0_n_5\,
      I1 => \i___169_carry_i_5_n_0\,
      O => \string_cent_decenas[1]5__124_carry_i_3_n_0\
    );
\string_cent_decenas[1]5__124_carry_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__0_n_6\,
      I1 => \^total_reg[0]\,
      O => \string_cent_decenas[1]5__124_carry_i_4_n_0\
    );
\string_cent_decenas[1]5__124_carry_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__1_n_7\,
      I1 => \string_cent_decenas[1]5__27_carry_n_7\,
      I2 => \string_cent_decenas[1]5_carry__1_n_6\,
      I3 => \string_cent_decenas[1]5__27_carry_n_6\,
      O => \string_cent_decenas[1]5__124_carry_i_5_n_0\
    );
\string_cent_decenas[1]5__124_carry_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \i___0_carry_i_2_n_0\,
      I1 => \string_cent_decenas[1]5_carry__0_n_4\,
      I2 => \string_cent_decenas[1]5_carry__1_n_7\,
      I3 => \string_cent_decenas[1]5__27_carry_n_7\,
      O => \string_cent_decenas[1]5__124_carry_i_6_n_0\
    );
\string_cent_decenas[1]5__124_carry_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4BB4"
    )
        port map (
      I0 => \i___169_carry_i_5_n_0\,
      I1 => \string_cent_decenas[1]5_carry__0_n_5\,
      I2 => \i___0_carry_i_2_n_0\,
      I3 => \string_cent_decenas[1]5_carry__0_n_4\,
      O => \string_cent_decenas[1]5__124_carry_i_7_n_0\
    );
\string_cent_decenas[1]5__124_carry_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7887"
    )
        port map (
      I0 => \^total_reg[0]\,
      I1 => \string_cent_decenas[1]5_carry__0_n_6\,
      I2 => \i___169_carry_i_5_n_0\,
      I3 => \string_cent_decenas[1]5_carry__0_n_5\,
      O => \string_cent_decenas[1]5__124_carry_i_8_n_0\
    );
\string_cent_decenas[1]5__180_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3 downto 0) => \NLW_string_cent_decenas[1]5__180_carry_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \string_cent_decenas[1]5__124_carry__5_n_6\,
      DI(0) => '0',
      O(3) => \NLW_string_cent_decenas[1]5__180_carry_O_UNCONNECTED\(3),
      O(2) => \string_cent_decenas[1]5__180_carry_n_5\,
      O(1) => \string_cent_decenas[1]5__180_carry_n_6\,
      O(0) => \string_cent_decenas[1]5__180_carry_n_7\,
      S(3) => '0',
      S(2) => \string_cent_decenas[1]5__180_carry_i_1_n_0\,
      S(1) => \string_cent_decenas[1]5__180_carry_i_2_n_0\,
      S(0) => \string_cent_decenas[1]5__124_carry__5_n_7\
    );
\string_cent_decenas[1]5__180_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__5_n_7\,
      I1 => \string_cent_decenas[1]5__124_carry__5_n_5\,
      O => \string_cent_decenas[1]5__180_carry_i_1_n_0\
    );
\string_cent_decenas[1]5__180_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__5_n_6\,
      I1 => \string_cent_decenas[1]5__124_carry__4_n_4\,
      O => \string_cent_decenas[1]5__180_carry_i_2_n_0\
    );
\string_cent_decenas[1]5__186_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5__186_carry_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5__186_carry_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '1',
      DI(3) => cent(3),
      DI(2) => \i___0_carry_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__186_carry_i_3_n_0\,
      DI(0) => \^total_reg[0]\,
      O(3) => \string_cent_decenas[1]5__186_carry_n_4\,
      O(2) => \string_cent_decenas[1]5__186_carry_n_5\,
      O(1) => \string_cent_decenas[1]5__186_carry_n_6\,
      O(0) => \string_cent_decenas[1]5\(0),
      S(3) => \string_cent_decenas[1]5__186_carry_i_4_n_0\,
      S(2) => \string_cent_decenas[1]5__186_carry_i_5_n_0\,
      S(1) => \string_cent_decenas[1]5__186_carry_i_6_n_0\,
      S(0) => \string_cent_decenas[1]5__186_carry_i_7_n_0\
    );
\string_cent_decenas[1]5__186_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__186_carry_n_0\,
      CO(3 downto 0) => \NLW_string_cent_decenas[1]5__186_carry__0_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_string_cent_decenas[1]5__186_carry__0_O_UNCONNECTED\(3 downto 1),
      O(0) => \string_cent_decenas[1]5__186_carry__0_n_7\,
      S(3 downto 1) => B"000",
      S(0) => \string_cent_decenas[1]5__186_carry__0_i_1_n_0\
    );
\string_cent_decenas[1]5__186_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \string_cent_decenas[1]5__180_carry_n_5\,
      I1 => cent(4),
      O => \string_cent_decenas[1]5__186_carry__0_i_1_n_0\
    );
\string_cent_decenas[1]5__186_carry_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCC3"
    )
        port map (
      I0 => Q(3),
      I1 => \euros1__339_carry_n_4\,
      I2 => \string_cent_decenas[1]5__186_carry_i_8_n_0\,
      I3 => \euros1__339_carry_n_5\,
      I4 => \^total_reg[29]\,
      O => cent(3)
    );
\string_cent_decenas[1]5__186_carry_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(1),
      I1 => \^total_reg[29]\,
      I2 => p_1_in(1),
      O => \string_cent_decenas[1]5__186_carry_i_3_n_0\
    );
\string_cent_decenas[1]5__186_carry_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(3),
      I1 => \string_cent_decenas[1]5__180_carry_n_6\,
      O => \string_cent_decenas[1]5__186_carry_i_4_n_0\
    );
\string_cent_decenas[1]5__186_carry_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \i___0_carry_i_2_n_0\,
      I1 => \string_cent_decenas[1]5__180_carry_n_7\,
      O => \string_cent_decenas[1]5__186_carry_i_5_n_0\
    );
\string_cent_decenas[1]5__186_carry_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \i___169_carry_i_5_n_0\,
      I1 => \string_cent_decenas[1]5__124_carry__4_n_4\,
      O => \string_cent_decenas[1]5__186_carry_i_6_n_0\
    );
\string_cent_decenas[1]5__186_carry_i_7\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^total_reg[0]\,
      O => \string_cent_decenas[1]5__186_carry_i_7_n_0\
    );
\string_cent_decenas[1]5__186_carry_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001555555555555"
    )
        port map (
      I0 => \euros1__339_carry__0_n_4\,
      I1 => \euros1__339_carry_n_5\,
      I2 => \euros1__339_carry_n_4\,
      I3 => \euros1__339_carry__0_n_7\,
      I4 => \euros1__339_carry__0_n_6\,
      I5 => \euros1__339_carry__0_n_5\,
      O => \string_cent_decenas[1]5__186_carry_i_8_n_0\
    );
\string_cent_decenas[1]5__27_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5__27_carry_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5__27_carry_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__27_carry_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__27_carry_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__27_carry_i_3_n_0\,
      DI(0) => '0',
      O(3) => \string_cent_decenas[1]5__27_carry_n_4\,
      O(2) => \string_cent_decenas[1]5__27_carry_n_5\,
      O(1) => \string_cent_decenas[1]5__27_carry_n_6\,
      O(0) => \string_cent_decenas[1]5__27_carry_n_7\,
      S(3) => \string_cent_decenas[1]5__27_carry_i_4_n_0\,
      S(2) => \string_cent_decenas[1]5__27_carry_i_5_n_0\,
      S(1) => \string_cent_decenas[1]5__27_carry_i_6_n_0\,
      S(0) => \string_cent_decenas[1]5__27_carry_i_7_n_0\
    );
\string_cent_decenas[1]5__27_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__27_carry_n_0\,
      CO(3) => \string_cent_decenas[1]5__27_carry__0_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5__27_carry__0_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__27_carry__0_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__27_carry__0_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__27_carry__0_i_3_n_0\,
      DI(0) => \string_cent_decenas[1]5__27_carry__0_i_4_n_0\,
      O(3) => \string_cent_decenas[1]5__27_carry__0_n_4\,
      O(2) => \string_cent_decenas[1]5__27_carry__0_n_5\,
      O(1) => \string_cent_decenas[1]5__27_carry__0_n_6\,
      O(0) => \string_cent_decenas[1]5__27_carry__0_n_7\,
      S(3) => \string_cent_decenas[1]5__27_carry__0_i_5_n_0\,
      S(2) => \string_cent_decenas[1]5__27_carry__0_i_6_n_0\,
      S(1) => \string_cent_decenas[1]5__27_carry__0_i_7_n_0\,
      S(0) => \string_cent_decenas[1]5__27_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5__27_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => cent(4),
      I1 => cent(6),
      O => \string_cent_decenas[1]5__27_carry__0_i_1_n_0\
    );
\string_cent_decenas[1]5__27_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => cent(3),
      I1 => cent(5),
      O => \string_cent_decenas[1]5__27_carry__0_i_2_n_0\
    );
\string_cent_decenas[1]5__27_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => cent(4),
      I1 => \i___0_carry_i_2_n_0\,
      O => \string_cent_decenas[1]5__27_carry__0_i_3_n_0\
    );
\string_cent_decenas[1]5__27_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(4),
      I1 => \i___0_carry_i_2_n_0\,
      O => \string_cent_decenas[1]5__27_carry__0_i_4_n_0\
    );
\string_cent_decenas[1]5__27_carry__0_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4B"
    )
        port map (
      I0 => cent(6),
      I1 => cent(4),
      I2 => cent(5),
      O => \string_cent_decenas[1]5__27_carry__0_i_5_n_0\
    );
\string_cent_decenas[1]5__27_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B44B"
    )
        port map (
      I0 => cent(5),
      I1 => cent(3),
      I2 => cent(6),
      I3 => cent(4),
      O => \string_cent_decenas[1]5__27_carry__0_i_6_n_0\
    );
\string_cent_decenas[1]5__27_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"D22D"
    )
        port map (
      I0 => \i___0_carry_i_2_n_0\,
      I1 => cent(4),
      I2 => cent(5),
      I3 => cent(3),
      O => \string_cent_decenas[1]5__27_carry__0_i_7_n_0\
    );
\string_cent_decenas[1]5__27_carry__0_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"D42B2BD4"
    )
        port map (
      I0 => cent(6),
      I1 => \i___169_carry_i_5_n_0\,
      I2 => cent(3),
      I3 => \i___0_carry_i_2_n_0\,
      I4 => cent(4),
      O => \string_cent_decenas[1]5__27_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5__27_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__27_carry__0_n_0\,
      CO(3) => \NLW_string_cent_decenas[1]5__27_carry__1_CO_UNCONNECTED\(3),
      CO(2) => \string_cent_decenas[1]5__27_carry__1_n_1\,
      CO(1 downto 0) => \NLW_string_cent_decenas[1]5__27_carry__1_CO_UNCONNECTED\(1 downto 0),
      CYINIT => '0',
      DI(3 downto 1) => B"001",
      DI(0) => cent(5),
      O(3 downto 2) => \NLW_string_cent_decenas[1]5__27_carry__1_O_UNCONNECTED\(3 downto 2),
      O(1) => \string_cent_decenas[1]5__27_carry__1_n_6\,
      O(0) => \string_cent_decenas[1]5__27_carry__1_n_7\,
      S(3 downto 2) => B"01",
      S(1) => \string_cent_decenas[1]5__27_carry__1_i_2_n_0\,
      S(0) => \string_cent_decenas[1]5__27_carry__1_i_3_n_0\
    );
\string_cent_decenas[1]5__27_carry__1_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BB88BB8B88BB8888"
    )
        port map (
      I0 => Q(5),
      I1 => \^total_reg[29]\,
      I2 => \euros1__339_carry__0_n_5\,
      I3 => \string_cent_decenas[1]5__27_carry__1_i_4_n_0\,
      I4 => \euros1__339_carry__0_n_4\,
      I5 => \euros1__339_carry__0_n_6\,
      O => cent(5)
    );
\string_cent_decenas[1]5__27_carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(6),
      O => \string_cent_decenas[1]5__27_carry__1_i_2_n_0\
    );
\string_cent_decenas[1]5__27_carry__1_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(5),
      I1 => cent(6),
      O => \string_cent_decenas[1]5__27_carry__1_i_3_n_0\
    );
\string_cent_decenas[1]5__27_carry__1_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \euros1__339_carry_n_5\,
      I1 => \euros1__339_carry_n_4\,
      I2 => \euros1__339_carry__0_n_7\,
      O => \string_cent_decenas[1]5__27_carry__1_i_4_n_0\
    );
\string_cent_decenas[1]5__27_carry_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \^total_reg[0]\,
      I1 => \i___0_carry_i_2_n_0\,
      I2 => cent(5),
      O => \string_cent_decenas[1]5__27_carry_i_1_n_0\
    );
\string_cent_decenas[1]5__27_carry_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^total_reg[0]\,
      I1 => \i___0_carry_i_2_n_0\,
      I2 => cent(5),
      O => \string_cent_decenas[1]5__27_carry_i_2_n_0\
    );
\string_cent_decenas[1]5__27_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => cent(3),
      I1 => \^total_reg[0]\,
      O => \string_cent_decenas[1]5__27_carry_i_3_n_0\
    );
\string_cent_decenas[1]5__27_carry_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry_i_1_n_0\,
      I1 => \i___169_carry_i_5_n_0\,
      I2 => cent(6),
      I3 => cent(3),
      O => \string_cent_decenas[1]5__27_carry_i_4_n_0\
    );
\string_cent_decenas[1]5__27_carry_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696969"
    )
        port map (
      I0 => \^total_reg[0]\,
      I1 => \i___0_carry_i_2_n_0\,
      I2 => cent(5),
      I3 => \i___169_carry_i_5_n_0\,
      I4 => cent(4),
      O => \string_cent_decenas[1]5__27_carry_i_5_n_0\
    );
\string_cent_decenas[1]5__27_carry_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"D22D"
    )
        port map (
      I0 => \^total_reg[0]\,
      I1 => cent(3),
      I2 => \i___169_carry_i_5_n_0\,
      I3 => cent(4),
      O => \string_cent_decenas[1]5__27_carry_i_6_n_0\
    );
\string_cent_decenas[1]5__27_carry_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(3),
      I1 => \^total_reg[0]\,
      O => \string_cent_decenas[1]5__27_carry_i_7_n_0\
    );
\string_cent_decenas[1]5__55_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5__55_carry_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5__55_carry_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__55_carry_i_1_n_0\,
      DI(2) => \^total_reg[0]\,
      DI(1 downto 0) => B"01",
      O(3) => \string_cent_decenas[1]5__55_carry_n_4\,
      O(2) => \string_cent_decenas[1]5__55_carry_n_5\,
      O(1) => \string_cent_decenas[1]5__55_carry_n_6\,
      O(0) => \NLW_string_cent_decenas[1]5__55_carry_O_UNCONNECTED\(0),
      S(3) => \string_cent_decenas[1]5__55_carry_i_2_n_0\,
      S(2) => \string_cent_decenas[1]5__55_carry_i_3_n_0\,
      S(1) => \i___169_carry_i_5_n_0\,
      S(0) => \string_cent_decenas[1]5__55_carry_i_5_n_0\
    );
\string_cent_decenas[1]5__55_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__55_carry_n_0\,
      CO(3) => \string_cent_decenas[1]5__55_carry__0_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5__55_carry__0_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__55_carry__0_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5_carry__0_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__55_carry__0_i_2_n_0\,
      DI(0) => \i___0_carry_i_2_n_0\,
      O(3) => \string_cent_decenas[1]5__55_carry__0_n_4\,
      O(2) => \string_cent_decenas[1]5__55_carry__0_n_5\,
      O(1) => \string_cent_decenas[1]5__55_carry__0_n_6\,
      O(0) => \string_cent_decenas[1]5__55_carry__0_n_7\,
      S(3) => \string_cent_decenas[1]5__55_carry__0_i_4_n_0\,
      S(2) => \string_cent_decenas[1]5__55_carry__0_i_5_n_0\,
      S(1) => \string_cent_decenas[1]5__55_carry__0_i_6_n_0\,
      S(0) => \string_cent_decenas[1]5__55_carry__0_i_7_n_0\
    );
\string_cent_decenas[1]5__55_carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8E"
    )
        port map (
      I0 => cent(4),
      I1 => \i___0_carry_i_2_n_0\,
      I2 => cent(6),
      O => \string_cent_decenas[1]5__55_carry__0_i_1_n_0\
    );
\string_cent_decenas[1]5__55_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \i___169_carry_i_5_n_0\,
      I1 => cent(5),
      I2 => cent(3),
      O => \string_cent_decenas[1]5__55_carry__0_i_2_n_0\
    );
\string_cent_decenas[1]5__55_carry__0_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"2BD4D42B"
    )
        port map (
      I0 => cent(6),
      I1 => \i___0_carry_i_2_n_0\,
      I2 => cent(4),
      I3 => cent(5),
      I4 => cent(3),
      O => \string_cent_decenas[1]5__55_carry__0_i_4_n_0\
    );
\string_cent_decenas[1]5__55_carry__0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4DB2B24DB24D4DB2"
    )
        port map (
      I0 => cent(5),
      I1 => cent(3),
      I2 => \i___169_carry_i_5_n_0\,
      I3 => cent(4),
      I4 => \i___0_carry_i_2_n_0\,
      I5 => cent(6),
      O => \string_cent_decenas[1]5__55_carry__0_i_5_n_0\
    );
\string_cent_decenas[1]5__55_carry__0_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96699696"
    )
        port map (
      I0 => cent(3),
      I1 => cent(5),
      I2 => \i___169_carry_i_5_n_0\,
      I3 => cent(4),
      I4 => \^total_reg[0]\,
      O => \string_cent_decenas[1]5__55_carry__0_i_6_n_0\
    );
\string_cent_decenas[1]5__55_carry__0_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^total_reg[0]\,
      I1 => cent(4),
      I2 => \i___0_carry_i_2_n_0\,
      O => \string_cent_decenas[1]5__55_carry__0_i_7_n_0\
    );
\string_cent_decenas[1]5__55_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__55_carry__0_n_0\,
      CO(3) => \string_cent_decenas[1]5__55_carry__1_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5__55_carry__1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => '1',
      DI(2) => \string_cent_decenas[1]5__55_carry__1_i_1_n_0\,
      DI(1) => \string_cent_decenas[1]5__55_carry__1_i_2_n_0\,
      DI(0) => \string_cent_decenas[1]5__55_carry__1_i_3_n_0\,
      O(3) => \string_cent_decenas[1]5__55_carry__1_n_4\,
      O(2) => \string_cent_decenas[1]5__55_carry__1_n_5\,
      O(1) => \string_cent_decenas[1]5__55_carry__1_n_6\,
      O(0) => \string_cent_decenas[1]5__55_carry__1_n_7\,
      S(3) => \string_cent_decenas[1]5__55_carry__1_i_4_n_0\,
      S(2) => \string_cent_decenas[1]5__55_carry__1_i_5_n_0\,
      S(1) => \string_cent_decenas[1]5__55_carry__1_i_6_n_0\,
      S(0) => \string_cent_decenas[1]5__55_carry__1_i_7_n_0\
    );
\string_cent_decenas[1]5__55_carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(6),
      O => \string_cent_decenas[1]5__55_carry__1_i_1_n_0\
    );
\string_cent_decenas[1]5__55_carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(5),
      O => \string_cent_decenas[1]5__55_carry__1_i_2_n_0\
    );
\string_cent_decenas[1]5__55_carry__1_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(4),
      I1 => cent(6),
      O => \string_cent_decenas[1]5__55_carry__1_i_3_n_0\
    );
\string_cent_decenas[1]5__55_carry__1_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(6),
      O => \string_cent_decenas[1]5__55_carry__1_i_4_n_0\
    );
\string_cent_decenas[1]5__55_carry__1_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(5),
      I1 => cent(6),
      O => \string_cent_decenas[1]5__55_carry__1_i_5_n_0\
    );
\string_cent_decenas[1]5__55_carry__1_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E1"
    )
        port map (
      I0 => cent(6),
      I1 => cent(4),
      I2 => cent(5),
      O => \string_cent_decenas[1]5__55_carry__1_i_6_n_0\
    );
\string_cent_decenas[1]5__55_carry__1_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1EE1"
    )
        port map (
      I0 => cent(5),
      I1 => cent(3),
      I2 => cent(6),
      I3 => cent(4),
      O => \string_cent_decenas[1]5__55_carry__1_i_7_n_0\
    );
\string_cent_decenas[1]5__55_carry_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(1),
      I1 => \^total_reg[29]\,
      I2 => p_1_in(1),
      O => \string_cent_decenas[1]5__55_carry_i_1_n_0\
    );
\string_cent_decenas[1]5__55_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \i___169_carry_i_5_n_0\,
      I1 => cent(3),
      O => \string_cent_decenas[1]5__55_carry_i_2_n_0\
    );
\string_cent_decenas[1]5__55_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^total_reg[0]\,
      I1 => \i___0_carry_i_2_n_0\,
      O => \string_cent_decenas[1]5__55_carry_i_3_n_0\
    );
\string_cent_decenas[1]5__55_carry_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEF00000020"
    )
        port map (
      I0 => Q(0),
      I1 => \string_cent_decenas[1]5_carry_i_8_n_0\,
      I2 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I3 => \^total_reg[20]\,
      I4 => \string_cent_decenas[1]5_carry_i_11_n_0\,
      I5 => p_1_in(0),
      O => \string_cent_decenas[1]5__55_carry_i_5_n_0\
    );
\string_cent_decenas[1]5__79_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5__79_carry_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5__79_carry_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__27_carry_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__79_carry_i_1_n_0\,
      DI(1) => \string_cent_decenas[1]5__79_carry_i_2_n_0\,
      DI(0) => '0',
      O(3) => \string_cent_decenas[1]5__79_carry_n_4\,
      O(2) => \string_cent_decenas[1]5__79_carry_n_5\,
      O(1) => \string_cent_decenas[1]5__79_carry_n_6\,
      O(0) => \NLW_string_cent_decenas[1]5__79_carry_O_UNCONNECTED\(0),
      S(3) => \string_cent_decenas[1]5__79_carry_i_3_n_0\,
      S(2) => \string_cent_decenas[1]5__79_carry_i_4_n_0\,
      S(1) => \string_cent_decenas[1]5__79_carry_i_5_n_0\,
      S(0) => \string_cent_decenas[1]5__79_carry_i_6_n_0\
    );
\string_cent_decenas[1]5__79_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__79_carry_n_0\,
      CO(3) => \string_cent_decenas[1]5__79_carry__0_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5__79_carry__0_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__79_carry__0_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__79_carry__0_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__79_carry__0_i_3_n_0\,
      DI(0) => \string_cent_decenas[1]5__79_carry__0_i_4_n_0\,
      O(3) => \string_cent_decenas[1]5__79_carry__0_n_4\,
      O(2) => \string_cent_decenas[1]5__79_carry__0_n_5\,
      O(1) => \string_cent_decenas[1]5__79_carry__0_n_6\,
      O(0) => \string_cent_decenas[1]5__79_carry__0_n_7\,
      S(3) => \string_cent_decenas[1]5__79_carry__0_i_5_n_0\,
      S(2) => \string_cent_decenas[1]5__79_carry__0_i_6_n_0\,
      S(1) => \string_cent_decenas[1]5__79_carry__0_i_7_n_0\,
      S(0) => \string_cent_decenas[1]5__79_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5__79_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => cent(4),
      I1 => cent(6),
      O => \string_cent_decenas[1]5__79_carry__0_i_1_n_0\
    );
\string_cent_decenas[1]5__79_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => cent(3),
      I1 => cent(5),
      O => \string_cent_decenas[1]5__79_carry__0_i_2_n_0\
    );
\string_cent_decenas[1]5__79_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => cent(4),
      I1 => \i___0_carry_i_2_n_0\,
      O => \string_cent_decenas[1]5__79_carry__0_i_3_n_0\
    );
\string_cent_decenas[1]5__79_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(4),
      I1 => \i___0_carry_i_2_n_0\,
      O => \string_cent_decenas[1]5__79_carry__0_i_4_n_0\
    );
\string_cent_decenas[1]5__79_carry__0_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4B"
    )
        port map (
      I0 => cent(6),
      I1 => cent(4),
      I2 => cent(5),
      O => \string_cent_decenas[1]5__79_carry__0_i_5_n_0\
    );
\string_cent_decenas[1]5__79_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B44B"
    )
        port map (
      I0 => cent(5),
      I1 => cent(3),
      I2 => cent(6),
      I3 => cent(4),
      O => \string_cent_decenas[1]5__79_carry__0_i_6_n_0\
    );
\string_cent_decenas[1]5__79_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"D22D"
    )
        port map (
      I0 => \i___0_carry_i_2_n_0\,
      I1 => cent(4),
      I2 => cent(5),
      I3 => cent(3),
      O => \string_cent_decenas[1]5__79_carry__0_i_7_n_0\
    );
\string_cent_decenas[1]5__79_carry__0_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"D42B2BD4"
    )
        port map (
      I0 => cent(6),
      I1 => \i___169_carry_i_5_n_0\,
      I2 => cent(3),
      I3 => \i___0_carry_i_2_n_0\,
      I4 => cent(4),
      O => \string_cent_decenas[1]5__79_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5__79_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__79_carry__0_n_0\,
      CO(3) => \NLW_string_cent_decenas[1]5__79_carry__1_CO_UNCONNECTED\(3),
      CO(2) => \string_cent_decenas[1]5__79_carry__1_n_1\,
      CO(1 downto 0) => \NLW_string_cent_decenas[1]5__79_carry__1_CO_UNCONNECTED\(1 downto 0),
      CYINIT => '0',
      DI(3 downto 1) => B"001",
      DI(0) => cent(5),
      O(3 downto 2) => \NLW_string_cent_decenas[1]5__79_carry__1_O_UNCONNECTED\(3 downto 2),
      O(1) => \string_cent_decenas[1]5__79_carry__1_n_6\,
      O(0) => \string_cent_decenas[1]5__79_carry__1_n_7\,
      S(3 downto 2) => B"01",
      S(1) => \string_cent_decenas[1]5__79_carry__1_i_1_n_0\,
      S(0) => \string_cent_decenas[1]5__79_carry__1_i_2_n_0\
    );
\string_cent_decenas[1]5__79_carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(6),
      O => \string_cent_decenas[1]5__79_carry__1_i_1_n_0\
    );
\string_cent_decenas[1]5__79_carry__1_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(5),
      I1 => cent(6),
      O => \string_cent_decenas[1]5__79_carry__1_i_2_n_0\
    );
\string_cent_decenas[1]5__79_carry_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^total_reg[0]\,
      I1 => \i___0_carry_i_2_n_0\,
      I2 => cent(5),
      O => \string_cent_decenas[1]5__79_carry_i_1_n_0\
    );
\string_cent_decenas[1]5__79_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => cent(3),
      I1 => \^total_reg[0]\,
      O => \string_cent_decenas[1]5__79_carry_i_2_n_0\
    );
\string_cent_decenas[1]5__79_carry_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry_i_1_n_0\,
      I1 => \i___169_carry_i_5_n_0\,
      I2 => cent(6),
      I3 => cent(3),
      O => \string_cent_decenas[1]5__79_carry_i_3_n_0\
    );
\string_cent_decenas[1]5__79_carry_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696969"
    )
        port map (
      I0 => \^total_reg[0]\,
      I1 => \i___0_carry_i_2_n_0\,
      I2 => cent(5),
      I3 => \i___169_carry_i_5_n_0\,
      I4 => cent(4),
      O => \string_cent_decenas[1]5__79_carry_i_4_n_0\
    );
\string_cent_decenas[1]5__79_carry_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"D22D"
    )
        port map (
      I0 => \^total_reg[0]\,
      I1 => cent(3),
      I2 => \i___169_carry_i_5_n_0\,
      I3 => cent(4),
      O => \string_cent_decenas[1]5__79_carry_i_5_n_0\
    );
\string_cent_decenas[1]5__79_carry_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(3),
      I1 => \^total_reg[0]\,
      O => \string_cent_decenas[1]5__79_carry_i_6_n_0\
    );
\string_cent_decenas[1]5_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5_carry_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5_carry_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5_carry_i_1_n_0\,
      DI(2) => \^total_reg[0]\,
      DI(1 downto 0) => B"01",
      O(3 downto 1) => \NLW_string_cent_decenas[1]5_carry_O_UNCONNECTED\(3 downto 1),
      O(0) => \string_cent_decenas[1]5_carry_n_7\,
      S(3) => \string_cent_decenas[1]5_carry_i_3_n_0\,
      S(2) => \string_cent_decenas[1]5_carry_i_4_n_0\,
      S(1) => \i___169_carry_i_5_n_0\,
      S(0) => \string_cent_decenas[1]5_carry_i_6_n_0\
    );
\string_cent_decenas[1]5_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_carry_n_0\,
      CO(3) => \string_cent_decenas[1]5_carry__0_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5_carry__0_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5_carry__0_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5_carry__0_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5_carry__0_i_3_n_0\,
      DI(0) => \i___0_carry_i_2_n_0\,
      O(3) => \string_cent_decenas[1]5_carry__0_n_4\,
      O(2) => \string_cent_decenas[1]5_carry__0_n_5\,
      O(1) => \string_cent_decenas[1]5_carry__0_n_6\,
      O(0) => \NLW_string_cent_decenas[1]5_carry__0_O_UNCONNECTED\(0),
      S(3) => \string_cent_decenas[1]5_carry__0_i_5_n_0\,
      S(2) => \string_cent_decenas[1]5_carry__0_i_6_n_0\,
      S(1) => \string_cent_decenas[1]5_carry__0_i_7_n_0\,
      S(0) => \string_cent_decenas[1]5_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5_carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8E"
    )
        port map (
      I0 => cent(4),
      I1 => \i___0_carry_i_2_n_0\,
      I2 => cent(6),
      O => \string_cent_decenas[1]5_carry__0_i_1_n_0\
    );
\string_cent_decenas[1]5_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4D"
    )
        port map (
      I0 => \i___169_carry_i_5_n_0\,
      I1 => cent(3),
      I2 => cent(5),
      O => \string_cent_decenas[1]5_carry__0_i_2_n_0\
    );
\string_cent_decenas[1]5_carry__0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \i___169_carry_i_5_n_0\,
      I1 => cent(5),
      I2 => cent(3),
      O => \string_cent_decenas[1]5_carry__0_i_3_n_0\
    );
\string_cent_decenas[1]5_carry__0_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"2BD4D42B"
    )
        port map (
      I0 => cent(6),
      I1 => \i___0_carry_i_2_n_0\,
      I2 => cent(4),
      I3 => cent(5),
      I4 => cent(3),
      O => \string_cent_decenas[1]5_carry__0_i_5_n_0\
    );
\string_cent_decenas[1]5_carry__0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4DB2B24DB24D4DB2"
    )
        port map (
      I0 => cent(5),
      I1 => cent(3),
      I2 => \i___169_carry_i_5_n_0\,
      I3 => cent(4),
      I4 => \i___0_carry_i_2_n_0\,
      I5 => cent(6),
      O => \string_cent_decenas[1]5_carry__0_i_6_n_0\
    );
\string_cent_decenas[1]5_carry__0_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96699696"
    )
        port map (
      I0 => cent(3),
      I1 => cent(5),
      I2 => \i___169_carry_i_5_n_0\,
      I3 => cent(4),
      I4 => \^total_reg[0]\,
      O => \string_cent_decenas[1]5_carry__0_i_7_n_0\
    );
\string_cent_decenas[1]5_carry__0_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^total_reg[0]\,
      I1 => cent(4),
      I2 => \i___0_carry_i_2_n_0\,
      O => \string_cent_decenas[1]5_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_carry__0_n_0\,
      CO(3) => \string_cent_decenas[1]5_carry__1_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5_carry__1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => '1',
      DI(2) => \string_cent_decenas[1]5_carry__1_i_1_n_0\,
      DI(1) => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      DI(0) => \string_cent_decenas[1]5_carry__1_i_3_n_0\,
      O(3) => \string_cent_decenas[1]5_carry__1_n_4\,
      O(2) => \string_cent_decenas[1]5_carry__1_n_5\,
      O(1) => \string_cent_decenas[1]5_carry__1_n_6\,
      O(0) => \string_cent_decenas[1]5_carry__1_n_7\,
      S(3) => \string_cent_decenas[1]5_carry__1_i_4_n_0\,
      S(2) => \string_cent_decenas[1]5_carry__1_i_5_n_0\,
      S(1) => \string_cent_decenas[1]5_carry__1_i_6_n_0\,
      S(0) => \string_cent_decenas[1]5_carry__1_i_7_n_0\
    );
\string_cent_decenas[1]5_carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(6),
      O => \string_cent_decenas[1]5_carry__1_i_1_n_0\
    );
\string_cent_decenas[1]5_carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(5),
      O => \string_cent_decenas[1]5_carry__1_i_2_n_0\
    );
\string_cent_decenas[1]5_carry__1_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(4),
      I1 => cent(6),
      O => \string_cent_decenas[1]5_carry__1_i_3_n_0\
    );
\string_cent_decenas[1]5_carry__1_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(6),
      O => \string_cent_decenas[1]5_carry__1_i_4_n_0\
    );
\string_cent_decenas[1]5_carry__1_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(5),
      I1 => cent(6),
      O => \string_cent_decenas[1]5_carry__1_i_5_n_0\
    );
\string_cent_decenas[1]5_carry__1_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E1"
    )
        port map (
      I0 => cent(6),
      I1 => cent(4),
      I2 => cent(5),
      O => \string_cent_decenas[1]5_carry__1_i_6_n_0\
    );
\string_cent_decenas[1]5_carry__1_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1EE1"
    )
        port map (
      I0 => cent(5),
      I1 => cent(3),
      I2 => cent(6),
      I3 => cent(4),
      O => \string_cent_decenas[1]5_carry__1_i_7_n_0\
    );
\string_cent_decenas[1]5_carry_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(1),
      I1 => \^total_reg[29]\,
      I2 => p_1_in(1),
      O => \string_cent_decenas[1]5_carry_i_1_n_0\
    );
\string_cent_decenas[1]5_carry_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFEFFF"
    )
        port map (
      I0 => Q(20),
      I1 => Q(22),
      I2 => \string_cent_decenas[1]5_carry_i_18_n_0\,
      I3 => \string_cent_decenas[1]5_carry_i_19_n_0\,
      I4 => Q(12),
      I5 => Q(14),
      O => \^total_reg[20]\
    );
\string_cent_decenas[1]5_carry_i_11\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"88888880"
    )
        port map (
      I0 => Q(6),
      I1 => Q(5),
      I2 => Q(3),
      I3 => Q(2),
      I4 => Q(4),
      O => \string_cent_decenas[1]5_carry_i_11_n_0\
    );
\string_cent_decenas[1]5_carry_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => Q(1),
      I1 => \^total_reg[29]\,
      I2 => p_1_in(1),
      O => \i___169_carry_i_5_n_0\
    );
\string_cent_decenas[1]5_carry_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AAC3"
    )
        port map (
      I0 => Q(2),
      I1 => \euros1__339_carry_n_5\,
      I2 => \string_cent_decenas[1]5__186_carry_i_8_n_0\,
      I3 => \^total_reg[29]\,
      O => \i___0_carry_i_2_n_0\
    );
\string_cent_decenas[1]5_carry_i_14\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => Q(26),
      I1 => Q(27),
      O => \^total_reg[26]\
    );
\string_cent_decenas[1]5_carry_i_15\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => Q(19),
      I1 => Q(21),
      O => \string_cent_decenas[1]5_carry_i_15_n_0\
    );
\string_cent_decenas[1]5_carry_i_16\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => Q(11),
      I1 => Q(13),
      O => \string_cent_decenas[1]5_carry_i_16_n_0\
    );
\string_cent_decenas[1]5_carry_i_17\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => Q(7),
      I1 => Q(9),
      O => \string_cent_decenas[1]5_carry_i_17_n_0\
    );
\string_cent_decenas[1]5_carry_i_18\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => Q(8),
      I1 => Q(10),
      O => \string_cent_decenas[1]5_carry_i_18_n_0\
    );
\string_cent_decenas[1]5_carry_i_19\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => Q(16),
      I1 => Q(18),
      O => \string_cent_decenas[1]5_carry_i_19_n_0\
    );
\string_cent_decenas[1]5_carry_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEF00000020"
    )
        port map (
      I0 => Q(0),
      I1 => \string_cent_decenas[1]5_carry_i_8_n_0\,
      I2 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I3 => \^total_reg[20]\,
      I4 => \string_cent_decenas[1]5_carry_i_11_n_0\,
      I5 => p_1_in(0),
      O => \^total_reg[0]\
    );
\string_cent_decenas[1]5_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \i___169_carry_i_5_n_0\,
      I1 => cent(3),
      O => \string_cent_decenas[1]5_carry_i_3_n_0\
    );
\string_cent_decenas[1]5_carry_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^total_reg[0]\,
      I1 => \i___0_carry_i_2_n_0\,
      O => \string_cent_decenas[1]5_carry_i_4_n_0\
    );
\string_cent_decenas[1]5_carry_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEF00000020"
    )
        port map (
      I0 => Q(0),
      I1 => \string_cent_decenas[1]5_carry_i_8_n_0\,
      I2 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I3 => \^total_reg[20]\,
      I4 => \string_cent_decenas[1]5_carry_i_11_n_0\,
      I5 => p_1_in(0),
      O => \string_cent_decenas[1]5_carry_i_6_n_0\
    );
\string_cent_decenas[1]5_carry_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000004000"
    )
        port map (
      I0 => \^total_reg[29]_0\,
      I1 => \^total_reg[26]\,
      I2 => \string_cent_decenas[1]5_carry_i_15_n_0\,
      I3 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I4 => \^total_reg[20]\,
      I5 => \string_cent_decenas[1]5_carry_i_11_n_0\,
      O => \^total_reg[29]\
    );
\string_cent_decenas[1]5_carry_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \^total_reg[29]_0\,
      I1 => Q(26),
      I2 => Q(27),
      I3 => Q(19),
      I4 => Q(21),
      O => \string_cent_decenas[1]5_carry_i_8_n_0\
    );
\string_cent_decenas[1]5_carry_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000010000"
    )
        port map (
      I0 => Q(23),
      I1 => Q(30),
      I2 => Q(15),
      I3 => Q(17),
      I4 => \string_cent_decenas[1]5_carry_i_16_n_0\,
      I5 => \string_cent_decenas[1]5_carry_i_17_n_0\,
      O => \string_cent_decenas[1]5_carry_i_9_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___0_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___0_carry_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___0_carry_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 2) => cent(4 downto 3),
      DI(1) => \i___0_carry_i_2_n_0\,
      DI(0) => '0',
      O(3 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___0_carry_O_UNCONNECTED\(3 downto 0),
      S(3) => \i___0_carry_i_3_n_0\,
      S(2) => \i___0_carry_i_4_n_0\,
      S(1) => \i___0_carry_i_5_n_0\,
      S(0) => \i___0_carry_i_6_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___0_carry_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__0_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \i___0_carry__0_i_1_n_0\,
      DI(2) => \i___0_carry__0_i_2_n_0\,
      DI(1 downto 0) => cent(6 downto 5),
      O(3) => \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_4\,
      O(2 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__0_O_UNCONNECTED\(2 downto 0),
      S(3) => \i___0_carry__0_i_4_n_0\,
      S(2) => \i___0_carry__0_i_5_n_0\,
      S(1) => \i___0_carry__0_i_6_n_0\,
      S(0) => \i___0_carry__0_i_7_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \i___0_carry__1_i_1_n_0\,
      O(3) => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_4\,
      O(2) => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_7\,
      S(3) => \i___0_carry__1_i_2_n_0\,
      S(2) => \i___0_carry__1_i_3_n_0\,
      S(1) => \i___0_carry__1_i_4_n_0\,
      S(0) => \i___0_carry__1_i_5_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_0\,
      CO(3 downto 2) => \NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__2_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___0_carry__2_n_2\,
      CO(0) => \NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__2_CO_UNCONNECTED\(0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__2_O_UNCONNECTED\(3 downto 1),
      O(0) => \string_cent_decenas[1]5_inferred__0/i___0_carry__2_n_7\,
      S(3 downto 1) => B"001",
      S(0) => \i___0_carry__2_i_1_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___106_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___106_carry_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \i___106_carry_i_1_n_0\,
      DI(2) => \i___106_carry_i_2_n_0\,
      DI(1) => \i___106_carry_i_3_n_0\,
      DI(0) => \i___106_carry_i_4_n_0\,
      O(3 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry_O_UNCONNECTED\(3 downto 0),
      S(3) => \i___106_carry_i_5_n_0\,
      S(2) => \i___106_carry_i_6_n_0\,
      S(1) => \i___106_carry_i_7_n_0\,
      S(0) => \i___106_carry_i_8_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___106_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___106_carry_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___106_carry__0_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__0_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \i___106_carry__0_i_1_n_0\,
      DI(2) => \i___106_carry__0_i_2_n_0\,
      DI(1) => \i___106_carry__0_i_3_n_0\,
      DI(0) => \i___106_carry__0_i_4_n_0\,
      O(3 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \i___106_carry__0_i_5_n_0\,
      S(2) => \i___106_carry__0_i_6_n_0\,
      S(1) => \i___106_carry__0_i_7_n_0\,
      S(0) => \i___106_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___106_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___106_carry__0_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___106_carry__1_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \i___106_carry__1_i_1_n_0\,
      DI(2) => \i___106_carry__1_i_2_n_0\,
      DI(1) => \i___106_carry__1_i_3_n_0\,
      DI(0) => \i___106_carry__1_i_4_n_0\,
      O(3 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__1_O_UNCONNECTED\(3 downto 0),
      S(3) => \i___106_carry__1_i_5_n_0\,
      S(2) => \i___106_carry__1_i_6_n_0\,
      S(1) => \i___106_carry__1_i_7_n_0\,
      S(0) => \i___106_carry__1_i_8_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___106_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___106_carry__1_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___106_carry__2_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__2_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \i___106_carry__2_i_1_n_0\,
      DI(2) => \i___106_carry__2_i_2_n_0\,
      DI(1) => \i___106_carry__2_i_3_n_0\,
      DI(0) => \i___106_carry__2_i_4_n_0\,
      O(3 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__2_O_UNCONNECTED\(3 downto 0),
      S(3) => \i___106_carry__2_i_5_n_0\,
      S(2) => \i___106_carry__2_i_6_n_0\,
      S(1) => \i___106_carry__2_i_7_n_0\,
      S(0) => \i___106_carry__2_i_8_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___106_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___106_carry__2_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___106_carry__3_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__3_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \i___106_carry__3_i_1_n_0\,
      DI(2) => \i___106_carry__3_i_2_n_0\,
      DI(1) => \i___106_carry__3_i_3_n_0\,
      DI(0) => \i___106_carry__3_i_4_n_0\,
      O(3 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__3_O_UNCONNECTED\(3 downto 0),
      S(3) => \i___106_carry__3_i_5_n_0\,
      S(2) => \i___106_carry__3_i_6_n_0\,
      S(1) => \i___106_carry__3_i_7_n_0\,
      S(0) => \i___106_carry__3_i_8_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___106_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___106_carry__3_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__4_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \i___106_carry__4_i_1_n_0\,
      DI(1) => \i___106_carry__4_i_2_n_0\,
      DI(0) => \i___106_carry__4_i_3_n_0\,
      O(3) => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_4\,
      O(2) => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_7\,
      S(3) => \i___106_carry__4_i_4_n_0\,
      S(2) => \i___106_carry__4_i_5_n_0\,
      S(1) => \i___106_carry__4_i_6_n_0\,
      S(0) => \i___106_carry__4_i_7_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___106_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_0\,
      CO(3 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__5_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 2) => \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__5_O_UNCONNECTED\(3 downto 2),
      O(1) => \string_cent_decenas[1]5_inferred__0/i___106_carry__5_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___106_carry__5_n_7\,
      S(3 downto 2) => B"00",
      S(1) => \i___106_carry__5_i_1_n_0\,
      S(0) => \i___106_carry__5_i_2_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___163_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___163_carry_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \i___163_carry_i_1_n_0\,
      DI(0) => '0',
      O(3) => \NLW_string_cent_decenas[1]5_inferred__0/i___163_carry_O_UNCONNECTED\(3),
      O(2) => \string_cent_decenas[1]5_inferred__0/i___163_carry_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i___163_carry_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___163_carry_n_7\,
      S(3) => '0',
      S(2) => \i___163_carry_i_2_n_0\,
      S(1) => \i___163_carry_i_3_n_0\,
      S(0) => \i___163_carry_i_4_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___169_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___169_carry_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '1',
      DI(3) => cent(3),
      DI(2) => \i___0_carry_i_2_n_0\,
      DI(1) => \i___169_carry_i_2_n_0\,
      DI(0) => \^total_reg[0]\,
      O(3) => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_4\,
      O(2) => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_5\,
      O(1) => \string_cent_decenas[1]5\(1),
      O(0) => \NLW_string_cent_decenas[1]5_inferred__0/i___169_carry_O_UNCONNECTED\(0),
      S(3) => \i___169_carry_i_3_n_0\,
      S(2) => \i___169_carry_i_4_n_0\,
      S(1) => \i___169_carry_i_5_n_0\,
      S(0) => \i___169_carry_i_6_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___169_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_0\,
      CO(3 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___169_carry__0_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => cent(6 downto 4),
      O(3) => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_4\,
      O(2) => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_7\,
      S(3) => \i___169_carry__0_i_1_n_0\,
      S(2) => \i___169_carry__0_i_2_n_0\,
      S(1) => \i___169_carry__0_i_3_n_0\,
      S(0) => \i___169_carry__0_i_4_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___27_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___27_carry_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \i___27_carry_i_1_n_0\,
      DI(2) => \^total_reg[0]\,
      DI(1 downto 0) => B"01",
      O(3) => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_4\,
      O(2) => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_6\,
      O(0) => \NLW_string_cent_decenas[1]5_inferred__0/i___27_carry_O_UNCONNECTED\(0),
      S(3) => \i___27_carry_i_2_n_0\,
      S(2) => \i___27_carry_i_3_n_0\,
      S(1) => \i___169_carry_i_5_n_0\,
      S(0) => \i___27_carry_i_5_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___27_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___27_carry__0_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \i___27_carry__0_i_1_n_0\,
      DI(2) => \i___27_carry__0_i_2_n_0\,
      DI(1) => \i___27_carry__0_i_3_n_0\,
      DI(0) => \i___27_carry__0_i_4_n_0\,
      O(3) => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_4\,
      O(2) => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_7\,
      S(3) => \i___27_carry__0_i_5_n_0\,
      S(2) => \i___27_carry__0_i_6_n_0\,
      S(1) => \i___27_carry__0_i_7_n_0\,
      S(0) => \i___27_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___27_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___27_carry__1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 2) => B"01",
      DI(1) => \i___27_carry__1_i_1_n_0\,
      DI(0) => \i___27_carry__1_i_2_n_0\,
      O(3) => \NLW_string_cent_decenas[1]5_inferred__0/i___27_carry__1_O_UNCONNECTED\(3),
      O(2) => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_7\,
      S(3) => '1',
      S(2) => \i___27_carry__1_i_3_n_0\,
      S(1) => \i___27_carry__1_i_4_n_0\,
      S(0) => \i___27_carry__1_i_5_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___56_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___56_carry_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \^total_reg[0]\,
      DI(2 downto 0) => B"001",
      O(3) => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_4\,
      O(2) => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_6\,
      O(0) => \NLW_string_cent_decenas[1]5_inferred__0/i___56_carry_O_UNCONNECTED\(0),
      S(3) => \i___56_carry_i_1_n_0\,
      S(2) => \i___56_carry_i_2_n_0\,
      S(1) => \i___169_carry_i_5_n_0\,
      S(0) => \i___56_carry_i_4_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___56_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___56_carry__0_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 2) => cent(4 downto 3),
      DI(1) => \i___0_carry_i_2_n_0\,
      DI(0) => \i___56_carry__0_i_2_n_0\,
      O(3) => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_4\,
      O(2) => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_7\,
      S(3) => \i___56_carry__0_i_3_n_0\,
      S(2) => \i___56_carry__0_i_4_n_0\,
      S(1) => \i___56_carry__0_i_5_n_0\,
      S(0) => \i___56_carry__0_i_6_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___56_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_0\,
      CO(3) => \NLW_string_cent_decenas[1]5_inferred__0/i___56_carry__1_CO_UNCONNECTED\(3),
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      CO(1 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___56_carry__1_CO_UNCONNECTED\(1 downto 0),
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1 downto 0) => cent(6 downto 5),
      O(3 downto 2) => \NLW_string_cent_decenas[1]5_inferred__0/i___56_carry__1_O_UNCONNECTED\(3 downto 2),
      O(1) => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_7\,
      S(3 downto 2) => B"01",
      S(1) => \i___56_carry__1_i_1_n_0\,
      S(0) => \i___56_carry__1_i_2_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___80_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___80_carry_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 1) => cent(6 downto 4),
      DI(0) => '0',
      O(3) => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_4\,
      O(2) => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_7\,
      S(3) => \i___80_carry_i_1_n_0\,
      S(2) => \i___80_carry_i_2_n_0\,
      S(1) => \i___80_carry_i_3_n_0\,
      S(0) => \i___80_carry_i_4_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___80_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___80_carry__0_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \i___80_carry__0_i_1_n_0\,
      DI(2) => \i___80_carry__0_i_2_n_0\,
      DI(1) => \i___80_carry__0_i_3_n_0\,
      DI(0) => \i___80_carry__0_i_4_n_0\,
      O(3) => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_4\,
      O(2) => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_7\,
      S(3) => \i___80_carry__0_i_5_n_0\,
      S(2) => \i___80_carry__0_i_6_n_0\,
      S(1) => \i___80_carry__0_i_7_n_0\,
      S(0) => \i___80_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___80_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_0\,
      CO(3) => \NLW_string_cent_decenas[1]5_inferred__0/i___80_carry__1_CO_UNCONNECTED\(3),
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_1\,
      CO(1 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___80_carry__1_CO_UNCONNECTED\(1 downto 0),
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \i___80_carry__1_i_1_n_0\,
      O(3 downto 2) => \NLW_string_cent_decenas[1]5_inferred__0/i___80_carry__1_O_UNCONNECTED\(3 downto 2),
      O(1) => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_7\,
      S(3 downto 2) => B"01",
      S(1) => \i___80_carry__1_i_2_n_0\,
      S(0) => \i___80_carry__1_i_3_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity synchrnzr is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    monedas_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end synchrnzr;

architecture STRUCTURE of synchrnzr is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  attribute srl_name : string;
  attribute srl_name of sync_out_reg_srl2 : label is "\inst_sincronizador/inst_synchrnzr0/sync_out_reg_srl2 ";
begin
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => monedas_IBUF(0),
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
sync_out_reg_srl2: unisim.vcomponents.SRL16E
    generic map(
      INIT => X"0000"
    )
        port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => clk_BUFG,
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg[0]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity synchrnzr_0 is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    monedas_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of synchrnzr_0 : entity is "synchrnzr";
end synchrnzr_0;

architecture STRUCTURE of synchrnzr_0 is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  attribute srl_name : string;
  attribute srl_name of sync_out_reg_srl2 : label is "\inst_sincronizador/inst_synchrnzr1/sync_out_reg_srl2 ";
begin
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => monedas_IBUF(0),
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
sync_out_reg_srl2: unisim.vcomponents.SRL16E
    generic map(
      INIT => X"0000"
    )
        port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => clk_BUFG,
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg[0]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity synchrnzr_1 is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    monedas_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of synchrnzr_1 : entity is "synchrnzr";
end synchrnzr_1;

architecture STRUCTURE of synchrnzr_1 is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  attribute srl_name : string;
  attribute srl_name of sync_out_reg_srl2 : label is "\inst_sincronizador/inst_synchrnzr2/sync_out_reg_srl2 ";
begin
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => monedas_IBUF(0),
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
sync_out_reg_srl2: unisim.vcomponents.SRL16E
    generic map(
      INIT => X"0000"
    )
        port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => clk_BUFG,
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg[0]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity synchrnzr_2 is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    monedas_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of synchrnzr_2 : entity is "synchrnzr";
end synchrnzr_2;

architecture STRUCTURE of synchrnzr_2 is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  attribute srl_name : string;
  attribute srl_name of sync_out_reg_srl2 : label is "\inst_sincronizador/inst_synchrnzr3/sync_out_reg_srl2 ";
begin
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => monedas_IBUF(0),
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
sync_out_reg_srl2: unisim.vcomponents.SRL16E
    generic map(
      INIT => X"0000"
    )
        port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => clk_BUFG,
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg[0]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity synchrnzr_3 is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    monedas_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of synchrnzr_3 : entity is "synchrnzr";
end synchrnzr_3;

architecture STRUCTURE of synchrnzr_3 is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  attribute srl_name : string;
  attribute srl_name of sync_out_reg_srl2 : label is "\inst_sincronizador/inst_synchrnzr4/sync_out_reg_srl2 ";
begin
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => monedas_IBUF(0),
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
sync_out_reg_srl2: unisim.vcomponents.SRL16E
    generic map(
      INIT => X"0000"
    )
        port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => clk_BUFG,
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg[0]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity detectorflancos is
  port (
    D : out STD_LOGIC_VECTOR ( 6 downto 0 );
    \sreg_reg[0]\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    \sreg_reg[0]_0\ : in STD_LOGIC;
    \sreg_reg[0]_1\ : in STD_LOGIC;
    \sreg_reg[0]_2\ : in STD_LOGIC;
    \sreg_reg[0]_3\ : in STD_LOGIC
  );
end detectorflancos;

architecture STRUCTURE of detectorflancos is
  signal inst_edgedtctr0_n_1 : STD_LOGIC;
  signal inst_edgedtctr1_n_4 : STD_LOGIC;
  signal inst_edgedtctr2_n_0 : STD_LOGIC;
  signal inst_edgedtctr2_n_1 : STD_LOGIC;
  signal inst_edgedtctr3_n_4 : STD_LOGIC;
  signal inst_edgedtctr4_n_4 : STD_LOGIC;
  signal inst_edgedtctr4_n_5 : STD_LOGIC;
  signal sreg : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal sreg_0 : STD_LOGIC_VECTOR ( 2 downto 0 );
begin
inst_edgedtctr0: entity work.edgedtctr
     port map (
      D(0) => D(0),
      clk_BUFG => clk_BUFG,
      \sreg_reg[0]_0\ => \sreg_reg[0]\,
      \sreg_reg[2]_0\ => inst_edgedtctr0_n_1,
      \v_reg[0]\ => inst_edgedtctr2_n_0,
      \v_reg[0]_0\ => inst_edgedtctr4_n_5
    );
inst_edgedtctr1: entity work.edgedtctr_4
     port map (
      D(0) => D(3),
      clk_BUFG => clk_BUFG,
      sreg(2 downto 0) => sreg(2 downto 0),
      \sreg_reg[0]_0\ => \sreg_reg[0]_0\,
      \sreg_reg[2]_0\ => inst_edgedtctr1_n_4,
      \v_reg[3]\ => inst_edgedtctr2_n_1,
      \v_reg[3]_0\ => inst_edgedtctr0_n_1,
      \v_reg[3]_1\ => inst_edgedtctr4_n_5
    );
inst_edgedtctr2: entity work.edgedtctr_5
     port map (
      clk_BUFG => clk_BUFG,
      sreg(2 downto 0) => sreg(2 downto 0),
      \sreg_reg[0]_0\ => inst_edgedtctr2_n_0,
      \sreg_reg[0]_1\ => \sreg_reg[0]_1\,
      \sreg_reg[2]_0\ => inst_edgedtctr2_n_1
    );
inst_edgedtctr3: entity work.edgedtctr_6
     port map (
      D(0) => D(6),
      clk_BUFG => clk_BUFG,
      sreg(2 downto 0) => sreg_0(2 downto 0),
      \sreg_reg[0]_0\ => \sreg_reg[0]_2\,
      \sreg_reg[2]_0\ => inst_edgedtctr3_n_4,
      \v_reg[6]\ => inst_edgedtctr4_n_4,
      \v_reg[6]_0\ => inst_edgedtctr0_n_1,
      \v_reg[6]_1\ => inst_edgedtctr2_n_0
    );
inst_edgedtctr4: entity work.edgedtctr_7
     port map (
      D(3 downto 2) => D(5 downto 4),
      D(1 downto 0) => D(2 downto 1),
      clk_BUFG => clk_BUFG,
      sreg(2 downto 0) => sreg_0(2 downto 0),
      \sreg_reg[0]_0\ => inst_edgedtctr4_n_5,
      \sreg_reg[0]_1\ => \sreg_reg[0]_3\,
      \sreg_reg[2]_0\ => inst_edgedtctr4_n_4,
      \v_reg[2]\ => inst_edgedtctr3_n_4,
      \v_reg[2]_0\ => inst_edgedtctr0_n_1,
      \v_reg[2]_1\ => inst_edgedtctr1_n_4,
      \v_reg[2]_2\ => inst_edgedtctr2_n_1,
      \v_reg[5]\ => inst_edgedtctr2_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity display is
  port (
    dot_OBUF : out STD_LOGIC;
    \charact_dot_reg[0]_0\ : out STD_LOGIC;
    \charact_dot_reg[0]_1\ : out STD_LOGIC;
    \charact_dot_reg[0]_2\ : out STD_LOGIC;
    \charact_dot_reg[0]_3\ : out STD_LOGIC;
    \reg_reg[0]\ : out STD_LOGIC;
    \charact_dot_reg[0]_4\ : out STD_LOGIC;
    \FSM_onehot_current_refresco_reg[4]\ : out STD_LOGIC;
    \reg_reg[1]\ : out STD_LOGIC;
    \reg_reg[2]\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \current_state_reg[1]\ : out STD_LOGIC;
    \current_state_reg[0]\ : out STD_LOGIC;
    \charact_dot_reg[0]_5\ : out STD_LOGIC;
    \current_state_reg[0]_0\ : out STD_LOGIC;
    \reg_reg[2]_0\ : out STD_LOGIC;
    \FSM_onehot_current_refresco_reg[1]\ : out STD_LOGIC;
    \FSM_onehot_current_refresco_reg[2]\ : out STD_LOGIC;
    \charact_dot_reg[0]_6\ : out STD_LOGIC;
    \FSM_onehot_current_refresco_reg[0]\ : out STD_LOGIC;
    \reg_reg[1]_0\ : out STD_LOGIC;
    \reg_reg[1]_1\ : out STD_LOGIC;
    elem_OBUF : out STD_LOGIC_VECTOR ( 7 downto 0 );
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    \current_state_reg[1]_0\ : out STD_LOGIC;
    \caracter_reg[6]_0\ : out STD_LOGIC_VECTOR ( 6 downto 0 );
    digit_OBUF : out STD_LOGIC_VECTOR ( 6 downto 0 );
    clk_BUFG : in STD_LOGIC;
    output0 : in STD_LOGIC;
    \disp_dinero[1]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter_reg[3]_0\ : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \caracter_reg[1]_0\ : in STD_LOGIC;
    \caracter_reg[1]_1\ : in STD_LOGIC;
    \caracter_reg[1]_2\ : in STD_LOGIC;
    \caracter_reg[1]_3\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 4 downto 0 );
    \caracter_reg[4]_0\ : in STD_LOGIC;
    dot_reg_0 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \caracter_reg[0]_0\ : in STD_LOGIC;
    \caracter_reg[0]_1\ : in STD_LOGIC;
    \disp_dinero[5]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    cent : in STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter_reg[0]_2\ : in STD_LOGIC;
    \caracter_reg[0]_3\ : in STD_LOGIC;
    \caracter_reg[0]_4\ : in STD_LOGIC;
    \caracter[2]_i_2\ : in STD_LOGIC;
    \caracter_reg[2]_0\ : in STD_LOGIC;
    \caracter_reg[6]_1\ : in STD_LOGIC;
    \caracter[2]_i_6\ : in STD_LOGIC;
    \caracter[2]_i_6_0\ : in STD_LOGIC;
    reset_IBUF : in STD_LOGIC;
    dot_reg_1 : in STD_LOGIC;
    \charact_dot_reg[0]_7\ : in STD_LOGIC;
    \caracter_reg[2]_1\ : in STD_LOGIC;
    \caracter_reg[2]_2\ : in STD_LOGIC;
    lopt : out STD_LOGIC;
    lopt_1 : out STD_LOGIC;
    lopt_2 : out STD_LOGIC;
    lopt_3 : out STD_LOGIC;
    lopt_4 : out STD_LOGIC;
    lopt_5 : out STD_LOGIC;
    lopt_6 : out STD_LOGIC
  );
end display;

architecture STRUCTURE of display is
  signal Inst_contador_n_33 : STD_LOGIC;
  signal Inst_contador_n_36 : STD_LOGIC;
  signal caracter : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \caracter_reg[0]_lopt_replica_1\ : STD_LOGIC;
  signal \caracter_reg[1]_lopt_replica_1\ : STD_LOGIC;
  signal \caracter_reg[2]_lopt_replica_1\ : STD_LOGIC;
  signal \caracter_reg[3]_lopt_replica_1\ : STD_LOGIC;
  signal \caracter_reg[4]_lopt_replica_1\ : STD_LOGIC;
  signal \caracter_reg[5]_lopt_replica_1\ : STD_LOGIC;
  signal \^caracter_reg[6]_0\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \caracter_reg[6]_lopt_replica_1\ : STD_LOGIC;
  signal \^charact_dot_reg[0]_0\ : STD_LOGIC;
  signal dot : STD_LOGIC;
  attribute OPT_INSERTED_REPDRIVER : boolean;
  attribute OPT_INSERTED_REPDRIVER of \caracter_reg[0]_lopt_replica\ : label is std.standard.true;
  attribute OPT_MODIFIED : string;
  attribute OPT_MODIFIED of \caracter_reg[0]_lopt_replica\ : label is "SWEEP";
  attribute OPT_INSERTED_REPDRIVER of \caracter_reg[1]_lopt_replica\ : label is std.standard.true;
  attribute OPT_MODIFIED of \caracter_reg[1]_lopt_replica\ : label is "SWEEP";
  attribute OPT_INSERTED_REPDRIVER of \caracter_reg[2]_lopt_replica\ : label is std.standard.true;
  attribute OPT_MODIFIED of \caracter_reg[2]_lopt_replica\ : label is "SWEEP";
  attribute OPT_INSERTED_REPDRIVER of \caracter_reg[3]_lopt_replica\ : label is std.standard.true;
  attribute OPT_MODIFIED of \caracter_reg[3]_lopt_replica\ : label is "SWEEP";
  attribute OPT_INSERTED_REPDRIVER of \caracter_reg[4]_lopt_replica\ : label is std.standard.true;
  attribute OPT_MODIFIED of \caracter_reg[4]_lopt_replica\ : label is "SWEEP";
  attribute OPT_INSERTED_REPDRIVER of \caracter_reg[5]_lopt_replica\ : label is std.standard.true;
  attribute OPT_MODIFIED of \caracter_reg[5]_lopt_replica\ : label is "SWEEP";
  attribute OPT_INSERTED_REPDRIVER of \caracter_reg[6]_lopt_replica\ : label is std.standard.true;
  attribute OPT_MODIFIED of \caracter_reg[6]_lopt_replica\ : label is "SWEEP";
begin
  \caracter_reg[6]_0\(6 downto 0) <= \^caracter_reg[6]_0\(6 downto 0);
  \charact_dot_reg[0]_0\ <= \^charact_dot_reg[0]_0\;
  lopt <= \caracter_reg[0]_lopt_replica_1\;
  lopt_1 <= \caracter_reg[1]_lopt_replica_1\;
  lopt_2 <= \caracter_reg[2]_lopt_replica_1\;
  lopt_3 <= \caracter_reg[3]_lopt_replica_1\;
  lopt_4 <= \caracter_reg[4]_lopt_replica_1\;
  lopt_5 <= \caracter_reg[5]_lopt_replica_1\;
  lopt_6 <= \caracter_reg[6]_lopt_replica_1\;
Inst_contador: entity work.\contador__parameterized1\
     port map (
      CO(0) => CO(0),
      D(3) => caracter(4),
      D(2 downto 0) => caracter(2 downto 0),
      \FSM_onehot_current_refresco_reg[0]\ => \FSM_onehot_current_refresco_reg[0]\,
      \FSM_onehot_current_refresco_reg[1]\ => \FSM_onehot_current_refresco_reg[1]\,
      \FSM_onehot_current_refresco_reg[2]\ => \FSM_onehot_current_refresco_reg[2]\,
      \FSM_onehot_current_refresco_reg[4]\ => \FSM_onehot_current_refresco_reg[4]\,
      Q(4 downto 0) => Q(4 downto 0),
      \caracter[2]_i_2_0\ => \caracter[2]_i_2\,
      \caracter[2]_i_6\ => \caracter[2]_i_6\,
      \caracter[2]_i_6_0\ => \caracter[2]_i_6_0\,
      \caracter_reg[0]\ => \caracter_reg[0]_0\,
      \caracter_reg[0]_0\ => \caracter_reg[0]_1\,
      \caracter_reg[0]_1\ => \caracter_reg[0]_2\,
      \caracter_reg[0]_2\ => \caracter_reg[0]_3\,
      \caracter_reg[0]_3\ => \caracter_reg[0]_4\,
      \caracter_reg[1]\ => \caracter_reg[1]_0\,
      \caracter_reg[1]_0\ => \caracter_reg[1]_1\,
      \caracter_reg[1]_1\ => \caracter_reg[1]_2\,
      \caracter_reg[1]_2\ => \caracter_reg[1]_3\,
      \caracter_reg[2]\ => \caracter_reg[2]_0\,
      \caracter_reg[2]_0\ => \caracter_reg[2]_1\,
      \caracter_reg[2]_1\ => \caracter_reg[2]_2\,
      \caracter_reg[3]\ => \caracter_reg[3]_0\,
      \caracter_reg[4]\ => \caracter_reg[4]_0\,
      \caracter_reg[6]\ => \caracter_reg[6]_1\,
      cent(0) => cent(0),
      \charact_dot_reg[0]\ => \charact_dot_reg[0]_1\,
      \charact_dot_reg[0]_0\ => \charact_dot_reg[0]_2\,
      \charact_dot_reg[0]_1\ => \charact_dot_reg[0]_3\,
      \charact_dot_reg[0]_2\ => \charact_dot_reg[0]_4\,
      \charact_dot_reg[0]_3\ => \charact_dot_reg[0]_5\,
      \charact_dot_reg[0]_4\ => \charact_dot_reg[0]_6\,
      \charact_dot_reg[0]_5\ => Inst_contador_n_36,
      \charact_dot_reg[0]_6\ => \^charact_dot_reg[0]_0\,
      \charact_dot_reg[0]_7\ => \charact_dot_reg[0]_7\,
      clk_BUFG => clk_BUFG,
      \current_state_reg[0]\ => \current_state_reg[0]\,
      \current_state_reg[0]_0\ => \current_state_reg[0]_0\,
      \current_state_reg[1]\ => \current_state_reg[1]\,
      \current_state_reg[1]_0\ => Inst_contador_n_33,
      \current_state_reg[1]_1\ => \current_state_reg[1]_0\,
      \disp_dinero[1]_0\(0) => \disp_dinero[1]_0\(0),
      \disp_dinero[5]_1\(0) => \disp_dinero[5]_1\(0),
      dot => dot,
      dot_reg(1 downto 0) => dot_reg_0(1 downto 0),
      dot_reg_0 => dot_reg_1,
      elem_OBUF(7 downto 0) => elem_OBUF(7 downto 0),
      output0 => output0,
      \reg_reg[0]_0\ => \reg_reg[0]\,
      \reg_reg[1]_0\ => \reg_reg[1]\,
      \reg_reg[1]_1\ => \reg_reg[1]_0\,
      \reg_reg[1]_2\ => \reg_reg[1]_1\,
      \reg_reg[2]_0\(2 downto 0) => \reg_reg[2]\(2 downto 0),
      \reg_reg[2]_1\ => \reg_reg[2]_0\,
      reset_IBUF => reset_IBUF
    );
Inst_digito: entity work.digito
     port map (
      Q(6 downto 0) => \^caracter_reg[6]_0\(6 downto 0),
      digit_OBUF(6 downto 0) => digit_OBUF(6 downto 0)
    );
\caracter_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => caracter(0),
      Q => \^caracter_reg[6]_0\(0)
    );
\caracter_reg[0]_lopt_replica\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => caracter(0),
      Q => \caracter_reg[0]_lopt_replica_1\
    );
\caracter_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => caracter(1),
      Q => \^caracter_reg[6]_0\(1)
    );
\caracter_reg[1]_lopt_replica\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => caracter(1),
      Q => \caracter_reg[1]_lopt_replica_1\
    );
\caracter_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => caracter(2),
      Q => \^caracter_reg[6]_0\(2)
    );
\caracter_reg[2]_lopt_replica\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => caracter(2),
      Q => \caracter_reg[2]_lopt_replica_1\
    );
\caracter_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => D(0),
      Q => \^caracter_reg[6]_0\(3)
    );
\caracter_reg[3]_lopt_replica\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => D(0),
      Q => \caracter_reg[3]_lopt_replica_1\
    );
\caracter_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => caracter(4),
      Q => \^caracter_reg[6]_0\(4)
    );
\caracter_reg[4]_lopt_replica\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => caracter(4),
      Q => \caracter_reg[4]_lopt_replica_1\
    );
\caracter_reg[5]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => D(1),
      PRE => output0,
      Q => \^caracter_reg[6]_0\(5)
    );
\caracter_reg[5]_lopt_replica\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => D(1),
      PRE => output0,
      Q => \caracter_reg[5]_lopt_replica_1\
    );
\caracter_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => D(2),
      Q => \^caracter_reg[6]_0\(6)
    );
\caracter_reg[6]_lopt_replica\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => D(2),
      Q => \caracter_reg[6]_lopt_replica_1\
    );
\charact_dot_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => Inst_contador_n_36,
      Q => \^charact_dot_reg[0]_0\,
      R => '0'
    );
dot_reg: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_BUFG,
      CE => dot,
      D => Inst_contador_n_33,
      PRE => output0,
      Q => dot_OBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gestion_dinero is
  port (
    \charact_dot_reg[0]\ : out STD_LOGIC;
    \total_reg[29]_0\ : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 0 to 0 );
    \current_state_reg[1]\ : out STD_LOGIC;
    \charact_dot_reg[0]_0\ : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \current_state_reg[1]_0\ : out STD_LOGIC;
    DI : out STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_dinero[5]_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    refresco_OBUF : out STD_LOGIC;
    devolver_OBUF : out STD_LOGIC;
    \caracter_reg[2]\ : in STD_LOGIC;
    \caracter_reg[2]_0\ : in STD_LOGIC;
    \caracter_reg[2]_1\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \caracter_reg[3]\ : in STD_LOGIC;
    \caracter_reg[3]_0\ : in STD_LOGIC;
    \caracter_reg[3]_1\ : in STD_LOGIC;
    \caracter_reg[3]_2\ : in STD_LOGIC;
    \caracter_reg[0]\ : in STD_LOGIC;
    \caracter_reg[0]_0\ : in STD_LOGIC;
    \caracter_reg[0]_1\ : in STD_LOGIC;
    \caracter_reg[3]_3\ : in STD_LOGIC;
    \caracter_reg[3]_4\ : in STD_LOGIC;
    \caracter_reg[3]_5\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \total_reg[7]_i_2_0\ : in STD_LOGIC_VECTOR ( 6 downto 0 );
    \current_state_reg[0]\ : in STD_LOGIC;
    \current_state_reg[0]_0\ : in STD_LOGIC;
    button_ok_IBUF : in STD_LOGIC;
    clr_refresco_r : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC
  );
end gestion_dinero;

architecture STRUCTURE of gestion_dinero is
  signal \FSM_sequential_current_state[1]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_current_state_reg_n_0_[1]\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_10_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_11_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_12_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_13_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_14_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_15_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_16_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_17_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_18_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_19_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_20_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_21_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_22_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_23_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_24_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_25_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_26_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_27_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_28_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_29_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_30_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_31_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_32_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_33_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_34_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_35_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_36_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_37_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_38_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_39_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_40_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_41_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_42_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_43_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_44_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_45_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_7_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_8_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_9_n_0\ : STD_LOGIC;
  signal aux : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \aux_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal bdf_dinero : STD_LOGIC;
  signal bdf_reg_i_1_n_0 : STD_LOGIC;
  signal current_state : STD_LOGIC_VECTOR ( 0 to 0 );
  signal data0 : STD_LOGIC;
  signal inst_int_to_string_n_10 : STD_LOGIC;
  signal inst_int_to_string_n_7 : STD_LOGIC;
  signal inst_int_to_string_n_9 : STD_LOGIC;
  signal \next_state__0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal total : STD_LOGIC;
  signal \total[0]_i_1_n_0\ : STD_LOGIC;
  signal \total[10]_i_1_n_0\ : STD_LOGIC;
  signal \total[11]_i_1_n_0\ : STD_LOGIC;
  signal \total[12]_i_1_n_0\ : STD_LOGIC;
  signal \total[13]_i_1_n_0\ : STD_LOGIC;
  signal \total[14]_i_1_n_0\ : STD_LOGIC;
  signal \total[15]_i_1_n_0\ : STD_LOGIC;
  signal \total[16]_i_1_n_0\ : STD_LOGIC;
  signal \total[17]_i_1_n_0\ : STD_LOGIC;
  signal \total[18]_i_1_n_0\ : STD_LOGIC;
  signal \total[19]_i_1_n_0\ : STD_LOGIC;
  signal \total[1]_i_1_n_0\ : STD_LOGIC;
  signal \total[20]_i_1_n_0\ : STD_LOGIC;
  signal \total[21]_i_1_n_0\ : STD_LOGIC;
  signal \total[22]_i_1_n_0\ : STD_LOGIC;
  signal \total[23]_i_1_n_0\ : STD_LOGIC;
  signal \total[24]_i_1_n_0\ : STD_LOGIC;
  signal \total[25]_i_1_n_0\ : STD_LOGIC;
  signal \total[26]_i_1_n_0\ : STD_LOGIC;
  signal \total[27]_i_1_n_0\ : STD_LOGIC;
  signal \total[28]_i_1_n_0\ : STD_LOGIC;
  signal \total[29]_i_1_n_0\ : STD_LOGIC;
  signal \total[2]_i_1_n_0\ : STD_LOGIC;
  signal \total[30]_i_1_n_0\ : STD_LOGIC;
  signal \total[31]_i_2_n_0\ : STD_LOGIC;
  signal \total[3]_i_1_n_0\ : STD_LOGIC;
  signal \total[3]_i_3_n_0\ : STD_LOGIC;
  signal \total[3]_i_4_n_0\ : STD_LOGIC;
  signal \total[3]_i_5_n_0\ : STD_LOGIC;
  signal \total[3]_i_6_n_0\ : STD_LOGIC;
  signal \total[4]_i_1_n_0\ : STD_LOGIC;
  signal \total[5]_i_1_n_0\ : STD_LOGIC;
  signal \total[6]_i_1_n_0\ : STD_LOGIC;
  signal \total[7]_i_1_n_0\ : STD_LOGIC;
  signal \total[7]_i_3_n_0\ : STD_LOGIC;
  signal \total[7]_i_4_n_0\ : STD_LOGIC;
  signal \total[7]_i_5_n_0\ : STD_LOGIC;
  signal \total[8]_i_1_n_0\ : STD_LOGIC;
  signal \total[9]_i_1_n_0\ : STD_LOGIC;
  signal \total_reg[11]_i_2_n_0\ : STD_LOGIC;
  signal \total_reg[11]_i_2_n_4\ : STD_LOGIC;
  signal \total_reg[11]_i_2_n_5\ : STD_LOGIC;
  signal \total_reg[11]_i_2_n_6\ : STD_LOGIC;
  signal \total_reg[11]_i_2_n_7\ : STD_LOGIC;
  signal \total_reg[15]_i_2_n_0\ : STD_LOGIC;
  signal \total_reg[15]_i_2_n_4\ : STD_LOGIC;
  signal \total_reg[15]_i_2_n_5\ : STD_LOGIC;
  signal \total_reg[15]_i_2_n_6\ : STD_LOGIC;
  signal \total_reg[15]_i_2_n_7\ : STD_LOGIC;
  signal \total_reg[19]_i_2_n_0\ : STD_LOGIC;
  signal \total_reg[19]_i_2_n_4\ : STD_LOGIC;
  signal \total_reg[19]_i_2_n_5\ : STD_LOGIC;
  signal \total_reg[19]_i_2_n_6\ : STD_LOGIC;
  signal \total_reg[19]_i_2_n_7\ : STD_LOGIC;
  signal \total_reg[23]_i_2_n_0\ : STD_LOGIC;
  signal \total_reg[23]_i_2_n_4\ : STD_LOGIC;
  signal \total_reg[23]_i_2_n_5\ : STD_LOGIC;
  signal \total_reg[23]_i_2_n_6\ : STD_LOGIC;
  signal \total_reg[23]_i_2_n_7\ : STD_LOGIC;
  signal \total_reg[27]_i_2_n_0\ : STD_LOGIC;
  signal \total_reg[27]_i_2_n_4\ : STD_LOGIC;
  signal \total_reg[27]_i_2_n_5\ : STD_LOGIC;
  signal \total_reg[27]_i_2_n_6\ : STD_LOGIC;
  signal \total_reg[27]_i_2_n_7\ : STD_LOGIC;
  signal \total_reg[31]_i_3_n_4\ : STD_LOGIC;
  signal \total_reg[31]_i_3_n_5\ : STD_LOGIC;
  signal \total_reg[31]_i_3_n_6\ : STD_LOGIC;
  signal \total_reg[31]_i_3_n_7\ : STD_LOGIC;
  signal \total_reg[3]_i_2_n_0\ : STD_LOGIC;
  signal \total_reg[3]_i_2_n_4\ : STD_LOGIC;
  signal \total_reg[3]_i_2_n_5\ : STD_LOGIC;
  signal \total_reg[3]_i_2_n_6\ : STD_LOGIC;
  signal \total_reg[3]_i_2_n_7\ : STD_LOGIC;
  signal \total_reg[7]_i_2_n_0\ : STD_LOGIC;
  signal \total_reg[7]_i_2_n_4\ : STD_LOGIC;
  signal \total_reg[7]_i_2_n_5\ : STD_LOGIC;
  signal \total_reg[7]_i_2_n_6\ : STD_LOGIC;
  signal \total_reg[7]_i_2_n_7\ : STD_LOGIC;
  signal \total_reg_n_0_[0]\ : STD_LOGIC;
  signal \total_reg_n_0_[10]\ : STD_LOGIC;
  signal \total_reg_n_0_[11]\ : STD_LOGIC;
  signal \total_reg_n_0_[12]\ : STD_LOGIC;
  signal \total_reg_n_0_[13]\ : STD_LOGIC;
  signal \total_reg_n_0_[14]\ : STD_LOGIC;
  signal \total_reg_n_0_[15]\ : STD_LOGIC;
  signal \total_reg_n_0_[16]\ : STD_LOGIC;
  signal \total_reg_n_0_[17]\ : STD_LOGIC;
  signal \total_reg_n_0_[18]\ : STD_LOGIC;
  signal \total_reg_n_0_[19]\ : STD_LOGIC;
  signal \total_reg_n_0_[1]\ : STD_LOGIC;
  signal \total_reg_n_0_[20]\ : STD_LOGIC;
  signal \total_reg_n_0_[21]\ : STD_LOGIC;
  signal \total_reg_n_0_[22]\ : STD_LOGIC;
  signal \total_reg_n_0_[23]\ : STD_LOGIC;
  signal \total_reg_n_0_[24]\ : STD_LOGIC;
  signal \total_reg_n_0_[25]\ : STD_LOGIC;
  signal \total_reg_n_0_[26]\ : STD_LOGIC;
  signal \total_reg_n_0_[27]\ : STD_LOGIC;
  signal \total_reg_n_0_[28]\ : STD_LOGIC;
  signal \total_reg_n_0_[29]\ : STD_LOGIC;
  signal \total_reg_n_0_[2]\ : STD_LOGIC;
  signal \total_reg_n_0_[30]\ : STD_LOGIC;
  signal \total_reg_n_0_[31]\ : STD_LOGIC;
  signal \total_reg_n_0_[3]\ : STD_LOGIC;
  signal \total_reg_n_0_[4]\ : STD_LOGIC;
  signal \total_reg_n_0_[5]\ : STD_LOGIC;
  signal \total_reg_n_0_[6]\ : STD_LOGIC;
  signal \total_reg_n_0_[7]\ : STD_LOGIC;
  signal \total_reg_n_0_[8]\ : STD_LOGIC;
  signal \total_reg_n_0_[9]\ : STD_LOGIC;
  signal \NLW_FSM_sequential_next_state_reg[1]_i_22_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_FSM_sequential_next_state_reg[1]_i_22_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_FSM_sequential_next_state_reg[1]_i_31_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_FSM_sequential_next_state_reg[1]_i_31_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_FSM_sequential_next_state_reg[1]_i_4_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_FSM_sequential_next_state_reg[1]_i_4_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_FSM_sequential_next_state_reg[1]_i_9_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_FSM_sequential_next_state_reg[1]_i_9_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_total_reg[11]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_total_reg[15]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_total_reg[19]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_total_reg[23]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_total_reg[27]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_total_reg[31]_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_total_reg[3]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_total_reg[7]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_current_state_reg[0]\ : label is "iSTATE:11,s1:01,s0:00,iSTATE0:10";
  attribute FSM_ENCODED_STATES of \FSM_sequential_current_state_reg[1]\ : label is "iSTATE:11,s1:01,s0:00,iSTATE0:10";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \FSM_sequential_next_state_reg[0]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \FSM_sequential_next_state_reg[1]\ : label is "LD";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_next_state_reg[1]_i_1\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \FSM_sequential_next_state_reg[1]_i_2\ : label is "soft_lutpair51";
  attribute OPT_MODIFIED : string;
  attribute OPT_MODIFIED of \FSM_sequential_next_state_reg[1]_i_22\ : label is "SWEEP";
  attribute OPT_MODIFIED of \FSM_sequential_next_state_reg[1]_i_31\ : label is "SWEEP";
  attribute OPT_MODIFIED of \FSM_sequential_next_state_reg[1]_i_4\ : label is "SWEEP";
  attribute OPT_MODIFIED of \FSM_sequential_next_state_reg[1]_i_9\ : label is "SWEEP";
  attribute XILINX_LEGACY_PRIM of \aux_reg[0]\ : label is "LD";
  attribute SOFT_HLUTNM of \aux_reg[0]_i_1\ : label is "soft_lutpair52";
  attribute XILINX_LEGACY_PRIM of bdf_reg : label is "LD";
  attribute SOFT_HLUTNM of bdf_reg_i_1 : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of devolver_OBUF_inst_i_1 : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of refresco_OBUF_inst_i_1 : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \total[0]_i_1\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \total[10]_i_1\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \total[11]_i_1\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \total[12]_i_1\ : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of \total[13]_i_1\ : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of \total[14]_i_1\ : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \total[15]_i_1\ : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \total[16]_i_1\ : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of \total[17]_i_1\ : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of \total[18]_i_1\ : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of \total[19]_i_1\ : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of \total[1]_i_1\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \total[20]_i_1\ : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of \total[21]_i_1\ : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of \total[22]_i_1\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \total[23]_i_1\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \total[24]_i_1\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \total[25]_i_1\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \total[26]_i_1\ : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of \total[27]_i_1\ : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of \total[28]_i_1\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \total[29]_i_1\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \total[2]_i_1\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of \total[30]_i_1\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \total[31]_i_2\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \total[3]_i_1\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of \total[4]_i_1\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \total[5]_i_1\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \total[6]_i_1\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \total[7]_i_1\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \total[8]_i_1\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \total[9]_i_1\ : label is "soft_lutpair65";
  attribute OPT_MODIFIED of \total_reg[11]_i_2\ : label is "SWEEP";
  attribute OPT_MODIFIED of \total_reg[15]_i_2\ : label is "SWEEP";
  attribute OPT_MODIFIED of \total_reg[19]_i_2\ : label is "SWEEP";
  attribute OPT_MODIFIED of \total_reg[23]_i_2\ : label is "SWEEP";
  attribute OPT_MODIFIED of \total_reg[27]_i_2\ : label is "SWEEP";
  attribute OPT_MODIFIED of \total_reg[31]_i_3\ : label is "SWEEP";
  attribute OPT_MODIFIED of \total_reg[3]_i_2\ : label is "SWEEP";
  attribute OPT_MODIFIED of \total_reg[7]_i_2\ : label is "SWEEP";
begin
\FSM_sequential_current_state[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D7"
    )
        port map (
      I0 => clr_refresco_r,
      I1 => Q(0),
      I2 => Q(1),
      O => \FSM_sequential_current_state[1]_i_1_n_0\
    );
\FSM_sequential_current_state_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \next_state__0\(0),
      Q => current_state(0)
    );
\FSM_sequential_current_state_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \next_state__0\(1),
      Q => \FSM_sequential_current_state_reg_n_0_[1]\
    );
\FSM_sequential_next_state_reg[0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \FSM_sequential_next_state_reg[0]_i_1_n_0\,
      G => \FSM_sequential_next_state_reg[1]_i_2_n_0\,
      GE => '1',
      Q => \next_state__0\(0)
    );
\FSM_sequential_next_state_reg[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \FSM_sequential_current_state_reg_n_0_[1]\,
      I1 => current_state(0),
      O => \FSM_sequential_next_state_reg[0]_i_1_n_0\
    );
\FSM_sequential_next_state_reg[1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \FSM_sequential_next_state_reg[1]_i_1_n_0\,
      G => \FSM_sequential_next_state_reg[1]_i_2_n_0\,
      GE => '1',
      Q => \next_state__0\(1)
    );
\FSM_sequential_next_state_reg[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2023"
    )
        port map (
      I0 => button_ok_IBUF,
      I1 => \FSM_sequential_current_state_reg_n_0_[1]\,
      I2 => current_state(0),
      I3 => \FSM_sequential_next_state_reg[1]_i_3_n_0\,
      O => \FSM_sequential_next_state_reg[1]_i_1_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \total_reg_n_0_[30]\,
      I1 => \total_reg_n_0_[31]\,
      O => \FSM_sequential_next_state_reg[1]_i_10_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \total_reg_n_0_[29]\,
      I1 => \total_reg_n_0_[28]\,
      O => \FSM_sequential_next_state_reg[1]_i_11_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_12\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \total_reg_n_0_[27]\,
      I1 => \total_reg_n_0_[26]\,
      O => \FSM_sequential_next_state_reg[1]_i_12_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_13\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \total_reg_n_0_[25]\,
      I1 => \total_reg_n_0_[24]\,
      O => \FSM_sequential_next_state_reg[1]_i_13_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_14\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \total_reg_n_0_[30]\,
      I1 => \total_reg_n_0_[31]\,
      O => \FSM_sequential_next_state_reg[1]_i_14_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_15\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \total_reg_n_0_[28]\,
      I1 => \total_reg_n_0_[29]\,
      O => \FSM_sequential_next_state_reg[1]_i_15_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_16\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \total_reg_n_0_[26]\,
      I1 => \total_reg_n_0_[27]\,
      O => \FSM_sequential_next_state_reg[1]_i_16_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_17\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \total_reg_n_0_[24]\,
      I1 => \total_reg_n_0_[25]\,
      O => \FSM_sequential_next_state_reg[1]_i_17_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_18\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \total_reg_n_0_[0]\,
      I1 => \total_reg_n_0_[1]\,
      O => \FSM_sequential_next_state_reg[1]_i_18_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_19\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \total_reg_n_0_[13]\,
      I1 => \total_reg_n_0_[15]\,
      O => \FSM_sequential_next_state_reg[1]_i_19_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAFAAAC"
    )
        port map (
      I0 => button_ok_IBUF,
      I1 => \FSM_sequential_next_state_reg[1]_i_3_n_0\,
      I2 => current_state(0),
      I3 => \FSM_sequential_current_state_reg_n_0_[1]\,
      I4 => data0,
      O => \FSM_sequential_next_state_reg[1]_i_2_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_20\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \total_reg_n_0_[17]\,
      I1 => \total_reg_n_0_[19]\,
      O => \FSM_sequential_next_state_reg[1]_i_20_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_21\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \total_reg_n_0_[21]\,
      I1 => \total_reg_n_0_[23]\,
      O => \FSM_sequential_next_state_reg[1]_i_21_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_22\: unisim.vcomponents.CARRY4
     port map (
      CI => \FSM_sequential_next_state_reg[1]_i_31_n_0\,
      CO(3) => \FSM_sequential_next_state_reg[1]_i_22_n_0\,
      CO(2 downto 0) => \NLW_FSM_sequential_next_state_reg[1]_i_22_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \FSM_sequential_next_state_reg[1]_i_32_n_0\,
      DI(2) => \FSM_sequential_next_state_reg[1]_i_33_n_0\,
      DI(1) => \FSM_sequential_next_state_reg[1]_i_34_n_0\,
      DI(0) => \FSM_sequential_next_state_reg[1]_i_35_n_0\,
      O(3 downto 0) => \NLW_FSM_sequential_next_state_reg[1]_i_22_O_UNCONNECTED\(3 downto 0),
      S(3) => \FSM_sequential_next_state_reg[1]_i_36_n_0\,
      S(2) => \FSM_sequential_next_state_reg[1]_i_37_n_0\,
      S(1) => \FSM_sequential_next_state_reg[1]_i_38_n_0\,
      S(0) => \FSM_sequential_next_state_reg[1]_i_39_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_23\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \total_reg_n_0_[23]\,
      I1 => \total_reg_n_0_[22]\,
      O => \FSM_sequential_next_state_reg[1]_i_23_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_24\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \total_reg_n_0_[21]\,
      I1 => \total_reg_n_0_[20]\,
      O => \FSM_sequential_next_state_reg[1]_i_24_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_25\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \total_reg_n_0_[19]\,
      I1 => \total_reg_n_0_[18]\,
      O => \FSM_sequential_next_state_reg[1]_i_25_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_26\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \total_reg_n_0_[17]\,
      I1 => \total_reg_n_0_[16]\,
      O => \FSM_sequential_next_state_reg[1]_i_26_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_27\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \total_reg_n_0_[22]\,
      I1 => \total_reg_n_0_[23]\,
      O => \FSM_sequential_next_state_reg[1]_i_27_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_28\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \total_reg_n_0_[20]\,
      I1 => \total_reg_n_0_[21]\,
      O => \FSM_sequential_next_state_reg[1]_i_28_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_29\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \total_reg_n_0_[18]\,
      I1 => \total_reg_n_0_[19]\,
      O => \FSM_sequential_next_state_reg[1]_i_29_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000001000000"
    )
        port map (
      I0 => inst_int_to_string_n_9,
      I1 => \total_reg_n_0_[7]\,
      I2 => \total_reg_n_0_[4]\,
      I3 => \FSM_sequential_next_state_reg[1]_i_6_n_0\,
      I4 => \FSM_sequential_next_state_reg[1]_i_7_n_0\,
      I5 => \FSM_sequential_next_state_reg[1]_i_8_n_0\,
      O => \FSM_sequential_next_state_reg[1]_i_3_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_30\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \total_reg_n_0_[16]\,
      I1 => \total_reg_n_0_[17]\,
      O => \FSM_sequential_next_state_reg[1]_i_30_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_31\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \FSM_sequential_next_state_reg[1]_i_31_n_0\,
      CO(2 downto 0) => \NLW_FSM_sequential_next_state_reg[1]_i_31_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \total_reg_n_0_[7]\,
      DI(2) => \FSM_sequential_next_state_reg[1]_i_40_n_0\,
      DI(1) => \total_reg_n_0_[3]\,
      DI(0) => \FSM_sequential_next_state_reg[1]_i_41_n_0\,
      O(3 downto 0) => \NLW_FSM_sequential_next_state_reg[1]_i_31_O_UNCONNECTED\(3 downto 0),
      S(3) => \FSM_sequential_next_state_reg[1]_i_42_n_0\,
      S(2) => \FSM_sequential_next_state_reg[1]_i_43_n_0\,
      S(1) => \FSM_sequential_next_state_reg[1]_i_44_n_0\,
      S(0) => \FSM_sequential_next_state_reg[1]_i_45_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_32\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \total_reg_n_0_[15]\,
      I1 => \total_reg_n_0_[14]\,
      O => \FSM_sequential_next_state_reg[1]_i_32_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_33\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \total_reg_n_0_[13]\,
      I1 => \total_reg_n_0_[12]\,
      O => \FSM_sequential_next_state_reg[1]_i_33_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_34\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \total_reg_n_0_[11]\,
      I1 => \total_reg_n_0_[10]\,
      O => \FSM_sequential_next_state_reg[1]_i_34_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_35\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \total_reg_n_0_[9]\,
      I1 => \total_reg_n_0_[8]\,
      O => \FSM_sequential_next_state_reg[1]_i_35_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_36\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \total_reg_n_0_[14]\,
      I1 => \total_reg_n_0_[15]\,
      O => \FSM_sequential_next_state_reg[1]_i_36_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_37\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \total_reg_n_0_[12]\,
      I1 => \total_reg_n_0_[13]\,
      O => \FSM_sequential_next_state_reg[1]_i_37_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_38\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \total_reg_n_0_[10]\,
      I1 => \total_reg_n_0_[11]\,
      O => \FSM_sequential_next_state_reg[1]_i_38_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_39\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \total_reg_n_0_[8]\,
      I1 => \total_reg_n_0_[9]\,
      O => \FSM_sequential_next_state_reg[1]_i_39_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_4\: unisim.vcomponents.CARRY4
     port map (
      CI => \FSM_sequential_next_state_reg[1]_i_9_n_0\,
      CO(3) => data0,
      CO(2 downto 0) => \NLW_FSM_sequential_next_state_reg[1]_i_4_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \FSM_sequential_next_state_reg[1]_i_10_n_0\,
      DI(2) => \FSM_sequential_next_state_reg[1]_i_11_n_0\,
      DI(1) => \FSM_sequential_next_state_reg[1]_i_12_n_0\,
      DI(0) => \FSM_sequential_next_state_reg[1]_i_13_n_0\,
      O(3 downto 0) => \NLW_FSM_sequential_next_state_reg[1]_i_4_O_UNCONNECTED\(3 downto 0),
      S(3) => \FSM_sequential_next_state_reg[1]_i_14_n_0\,
      S(2) => \FSM_sequential_next_state_reg[1]_i_15_n_0\,
      S(1) => \FSM_sequential_next_state_reg[1]_i_16_n_0\,
      S(0) => \FSM_sequential_next_state_reg[1]_i_17_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_40\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \total_reg_n_0_[5]\,
      I1 => \total_reg_n_0_[4]\,
      O => \FSM_sequential_next_state_reg[1]_i_40_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_41\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \total_reg_n_0_[1]\,
      I1 => \total_reg_n_0_[0]\,
      O => \FSM_sequential_next_state_reg[1]_i_41_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_42\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \total_reg_n_0_[6]\,
      I1 => \total_reg_n_0_[7]\,
      O => \FSM_sequential_next_state_reg[1]_i_42_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_43\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \total_reg_n_0_[5]\,
      I1 => \total_reg_n_0_[4]\,
      O => \FSM_sequential_next_state_reg[1]_i_43_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_44\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \total_reg_n_0_[2]\,
      I1 => \total_reg_n_0_[3]\,
      O => \FSM_sequential_next_state_reg[1]_i_44_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_45\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \total_reg_n_0_[0]\,
      I1 => \total_reg_n_0_[1]\,
      O => \FSM_sequential_next_state_reg[1]_i_45_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \total_reg_n_0_[9]\,
      I1 => \total_reg_n_0_[11]\,
      O => \FSM_sequential_next_state_reg[1]_i_6_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2000000000000000"
    )
        port map (
      I0 => \total_reg_n_0_[2]\,
      I1 => \total_reg_n_0_[3]\,
      I2 => \FSM_sequential_next_state_reg[1]_i_18_n_0\,
      I3 => \FSM_sequential_next_state_reg[1]_i_19_n_0\,
      I4 => \total_reg_n_0_[5]\,
      I5 => \total_reg_n_0_[6]\,
      O => \FSM_sequential_next_state_reg[1]_i_7_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFBFFFFFFFFFFFF"
    )
        port map (
      I0 => inst_int_to_string_n_7,
      I1 => inst_int_to_string_n_10,
      I2 => \total_reg_n_0_[31]\,
      I3 => \total_reg_n_0_[30]\,
      I4 => \FSM_sequential_next_state_reg[1]_i_20_n_0\,
      I5 => \FSM_sequential_next_state_reg[1]_i_21_n_0\,
      O => \FSM_sequential_next_state_reg[1]_i_8_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_9\: unisim.vcomponents.CARRY4
     port map (
      CI => \FSM_sequential_next_state_reg[1]_i_22_n_0\,
      CO(3) => \FSM_sequential_next_state_reg[1]_i_9_n_0\,
      CO(2 downto 0) => \NLW_FSM_sequential_next_state_reg[1]_i_9_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \FSM_sequential_next_state_reg[1]_i_23_n_0\,
      DI(2) => \FSM_sequential_next_state_reg[1]_i_24_n_0\,
      DI(1) => \FSM_sequential_next_state_reg[1]_i_25_n_0\,
      DI(0) => \FSM_sequential_next_state_reg[1]_i_26_n_0\,
      O(3 downto 0) => \NLW_FSM_sequential_next_state_reg[1]_i_9_O_UNCONNECTED\(3 downto 0),
      S(3) => \FSM_sequential_next_state_reg[1]_i_27_n_0\,
      S(2) => \FSM_sequential_next_state_reg[1]_i_28_n_0\,
      S(1) => \FSM_sequential_next_state_reg[1]_i_29_n_0\,
      S(0) => \FSM_sequential_next_state_reg[1]_i_30_n_0\
    );
\aux_reg[0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \FSM_sequential_next_state_reg[0]_i_1_n_0\,
      G => \aux_reg[0]_i_1_n_0\,
      GE => '1',
      Q => aux(0)
    );
\aux_reg[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F1"
    )
        port map (
      I0 => \FSM_sequential_current_state_reg_n_0_[1]\,
      I1 => current_state(0),
      I2 => button_ok_IBUF,
      O => \aux_reg[0]_i_1_n_0\
    );
bdf_reg: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \FSM_sequential_current_state_reg_n_0_[1]\,
      G => bdf_reg_i_1_n_0,
      GE => '1',
      Q => bdf_dinero
    );
bdf_reg_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => current_state(0),
      I1 => \FSM_sequential_current_state_reg_n_0_[1]\,
      O => bdf_reg_i_1_n_0
    );
devolver_OBUF_inst_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_sequential_current_state_reg_n_0_[1]\,
      I1 => current_state(0),
      O => devolver_OBUF
    );
inst_int_to_string: entity work.int_to_string
     port map (
      D(0) => D(0),
      Q(30) => \total_reg_n_0_[30]\,
      Q(29) => \total_reg_n_0_[29]\,
      Q(28) => \total_reg_n_0_[28]\,
      Q(27) => \total_reg_n_0_[27]\,
      Q(26) => \total_reg_n_0_[26]\,
      Q(25) => \total_reg_n_0_[25]\,
      Q(24) => \total_reg_n_0_[24]\,
      Q(23) => \total_reg_n_0_[23]\,
      Q(22) => \total_reg_n_0_[22]\,
      Q(21) => \total_reg_n_0_[21]\,
      Q(20) => \total_reg_n_0_[20]\,
      Q(19) => \total_reg_n_0_[19]\,
      Q(18) => \total_reg_n_0_[18]\,
      Q(17) => \total_reg_n_0_[17]\,
      Q(16) => \total_reg_n_0_[16]\,
      Q(15) => \total_reg_n_0_[15]\,
      Q(14) => \total_reg_n_0_[14]\,
      Q(13) => \total_reg_n_0_[13]\,
      Q(12) => \total_reg_n_0_[12]\,
      Q(11) => \total_reg_n_0_[11]\,
      Q(10) => \total_reg_n_0_[10]\,
      Q(9) => \total_reg_n_0_[9]\,
      Q(8) => \total_reg_n_0_[8]\,
      Q(7) => \total_reg_n_0_[7]\,
      Q(6) => \total_reg_n_0_[6]\,
      Q(5) => \total_reg_n_0_[5]\,
      Q(4) => \total_reg_n_0_[4]\,
      Q(3) => \total_reg_n_0_[3]\,
      Q(2) => \total_reg_n_0_[2]\,
      Q(1) => \total_reg_n_0_[1]\,
      Q(0) => \total_reg_n_0_[0]\,
      \caracter_reg[0]\ => \caracter_reg[0]\,
      \caracter_reg[0]_0\ => \caracter_reg[0]_0\,
      \caracter_reg[0]_1\ => \caracter_reg[0]_1\,
      \caracter_reg[2]\ => \caracter_reg[2]\,
      \caracter_reg[2]_0\ => \caracter_reg[2]_0\,
      \caracter_reg[2]_1\(1 downto 0) => \caracter_reg[2]_1\(1 downto 0),
      \caracter_reg[3]\ => \caracter_reg[3]\,
      \caracter_reg[3]_0\ => \caracter_reg[3]_0\,
      \caracter_reg[3]_1\ => \caracter_reg[3]_1\,
      \caracter_reg[3]_2\ => \caracter_reg[3]_2\,
      \caracter_reg[3]_3\ => \caracter_reg[3]_3\,
      \caracter_reg[3]_4\ => \caracter_reg[3]_4\,
      \caracter_reg[3]_5\ => \caracter_reg[3]_5\,
      \charact_dot_reg[0]\ => \charact_dot_reg[0]\,
      \charact_dot_reg[0]_0\ => \charact_dot_reg[0]_0\,
      \current_state_reg[1]\ => \current_state_reg[1]\,
      \current_state_reg[1]_0\ => \current_state_reg[1]_0\,
      \disp_dinero[5]_1\(0) => \disp_dinero[5]_1\(0),
      \total_reg[0]\ => DI(0),
      \total_reg[20]\ => inst_int_to_string_n_7,
      \total_reg[26]\ => inst_int_to_string_n_10,
      \total_reg[29]\ => \total_reg[29]_0\,
      \total_reg[29]_0\ => inst_int_to_string_n_9
    );
\next_state_reg[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFEA40"
    )
        port map (
      I0 => Q(1),
      I1 => Q(0),
      I2 => \total_reg[7]_i_2_0\(0),
      I3 => bdf_dinero,
      I4 => \current_state_reg[0]\,
      I5 => \current_state_reg[0]_0\,
      O => E(0)
    );
refresco_OBUF_inst_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \FSM_sequential_current_state_reg_n_0_[1]\,
      I1 => current_state(0),
      O => refresco_OBUF
    );
\total[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => aux(0),
      I1 => \total_reg[3]_i_2_n_7\,
      O => \total[0]_i_1_n_0\
    );
\total[10]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => aux(0),
      I1 => \total_reg[11]_i_2_n_5\,
      O => \total[10]_i_1_n_0\
    );
\total[11]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => aux(0),
      I1 => \total_reg[11]_i_2_n_4\,
      O => \total[11]_i_1_n_0\
    );
\total[12]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => aux(0),
      I1 => \total_reg[15]_i_2_n_7\,
      O => \total[12]_i_1_n_0\
    );
\total[13]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => aux(0),
      I1 => \total_reg[15]_i_2_n_6\,
      O => \total[13]_i_1_n_0\
    );
\total[14]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => aux(0),
      I1 => \total_reg[15]_i_2_n_5\,
      O => \total[14]_i_1_n_0\
    );
\total[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => aux(0),
      I1 => \total_reg[15]_i_2_n_4\,
      O => \total[15]_i_1_n_0\
    );
\total[16]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => aux(0),
      I1 => \total_reg[19]_i_2_n_7\,
      O => \total[16]_i_1_n_0\
    );
\total[17]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => aux(0),
      I1 => \total_reg[19]_i_2_n_6\,
      O => \total[17]_i_1_n_0\
    );
\total[18]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => aux(0),
      I1 => \total_reg[19]_i_2_n_5\,
      O => \total[18]_i_1_n_0\
    );
\total[19]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => aux(0),
      I1 => \total_reg[19]_i_2_n_4\,
      O => \total[19]_i_1_n_0\
    );
\total[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => aux(0),
      I1 => \total_reg[3]_i_2_n_6\,
      O => \total[1]_i_1_n_0\
    );
\total[20]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => aux(0),
      I1 => \total_reg[23]_i_2_n_7\,
      O => \total[20]_i_1_n_0\
    );
\total[21]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => aux(0),
      I1 => \total_reg[23]_i_2_n_6\,
      O => \total[21]_i_1_n_0\
    );
\total[22]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => aux(0),
      I1 => \total_reg[23]_i_2_n_5\,
      O => \total[22]_i_1_n_0\
    );
\total[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => aux(0),
      I1 => \total_reg[23]_i_2_n_4\,
      O => \total[23]_i_1_n_0\
    );
\total[24]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => aux(0),
      I1 => \total_reg[27]_i_2_n_7\,
      O => \total[24]_i_1_n_0\
    );
\total[25]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => aux(0),
      I1 => \total_reg[27]_i_2_n_6\,
      O => \total[25]_i_1_n_0\
    );
\total[26]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => aux(0),
      I1 => \total_reg[27]_i_2_n_5\,
      O => \total[26]_i_1_n_0\
    );
\total[27]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => aux(0),
      I1 => \total_reg[27]_i_2_n_4\,
      O => \total[27]_i_1_n_0\
    );
\total[28]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => aux(0),
      I1 => \total_reg[31]_i_3_n_7\,
      O => \total[28]_i_1_n_0\
    );
\total[29]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => aux(0),
      I1 => \total_reg[31]_i_3_n_6\,
      O => \total[29]_i_1_n_0\
    );
\total[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => aux(0),
      I1 => \total_reg[3]_i_2_n_5\,
      O => \total[2]_i_1_n_0\
    );
\total[30]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => aux(0),
      I1 => \total_reg[31]_i_3_n_5\,
      O => \total[30]_i_1_n_0\
    );
\total[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1F"
    )
        port map (
      I0 => current_state(0),
      I1 => \FSM_sequential_current_state_reg_n_0_[1]\,
      I2 => aux(0),
      O => total
    );
\total[31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => aux(0),
      I1 => \total_reg[31]_i_3_n_4\,
      O => \total[31]_i_2_n_0\
    );
\total[3]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => aux(0),
      I1 => \total_reg[3]_i_2_n_4\,
      O => \total[3]_i_1_n_0\
    );
\total[3]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \total_reg_n_0_[3]\,
      I1 => \total_reg[7]_i_2_0\(3),
      O => \total[3]_i_3_n_0\
    );
\total[3]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \total_reg_n_0_[2]\,
      I1 => \total_reg[7]_i_2_0\(2),
      O => \total[3]_i_4_n_0\
    );
\total[3]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \total_reg_n_0_[1]\,
      I1 => \total_reg[7]_i_2_0\(1),
      O => \total[3]_i_5_n_0\
    );
\total[3]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \total_reg_n_0_[0]\,
      I1 => \total_reg[7]_i_2_0\(0),
      O => \total[3]_i_6_n_0\
    );
\total[4]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => aux(0),
      I1 => \total_reg[7]_i_2_n_7\,
      O => \total[4]_i_1_n_0\
    );
\total[5]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => aux(0),
      I1 => \total_reg[7]_i_2_n_6\,
      O => \total[5]_i_1_n_0\
    );
\total[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => aux(0),
      I1 => \total_reg[7]_i_2_n_5\,
      O => \total[6]_i_1_n_0\
    );
\total[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => aux(0),
      I1 => \total_reg[7]_i_2_n_4\,
      O => \total[7]_i_1_n_0\
    );
\total[7]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \total_reg_n_0_[6]\,
      I1 => \total_reg[7]_i_2_0\(6),
      O => \total[7]_i_3_n_0\
    );
\total[7]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \total_reg_n_0_[5]\,
      I1 => \total_reg[7]_i_2_0\(5),
      O => \total[7]_i_4_n_0\
    );
\total[7]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \total_reg_n_0_[4]\,
      I1 => \total_reg[7]_i_2_0\(4),
      O => \total[7]_i_5_n_0\
    );
\total[8]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => aux(0),
      I1 => \total_reg[11]_i_2_n_7\,
      O => \total[8]_i_1_n_0\
    );
\total[9]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => aux(0),
      I1 => \total_reg[11]_i_2_n_6\,
      O => \total[9]_i_1_n_0\
    );
\total_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => total,
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \total[0]_i_1_n_0\,
      Q => \total_reg_n_0_[0]\
    );
\total_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => total,
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \total[10]_i_1_n_0\,
      Q => \total_reg_n_0_[10]\
    );
\total_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => total,
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \total[11]_i_1_n_0\,
      Q => \total_reg_n_0_[11]\
    );
\total_reg[11]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \total_reg[7]_i_2_n_0\,
      CO(3) => \total_reg[11]_i_2_n_0\,
      CO(2 downto 0) => \NLW_total_reg[11]_i_2_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \total_reg_n_0_[11]\,
      DI(2) => \total_reg_n_0_[10]\,
      DI(1) => \total_reg_n_0_[9]\,
      DI(0) => \total_reg_n_0_[8]\,
      O(3) => \total_reg[11]_i_2_n_4\,
      O(2) => \total_reg[11]_i_2_n_5\,
      O(1) => \total_reg[11]_i_2_n_6\,
      O(0) => \total_reg[11]_i_2_n_7\,
      S(3) => \total_reg_n_0_[11]\,
      S(2) => \total_reg_n_0_[10]\,
      S(1) => \total_reg_n_0_[9]\,
      S(0) => \total_reg_n_0_[8]\
    );
\total_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => total,
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \total[12]_i_1_n_0\,
      Q => \total_reg_n_0_[12]\
    );
\total_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => total,
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \total[13]_i_1_n_0\,
      Q => \total_reg_n_0_[13]\
    );
\total_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => total,
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \total[14]_i_1_n_0\,
      Q => \total_reg_n_0_[14]\
    );
\total_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => total,
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \total[15]_i_1_n_0\,
      Q => \total_reg_n_0_[15]\
    );
\total_reg[15]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \total_reg[11]_i_2_n_0\,
      CO(3) => \total_reg[15]_i_2_n_0\,
      CO(2 downto 0) => \NLW_total_reg[15]_i_2_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \total_reg_n_0_[15]\,
      DI(2) => \total_reg_n_0_[14]\,
      DI(1) => \total_reg_n_0_[13]\,
      DI(0) => \total_reg_n_0_[12]\,
      O(3) => \total_reg[15]_i_2_n_4\,
      O(2) => \total_reg[15]_i_2_n_5\,
      O(1) => \total_reg[15]_i_2_n_6\,
      O(0) => \total_reg[15]_i_2_n_7\,
      S(3) => \total_reg_n_0_[15]\,
      S(2) => \total_reg_n_0_[14]\,
      S(1) => \total_reg_n_0_[13]\,
      S(0) => \total_reg_n_0_[12]\
    );
\total_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => total,
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \total[16]_i_1_n_0\,
      Q => \total_reg_n_0_[16]\
    );
\total_reg[17]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => total,
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \total[17]_i_1_n_0\,
      Q => \total_reg_n_0_[17]\
    );
\total_reg[18]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => total,
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \total[18]_i_1_n_0\,
      Q => \total_reg_n_0_[18]\
    );
\total_reg[19]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => total,
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \total[19]_i_1_n_0\,
      Q => \total_reg_n_0_[19]\
    );
\total_reg[19]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \total_reg[15]_i_2_n_0\,
      CO(3) => \total_reg[19]_i_2_n_0\,
      CO(2 downto 0) => \NLW_total_reg[19]_i_2_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \total_reg_n_0_[19]\,
      DI(2) => \total_reg_n_0_[18]\,
      DI(1) => \total_reg_n_0_[17]\,
      DI(0) => \total_reg_n_0_[16]\,
      O(3) => \total_reg[19]_i_2_n_4\,
      O(2) => \total_reg[19]_i_2_n_5\,
      O(1) => \total_reg[19]_i_2_n_6\,
      O(0) => \total_reg[19]_i_2_n_7\,
      S(3) => \total_reg_n_0_[19]\,
      S(2) => \total_reg_n_0_[18]\,
      S(1) => \total_reg_n_0_[17]\,
      S(0) => \total_reg_n_0_[16]\
    );
\total_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => total,
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \total[1]_i_1_n_0\,
      Q => \total_reg_n_0_[1]\
    );
\total_reg[20]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => total,
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \total[20]_i_1_n_0\,
      Q => \total_reg_n_0_[20]\
    );
\total_reg[21]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => total,
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \total[21]_i_1_n_0\,
      Q => \total_reg_n_0_[21]\
    );
\total_reg[22]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => total,
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \total[22]_i_1_n_0\,
      Q => \total_reg_n_0_[22]\
    );
\total_reg[23]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => total,
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \total[23]_i_1_n_0\,
      Q => \total_reg_n_0_[23]\
    );
\total_reg[23]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \total_reg[19]_i_2_n_0\,
      CO(3) => \total_reg[23]_i_2_n_0\,
      CO(2 downto 0) => \NLW_total_reg[23]_i_2_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \total_reg_n_0_[23]\,
      DI(2) => \total_reg_n_0_[22]\,
      DI(1) => \total_reg_n_0_[21]\,
      DI(0) => \total_reg_n_0_[20]\,
      O(3) => \total_reg[23]_i_2_n_4\,
      O(2) => \total_reg[23]_i_2_n_5\,
      O(1) => \total_reg[23]_i_2_n_6\,
      O(0) => \total_reg[23]_i_2_n_7\,
      S(3) => \total_reg_n_0_[23]\,
      S(2) => \total_reg_n_0_[22]\,
      S(1) => \total_reg_n_0_[21]\,
      S(0) => \total_reg_n_0_[20]\
    );
\total_reg[24]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => total,
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \total[24]_i_1_n_0\,
      Q => \total_reg_n_0_[24]\
    );
\total_reg[25]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => total,
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \total[25]_i_1_n_0\,
      Q => \total_reg_n_0_[25]\
    );
\total_reg[26]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => total,
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \total[26]_i_1_n_0\,
      Q => \total_reg_n_0_[26]\
    );
\total_reg[27]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => total,
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \total[27]_i_1_n_0\,
      Q => \total_reg_n_0_[27]\
    );
\total_reg[27]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \total_reg[23]_i_2_n_0\,
      CO(3) => \total_reg[27]_i_2_n_0\,
      CO(2 downto 0) => \NLW_total_reg[27]_i_2_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \total_reg_n_0_[27]\,
      DI(2) => \total_reg_n_0_[26]\,
      DI(1) => \total_reg_n_0_[25]\,
      DI(0) => \total_reg_n_0_[24]\,
      O(3) => \total_reg[27]_i_2_n_4\,
      O(2) => \total_reg[27]_i_2_n_5\,
      O(1) => \total_reg[27]_i_2_n_6\,
      O(0) => \total_reg[27]_i_2_n_7\,
      S(3) => \total_reg_n_0_[27]\,
      S(2) => \total_reg_n_0_[26]\,
      S(1) => \total_reg_n_0_[25]\,
      S(0) => \total_reg_n_0_[24]\
    );
\total_reg[28]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => total,
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \total[28]_i_1_n_0\,
      Q => \total_reg_n_0_[28]\
    );
\total_reg[29]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => total,
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \total[29]_i_1_n_0\,
      Q => \total_reg_n_0_[29]\
    );
\total_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => total,
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \total[2]_i_1_n_0\,
      Q => \total_reg_n_0_[2]\
    );
\total_reg[30]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => total,
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \total[30]_i_1_n_0\,
      Q => \total_reg_n_0_[30]\
    );
\total_reg[31]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => total,
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \total[31]_i_2_n_0\,
      Q => \total_reg_n_0_[31]\
    );
\total_reg[31]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \total_reg[27]_i_2_n_0\,
      CO(3 downto 0) => \NLW_total_reg[31]_i_3_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \total_reg_n_0_[30]\,
      DI(1) => \total_reg_n_0_[29]\,
      DI(0) => \total_reg_n_0_[28]\,
      O(3) => \total_reg[31]_i_3_n_4\,
      O(2) => \total_reg[31]_i_3_n_5\,
      O(1) => \total_reg[31]_i_3_n_6\,
      O(0) => \total_reg[31]_i_3_n_7\,
      S(3) => \total_reg_n_0_[31]\,
      S(2) => \total_reg_n_0_[30]\,
      S(1) => \total_reg_n_0_[29]\,
      S(0) => \total_reg_n_0_[28]\
    );
\total_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => total,
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \total[3]_i_1_n_0\,
      Q => \total_reg_n_0_[3]\
    );
\total_reg[3]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \total_reg[3]_i_2_n_0\,
      CO(2 downto 0) => \NLW_total_reg[3]_i_2_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \total_reg_n_0_[3]\,
      DI(2) => \total_reg_n_0_[2]\,
      DI(1) => \total_reg_n_0_[1]\,
      DI(0) => \total_reg_n_0_[0]\,
      O(3) => \total_reg[3]_i_2_n_4\,
      O(2) => \total_reg[3]_i_2_n_5\,
      O(1) => \total_reg[3]_i_2_n_6\,
      O(0) => \total_reg[3]_i_2_n_7\,
      S(3) => \total[3]_i_3_n_0\,
      S(2) => \total[3]_i_4_n_0\,
      S(1) => \total[3]_i_5_n_0\,
      S(0) => \total[3]_i_6_n_0\
    );
\total_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => total,
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \total[4]_i_1_n_0\,
      Q => \total_reg_n_0_[4]\
    );
\total_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => total,
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \total[5]_i_1_n_0\,
      Q => \total_reg_n_0_[5]\
    );
\total_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => total,
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \total[6]_i_1_n_0\,
      Q => \total_reg_n_0_[6]\
    );
\total_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => total,
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \total[7]_i_1_n_0\,
      Q => \total_reg_n_0_[7]\
    );
\total_reg[7]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \total_reg[3]_i_2_n_0\,
      CO(3) => \total_reg[7]_i_2_n_0\,
      CO(2 downto 0) => \NLW_total_reg[7]_i_2_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \total_reg_n_0_[7]\,
      DI(2) => \total_reg_n_0_[6]\,
      DI(1) => \total_reg_n_0_[5]\,
      DI(0) => \total_reg_n_0_[4]\,
      O(3) => \total_reg[7]_i_2_n_4\,
      O(2) => \total_reg[7]_i_2_n_5\,
      O(1) => \total_reg[7]_i_2_n_6\,
      O(0) => \total_reg[7]_i_2_n_7\,
      S(3) => \total_reg_n_0_[7]\,
      S(2) => \total[7]_i_3_n_0\,
      S(1) => \total[7]_i_4_n_0\,
      S(0) => \total[7]_i_5_n_0\
    );
\total_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => total,
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \total[8]_i_1_n_0\,
      Q => \total_reg_n_0_[8]\
    );
\total_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => total,
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => \total[9]_i_1_n_0\,
      Q => \total_reg_n_0_[9]\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity prescaler is
  port (
    output0 : out STD_LOGIC;
    clk : out STD_LOGIC;
    clk_100Mh_IBUF_BUFG : in STD_LOGIC;
    reset_IBUF : in STD_LOGIC
  );
end prescaler;

architecture STRUCTURE of prescaler is
  signal \^clk\ : STD_LOGIC;
  signal uut_n_1 : STD_LOGIC;
begin
  clk <= \^clk\;
clk_pre_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      D => uut_n_1,
      Q => \^clk\,
      R => '0'
    );
uut: entity work.contador
     port map (
      clk => \^clk\,
      clk_100Mh_IBUF_BUFG => clk_100Mh_IBUF_BUFG,
      clk_pre_reg => uut_n_1,
      reset => output0,
      reset_IBUF => reset_IBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity sincronizador is
  port (
    \sreg_reg[0]\ : out STD_LOGIC;
    \sreg_reg[0]_0\ : out STD_LOGIC;
    \sreg_reg[0]_1\ : out STD_LOGIC;
    \sreg_reg[0]_2\ : out STD_LOGIC;
    \sreg_reg[0]_3\ : out STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    monedas_IBUF : in STD_LOGIC_VECTOR ( 4 downto 0 )
  );
end sincronizador;

architecture STRUCTURE of sincronizador is
begin
inst_synchrnzr0: entity work.synchrnzr
     port map (
      clk_BUFG => clk_BUFG,
      monedas_IBUF(0) => monedas_IBUF(0),
      \sreg_reg[0]_0\ => \sreg_reg[0]\
    );
inst_synchrnzr1: entity work.synchrnzr_0
     port map (
      clk_BUFG => clk_BUFG,
      monedas_IBUF(0) => monedas_IBUF(1),
      \sreg_reg[0]_0\ => \sreg_reg[0]_0\
    );
inst_synchrnzr2: entity work.synchrnzr_1
     port map (
      clk_BUFG => clk_BUFG,
      monedas_IBUF(0) => monedas_IBUF(2),
      \sreg_reg[0]_0\ => \sreg_reg[0]_1\
    );
inst_synchrnzr3: entity work.synchrnzr_2
     port map (
      clk_BUFG => clk_BUFG,
      monedas_IBUF(0) => monedas_IBUF(3),
      \sreg_reg[0]_0\ => \sreg_reg[0]_2\
    );
inst_synchrnzr4: entity work.synchrnzr_3
     port map (
      clk_BUFG => clk_BUFG,
      monedas_IBUF(0) => monedas_IBUF(4),
      \sreg_reg[0]_0\ => \sreg_reg[0]_3\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity fsm is
  port (
    \charact_dot_reg[0]\ : out STD_LOGIC;
    \current_state_reg[1]_0\ : out STD_LOGIC;
    \disp_dinero[1]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    D : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \current_state_reg[0]_0\ : out STD_LOGIC;
    \reg_reg[1]\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \current_state_reg[1]_1\ : out STD_LOGIC;
    \FSM_onehot_current_refresco_reg[3]\ : out STD_LOGIC;
    \FSM_onehot_current_refresco_reg[4]\ : out STD_LOGIC_VECTOR ( 4 downto 0 );
    \current_state_reg[1]_2\ : out STD_LOGIC;
    \disp_dinero[5]_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \FSM_onehot_current_refresco_reg[1]\ : out STD_LOGIC;
    \FSM_onehot_current_refresco_reg[4]_0\ : out STD_LOGIC;
    \current_state_reg[1]_3\ : out STD_LOGIC;
    \FSM_onehot_current_refresco_reg[1]_0\ : out STD_LOGIC;
    \current_state_reg[1]_4\ : out STD_LOGIC;
    \FSM_onehot_current_refresco_reg[2]\ : out STD_LOGIC;
    \FSM_onehot_current_refresco_reg[4]_1\ : out STD_LOGIC;
    \FSM_onehot_current_refresco_reg[0]\ : out STD_LOGIC;
    \charact_dot_reg[0]_0\ : out STD_LOGIC;
    \FSM_onehot_current_refresco_reg[1]_1\ : out STD_LOGIC;
    \current_state_reg[0]_1\ : out STD_LOGIC;
    \FSM_onehot_current_refresco_reg[3]_0\ : out STD_LOGIC;
    \current_state_reg[1]_5\ : out STD_LOGIC;
    \current_state_reg[1]_6\ : out STD_LOGIC;
    \total_reg[0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    refresco_OBUF : out STD_LOGIC;
    devolver_OBUF : out STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    output0 : in STD_LOGIC;
    \caracter_reg[2]\ : in STD_LOGIC;
    \caracter[3]_i_32\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \caracter_reg[3]\ : in STD_LOGIC;
    \caracter_reg[3]_0\ : in STD_LOGIC;
    \caracter_reg[3]_1\ : in STD_LOGIC;
    \caracter_reg[0]\ : in STD_LOGIC;
    \caracter_reg[0]_0\ : in STD_LOGIC;
    \caracter_reg[3]_2\ : in STD_LOGIC;
    \caracter_reg[6]\ : in STD_LOGIC;
    \caracter_reg[1]\ : in STD_LOGIC;
    \caracter_reg[1]_0\ : in STD_LOGIC;
    \caracter_reg[6]_0\ : in STD_LOGIC;
    \caracter_reg[6]_1\ : in STD_LOGIC;
    \caracter[0]_i_2\ : in STD_LOGIC;
    \caracter_reg[6]_2\ : in STD_LOGIC;
    \caracter_reg[6]_3\ : in STD_LOGIC;
    \caracter[1]_i_2\ : in STD_LOGIC;
    \caracter_reg[2]_0\ : in STD_LOGIC;
    \caracter_reg[2]_1\ : in STD_LOGIC;
    \caracter_reg[2]_2\ : in STD_LOGIC;
    \total_reg[7]_i_2\ : in STD_LOGIC_VECTOR ( 6 downto 0 );
    \current_state_reg[0]_2\ : in STD_LOGIC;
    button_ok_IBUF : in STD_LOGIC;
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter_reg[3]_3\ : in STD_LOGIC;
    ref_option_IBUF : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
end fsm;

architecture STRUCTURE of fsm is
  signal Inst_gestion_dinero_n_4 : STD_LOGIC;
  signal Inst_gestion_dinero_n_5 : STD_LOGIC;
  signal Inst_gestion_refresco_n_17 : STD_LOGIC;
  signal Inst_gestion_refresco_n_20 : STD_LOGIC;
  signal \^q\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal clr_refresco_r : STD_LOGIC;
  signal \^current_state_reg[0]_0\ : STD_LOGIC;
  signal \^current_state_reg[1]_0\ : STD_LOGIC;
  signal \^current_state_reg[1]_2\ : STD_LOGIC;
  signal \^current_state_reg[1]_4\ : STD_LOGIC;
  signal \^disp_dinero[1]_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^disp_dinero[5]_1\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal next_state : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \next_state_reg[0]_i_1_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \caracter[5]_i_2\ : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of \caracter[6]_i_3\ : label is "soft_lutpair75";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \next_state_reg[0]\ : label is "LD";
  attribute SOFT_HLUTNM of \next_state_reg[0]_i_1\ : label is "soft_lutpair76";
  attribute XILINX_LEGACY_PRIM of \next_state_reg[1]\ : label is "LD";
  attribute SOFT_HLUTNM of \next_state_reg[1]_i_5\ : label is "soft_lutpair76";
begin
  Q(1 downto 0) <= \^q\(1 downto 0);
  \current_state_reg[0]_0\ <= \^current_state_reg[0]_0\;
  \current_state_reg[1]_0\ <= \^current_state_reg[1]_0\;
  \current_state_reg[1]_2\ <= \^current_state_reg[1]_2\;
  \current_state_reg[1]_4\ <= \^current_state_reg[1]_4\;
  \disp_dinero[1]_0\(0) <= \^disp_dinero[1]_0\(0);
  \disp_dinero[5]_1\(0) <= \^disp_dinero[5]_1\(0);
Inst_gestion_dinero: entity work.gestion_dinero
     port map (
      D(0) => D(0),
      DI(0) => \total_reg[0]\(0),
      E(0) => Inst_gestion_dinero_n_5,
      Q(1 downto 0) => \^q\(1 downto 0),
      button_ok_IBUF => button_ok_IBUF,
      \caracter_reg[0]\ => Inst_gestion_refresco_n_20,
      \caracter_reg[0]_0\ => \caracter_reg[0]\,
      \caracter_reg[0]_1\ => \caracter_reg[0]_0\,
      \caracter_reg[2]\ => \^current_state_reg[1]_0\,
      \caracter_reg[2]_0\ => \caracter_reg[2]\,
      \caracter_reg[2]_1\(1 downto 0) => \caracter[3]_i_32\(1 downto 0),
      \caracter_reg[3]\ => \caracter_reg[3]\,
      \caracter_reg[3]_0\ => \^current_state_reg[0]_0\,
      \caracter_reg[3]_1\ => \caracter_reg[3]_0\,
      \caracter_reg[3]_2\ => \caracter_reg[3]_1\,
      \caracter_reg[3]_3\ => \caracter_reg[3]_2\,
      \caracter_reg[3]_4\ => \caracter_reg[6]_0\,
      \caracter_reg[3]_5\ => \caracter_reg[1]\,
      \charact_dot_reg[0]\ => \charact_dot_reg[0]\,
      \charact_dot_reg[0]_0\ => Inst_gestion_dinero_n_4,
      clk_BUFG => clk_BUFG,
      clr_refresco_r => clr_refresco_r,
      \current_state_reg[0]\ => Inst_gestion_refresco_n_17,
      \current_state_reg[0]_0\ => \current_state_reg[0]_2\,
      \current_state_reg[1]\ => \current_state_reg[1]_1\,
      \current_state_reg[1]_0\ => \current_state_reg[1]_5\,
      devolver_OBUF => devolver_OBUF,
      \disp_dinero[5]_1\(0) => \^disp_dinero[5]_1\(0),
      refresco_OBUF => refresco_OBUF,
      \total_reg[29]_0\ => \^disp_dinero[1]_0\(0),
      \total_reg[7]_i_2_0\(6 downto 0) => \total_reg[7]_i_2\(6 downto 0)
    );
Inst_gestion_refresco: entity work.gestion_refresco
     port map (
      CO(0) => CO(0),
      D(1 downto 0) => D(2 downto 1),
      \FSM_onehot_current_refresco_reg[0]_0\ => \FSM_onehot_current_refresco_reg[0]\,
      \FSM_onehot_current_refresco_reg[1]_0\ => \FSM_onehot_current_refresco_reg[1]\,
      \FSM_onehot_current_refresco_reg[1]_1\ => \FSM_onehot_current_refresco_reg[1]_0\,
      \FSM_onehot_current_refresco_reg[1]_2\ => \FSM_onehot_current_refresco_reg[1]_1\,
      \FSM_onehot_current_refresco_reg[2]_0\ => \FSM_onehot_current_refresco_reg[2]\,
      \FSM_onehot_current_refresco_reg[3]_0\ => \FSM_onehot_current_refresco_reg[3]\,
      \FSM_onehot_current_refresco_reg[3]_1\ => \FSM_onehot_current_refresco_reg[3]_0\,
      \FSM_onehot_current_refresco_reg[4]_0\(4 downto 0) => \FSM_onehot_current_refresco_reg[4]\(4 downto 0),
      \FSM_onehot_current_refresco_reg[4]_1\ => \FSM_onehot_current_refresco_reg[4]_0\,
      \FSM_onehot_current_refresco_reg[4]_2\ => \FSM_onehot_current_refresco_reg[4]_1\,
      Q(1 downto 0) => \^q\(1 downto 0),
      button_ok_IBUF => button_ok_IBUF,
      \caracter[0]_i_2\ => \caracter_reg[3]_2\,
      \caracter[0]_i_2_0\ => \caracter[0]_i_2\,
      \caracter[0]_i_2_1\ => \^current_state_reg[1]_4\,
      \caracter[1]_i_2\ => \caracter[1]_i_2\,
      \caracter[1]_i_2_0\ => \caracter_reg[0]_0\,
      \caracter[3]_i_15\ => \caracter_reg[2]\,
      \caracter[3]_i_32_0\(2 downto 0) => \caracter[3]_i_32\(2 downto 0),
      \caracter_reg[1]\ => \caracter_reg[1]\,
      \caracter_reg[1]_0\ => Inst_gestion_dinero_n_4,
      \caracter_reg[1]_1\ => \caracter_reg[3]\,
      \caracter_reg[1]_2\ => \caracter_reg[1]_0\,
      \caracter_reg[2]\ => \caracter_reg[2]_0\,
      \caracter_reg[2]_0\ => \caracter_reg[2]_1\,
      \caracter_reg[2]_1\ => \caracter_reg[2]_2\,
      \caracter_reg[3]\ => \caracter_reg[3]_3\,
      \caracter_reg[5]\ => \^current_state_reg[1]_2\,
      \caracter_reg[6]\ => \caracter_reg[6]\,
      \caracter_reg[6]_0\ => \^current_state_reg[1]_0\,
      \caracter_reg[6]_1\ => \caracter_reg[6]_1\,
      \caracter_reg[6]_2\ => \caracter_reg[6]_0\,
      \caracter_reg[6]_3\ => \caracter_reg[6]_2\,
      \caracter_reg[6]_4\ => \caracter_reg[6]_3\,
      \charact_dot_reg[0]\ => \charact_dot_reg[0]_0\,
      clk_BUFG => clk_BUFG,
      clr_refresco_r => clr_refresco_r,
      \current_state_reg[0]\ => \^current_state_reg[0]_0\,
      \current_state_reg[1]\ => \current_state_reg[1]_3\,
      \current_state_reg[1]_0\ => Inst_gestion_refresco_n_20,
      \current_state_reg[1]_1\ => \current_state_reg[1]_6\,
      \disp_dinero[1]_0\(0) => \^disp_dinero[1]_0\(0),
      \disp_dinero[5]_1\(0) => \^disp_dinero[5]_1\(0),
      \next_state_reg[1]_i_1\(0) => \total_reg[7]_i_2\(6),
      ref_option_IBUF(3 downto 0) => ref_option_IBUF(3 downto 0),
      \reg_reg[1]\ => \reg_reg[1]\,
      \v_reg[6]\ => Inst_gestion_refresco_n_17
    );
\caracter[2]_i_17\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^q\(1),
      I1 => CO(0),
      O => \^current_state_reg[1]_4\
    );
\caracter[5]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => CO(0),
      I1 => \^q\(1),
      I2 => \^q\(0),
      O => \^current_state_reg[1]_2\
    );
\caracter[6]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0002"
    )
        port map (
      I0 => \^q\(1),
      I1 => CO(0),
      I2 => \^q\(0),
      I3 => \caracter_reg[6]_2\,
      O => \^current_state_reg[1]_0\
    );
clr_dinero_r_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => '1',
      Q => clr_refresco_r
    );
\current_state_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => next_state(0),
      Q => \^q\(0)
    );
\current_state_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => next_state(1),
      Q => \^q\(1)
    );
\next_state_reg[0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \next_state_reg[0]_i_1_n_0\,
      G => Inst_gestion_dinero_n_5,
      GE => '1',
      Q => next_state(0)
    );
\next_state_reg[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      O => \next_state_reg[0]_i_1_n_0\
    );
\next_state_reg[1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \^q\(0),
      G => Inst_gestion_dinero_n_5,
      GE => '1',
      Q => next_state(1)
    );
\next_state_reg[1]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      O => \current_state_reg[0]_1\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity top is
  port (
    clk_100Mh : in STD_LOGIC;
    monedas : in STD_LOGIC_VECTOR ( 4 downto 0 );
    ref_option : in STD_LOGIC_VECTOR ( 3 downto 0 );
    button_ok : in STD_LOGIC;
    reset : in STD_LOGIC;
    devolver : out STD_LOGIC;
    refresco : out STD_LOGIC;
    digit : out STD_LOGIC_VECTOR ( 6 downto 0 );
    dot : out STD_LOGIC;
    elem : out STD_LOGIC_VECTOR ( 7 downto 0 );
    letra : out STD_LOGIC_VECTOR ( 7 downto 0 );
    st : out STD_LOGIC_VECTOR ( 1 downto 0 );
    v_mon : out STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of top : entity is true;
  attribute ECO_CHECKSUM : string;
  attribute ECO_CHECKSUM of top : entity is "8badc1cc";
  attribute digit_number : integer;
  attribute digit_number of top : entity is 8;
  attribute division_prescaler : integer;
  attribute division_prescaler of top : entity is 250000;
  attribute num_refrescos : integer;
  attribute num_refrescos of top : entity is 4;
  attribute size_counter : integer;
  attribute size_counter of top : entity is 3;
  attribute size_prescaler : integer;
  attribute size_prescaler of top : entity is 17;
  attribute width_word : integer;
  attribute width_word of top : entity is 8;
end top;

architecture STRUCTURE of top is
  signal \Inst_gestion_dinero/cent\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal button_ok_IBUF : STD_LOGIC;
  signal caracter : STD_LOGIC_VECTOR ( 6 downto 3 );
  signal caracter2 : STD_LOGIC_VECTOR ( 3 to 3 );
  signal clk : STD_LOGIC;
  signal clk_100Mh_IBUF : STD_LOGIC;
  signal clk_100Mh_IBUF_BUFG : STD_LOGIC;
  signal clk_BUFG : STD_LOGIC;
  signal devolver_OBUF : STD_LOGIC;
  signal digit_OBUF : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \disp_dinero[1]_1\ : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \disp_dinero[5]_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal dot_OBUF : STD_LOGIC;
  signal elem_OBUF : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal inst_decodmonedas_n_0 : STD_LOGIC;
  signal inst_detectorflancos_n_0 : STD_LOGIC;
  signal inst_detectorflancos_n_1 : STD_LOGIC;
  signal inst_detectorflancos_n_2 : STD_LOGIC;
  signal inst_detectorflancos_n_3 : STD_LOGIC;
  signal inst_detectorflancos_n_4 : STD_LOGIC;
  signal inst_detectorflancos_n_5 : STD_LOGIC;
  signal inst_detectorflancos_n_6 : STD_LOGIC;
  signal inst_display_n_1 : STD_LOGIC;
  signal inst_display_n_12 : STD_LOGIC;
  signal inst_display_n_13 : STD_LOGIC;
  signal inst_display_n_14 : STD_LOGIC;
  signal inst_display_n_15 : STD_LOGIC;
  signal inst_display_n_16 : STD_LOGIC;
  signal inst_display_n_17 : STD_LOGIC;
  signal inst_display_n_18 : STD_LOGIC;
  signal inst_display_n_19 : STD_LOGIC;
  signal inst_display_n_2 : STD_LOGIC;
  signal inst_display_n_20 : STD_LOGIC;
  signal inst_display_n_21 : STD_LOGIC;
  signal inst_display_n_22 : STD_LOGIC;
  signal inst_display_n_3 : STD_LOGIC;
  signal inst_display_n_32 : STD_LOGIC;
  signal inst_display_n_4 : STD_LOGIC;
  signal inst_display_n_5 : STD_LOGIC;
  signal inst_display_n_6 : STD_LOGIC;
  signal inst_display_n_7 : STD_LOGIC;
  signal inst_display_n_8 : STD_LOGIC;
  signal inst_fsm_n_0 : STD_LOGIC;
  signal inst_fsm_n_1 : STD_LOGIC;
  signal inst_fsm_n_10 : STD_LOGIC;
  signal inst_fsm_n_11 : STD_LOGIC;
  signal inst_fsm_n_12 : STD_LOGIC;
  signal inst_fsm_n_13 : STD_LOGIC;
  signal inst_fsm_n_14 : STD_LOGIC;
  signal inst_fsm_n_15 : STD_LOGIC;
  signal inst_fsm_n_16 : STD_LOGIC;
  signal inst_fsm_n_17 : STD_LOGIC;
  signal inst_fsm_n_19 : STD_LOGIC;
  signal inst_fsm_n_20 : STD_LOGIC;
  signal inst_fsm_n_21 : STD_LOGIC;
  signal inst_fsm_n_22 : STD_LOGIC;
  signal inst_fsm_n_23 : STD_LOGIC;
  signal inst_fsm_n_24 : STD_LOGIC;
  signal inst_fsm_n_25 : STD_LOGIC;
  signal inst_fsm_n_26 : STD_LOGIC;
  signal inst_fsm_n_27 : STD_LOGIC;
  signal inst_fsm_n_28 : STD_LOGIC;
  signal inst_fsm_n_29 : STD_LOGIC;
  signal inst_fsm_n_30 : STD_LOGIC;
  signal inst_fsm_n_31 : STD_LOGIC;
  signal inst_fsm_n_32 : STD_LOGIC;
  signal inst_fsm_n_6 : STD_LOGIC;
  signal inst_fsm_n_7 : STD_LOGIC;
  signal inst_sincronizador_n_0 : STD_LOGIC;
  signal inst_sincronizador_n_1 : STD_LOGIC;
  signal inst_sincronizador_n_2 : STD_LOGIC;
  signal inst_sincronizador_n_3 : STD_LOGIC;
  signal inst_sincronizador_n_4 : STD_LOGIC;
  signal lopt : STD_LOGIC;
  signal lopt_1 : STD_LOGIC;
  signal lopt_10 : STD_LOGIC;
  signal lopt_11 : STD_LOGIC;
  signal lopt_12 : STD_LOGIC;
  signal lopt_13 : STD_LOGIC;
  signal lopt_2 : STD_LOGIC;
  signal lopt_3 : STD_LOGIC;
  signal lopt_4 : STD_LOGIC;
  signal lopt_5 : STD_LOGIC;
  signal lopt_6 : STD_LOGIC;
  signal lopt_7 : STD_LOGIC;
  signal lopt_8 : STD_LOGIC;
  signal lopt_9 : STD_LOGIC;
  signal monedas_IBUF : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal output0 : STD_LOGIC;
  signal ref_option_IBUF : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal refresco_OBUF : STD_LOGIC;
  signal reg : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal reset_IBUF : STD_LOGIC;
  signal st_OBUF : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal v_mon_OBUF : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \NLW_inst_display_caracter_reg[6]_0_UNCONNECTED\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  attribute OPT_MODIFIED : string;
  attribute OPT_MODIFIED of \letra_OBUF[0]_inst\ : label is "SWEEP";
  attribute OPT_MODIFIED of \letra_OBUF[1]_inst\ : label is "SWEEP";
  attribute OPT_MODIFIED of \letra_OBUF[2]_inst\ : label is "SWEEP";
  attribute OPT_MODIFIED of \letra_OBUF[3]_inst\ : label is "SWEEP";
  attribute OPT_MODIFIED of \letra_OBUF[4]_inst\ : label is "SWEEP";
  attribute OPT_MODIFIED of \letra_OBUF[5]_inst\ : label is "SWEEP";
  attribute OPT_MODIFIED of \letra_OBUF[6]_inst\ : label is "SWEEP";
  attribute OPT_MODIFIED of \v_mon_OBUF[0]_inst\ : label is "SWEEP";
  attribute OPT_MODIFIED of \v_mon_OBUF[1]_inst\ : label is "SWEEP";
  attribute OPT_MODIFIED of \v_mon_OBUF[2]_inst\ : label is "SWEEP";
  attribute OPT_MODIFIED of \v_mon_OBUF[3]_inst\ : label is "SWEEP";
  attribute OPT_MODIFIED of \v_mon_OBUF[4]_inst\ : label is "SWEEP";
  attribute OPT_MODIFIED of \v_mon_OBUF[5]_inst\ : label is "SWEEP";
  attribute OPT_MODIFIED of \v_mon_OBUF[6]_inst\ : label is "SWEEP";
begin
button_ok_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => button_ok,
      O => button_ok_IBUF
    );
clk_100Mh_IBUF_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => clk_100Mh_IBUF,
      O => clk_100Mh_IBUF_BUFG
    );
clk_100Mh_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => clk_100Mh,
      O => clk_100Mh_IBUF
    );
clk_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => clk,
      O => clk_BUFG
    );
devolver_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => devolver_OBUF,
      O => devolver
    );
\digit_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digit_OBUF(0),
      O => digit(0)
    );
\digit_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digit_OBUF(1),
      O => digit(1)
    );
\digit_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digit_OBUF(2),
      O => digit(2)
    );
\digit_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digit_OBUF(3),
      O => digit(3)
    );
\digit_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digit_OBUF(4),
      O => digit(4)
    );
\digit_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digit_OBUF(5),
      O => digit(5)
    );
\digit_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digit_OBUF(6),
      O => digit(6)
    );
dot_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => dot_OBUF,
      O => dot
    );
\elem_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => elem_OBUF(0),
      O => elem(0)
    );
\elem_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => elem_OBUF(1),
      O => elem(1)
    );
\elem_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => elem_OBUF(2),
      O => elem(2)
    );
\elem_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => elem_OBUF(3),
      O => elem(3)
    );
\elem_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => elem_OBUF(4),
      O => elem(4)
    );
\elem_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => elem_OBUF(5),
      O => elem(5)
    );
\elem_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => elem_OBUF(6),
      O => elem(6)
    );
\elem_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => elem_OBUF(7),
      O => elem(7)
    );
inst_decodmonedas: entity work.decod_monedas
     port map (
      D(6) => inst_detectorflancos_n_0,
      D(5) => inst_detectorflancos_n_1,
      D(4) => inst_detectorflancos_n_2,
      D(3) => inst_detectorflancos_n_3,
      D(2) => inst_detectorflancos_n_4,
      D(1) => inst_detectorflancos_n_5,
      D(0) => inst_detectorflancos_n_6,
      Q(6 downto 0) => v_mon_OBUF(6 downto 0),
      clk_BUFG => clk_BUFG,
      lopt => lopt_7,
      lopt_1 => lopt_8,
      lopt_2 => lopt_9,
      lopt_3 => lopt_10,
      lopt_4 => lopt_11,
      lopt_5 => lopt_12,
      lopt_6 => lopt_13,
      \next_state_reg[1]_i_1\ => inst_fsm_n_29,
      \v_reg[1]_0\ => inst_decodmonedas_n_0
    );
inst_detectorflancos: entity work.detectorflancos
     port map (
      D(6) => inst_detectorflancos_n_0,
      D(5) => inst_detectorflancos_n_1,
      D(4) => inst_detectorflancos_n_2,
      D(3) => inst_detectorflancos_n_3,
      D(2) => inst_detectorflancos_n_4,
      D(1) => inst_detectorflancos_n_5,
      D(0) => inst_detectorflancos_n_6,
      clk_BUFG => clk_BUFG,
      \sreg_reg[0]\ => inst_sincronizador_n_0,
      \sreg_reg[0]_0\ => inst_sincronizador_n_1,
      \sreg_reg[0]_1\ => inst_sincronizador_n_2,
      \sreg_reg[0]_2\ => inst_sincronizador_n_3,
      \sreg_reg[0]_3\ => inst_sincronizador_n_4
    );
inst_display: entity work.display
     port map (
      CO(0) => caracter2(3),
      D(2 downto 1) => caracter(6 downto 5),
      D(0) => caracter(3),
      \FSM_onehot_current_refresco_reg[0]\ => inst_display_n_20,
      \FSM_onehot_current_refresco_reg[1]\ => inst_display_n_17,
      \FSM_onehot_current_refresco_reg[2]\ => inst_display_n_18,
      \FSM_onehot_current_refresco_reg[4]\ => inst_display_n_7,
      Q(4) => inst_fsm_n_12,
      Q(3) => inst_fsm_n_13,
      Q(2) => inst_fsm_n_14,
      Q(1) => inst_fsm_n_15,
      Q(0) => inst_fsm_n_16,
      \caracter[2]_i_2\ => inst_fsm_n_27,
      \caracter[2]_i_6\ => inst_fsm_n_23,
      \caracter[2]_i_6_0\ => inst_fsm_n_28,
      \caracter_reg[0]_0\ => inst_fsm_n_20,
      \caracter_reg[0]_1\ => inst_fsm_n_10,
      \caracter_reg[0]_2\ => inst_fsm_n_1,
      \caracter_reg[0]_3\ => inst_fsm_n_22,
      \caracter_reg[0]_4\ => inst_fsm_n_11,
      \caracter_reg[1]_0\ => inst_fsm_n_31,
      \caracter_reg[1]_1\ => inst_fsm_n_19,
      \caracter_reg[1]_2\ => inst_fsm_n_25,
      \caracter_reg[1]_3\ => inst_fsm_n_6,
      \caracter_reg[2]_0\ => inst_fsm_n_21,
      \caracter_reg[2]_1\ => inst_fsm_n_0,
      \caracter_reg[2]_2\ => inst_fsm_n_26,
      \caracter_reg[3]_0\ => inst_fsm_n_32,
      \caracter_reg[4]_0\ => inst_fsm_n_17,
      \caracter_reg[6]_0\(6 downto 0) => \NLW_inst_display_caracter_reg[6]_0_UNCONNECTED\(6 downto 0),
      \caracter_reg[6]_1\ => inst_fsm_n_30,
      cent(0) => \Inst_gestion_dinero/cent\(0),
      \charact_dot_reg[0]_0\ => inst_display_n_1,
      \charact_dot_reg[0]_1\ => inst_display_n_2,
      \charact_dot_reg[0]_2\ => inst_display_n_3,
      \charact_dot_reg[0]_3\ => inst_display_n_4,
      \charact_dot_reg[0]_4\ => inst_display_n_6,
      \charact_dot_reg[0]_5\ => inst_display_n_14,
      \charact_dot_reg[0]_6\ => inst_display_n_19,
      \charact_dot_reg[0]_7\ => inst_fsm_n_7,
      clk_BUFG => clk_BUFG,
      \current_state_reg[0]\ => inst_display_n_13,
      \current_state_reg[0]_0\ => inst_display_n_15,
      \current_state_reg[1]\ => inst_display_n_12,
      \current_state_reg[1]_0\ => inst_display_n_32,
      digit_OBUF(6 downto 0) => digit_OBUF(6 downto 0),
      \disp_dinero[1]_0\(0) => \disp_dinero[1]_1\(1),
      \disp_dinero[5]_1\(0) => \disp_dinero[5]_0\(0),
      dot_OBUF => dot_OBUF,
      dot_reg_0(1 downto 0) => st_OBUF(1 downto 0),
      dot_reg_1 => inst_fsm_n_24,
      elem_OBUF(7 downto 0) => elem_OBUF(7 downto 0),
      lopt => lopt,
      lopt_1 => lopt_1,
      lopt_2 => lopt_2,
      lopt_3 => lopt_3,
      lopt_4 => lopt_4,
      lopt_5 => lopt_5,
      lopt_6 => lopt_6,
      output0 => output0,
      \reg_reg[0]\ => inst_display_n_5,
      \reg_reg[1]\ => inst_display_n_8,
      \reg_reg[1]_0\ => inst_display_n_21,
      \reg_reg[1]_1\ => inst_display_n_22,
      \reg_reg[2]\(2 downto 0) => reg(2 downto 0),
      \reg_reg[2]_0\ => inst_display_n_16,
      reset_IBUF => reset_IBUF
    );
inst_fsm: entity work.fsm
     port map (
      CO(0) => caracter2(3),
      D(2 downto 1) => caracter(6 downto 5),
      D(0) => caracter(3),
      \FSM_onehot_current_refresco_reg[0]\ => inst_fsm_n_26,
      \FSM_onehot_current_refresco_reg[1]\ => inst_fsm_n_19,
      \FSM_onehot_current_refresco_reg[1]_0\ => inst_fsm_n_22,
      \FSM_onehot_current_refresco_reg[1]_1\ => inst_fsm_n_28,
      \FSM_onehot_current_refresco_reg[2]\ => inst_fsm_n_24,
      \FSM_onehot_current_refresco_reg[3]\ => inst_fsm_n_11,
      \FSM_onehot_current_refresco_reg[3]_0\ => inst_fsm_n_30,
      \FSM_onehot_current_refresco_reg[4]\(4) => inst_fsm_n_12,
      \FSM_onehot_current_refresco_reg[4]\(3) => inst_fsm_n_13,
      \FSM_onehot_current_refresco_reg[4]\(2) => inst_fsm_n_14,
      \FSM_onehot_current_refresco_reg[4]\(1) => inst_fsm_n_15,
      \FSM_onehot_current_refresco_reg[4]\(0) => inst_fsm_n_16,
      \FSM_onehot_current_refresco_reg[4]_0\ => inst_fsm_n_20,
      \FSM_onehot_current_refresco_reg[4]_1\ => inst_fsm_n_25,
      Q(1 downto 0) => st_OBUF(1 downto 0),
      button_ok_IBUF => button_ok_IBUF,
      \caracter[0]_i_2\ => inst_display_n_6,
      \caracter[1]_i_2\ => inst_display_n_19,
      \caracter[3]_i_32\(2 downto 0) => reg(2 downto 0),
      \caracter_reg[0]\ => inst_display_n_13,
      \caracter_reg[0]_0\ => inst_display_n_14,
      \caracter_reg[1]\ => inst_display_n_12,
      \caracter_reg[1]_0\ => inst_display_n_15,
      \caracter_reg[2]\ => inst_display_n_1,
      \caracter_reg[2]_0\ => inst_display_n_18,
      \caracter_reg[2]_1\ => inst_display_n_20,
      \caracter_reg[2]_2\ => inst_display_n_22,
      \caracter_reg[3]\ => inst_display_n_4,
      \caracter_reg[3]_0\ => inst_display_n_7,
      \caracter_reg[3]_1\ => inst_display_n_16,
      \caracter_reg[3]_2\ => inst_display_n_32,
      \caracter_reg[3]_3\ => inst_display_n_3,
      \caracter_reg[6]\ => inst_display_n_2,
      \caracter_reg[6]_0\ => inst_display_n_5,
      \caracter_reg[6]_1\ => inst_display_n_17,
      \caracter_reg[6]_2\ => inst_display_n_8,
      \caracter_reg[6]_3\ => inst_display_n_21,
      \charact_dot_reg[0]\ => inst_fsm_n_0,
      \charact_dot_reg[0]_0\ => inst_fsm_n_27,
      clk_BUFG => clk_BUFG,
      \current_state_reg[0]_0\ => inst_fsm_n_6,
      \current_state_reg[0]_1\ => inst_fsm_n_29,
      \current_state_reg[0]_2\ => inst_decodmonedas_n_0,
      \current_state_reg[1]_0\ => inst_fsm_n_1,
      \current_state_reg[1]_1\ => inst_fsm_n_10,
      \current_state_reg[1]_2\ => inst_fsm_n_17,
      \current_state_reg[1]_3\ => inst_fsm_n_21,
      \current_state_reg[1]_4\ => inst_fsm_n_23,
      \current_state_reg[1]_5\ => inst_fsm_n_31,
      \current_state_reg[1]_6\ => inst_fsm_n_32,
      devolver_OBUF => devolver_OBUF,
      \disp_dinero[1]_0\(0) => \disp_dinero[1]_1\(1),
      \disp_dinero[5]_1\(0) => \disp_dinero[5]_0\(0),
      output0 => output0,
      ref_option_IBUF(3 downto 0) => ref_option_IBUF(3 downto 0),
      refresco_OBUF => refresco_OBUF,
      \reg_reg[1]\ => inst_fsm_n_7,
      \total_reg[0]\(0) => \Inst_gestion_dinero/cent\(0),
      \total_reg[7]_i_2\(6 downto 0) => v_mon_OBUF(6 downto 0)
    );
inst_prescaler: entity work.prescaler
     port map (
      clk => clk,
      clk_100Mh_IBUF_BUFG => clk_100Mh_IBUF_BUFG,
      output0 => output0,
      reset_IBUF => reset_IBUF
    );
inst_sincronizador: entity work.sincronizador
     port map (
      clk_BUFG => clk_BUFG,
      monedas_IBUF(4 downto 0) => monedas_IBUF(4 downto 0),
      \sreg_reg[0]\ => inst_sincronizador_n_0,
      \sreg_reg[0]_0\ => inst_sincronizador_n_1,
      \sreg_reg[0]_1\ => inst_sincronizador_n_2,
      \sreg_reg[0]_2\ => inst_sincronizador_n_3,
      \sreg_reg[0]_3\ => inst_sincronizador_n_4
    );
\letra_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => lopt,
      O => letra(0)
    );
\letra_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => lopt_1,
      O => letra(1)
    );
\letra_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => lopt_2,
      O => letra(2)
    );
\letra_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => lopt_3,
      O => letra(3)
    );
\letra_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => lopt_4,
      O => letra(4)
    );
\letra_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => lopt_5,
      O => letra(5)
    );
\letra_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => lopt_6,
      O => letra(6)
    );
\letra_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => letra(7)
    );
\monedas_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => monedas(0),
      O => monedas_IBUF(0)
    );
\monedas_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => monedas(1),
      O => monedas_IBUF(1)
    );
\monedas_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => monedas(2),
      O => monedas_IBUF(2)
    );
\monedas_IBUF[3]_inst\: unisim.vcomponents.IBUF
     port map (
      I => monedas(3),
      O => monedas_IBUF(3)
    );
\monedas_IBUF[4]_inst\: unisim.vcomponents.IBUF
     port map (
      I => monedas(4),
      O => monedas_IBUF(4)
    );
\ref_option_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ref_option(0),
      O => ref_option_IBUF(0)
    );
\ref_option_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ref_option(1),
      O => ref_option_IBUF(1)
    );
\ref_option_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ref_option(2),
      O => ref_option_IBUF(2)
    );
\ref_option_IBUF[3]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ref_option(3),
      O => ref_option_IBUF(3)
    );
refresco_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => refresco_OBUF,
      O => refresco
    );
reset_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => reset,
      O => reset_IBUF
    );
\st_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => st_OBUF(0),
      O => st(0)
    );
\st_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => st_OBUF(1),
      O => st(1)
    );
\v_mon_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => lopt_7,
      O => v_mon(0)
    );
\v_mon_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => lopt_8,
      O => v_mon(1)
    );
\v_mon_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => lopt_9,
      O => v_mon(2)
    );
\v_mon_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => lopt_10,
      O => v_mon(3)
    );
\v_mon_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => lopt_11,
      O => v_mon(4)
    );
\v_mon_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => lopt_12,
      O => v_mon(5)
    );
\v_mon_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => lopt_13,
      O => v_mon(6)
    );
\v_mon_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => v_mon(7)
    );
end STRUCTURE;
