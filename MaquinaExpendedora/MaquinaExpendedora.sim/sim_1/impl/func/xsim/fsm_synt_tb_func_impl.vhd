-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Sun Jan  3 18:38:59 2021
-- Host        : LAPTOP-7BL7BHFF running 64-bit major release  (build 9200)
-- Command     : write_vhdl -mode funcsim -nolib -force -file
--               C:/Users/Inees/Desktop/Trabajo_SED/MaquinaExpendedora_aux/MaquinaExpendedora_aux.sim/sim_1/impl/func/xsim/fsm_synt_tb_func_impl.vhd
-- Design      : fsm
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tcsg324-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gestion_refresco is
  port (
    \disp_refresco[5]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_aux_reg[6][3]_i_2_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_refresco[3]\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \disp_aux_reg[6][3]_i_2_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    Q : out STD_LOGIC_VECTOR ( 0 to 0 );
    \disp[1]_OBUF\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \disp[3]_OBUF\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \disp[2]_OBUF\ : out STD_LOGIC_VECTOR ( 4 downto 0 );
    \disp[4]_OBUF\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \disp[8]_OBUF\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \disp[7]_OBUF\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \disp[6]_OBUF\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \disp[5]_OBUF\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \disp[1][1]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp[1][1]_0\ : in STD_LOGIC;
    \disp[3][3]\ : in STD_LOGIC;
    \disp[2][1]\ : in STD_LOGIC;
    ref_option_IBUF : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \disp[2][3]\ : in STD_LOGIC;
    \disp[2][2]\ : in STD_LOGIC
  );
end gestion_refresco;

architecture STRUCTURE of gestion_refresco is
  signal \disp_aux[1]_0\ : STD_LOGIC;
  signal \disp_aux_reg[2][0]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[2][1]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[2][4]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[3][0]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[3][1]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[3][6]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[4][1]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[4][4]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[4][6]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[5][0]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[6][1]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[6][2]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[6][3]_i_1_n_0\ : STD_LOGIC;
  signal \^disp_aux_reg[6][3]_i_2_1\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \disp_aux_reg[6][6]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[7][3]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[8][0]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[8][1]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[8][2]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[8][3]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[8][6]_i_1_n_0\ : STD_LOGIC;
  signal \disp_refresco[2]\ : STD_LOGIC_VECTOR ( 4 downto 1 );
  signal \^disp_refresco[3]\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \disp_refresco[4]\ : STD_LOGIC_VECTOR ( 6 downto 4 );
  signal \disp_refresco[6]\ : STD_LOGIC_VECTOR ( 6 downto 1 );
  signal \disp_refresco[7]\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \disp_refresco[8]\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \disp[1][1]_INST_0_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \disp[1][2]_INST_0_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \disp[1][6]_INST_0_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \disp[2][1]_INST_0_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \disp[2][2]_INST_0_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \disp[2][3]_INST_0_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \disp[2][4]_INST_0_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \disp[3][3]_INST_0_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \disp[3][4]_INST_0_i_1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \disp[3][6]_INST_0_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \disp[4][0]_INST_0_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \disp[4][3]_INST_0_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \disp[4][4]_INST_0_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \disp[4][6]_INST_0_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \disp[5][1]_INST_0_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \disp[5][2]_INST_0_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \disp[5][3]_INST_0_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \disp[6][1]_INST_0_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \disp[6][2]_INST_0_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \disp[6][3]_INST_0_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \disp[6][6]_INST_0_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \disp[7][2]_INST_0_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \disp[7][3]_INST_0_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \disp[8][0]_INST_0_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \disp[8][1]_INST_0_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \disp[8][2]_INST_0_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \disp[8][3]_INST_0_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \disp[8][6]_INST_0_i_1\ : label is "soft_lutpair27";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[2][0]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[2][0]_i_1\ : label is "soft_lutpair16";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[2][1]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[2][1]_i_1\ : label is "soft_lutpair17";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[2][4]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[2][4]_i_1\ : label is "soft_lutpair26";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[3][0]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[3][0]_i_1\ : label is "soft_lutpair14";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[3][1]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[3][1]_i_1\ : label is "soft_lutpair14";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[3][6]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[3][6]_i_1\ : label is "soft_lutpair16";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[4][1]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[4][1]_i_1\ : label is "soft_lutpair17";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[4][4]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[4][6]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[4][6]_i_1\ : label is "soft_lutpair15";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[5][0]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[5][0]_i_1\ : label is "soft_lutpair12";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[6][1]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[6][1]_i_1\ : label is "soft_lutpair23";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[6][2]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[6][2]_i_1\ : label is "soft_lutpair15";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[6][3]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[6][3]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \disp_aux_reg[6][3]_i_2\ : label is "soft_lutpair11";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[6][6]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[6][6]_i_1\ : label is "soft_lutpair12";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[7][3]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[7][3]_i_1\ : label is "soft_lutpair18";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[8][0]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[8][0]_i_1\ : label is "soft_lutpair23";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[8][1]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[8][1]_i_1\ : label is "soft_lutpair18";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[8][2]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[8][2]_i_1\ : label is "soft_lutpair26";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[8][3]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[8][3]_i_1\ : label is "soft_lutpair13";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[8][6]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[8][6]_i_1\ : label is "soft_lutpair13";
begin
  \disp_aux_reg[6][3]_i_2_1\(0) <= \^disp_aux_reg[6][3]_i_2_1\(0);
  \disp_refresco[3]\(2 downto 0) <= \^disp_refresco[3]\(2 downto 0);
\disp[1][1]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FD5D"
    )
        port map (
      I0 => \disp[1][1]\(0),
      I1 => \disp_refresco[2]\(1),
      I2 => \disp[1][1]\(1),
      I3 => \disp[1][1]_0\,
      O => \disp[1]_OBUF\(1)
    );
\disp[1][2]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3BFB"
    )
        port map (
      I0 => \^disp_aux_reg[6][3]_i_2_1\(0),
      I1 => \disp[1][1]\(0),
      I2 => \disp[1][1]\(1),
      I3 => \disp[1][1]_0\,
      O => \disp[1]_OBUF\(2)
    );
\disp[1][3]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => \disp[1][1]\(1),
      I1 => \disp_refresco[2]\(1),
      I2 => \disp[1][1]\(0),
      O => \disp[1]_OBUF\(3)
    );
\disp[1][6]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EF"
    )
        port map (
      I0 => \^disp_refresco[3]\(2),
      I1 => \disp[1][1]\(1),
      I2 => \disp[1][1]\(0),
      O => \disp[1]_OBUF\(0)
    );
\disp[2][1]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2FEF"
    )
        port map (
      I0 => \disp_refresco[2]\(1),
      I1 => \disp[1][1]\(1),
      I2 => \disp[1][1]\(0),
      I3 => \disp[2][1]\,
      O => \disp[2]_OBUF\(0)
    );
\disp[2][2]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"E200"
    )
        port map (
      I0 => \disp_refresco[4]\(6),
      I1 => \disp[1][1]\(1),
      I2 => \disp[2][2]\,
      I3 => \disp[1][1]\(0),
      O => \disp[2]_OBUF\(1)
    );
\disp[2][3]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"E200"
    )
        port map (
      I0 => \disp_refresco[8]\(1),
      I1 => \disp[1][1]\(1),
      I2 => \disp[2][3]\,
      I3 => \disp[1][1]\(0),
      O => \disp[2]_OBUF\(2)
    );
\disp[2][4]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => \disp[1][1]\(1),
      I1 => \disp_refresco[2]\(4),
      I2 => \disp[1][1]\(0),
      O => \disp[2]_OBUF\(3)
    );
\disp[3][3]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"E020"
    )
        port map (
      I0 => \disp_refresco[7]\(3),
      I1 => \disp[1][1]\(1),
      I2 => \disp[1][1]\(0),
      I3 => \disp[3][3]\,
      O => \disp[3]_OBUF\(0)
    );
\disp[3][4]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"CB"
    )
        port map (
      I0 => \disp_refresco[4]\(4),
      I1 => \disp[1][1]\(0),
      I2 => \disp[1][1]\(1),
      O => \disp[3]_OBUF\(1)
    );
\disp[3][6]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => \disp[1][1]\(1),
      I1 => \^disp_refresco[3]\(2),
      I2 => \disp[1][1]\(0),
      O => \disp[3]_OBUF\(2)
    );
\disp[4][0]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"31"
    )
        port map (
      I0 => \disp[1][1]\(0),
      I1 => \disp[1][1]\(1),
      I2 => \disp_refresco[4]\(6),
      O => \disp[4]_OBUF\(0)
    );
\disp[4][3]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2CEC"
    )
        port map (
      I0 => \disp_refresco[8]\(3),
      I1 => \disp[1][1]\(1),
      I2 => \disp[1][1]\(0),
      I3 => \disp[1][1]_0\,
      O => \disp[4]_OBUF\(1)
    );
\disp[4][4]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \disp_refresco[4]\(4),
      I1 => \disp[1][1]\(1),
      I2 => \disp[1][1]\(0),
      O => \disp[4]_OBUF\(2)
    );
\disp[4][6]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => \disp[1][1]\(1),
      I1 => \disp_refresco[4]\(6),
      I2 => \disp[1][1]\(0),
      O => \disp[2]_OBUF\(4)
    );
\disp[5][1]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => \disp[1][1]\(1),
      I1 => \disp_refresco[8]\(3),
      I2 => \disp[1][1]\(0),
      O => \disp[5]_OBUF\(0)
    );
\disp[5][2]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \disp[1][1]\(1),
      I1 => \disp_refresco[6]\(2),
      I2 => \disp[1][1]\(0),
      O => \disp[5]_OBUF\(1)
    );
\disp[5][3]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4A"
    )
        port map (
      I0 => \disp[1][1]\(1),
      I1 => \disp_refresco[8]\(3),
      I2 => \disp[1][1]\(0),
      O => \disp[5]_OBUF\(2)
    );
\disp[6][1]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => \disp[1][1]\(1),
      I1 => \disp_refresco[6]\(1),
      I2 => \disp[1][1]\(0),
      O => \disp[6]_OBUF\(1)
    );
\disp[6][2]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => \disp[1][1]\(1),
      I1 => \disp_refresco[6]\(2),
      I2 => \disp[1][1]\(0),
      O => \disp[6]_OBUF\(2)
    );
\disp[6][3]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \disp_refresco[6]\(3),
      I1 => \disp[1][1]\(1),
      I2 => \disp[1][1]\(0),
      O => \disp[6]_OBUF\(3)
    );
\disp[6][6]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => \disp[1][1]\(1),
      I1 => \disp_refresco[6]\(6),
      I2 => \disp[1][1]\(0),
      O => \disp[5]_OBUF\(3)
    );
\disp[7][2]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"31"
    )
        port map (
      I0 => \disp[1][1]\(0),
      I1 => \disp[1][1]\(1),
      I2 => \disp_refresco[8]\(0),
      O => \disp[7]_OBUF\(2)
    );
\disp[7][3]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \disp_refresco[7]\(3),
      I1 => \disp[1][1]\(1),
      I2 => \disp[1][1]\(0),
      O => \disp[7]_OBUF\(1)
    );
\disp[8][0]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \disp_refresco[8]\(0),
      I1 => \disp[1][1]\(1),
      I2 => \disp[1][1]\(0),
      O => \disp[6]_OBUF\(0)
    );
\disp[8][1]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"31"
    )
        port map (
      I0 => \disp[1][1]\(0),
      I1 => \disp[1][1]\(1),
      I2 => \disp_refresco[8]\(1),
      O => \disp[8]_OBUF\(0)
    );
\disp[8][2]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \^disp_aux_reg[6][3]_i_2_1\(0),
      I1 => \disp[1][1]\(1),
      I2 => \disp[1][1]\(0),
      O => \disp[8]_OBUF\(1)
    );
\disp[8][3]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \disp_refresco[8]\(3),
      I1 => \disp[1][1]\(1),
      I2 => \disp[1][1]\(0),
      O => \disp[8]_OBUF\(2)
    );
\disp[8][6]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => \disp[1][1]\(1),
      I1 => \disp_refresco[8]\(6),
      I2 => \disp[1][1]\(0),
      O => \disp[7]_OBUF\(0)
    );
\disp_aux_reg[2][0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[2][0]_i_1_n_0\,
      G => \disp_aux[1]_0\,
      GE => '1',
      Q => Q(0)
    );
\disp_aux_reg[2][0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0103"
    )
        port map (
      I0 => ref_option_IBUF(2),
      I1 => ref_option_IBUF(3),
      I2 => ref_option_IBUF(1),
      I3 => ref_option_IBUF(0),
      O => \disp_aux_reg[2][0]_i_1_n_0\
    );
\disp_aux_reg[2][1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[2][1]_i_1_n_0\,
      G => \disp_aux[1]_0\,
      GE => '1',
      Q => \disp_refresco[2]\(1)
    );
\disp_aux_reg[2][1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFE8"
    )
        port map (
      I0 => ref_option_IBUF(0),
      I1 => ref_option_IBUF(3),
      I2 => ref_option_IBUF(2),
      I3 => ref_option_IBUF(1),
      O => \disp_aux_reg[2][1]_i_1_n_0\
    );
\disp_aux_reg[2][4]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[2][4]_i_1_n_0\,
      G => \disp_aux[1]_0\,
      GE => '1',
      Q => \disp_refresco[2]\(4)
    );
\disp_aux_reg[2][4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => ref_option_IBUF(2),
      I1 => ref_option_IBUF(3),
      I2 => ref_option_IBUF(1),
      O => \disp_aux_reg[2][4]_i_1_n_0\
    );
\disp_aux_reg[3][0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[3][0]_i_1_n_0\,
      G => \disp_aux[1]_0\,
      GE => '1',
      Q => \^disp_refresco[3]\(0)
    );
\disp_aux_reg[3][0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0105"
    )
        port map (
      I0 => ref_option_IBUF(2),
      I1 => ref_option_IBUF(3),
      I2 => ref_option_IBUF(1),
      I3 => ref_option_IBUF(0),
      O => \disp_aux_reg[3][0]_i_1_n_0\
    );
\disp_aux_reg[3][1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[3][1]_i_1_n_0\,
      G => \disp_aux[1]_0\,
      GE => '1',
      Q => \^disp_refresco[3]\(1)
    );
\disp_aux_reg[3][1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEFC"
    )
        port map (
      I0 => ref_option_IBUF(1),
      I1 => ref_option_IBUF(0),
      I2 => ref_option_IBUF(3),
      I3 => ref_option_IBUF(2),
      O => \disp_aux_reg[3][1]_i_1_n_0\
    );
\disp_aux_reg[3][6]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[3][6]_i_1_n_0\,
      G => \disp_aux[1]_0\,
      GE => '1',
      Q => \^disp_refresco[3]\(2)
    );
\disp_aux_reg[3][6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0115"
    )
        port map (
      I0 => ref_option_IBUF(1),
      I1 => ref_option_IBUF(2),
      I2 => ref_option_IBUF(3),
      I3 => ref_option_IBUF(0),
      O => \disp_aux_reg[3][6]_i_1_n_0\
    );
\disp_aux_reg[4][1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[4][1]_i_1_n_0\,
      G => \disp_aux[1]_0\,
      GE => '1',
      Q => \disp_aux_reg[6][3]_i_2_0\(0)
    );
\disp_aux_reg[4][1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFF8"
    )
        port map (
      I0 => ref_option_IBUF(0),
      I1 => ref_option_IBUF(1),
      I2 => ref_option_IBUF(3),
      I3 => ref_option_IBUF(2),
      O => \disp_aux_reg[4][1]_i_1_n_0\
    );
\disp_aux_reg[4][4]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[4][4]_i_1_n_0\,
      G => \disp_aux[1]_0\,
      GE => '1',
      Q => \disp_refresco[4]\(4)
    );
\disp_aux_reg[4][4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => ref_option_IBUF(1),
      I1 => ref_option_IBUF(3),
      I2 => ref_option_IBUF(0),
      O => \disp_aux_reg[4][4]_i_1_n_0\
    );
\disp_aux_reg[4][6]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[4][6]_i_1_n_0\,
      G => \disp_aux[1]_0\,
      GE => '1',
      Q => \disp_refresco[4]\(6)
    );
\disp_aux_reg[4][6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0117"
    )
        port map (
      I0 => ref_option_IBUF(2),
      I1 => ref_option_IBUF(0),
      I2 => ref_option_IBUF(3),
      I3 => ref_option_IBUF(1),
      O => \disp_aux_reg[4][6]_i_1_n_0\
    );
\disp_aux_reg[5][0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[5][0]_i_1_n_0\,
      G => \disp_aux[1]_0\,
      GE => '1',
      Q => \disp_refresco[5]\(0)
    );
\disp_aux_reg[5][0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0013"
    )
        port map (
      I0 => ref_option_IBUF(2),
      I1 => ref_option_IBUF(0),
      I2 => ref_option_IBUF(3),
      I3 => ref_option_IBUF(1),
      O => \disp_aux_reg[5][0]_i_1_n_0\
    );
\disp_aux_reg[6][1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[6][1]_i_1_n_0\,
      G => \disp_aux[1]_0\,
      GE => '1',
      Q => \disp_refresco[6]\(1)
    );
\disp_aux_reg[6][1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => ref_option_IBUF(1),
      I1 => ref_option_IBUF(3),
      I2 => ref_option_IBUF(2),
      O => \disp_aux_reg[6][1]_i_1_n_0\
    );
\disp_aux_reg[6][2]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[6][2]_i_1_n_0\,
      G => \disp_aux[1]_0\,
      GE => '1',
      Q => \disp_refresco[6]\(2)
    );
\disp_aux_reg[6][2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0103"
    )
        port map (
      I0 => ref_option_IBUF(2),
      I1 => ref_option_IBUF(3),
      I2 => ref_option_IBUF(0),
      I3 => ref_option_IBUF(1),
      O => \disp_aux_reg[6][2]_i_1_n_0\
    );
\disp_aux_reg[6][3]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[6][3]_i_1_n_0\,
      G => \disp_aux[1]_0\,
      GE => '1',
      Q => \disp_refresco[6]\(3)
    );
\disp_aux_reg[6][3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEC"
    )
        port map (
      I0 => ref_option_IBUF(0),
      I1 => ref_option_IBUF(1),
      I2 => ref_option_IBUF(3),
      I3 => ref_option_IBUF(2),
      O => \disp_aux_reg[6][3]_i_1_n_0\
    );
\disp_aux_reg[6][3]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => ref_option_IBUF(3),
      I1 => ref_option_IBUF(2),
      I2 => ref_option_IBUF(1),
      I3 => ref_option_IBUF(0),
      O => \disp_aux[1]_0\
    );
\disp_aux_reg[6][6]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[6][6]_i_1_n_0\,
      G => \disp_aux[1]_0\,
      GE => '1',
      Q => \disp_refresco[6]\(6)
    );
\disp_aux_reg[6][6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0107"
    )
        port map (
      I0 => ref_option_IBUF(2),
      I1 => ref_option_IBUF(3),
      I2 => ref_option_IBUF(0),
      I3 => ref_option_IBUF(1),
      O => \disp_aux_reg[6][6]_i_1_n_0\
    );
\disp_aux_reg[7][3]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[7][3]_i_1_n_0\,
      G => \disp_aux[1]_0\,
      GE => '1',
      Q => \disp_refresco[7]\(3)
    );
\disp_aux_reg[7][3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEF8"
    )
        port map (
      I0 => ref_option_IBUF(2),
      I1 => ref_option_IBUF(0),
      I2 => ref_option_IBUF(3),
      I3 => ref_option_IBUF(1),
      O => \disp_aux_reg[7][3]_i_1_n_0\
    );
\disp_aux_reg[8][0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[8][0]_i_1_n_0\,
      G => \disp_aux[1]_0\,
      GE => '1',
      Q => \disp_refresco[8]\(0)
    );
\disp_aux_reg[8][0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => ref_option_IBUF(2),
      I1 => ref_option_IBUF(1),
      I2 => ref_option_IBUF(0),
      O => \disp_aux_reg[8][0]_i_1_n_0\
    );
\disp_aux_reg[8][1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[8][1]_i_1_n_0\,
      G => \disp_aux[1]_0\,
      GE => '1',
      Q => \disp_refresco[8]\(1)
    );
\disp_aux_reg[8][1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEFC"
    )
        port map (
      I0 => ref_option_IBUF(0),
      I1 => ref_option_IBUF(1),
      I2 => ref_option_IBUF(3),
      I3 => ref_option_IBUF(2),
      O => \disp_aux_reg[8][1]_i_1_n_0\
    );
\disp_aux_reg[8][2]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[8][2]_i_1_n_0\,
      G => \disp_aux[1]_0\,
      GE => '1',
      Q => \^disp_aux_reg[6][3]_i_2_1\(0)
    );
\disp_aux_reg[8][2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => ref_option_IBUF(2),
      I1 => ref_option_IBUF(3),
      I2 => ref_option_IBUF(0),
      O => \disp_aux_reg[8][2]_i_1_n_0\
    );
\disp_aux_reg[8][3]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[8][3]_i_1_n_0\,
      G => \disp_aux[1]_0\,
      GE => '1',
      Q => \disp_refresco[8]\(3)
    );
\disp_aux_reg[8][3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEE8"
    )
        port map (
      I0 => ref_option_IBUF(1),
      I1 => ref_option_IBUF(3),
      I2 => ref_option_IBUF(0),
      I3 => ref_option_IBUF(2),
      O => \disp_aux_reg[8][3]_i_1_n_0\
    );
\disp_aux_reg[8][6]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[8][6]_i_1_n_0\,
      G => \disp_aux[1]_0\,
      GE => '1',
      Q => \disp_refresco[8]\(6)
    );
\disp_aux_reg[8][6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0013"
    )
        port map (
      I0 => ref_option_IBUF(1),
      I1 => ref_option_IBUF(0),
      I2 => ref_option_IBUF(3),
      I3 => ref_option_IBUF(2),
      O => \disp_aux_reg[8][6]_i_1_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity int_to_string is
  port (
    \disp[3]_OBUF\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \disp[3][3]_INST_0_i_7_0\ : out STD_LOGIC;
    \total_string_reg[5]\ : out STD_LOGIC;
    \total_string_reg[1]\ : out STD_LOGIC;
    \total_string_reg[1]_0\ : out STD_LOGIC;
    \total_string_reg[7]\ : out STD_LOGIC;
    \disp[2]_OBUF\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \disp[5]_OBUF\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 7 downto 0 );
    \disp_refresco[3]\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \disp[5][0]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp[2][0]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_refresco[5]\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end int_to_string;

architecture STRUCTURE of int_to_string is
  signal cent : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \disp[2][3]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \disp[3][0]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \disp[3][0]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \disp[3][0]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \disp[3][0]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \disp[3][0]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \disp[3][0]_INST_0_i_7_n_0\ : STD_LOGIC;
  signal \disp[3][1]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \disp[3][1]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \disp[3][1]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \disp[3][1]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \disp[3][1]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \disp[3][1]_INST_0_i_7_n_0\ : STD_LOGIC;
  signal \disp[3][1]_INST_0_i_8_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \^disp[3][3]_inst_0_i_7_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_7_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_8_n_0\ : STD_LOGIC;
  signal \euros1_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \euros1_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \euros1_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \euros1_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \euros1_carry__0_n_4\ : STD_LOGIC;
  signal \euros1_carry__0_n_5\ : STD_LOGIC;
  signal \euros1_carry__0_n_6\ : STD_LOGIC;
  signal \euros1_carry__0_n_7\ : STD_LOGIC;
  signal euros1_carry_i_1_n_0 : STD_LOGIC;
  signal euros1_carry_i_2_n_0 : STD_LOGIC;
  signal euros1_carry_i_3_n_0 : STD_LOGIC;
  signal euros1_carry_i_4_n_0 : STD_LOGIC;
  signal euros1_carry_n_0 : STD_LOGIC;
  signal euros1_carry_n_4 : STD_LOGIC;
  signal euros1_carry_n_5 : STD_LOGIC;
  signal \i__carry__0_i_3_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_4_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_5_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_7_n_0\ : STD_LOGIC;
  signal \i__carry_i_11_n_0\ : STD_LOGIC;
  signal \i__carry_i_3_n_0\ : STD_LOGIC;
  signal \i__carry_i_4_n_0\ : STD_LOGIC;
  signal \i__carry_i_5_n_0\ : STD_LOGIC;
  signal \i__carry_i_6_n_0\ : STD_LOGIC;
  signal \i__carry_i_7_n_0\ : STD_LOGIC;
  signal \i__carry_i_8_n_0\ : STD_LOGIC;
  signal p_1_in : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \string_cent_decenas[1]5\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \string_cent_decenas[1]5_inferred__0/i__carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i__carry__0_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i__carry__0_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i__carry__0_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i__carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i__carry_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i__carry_n_5\ : STD_LOGIC;
  signal \^total_string_reg[1]\ : STD_LOGIC;
  signal \^total_string_reg[1]_0\ : STD_LOGIC;
  signal \^total_string_reg[5]\ : STD_LOGIC;
  signal \^total_string_reg[7]\ : STD_LOGIC;
  signal NLW_euros1_carry_CO_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_euros1_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i__carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute OPT_MODIFIED : string;
  attribute OPT_MODIFIED of \disp[2][1]_INST_0_i_2\ : label is "RETARGET";
  attribute OPT_MODIFIED of \disp[2][2]_INST_0_i_2\ : label is "RETARGET";
  attribute OPT_MODIFIED of \disp[2][3]_INST_0_i_2\ : label is "RETARGET";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \disp[2][3]_INST_0_i_4\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \disp[3][0]_INST_0_i_7\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \disp[3][1]_INST_0_i_3\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \disp[3][1]_INST_0_i_4\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \disp[3][1]_INST_0_i_5\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \disp[3][2]_INST_0_i_3\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \disp[3][2]_INST_0_i_4\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \disp[3][3]_INST_0_i_9\ : label is "soft_lutpair3";
  attribute OPT_MODIFIED of euros1_carry : label is "SWEEP";
  attribute OPT_MODIFIED of \euros1_carry__0\ : label is "SWEEP";
  attribute OPT_MODIFIED of \i__carry__0_i_6\ : label is "RETARGET";
  attribute OPT_MODIFIED of \i__carry_i_10\ : label is "RETARGET";
  attribute OPT_MODIFIED of \i__carry_i_9\ : label is "RETARGET";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_inferred__0/i__carry\ : label is "SWEEP";
  attribute OPT_MODIFIED of \string_cent_decenas[1]5_inferred__0/i__carry__0\ : label is "SWEEP";
begin
  \disp[3][3]_INST_0_i_7_0\ <= \^disp[3][3]_inst_0_i_7_0\;
  \total_string_reg[1]\ <= \^total_string_reg[1]\;
  \total_string_reg[1]_0\ <= \^total_string_reg[1]_0\;
  \total_string_reg[5]\ <= \^total_string_reg[5]\;
  \total_string_reg[7]\ <= \^total_string_reg[7]\;
\disp[2][0]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B800B800FFFF00FF"
    )
        port map (
      I0 => Q(0),
      I1 => \^total_string_reg[7]\,
      I2 => p_1_in(0),
      I3 => \disp[5][0]\(0),
      I4 => \disp[2][0]\(0),
      I5 => \disp[5][0]\(1),
      O => \disp[2]_OBUF\(0)
    );
\disp[2][1]_INST_0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9BD96D66D96D66B6"
    )
        port map (
      I0 => cent(1),
      I1 => \disp[2][3]_INST_0_i_3_n_0\,
      I2 => cent(4),
      I3 => cent(6),
      I4 => cent(3),
      I5 => cent(2),
      O => \^total_string_reg[1]\
    );
\disp[2][2]_INST_0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B40BD02D0BD02D42"
    )
        port map (
      I0 => cent(1),
      I1 => \disp[2][3]_INST_0_i_3_n_0\,
      I2 => cent(4),
      I3 => cent(6),
      I4 => cent(3),
      I5 => cent(2),
      O => \^total_string_reg[1]_0\
    );
\disp[2][3]_INST_0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2041041818208204"
    )
        port map (
      I0 => \disp[2][3]_INST_0_i_3_n_0\,
      I1 => cent(4),
      I2 => cent(6),
      I3 => cent(3),
      I4 => cent(2),
      I5 => cent(1),
      O => \^total_string_reg[5]\
    );
\disp[2][3]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4477774744747747"
    )
        port map (
      I0 => Q(5),
      I1 => \^total_string_reg[7]\,
      I2 => \euros1_carry__0_n_4\,
      I3 => \i__carry__0_i_7_n_0\,
      I4 => \euros1_carry__0_n_6\,
      I5 => \euros1_carry__0_n_5\,
      O => \disp[2][3]_INST_0_i_3_n_0\
    );
\disp[2][3]_INST_0_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(1),
      I1 => \^total_string_reg[7]\,
      I2 => p_1_in(1),
      O => cent(1)
    );
\disp[3][0]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AFCF"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_2_n_0\,
      I1 => \disp_refresco[3]\(0),
      I2 => \disp[5][0]\(0),
      I3 => \disp[5][0]\(1),
      O => \disp[3]_OBUF\(0)
    );
\disp[3][0]_INST_0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E2BBCF2C22B8CB0C"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_3_n_0\,
      I1 => \disp[3][1]_INST_0_i_4_n_0\,
      I2 => \disp[3][1]_INST_0_i_3_n_0\,
      I3 => \disp[3][2]_INST_0_i_2_n_0\,
      I4 => \disp[3][0]_INST_0_i_4_n_0\,
      I5 => \disp[3][0]_INST_0_i_5_n_0\,
      O => \disp[3][0]_INST_0_i_2_n_0\
    );
\disp[3][0]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5FFA055FAAA0FAA9"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_3_n_0\,
      I1 => \disp[3][0]_INST_0_i_6_n_0\,
      I2 => \disp[3][3]_INST_0_i_4_n_0\,
      I3 => \disp[3][2]_INST_0_i_4_n_0\,
      I4 => \disp[3][3]_INST_0_i_3_n_0\,
      I5 => \disp[3][3]_INST_0_i_7_n_0\,
      O => \disp[3][0]_INST_0_i_3_n_0\
    );
\disp[3][0]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F67E5B5BDB6DA5DA"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_4_n_0\,
      I1 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_6\,
      I2 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_7\,
      I3 => \disp[3][0]_INST_0_i_7_n_0\,
      I4 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_5\,
      I5 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_0\,
      O => \disp[3][0]_INST_0_i_4_n_0\
    );
\disp[3][0]_INST_0_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96999969"
    )
        port map (
      I0 => \string_cent_decenas[1]5\(1),
      I1 => \^total_string_reg[1]\,
      I2 => \string_cent_decenas[1]5\(0),
      I3 => cent(0),
      I4 => \disp[3][1]_INST_0_i_7_n_0\,
      O => \disp[3][0]_INST_0_i_5_n_0\
    );
\disp[3][0]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000600660060000"
    )
        port map (
      I0 => \disp[3][3]_INST_0_i_8_n_0\,
      I1 => \^total_string_reg[1]_0\,
      I2 => \string_cent_decenas[1]5\(0),
      I3 => cent(0),
      I4 => \string_cent_decenas[1]5\(1),
      I5 => \^total_string_reg[1]\,
      O => \disp[3][0]_INST_0_i_6_n_0\
    );
\disp[3][0]_INST_0_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i__carry_n_4\,
      I1 => \string_cent_decenas[1]5_inferred__0/i__carry_n_5\,
      O => \disp[3][0]_INST_0_i_7_n_0\
    );
\disp[3][1]_INST_0_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"1010FF0F"
    )
        port map (
      I0 => \disp[3][1]_INST_0_i_2_n_0\,
      I1 => \^disp[3][3]_inst_0_i_7_0\,
      I2 => \disp[5][0]\(0),
      I3 => \disp_refresco[3]\(1),
      I4 => \disp[5][0]\(1),
      O => \disp[3]_OBUF\(1)
    );
\disp[3][1]_INST_0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3FD4403F403FFD4F"
    )
        port map (
      I0 => \disp[3][1]_INST_0_i_3_n_0\,
      I1 => \disp[3][1]_INST_0_i_4_n_0\,
      I2 => \disp[3][3]_INST_0_i_7_n_0\,
      I3 => \disp[3][3]_INST_0_i_3_n_0\,
      I4 => \disp[3][2]_INST_0_i_4_n_0\,
      I5 => \disp[3][3]_INST_0_i_4_n_0\,
      O => \disp[3][1]_INST_0_i_2_n_0\
    );
\disp[3][1]_INST_0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A9"
    )
        port map (
      I0 => \disp[3][1]_INST_0_i_5_n_0\,
      I1 => \disp[3][1]_INST_0_i_6_n_0\,
      I2 => \disp[3][1]_INST_0_i_7_n_0\,
      O => \disp[3][1]_INST_0_i_3_n_0\
    );
\disp[3][1]_INST_0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6366"
    )
        port map (
      I0 => \disp[3][1]_INST_0_i_7_n_0\,
      I1 => \disp[3][2]_INST_0_i_3_n_0\,
      I2 => \disp[3][1]_INST_0_i_5_n_0\,
      I3 => \disp[3][1]_INST_0_i_6_n_0\,
      O => \disp[3][1]_INST_0_i_4_n_0\
    );
\disp[3][1]_INST_0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \disp[3][1]_INST_0_i_8_n_0\,
      I1 => \^total_string_reg[1]_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i__carry_n_5\,
      I3 => \disp[3][2]_INST_0_i_5_n_0\,
      O => \disp[3][1]_INST_0_i_5_n_0\
    );
\disp[3][1]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"54040151A80802A2"
    )
        port map (
      I0 => \string_cent_decenas[1]5\(1),
      I1 => p_1_in(0),
      I2 => \^total_string_reg[7]\,
      I3 => Q(0),
      I4 => \string_cent_decenas[1]5\(0),
      I5 => \^total_string_reg[1]\,
      O => \disp[3][1]_INST_0_i_6_n_0\
    );
\disp[3][1]_INST_0_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFEFFFEEEF"
    )
        port map (
      I0 => \disp[3][3]_INST_0_i_3_n_0\,
      I1 => \disp[3][3]_INST_0_i_4_n_0\,
      I2 => \^total_string_reg[5]\,
      I3 => \disp[3][3]_INST_0_i_5_n_0\,
      I4 => \disp[3][3]_INST_0_i_6_n_0\,
      I5 => \disp[3][3]_INST_0_i_7_n_0\,
      O => \disp[3][1]_INST_0_i_7_n_0\
    );
\disp[3][1]_INST_0_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000045404540FFFF"
    )
        port map (
      I0 => \string_cent_decenas[1]5\(0),
      I1 => Q(0),
      I2 => \^total_string_reg[7]\,
      I3 => p_1_in(0),
      I4 => \string_cent_decenas[1]5\(1),
      I5 => \^total_string_reg[1]\,
      O => \disp[3][1]_INST_0_i_8_n_0\
    );
\disp[3][2]_INST_0_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"2FF020F0"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_2_n_0\,
      I1 => \^disp[3][3]_inst_0_i_7_0\,
      I2 => \disp[5][0]\(1),
      I3 => \disp[5][0]\(0),
      I4 => \disp_refresco[3]\(2),
      O => \disp[3]_OBUF\(2)
    );
\disp[3][2]_INST_0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8C383830"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_3_n_0\,
      I1 => \disp[3][3]_INST_0_i_7_n_0\,
      I2 => \disp[3][3]_INST_0_i_3_n_0\,
      I3 => \disp[3][2]_INST_0_i_4_n_0\,
      I4 => \disp[3][3]_INST_0_i_4_n_0\,
      O => \disp[3][2]_INST_0_i_2_n_0\
    );
\disp[3][2]_INST_0_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96969669"
    )
        port map (
      I0 => \disp[3][3]_INST_0_i_5_n_0\,
      I1 => \^total_string_reg[5]\,
      I2 => \string_cent_decenas[1]5_inferred__0/i__carry_n_4\,
      I3 => \disp[3][2]_INST_0_i_5_n_0\,
      I4 => \string_cent_decenas[1]5_inferred__0/i__carry_n_5\,
      O => \disp[3][2]_INST_0_i_3_n_0\
    );
\disp[3][2]_INST_0_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00E1E1FF"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i__carry_n_5\,
      I1 => \disp[3][2]_INST_0_i_5_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i__carry_n_4\,
      I3 => \disp[3][3]_INST_0_i_5_n_0\,
      I4 => \^total_string_reg[5]\,
      O => \disp[3][2]_INST_0_i_4_n_0\
    );
\disp[3][2]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5557FFFF00000000"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_7\,
      I2 => \string_cent_decenas[1]5_inferred__0/i__carry_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i__carry_n_5\,
      I4 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_5\,
      I5 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_0\,
      O => \disp[3][2]_INST_0_i_5_n_0\
    );
\disp[3][3]_INST_0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAA08880008"
    )
        port map (
      I0 => \disp[3][3]_INST_0_i_3_n_0\,
      I1 => \disp[3][3]_INST_0_i_4_n_0\,
      I2 => \^total_string_reg[5]\,
      I3 => \disp[3][3]_INST_0_i_5_n_0\,
      I4 => \disp[3][3]_INST_0_i_6_n_0\,
      I5 => \disp[3][3]_INST_0_i_7_n_0\,
      O => \^disp[3][3]_inst_0_i_7_0\
    );
\disp[3][3]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"555700005557AAA8"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_7\,
      I2 => \string_cent_decenas[1]5_inferred__0/i__carry_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i__carry_n_5\,
      I4 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_5\,
      I5 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_0\,
      O => \disp[3][3]_INST_0_i_3_n_0\
    );
\disp[3][3]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCC4CCCCCCC3CCC3"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_7\,
      I2 => \string_cent_decenas[1]5_inferred__0/i__carry_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i__carry_n_5\,
      I4 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_5\,
      I5 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_0\,
      O => \disp[3][3]_INST_0_i_4_n_0\
    );
\disp[3][3]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"888E888E8EEE888E"
    )
        port map (
      I0 => \^total_string_reg[1]_0\,
      I1 => \disp[3][3]_INST_0_i_8_n_0\,
      I2 => \^total_string_reg[1]\,
      I3 => \string_cent_decenas[1]5\(1),
      I4 => cent(0),
      I5 => \string_cent_decenas[1]5\(0),
      O => \disp[3][3]_INST_0_i_5_n_0\
    );
\disp[3][3]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F058F0F0F00FF00F"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_7\,
      I2 => \string_cent_decenas[1]5_inferred__0/i__carry_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i__carry_n_5\,
      I4 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_5\,
      I5 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_0\,
      O => \disp[3][3]_INST_0_i_6_n_0\
    );
\disp[3][3]_INST_0_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0002AAAA55565556"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_7\,
      I2 => \string_cent_decenas[1]5_inferred__0/i__carry_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i__carry_n_5\,
      I4 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_5\,
      I5 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_0\,
      O => \disp[3][3]_INST_0_i_7_n_0\
    );
\disp[3][3]_INST_0_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"D2D2D2DA5A5A5A5A"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_0\,
      I1 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_5\,
      I2 => \string_cent_decenas[1]5_inferred__0/i__carry_n_5\,
      I3 => \string_cent_decenas[1]5_inferred__0/i__carry_n_4\,
      I4 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_7\,
      I5 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_6\,
      O => \disp[3][3]_INST_0_i_8_n_0\
    );
\disp[3][3]_INST_0_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(0),
      I1 => \^total_string_reg[7]\,
      I2 => p_1_in(0),
      O => cent(0)
    );
\disp[5][0]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0606FF00FFFF0000"
    )
        port map (
      I0 => Q(0),
      I1 => p_1_in(0),
      I2 => \^total_string_reg[7]\,
      I3 => \disp_refresco[5]\(0),
      I4 => \disp[5][0]\(1),
      I5 => \disp[5][0]\(0),
      O => \disp[5]_OBUF\(0)
    );
\disp[5][4]_INST_0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1515151515151555"
    )
        port map (
      I0 => Q(7),
      I1 => Q(5),
      I2 => Q(6),
      I3 => Q(4),
      I4 => Q(2),
      I5 => Q(3),
      O => \^total_string_reg[7]\
    );
euros1_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => euros1_carry_n_0,
      CO(2 downto 0) => NLW_euros1_carry_CO_UNCONNECTED(2 downto 0),
      CYINIT => '1',
      DI(3 downto 0) => Q(3 downto 0),
      O(3) => euros1_carry_n_4,
      O(2) => euros1_carry_n_5,
      O(1 downto 0) => p_1_in(1 downto 0),
      S(3) => euros1_carry_i_1_n_0,
      S(2) => euros1_carry_i_2_n_0,
      S(1) => euros1_carry_i_3_n_0,
      S(0) => euros1_carry_i_4_n_0
    );
\euros1_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => euros1_carry_n_0,
      CO(3 downto 0) => \NLW_euros1_carry__0_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => Q(6 downto 4),
      O(3) => \euros1_carry__0_n_4\,
      O(2) => \euros1_carry__0_n_5\,
      O(1) => \euros1_carry__0_n_6\,
      O(0) => \euros1_carry__0_n_7\,
      S(3) => \euros1_carry__0_i_1_n_0\,
      S(2) => \euros1_carry__0_i_2_n_0\,
      S(1) => \euros1_carry__0_i_3_n_0\,
      S(0) => \euros1_carry__0_i_4_n_0\
    );
\euros1_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => Q(7),
      O => \euros1_carry__0_i_1_n_0\
    );
\euros1_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(6),
      I1 => Q(7),
      O => \euros1_carry__0_i_2_n_0\
    );
\euros1_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(5),
      I1 => Q(7),
      O => \euros1_carry__0_i_3_n_0\
    );
\euros1_carry__0_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => Q(4),
      O => \euros1_carry__0_i_4_n_0\
    );
euros1_carry_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => Q(3),
      O => euros1_carry_i_1_n_0
    );
euros1_carry_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(2),
      I1 => Q(7),
      O => euros1_carry_i_2_n_0
    );
euros1_carry_i_3: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => Q(1),
      O => euros1_carry_i_3_n_0
    );
euros1_carry_i_4: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => Q(0),
      O => euros1_carry_i_4_n_0
    );
\i__carry__0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BB88BB88BB88B88B"
    )
        port map (
      I0 => Q(4),
      I1 => \^total_string_reg[7]\,
      I2 => \i__carry_i_11_n_0\,
      I3 => \euros1_carry__0_n_7\,
      I4 => euros1_carry_n_5,
      I5 => euros1_carry_n_4,
      O => cent(4)
    );
\i__carry__0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4747747747474747"
    )
        port map (
      I0 => Q(6),
      I1 => \^total_string_reg[7]\,
      I2 => \euros1_carry__0_n_5\,
      I3 => \euros1_carry__0_n_4\,
      I4 => \i__carry__0_i_7_n_0\,
      I5 => \euros1_carry__0_n_6\,
      O => \i__carry__0_i_3_n_0\
    );
\i__carry__0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"555555550FF30CF3"
    )
        port map (
      I0 => Q(5),
      I1 => \euros1_carry__0_n_4\,
      I2 => \i__carry__0_i_7_n_0\,
      I3 => \euros1_carry__0_n_6\,
      I4 => \euros1_carry__0_n_5\,
      I5 => \^total_string_reg[7]\,
      O => \i__carry__0_i_4_n_0\
    );
\i__carry__0_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(4),
      O => \i__carry__0_i_5_n_0\
    );
\i__carry__0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B88B88B8B8B8B8"
    )
        port map (
      I0 => Q(6),
      I1 => \^total_string_reg[7]\,
      I2 => \euros1_carry__0_n_5\,
      I3 => \euros1_carry__0_n_4\,
      I4 => \i__carry__0_i_7_n_0\,
      I5 => \euros1_carry__0_n_6\,
      O => cent(6)
    );
\i__carry__0_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \euros1_carry__0_n_7\,
      I1 => euros1_carry_n_5,
      I2 => euros1_carry_n_4,
      O => \i__carry__0_i_7_n_0\
    );
\i__carry_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AAC3"
    )
        port map (
      I0 => Q(2),
      I1 => euros1_carry_n_5,
      I2 => \i__carry_i_11_n_0\,
      I3 => \^total_string_reg[7]\,
      O => cent(2)
    );
\i__carry_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001555555555555"
    )
        port map (
      I0 => \euros1_carry__0_n_4\,
      I1 => \euros1_carry__0_n_7\,
      I2 => euros1_carry_n_5,
      I3 => euros1_carry_n_4,
      I4 => \euros1_carry__0_n_6\,
      I5 => \euros1_carry__0_n_5\,
      O => \i__carry_i_11_n_0\
    );
\i__carry_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(1),
      I1 => \^total_string_reg[7]\,
      I2 => p_1_in(1),
      O => \i__carry_i_3_n_0\
    );
\i__carry_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(0),
      I1 => \^total_string_reg[7]\,
      I2 => p_1_in(0),
      O => \i__carry_i_4_n_0\
    );
\i__carry_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"5555333C"
    )
        port map (
      I0 => Q(3),
      I1 => euros1_carry_n_4,
      I2 => \i__carry_i_11_n_0\,
      I3 => euros1_carry_n_5,
      I4 => \^total_string_reg[7]\,
      O => \i__carry_i_5_n_0\
    );
\i__carry_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"553C"
    )
        port map (
      I0 => Q(2),
      I1 => euros1_carry_n_5,
      I2 => \i__carry_i_11_n_0\,
      I3 => \^total_string_reg[7]\,
      O => \i__carry_i_6_n_0\
    );
\i__carry_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1D"
    )
        port map (
      I0 => p_1_in(1),
      I1 => \^total_string_reg[7]\,
      I2 => Q(1),
      O => \i__carry_i_7_n_0\
    );
\i__carry_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1D"
    )
        port map (
      I0 => p_1_in(0),
      I1 => \^total_string_reg[7]\,
      I2 => Q(0),
      O => \i__carry_i_8_n_0\
    );
\i__carry_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCC3"
    )
        port map (
      I0 => Q(3),
      I1 => euros1_carry_n_4,
      I2 => \i__carry_i_11_n_0\,
      I3 => euros1_carry_n_5,
      I4 => \^total_string_reg[7]\,
      O => cent(3)
    );
\string_cent_decenas[1]5_inferred__0/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5_inferred__0/i__carry_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i__carry_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '1',
      DI(3 downto 2) => cent(3 downto 2),
      DI(1) => \i__carry_i_3_n_0\,
      DI(0) => \i__carry_i_4_n_0\,
      O(3) => \string_cent_decenas[1]5_inferred__0/i__carry_n_4\,
      O(2) => \string_cent_decenas[1]5_inferred__0/i__carry_n_5\,
      O(1 downto 0) => \string_cent_decenas[1]5\(1 downto 0),
      S(3) => \i__carry_i_5_n_0\,
      S(2) => \i__carry_i_6_n_0\,
      S(1) => \i__carry_i_7_n_0\,
      S(0) => \i__carry_i_8_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i__carry_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_0\,
      CO(2 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i__carry__0_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => cent(6),
      DI(1) => '1',
      DI(0) => cent(4),
      O(3) => \NLW_string_cent_decenas[1]5_inferred__0/i__carry__0_O_UNCONNECTED\(3),
      O(2) => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_7\,
      S(3) => '1',
      S(2) => \i__carry__0_i_3_n_0\,
      S(1) => \i__carry__0_i_4_n_0\,
      S(0) => \i__carry__0_i_5_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gestion_dinero is
  port (
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 );
    st_dinero_OBUF : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp[3]_OBUF\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \disp[3][3]_INST_0_i_7\ : out STD_LOGIC;
    \total_string_reg[5]_0\ : out STD_LOGIC;
    \total_string_reg[1]_0\ : out STD_LOGIC;
    \total_string_reg[1]_1\ : out STD_LOGIC;
    \total_string_reg[7]_0\ : out STD_LOGIC;
    \disp[2]_OBUF\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \disp[5]_OBUF\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp[4]_OBUF\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    clr_dinero_OBUF : out STD_LOGIC;
    \tot_reg[7]_0\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \tot_aux_reg[7]_0\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    button_ok_IBUF : in STD_LOGIC;
    \disp_refresco[3]\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \tot_aux_reg[0]_0\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp[2][0]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_refresco[8]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_refresco[5]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_refresco[4]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \current_state_reg[0]\ : in STD_LOGIC;
    \current_state_reg[0]_0\ : in STD_LOGIC;
    clr_dinero_r : in STD_LOGIC;
    v_mon_OBUF : in STD_LOGIC_VECTOR ( 6 downto 0 );
    clk_IBUF_BUFG : in STD_LOGIC;
    AS : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end gestion_dinero;

architecture STRUCTURE of gestion_dinero is
  signal \FSM_onehot_current_state_reg_n_0_[1]\ : STD_LOGIC;
  signal \FSM_onehot_next_state_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_next_state_reg[1]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_next_state_reg[2]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_next_state_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_next_state_reg[3]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_onehot_next_state_reg[3]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_onehot_next_state_reg_n_0_[0]\ : STD_LOGIC;
  signal \FSM_onehot_next_state_reg_n_0_[1]\ : STD_LOGIC;
  signal \FSM_onehot_next_state_reg_n_0_[2]\ : STD_LOGIC;
  signal \FSM_onehot_next_state_reg_n_0_[3]\ : STD_LOGIC;
  signal \^q\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal aux : STD_LOGIC;
  signal \^clr_dinero_obuf\ : STD_LOGIC;
  signal \next_state__0\ : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \total0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \total0_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \total0_carry__0_i_3_n_0\ : STD_LOGIC;
  signal total0_carry_i_1_n_0 : STD_LOGIC;
  signal total0_carry_i_2_n_0 : STD_LOGIC;
  signal total0_carry_i_3_n_0 : STD_LOGIC;
  signal total0_carry_i_4_n_0 : STD_LOGIC;
  signal total0_carry_n_0 : STD_LOGIC;
  signal total0_carry_n_7 : STD_LOGIC;
  signal \total[7]_i_1_n_0\ : STD_LOGIC;
  signal \total[7]_i_2_n_0\ : STD_LOGIC;
  signal total_out : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal total_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \total_string[0]_i_1_n_0\ : STD_LOGIC;
  signal \total_string[1]_i_1_n_0\ : STD_LOGIC;
  signal \total_string[2]_i_1_n_0\ : STD_LOGIC;
  signal \total_string[3]_i_1_n_0\ : STD_LOGIC;
  signal \total_string[3]_i_3_n_0\ : STD_LOGIC;
  signal \total_string[3]_i_4_n_0\ : STD_LOGIC;
  signal \total_string[3]_i_5_n_0\ : STD_LOGIC;
  signal \total_string[3]_i_6_n_0\ : STD_LOGIC;
  signal \total_string[4]_i_1_n_0\ : STD_LOGIC;
  signal \total_string[5]_i_1_n_0\ : STD_LOGIC;
  signal \total_string[6]_i_1_n_0\ : STD_LOGIC;
  signal \total_string[7]_i_1_n_0\ : STD_LOGIC;
  signal \total_string[7]_i_3_n_0\ : STD_LOGIC;
  signal \total_string[7]_i_4_n_0\ : STD_LOGIC;
  signal \total_string[7]_i_5_n_0\ : STD_LOGIC;
  signal \total_string_reg[3]_i_2_n_0\ : STD_LOGIC;
  signal \total_string_reg[3]_i_2_n_4\ : STD_LOGIC;
  signal \total_string_reg[3]_i_2_n_5\ : STD_LOGIC;
  signal \total_string_reg[3]_i_2_n_6\ : STD_LOGIC;
  signal \^total_string_reg[7]_0\ : STD_LOGIC;
  signal \total_string_reg[7]_i_2_n_4\ : STD_LOGIC;
  signal \total_string_reg[7]_i_2_n_5\ : STD_LOGIC;
  signal \total_string_reg[7]_i_2_n_6\ : STD_LOGIC;
  signal \total_string_reg[7]_i_2_n_7\ : STD_LOGIC;
  signal NLW_total0_carry_CO_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_total0_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_total_string_reg[3]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_total_string_reg[3]_i_2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_total_string_reg[7]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[0]\ : label is "s2:1000,s1:0010,s0:0001,s3:0100";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[1]\ : label is "s2:1000,s1:0010,s0:0001,s3:0100";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[2]\ : label is "s2:1000,s1:0010,s0:0001,s3:0100";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[3]\ : label is "s2:1000,s1:0010,s0:0001,s3:0100";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \FSM_onehot_next_state_reg[0]\ : label is "LD";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_onehot_next_state_reg[0]_i_1\ : label is "soft_lutpair4";
  attribute XILINX_LEGACY_PRIM of \FSM_onehot_next_state_reg[1]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \FSM_onehot_next_state_reg[2]\ : label is "LD";
  attribute SOFT_HLUTNM of \FSM_onehot_next_state_reg[2]_i_1\ : label is "soft_lutpair4";
  attribute XILINX_LEGACY_PRIM of \FSM_onehot_next_state_reg[3]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp[4][1]_INST_0_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \disp[5][4]_INST_0_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of refresco_OBUF_inst_i_1 : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \st_dinero_OBUF[1]_inst_i_1\ : label is "soft_lutpair6";
  attribute OPT_MODIFIED : string;
  attribute OPT_MODIFIED of total0_carry : label is "SWEEP";
  attribute OPT_MODIFIED of \total0_carry__0\ : label is "SWEEP";
  attribute OPT_MODIFIED of \total_string_reg[3]_i_2\ : label is "SWEEP";
  attribute OPT_MODIFIED of \total_string_reg[7]_i_2\ : label is "SWEEP";
begin
  Q(1 downto 0) <= \^q\(1 downto 0);
  clr_dinero_OBUF <= \^clr_dinero_obuf\;
  \total_string_reg[7]_0\ <= \^total_string_reg[7]_0\;
\FSM_onehot_current_state_reg[0]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \FSM_onehot_next_state_reg_n_0_[0]\,
      PRE => AS(0),
      Q => aux
    );
\FSM_onehot_current_state_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => AS(0),
      D => \FSM_onehot_next_state_reg_n_0_[1]\,
      Q => \FSM_onehot_current_state_reg_n_0_[1]\
    );
\FSM_onehot_current_state_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => AS(0),
      D => \FSM_onehot_next_state_reg_n_0_[2]\,
      Q => \^q\(0)
    );
\FSM_onehot_current_state_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => AS(0),
      D => \FSM_onehot_next_state_reg_n_0_[3]\,
      Q => \^q\(1)
    );
\FSM_onehot_next_state_reg[0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '1'
    )
        port map (
      CLR => '0',
      D => \FSM_onehot_next_state_reg[0]_i_1_n_0\,
      G => \next_state__0\,
      GE => '1',
      Q => \FSM_onehot_next_state_reg_n_0_[0]\
    );
\FSM_onehot_next_state_reg[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"11331130"
    )
        port map (
      I0 => button_ok_IBUF,
      I1 => aux,
      I2 => \^q\(0),
      I3 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I4 => \^q\(1),
      O => \FSM_onehot_next_state_reg[0]_i_1_n_0\
    );
\FSM_onehot_next_state_reg[1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \FSM_onehot_next_state_reg[1]_i_1_n_0\,
      G => \next_state__0\,
      GE => '1',
      Q => \FSM_onehot_next_state_reg_n_0_[1]\
    );
\FSM_onehot_next_state_reg[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000100000000"
    )
        port map (
      I0 => \FSM_onehot_next_state_reg[3]_i_3_n_0\,
      I1 => total_reg(3),
      I2 => total_reg(4),
      I3 => total_reg(1),
      I4 => total_reg(0),
      I5 => aux,
      O => \FSM_onehot_next_state_reg[1]_i_1_n_0\
    );
\FSM_onehot_next_state_reg[2]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \FSM_onehot_next_state_reg[2]_i_1_n_0\,
      G => \next_state__0\,
      GE => '1',
      Q => \FSM_onehot_next_state_reg_n_0_[2]\
    );
\FSM_onehot_next_state_reg[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => button_ok_IBUF,
      I1 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I2 => aux,
      O => \FSM_onehot_next_state_reg[2]_i_1_n_0\
    );
\FSM_onehot_next_state_reg[3]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \FSM_onehot_next_state_reg[3]_i_1_n_0\,
      G => \next_state__0\,
      GE => '1',
      Q => \FSM_onehot_next_state_reg_n_0_[3]\
    );
\FSM_onehot_next_state_reg[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAAAAA8"
    )
        port map (
      I0 => aux,
      I1 => \FSM_onehot_next_state_reg[3]_i_3_n_0\,
      I2 => total_reg(3),
      I3 => total_reg(4),
      I4 => total_reg(1),
      I5 => total_reg(0),
      O => \FSM_onehot_next_state_reg[3]_i_1_n_0\
    );
\FSM_onehot_next_state_reg[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFF444444444"
    )
        port map (
      I0 => \FSM_onehot_next_state_reg[3]_i_4_n_0\,
      I1 => aux,
      I2 => \^q\(1),
      I3 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I4 => \^q\(0),
      I5 => button_ok_IBUF,
      O => \next_state__0\
    );
\FSM_onehot_next_state_reg[3]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => total_reg(5),
      I1 => total_reg(7),
      I2 => total_reg(6),
      I3 => total_reg(2),
      O => \FSM_onehot_next_state_reg[3]_i_3_n_0\
    );
\FSM_onehot_next_state_reg[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000007777777F"
    )
        port map (
      I0 => total_reg(5),
      I1 => total_reg(6),
      I2 => total_reg(3),
      I3 => total_reg(4),
      I4 => total_reg(2),
      I5 => total_reg(7),
      O => \FSM_onehot_next_state_reg[3]_i_4_n_0\
    );
clr_dinero_OBUF_inst_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \tot_aux_reg[0]_0\(1),
      I1 => clr_dinero_r,
      O => \^clr_dinero_obuf\
    );
\disp[4][1]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7C70"
    )
        port map (
      I0 => \^total_string_reg[7]_0\,
      I1 => \tot_aux_reg[0]_0\(0),
      I2 => \tot_aux_reg[0]_0\(1),
      I3 => \disp_refresco[4]\(0),
      O => \disp[4]_OBUF\(0)
    );
\disp[4][2]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => \^total_string_reg[7]_0\,
      I1 => \tot_aux_reg[0]_0\(1),
      I2 => \tot_aux_reg[0]_0\(0),
      O => \disp[4]_OBUF\(1)
    );
\disp[5][4]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"50CF"
    )
        port map (
      I0 => \^total_string_reg[7]_0\,
      I1 => \disp_refresco[8]\(0),
      I2 => \tot_aux_reg[0]_0\(0),
      I3 => \tot_aux_reg[0]_0\(1),
      O => \disp[5]_OBUF\(1)
    );
inst_int_to_string: entity work.int_to_string
     port map (
      Q(7 downto 0) => total_out(7 downto 0),
      \disp[2][0]\(0) => \disp[2][0]\(0),
      \disp[2]_OBUF\(0) => \disp[2]_OBUF\(0),
      \disp[3][3]_INST_0_i_7_0\ => \disp[3][3]_INST_0_i_7\,
      \disp[3]_OBUF\(2 downto 0) => \disp[3]_OBUF\(2 downto 0),
      \disp[5][0]\(1 downto 0) => \tot_aux_reg[0]_0\(1 downto 0),
      \disp[5]_OBUF\(0) => \disp[5]_OBUF\(0),
      \disp_refresco[3]\(2 downto 0) => \disp_refresco[3]\(2 downto 0),
      \disp_refresco[5]\(0) => \disp_refresco[5]\(0),
      \total_string_reg[1]\ => \total_string_reg[1]_0\,
      \total_string_reg[1]_0\ => \total_string_reg[1]_1\,
      \total_string_reg[5]\ => \total_string_reg[5]_0\,
      \total_string_reg[7]\ => \^total_string_reg[7]_0\
    );
\next_state_reg[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C5F0C500C5FFC50F"
    )
        port map (
      I0 => \current_state_reg[0]\,
      I1 => \^q\(0),
      I2 => \tot_aux_reg[0]_0\(0),
      I3 => \tot_aux_reg[0]_0\(1),
      I4 => button_ok_IBUF,
      I5 => \current_state_reg[0]_0\,
      O => E(0)
    );
refresco_OBUF_inst_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^q\(0),
      I1 => \FSM_onehot_current_state_reg_n_0_[1]\,
      O => st_dinero_OBUF(0)
    );
\st_dinero_OBUF[1]_inst_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      O => st_dinero_OBUF(1)
    );
\tot_aux_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \^clr_dinero_obuf\,
      D => total_reg(0),
      Q => \tot_aux_reg[7]_0\(0),
      R => '0'
    );
\tot_aux_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \^clr_dinero_obuf\,
      D => total_reg(1),
      Q => \tot_aux_reg[7]_0\(1),
      R => '0'
    );
\tot_aux_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \^clr_dinero_obuf\,
      D => total_reg(2),
      Q => \tot_aux_reg[7]_0\(2),
      R => '0'
    );
\tot_aux_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \^clr_dinero_obuf\,
      D => total_reg(3),
      Q => \tot_aux_reg[7]_0\(3),
      R => '0'
    );
\tot_aux_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \^clr_dinero_obuf\,
      D => total_reg(4),
      Q => \tot_aux_reg[7]_0\(4),
      R => '0'
    );
\tot_aux_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \^clr_dinero_obuf\,
      D => total_reg(5),
      Q => \tot_aux_reg[7]_0\(5),
      R => '0'
    );
\tot_aux_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \^clr_dinero_obuf\,
      D => total_reg(6),
      Q => \tot_aux_reg[7]_0\(6),
      R => '0'
    );
\tot_aux_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \^clr_dinero_obuf\,
      D => total_reg(7),
      Q => \tot_aux_reg[7]_0\(7),
      R => '0'
    );
\tot_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \^clr_dinero_obuf\,
      D => total_out(0),
      Q => \tot_reg[7]_0\(0),
      R => '0'
    );
\tot_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \^clr_dinero_obuf\,
      D => total_out(1),
      Q => \tot_reg[7]_0\(1),
      R => '0'
    );
\tot_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \^clr_dinero_obuf\,
      D => total_out(2),
      Q => \tot_reg[7]_0\(2),
      R => '0'
    );
\tot_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \^clr_dinero_obuf\,
      D => total_out(3),
      Q => \tot_reg[7]_0\(3),
      R => '0'
    );
\tot_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \^clr_dinero_obuf\,
      D => total_out(4),
      Q => \tot_reg[7]_0\(4),
      R => '0'
    );
\tot_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \^clr_dinero_obuf\,
      D => total_out(5),
      Q => \tot_reg[7]_0\(5),
      R => '0'
    );
\tot_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \^clr_dinero_obuf\,
      D => total_out(6),
      Q => \tot_reg[7]_0\(6),
      R => '0'
    );
\tot_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \^clr_dinero_obuf\,
      D => total_out(7),
      Q => \tot_reg[7]_0\(7),
      R => '0'
    );
total0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => total0_carry_n_0,
      CO(2 downto 0) => NLW_total0_carry_CO_UNCONNECTED(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => total_reg(3 downto 0),
      O(3 downto 1) => p_0_in(3 downto 1),
      O(0) => total0_carry_n_7,
      S(3) => total0_carry_i_1_n_0,
      S(2) => total0_carry_i_2_n_0,
      S(1) => total0_carry_i_3_n_0,
      S(0) => total0_carry_i_4_n_0
    );
\total0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => total0_carry_n_0,
      CO(3 downto 0) => \NLW_total0_carry__0_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => total_reg(6 downto 4),
      O(3 downto 0) => p_0_in(7 downto 4),
      S(3) => total_reg(7),
      S(2) => \total0_carry__0_i_1_n_0\,
      S(1) => \total0_carry__0_i_2_n_0\,
      S(0) => \total0_carry__0_i_3_n_0\
    );
\total0_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(6),
      I1 => v_mon_OBUF(6),
      O => \total0_carry__0_i_1_n_0\
    );
\total0_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(5),
      I1 => v_mon_OBUF(5),
      O => \total0_carry__0_i_2_n_0\
    );
\total0_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(4),
      I1 => v_mon_OBUF(4),
      O => \total0_carry__0_i_3_n_0\
    );
total0_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(3),
      I1 => v_mon_OBUF(3),
      O => total0_carry_i_1_n_0
    );
total0_carry_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(2),
      I1 => v_mon_OBUF(2),
      O => total0_carry_i_2_n_0
    );
total0_carry_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(1),
      I1 => v_mon_OBUF(1),
      O => total0_carry_i_3_n_0
    );
total0_carry_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(0),
      I1 => v_mon_OBUF(0),
      O => total0_carry_i_4_n_0
    );
\total[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(0),
      I1 => v_mon_OBUF(0),
      O => p_0_in(0)
    );
\total[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^clr_dinero_obuf\,
      I1 => aux,
      O => \total[7]_i_1_n_0\
    );
\total[7]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0100"
    )
        port map (
      I0 => \^q\(1),
      I1 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I2 => \^q\(0),
      I3 => \^clr_dinero_obuf\,
      O => \total[7]_i_2_n_0\
    );
\total_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[7]_i_2_n_0\,
      D => p_0_in(0),
      Q => total_reg(0),
      R => \total[7]_i_1_n_0\
    );
\total_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[7]_i_2_n_0\,
      D => p_0_in(1),
      Q => total_reg(1),
      R => \total[7]_i_1_n_0\
    );
\total_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[7]_i_2_n_0\,
      D => p_0_in(2),
      Q => total_reg(2),
      R => \total[7]_i_1_n_0\
    );
\total_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[7]_i_2_n_0\,
      D => p_0_in(3),
      Q => total_reg(3),
      R => \total[7]_i_1_n_0\
    );
\total_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[7]_i_2_n_0\,
      D => p_0_in(4),
      Q => total_reg(4),
      R => \total[7]_i_1_n_0\
    );
\total_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[7]_i_2_n_0\,
      D => p_0_in(5),
      Q => total_reg(5),
      R => \total[7]_i_1_n_0\
    );
\total_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[7]_i_2_n_0\,
      D => p_0_in(6),
      Q => total_reg(6),
      R => \total[7]_i_1_n_0\
    );
\total_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[7]_i_2_n_0\,
      D => p_0_in(7),
      Q => total_reg(7),
      R => \total[7]_i_1_n_0\
    );
\total_string[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00020000"
    )
        port map (
      I0 => total0_carry_n_7,
      I1 => \^q\(0),
      I2 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I3 => \^q\(1),
      I4 => aux,
      I5 => total_out(0),
      O => \total_string[0]_i_1_n_0\
    );
\total_string[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00020000"
    )
        port map (
      I0 => \total_string_reg[3]_i_2_n_6\,
      I1 => \^q\(0),
      I2 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I3 => \^q\(1),
      I4 => aux,
      I5 => total_out(1),
      O => \total_string[1]_i_1_n_0\
    );
\total_string[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00020000"
    )
        port map (
      I0 => \total_string_reg[3]_i_2_n_5\,
      I1 => \^q\(0),
      I2 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I3 => \^q\(1),
      I4 => aux,
      I5 => total_out(2),
      O => \total_string[2]_i_1_n_0\
    );
\total_string[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00020000"
    )
        port map (
      I0 => \total_string_reg[3]_i_2_n_4\,
      I1 => \^q\(0),
      I2 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I3 => \^q\(1),
      I4 => aux,
      I5 => total_out(3),
      O => \total_string[3]_i_1_n_0\
    );
\total_string[3]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(3),
      I1 => v_mon_OBUF(3),
      O => \total_string[3]_i_3_n_0\
    );
\total_string[3]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(2),
      I1 => v_mon_OBUF(2),
      O => \total_string[3]_i_4_n_0\
    );
\total_string[3]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(1),
      I1 => v_mon_OBUF(1),
      O => \total_string[3]_i_5_n_0\
    );
\total_string[3]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(0),
      I1 => v_mon_OBUF(0),
      O => \total_string[3]_i_6_n_0\
    );
\total_string[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00020000"
    )
        port map (
      I0 => \total_string_reg[7]_i_2_n_7\,
      I1 => \^q\(0),
      I2 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I3 => \^q\(1),
      I4 => aux,
      I5 => total_out(4),
      O => \total_string[4]_i_1_n_0\
    );
\total_string[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00020000"
    )
        port map (
      I0 => \total_string_reg[7]_i_2_n_6\,
      I1 => \^q\(0),
      I2 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I3 => \^q\(1),
      I4 => aux,
      I5 => total_out(5),
      O => \total_string[5]_i_1_n_0\
    );
\total_string[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00020000"
    )
        port map (
      I0 => \total_string_reg[7]_i_2_n_5\,
      I1 => \^q\(0),
      I2 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I3 => \^q\(1),
      I4 => aux,
      I5 => total_out(6),
      O => \total_string[6]_i_1_n_0\
    );
\total_string[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00020000"
    )
        port map (
      I0 => \total_string_reg[7]_i_2_n_4\,
      I1 => \^q\(0),
      I2 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I3 => \^q\(1),
      I4 => aux,
      I5 => total_out(7),
      O => \total_string[7]_i_1_n_0\
    );
\total_string[7]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(6),
      I1 => v_mon_OBUF(6),
      O => \total_string[7]_i_3_n_0\
    );
\total_string[7]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(5),
      I1 => v_mon_OBUF(5),
      O => \total_string[7]_i_4_n_0\
    );
\total_string[7]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(4),
      I1 => v_mon_OBUF(4),
      O => \total_string[7]_i_5_n_0\
    );
\total_string_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \^clr_dinero_obuf\,
      D => \total_string[0]_i_1_n_0\,
      Q => total_out(0),
      R => '0'
    );
\total_string_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \^clr_dinero_obuf\,
      D => \total_string[1]_i_1_n_0\,
      Q => total_out(1),
      R => '0'
    );
\total_string_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \^clr_dinero_obuf\,
      D => \total_string[2]_i_1_n_0\,
      Q => total_out(2),
      R => '0'
    );
\total_string_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \^clr_dinero_obuf\,
      D => \total_string[3]_i_1_n_0\,
      Q => total_out(3),
      R => '0'
    );
\total_string_reg[3]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \total_string_reg[3]_i_2_n_0\,
      CO(2 downto 0) => \NLW_total_string_reg[3]_i_2_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => total_reg(3 downto 0),
      O(3) => \total_string_reg[3]_i_2_n_4\,
      O(2) => \total_string_reg[3]_i_2_n_5\,
      O(1) => \total_string_reg[3]_i_2_n_6\,
      O(0) => \NLW_total_string_reg[3]_i_2_O_UNCONNECTED\(0),
      S(3) => \total_string[3]_i_3_n_0\,
      S(2) => \total_string[3]_i_4_n_0\,
      S(1) => \total_string[3]_i_5_n_0\,
      S(0) => \total_string[3]_i_6_n_0\
    );
\total_string_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \^clr_dinero_obuf\,
      D => \total_string[4]_i_1_n_0\,
      Q => total_out(4),
      R => '0'
    );
\total_string_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \^clr_dinero_obuf\,
      D => \total_string[5]_i_1_n_0\,
      Q => total_out(5),
      R => '0'
    );
\total_string_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \^clr_dinero_obuf\,
      D => \total_string[6]_i_1_n_0\,
      Q => total_out(6),
      R => '0'
    );
\total_string_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \^clr_dinero_obuf\,
      D => \total_string[7]_i_1_n_0\,
      Q => total_out(7),
      R => '0'
    );
\total_string_reg[7]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \total_string_reg[3]_i_2_n_0\,
      CO(3 downto 0) => \NLW_total_string_reg[7]_i_2_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => total_reg(6 downto 4),
      O(3) => \total_string_reg[7]_i_2_n_4\,
      O(2) => \total_string_reg[7]_i_2_n_5\,
      O(1) => \total_string_reg[7]_i_2_n_6\,
      O(0) => \total_string_reg[7]_i_2_n_7\,
      S(3) => total_reg(7),
      S(2) => \total_string[7]_i_3_n_0\,
      S(1) => \total_string[7]_i_4_n_0\,
      S(0) => \total_string[7]_i_5_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity fsm is
  port (
    RESET : in STD_LOGIC;
    clk : in STD_LOGIC;
    valor_moneda : in STD_LOGIC_VECTOR ( 7 downto 0 );
    button_ok : in STD_LOGIC;
    ref_option : in STD_LOGIC_VECTOR ( 3 downto 0 );
    devolver : out STD_LOGIC;
    refresco : out STD_LOGIC;
    \disp[8]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \disp[7]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \disp[6]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \disp[5]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \disp[4]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \disp[3]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \disp[2]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \disp[1]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    st : out STD_LOGIC_VECTOR ( 1 downto 0 );
    st_dinero : out STD_LOGIC_VECTOR ( 1 downto 0 );
    v_mon : out STD_LOGIC_VECTOR ( 7 downto 0 );
    tot : out STD_LOGIC_VECTOR ( 10 downto 0 );
    tot_aux : out STD_LOGIC_VECTOR ( 10 downto 0 );
    clr_dinero : inout STD_LOGIC;
    bdf_din : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of fsm : entity is true;
  attribute ECO_CHECKSUM : string;
  attribute ECO_CHECKSUM of fsm : entity is "35ae727";
  attribute num_refrescos : integer;
  attribute num_refrescos of fsm : entity is 4;
  attribute width_word : integer;
  attribute width_word of fsm : entity is 8;
end fsm;

architecture STRUCTURE of fsm is
  signal \FSM_onehot_current_state_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal Inst_gestion_dinero_n_10 : STD_LOGIC;
  signal Inst_gestion_dinero_n_11 : STD_LOGIC;
  signal Inst_gestion_dinero_n_17 : STD_LOGIC;
  signal Inst_gestion_dinero_n_7 : STD_LOGIC;
  signal Inst_gestion_dinero_n_8 : STD_LOGIC;
  signal Inst_gestion_dinero_n_9 : STD_LOGIC;
  signal RESET_IBUF : STD_LOGIC;
  signal bdf_din_OBUF : STD_LOGIC;
  signal button_ok_IBUF : STD_LOGIC;
  signal clk_IBUF : STD_LOGIC;
  signal clk_IBUF_BUFG : STD_LOGIC;
  signal clr_dinero_OBUF : STD_LOGIC;
  signal clr_dinero_r : STD_LOGIC;
  signal \current_state[1]_i_1_n_0\ : STD_LOGIC;
  signal devolver_OBUF : STD_LOGIC;
  signal \disp[1]_OBUF\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \disp[2]_OBUF\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \disp[3]_OBUF\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \disp[4]_OBUF\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \disp[5]_OBUF\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \disp[6]_OBUF\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \disp[7]_OBUF\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \disp[8]_OBUF\ : STD_LOGIC_VECTOR ( 4 downto 1 );
  signal \disp_refresco[2]\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \disp_refresco[3]\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \disp_refresco[4]\ : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \disp_refresco[5]\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \disp_refresco[8]\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal next_state : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \next_state_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \next_state_reg[1]_i_1_n_0\ : STD_LOGIC;
  signal \next_state_reg[1]_i_3_n_0\ : STD_LOGIC;
  signal \next_state_reg[1]_i_4_n_0\ : STD_LOGIC;
  signal \next_state_reg[1]_i_5_n_0\ : STD_LOGIC;
  signal ref_option_IBUF : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal st_OBUF : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal st_dinero_OBUF : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal tot_OBUF : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal tot_aux_OBUF : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal v_mon_OBUF : STD_LOGIC_VECTOR ( 7 downto 0 );
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \next_state_reg[0]\ : label is "LD";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \next_state_reg[0]_i_1\ : label is "soft_lutpair31";
  attribute XILINX_LEGACY_PRIM of \next_state_reg[1]\ : label is "LD";
  attribute SOFT_HLUTNM of \next_state_reg[1]_i_1\ : label is "soft_lutpair31";
begin
\FSM_onehot_current_state_reg[3]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => clr_dinero_OBUF,
      O => \FSM_onehot_current_state_reg[3]_i_1_n_0\
    );
Inst_gestion_dinero: entity work.gestion_dinero
     port map (
      AS(0) => \FSM_onehot_current_state_reg[3]_i_1_n_0\,
      E(0) => Inst_gestion_dinero_n_17,
      Q(1) => devolver_OBUF,
      Q(0) => bdf_din_OBUF,
      button_ok_IBUF => button_ok_IBUF,
      clk_IBUF_BUFG => clk_IBUF_BUFG,
      clr_dinero_OBUF => clr_dinero_OBUF,
      clr_dinero_r => clr_dinero_r,
      \current_state_reg[0]\ => \next_state_reg[1]_i_3_n_0\,
      \current_state_reg[0]_0\ => \next_state_reg[1]_i_4_n_0\,
      \disp[2][0]\(0) => \disp_refresco[2]\(0),
      \disp[2]_OBUF\(0) => \disp[2]_OBUF\(0),
      \disp[3][3]_INST_0_i_7\ => Inst_gestion_dinero_n_7,
      \disp[3]_OBUF\(2 downto 0) => \disp[3]_OBUF\(2 downto 0),
      \disp[4]_OBUF\(1 downto 0) => \disp[4]_OBUF\(2 downto 1),
      \disp[5]_OBUF\(1) => \disp[5]_OBUF\(4),
      \disp[5]_OBUF\(0) => \disp[5]_OBUF\(0),
      \disp_refresco[3]\(2) => \disp_refresco[3]\(6),
      \disp_refresco[3]\(1 downto 0) => \disp_refresco[3]\(1 downto 0),
      \disp_refresco[4]\(0) => \disp_refresco[4]\(1),
      \disp_refresco[5]\(0) => \disp_refresco[5]\(0),
      \disp_refresco[8]\(0) => \disp_refresco[8]\(2),
      st_dinero_OBUF(1 downto 0) => st_dinero_OBUF(1 downto 0),
      \tot_aux_reg[0]_0\(1 downto 0) => st_OBUF(1 downto 0),
      \tot_aux_reg[7]_0\(7 downto 0) => tot_aux_OBUF(7 downto 0),
      \tot_reg[7]_0\(7 downto 0) => tot_OBUF(7 downto 0),
      \total_string_reg[1]_0\ => Inst_gestion_dinero_n_9,
      \total_string_reg[1]_1\ => Inst_gestion_dinero_n_10,
      \total_string_reg[5]_0\ => Inst_gestion_dinero_n_8,
      \total_string_reg[7]_0\ => Inst_gestion_dinero_n_11,
      v_mon_OBUF(6 downto 0) => v_mon_OBUF(6 downto 0)
    );
Inst_gestion_refresco: entity work.gestion_refresco
     port map (
      Q(0) => \disp_refresco[2]\(0),
      \disp[1][1]\(1 downto 0) => st_OBUF(1 downto 0),
      \disp[1][1]_0\ => Inst_gestion_dinero_n_11,
      \disp[1]_OBUF\(3 downto 0) => \disp[1]_OBUF\(3 downto 0),
      \disp[2][1]\ => Inst_gestion_dinero_n_9,
      \disp[2][2]\ => Inst_gestion_dinero_n_10,
      \disp[2][3]\ => Inst_gestion_dinero_n_8,
      \disp[2]_OBUF\(4) => \disp[2]_OBUF\(6),
      \disp[2]_OBUF\(3 downto 0) => \disp[2]_OBUF\(4 downto 1),
      \disp[3][3]\ => Inst_gestion_dinero_n_7,
      \disp[3]_OBUF\(2) => \disp[3]_OBUF\(6),
      \disp[3]_OBUF\(1 downto 0) => \disp[3]_OBUF\(4 downto 3),
      \disp[4]_OBUF\(2 downto 1) => \disp[4]_OBUF\(4 downto 3),
      \disp[4]_OBUF\(0) => \disp[4]_OBUF\(0),
      \disp[5]_OBUF\(3) => \disp[5]_OBUF\(6),
      \disp[5]_OBUF\(2 downto 0) => \disp[5]_OBUF\(3 downto 1),
      \disp[6]_OBUF\(3 downto 0) => \disp[6]_OBUF\(3 downto 0),
      \disp[7]_OBUF\(2 downto 0) => \disp[7]_OBUF\(2 downto 0),
      \disp[8]_OBUF\(2 downto 0) => \disp[8]_OBUF\(3 downto 1),
      \disp_aux_reg[6][3]_i_2_0\(0) => \disp_refresco[4]\(1),
      \disp_aux_reg[6][3]_i_2_1\(0) => \disp_refresco[8]\(2),
      \disp_refresco[3]\(2) => \disp_refresco[3]\(6),
      \disp_refresco[3]\(1 downto 0) => \disp_refresco[3]\(1 downto 0),
      \disp_refresco[5]\(0) => \disp_refresco[5]\(0),
      ref_option_IBUF(3 downto 0) => ref_option_IBUF(3 downto 0)
    );
RESET_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => RESET,
      O => RESET_IBUF
    );
bdf_din_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => bdf_din_OBUF,
      O => bdf_din
    );
button_ok_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => button_ok,
      O => button_ok_IBUF
    );
clk_IBUF_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => clk_IBUF,
      O => clk_IBUF_BUFG
    );
clk_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => clk,
      O => clk_IBUF
    );
clr_dinero_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => clr_dinero_OBUF,
      O => clr_dinero
    );
clr_dinero_r_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \current_state[1]_i_1_n_0\,
      D => '1',
      Q => clr_dinero_r
    );
\current_state[1]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => RESET_IBUF,
      O => \current_state[1]_i_1_n_0\
    );
\current_state_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \current_state[1]_i_1_n_0\,
      D => next_state(0),
      Q => st_OBUF(0)
    );
\current_state_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \current_state[1]_i_1_n_0\,
      D => next_state(1),
      Q => st_OBUF(1)
    );
devolver_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => devolver_OBUF,
      O => devolver
    );
\disp[1][0]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[1]_OBUF\(0),
      O => \disp[1]\(0)
    );
\disp[1][1]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[1]_OBUF\(1),
      O => \disp[1]\(1)
    );
\disp[1][2]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[1]_OBUF\(2),
      O => \disp[1]\(2)
    );
\disp[1][3]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[1]_OBUF\(3),
      O => \disp[1]\(3)
    );
\disp[1][4]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[1]\(4)
    );
\disp[1][5]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[1]\(5)
    );
\disp[1][6]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[1]_OBUF\(0),
      O => \disp[1]\(6)
    );
\disp[1][7]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[1]\(7)
    );
\disp[2][0]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[2]_OBUF\(0),
      O => \disp[2]\(0)
    );
\disp[2][1]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[2]_OBUF\(1),
      O => \disp[2]\(1)
    );
\disp[2][2]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[2]_OBUF\(2),
      O => \disp[2]\(2)
    );
\disp[2][3]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[2]_OBUF\(3),
      O => \disp[2]\(3)
    );
\disp[2][4]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[2]_OBUF\(4),
      O => \disp[2]\(4)
    );
\disp[2][5]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[2]\(5)
    );
\disp[2][6]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[2]_OBUF\(6),
      O => \disp[2]\(6)
    );
\disp[2][7]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[2]\(7)
    );
\disp[3][0]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[3]_OBUF\(0),
      O => \disp[3]\(0)
    );
\disp[3][1]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[3]_OBUF\(1),
      O => \disp[3]\(1)
    );
\disp[3][2]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[3]_OBUF\(2),
      O => \disp[3]\(2)
    );
\disp[3][3]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[3]_OBUF\(3),
      O => \disp[3]\(3)
    );
\disp[3][4]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[3]_OBUF\(4),
      O => \disp[3]\(4)
    );
\disp[3][5]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[3]\(5)
    );
\disp[3][6]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[3]_OBUF\(6),
      O => \disp[3]\(6)
    );
\disp[3][7]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[3]\(7)
    );
\disp[4][0]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[4]_OBUF\(0),
      O => \disp[4]\(0)
    );
\disp[4][1]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[4]_OBUF\(1),
      O => \disp[4]\(1)
    );
\disp[4][2]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[4]_OBUF\(2),
      O => \disp[4]\(2)
    );
\disp[4][3]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[4]_OBUF\(3),
      O => \disp[4]\(3)
    );
\disp[4][4]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[4]_OBUF\(4),
      O => \disp[4]\(4)
    );
\disp[4][5]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[4]\(5)
    );
\disp[4][6]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[2]_OBUF\(6),
      O => \disp[4]\(6)
    );
\disp[4][7]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[4]\(7)
    );
\disp[5][0]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[5]_OBUF\(0),
      O => \disp[5]\(0)
    );
\disp[5][1]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[5]_OBUF\(1),
      O => \disp[5]\(1)
    );
\disp[5][2]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[5]_OBUF\(2),
      O => \disp[5]\(2)
    );
\disp[5][3]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[5]_OBUF\(3),
      O => \disp[5]\(3)
    );
\disp[5][4]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[5]_OBUF\(4),
      O => \disp[5]\(4)
    );
\disp[5][5]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[5]\(5)
    );
\disp[5][6]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[5]_OBUF\(6),
      O => \disp[5]\(6)
    );
\disp[5][7]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[5]\(7)
    );
\disp[6][0]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[6]_OBUF\(0),
      O => \disp[6]\(0)
    );
\disp[6][1]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[6]_OBUF\(1),
      O => \disp[6]\(1)
    );
\disp[6][2]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[6]_OBUF\(2),
      O => \disp[6]\(2)
    );
\disp[6][3]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[6]_OBUF\(3),
      O => \disp[6]\(3)
    );
\disp[6][4]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[6]\(4)
    );
\disp[6][5]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[6]\(5)
    );
\disp[6][6]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[5]_OBUF\(6),
      O => \disp[6]\(6)
    );
\disp[6][7]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[6]\(7)
    );
\disp[7][0]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[7]_OBUF\(0),
      O => \disp[7]\(0)
    );
\disp[7][1]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[7]_OBUF\(1),
      O => \disp[7]\(1)
    );
\disp[7][2]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[7]_OBUF\(2),
      O => \disp[7]\(2)
    );
\disp[7][3]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[7]_OBUF\(1),
      O => \disp[7]\(3)
    );
\disp[7][4]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[7]\(4)
    );
\disp[7][5]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[7]\(5)
    );
\disp[7][6]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[7]_OBUF\(0),
      O => \disp[7]\(6)
    );
\disp[7][7]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[7]\(7)
    );
\disp[8][0]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[6]_OBUF\(0),
      O => \disp[8]\(0)
    );
\disp[8][1]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[8]_OBUF\(1),
      O => \disp[8]\(1)
    );
\disp[8][2]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[8]_OBUF\(2),
      O => \disp[8]\(2)
    );
\disp[8][3]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[8]_OBUF\(3),
      O => \disp[8]\(3)
    );
\disp[8][4]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[8]_OBUF\(4),
      O => \disp[8]\(4)
    );
\disp[8][4]_INST_0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => st_OBUF(1),
      I1 => st_OBUF(0),
      O => \disp[8]_OBUF\(4)
    );
\disp[8][5]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[8]\(5)
    );
\disp[8][6]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[7]_OBUF\(0),
      O => \disp[8]\(6)
    );
\disp[8][7]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[8]\(7)
    );
\next_state_reg[0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \next_state_reg[0]_i_1_n_0\,
      G => Inst_gestion_dinero_n_17,
      GE => '1',
      Q => next_state(0)
    );
\next_state_reg[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"15"
    )
        port map (
      I0 => st_OBUF(0),
      I1 => st_OBUF(1),
      I2 => \next_state_reg[1]_i_3_n_0\,
      O => \next_state_reg[0]_i_1_n_0\
    );
\next_state_reg[1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \next_state_reg[1]_i_1_n_0\,
      G => Inst_gestion_dinero_n_17,
      GE => '1',
      Q => next_state(1)
    );
\next_state_reg[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"34"
    )
        port map (
      I0 => \next_state_reg[1]_i_3_n_0\,
      I1 => st_OBUF(1),
      I2 => st_OBUF(0),
      O => \next_state_reg[1]_i_1_n_0\
    );
\next_state_reg[1]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => v_mon_OBUF(0),
      I1 => v_mon_OBUF(2),
      I2 => v_mon_OBUF(5),
      I3 => v_mon_OBUF(6),
      I4 => \next_state_reg[1]_i_5_n_0\,
      O => \next_state_reg[1]_i_3_n_0\
    );
\next_state_reg[1]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => ref_option_IBUF(0),
      I1 => ref_option_IBUF(1),
      I2 => ref_option_IBUF(2),
      I3 => ref_option_IBUF(3),
      O => \next_state_reg[1]_i_4_n_0\
    );
\next_state_reg[1]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => v_mon_OBUF(3),
      I1 => v_mon_OBUF(1),
      I2 => v_mon_OBUF(7),
      I3 => v_mon_OBUF(4),
      O => \next_state_reg[1]_i_5_n_0\
    );
\ref_option_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ref_option(0),
      O => ref_option_IBUF(0)
    );
\ref_option_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ref_option(1),
      O => ref_option_IBUF(1)
    );
\ref_option_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ref_option(2),
      O => ref_option_IBUF(2)
    );
\ref_option_IBUF[3]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ref_option(3),
      O => ref_option_IBUF(3)
    );
refresco_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => st_dinero_OBUF(0),
      O => refresco
    );
\st_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => st_OBUF(0),
      O => st(0)
    );
\st_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => st_OBUF(1),
      O => st(1)
    );
\st_dinero_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => st_dinero_OBUF(0),
      O => st_dinero(0)
    );
\st_dinero_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => st_dinero_OBUF(1),
      O => st_dinero(1)
    );
\tot_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_OBUF(0),
      O => tot(0)
    );
\tot_OBUF[10]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => tot(10)
    );
\tot_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_OBUF(1),
      O => tot(1)
    );
\tot_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_OBUF(2),
      O => tot(2)
    );
\tot_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_OBUF(3),
      O => tot(3)
    );
\tot_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_OBUF(4),
      O => tot(4)
    );
\tot_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_OBUF(5),
      O => tot(5)
    );
\tot_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_OBUF(6),
      O => tot(6)
    );
\tot_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_OBUF(7),
      O => tot(7)
    );
\tot_OBUF[8]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => tot(8)
    );
\tot_OBUF[9]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => tot(9)
    );
\tot_aux_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_aux_OBUF(0),
      O => tot_aux(0)
    );
\tot_aux_OBUF[10]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => tot_aux(10)
    );
\tot_aux_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_aux_OBUF(1),
      O => tot_aux(1)
    );
\tot_aux_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_aux_OBUF(2),
      O => tot_aux(2)
    );
\tot_aux_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_aux_OBUF(3),
      O => tot_aux(3)
    );
\tot_aux_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_aux_OBUF(4),
      O => tot_aux(4)
    );
\tot_aux_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_aux_OBUF(5),
      O => tot_aux(5)
    );
\tot_aux_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_aux_OBUF(6),
      O => tot_aux(6)
    );
\tot_aux_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_aux_OBUF(7),
      O => tot_aux(7)
    );
\tot_aux_OBUF[8]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => tot_aux(8)
    );
\tot_aux_OBUF[9]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => tot_aux(9)
    );
\v_mon_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => v_mon_OBUF(0),
      O => v_mon(0)
    );
\v_mon_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => v_mon_OBUF(1),
      O => v_mon(1)
    );
\v_mon_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => v_mon_OBUF(2),
      O => v_mon(2)
    );
\v_mon_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => v_mon_OBUF(3),
      O => v_mon(3)
    );
\v_mon_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => v_mon_OBUF(4),
      O => v_mon(4)
    );
\v_mon_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => v_mon_OBUF(5),
      O => v_mon(5)
    );
\v_mon_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => v_mon_OBUF(6),
      O => v_mon(6)
    );
\v_mon_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => v_mon_OBUF(7),
      O => v_mon(7)
    );
\valor_moneda_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(0),
      O => v_mon_OBUF(0)
    );
\valor_moneda_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(1),
      O => v_mon_OBUF(1)
    );
\valor_moneda_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(2),
      O => v_mon_OBUF(2)
    );
\valor_moneda_IBUF[3]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(3),
      O => v_mon_OBUF(3)
    );
\valor_moneda_IBUF[4]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(4),
      O => v_mon_OBUF(4)
    );
\valor_moneda_IBUF[5]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(5),
      O => v_mon_OBUF(5)
    );
\valor_moneda_IBUF[6]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(6),
      O => v_mon_OBUF(6)
    );
\valor_moneda_IBUF[7]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(7),
      O => v_mon_OBUF(7)
    );
end STRUCTURE;
