-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Fri Jan  8 13:31:43 2021
-- Host        : LAPTOP-7BL7BHFF running 64-bit major release  (build 9200)
-- Command     : write_vhdl -mode funcsim -nolib -force -file
--               C:/Users/Inees/Desktop/Trabajo_SED/MaquinaExpendedora/MaquinaExpendedora.sim/sim_1/impl/func/xsim/top_synt_tb_func_impl.vhd
-- Design      : top
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tcsg324-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity contador is
  port (
    reset : out STD_LOGIC;
    \reg_reg[10]_0\ : out STD_LOGIC;
    clk_100Mh_IBUF_BUFG : in STD_LOGIC;
    reset_IBUF : in STD_LOGIC;
    clk : in STD_LOGIC
  );
end contador;

architecture STRUCTURE of contador is
  signal clk_pre_i_2_n_0 : STD_LOGIC;
  signal clk_pre_i_3_n_0 : STD_LOGIC;
  signal clk_pre_i_4_n_0 : STD_LOGIC;
  signal \reg[0]_i_2_n_0\ : STD_LOGIC;
  signal reg_reg : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal \reg_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \reg_reg[0]_i_1_n_4\ : STD_LOGIC;
  signal \reg_reg[0]_i_1_n_5\ : STD_LOGIC;
  signal \reg_reg[0]_i_1_n_6\ : STD_LOGIC;
  signal \reg_reg[0]_i_1_n_7\ : STD_LOGIC;
  signal \reg_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \reg_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \reg_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \reg_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \reg_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \reg_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \reg_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \reg_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \reg_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \reg_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \reg_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \reg_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \reg_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \reg_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \reg_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \reg_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal \^reset\ : STD_LOGIC;
  signal \NLW_reg_reg[0]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_reg_reg[12]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_reg_reg[16]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_reg_reg[16]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_reg_reg[4]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_reg_reg[8]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  attribute OPT_MODIFIED : string;
  attribute OPT_MODIFIED of \reg_reg[0]_i_1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \reg_reg[12]_i_1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \reg_reg[4]_i_1\ : label is "SWEEP";
  attribute OPT_MODIFIED of \reg_reg[8]_i_1\ : label is "SWEEP";
begin
  reset <= \^reset\;
\FSM_sequential_current_state[1]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => reset_IBUF,
      O => \^reset\
    );
clk_pre_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFBFFFF00040000"
    )
        port map (
      I0 => reg_reg(10),
      I1 => reg_reg(14),
      I2 => reg_reg(3),
      I3 => clk_pre_i_2_n_0,
      I4 => clk_pre_i_3_n_0,
      I5 => clk,
      O => \reg_reg[10]_0\
    );
clk_pre_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFEFFF"
    )
        port map (
      I0 => reg_reg(5),
      I1 => reg_reg(9),
      I2 => reg_reg(16),
      I3 => reg_reg(13),
      I4 => clk_pre_i_4_n_0,
      O => clk_pre_i_2_n_0
    );
clk_pre_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000800"
    )
        port map (
      I0 => reg_reg(11),
      I1 => reg_reg(2),
      I2 => reg_reg(7),
      I3 => reg_reg(1),
      I4 => reg_reg(8),
      I5 => reg_reg(12),
      O => clk_pre_i_3_n_0
    );
clk_pre_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => reg_reg(6),
      I1 => reg_reg(4),
      I2 => reg_reg(15),
      I3 => reg_reg(0),
      O => clk_pre_i_4_n_0
    );
\reg[0]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => reg_reg(0),
      O => \reg[0]_i_2_n_0\
    );
\reg_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[0]_i_1_n_7\,
      Q => reg_reg(0)
    );
\reg_reg[0]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \reg_reg[0]_i_1_n_0\,
      CO(2 downto 0) => \NLW_reg_reg[0]_i_1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \reg_reg[0]_i_1_n_4\,
      O(2) => \reg_reg[0]_i_1_n_5\,
      O(1) => \reg_reg[0]_i_1_n_6\,
      O(0) => \reg_reg[0]_i_1_n_7\,
      S(3 downto 1) => reg_reg(3 downto 1),
      S(0) => \reg[0]_i_2_n_0\
    );
\reg_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[8]_i_1_n_5\,
      Q => reg_reg(10)
    );
\reg_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[8]_i_1_n_4\,
      Q => reg_reg(11)
    );
\reg_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[12]_i_1_n_7\,
      Q => reg_reg(12)
    );
\reg_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \reg_reg[8]_i_1_n_0\,
      CO(3) => \reg_reg[12]_i_1_n_0\,
      CO(2 downto 0) => \NLW_reg_reg[12]_i_1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \reg_reg[12]_i_1_n_4\,
      O(2) => \reg_reg[12]_i_1_n_5\,
      O(1) => \reg_reg[12]_i_1_n_6\,
      O(0) => \reg_reg[12]_i_1_n_7\,
      S(3 downto 0) => reg_reg(15 downto 12)
    );
\reg_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[12]_i_1_n_6\,
      Q => reg_reg(13)
    );
\reg_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[12]_i_1_n_5\,
      Q => reg_reg(14)
    );
\reg_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[12]_i_1_n_4\,
      Q => reg_reg(15)
    );
\reg_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[16]_i_1_n_7\,
      Q => reg_reg(16)
    );
\reg_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \reg_reg[12]_i_1_n_0\,
      CO(3 downto 0) => \NLW_reg_reg[16]_i_1_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_reg_reg[16]_i_1_O_UNCONNECTED\(3 downto 1),
      O(0) => \reg_reg[16]_i_1_n_7\,
      S(3 downto 1) => B"000",
      S(0) => reg_reg(16)
    );
\reg_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[0]_i_1_n_6\,
      Q => reg_reg(1)
    );
\reg_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[0]_i_1_n_5\,
      Q => reg_reg(2)
    );
\reg_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[0]_i_1_n_4\,
      Q => reg_reg(3)
    );
\reg_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[4]_i_1_n_7\,
      Q => reg_reg(4)
    );
\reg_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \reg_reg[0]_i_1_n_0\,
      CO(3) => \reg_reg[4]_i_1_n_0\,
      CO(2 downto 0) => \NLW_reg_reg[4]_i_1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \reg_reg[4]_i_1_n_4\,
      O(2) => \reg_reg[4]_i_1_n_5\,
      O(1) => \reg_reg[4]_i_1_n_6\,
      O(0) => \reg_reg[4]_i_1_n_7\,
      S(3 downto 0) => reg_reg(7 downto 4)
    );
\reg_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[4]_i_1_n_6\,
      Q => reg_reg(5)
    );
\reg_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[4]_i_1_n_5\,
      Q => reg_reg(6)
    );
\reg_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[4]_i_1_n_4\,
      Q => reg_reg(7)
    );
\reg_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[8]_i_1_n_7\,
      Q => reg_reg(8)
    );
\reg_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \reg_reg[4]_i_1_n_0\,
      CO(3) => \reg_reg[8]_i_1_n_0\,
      CO(2 downto 0) => \NLW_reg_reg[8]_i_1_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \reg_reg[8]_i_1_n_4\,
      O(2) => \reg_reg[8]_i_1_n_5\,
      O(1) => \reg_reg[8]_i_1_n_6\,
      O(0) => \reg_reg[8]_i_1_n_7\,
      S(3 downto 0) => reg_reg(11 downto 8)
    );
\reg_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[8]_i_1_n_6\,
      Q => reg_reg(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \contador__parameterized1\ is
  port (
    \reg_reg[0]_0\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \reg_reg[2]_0\ : out STD_LOGIC;
    \charact_dot_reg[0]\ : out STD_LOGIC;
    \reg_reg[2]_1\ : out STD_LOGIC;
    \reg_reg[2]_2\ : out STD_LOGIC;
    \reg_reg[2]_3\ : out STD_LOGIC;
    \reg_reg[1]_0\ : out STD_LOGIC;
    \reg_reg[2]_4\ : out STD_LOGIC;
    elem_OBUF : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \charact_dot_reg[0]_0\ : out STD_LOGIC;
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    dot : out STD_LOGIC;
    \caracter[6]_i_4\ : out STD_LOGIC;
    \reg_reg[0]_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \reg_reg[2]_5\ : out STD_LOGIC;
    \reg_reg[0]_2\ : out STD_LOGIC;
    \caracter_reg[4]\ : in STD_LOGIC;
    \charact_dot_reg[0]_1\ : in STD_LOGIC;
    \caracter_reg[4]_0\ : in STD_LOGIC;
    \caracter[6]_i_4_0\ : in STD_LOGIC;
    \caracter[6]_i_4_1\ : in STD_LOGIC;
    \caracter[6]_i_4_2\ : in STD_LOGIC;
    \caracter[6]_i_16\ : in STD_LOGIC;
    current_state : in STD_LOGIC_VECTOR ( 0 to 0 );
    reset_IBUF : in STD_LOGIC;
    dot_reg : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    output0 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \contador__parameterized1\ : entity is "contador";
end \contador__parameterized1\;

architecture STRUCTURE of \contador__parameterized1\ is
  signal \^co\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^q\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \caracter[6]_i_10_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_11_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_12_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_13_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_9_n_0\ : STD_LOGIC;
  signal plusOp : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \reg[1]_i_1_n_0\ : STD_LOGIC;
  signal \reg[2]_i_1_n_0\ : STD_LOGIC;
  signal \NLW_caracter_reg[6]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[6]_i_2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \caracter[1]_i_10\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \caracter[6]_i_22\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \caracter[6]_i_24\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \caracter[6]_i_26\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \caracter[6]_i_6\ : label is "soft_lutpair5";
  attribute OPT_MODIFIED : string;
  attribute OPT_MODIFIED of \caracter_reg[6]_i_2\ : label is "SWEEP";
  attribute SOFT_HLUTNM of \charact_dot[0]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of dot_i_1 : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \elem_OBUF[0]_inst_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \elem_OBUF[1]_inst_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \elem_OBUF[2]_inst_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \elem_OBUF[3]_inst_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \elem_OBUF[4]_inst_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \elem_OBUF[5]_inst_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \elem_OBUF[6]_inst_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \elem_OBUF[7]_inst_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \reg[0]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \reg[1]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \reg[2]_i_1\ : label is "soft_lutpair11";
begin
  CO(0) <= \^co\(0);
  Q(2 downto 0) <= \^q\(2 downto 0);
\caracter[1]_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"68"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(0),
      I2 => \^q\(1),
      O => \reg_reg[2]_3\
    );
\caracter[2]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"42"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(1),
      I2 => \^q\(0),
      O => \reg_reg[2]_4\
    );
\caracter[4]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"114011403FC00000"
    )
        port map (
      I0 => \caracter_reg[4]\,
      I1 => \^q\(0),
      I2 => \charact_dot_reg[0]_1\,
      I3 => \^q\(1),
      I4 => \caracter_reg[4]_0\,
      I5 => \^q\(2),
      O => \reg_reg[0]_0\
    );
\caracter[4]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      O => \reg_reg[0]_1\(0)
    );
\caracter[6]_i_10\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(0),
      O => \caracter[6]_i_10_n_0\
    );
\caracter[6]_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(2),
      O => \caracter[6]_i_11_n_0\
    );
\caracter[6]_i_12\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      O => \caracter[6]_i_12_n_0\
    );
\caracter[6]_i_13\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(0),
      I1 => \charact_dot_reg[0]_1\,
      O => \caracter[6]_i_13_n_0\
    );
\caracter[6]_i_19\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000090951015"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \caracter[6]_i_4_0\,
      I4 => \caracter[6]_i_4_1\,
      I5 => \caracter[6]_i_4_2\,
      O => \reg_reg[2]_1\
    );
\caracter[6]_i_22\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8015"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(0),
      I2 => \charact_dot_reg[0]_1\,
      I3 => \^q\(1),
      O => \reg_reg[2]_5\
    );
\caracter[6]_i_24\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"13FE"
    )
        port map (
      I0 => \charact_dot_reg[0]_1\,
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \^q\(2),
      O => \charact_dot_reg[0]\
    );
\caracter[6]_i_26\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFBBB"
    )
        port map (
      I0 => \caracter[6]_i_16\,
      I1 => \^q\(2),
      I2 => \^q\(0),
      I3 => \^q\(1),
      I4 => current_state(0),
      O => \reg_reg[2]_2\
    );
\caracter[6]_i_34\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \^q\(1),
      I1 => \charact_dot_reg[0]_1\,
      I2 => \^q\(0),
      O => \reg_reg[1]_0\
    );
\caracter[6]_i_35\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(0),
      I1 => \charact_dot_reg[0]_1\,
      O => \reg_reg[0]_2\
    );
\caracter[6]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2820"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(0),
      I2 => \^q\(1),
      I3 => \charact_dot_reg[0]_1\,
      O => \reg_reg[2]_0\
    );
\caracter[6]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(2),
      O => plusOp(2)
    );
\caracter[6]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      O => \caracter[6]_i_9_n_0\
    );
\caracter_reg[6]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \^co\(0),
      CO(2 downto 0) => \NLW_caracter_reg[6]_i_2_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => plusOp(2),
      DI(1) => \caracter[6]_i_9_n_0\,
      DI(0) => \caracter[6]_i_10_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[6]_i_2_O_UNCONNECTED\(3 downto 0),
      S(3) => '1',
      S(2) => \caracter[6]_i_11_n_0\,
      S(1) => \caracter[6]_i_12_n_0\,
      S(0) => \caracter[6]_i_13_n_0\
    );
\charact_dot[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F4CC"
    )
        port map (
      I0 => \^co\(0),
      I1 => \charact_dot_reg[0]_1\,
      I2 => dot_reg,
      I3 => reset_IBUF,
      O => \charact_dot_reg[0]_0\
    );
dot_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"D"
    )
        port map (
      I0 => \^co\(0),
      I1 => dot_reg,
      O => dot
    );
dot_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^co\(0),
      I1 => dot_reg,
      O => \caracter[6]_i_4\
    );
\elem_OBUF[0]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEF"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(0),
      I2 => reset_IBUF,
      I3 => \^q\(1),
      O => elem_OBUF(0)
    );
\elem_OBUF[1]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FDFF"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => \^q\(2),
      I3 => reset_IBUF,
      O => elem_OBUF(1)
    );
\elem_OBUF[2]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FBFF"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => reset_IBUF,
      O => elem_OBUF(2)
    );
\elem_OBUF[3]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F7FF"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(2),
      I3 => reset_IBUF,
      O => elem_OBUF(3)
    );
\elem_OBUF[4]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FBFF"
    )
        port map (
      I0 => \^q\(0),
      I1 => reset_IBUF,
      I2 => \^q\(1),
      I3 => \^q\(2),
      O => elem_OBUF(4)
    );
\elem_OBUF[5]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => reset_IBUF,
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \^q\(2),
      O => elem_OBUF(5)
    );
\elem_OBUF[6]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => reset_IBUF,
      I3 => \^q\(2),
      O => elem_OBUF(6)
    );
\elem_OBUF[7]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => reset_IBUF,
      I1 => \^q\(2),
      I2 => \^q\(0),
      I3 => \^q\(1),
      O => elem_OBUF(7)
    );
\reg[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(0),
      O => \reg[0]_i_1_n_0\
    );
\reg[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      O => \reg[1]_i_1_n_0\
    );
\reg[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(0),
      I2 => \^q\(1),
      O => \reg[2]_i_1_n_0\
    );
\reg_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => \reg[0]_i_1_n_0\,
      Q => \^q\(0)
    );
\reg_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => \reg[1]_i_1_n_0\,
      Q => \^q\(1)
    );
\reg_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => \reg[2]_i_1_n_0\,
      Q => \^q\(2)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decod_monedas is
  port (
    DI : out STD_LOGIC_VECTOR ( 3 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 6 downto 0 );
    \valor_moneda_reg[4]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \valor_moneda_reg[5]_0\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[1]\ : out STD_LOGIC;
    \total_reg[3]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    current_state : in STD_LOGIC_VECTOR ( 1 downto 0 );
    D : in STD_LOGIC_VECTOR ( 6 downto 0 );
    clk_BUFG : in STD_LOGIC
  );
end decod_monedas;

architecture STRUCTURE of decod_monedas is
  signal \FSM_sequential_current_state[1]_i_5_n_0\ : STD_LOGIC;
  signal \^q\ : STD_LOGIC_VECTOR ( 6 downto 0 );
begin
  Q(6 downto 0) <= \^q\(6 downto 0);
\FSM_sequential_current_state[0]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => \^q\(5),
      I1 => \^q\(6),
      I2 => \^q\(3),
      I3 => \FSM_sequential_current_state[1]_i_5_n_0\,
      O => \valor_moneda_reg[5]_0\
    );
\FSM_sequential_current_state[1]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222220"
    )
        port map (
      I0 => current_state(1),
      I1 => current_state(0),
      I2 => \FSM_sequential_current_state[1]_i_5_n_0\,
      I3 => \^q\(3),
      I4 => \^q\(6),
      I5 => \^q\(5),
      O => \FSM_sequential_current_state_reg[1]\
    );
\FSM_sequential_current_state[1]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(4),
      I3 => \^q\(2),
      O => \FSM_sequential_current_state[1]_i_5_n_0\
    );
\i__carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(4),
      I1 => \total_reg[3]\(0),
      O => \valor_moneda_reg[4]_0\(0)
    );
\i__carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(3),
      I1 => \total_reg[3]\(0),
      O => DI(3)
    );
\i__carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(2),
      I1 => \total_reg[3]\(0),
      O => DI(2)
    );
\i__carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(1),
      I1 => \total_reg[3]\(0),
      O => DI(1)
    );
\i__carry_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q\(0),
      I1 => \total_reg[3]\(0),
      O => DI(0)
    );
\valor_moneda_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => D(0),
      Q => \^q\(0),
      R => '0'
    );
\valor_moneda_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => D(1),
      Q => \^q\(1),
      R => '0'
    );
\valor_moneda_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => D(2),
      Q => \^q\(2),
      R => '0'
    );
\valor_moneda_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => D(3),
      Q => \^q\(3),
      R => '0'
    );
\valor_moneda_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => D(4),
      Q => \^q\(4),
      R => '0'
    );
\valor_moneda_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => D(5),
      Q => \^q\(5),
      R => '0'
    );
\valor_moneda_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => D(6),
      Q => \^q\(6),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity digito is
  port (
    digit_OBUF : out STD_LOGIC_VECTOR ( 3 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 5 downto 0 )
  );
end digito;

architecture STRUCTURE of digito is
begin
\digit_OBUF[0]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0008000390102081"
    )
        port map (
      I0 => Q(0),
      I1 => Q(1),
      I2 => Q(5),
      I3 => Q(3),
      I4 => Q(2),
      I5 => Q(4),
      O => digit_OBUF(0)
    );
\digit_OBUF[1]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEEABDBC7B33BBFF"
    )
        port map (
      I0 => Q(3),
      I1 => Q(5),
      I2 => Q(1),
      I3 => Q(0),
      I4 => Q(2),
      I5 => Q(4),
      O => digit_OBUF(1)
    );
\digit_OBUF[2]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DFFFECFFCFFB8773"
    )
        port map (
      I0 => Q(1),
      I1 => Q(4),
      I2 => Q(2),
      I3 => Q(5),
      I4 => Q(3),
      I5 => Q(0),
      O => digit_OBUF(2)
    );
\digit_OBUF[4]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFC3FDFC1F0FF9F"
    )
        port map (
      I0 => Q(0),
      I1 => Q(2),
      I2 => Q(5),
      I3 => Q(1),
      I4 => Q(4),
      I5 => Q(3),
      O => digit_OBUF(3)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity edgedtctr is
  port (
    \sreg_reg[2]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \sreg_reg[2]_1\ : out STD_LOGIC;
    \sreg_reg[0]_0\ : out STD_LOGIC;
    \sreg_reg[0]_1\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    \valor_moneda_reg[2]\ : in STD_LOGIC;
    \valor_moneda_reg[2]_0\ : in STD_LOGIC;
    \valor_moneda_reg[2]_1\ : in STD_LOGIC;
    \valor_moneda_reg[2]_2\ : in STD_LOGIC;
    \valor_moneda_reg[0]\ : in STD_LOGIC;
    sreg : in STD_LOGIC_VECTOR ( 2 downto 0 )
  );
end edgedtctr;

architecture STRUCTURE of edgedtctr is
  signal sreg_0 : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \^sreg_reg[2]_1\ : STD_LOGIC;
begin
  \sreg_reg[2]_1\ <= \^sreg_reg[2]_1\;
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \sreg_reg[0]_1\,
      Q => sreg_0(0),
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => sreg_0(0),
      Q => sreg_0(1),
      R => '0'
    );
\sreg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => sreg_0(1),
      Q => sreg_0(2),
      R => '0'
    );
\valor_moneda[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000A8AA00000000"
    )
        port map (
      I0 => \valor_moneda_reg[2]_2\,
      I1 => sreg_0(0),
      I2 => sreg_0(1),
      I3 => sreg_0(2),
      I4 => \valor_moneda_reg[2]\,
      I5 => \valor_moneda_reg[0]\,
      O => \sreg_reg[2]_0\(0)
    );
\valor_moneda[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"68000000"
    )
        port map (
      I0 => \^sreg_reg[2]_1\,
      I1 => \valor_moneda_reg[2]\,
      I2 => \valor_moneda_reg[2]_0\,
      I3 => \valor_moneda_reg[2]_1\,
      I4 => \valor_moneda_reg[2]_2\,
      O => \sreg_reg[2]_0\(1)
    );
\valor_moneda[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFEFEF00EFEFEFEF"
    )
        port map (
      I0 => sreg_0(0),
      I1 => sreg_0(1),
      I2 => sreg_0(2),
      I3 => sreg(0),
      I4 => sreg(1),
      I5 => sreg(2),
      O => \sreg_reg[0]_0\
    );
\valor_moneda[6]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => sreg_0(2),
      I1 => sreg_0(1),
      I2 => sreg_0(0),
      O => \^sreg_reg[2]_1\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity edgedtctr_10 is
  port (
    sreg : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \sreg_reg[0]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \sreg_reg[2]_0\ : out STD_LOGIC;
    \sreg_reg[0]_1\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    \valor_moneda_reg[3]\ : in STD_LOGIC;
    \valor_moneda_reg[3]_0\ : in STD_LOGIC;
    \valor_moneda_reg[3]_1\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of edgedtctr_10 : entity is "edgedtctr";
end edgedtctr_10;

architecture STRUCTURE of edgedtctr_10 is
  signal \^sreg\ : STD_LOGIC_VECTOR ( 2 downto 0 );
begin
  sreg(2 downto 0) <= \^sreg\(2 downto 0);
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \sreg_reg[0]_1\,
      Q => \^sreg\(0),
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \^sreg\(0),
      Q => \^sreg\(1),
      R => '0'
    );
\sreg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \^sreg\(1),
      Q => \^sreg\(2),
      R => '0'
    );
\valor_moneda[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5455000000000000"
    )
        port map (
      I0 => \valor_moneda_reg[3]\,
      I1 => \^sreg\(0),
      I2 => \^sreg\(1),
      I3 => \^sreg\(2),
      I4 => \valor_moneda_reg[3]_0\,
      I5 => \valor_moneda_reg[3]_1\,
      O => \sreg_reg[0]_0\(0)
    );
\valor_moneda[4]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => \^sreg\(2),
      I1 => \^sreg\(1),
      I2 => \^sreg\(0),
      O => \sreg_reg[2]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity edgedtctr_11 is
  port (
    \sreg_reg[2]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \sreg_reg[0]_0\ : out STD_LOGIC;
    \sreg_reg[2]_1\ : out STD_LOGIC;
    \sreg_reg[0]_1\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    \valor_moneda_reg[1]\ : in STD_LOGIC;
    \valor_moneda_reg[1]_0\ : in STD_LOGIC;
    \valor_moneda_reg[1]_1\ : in STD_LOGIC;
    sreg : in STD_LOGIC_VECTOR ( 2 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of edgedtctr_11 : entity is "edgedtctr";
end edgedtctr_11;

architecture STRUCTURE of edgedtctr_11 is
  signal sreg_0 : STD_LOGIC_VECTOR ( 2 downto 0 );
begin
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \sreg_reg[0]_1\,
      Q => sreg_0(0),
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => sreg_0(0),
      Q => sreg_0(1),
      R => '0'
    );
\sreg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => sreg_0(1),
      Q => sreg_0(2),
      R => '0'
    );
\valor_moneda[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000008088888808"
    )
        port map (
      I0 => \valor_moneda_reg[1]\,
      I1 => \valor_moneda_reg[1]_0\,
      I2 => sreg_0(2),
      I3 => sreg_0(1),
      I4 => sreg_0(0),
      I5 => \valor_moneda_reg[1]_1\,
      O => \sreg_reg[2]_0\(0)
    );
\valor_moneda[3]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => sreg_0(2),
      I1 => sreg_0(1),
      I2 => sreg_0(0),
      O => \sreg_reg[2]_1\
    );
\valor_moneda[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222220288888808"
    )
        port map (
      I0 => \valor_moneda_reg[1]\,
      I1 => \valor_moneda_reg[1]_0\,
      I2 => sreg_0(2),
      I3 => sreg_0(1),
      I4 => sreg_0(0),
      I5 => \valor_moneda_reg[1]_1\,
      O => \sreg_reg[2]_0\(1)
    );
\valor_moneda[6]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFEFEF00EFEFEFEF"
    )
        port map (
      I0 => sreg_0(0),
      I1 => sreg_0(1),
      I2 => sreg_0(2),
      I3 => sreg(0),
      I4 => sreg(1),
      I5 => sreg(2),
      O => \sreg_reg[0]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity edgedtctr_12 is
  port (
    sreg : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \sreg_reg[0]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \sreg_reg[2]_0\ : out STD_LOGIC;
    \sreg_reg[0]_1\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    \valor_moneda_reg[5]\ : in STD_LOGIC;
    \valor_moneda_reg[5]_0\ : in STD_LOGIC;
    \valor_moneda_reg[5]_1\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of edgedtctr_12 : entity is "edgedtctr";
end edgedtctr_12;

architecture STRUCTURE of edgedtctr_12 is
  signal \^sreg\ : STD_LOGIC_VECTOR ( 2 downto 0 );
begin
  sreg(2 downto 0) <= \^sreg\(2 downto 0);
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \sreg_reg[0]_1\,
      Q => \^sreg\(0),
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \^sreg\(0),
      Q => \^sreg\(1),
      R => '0'
    );
\sreg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \^sreg\(1),
      Q => \^sreg\(2),
      R => '0'
    );
\valor_moneda[2]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => \^sreg\(2),
      I1 => \^sreg\(1),
      I2 => \^sreg\(0),
      O => \sreg_reg[2]_0\
    );
\valor_moneda[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222220288888808"
    )
        port map (
      I0 => \valor_moneda_reg[5]\,
      I1 => \valor_moneda_reg[5]_0\,
      I2 => \^sreg\(2),
      I3 => \^sreg\(1),
      I4 => \^sreg\(0),
      I5 => \valor_moneda_reg[5]_1\,
      O => \sreg_reg[0]_0\(0)
    );
\valor_moneda[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000A8AA00000000"
    )
        port map (
      I0 => \valor_moneda_reg[5]_1\,
      I1 => \^sreg\(0),
      I2 => \^sreg\(1),
      I3 => \^sreg\(2),
      I4 => \valor_moneda_reg[5]_0\,
      I5 => \valor_moneda_reg[5]\,
      O => \sreg_reg[0]_0\(1)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity edgedtctr_13 is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    \sreg_reg[1]_0\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 );
    D : out STD_LOGIC_VECTOR ( 0 to 0 );
    \refresco__0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \sreg_reg[0]_1\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    \disp_next_reg[7][6]\ : in STD_LOGIC;
    \disp_next_reg[7][6]_0\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \FSM_sequential_current_state_reg[0]\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of edgedtctr_13 : entity is "edgedtctr";
end edgedtctr_13;

architecture STRUCTURE of edgedtctr_13 is
  signal \^q\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^sreg_reg[0]_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \disp_next_reg[2][4]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \disp_next_reg[3][3]_i_1\ : label is "soft_lutpair0";
begin
  Q(1 downto 0) <= \^q\(1 downto 0);
  \sreg_reg[0]_0\ <= \^sreg_reg[0]_0\;
\disp_next_reg[2][4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AA8A"
    )
        port map (
      I0 => \FSM_sequential_current_state_reg[0]\,
      I1 => \^sreg_reg[0]_0\,
      I2 => \^q\(1),
      I3 => \^q\(0),
      O => D(0)
    );
\disp_next_reg[3][3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => \^sreg_reg[0]_0\,
      O => \refresco__0\(0)
    );
\disp_next_reg[7][6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0404040404FF0404"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => \^sreg_reg[0]_0\,
      I3 => \disp_next_reg[7][6]\,
      I4 => \disp_next_reg[7][6]_0\(1),
      I5 => \disp_next_reg[7][6]_0\(0),
      O => \sreg_reg[1]_0\
    );
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \sreg_reg[0]_1\,
      Q => \^sreg_reg[0]_0\,
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \^sreg_reg[0]_0\,
      Q => \^q\(0),
      R => '0'
    );
\sreg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \^q\(0),
      Q => \^q\(1),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity edgedtctr_14 is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    \sreg_reg[1]_0\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \sreg_reg[1]_1\ : out STD_LOGIC;
    \refresco__0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \sreg_reg[0]_1\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    \disp_next_reg[5][0]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_next_reg[5][0]_0\ : in STD_LOGIC;
    \disp_next_reg[6][3]\ : in STD_LOGIC;
    \disp_next_reg[6][3]_0\ : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of edgedtctr_14 : entity is "edgedtctr";
end edgedtctr_14;

architecture STRUCTURE of edgedtctr_14 is
  signal \^q\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^sreg_reg[0]_0\ : STD_LOGIC;
begin
  Q(1 downto 0) <= \^q\(1 downto 0);
  \sreg_reg[0]_0\ <= \^sreg_reg[0]_0\;
\disp_next_reg[3][4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => \^sreg_reg[0]_0\,
      O => \refresco__0\(0)
    );
\disp_next_reg[5][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0404040404FF0404"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => \^sreg_reg[0]_0\,
      I3 => \disp_next_reg[5][0]\(0),
      I4 => \disp_next_reg[5][0]\(1),
      I5 => \disp_next_reg[5][0]_0\,
      O => \sreg_reg[1]_0\
    );
\disp_next_reg[6][3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0404040404FF0404"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => \^sreg_reg[0]_0\,
      I3 => \disp_next_reg[6][3]\,
      I4 => \disp_next_reg[6][3]_0\(1),
      I5 => \disp_next_reg[6][3]_0\(0),
      O => \sreg_reg[1]_1\
    );
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \sreg_reg[0]_1\,
      Q => \^sreg_reg[0]_0\,
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \^sreg_reg[0]_0\,
      Q => \^q\(0),
      R => '0'
    );
\sreg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \^q\(0),
      Q => \^q\(1),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity edgedtctr_15 is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 0 to 0 );
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \sreg_reg[1]_0\ : out STD_LOGIC;
    \sreg_reg[1]_1\ : out STD_LOGIC;
    \refresco__0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \sreg_reg[0]_1\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    \disp_next_reg[2][0]\ : in STD_LOGIC;
    \disp_next_reg[2][0]_0\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_next_reg[3][0]\ : in STD_LOGIC;
    \disp_next_reg[3][0]_0\ : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of edgedtctr_15 : entity is "edgedtctr";
end edgedtctr_15;

architecture STRUCTURE of edgedtctr_15 is
  signal \^q\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^sreg_reg[0]_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \disp_next_reg[1][6]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \disp_next_reg[5][4]_i_1\ : label is "soft_lutpair1";
begin
  Q(1 downto 0) <= \^q\(1 downto 0);
  \sreg_reg[0]_0\ <= \^sreg_reg[0]_0\;
\disp_next_reg[1][6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => \^sreg_reg[0]_0\,
      O => \sreg_reg[1]_1\
    );
\disp_next_reg[2][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FBFBFBFBFB00FBFB"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => \^sreg_reg[0]_0\,
      I3 => \disp_next_reg[2][0]\,
      I4 => \disp_next_reg[2][0]_0\(1),
      I5 => \disp_next_reg[2][0]_0\(0),
      O => D(0)
    );
\disp_next_reg[3][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FBFBFBFBFB00FBFB"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => \^sreg_reg[0]_0\,
      I3 => \disp_next_reg[3][0]\,
      I4 => \disp_next_reg[3][0]_0\(1),
      I5 => \disp_next_reg[3][0]_0\(0),
      O => \sreg_reg[1]_0\
    );
\disp_next_reg[5][4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \^sreg_reg[0]_0\,
      I1 => \^q\(1),
      I2 => \^q\(0),
      O => \refresco__0\(0)
    );
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \sreg_reg[0]_1\,
      Q => \^sreg_reg[0]_0\,
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \^sreg_reg[0]_0\,
      Q => \^q\(0),
      R => '0'
    );
\sreg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \^q\(0),
      Q => \^q\(1),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity edgedtctr_16 is
  port (
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \sreg_reg[1]_0\ : out STD_LOGIC;
    \sreg_reg[0]_0\ : out STD_LOGIC;
    \sreg_reg[0]_1\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    \refresco__0\ : in STD_LOGIC_VECTOR ( 2 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of edgedtctr_16 : entity is "edgedtctr";
end edgedtctr_16;

architecture STRUCTURE of edgedtctr_16 is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  signal \sreg_reg_n_0_[1]\ : STD_LOGIC;
  signal \sreg_reg_n_0_[2]\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_current_state[0]_i_2\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \disp_next_reg[5][6]_i_1\ : label is "soft_lutpair2";
begin
\FSM_sequential_current_state[0]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \sreg_reg_n_0_[0]\,
      I1 => \sreg_reg_n_0_[2]\,
      I2 => \sreg_reg_n_0_[1]\,
      O => \sreg_reg[0]_0\
    );
\disp_next_reg[5][6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => \sreg_reg_n_0_[1]\,
      I1 => \sreg_reg_n_0_[2]\,
      I2 => \sreg_reg_n_0_[0]\,
      O => \sreg_reg[1]_0\
    );
\disp_next_reg[7][6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000FB00FBFB04"
    )
        port map (
      I0 => \sreg_reg_n_0_[0]\,
      I1 => \sreg_reg_n_0_[2]\,
      I2 => \sreg_reg_n_0_[1]\,
      I3 => \refresco__0\(0),
      I4 => \refresco__0\(2),
      I5 => \refresco__0\(1),
      O => E(0)
    );
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \sreg_reg[0]_1\,
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg_n_0_[1]\,
      R => '0'
    );
\sreg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \sreg_reg_n_0_[1]\,
      Q => \sreg_reg_n_0_[2]\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity edgedtctr_17 is
  port (
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \sreg_reg[0]_0\ : out STD_LOGIC;
    sreg : out STD_LOGIC_VECTOR ( 0 to 0 );
    \FSM_onehot_current_state_reg[0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \sreg_reg[0]_1\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of edgedtctr_17 : entity is "edgedtctr";
end edgedtctr_17;

architecture STRUCTURE of edgedtctr_17 is
  signal \^d\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^sreg\ : STD_LOGIC_VECTOR ( 0 to 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_onehot_current_state[2]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \FSM_onehot_current_state[3]_i_4\ : label is "soft_lutpair3";
begin
  D(1 downto 0) <= \^d\(1 downto 0);
  sreg(0) <= \^sreg\(0);
\FSM_onehot_current_state[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000400"
    )
        port map (
      I0 => Q(0),
      I1 => Q(1),
      I2 => \^d\(0),
      I3 => \^sreg\(0),
      I4 => \^d\(1),
      O => \FSM_onehot_current_state_reg[0]\(0)
    );
\FSM_onehot_current_state[3]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => \^d\(0),
      I1 => \^sreg\(0),
      I2 => \^d\(1),
      O => \sreg_reg[0]_0\
    );
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \sreg_reg[0]_1\,
      Q => \^d\(0),
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \^d\(0),
      Q => \^d\(1),
      R => '0'
    );
\sreg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \^d\(1),
      Q => \^sreg\(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity edgedtctr_9 is
  port (
    \sreg_reg[2]_0\ : out STD_LOGIC;
    \sreg_reg[0]_0\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of edgedtctr_9 : entity is "edgedtctr";
end edgedtctr_9;

architecture STRUCTURE of edgedtctr_9 is
  signal sreg : STD_LOGIC_VECTOR ( 2 downto 0 );
begin
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \sreg_reg[0]_0\,
      Q => sreg(0),
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => sreg(0),
      Q => sreg(1),
      R => '0'
    );
\sreg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => sreg(1),
      Q => sreg(2),
      R => '0'
    );
\valor_moneda[6]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => sreg(2),
      I1 => sreg(1),
      I2 => sreg(0),
      O => \sreg_reg[2]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gestion_refresco is
  port (
    \disp_refresco[6]_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \sreg_reg[0]\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_refresco[1]_4\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \FSM_sequential_current_state_reg[0]\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[0]_0\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[1]\ : out STD_LOGIC;
    \reg_reg[1]\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[1]_0\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[0]_1\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[0]_2\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[1]_1\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[1]_2\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[0]_3\ : out STD_LOGIC;
    \reg_reg[1]_0\ : out STD_LOGIC;
    \sreg_reg[1]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \FSM_sequential_current_state_reg[0]_4\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[0]_5\ : out STD_LOGIC;
    \reg_reg[0]\ : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 5 downto 0 );
    \caracter[3]_i_7_0\ : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter[2]_i_17\ : in STD_LOGIC;
    \caracter[0]_i_5_0\ : in STD_LOGIC;
    \refresco__0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \caracter[6]_i_36_0\ : in STD_LOGIC;
    \caracter[6]_i_38\ : in STD_LOGIC;
    \caracter[6]_i_27\ : in STD_LOGIC;
    \caracter_reg[0]\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \caracter_reg[0]_0\ : in STD_LOGIC;
    \caracter_reg[4]\ : in STD_LOGIC;
    \caracter_reg[4]_0\ : in STD_LOGIC;
    \caracter_reg[0]_1\ : in STD_LOGIC;
    \caracter_reg[0]_2\ : in STD_LOGIC;
    \caracter_reg[0]_3\ : in STD_LOGIC;
    \caracter_reg[2]\ : in STD_LOGIC;
    \caracter_reg[1]\ : in STD_LOGIC;
    \caracter_reg[1]_0\ : in STD_LOGIC;
    \caracter_reg[1]_1\ : in STD_LOGIC;
    \caracter[6]_i_4_0\ : in STD_LOGIC;
    \caracter_reg[1]_2\ : in STD_LOGIC;
    \caracter[1]_i_9\ : in STD_LOGIC;
    \caracter_reg[3]\ : in STD_LOGIC;
    \caracter_reg[3]_0\ : in STD_LOGIC;
    \caracter_reg[1]_3\ : in STD_LOGIC;
    \disp_dinero[3]_6\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter_reg[2]_i_2_0\ : in STD_LOGIC;
    \caracter[0]_i_3_0\ : in STD_LOGIC;
    \caracter[0]_i_3_1\ : in STD_LOGIC;
    \caracter[0]_i_3_2\ : in STD_LOGIC;
    \caracter[1]_i_2_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter[4]_i_2_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter[2]_i_3\ : in STD_LOGIC;
    \caracter[3]_i_3_0\ : in STD_LOGIC;
    \caracter[3]_i_3_1\ : in STD_LOGIC;
    \caracter[1]_i_2_1\ : in STD_LOGIC;
    \caracter[6]_i_20\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter[6]_i_4_1\ : in STD_LOGIC;
    \caracter_reg[1]_i_6_0\ : in STD_LOGIC;
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter_reg[4]_1\ : in STD_LOGIC;
    \caracter_reg[0]_4\ : in STD_LOGIC;
    \caracter_reg[2]_0\ : in STD_LOGIC;
    \caracter[4]_i_4_0\ : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
end gestion_refresco;

architecture STRUCTURE of gestion_refresco is
  signal \^fsm_sequential_current_state_reg[0]\ : STD_LOGIC;
  signal \^fsm_sequential_current_state_reg[0]_0\ : STD_LOGIC;
  signal \^fsm_sequential_current_state_reg[0]_1\ : STD_LOGIC;
  signal \^fsm_sequential_current_state_reg[0]_2\ : STD_LOGIC;
  signal \^fsm_sequential_current_state_reg[1]\ : STD_LOGIC;
  signal \^fsm_sequential_current_state_reg[1]_0\ : STD_LOGIC;
  signal \^fsm_sequential_current_state_reg[1]_1\ : STD_LOGIC;
  signal \caracter[0]_i_3_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_4_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_7_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_11_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_13_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_2_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_3_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_7_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_8_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_16_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_4_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_6_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_7_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_2_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_3_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_8_n_0\ : STD_LOGIC;
  signal \caracter[4]_i_10_n_0\ : STD_LOGIC;
  signal \caracter[4]_i_2_n_0\ : STD_LOGIC;
  signal \caracter[4]_i_4_n_0\ : STD_LOGIC;
  signal \caracter[4]_i_9_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_14_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_15_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_16_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_17_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_21_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_23_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_25_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_33_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_39_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_3_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_5_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_7_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_6_n_0\ : STD_LOGIC;
  signal \caracter_reg[2]_i_2_n_0\ : STD_LOGIC;
  signal \caracter_reg[4]_i_5_n_0\ : STD_LOGIC;
  signal \^disp_refresco[1]_4\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \disp_refresco[2]_5\ : STD_LOGIC_VECTOR ( 4 to 4 );
  signal \disp_refresco[3]_2\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \disp_refresco[5]_3\ : STD_LOGIC_VECTOR ( 6 to 6 );
  signal \^disp_refresco[6]_1\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \disp_refresco[7]_0\ : STD_LOGIC_VECTOR ( 6 to 6 );
  signal \^reg_reg[1]\ : STD_LOGIC;
  signal \^sreg_reg[0]\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \caracter[0]_i_2\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \caracter[0]_i_5\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \caracter[0]_i_8\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \caracter[1]_i_7\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \caracter[1]_i_8\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \caracter[2]_i_10\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \caracter[2]_i_4\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \caracter[3]_i_4\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \caracter[3]_i_7\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \caracter[3]_i_9\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \caracter[6]_i_14\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \caracter[6]_i_7\ : label is "soft_lutpair46";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \disp_next_reg[1][6]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \disp_next_reg[2][0]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \disp_next_reg[2][4]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \disp_next_reg[3][0]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \disp_next_reg[3][3]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \disp_next_reg[3][4]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \disp_next_reg[5][0]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \disp_next_reg[5][4]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \disp_next_reg[5][6]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \disp_next_reg[6][3]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \disp_next_reg[7][6]\ : label is "LD";
begin
  \FSM_sequential_current_state_reg[0]\ <= \^fsm_sequential_current_state_reg[0]\;
  \FSM_sequential_current_state_reg[0]_0\ <= \^fsm_sequential_current_state_reg[0]_0\;
  \FSM_sequential_current_state_reg[0]_1\ <= \^fsm_sequential_current_state_reg[0]_1\;
  \FSM_sequential_current_state_reg[0]_2\ <= \^fsm_sequential_current_state_reg[0]_2\;
  \FSM_sequential_current_state_reg[1]\ <= \^fsm_sequential_current_state_reg[1]\;
  \FSM_sequential_current_state_reg[1]_0\ <= \^fsm_sequential_current_state_reg[1]_0\;
  \FSM_sequential_current_state_reg[1]_1\ <= \^fsm_sequential_current_state_reg[1]_1\;
  \disp_refresco[1]_4\(0) <= \^disp_refresco[1]_4\(0);
  \disp_refresco[6]_1\(0) <= \^disp_refresco[6]_1\(0);
  \reg_reg[1]\ <= \^reg_reg[1]\;
  \sreg_reg[0]\(1 downto 0) <= \^sreg_reg[0]\(1 downto 0);
\caracter[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFF10151015"
    )
        port map (
      I0 => CO(0),
      I1 => \^fsm_sequential_current_state_reg[0]\,
      I2 => \caracter_reg[0]_4\,
      I3 => \caracter[0]_i_3_n_0\,
      I4 => \caracter[0]_i_4_n_0\,
      I5 => \^reg_reg[1]\,
      O => D(0)
    );
\caracter[0]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"DF"
    )
        port map (
      I0 => \caracter_reg[1]_3\,
      I1 => \caracter_reg[1]_2\,
      I2 => \disp_refresco[3]_2\(3),
      O => \^fsm_sequential_current_state_reg[0]\
    );
\caracter[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^fsm_sequential_current_state_reg[0]_0\,
      I1 => \caracter_reg[0]_0\,
      I2 => \caracter_reg[4]\,
      I3 => \caracter[0]_i_7_n_0\,
      I4 => \caracter_reg[4]_0\,
      I5 => \^fsm_sequential_current_state_reg[1]\,
      O => \caracter[0]_i_3_n_0\
    );
\caracter[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFEA832ABCEA802A"
    )
        port map (
      I0 => \caracter_reg[0]\,
      I1 => Q(1),
      I2 => Q(0),
      I3 => Q(2),
      I4 => \^fsm_sequential_current_state_reg[0]\,
      I5 => \caracter[6]_i_7_n_0\,
      O => \caracter[0]_i_4_n_0\
    );
\caracter[0]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"02A2"
    )
        port map (
      I0 => \caracter_reg[1]_3\,
      I1 => \disp_refresco[3]_2\(0),
      I2 => \caracter_reg[1]_2\,
      I3 => \disp_dinero[3]_6\(0),
      O => \^fsm_sequential_current_state_reg[0]_0\
    );
\caracter[0]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \caracter_reg[1]_2\,
      I1 => \caracter[0]_i_3_0\,
      I2 => \caracter[0]_i_3_1\,
      I3 => \^fsm_sequential_current_state_reg[0]\,
      I4 => \caracter[0]_i_3_2\,
      I5 => \caracter[6]_i_7_n_0\,
      O => \caracter[0]_i_7_n_0\
    );
\caracter[0]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \caracter_reg[1]_2\,
      I1 => \caracter_reg[1]_3\,
      I2 => \^disp_refresco[1]_4\(0),
      O => \^fsm_sequential_current_state_reg[1]\
    );
\caracter[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF101500001015"
    )
        port map (
      I0 => CO(0),
      I1 => \caracter[6]_i_7_n_0\,
      I2 => \caracter_reg[0]_4\,
      I3 => \caracter[1]_i_2_n_0\,
      I4 => \^reg_reg[1]\,
      I5 => \caracter[1]_i_3_n_0\,
      O => D(1)
    );
\caracter[1]_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \caracter_reg[1]_3\,
      I1 => \disp_refresco[3]_2\(0),
      O => \caracter[1]_i_11_n_0\
    );
\caracter[1]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF413CFFFF7D3C"
    )
        port map (
      I0 => \disp_refresco[5]_3\(6),
      I1 => Q(0),
      I2 => \caracter_reg[1]_i_6_0\,
      I3 => \caracter_reg[1]_3\,
      I4 => \caracter_reg[1]_2\,
      I5 => \disp_refresco[3]_2\(3),
      O => \caracter[1]_i_13_n_0\
    );
\caracter[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^fsm_sequential_current_state_reg[1]_0\,
      I1 => \caracter_reg[1]_1\,
      I2 => \caracter_reg[4]\,
      I3 => \caracter_reg[1]_i_6_n_0\,
      I4 => \caracter_reg[4]_0\,
      I5 => \caracter[1]_i_7_n_0\,
      O => \caracter[1]_i_2_n_0\
    );
\caracter[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"303F303F50505F5F"
    )
        port map (
      I0 => \^fsm_sequential_current_state_reg[0]\,
      I1 => \caracter[1]_i_8_n_0\,
      I2 => \caracter_reg[2]\,
      I3 => \caracter[6]_i_7_n_0\,
      I4 => \caracter_reg[1]\,
      I5 => \caracter_reg[1]_0\,
      O => \caracter[1]_i_3_n_0\
    );
\caracter[1]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A022"
    )
        port map (
      I0 => \caracter_reg[1]_3\,
      I1 => \^sreg_reg[0]\(1),
      I2 => \caracter[1]_i_2_0\(0),
      I3 => \caracter_reg[1]_2\,
      O => \caracter[1]_i_7_n_0\
    );
\caracter[1]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"CE"
    )
        port map (
      I0 => \caracter_reg[1]_3\,
      I1 => \caracter_reg[1]_2\,
      I2 => \disp_refresco[5]_3\(6),
      O => \caracter[1]_i_8_n_0\
    );
\caracter[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C5C5C0C5C0C0C0C5"
    )
        port map (
      I0 => CO(0),
      I1 => \caracter_reg[2]_i_2_n_0\,
      I2 => \^reg_reg[1]\,
      I3 => \caracter_reg[2]_0\,
      I4 => \caracter_reg[0]_4\,
      I5 => \caracter[2]_i_4_n_0\,
      O => D(2)
    );
\caracter[2]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"04C4"
    )
        port map (
      I0 => \^sreg_reg[0]\(1),
      I1 => \caracter_reg[1]_3\,
      I2 => \caracter_reg[1]_2\,
      I3 => \caracter[1]_i_2_0\(0),
      O => \FSM_sequential_current_state_reg[0]_3\
    );
\caracter[2]_i_16\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000C3C3BE82FFFF"
    )
        port map (
      I0 => \^disp_refresco[6]_1\(0),
      I1 => Q(0),
      I2 => \caracter_reg[1]_i_6_0\,
      I3 => \disp_refresco[3]_2\(3),
      I4 => \caracter_reg[1]_3\,
      I5 => \caracter_reg[1]_2\,
      O => \caracter[2]_i_16_n_0\
    );
\caracter[2]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^sreg_reg[0]\(1),
      I1 => \caracter_reg[1]_3\,
      I2 => \caracter_reg[1]_2\,
      O => \caracter[2]_i_4_n_0\
    );
\caracter[2]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08FF0800"
    )
        port map (
      I0 => \^sreg_reg[0]\(1),
      I1 => \caracter_reg[1]_3\,
      I2 => \caracter_reg[1]_2\,
      I3 => \caracter_reg[1]_0\,
      I4 => \caracter_reg[2]_i_2_0\,
      O => \caracter[2]_i_6_n_0\
    );
\caracter[2]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00CCB8FF"
    )
        port map (
      I0 => \^disp_refresco[6]_1\(0),
      I1 => \caracter_reg[1]_0\,
      I2 => \disp_refresco[3]_2\(3),
      I3 => \caracter_reg[1]_3\,
      I4 => \caracter_reg[1]_2\,
      O => \caracter[2]_i_7_n_0\
    );
\caracter[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8888888B"
    )
        port map (
      I0 => \caracter[3]_i_2_n_0\,
      I1 => \^reg_reg[1]\,
      I2 => CO(0),
      I3 => \caracter[3]_i_3_n_0\,
      I4 => \caracter_reg[0]_4\,
      O => D(3)
    );
\caracter[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0F000033AA0F0F0F"
    )
        port map (
      I0 => \^fsm_sequential_current_state_reg[0]_1\,
      I1 => \^fsm_sequential_current_state_reg[0]\,
      I2 => \caracter_reg[3]\,
      I3 => Q(1),
      I4 => Q(0),
      I5 => Q(2),
      O => \caracter[3]_i_2_n_0\
    );
\caracter[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \caracter_reg[3]_0\,
      I1 => \^fsm_sequential_current_state_reg[0]_2\,
      I2 => \caracter_reg[4]\,
      I3 => \caracter[3]_i_8_n_0\,
      I4 => \caracter_reg[4]_0\,
      I5 => \^fsm_sequential_current_state_reg[1]_1\,
      O => \caracter[3]_i_3_n_0\
    );
\caracter[3]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^disp_refresco[6]_1\(0),
      I1 => \caracter_reg[1]_3\,
      I2 => \caracter_reg[1]_2\,
      O => \^fsm_sequential_current_state_reg[0]_1\
    );
\caracter[3]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"57F7"
    )
        port map (
      I0 => \caracter_reg[1]_3\,
      I1 => \disp_refresco[7]_0\(6),
      I2 => \caracter_reg[1]_2\,
      I3 => \caracter[6]_i_20\(0),
      O => \^fsm_sequential_current_state_reg[0]_2\
    );
\caracter[3]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0AFEFEFA0AFE0E0"
    )
        port map (
      I0 => \caracter[3]_i_3_0\,
      I1 => \caracter[3]_i_3_1\,
      I2 => \caracter[0]_i_3_1\,
      I3 => \^fsm_sequential_current_state_reg[0]_1\,
      I4 => \caracter[0]_i_3_2\,
      I5 => \^fsm_sequential_current_state_reg[0]\,
      O => \caracter[3]_i_8_n_0\
    );
\caracter[3]_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D0"
    )
        port map (
      I0 => \^sreg_reg[0]\(1),
      I1 => \caracter_reg[1]_2\,
      I2 => \caracter_reg[1]_3\,
      O => \^fsm_sequential_current_state_reg[1]_1\
    );
\caracter[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8B888B8B"
    )
        port map (
      I0 => \caracter[4]_i_2_n_0\,
      I1 => \^reg_reg[1]\,
      I2 => CO(0),
      I3 => \caracter_reg[4]_1\,
      I4 => \caracter[4]_i_4_n_0\,
      O => D(4)
    );
\caracter[4]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"30BB003330880033"
    )
        port map (
      I0 => \disp_refresco[3]_2\(4),
      I1 => Q(0),
      I2 => \caracter[1]_i_2_0\(0),
      I3 => \caracter_reg[1]_2\,
      I4 => \caracter_reg[1]_3\,
      I5 => \^sreg_reg[0]\(1),
      O => \caracter[4]_i_10_n_0\
    );
\caracter[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AA03030000AAAAAA"
    )
        port map (
      I0 => \caracter_reg[4]_i_5_n_0\,
      I1 => \caracter_reg[1]_2\,
      I2 => \caracter_reg[1]_3\,
      I3 => Q(0),
      I4 => Q(1),
      I5 => Q(2),
      O => \caracter[4]_i_2_n_0\
    );
\caracter[4]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"55DD577777DD5777"
    )
        port map (
      I0 => \caracter_reg[4]\,
      I1 => \caracter_reg[1]_2\,
      I2 => \disp_refresco[2]_5\(4),
      I3 => \caracter_reg[1]_3\,
      I4 => \caracter_reg[4]_0\,
      I5 => \disp_refresco[3]_2\(4),
      O => \caracter[4]_i_4_n_0\
    );
\caracter[4]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"30BB003330880033"
    )
        port map (
      I0 => \disp_refresco[3]_2\(4),
      I1 => \caracter[0]_i_3_2\,
      I2 => \caracter[1]_i_2_0\(0),
      I3 => \caracter_reg[1]_2\,
      I4 => \caracter_reg[1]_3\,
      I5 => \^sreg_reg[0]\(1),
      O => \FSM_sequential_current_state_reg[1]_2\
    );
\caracter[4]_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFCCB833"
    )
        port map (
      I0 => \disp_refresco[2]_5\(4),
      I1 => Q(0),
      I2 => \disp_refresco[3]_2\(4),
      I3 => \caracter_reg[1]_3\,
      I4 => \caracter_reg[1]_2\,
      O => \caracter[4]_i_9_n_0\
    );
\caracter[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C0C0C0C5C5C5C0C5"
    )
        port map (
      I0 => CO(0),
      I1 => \caracter[6]_i_3_n_0\,
      I2 => \^reg_reg[1]\,
      I3 => \caracter[6]_i_5_n_0\,
      I4 => \caracter_reg[0]_4\,
      I5 => \caracter[6]_i_7_n_0\,
      O => D(5)
    );
\caracter[6]_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[1]_3\,
      I1 => \caracter_reg[1]_2\,
      I2 => \disp_refresco[5]_3\(6),
      O => \caracter[6]_i_14_n_0\
    );
\caracter[6]_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3F3F3F3B3F333F3B"
    )
        port map (
      I0 => \^disp_refresco[1]_4\(0),
      I1 => \caracter_reg[1]_3\,
      I2 => \caracter_reg[1]_2\,
      I3 => Q(0),
      I4 => Q(1),
      I5 => \disp_refresco[5]_3\(6),
      O => \caracter[6]_i_15_n_0\
    );
\caracter[6]_i_16\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000B000000F30"
    )
        port map (
      I0 => \caracter[2]_i_4_n_0\,
      I1 => Q(1),
      I2 => \caracter[6]_i_25_n_0\,
      I3 => Q(0),
      I4 => \caracter[6]_i_4_1\,
      I5 => \disp_refresco[7]_0\(6),
      O => \caracter[6]_i_16_n_0\
    );
\caracter[6]_i_17\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCAF0CAFFCA00CA0"
    )
        port map (
      I0 => \^fsm_sequential_current_state_reg[1]_0\,
      I1 => \caracter[6]_i_4_0\,
      I2 => Q(0),
      I3 => Q(1),
      I4 => \caracter[1]_i_7_n_0\,
      I5 => \caracter_reg[1]_1\,
      O => \caracter[6]_i_17_n_0\
    );
\caracter[6]_i_21\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FBBAFBBFAFFFFFFF"
    )
        port map (
      I0 => \caracter[6]_i_33_n_0\,
      I1 => \caracter[6]_i_7_n_0\,
      I2 => Q(0),
      I3 => Q(1),
      I4 => \caracter[6]_i_14_n_0\,
      I5 => Q(2),
      O => \caracter[6]_i_21_n_0\
    );
\caracter[6]_i_23\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF051200FF051700"
    )
        port map (
      I0 => \caracter[0]_i_3_1\,
      I1 => \disp_refresco[5]_3\(6),
      I2 => \caracter[0]_i_3_2\,
      I3 => \caracter_reg[1]_3\,
      I4 => \caracter_reg[1]_2\,
      I5 => \disp_refresco[7]_0\(6),
      O => \caracter[6]_i_23_n_0\
    );
\caracter[6]_i_25\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \caracter_reg[1]_3\,
      I1 => \disp_refresco[3]_2\(3),
      O => \caracter[6]_i_25_n_0\
    );
\caracter[6]_i_29\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FDFFFFFFFF0F0000"
    )
        port map (
      I0 => \caracter_reg[1]_3\,
      I1 => \^disp_refresco[1]_4\(0),
      I2 => \caracter_reg[1]_2\,
      I3 => Q(0),
      I4 => Q(1),
      I5 => Q(2),
      O => \FSM_sequential_current_state_reg[0]_4\
    );
\caracter[6]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"D333DFFF10001CCC"
    )
        port map (
      I0 => \caracter[6]_i_14_n_0\,
      I1 => Q(2),
      I2 => Q(1),
      I3 => Q(0),
      I4 => \caracter[6]_i_7_n_0\,
      I5 => \caracter[6]_i_15_n_0\,
      O => \caracter[6]_i_3_n_0\
    );
\caracter[6]_i_31\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF1000A282"
    )
        port map (
      I0 => Q(0),
      I1 => \caracter_reg[1]_2\,
      I2 => \caracter_reg[1]_3\,
      I3 => \disp_refresco[3]_2\(4),
      I4 => Q(1),
      I5 => \caracter[6]_i_39_n_0\,
      O => \reg_reg[0]\
    );
\caracter[6]_i_33\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFF43FFFFFF73FFF"
    )
        port map (
      I0 => \disp_refresco[3]_2\(3),
      I1 => Q(0),
      I2 => Q(1),
      I3 => \caracter_reg[1]_2\,
      I4 => \caracter_reg[1]_3\,
      I5 => \^disp_refresco[6]_1\(0),
      O => \caracter[6]_i_33_n_0\
    );
\caracter[6]_i_36\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"70000070"
    )
        port map (
      I0 => \^disp_refresco[6]_1\(0),
      I1 => \disp_refresco[5]_3\(6),
      I2 => \caracter_reg[1]_3\,
      I3 => Q(0),
      I4 => Q(1),
      O => \FSM_sequential_current_state_reg[0]_5\
    );
\caracter[6]_i_39\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00003F0F0000ECEC"
    )
        port map (
      I0 => \disp_refresco[2]_5\(4),
      I1 => \caracter_reg[1]_2\,
      I2 => \caracter_reg[1]_3\,
      I3 => \^disp_refresco[1]_4\(0),
      I4 => Q(1),
      I5 => Q(0),
      O => \caracter[6]_i_39_n_0\
    );
\caracter[6]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000AB00ABABABAB"
    )
        port map (
      I0 => \caracter[6]_i_16_n_0\,
      I1 => \caracter[6]_i_17_n_0\,
      I2 => \caracter_reg[0]_1\,
      I3 => \caracter_reg[0]_2\,
      I4 => \caracter_reg[0]_3\,
      I5 => \caracter[6]_i_21_n_0\,
      O => \^reg_reg[1]\
    );
\caracter[6]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EA404040EAE54040"
    )
        port map (
      I0 => \caracter_reg[4]\,
      I1 => \caracter[6]_i_23_n_0\,
      I2 => \caracter_reg[4]_0\,
      I3 => \caracter_reg[1]_2\,
      I4 => \caracter_reg[1]_3\,
      I5 => \^disp_refresco[1]_4\(0),
      O => \caracter[6]_i_5_n_0\
    );
\caracter[6]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"CE"
    )
        port map (
      I0 => \caracter_reg[1]_3\,
      I1 => \caracter_reg[1]_2\,
      I2 => \disp_refresco[7]_0\(6),
      O => \caracter[6]_i_7_n_0\
    );
\caracter_reg[1]_i_4\: unisim.vcomponents.MUXF7
     port map (
      I0 => \caracter[1]_i_11_n_0\,
      I1 => \caracter[1]_i_9\,
      O => \^fsm_sequential_current_state_reg[1]_0\,
      S => \caracter_reg[1]_2\
    );
\caracter_reg[1]_i_6\: unisim.vcomponents.MUXF7
     port map (
      I0 => \caracter[1]_i_13_n_0\,
      I1 => \caracter[1]_i_2_1\,
      O => \caracter_reg[1]_i_6_n_0\,
      S => \caracter[0]_i_3_1\
    );
\caracter_reg[2]_i_11\: unisim.vcomponents.MUXF7
     port map (
      I0 => \caracter[2]_i_16_n_0\,
      I1 => \caracter[2]_i_3\,
      O => \reg_reg[1]_0\,
      S => \caracter[0]_i_3_1\
    );
\caracter_reg[2]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \caracter[2]_i_6_n_0\,
      I1 => \caracter[2]_i_7_n_0\,
      O => \caracter_reg[2]_i_2_n_0\,
      S => \caracter_reg[2]\
    );
\caracter_reg[4]_i_5\: unisim.vcomponents.MUXF7
     port map (
      I0 => \caracter[4]_i_9_n_0\,
      I1 => \caracter[4]_i_10_n_0\,
      O => \caracter_reg[4]_i_5_n_0\,
      S => \caracter[4]_i_2_0\(0)
    );
\disp_next_reg[1][6]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \caracter[6]_i_38\,
      G => E(0),
      GE => '1',
      Q => \^disp_refresco[1]_4\(0)
    );
\disp_next_reg[2][0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \caracter[4]_i_4_0\(0),
      G => E(0),
      GE => '1',
      Q => \sreg_reg[1]\(0)
    );
\disp_next_reg[2][4]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \caracter[4]_i_4_0\(1),
      G => E(0),
      GE => '1',
      Q => \disp_refresco[2]_5\(4)
    );
\disp_next_reg[3][0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \caracter[0]_i_5_0\,
      G => E(0),
      GE => '1',
      Q => \disp_refresco[3]_2\(0)
    );
\disp_next_reg[3][3]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \refresco__0\(2),
      G => E(0),
      GE => '1',
      Q => \disp_refresco[3]_2\(3)
    );
\disp_next_reg[3][4]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \refresco__0\(1),
      G => E(0),
      GE => '1',
      Q => \disp_refresco[3]_2\(4)
    );
\disp_next_reg[5][0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \caracter[6]_i_27\,
      G => E(0),
      GE => '1',
      Q => \^sreg_reg[0]\(0)
    );
\disp_next_reg[5][4]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \refresco__0\(0),
      G => E(0),
      GE => '1',
      Q => \^sreg_reg[0]\(1)
    );
\disp_next_reg[5][6]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \caracter[6]_i_36_0\,
      G => E(0),
      GE => '1',
      Q => \disp_refresco[5]_3\(6)
    );
\disp_next_reg[6][3]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \caracter[2]_i_17\,
      G => E(0),
      GE => '1',
      Q => \^disp_refresco[6]_1\(0)
    );
\disp_next_reg[7][6]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \caracter[3]_i_7_0\,
      G => E(0),
      GE => '1',
      Q => \disp_refresco[7]_0\(6)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity int_to_string is
  port (
    \FSM_sequential_current_state_reg[1]\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[1]_0\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[1]_1\ : out STD_LOGIC;
    \reg_reg[1]\ : out STD_LOGIC;
    \reg_reg[0]\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[1]_2\ : out STD_LOGIC;
    \reg_reg[0]_0\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[0]\ : out STD_LOGIC;
    \reg_reg[0]_1\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[1]_3\ : out STD_LOGIC;
    \reg_reg[1]_0\ : out STD_LOGIC;
    \total_out_reg[5]\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[0]_0\ : out STD_LOGIC;
    \disp_dinero[3]_6\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter_reg[2]_i_15_0\ : out STD_LOGIC;
    \caracter[6]_i_38_0\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \caracter[0]_i_4\ : in STD_LOGIC;
    \caracter[6]_i_4\ : in STD_LOGIC;
    \caracter_reg[1]_i_4\ : in STD_LOGIC;
    \caracter[0]_i_3\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter[1]_i_3\ : in STD_LOGIC;
    \caracter[1]_i_3_0\ : in STD_LOGIC;
    \caracter[1]_i_3_1\ : in STD_LOGIC;
    \caracter[6]_i_4_0\ : in STD_LOGIC;
    \caracter[6]_i_4_1\ : in STD_LOGIC;
    \caracter[6]_i_4_2\ : in STD_LOGIC;
    \caracter[3]_i_2\ : in STD_LOGIC;
    \caracter_reg[2]\ : in STD_LOGIC;
    \caracter_reg[2]_0\ : in STD_LOGIC;
    \caracter_reg[2]_1\ : in STD_LOGIC;
    \caracter_reg[2]_2\ : in STD_LOGIC;
    \caracter[2]_i_6\ : in STD_LOGIC;
    \caracter[2]_i_6_0\ : in STD_LOGIC;
    \caracter[6]_i_18_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_refresco[1]_4\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_refresco[5]_3\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \caracter_reg[3]_i_141_0\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    \caracter[3]_i_3\ : in STD_LOGIC;
    \caracter[3]_i_3_0\ : in STD_LOGIC
  );
end int_to_string;

architecture STRUCTURE of int_to_string is
  signal \^fsm_sequential_current_state_reg[0]\ : STD_LOGIC;
  signal \^fsm_sequential_current_state_reg[1]_0\ : STD_LOGIC;
  signal \^fsm_sequential_current_state_reg[1]_1\ : STD_LOGIC;
  signal \^fsm_sequential_current_state_reg[1]_2\ : STD_LOGIC;
  signal aD2M4dsP : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \caracter[0]_i_101_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_102_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_104_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_105_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_106_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_107_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_108_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_109_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_110_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_111_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_116_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_117_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_118_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_119_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_11_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_120_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_121_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_122_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_123_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_125_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_126_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_127_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_128_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_130_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_131_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_132_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_133_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_134_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_135_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_136_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_137_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_139_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_140_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_141_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_142_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_143_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_144_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_145_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_146_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_147_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_149_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_150_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_151_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_152_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_153_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_154_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_155_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_156_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_158_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_159_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_15_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_161_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_164_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_165_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_166_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_167_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_168_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_169_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_16_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_170_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_171_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_172_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_173_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_174_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_175_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_176_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_177_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_178_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_179_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_17_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_180_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_181_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_182_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_187_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_188_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_189_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_18_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_190_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_191_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_192_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_193_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_194_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_195_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_196_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_197_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_198_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_203_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_204_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_205_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_206_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_207_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_208_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_209_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_210_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_211_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_212_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_213_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_214_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_215_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_216_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_217_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_218_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_219_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_220_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_221_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_222_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_223_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_224_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_225_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_226_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_227_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_228_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_229_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_230_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_231_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_232_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_233_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_234_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_235_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_236_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_237_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_238_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_240_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_241_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_242_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_243_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_244_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_245_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_246_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_247_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_251_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_252_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_253_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_254_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_255_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_256_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_258_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_259_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_260_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_261_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_262_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_263_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_264_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_265_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_268_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_269_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_26_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_270_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_271_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_272_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_273_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_274_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_275_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_277_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_278_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_279_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_27_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_280_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_281_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_282_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_283_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_284_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_290_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_291_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_292_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_293_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_294_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_295_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_296_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_297_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_299_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_300_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_302_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_304_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_306_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_307_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_308_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_309_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_310_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_311_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_312_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_313_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_314_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_315_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_316_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_317_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_319_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_31_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_320_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_321_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_322_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_323_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_324_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_325_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_326_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_327_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_328_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_329_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_32_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_330_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_331_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_332_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_333_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_334_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_335_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_336_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_337_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_338_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_339_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_33_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_340_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_341_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_347_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_348_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_349_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_350_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_351_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_352_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_353_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_354_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_356_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_357_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_358_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_359_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_35_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_360_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_361_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_362_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_363_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_365_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_366_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_367_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_368_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_369_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_36_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_370_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_372_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_373_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_374_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_375_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_377_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_378_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_379_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_37_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_380_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_381_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_382_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_383_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_384_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_386_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_387_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_388_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_389_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_38_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_390_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_391_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_392_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_393_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_395_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_396_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_397_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_398_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_399_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_39_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_400_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_401_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_402_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_405_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_406_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_407_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_408_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_409_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_40_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_410_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_411_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_412_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_413_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_415_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_417_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_418_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_419_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_41_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_420_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_421_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_422_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_423_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_425_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_426_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_428_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_429_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_42_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_430_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_431_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_432_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_433_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_434_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_435_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_437_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_438_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_440_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_445_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_446_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_447_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_448_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_449_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_44_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_450_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_451_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_452_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_453_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_454_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_455_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_456_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_457_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_458_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_459_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_45_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_460_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_461_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_462_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_463_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_464_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_465_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_466_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_467_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_468_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_469_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_46_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_470_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_471_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_472_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_473_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_474_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_475_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_478_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_479_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_47_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_480_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_481_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_482_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_483_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_484_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_485_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_487_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_488_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_489_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_48_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_490_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_491_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_492_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_493_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_494_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_495_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_496_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_497_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_498_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_499_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_49_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_500_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_501_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_503_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_504_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_505_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_506_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_507_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_508_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_509_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_50_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_510_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_511_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_512_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_513_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_515_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_516_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_517_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_518_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_519_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_51_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_520_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_521_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_522_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_524_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_525_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_526_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_527_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_528_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_529_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_530_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_531_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_533_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_534_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_535_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_536_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_537_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_538_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_539_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_53_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_540_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_543_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_544_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_545_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_546_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_547_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_548_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_549_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_54_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_550_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_551_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_552_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_553_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_558_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_559_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_55_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_560_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_561_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_562_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_563_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_564_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_565_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_566_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_567_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_568_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_569_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_56_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_570_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_571_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_572_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_573_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_574_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_575_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_576_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_577_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_579_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_57_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_580_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_581_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_582_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_583_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_584_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_585_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_586_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_587_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_588_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_589_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_58_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_590_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_592_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_593_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_594_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_595_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_596_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_597_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_598_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_599_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_59_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_600_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_601_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_602_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_603_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_604_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_605_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_606_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_607_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_60_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_610_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_611_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_612_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_613_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_614_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_616_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_617_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_618_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_619_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_620_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_621_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_622_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_623_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_624_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_625_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_626_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_627_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_628_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_629_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_630_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_631_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_632_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_633_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_634_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_635_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_636_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_637_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_638_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_63_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_641_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_643_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_645_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_646_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_647_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_648_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_649_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_64_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_650_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_651_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_652_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_653_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_654_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_655_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_656_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_657_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_65_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_662_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_663_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_664_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_665_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_667_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_668_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_669_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_66_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_670_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_671_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_672_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_673_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_674_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_675_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_676_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_677_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_679_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_680_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_681_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_682_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_683_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_684_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_685_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_686_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_687_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_688_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_689_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_68_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_690_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_691_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_692_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_693_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_694_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_695_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_696_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_697_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_698_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_699_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_69_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_700_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_701_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_702_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_703_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_704_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_709_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_70_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_710_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_711_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_712_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_713_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_714_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_715_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_716_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_717_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_719_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_71_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_721_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_722_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_723_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_724_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_725_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_726_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_728_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_729_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_72_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_730_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_731_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_732_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_733_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_734_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_735_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_736_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_737_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_738_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_739_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_73_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_740_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_741_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_742_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_743_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_74_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_75_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_79_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_80_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_81_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_82_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_83_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_84_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_85_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_86_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_91_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_92_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_94_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_96_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_98_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_15_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_100_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_101_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_102_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_103_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_104_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_105_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_106_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_107_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_108_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_109_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_110_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_111_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_112_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_113_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_114_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_116_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_117_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_118_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_119_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_121_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_122_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_123_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_124_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_125_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_126_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_127_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_128_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_129_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_130_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_131_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_132_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_133_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_134_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_135_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_136_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_138_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_139_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_13_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_140_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_141_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_142_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_143_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_144_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_145_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_147_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_148_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_149_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_150_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_153_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_154_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_155_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_156_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_157_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_158_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_159_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_160_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_161_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_162_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_163_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_164_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_165_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_167_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_168_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_169_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_170_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_171_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_172_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_173_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_174_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_177_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_178_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_179_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_180_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_181_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_182_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_183_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_184_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_187_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_188_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_189_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_190_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_191_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_192_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_193_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_194_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_197_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_198_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_199_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_19_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_200_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_201_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_202_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_203_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_204_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_205_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_206_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_207_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_208_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_209_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_20_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_210_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_211_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_212_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_213_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_214_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_215_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_216_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_217_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_218_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_219_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_21_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_220_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_221_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_222_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_223_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_224_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_226_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_227_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_228_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_229_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_22_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_230_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_231_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_232_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_234_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_235_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_236_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_237_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_23_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_26_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_27_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_28_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_29_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_30_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_38_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_39_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_41_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_44_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_45_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_46_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_47_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_48_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_49_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_50_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_51_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_52_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_53_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_54_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_55_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_56_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_57_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_58_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_59_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_60_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_62_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_63_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_64_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_65_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_66_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_67_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_68_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_69_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_76_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_77_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_78_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_79_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_81_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_84_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_85_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_86_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_87_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_88_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_89_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_8_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_90_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_91_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_92_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_96_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_97_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_98_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_99_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_9_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_100_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_102_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_104_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_105_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_106_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_107_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_108_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_111_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_112_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_113_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_115_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_116_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_117_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_118_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_119_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_11_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_120_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_121_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_122_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_124_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_125_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_126_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_127_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_128_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_129_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_12_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_130_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_131_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_133_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_134_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_135_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_136_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_137_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_138_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_139_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_13_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_140_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_146_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_147_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_148_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_149_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_14_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_151_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_152_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_153_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_154_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_155_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_156_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_157_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_158_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_159_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_160_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_161_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_162_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_164_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_166_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_167_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_168_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_16_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_170_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_171_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_172_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_173_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_174_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_175_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_176_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_177_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_178_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_179_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_17_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_180_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_181_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_182_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_183_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_184_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_185_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_186_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_187_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_18_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_190_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_193_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_194_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_197_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_199_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_19_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_200_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_202_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_203_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_204_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_205_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_206_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_20_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_210_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_211_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_212_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_22_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_23_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_24_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_25_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_26_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_27_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_28_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_29_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_31_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_32_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_33_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_37_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_38_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_39_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_40_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_41_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_42_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_43_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_44_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_45_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_46_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_47_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_48_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_53_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_54_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_55_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_56_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_58_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_61_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_62_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_63_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_64_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_65_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_66_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_68_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_69_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_70_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_71_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_72_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_73_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_74_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_77_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_78_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_79_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_80_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_81_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_82_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_83_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_84_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_88_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_89_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_91_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_92_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_93_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_94_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_95_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_96_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_97_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_98_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_37_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_38_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_100_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_100_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_100_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_100_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_100_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_103_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_112_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_113_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_113_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_113_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_113_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_114_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_114_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_114_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_114_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_115_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_124_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_129_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_129_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_129_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_129_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_129_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_138_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_138_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_138_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_138_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_138_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_13_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_13_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_13_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_148_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_157_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_157_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_157_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_157_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_157_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_160_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_160_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_160_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_160_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_160_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_162_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_162_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_162_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_162_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_162_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_163_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_163_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_163_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_163_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_163_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_19_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_21_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_22_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_22_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_239_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_23_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_23_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_23_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_23_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_23_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_248_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_248_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_248_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_248_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_248_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_249_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_249_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_249_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_249_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_249_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_24_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_24_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_24_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_24_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_24_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_250_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_250_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_250_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_250_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_250_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_257_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_25_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_266_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_267_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_267_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_267_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_267_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_267_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_276_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_276_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_276_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_276_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_276_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_285_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_285_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_285_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_285_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_285_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_286_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_286_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_286_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_287_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_287_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_287_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_287_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_288_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_288_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_288_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_289_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_28_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_298_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_298_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_298_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_298_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_298_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_29_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_29_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_29_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_29_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_29_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_301_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_301_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_301_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_301_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_301_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_303_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_303_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_303_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_303_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_303_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_305_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_305_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_305_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_305_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_305_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_30_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_30_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_30_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_30_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_30_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_342_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_343_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_344_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_346_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_34_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_355_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_355_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_355_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_355_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_355_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_364_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_364_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_364_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_364_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_364_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_371_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_371_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_371_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_371_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_376_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_385_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_385_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_385_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_385_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_385_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_394_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_394_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_394_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_394_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_394_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_403_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_403_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_403_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_403_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_403_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_404_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_404_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_404_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_404_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_404_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_416_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_416_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_416_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_416_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_416_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_427_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_43_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_441_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_441_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_441_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_441_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_442_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_442_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_442_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_442_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_442_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_443_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_443_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_443_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_443_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_443_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_486_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_502_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_502_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_502_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_502_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_502_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_514_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_523_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_523_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_523_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_523_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_523_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_52_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_532_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_532_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_532_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_532_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_532_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_541_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_541_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_541_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_541_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_541_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_542_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_542_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_542_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_542_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_542_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_578_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_578_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_578_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_578_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_578_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_591_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_591_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_591_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_591_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_591_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_608_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_608_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_608_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_609_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_609_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_609_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_609_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_609_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_615_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_61_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_62_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_62_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_62_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_62_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_639_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_639_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_639_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_639_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_639_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_640_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_640_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_642_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_642_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_642_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_642_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_642_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_644_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_644_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_644_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_644_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_644_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_666_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_666_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_678_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_678_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_67_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_67_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_67_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_67_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_67_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_718_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_727_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_76_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_76_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_76_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_76_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_76_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_77_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_78_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_87_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_87_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_87_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_87_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_87_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_88_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_88_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_88_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_88_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_88_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_89_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_89_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_89_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_89_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_89_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_90_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_90_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_90_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_90_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_90_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_93_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_93_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_93_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_93_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_93_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_95_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_95_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_95_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_95_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_95_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_97_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_97_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_97_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_97_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_97_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_99_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_99_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_99_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_99_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_99_n_7\ : STD_LOGIC;
  signal \caracter_reg[2]_i_115_n_0\ : STD_LOGIC;
  signal \caracter_reg[2]_i_115_n_4\ : STD_LOGIC;
  signal \caracter_reg[2]_i_115_n_5\ : STD_LOGIC;
  signal \caracter_reg[2]_i_115_n_6\ : STD_LOGIC;
  signal \caracter_reg[2]_i_115_n_7\ : STD_LOGIC;
  signal \caracter_reg[2]_i_137_n_0\ : STD_LOGIC;
  signal \caracter_reg[2]_i_146_n_0\ : STD_LOGIC;
  signal \caracter_reg[2]_i_146_n_4\ : STD_LOGIC;
  signal \caracter_reg[2]_i_146_n_5\ : STD_LOGIC;
  signal \caracter_reg[2]_i_146_n_6\ : STD_LOGIC;
  signal \caracter_reg[2]_i_146_n_7\ : STD_LOGIC;
  signal \caracter_reg[2]_i_14_n_7\ : STD_LOGIC;
  signal \caracter_reg[2]_i_151_n_0\ : STD_LOGIC;
  signal \caracter_reg[2]_i_151_n_4\ : STD_LOGIC;
  signal \caracter_reg[2]_i_151_n_5\ : STD_LOGIC;
  signal \caracter_reg[2]_i_151_n_6\ : STD_LOGIC;
  signal \caracter_reg[2]_i_151_n_7\ : STD_LOGIC;
  signal \^caracter_reg[2]_i_15_0\ : STD_LOGIC;
  signal \caracter_reg[2]_i_15_n_0\ : STD_LOGIC;
  signal \caracter_reg[2]_i_15_n_4\ : STD_LOGIC;
  signal \caracter_reg[2]_i_15_n_5\ : STD_LOGIC;
  signal \caracter_reg[2]_i_15_n_6\ : STD_LOGIC;
  signal \caracter_reg[2]_i_166_n_0\ : STD_LOGIC;
  signal \caracter_reg[2]_i_166_n_4\ : STD_LOGIC;
  signal \caracter_reg[2]_i_166_n_5\ : STD_LOGIC;
  signal \caracter_reg[2]_i_166_n_6\ : STD_LOGIC;
  signal \caracter_reg[2]_i_166_n_7\ : STD_LOGIC;
  signal \caracter_reg[2]_i_175_n_6\ : STD_LOGIC;
  signal \caracter_reg[2]_i_175_n_7\ : STD_LOGIC;
  signal \caracter_reg[2]_i_176_n_0\ : STD_LOGIC;
  signal \caracter_reg[2]_i_185_n_0\ : STD_LOGIC;
  signal \caracter_reg[2]_i_185_n_4\ : STD_LOGIC;
  signal \caracter_reg[2]_i_185_n_5\ : STD_LOGIC;
  signal \caracter_reg[2]_i_185_n_6\ : STD_LOGIC;
  signal \caracter_reg[2]_i_186_n_0\ : STD_LOGIC;
  signal \caracter_reg[2]_i_186_n_7\ : STD_LOGIC;
  signal \caracter_reg[2]_i_195_n_0\ : STD_LOGIC;
  signal \caracter_reg[2]_i_195_n_4\ : STD_LOGIC;
  signal \caracter_reg[2]_i_195_n_5\ : STD_LOGIC;
  signal \caracter_reg[2]_i_195_n_6\ : STD_LOGIC;
  signal \caracter_reg[2]_i_31_n_5\ : STD_LOGIC;
  signal \caracter_reg[2]_i_31_n_6\ : STD_LOGIC;
  signal \caracter_reg[2]_i_31_n_7\ : STD_LOGIC;
  signal \caracter_reg[2]_i_36_n_0\ : STD_LOGIC;
  signal \caracter_reg[2]_i_36_n_4\ : STD_LOGIC;
  signal \caracter_reg[2]_i_37_n_5\ : STD_LOGIC;
  signal \caracter_reg[2]_i_37_n_6\ : STD_LOGIC;
  signal \caracter_reg[2]_i_37_n_7\ : STD_LOGIC;
  signal \caracter_reg[2]_i_40_n_4\ : STD_LOGIC;
  signal \caracter_reg[2]_i_40_n_5\ : STD_LOGIC;
  signal \caracter_reg[2]_i_40_n_6\ : STD_LOGIC;
  signal \caracter_reg[2]_i_40_n_7\ : STD_LOGIC;
  signal \caracter_reg[2]_i_43_n_0\ : STD_LOGIC;
  signal \caracter_reg[2]_i_61_n_0\ : STD_LOGIC;
  signal \caracter_reg[2]_i_70_n_0\ : STD_LOGIC;
  signal \caracter_reg[2]_i_70_n_4\ : STD_LOGIC;
  signal \caracter_reg[2]_i_70_n_5\ : STD_LOGIC;
  signal \caracter_reg[2]_i_70_n_6\ : STD_LOGIC;
  signal \caracter_reg[2]_i_70_n_7\ : STD_LOGIC;
  signal \caracter_reg[2]_i_71_n_3\ : STD_LOGIC;
  signal \caracter_reg[2]_i_72_n_0\ : STD_LOGIC;
  signal \caracter_reg[2]_i_72_n_4\ : STD_LOGIC;
  signal \caracter_reg[2]_i_72_n_5\ : STD_LOGIC;
  signal \caracter_reg[2]_i_72_n_6\ : STD_LOGIC;
  signal \caracter_reg[2]_i_72_n_7\ : STD_LOGIC;
  signal \caracter_reg[2]_i_73_n_1\ : STD_LOGIC;
  signal \caracter_reg[2]_i_73_n_6\ : STD_LOGIC;
  signal \caracter_reg[2]_i_73_n_7\ : STD_LOGIC;
  signal \caracter_reg[2]_i_74_n_1\ : STD_LOGIC;
  signal \caracter_reg[2]_i_74_n_6\ : STD_LOGIC;
  signal \caracter_reg[2]_i_74_n_7\ : STD_LOGIC;
  signal \caracter_reg[2]_i_75_n_0\ : STD_LOGIC;
  signal \caracter_reg[2]_i_75_n_4\ : STD_LOGIC;
  signal \caracter_reg[2]_i_75_n_5\ : STD_LOGIC;
  signal \caracter_reg[2]_i_75_n_6\ : STD_LOGIC;
  signal \caracter_reg[2]_i_75_n_7\ : STD_LOGIC;
  signal \caracter_reg[2]_i_80_n_7\ : STD_LOGIC;
  signal \caracter_reg[2]_i_82_n_5\ : STD_LOGIC;
  signal \caracter_reg[2]_i_82_n_6\ : STD_LOGIC;
  signal \caracter_reg[2]_i_82_n_7\ : STD_LOGIC;
  signal \caracter_reg[2]_i_83_n_0\ : STD_LOGIC;
  signal \caracter_reg[2]_i_93_n_3\ : STD_LOGIC;
  signal \caracter_reg[2]_i_94_n_0\ : STD_LOGIC;
  signal \caracter_reg[2]_i_94_n_4\ : STD_LOGIC;
  signal \caracter_reg[2]_i_94_n_5\ : STD_LOGIC;
  signal \caracter_reg[2]_i_94_n_6\ : STD_LOGIC;
  signal \caracter_reg[2]_i_94_n_7\ : STD_LOGIC;
  signal \caracter_reg[2]_i_95_n_0\ : STD_LOGIC;
  signal \caracter_reg[2]_i_95_n_4\ : STD_LOGIC;
  signal \caracter_reg[2]_i_95_n_5\ : STD_LOGIC;
  signal \caracter_reg[2]_i_95_n_6\ : STD_LOGIC;
  signal \caracter_reg[3]_i_103_n_0\ : STD_LOGIC;
  signal \caracter_reg[3]_i_103_n_4\ : STD_LOGIC;
  signal \caracter_reg[3]_i_103_n_5\ : STD_LOGIC;
  signal \caracter_reg[3]_i_103_n_6\ : STD_LOGIC;
  signal \caracter_reg[3]_i_103_n_7\ : STD_LOGIC;
  signal \caracter_reg[3]_i_109_n_0\ : STD_LOGIC;
  signal \caracter_reg[3]_i_109_n_4\ : STD_LOGIC;
  signal \caracter_reg[3]_i_109_n_5\ : STD_LOGIC;
  signal \caracter_reg[3]_i_109_n_6\ : STD_LOGIC;
  signal \caracter_reg[3]_i_109_n_7\ : STD_LOGIC;
  signal \caracter_reg[3]_i_114_n_0\ : STD_LOGIC;
  signal \caracter_reg[3]_i_123_n_0\ : STD_LOGIC;
  signal \caracter_reg[3]_i_123_n_4\ : STD_LOGIC;
  signal \caracter_reg[3]_i_123_n_5\ : STD_LOGIC;
  signal \caracter_reg[3]_i_123_n_6\ : STD_LOGIC;
  signal \caracter_reg[3]_i_123_n_7\ : STD_LOGIC;
  signal \caracter_reg[3]_i_132_n_0\ : STD_LOGIC;
  signal \caracter_reg[3]_i_132_n_4\ : STD_LOGIC;
  signal \caracter_reg[3]_i_132_n_5\ : STD_LOGIC;
  signal \caracter_reg[3]_i_132_n_6\ : STD_LOGIC;
  signal \caracter_reg[3]_i_141_n_0\ : STD_LOGIC;
  signal \caracter_reg[3]_i_141_n_4\ : STD_LOGIC;
  signal \caracter_reg[3]_i_141_n_5\ : STD_LOGIC;
  signal \caracter_reg[3]_i_141_n_6\ : STD_LOGIC;
  signal \caracter_reg[3]_i_150_n_0\ : STD_LOGIC;
  signal \caracter_reg[3]_i_188_n_2\ : STD_LOGIC;
  signal \caracter_reg[3]_i_188_n_7\ : STD_LOGIC;
  signal \caracter_reg[3]_i_189_n_0\ : STD_LOGIC;
  signal \caracter_reg[3]_i_189_n_4\ : STD_LOGIC;
  signal \caracter_reg[3]_i_189_n_5\ : STD_LOGIC;
  signal \caracter_reg[3]_i_189_n_6\ : STD_LOGIC;
  signal \caracter_reg[3]_i_189_n_7\ : STD_LOGIC;
  signal \caracter_reg[3]_i_191_n_0\ : STD_LOGIC;
  signal \caracter_reg[3]_i_191_n_4\ : STD_LOGIC;
  signal \caracter_reg[3]_i_198_n_0\ : STD_LOGIC;
  signal \caracter_reg[3]_i_21_n_4\ : STD_LOGIC;
  signal \caracter_reg[3]_i_21_n_5\ : STD_LOGIC;
  signal \caracter_reg[3]_i_21_n_6\ : STD_LOGIC;
  signal \caracter_reg[3]_i_21_n_7\ : STD_LOGIC;
  signal \caracter_reg[3]_i_30_n_0\ : STD_LOGIC;
  signal \caracter_reg[3]_i_30_n_4\ : STD_LOGIC;
  signal \caracter_reg[3]_i_30_n_5\ : STD_LOGIC;
  signal \caracter_reg[3]_i_59_n_5\ : STD_LOGIC;
  signal \caracter_reg[3]_i_59_n_6\ : STD_LOGIC;
  signal \caracter_reg[3]_i_59_n_7\ : STD_LOGIC;
  signal \caracter_reg[3]_i_60_n_0\ : STD_LOGIC;
  signal \caracter_reg[3]_i_60_n_4\ : STD_LOGIC;
  signal \caracter_reg[3]_i_60_n_5\ : STD_LOGIC;
  signal \caracter_reg[3]_i_60_n_6\ : STD_LOGIC;
  signal \caracter_reg[3]_i_60_n_7\ : STD_LOGIC;
  signal \caracter_reg[3]_i_67_n_0\ : STD_LOGIC;
  signal \caracter_reg[3]_i_75_n_6\ : STD_LOGIC;
  signal \caracter_reg[3]_i_75_n_7\ : STD_LOGIC;
  signal \caracter_reg[3]_i_76_n_0\ : STD_LOGIC;
  signal \caracter_reg[3]_i_85_n_1\ : STD_LOGIC;
  signal \caracter_reg[3]_i_85_n_6\ : STD_LOGIC;
  signal \caracter_reg[3]_i_85_n_7\ : STD_LOGIC;
  signal \caracter_reg[3]_i_86_n_0\ : STD_LOGIC;
  signal \caracter_reg[3]_i_86_n_5\ : STD_LOGIC;
  signal \caracter_reg[3]_i_86_n_6\ : STD_LOGIC;
  signal \caracter_reg[3]_i_86_n_7\ : STD_LOGIC;
  signal \caracter_reg[3]_i_87_n_1\ : STD_LOGIC;
  signal \caracter_reg[3]_i_87_n_6\ : STD_LOGIC;
  signal \caracter_reg[3]_i_87_n_7\ : STD_LOGIC;
  signal \caracter_reg[3]_i_90_n_0\ : STD_LOGIC;
  signal \caracter_reg[3]_i_99_n_0\ : STD_LOGIC;
  signal \caracter_reg[3]_i_99_n_4\ : STD_LOGIC;
  signal \caracter_reg[3]_i_99_n_5\ : STD_LOGIC;
  signal \caracter_reg[3]_i_99_n_6\ : STD_LOGIC;
  signal \caracter_reg[3]_i_99_n_7\ : STD_LOGIC;
  signal \caracter_reg[6]_i_28_n_0\ : STD_LOGIC;
  signal cent : STD_LOGIC_VECTOR ( 6 downto 1 );
  signal \disp_dinero[2]_7\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \disp_dinero[5]_8\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal euros2 : STD_LOGIC_VECTOR ( 30 downto 0 );
  signal euros3 : STD_LOGIC_VECTOR ( 30 downto 1 );
  signal p_1_in : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \string_cent_decenas[1]5\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^total_out_reg[5]\ : STD_LOGIC;
  signal \NLW_caracter_reg[0]_i_100_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_103_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_103_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_112_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_caracter_reg[0]_i_112_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_113_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_113_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_caracter_reg[0]_i_114_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_114_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_caracter_reg[0]_i_115_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_115_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_124_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_129_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_13_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_138_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_148_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_148_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_157_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_160_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_162_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_163_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_19_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_caracter_reg[0]_i_19_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_20_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_20_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_caracter_reg[0]_i_21_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_caracter_reg[0]_i_21_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_22_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_22_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_caracter_reg[0]_i_23_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_239_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_239_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_24_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_248_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_249_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_25_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_25_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_250_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_257_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_257_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_266_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_267_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_276_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_28_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_285_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_286_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_286_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_caracter_reg[0]_i_287_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_287_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_caracter_reg[0]_i_288_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_288_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_caracter_reg[0]_i_289_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_289_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_29_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_298_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_30_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_301_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_303_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_305_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_34_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_34_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_342_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_343_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_344_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_346_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_346_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_355_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_364_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_371_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_371_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_caracter_reg[0]_i_376_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_376_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_385_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_394_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_403_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_404_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_416_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_427_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_427_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_43_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_43_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_441_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_441_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_caracter_reg[0]_i_442_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_443_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_486_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_486_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_502_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_514_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_514_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_52_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_52_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_523_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_532_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_541_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_542_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_578_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_591_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_608_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_608_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_caracter_reg[0]_i_609_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_61_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_615_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_615_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_62_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_639_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_640_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_640_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_caracter_reg[0]_i_642_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_644_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_666_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_666_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_67_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_678_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_678_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_718_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_718_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_727_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_727_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_76_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_77_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_77_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_caracter_reg[0]_i_78_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_78_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_87_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_88_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_89_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_90_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_93_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_95_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_97_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_99_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[2]_i_115_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[2]_i_137_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[2]_i_137_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[2]_i_14_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[2]_i_14_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_caracter_reg[2]_i_146_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[2]_i_15_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[2]_i_151_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[2]_i_166_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[2]_i_175_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[2]_i_175_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_caracter_reg[2]_i_176_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[2]_i_176_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[2]_i_185_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[2]_i_185_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_caracter_reg[2]_i_186_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[2]_i_186_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_caracter_reg[2]_i_195_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[2]_i_195_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_caracter_reg[2]_i_31_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[2]_i_31_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_caracter_reg[2]_i_36_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[2]_i_36_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[2]_i_37_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[2]_i_37_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_caracter_reg[2]_i_40_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[2]_i_43_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[2]_i_43_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[2]_i_61_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[2]_i_61_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[2]_i_70_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[2]_i_71_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_caracter_reg[2]_i_71_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[2]_i_72_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[2]_i_73_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[2]_i_73_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_caracter_reg[2]_i_74_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[2]_i_74_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_caracter_reg[2]_i_75_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[2]_i_80_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[2]_i_80_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_caracter_reg[2]_i_82_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[2]_i_82_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_caracter_reg[2]_i_83_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[2]_i_83_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[2]_i_93_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_caracter_reg[2]_i_93_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[2]_i_94_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[2]_i_95_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[2]_i_95_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_caracter_reg[3]_i_103_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[3]_i_109_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[3]_i_114_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[3]_i_114_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[3]_i_123_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[3]_i_132_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[3]_i_132_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_caracter_reg[3]_i_141_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[3]_i_141_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_caracter_reg[3]_i_150_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[3]_i_150_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[3]_i_188_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[3]_i_188_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_caracter_reg[3]_i_189_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[3]_i_191_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[3]_i_191_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[3]_i_198_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[3]_i_198_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[3]_i_21_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[3]_i_30_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[3]_i_30_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_caracter_reg[3]_i_59_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[3]_i_59_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_caracter_reg[3]_i_60_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[3]_i_67_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[3]_i_67_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[3]_i_75_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[3]_i_75_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_caracter_reg[3]_i_76_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[3]_i_76_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[3]_i_85_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[3]_i_85_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_caracter_reg[3]_i_86_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[3]_i_86_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_caracter_reg[3]_i_87_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[3]_i_87_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_caracter_reg[3]_i_90_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[3]_i_90_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[3]_i_99_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \caracter[0]_i_101\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \caracter[0]_i_102\ : label is "soft_lutpair42";
  attribute HLUTNM : string;
  attribute HLUTNM of \caracter[0]_i_130\ : label is "lutpair67";
  attribute HLUTNM of \caracter[0]_i_133\ : label is "lutpair66";
  attribute HLUTNM of \caracter[0]_i_134\ : label is "lutpair68";
  attribute HLUTNM of \caracter[0]_i_135\ : label is "lutpair67";
  attribute HLUTNM of \caracter[0]_i_139\ : label is "lutpair58";
  attribute HLUTNM of \caracter[0]_i_140\ : label is "lutpair57";
  attribute HLUTNM of \caracter[0]_i_141\ : label is "lutpair56";
  attribute HLUTNM of \caracter[0]_i_142\ : label is "lutpair55";
  attribute HLUTNM of \caracter[0]_i_144\ : label is "lutpair58";
  attribute HLUTNM of \caracter[0]_i_145\ : label is "lutpair57";
  attribute HLUTNM of \caracter[0]_i_146\ : label is "lutpair56";
  attribute SOFT_HLUTNM of \caracter[0]_i_159\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \caracter[0]_i_164\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \caracter[0]_i_165\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \caracter[0]_i_166\ : label is "soft_lutpair29";
  attribute HLUTNM of \caracter[0]_i_171\ : label is "lutpair37";
  attribute HLUTNM of \caracter[0]_i_175\ : label is "lutpair29";
  attribute HLUTNM of \caracter[0]_i_176\ : label is "lutpair28";
  attribute HLUTNM of \caracter[0]_i_177\ : label is "lutpair27";
  attribute HLUTNM of \caracter[0]_i_178\ : label is "lutpair26";
  attribute HLUTNM of \caracter[0]_i_179\ : label is "lutpair30";
  attribute HLUTNM of \caracter[0]_i_180\ : label is "lutpair29";
  attribute HLUTNM of \caracter[0]_i_181\ : label is "lutpair28";
  attribute HLUTNM of \caracter[0]_i_182\ : label is "lutpair27";
  attribute HLUTNM of \caracter[0]_i_193\ : label is "lutpair18";
  attribute HLUTNM of \caracter[0]_i_194\ : label is "lutpair17";
  attribute HLUTNM of \caracter[0]_i_198\ : label is "lutpair18";
  attribute HLUTNM of \caracter[0]_i_207\ : label is "lutpair16";
  attribute HLUTNM of \caracter[0]_i_208\ : label is "lutpair15";
  attribute HLUTNM of \caracter[0]_i_209\ : label is "lutpair14";
  attribute HLUTNM of \caracter[0]_i_210\ : label is "lutpair13";
  attribute HLUTNM of \caracter[0]_i_211\ : label is "lutpair17";
  attribute HLUTNM of \caracter[0]_i_212\ : label is "lutpair16";
  attribute HLUTNM of \caracter[0]_i_213\ : label is "lutpair15";
  attribute HLUTNM of \caracter[0]_i_214\ : label is "lutpair14";
  attribute HLUTNM of \caracter[0]_i_215\ : label is "lutpair25";
  attribute HLUTNM of \caracter[0]_i_216\ : label is "lutpair24";
  attribute HLUTNM of \caracter[0]_i_217\ : label is "lutpair23";
  attribute HLUTNM of \caracter[0]_i_218\ : label is "lutpair22";
  attribute HLUTNM of \caracter[0]_i_219\ : label is "lutpair26";
  attribute HLUTNM of \caracter[0]_i_220\ : label is "lutpair25";
  attribute HLUTNM of \caracter[0]_i_221\ : label is "lutpair24";
  attribute HLUTNM of \caracter[0]_i_222\ : label is "lutpair23";
  attribute HLUTNM of \caracter[0]_i_223\ : label is "lutpair33";
  attribute HLUTNM of \caracter[0]_i_224\ : label is "lutpair32";
  attribute HLUTNM of \caracter[0]_i_225\ : label is "lutpair31";
  attribute HLUTNM of \caracter[0]_i_226\ : label is "lutpair30";
  attribute HLUTNM of \caracter[0]_i_227\ : label is "lutpair34";
  attribute HLUTNM of \caracter[0]_i_228\ : label is "lutpair33";
  attribute HLUTNM of \caracter[0]_i_229\ : label is "lutpair32";
  attribute HLUTNM of \caracter[0]_i_230\ : label is "lutpair31";
  attribute HLUTNM of \caracter[0]_i_231\ : label is "lutpair40";
  attribute HLUTNM of \caracter[0]_i_232\ : label is "lutpair39";
  attribute HLUTNM of \caracter[0]_i_233\ : label is "lutpair38";
  attribute HLUTNM of \caracter[0]_i_234\ : label is "lutpair37";
  attribute HLUTNM of \caracter[0]_i_235\ : label is "lutpair41";
  attribute HLUTNM of \caracter[0]_i_236\ : label is "lutpair40";
  attribute HLUTNM of \caracter[0]_i_237\ : label is "lutpair39";
  attribute HLUTNM of \caracter[0]_i_238\ : label is "lutpair38";
  attribute HLUTNM of \caracter[0]_i_268\ : label is "lutpair65";
  attribute HLUTNM of \caracter[0]_i_271\ : label is "lutpair64";
  attribute HLUTNM of \caracter[0]_i_272\ : label is "lutpair66";
  attribute HLUTNM of \caracter[0]_i_273\ : label is "lutpair65";
  attribute HLUTNM of \caracter[0]_i_281\ : label is "lutpair55";
  attribute SOFT_HLUTNM of \caracter[0]_i_299\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \caracter[0]_i_300\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \caracter[0]_i_302\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \caracter[0]_i_304\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \caracter[0]_i_306\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \caracter[0]_i_307\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \caracter[0]_i_308\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \caracter[0]_i_309\ : label is "soft_lutpair25";
  attribute HLUTNM of \caracter[0]_i_326\ : label is "lutpair21";
  attribute HLUTNM of \caracter[0]_i_327\ : label is "lutpair20";
  attribute HLUTNM of \caracter[0]_i_328\ : label is "lutpair19";
  attribute HLUTNM of \caracter[0]_i_330\ : label is "lutpair22";
  attribute HLUTNM of \caracter[0]_i_331\ : label is "lutpair21";
  attribute HLUTNM of \caracter[0]_i_332\ : label is "lutpair20";
  attribute HLUTNM of \caracter[0]_i_333\ : label is "lutpair19";
  attribute HLUTNM of \caracter[0]_i_334\ : label is "lutpair12";
  attribute HLUTNM of \caracter[0]_i_335\ : label is "lutpair11";
  attribute HLUTNM of \caracter[0]_i_336\ : label is "lutpair10";
  attribute HLUTNM of \caracter[0]_i_337\ : label is "lutpair9";
  attribute HLUTNM of \caracter[0]_i_338\ : label is "lutpair13";
  attribute HLUTNM of \caracter[0]_i_339\ : label is "lutpair12";
  attribute HLUTNM of \caracter[0]_i_340\ : label is "lutpair11";
  attribute HLUTNM of \caracter[0]_i_341\ : label is "lutpair10";
  attribute SOFT_HLUTNM of \caracter[0]_i_345\ : label is "soft_lutpair21";
  attribute HLUTNM of \caracter[0]_i_386\ : label is "lutpair63";
  attribute HLUTNM of \caracter[0]_i_389\ : label is "lutpair62";
  attribute HLUTNM of \caracter[0]_i_390\ : label is "lutpair64";
  attribute HLUTNM of \caracter[0]_i_391\ : label is "lutpair63";
  attribute HLUTNM of \caracter[0]_i_430\ : label is "lutpair54";
  attribute HLUTNM of \caracter[0]_i_431\ : label is "lutpair53";
  attribute HLUTNM of \caracter[0]_i_435\ : label is "lutpair54";
  attribute SOFT_HLUTNM of \caracter[0]_i_436\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \caracter[0]_i_438\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \caracter[0]_i_439\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \caracter[0]_i_440\ : label is "soft_lutpair13";
  attribute HLUTNM of \caracter[0]_i_46\ : label is "lutpair3";
  attribute HLUTNM of \caracter[0]_i_467\ : label is "lutpair8";
  attribute HLUTNM of \caracter[0]_i_468\ : label is "lutpair7";
  attribute HLUTNM of \caracter[0]_i_469\ : label is "lutpair6";
  attribute HLUTNM of \caracter[0]_i_470\ : label is "lutpair5";
  attribute HLUTNM of \caracter[0]_i_471\ : label is "lutpair9";
  attribute HLUTNM of \caracter[0]_i_472\ : label is "lutpair8";
  attribute HLUTNM of \caracter[0]_i_473\ : label is "lutpair7";
  attribute HLUTNM of \caracter[0]_i_474\ : label is "lutpair6";
  attribute SOFT_HLUTNM of \caracter[0]_i_475\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \caracter[0]_i_477\ : label is "soft_lutpair23";
  attribute HLUTNM of \caracter[0]_i_489\ : label is "lutpair2";
  attribute HLUTNM of \caracter[0]_i_490\ : label is "lutpair1";
  attribute HLUTNM of \caracter[0]_i_494\ : label is "lutpair2";
  attribute HLUTNM of \caracter[0]_i_503\ : label is "lutpair0";
  attribute HLUTNM of \caracter[0]_i_506\ : label is "lutpair81";
  attribute HLUTNM of \caracter[0]_i_508\ : label is "lutpair0";
  attribute HLUTNM of \caracter[0]_i_51\ : label is "lutpair3";
  attribute HLUTNM of \caracter[0]_i_524\ : label is "lutpair61";
  attribute HLUTNM of \caracter[0]_i_527\ : label is "lutpair60";
  attribute HLUTNM of \caracter[0]_i_528\ : label is "lutpair62";
  attribute HLUTNM of \caracter[0]_i_529\ : label is "lutpair61";
  attribute HLUTNM of \caracter[0]_i_546\ : label is "lutpair52";
  attribute HLUTNM of \caracter[0]_i_547\ : label is "lutpair51";
  attribute HLUTNM of \caracter[0]_i_548\ : label is "lutpair50";
  attribute HLUTNM of \caracter[0]_i_549\ : label is "lutpair49";
  attribute HLUTNM of \caracter[0]_i_551\ : label is "lutpair52";
  attribute HLUTNM of \caracter[0]_i_552\ : label is "lutpair51";
  attribute HLUTNM of \caracter[0]_i_553\ : label is "lutpair50";
  attribute HLUTNM of \caracter[0]_i_563\ : label is "lutpair36";
  attribute HLUTNM of \caracter[0]_i_564\ : label is "lutpair35";
  attribute HLUTNM of \caracter[0]_i_565\ : label is "lutpair34";
  attribute HLUTNM of \caracter[0]_i_568\ : label is "lutpair36";
  attribute HLUTNM of \caracter[0]_i_569\ : label is "lutpair35";
  attribute HLUTNM of \caracter[0]_i_574\ : label is "lutpair53";
  attribute HLUTNM of \caracter[0]_i_583\ : label is "lutpair4";
  attribute HLUTNM of \caracter[0]_i_587\ : label is "lutpair5";
  attribute HLUTNM of \caracter[0]_i_588\ : label is "lutpair4";
  attribute SOFT_HLUTNM of \caracter[0]_i_6\ : label is "soft_lutpair20";
  attribute HLUTNM of \caracter[0]_i_604\ : label is "lutpair1";
  attribute HLUTNM of \caracter[0]_i_612\ : label is "lutpair81";
  attribute HLUTNM of \caracter[0]_i_624\ : label is "lutpair59";
  attribute HLUTNM of \caracter[0]_i_627\ : label is "lutpair60";
  attribute HLUTNM of \caracter[0]_i_628\ : label is "lutpair59";
  attribute SOFT_HLUTNM of \caracter[0]_i_643\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \caracter[0]_i_645\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \caracter[0]_i_648\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \caracter[0]_i_649\ : label is "soft_lutpair37";
  attribute HLUTNM of \caracter[0]_i_650\ : label is "lutpair48";
  attribute HLUTNM of \caracter[0]_i_651\ : label is "lutpair47";
  attribute HLUTNM of \caracter[0]_i_652\ : label is "lutpair46";
  attribute HLUTNM of \caracter[0]_i_653\ : label is "lutpair45";
  attribute HLUTNM of \caracter[0]_i_654\ : label is "lutpair49";
  attribute HLUTNM of \caracter[0]_i_655\ : label is "lutpair48";
  attribute HLUTNM of \caracter[0]_i_656\ : label is "lutpair47";
  attribute HLUTNM of \caracter[0]_i_657\ : label is "lutpair46";
  attribute SOFT_HLUTNM of \caracter[0]_i_690\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \caracter[0]_i_691\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \caracter[0]_i_692\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \caracter[0]_i_693\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \caracter[0]_i_694\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \caracter[0]_i_695\ : label is "soft_lutpair34";
  attribute HLUTNM of \caracter[0]_i_696\ : label is "lutpair44";
  attribute HLUTNM of \caracter[0]_i_697\ : label is "lutpair43";
  attribute HLUTNM of \caracter[0]_i_698\ : label is "lutpair42";
  attribute HLUTNM of \caracter[0]_i_699\ : label is "lutpair41";
  attribute HLUTNM of \caracter[0]_i_70\ : label is "lutpair69";
  attribute HLUTNM of \caracter[0]_i_700\ : label is "lutpair45";
  attribute HLUTNM of \caracter[0]_i_701\ : label is "lutpair44";
  attribute HLUTNM of \caracter[0]_i_702\ : label is "lutpair43";
  attribute HLUTNM of \caracter[0]_i_703\ : label is "lutpair42";
  attribute HLUTNM of \caracter[0]_i_71\ : label is "lutpair68";
  attribute SOFT_HLUTNM of \caracter[0]_i_740\ : label is "soft_lutpair21";
  attribute HLUTNM of \caracter[0]_i_75\ : label is "lutpair69";
  attribute SOFT_HLUTNM of \caracter[0]_i_91\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \caracter[0]_i_92\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \caracter[0]_i_96\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \caracter[0]_i_98\ : label is "soft_lutpair33";
  attribute OPT_MODIFIED : string;
  attribute OPT_MODIFIED of \caracter[2]_i_100\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_101\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_102\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_103\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_104\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_105\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_106\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_107\ : label is "RETARGET";
  attribute HLUTNM of \caracter[2]_i_108\ : label is "lutpair71";
  attribute OPT_MODIFIED of \caracter[2]_i_108\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_109\ : label is "RETARGET";
  attribute HLUTNM of \caracter[2]_i_111\ : label is "lutpair72";
  attribute OPT_MODIFIED of \caracter[2]_i_111\ : label is "RETARGET";
  attribute HLUTNM of \caracter[2]_i_112\ : label is "lutpair71";
  attribute OPT_MODIFIED of \caracter[2]_i_112\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_113\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_119\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_123\ : label is "RETARGET";
  attribute HLUTNM of \caracter[2]_i_125\ : label is "lutpair74";
  attribute OPT_MODIFIED of \caracter[2]_i_125\ : label is "RETARGET";
  attribute HLUTNM of \caracter[2]_i_126\ : label is "lutpair73";
  attribute OPT_MODIFIED of \caracter[2]_i_126\ : label is "RETARGET";
  attribute HLUTNM of \caracter[2]_i_127\ : label is "lutpair72";
  attribute OPT_MODIFIED of \caracter[2]_i_127\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_128\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_129\ : label is "RETARGET";
  attribute HLUTNM of \caracter[2]_i_130\ : label is "lutpair74";
  attribute OPT_MODIFIED of \caracter[2]_i_130\ : label is "RETARGET";
  attribute HLUTNM of \caracter[2]_i_131\ : label is "lutpair73";
  attribute OPT_MODIFIED of \caracter[2]_i_131\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_132\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_147\ : label is "RETARGET";
  attribute SOFT_HLUTNM of \caracter[2]_i_147\ : label is "soft_lutpair17";
  attribute OPT_MODIFIED of \caracter[2]_i_148\ : label is "RETARGET";
  attribute SOFT_HLUTNM of \caracter[2]_i_148\ : label is "soft_lutpair17";
  attribute OPT_MODIFIED of \caracter[2]_i_149\ : label is "RETARGET";
  attribute SOFT_HLUTNM of \caracter[2]_i_149\ : label is "soft_lutpair40";
  attribute OPT_MODIFIED of \caracter[2]_i_150\ : label is "RETARGET";
  attribute SOFT_HLUTNM of \caracter[2]_i_150\ : label is "soft_lutpair39";
  attribute OPT_MODIFIED of \caracter[2]_i_154\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_156\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_157\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_158\ : label is "RETARGET";
  attribute HLUTNM of \caracter[2]_i_159\ : label is "lutpair70";
  attribute OPT_MODIFIED of \caracter[2]_i_159\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_160\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_161\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_162\ : label is "RETARGET";
  attribute HLUTNM of \caracter[2]_i_163\ : label is "lutpair70";
  attribute OPT_MODIFIED of \caracter[2]_i_163\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_164\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_165\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_167\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_168\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_169\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_170\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_171\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_172\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_173\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_174\ : label is "RETARGET";
  attribute HLUTNM of \caracter[2]_i_177\ : label is "lutpair76";
  attribute HLUTNM of \caracter[2]_i_178\ : label is "lutpair75";
  attribute HLUTNM of \caracter[2]_i_182\ : label is "lutpair76";
  attribute HLUTNM of \caracter[2]_i_183\ : label is "lutpair75";
  attribute OPT_MODIFIED of \caracter[2]_i_187\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_188\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_189\ : label is "RETARGET";
  attribute SOFT_HLUTNM of \caracter[2]_i_19\ : label is "soft_lutpair14";
  attribute OPT_MODIFIED of \caracter[2]_i_191\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_192\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_193\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_194\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_198\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_200\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_201\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_202\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_203\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_204\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_205\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_206\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_207\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_208\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_212\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_213\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_216\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_217\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_218\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_22\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_221\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_222\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_227\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_228\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_231\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_232\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_234\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_235\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_236\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_237\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_27\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_28\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_29\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_32\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_33\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_34\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_35\ : label is "RETARGET";
  attribute SOFT_HLUTNM of \caracter[2]_i_76\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \caracter[2]_i_77\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \caracter[2]_i_78\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \caracter[2]_i_79\ : label is "soft_lutpair24";
  attribute OPT_MODIFIED of \caracter[2]_i_84\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_85\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_86\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_89\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[2]_i_90\ : label is "RETARGET";
  attribute SOFT_HLUTNM of \caracter[2]_i_92\ : label is "soft_lutpair41";
  attribute OPT_MODIFIED of \caracter[2]_i_96\ : label is "RETARGET";
  attribute SOFT_HLUTNM of \caracter[2]_i_96\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \caracter[2]_i_97\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \caracter[2]_i_98\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \caracter[2]_i_99\ : label is "soft_lutpair39";
  attribute OPT_MODIFIED of \caracter[3]_i_100\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_102\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_104\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_105\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_107\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_108\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_115\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_116\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_117\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_120\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_121\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_124\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_125\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_126\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_127\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_128\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_129\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_130\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_131\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_133\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_134\ : label is "RETARGET";
  attribute HLUTNM of \caracter[3]_i_135\ : label is "lutpair77";
  attribute OPT_MODIFIED of \caracter[3]_i_135\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_136\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_137\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_138\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_139\ : label is "RETARGET";
  attribute HLUTNM of \caracter[3]_i_140\ : label is "lutpair77";
  attribute OPT_MODIFIED of \caracter[3]_i_140\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_147\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_148\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_149\ : label is "RETARGET";
  attribute SOFT_HLUTNM of \caracter[3]_i_15\ : label is "soft_lutpair18";
  attribute HLUTNM of \caracter[3]_i_153\ : label is "lutpair79";
  attribute HLUTNM of \caracter[3]_i_154\ : label is "lutpair78";
  attribute HLUTNM of \caracter[3]_i_158\ : label is "lutpair79";
  attribute OPT_MODIFIED of \caracter[3]_i_159\ : label is "RETARGET";
  attribute SOFT_HLUTNM of \caracter[3]_i_159\ : label is "soft_lutpair31";
  attribute OPT_MODIFIED of \caracter[3]_i_161\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_162\ : label is "RETARGET";
  attribute SOFT_HLUTNM of \caracter[3]_i_162\ : label is "soft_lutpair31";
  attribute OPT_MODIFIED of \caracter[3]_i_166\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_167\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_168\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_171\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_172\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_176\ : label is "RETARGET";
  attribute HLUTNM of \caracter[3]_i_184\ : label is "lutpair78";
  attribute OPT_MODIFIED of \caracter[3]_i_193\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_197\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_199\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_203\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_205\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_206\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_210\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_211\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_212\ : label is "RETARGET";
  attribute SOFT_HLUTNM of \caracter[3]_i_22\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \caracter[3]_i_26\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \caracter[3]_i_32\ : label is "soft_lutpair19";
  attribute OPT_MODIFIED of \caracter[3]_i_38\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_40\ : label is "RETARGET";
  attribute SOFT_HLUTNM of \caracter[3]_i_42\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \caracter[3]_i_45\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \caracter[3]_i_46\ : label is "soft_lutpair16";
  attribute OPT_MODIFIED of \caracter[3]_i_53\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_54\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter[3]_i_57\ : label is "RETARGET";
  attribute HLUTNM of \caracter[3]_i_69\ : label is "lutpair80";
  attribute HLUTNM of \caracter[3]_i_74\ : label is "lutpair80";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_100\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_103\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_113\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_114\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_115\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_124\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_129\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_13\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_138\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_148\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_157\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_160\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_162\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_163\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_20\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_22\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_23\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_239\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_24\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_248\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_249\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_25\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_250\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_257\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_266\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_267\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_276\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_28\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_285\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_286\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_287\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_288\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_289\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_29\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_298\ : label is "PROPCONST SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_30\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_301\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_303\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_305\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_34\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_342\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_343\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_344\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_346\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_355\ : label is "PROPCONST SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_364\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_371\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_376\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_385\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_394\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_403\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_404\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_416\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_427\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_43\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_441\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_442\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_443\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_486\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_502\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_514\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_52\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_523\ : label is "PROPCONST SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_532\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_541\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_542\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_578\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_591\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_608\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_609\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_61\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_615\ : label is "PROPCONST SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_62\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_639\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_642\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_644\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_666\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_67\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_678\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_718\ : label is "PROPCONST SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_727\ : label is "PROPCONST SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_76\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_78\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_87\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_88\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_89\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_90\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_93\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_95\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_97\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[0]_i_99\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[2]_i_115\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[2]_i_137\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[2]_i_146\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[2]_i_15\ : label is "RETARGET SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[2]_i_151\ : label is "RETARGET SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[2]_i_166\ : label is "PROPCONST SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[2]_i_175\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[2]_i_176\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[2]_i_185\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[2]_i_186\ : label is "RETARGET SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[2]_i_195\ : label is "RETARGET SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[2]_i_31\ : label is "PROPCONST SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[2]_i_36\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[2]_i_37\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[2]_i_40\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[2]_i_43\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[2]_i_61\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[2]_i_70\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[2]_i_72\ : label is "PROPCONST SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[2]_i_73\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[2]_i_74\ : label is "RETARGET SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[2]_i_75\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[2]_i_82\ : label is "PROPCONST SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[2]_i_83\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[2]_i_94\ : label is "RETARGET SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[2]_i_95\ : label is "PROPCONST SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[3]_i_103\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[3]_i_109\ : label is "RETARGET SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[3]_i_114\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[3]_i_123\ : label is "RETARGET PROPCONST SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[3]_i_132\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[3]_i_141\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[3]_i_150\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[3]_i_188\ : label is "RETARGET";
  attribute OPT_MODIFIED of \caracter_reg[3]_i_189\ : label is "RETARGET SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[3]_i_191\ : label is "RETARGET SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[3]_i_198\ : label is "RETARGET PROPCONST SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[3]_i_21\ : label is "RETARGET SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[3]_i_30\ : label is "RETARGET SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[3]_i_59\ : label is "PROPCONST SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[3]_i_60\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[3]_i_67\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[3]_i_75\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[3]_i_76\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[3]_i_85\ : label is "RETARGET SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[3]_i_86\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[3]_i_87\ : label is "RETARGET SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[3]_i_90\ : label is "SWEEP";
  attribute OPT_MODIFIED of \caracter_reg[3]_i_99\ : label is "SWEEP";
begin
  \FSM_sequential_current_state_reg[0]\ <= \^fsm_sequential_current_state_reg[0]\;
  \FSM_sequential_current_state_reg[1]_0\ <= \^fsm_sequential_current_state_reg[1]_0\;
  \FSM_sequential_current_state_reg[1]_1\ <= \^fsm_sequential_current_state_reg[1]_1\;
  \FSM_sequential_current_state_reg[1]_2\ <= \^fsm_sequential_current_state_reg[1]_2\;
  \caracter_reg[2]_i_15_0\ <= \^caracter_reg[2]_i_15_0\;
  \total_out_reg[5]\ <= \^total_out_reg[5]\;
\caracter[0]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BB55F0FB20F05522"
    )
        port map (
      I0 => \caracter[3]_i_24_n_0\,
      I1 => \caracter[3]_i_26_n_0\,
      I2 => \caracter[3]_i_27_n_0\,
      I3 => \caracter[3]_i_25_n_0\,
      I4 => \caracter[3]_i_28_n_0\,
      I5 => \caracter[3]_i_29_n_0\,
      O => \disp_dinero[3]_6\(0)
    );
\caracter[0]_i_101\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter_reg[0]_i_87_n_4\,
      I1 => \caracter_reg[0]_i_88_n_5\,
      I2 => \caracter_reg[0]_i_89_n_6\,
      O => \caracter[0]_i_101_n_0\
    );
\caracter[0]_i_102\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter_reg[0]_i_87_n_6\,
      I1 => \caracter_reg[0]_i_88_n_7\,
      I2 => \caracter_reg[0]_i_93_n_4\,
      O => \caracter[0]_i_102_n_0\
    );
\caracter[0]_i_104\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \caracter_reg[0]_i_248_n_4\,
      I1 => \caracter_reg[0]_i_114_n_7\,
      I2 => \caracter_reg[0]_i_113_n_0\,
      I3 => \caracter_reg[0]_i_112_n_3\,
      O => \caracter[0]_i_104_n_0\
    );
\caracter[0]_i_105\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \caracter_reg[0]_i_248_n_5\,
      I1 => \caracter_reg[0]_i_248_n_4\,
      I2 => \caracter_reg[0]_i_113_n_0\,
      I3 => \caracter_reg[0]_i_112_n_3\,
      O => \caracter[0]_i_105_n_0\
    );
\caracter[0]_i_106\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \caracter_reg[0]_i_248_n_6\,
      I1 => \caracter_reg[0]_i_248_n_5\,
      I2 => \caracter_reg[0]_i_113_n_0\,
      I3 => \caracter_reg[0]_i_112_n_3\,
      O => \caracter[0]_i_106_n_0\
    );
\caracter[0]_i_107\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"90660060"
    )
        port map (
      I0 => \caracter_reg[0]_i_248_n_6\,
      I1 => \caracter_reg[0]_i_113_n_0\,
      I2 => \caracter_reg[0]_i_248_n_7\,
      I3 => \caracter_reg[0]_i_112_n_3\,
      I4 => \caracter_reg[0]_i_113_n_5\,
      O => \caracter[0]_i_107_n_0\
    );
\caracter[0]_i_108\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0E1E1C3"
    )
        port map (
      I0 => \caracter_reg[0]_i_248_n_4\,
      I1 => \caracter_reg[0]_i_114_n_7\,
      I2 => \caracter_reg[0]_i_114_n_6\,
      I3 => \caracter_reg[0]_i_113_n_0\,
      I4 => \caracter_reg[0]_i_112_n_3\,
      O => \caracter[0]_i_108_n_0\
    );
\caracter[0]_i_109\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0E1E1C3"
    )
        port map (
      I0 => \caracter_reg[0]_i_248_n_5\,
      I1 => \caracter_reg[0]_i_248_n_4\,
      I2 => \caracter_reg[0]_i_114_n_7\,
      I3 => \caracter_reg[0]_i_113_n_0\,
      I4 => \caracter_reg[0]_i_112_n_3\,
      O => \caracter[0]_i_109_n_0\
    );
\caracter[0]_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => p_1_in(0),
      I1 => \^total_out_reg[5]\,
      I2 => \caracter_reg[3]_i_141_0\(0),
      O => \caracter[0]_i_11_n_0\
    );
\caracter[0]_i_110\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0E1E1C3"
    )
        port map (
      I0 => \caracter_reg[0]_i_248_n_6\,
      I1 => \caracter_reg[0]_i_248_n_5\,
      I2 => \caracter_reg[0]_i_248_n_4\,
      I3 => \caracter_reg[0]_i_113_n_0\,
      I4 => \caracter_reg[0]_i_112_n_3\,
      O => \caracter[0]_i_110_n_0\
    );
\caracter[0]_i_111\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7F80F807FE01E01F"
    )
        port map (
      I0 => \caracter_reg[0]_i_113_n_5\,
      I1 => \caracter_reg[0]_i_248_n_7\,
      I2 => \caracter_reg[0]_i_248_n_6\,
      I3 => \caracter_reg[0]_i_248_n_5\,
      I4 => \caracter_reg[0]_i_113_n_0\,
      I5 => \caracter_reg[0]_i_112_n_3\,
      O => \caracter[0]_i_111_n_0\
    );
\caracter[0]_i_116\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_67_n_4\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(24),
      O => \caracter[0]_i_116_n_0\
    );
\caracter[0]_i_117\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_67_n_5\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(23),
      O => \caracter[0]_i_117_n_0\
    );
\caracter[0]_i_118\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_67_n_6\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(22),
      O => \caracter[0]_i_118_n_0\
    );
\caracter[0]_i_119\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_67_n_7\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(21),
      O => \caracter[0]_i_119_n_0\
    );
\caracter[0]_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4373"
    )
        port map (
      I0 => \disp_dinero[5]_8\(0),
      I1 => \caracter[6]_i_38_0\,
      I2 => \caracter_reg[1]_i_4\,
      I3 => \disp_refresco[5]_3\(0),
      O => \^fsm_sequential_current_state_reg[1]_0\
    );
\caracter[0]_i_120\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(24),
      I1 => \caracter_reg[0]_i_67_n_4\,
      I2 => euros3(25),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_30_n_7\,
      O => \caracter[0]_i_120_n_0\
    );
\caracter[0]_i_121\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(23),
      I1 => \caracter_reg[0]_i_67_n_5\,
      I2 => euros3(24),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_67_n_4\,
      O => \caracter[0]_i_121_n_0\
    );
\caracter[0]_i_122\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(22),
      I1 => \caracter_reg[0]_i_67_n_6\,
      I2 => euros3(23),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_67_n_5\,
      O => \caracter[0]_i_122_n_0\
    );
\caracter[0]_i_123\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(21),
      I1 => \caracter_reg[0]_i_67_n_7\,
      I2 => euros3(22),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_67_n_6\,
      O => \caracter[0]_i_123_n_0\
    );
\caracter[0]_i_125\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A956"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(3),
      I1 => \caracter_reg[0]_i_13_n_5\,
      I2 => \caracter[2]_i_41_n_0\,
      I3 => \caracter_reg[0]_i_13_n_4\,
      O => \caracter[0]_i_125_n_0\
    );
\caracter[0]_i_126\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(2),
      I1 => \caracter[2]_i_41_n_0\,
      I2 => \caracter_reg[0]_i_13_n_5\,
      O => \caracter[0]_i_126_n_0\
    );
\caracter[0]_i_127\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(1),
      I1 => p_1_in(1),
      O => \caracter[0]_i_127_n_0\
    );
\caracter[0]_i_128\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(0),
      I1 => p_1_in(0),
      O => \caracter[0]_i_128_n_0\
    );
\caracter[0]_i_130\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_138_n_7\,
      I1 => \caracter_reg[0]_i_138_n_5\,
      I2 => \caracter_reg[0]_i_76_n_6\,
      O => \caracter[0]_i_130_n_0\
    );
\caracter[0]_i_131\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_276_n_4\,
      I1 => \caracter_reg[0]_i_138_n_6\,
      I2 => \caracter_reg[0]_i_76_n_7\,
      O => \caracter[0]_i_131_n_0\
    );
\caracter[0]_i_132\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_276_n_5\,
      I1 => \caracter_reg[0]_i_138_n_7\,
      I2 => \caracter_reg[0]_i_138_n_4\,
      O => \caracter[0]_i_132_n_0\
    );
\caracter[0]_i_133\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_276_n_6\,
      I1 => \caracter_reg[0]_i_276_n_4\,
      I2 => \caracter_reg[0]_i_138_n_5\,
      O => \caracter[0]_i_133_n_0\
    );
\caracter[0]_i_134\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_138_n_6\,
      I1 => \caracter_reg[0]_i_138_n_4\,
      I2 => \caracter_reg[0]_i_76_n_5\,
      I3 => \caracter[0]_i_130_n_0\,
      O => \caracter[0]_i_134_n_0\
    );
\caracter[0]_i_135\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_138_n_7\,
      I1 => \caracter_reg[0]_i_138_n_5\,
      I2 => \caracter_reg[0]_i_76_n_6\,
      I3 => \caracter[0]_i_131_n_0\,
      O => \caracter[0]_i_135_n_0\
    );
\caracter[0]_i_136\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_276_n_4\,
      I1 => \caracter_reg[0]_i_138_n_6\,
      I2 => \caracter_reg[0]_i_76_n_7\,
      I3 => \caracter[0]_i_132_n_0\,
      O => \caracter[0]_i_136_n_0\
    );
\caracter[0]_i_137\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_276_n_5\,
      I1 => \caracter_reg[0]_i_138_n_7\,
      I2 => \caracter_reg[0]_i_138_n_4\,
      I3 => \caracter[0]_i_133_n_0\,
      O => \caracter[0]_i_137_n_0\
    );
\caracter[0]_i_139\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \caracter_reg[0]_i_285_n_4\,
      I1 => \caracter_reg[0]_i_286_n_7\,
      I2 => \caracter_reg[0]_i_287_n_0\,
      I3 => \caracter_reg[0]_i_288_n_1\,
      O => \caracter[0]_i_139_n_0\
    );
\caracter[0]_i_14\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AA8A888800202222"
    )
        port map (
      I0 => \^total_out_reg[5]\,
      I1 => \caracter_reg[0]_i_19_n_3\,
      I2 => euros3(30),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_22_n_6\,
      I5 => \caracter_reg[0]_i_23_n_7\,
      O => \disp_dinero[5]_8\(0)
    );
\caracter[0]_i_140\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \caracter_reg[0]_i_285_n_5\,
      I1 => \caracter_reg[0]_i_285_n_4\,
      I2 => \caracter_reg[0]_i_287_n_0\,
      I3 => \caracter_reg[0]_i_288_n_1\,
      O => \caracter[0]_i_140_n_0\
    );
\caracter[0]_i_141\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \caracter_reg[0]_i_285_n_6\,
      I1 => \caracter_reg[0]_i_285_n_5\,
      I2 => \caracter_reg[0]_i_287_n_0\,
      I3 => \caracter_reg[0]_i_288_n_1\,
      O => \caracter[0]_i_141_n_0\
    );
\caracter[0]_i_142\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \caracter_reg[0]_i_285_n_7\,
      I1 => \caracter_reg[0]_i_285_n_6\,
      I2 => \caracter_reg[0]_i_287_n_0\,
      I3 => \caracter_reg[0]_i_288_n_1\,
      O => \caracter[0]_i_142_n_0\
    );
\caracter[0]_i_143\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"566AA995"
    )
        port map (
      I0 => \caracter[0]_i_139_n_0\,
      I1 => \caracter_reg[0]_i_286_n_7\,
      I2 => \caracter_reg[0]_i_287_n_0\,
      I3 => \caracter_reg[0]_i_288_n_1\,
      I4 => \caracter_reg[0]_i_286_n_6\,
      O => \caracter[0]_i_143_n_0\
    );
\caracter[0]_i_144\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"366CC993"
    )
        port map (
      I0 => \caracter_reg[0]_i_285_n_4\,
      I1 => \caracter_reg[0]_i_286_n_7\,
      I2 => \caracter_reg[0]_i_287_n_0\,
      I3 => \caracter_reg[0]_i_288_n_1\,
      I4 => \caracter[0]_i_140_n_0\,
      O => \caracter[0]_i_144_n_0\
    );
\caracter[0]_i_145\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"366CC993"
    )
        port map (
      I0 => \caracter_reg[0]_i_285_n_5\,
      I1 => \caracter_reg[0]_i_285_n_4\,
      I2 => \caracter_reg[0]_i_287_n_0\,
      I3 => \caracter_reg[0]_i_288_n_1\,
      I4 => \caracter[0]_i_141_n_0\,
      O => \caracter[0]_i_145_n_0\
    );
\caracter[0]_i_146\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"366CC993"
    )
        port map (
      I0 => \caracter_reg[0]_i_285_n_6\,
      I1 => \caracter_reg[0]_i_285_n_5\,
      I2 => \caracter_reg[0]_i_287_n_0\,
      I3 => \caracter_reg[0]_i_288_n_1\,
      I4 => \caracter[0]_i_142_n_0\,
      O => \caracter[0]_i_146_n_0\
    );
\caracter[0]_i_147\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE80017"
    )
        port map (
      I0 => \caracter_reg[0]_i_286_n_7\,
      I1 => \caracter_reg[0]_i_288_n_1\,
      I2 => \caracter_reg[0]_i_287_n_0\,
      I3 => \caracter_reg[0]_i_286_n_6\,
      I4 => \caracter_reg[0]_i_286_n_1\,
      O => \caracter[0]_i_147_n_0\
    );
\caracter[0]_i_149\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_298_n_4\,
      I1 => \caracter_reg[0]_i_162_n_5\,
      I2 => \caracter_reg[0]_i_160_n_6\,
      I3 => \caracter_reg[0]_i_163_n_5\,
      I4 => \caracter[0]_i_299_n_0\,
      O => \caracter[0]_i_149_n_0\
    );
\caracter[0]_i_15\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(3),
      I1 => \caracter_reg[0]_i_24_n_6\,
      O => \caracter[0]_i_15_n_0\
    );
\caracter[0]_i_150\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_298_n_5\,
      I1 => \caracter_reg[0]_i_162_n_6\,
      I2 => \caracter_reg[0]_i_160_n_7\,
      I3 => \caracter_reg[0]_i_163_n_6\,
      I4 => \caracter[0]_i_300_n_0\,
      O => \caracter[0]_i_150_n_0\
    );
\caracter[0]_i_151\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_298_n_6\,
      I1 => \caracter_reg[0]_i_162_n_7\,
      I2 => \caracter_reg[0]_i_301_n_4\,
      I3 => \caracter_reg[0]_i_163_n_7\,
      I4 => \caracter[0]_i_302_n_0\,
      O => \caracter[0]_i_151_n_0\
    );
\caracter[0]_i_152\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_298_n_7\,
      I1 => \caracter_reg[0]_i_303_n_4\,
      I2 => \caracter_reg[0]_i_301_n_5\,
      I3 => \caracter[0]_i_304_n_0\,
      I4 => \caracter_reg[0]_i_305_n_4\,
      O => \caracter[0]_i_152_n_0\
    );
\caracter[0]_i_153\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \caracter[0]_i_149_n_0\,
      I1 => \caracter[0]_i_306_n_0\,
      I2 => \caracter_reg[0]_i_160_n_4\,
      I3 => \caracter_reg[0]_i_97_n_7\,
      I4 => \caracter_reg[0]_i_157_n_6\,
      I5 => \caracter_reg[0]_i_163_n_4\,
      O => \caracter[0]_i_153_n_0\
    );
\caracter[0]_i_154\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \caracter[0]_i_150_n_0\,
      I1 => \caracter[0]_i_307_n_0\,
      I2 => \caracter_reg[0]_i_160_n_5\,
      I3 => \caracter_reg[0]_i_162_n_4\,
      I4 => \caracter_reg[0]_i_157_n_7\,
      I5 => \caracter_reg[0]_i_163_n_5\,
      O => \caracter[0]_i_154_n_0\
    );
\caracter[0]_i_155\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \caracter[0]_i_151_n_0\,
      I1 => \caracter[0]_i_308_n_0\,
      I2 => \caracter_reg[0]_i_160_n_6\,
      I3 => \caracter_reg[0]_i_162_n_5\,
      I4 => \caracter_reg[0]_i_298_n_4\,
      I5 => \caracter_reg[0]_i_163_n_6\,
      O => \caracter[0]_i_155_n_0\
    );
\caracter[0]_i_156\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \caracter[0]_i_152_n_0\,
      I1 => \caracter[0]_i_309_n_0\,
      I2 => \caracter_reg[0]_i_160_n_7\,
      I3 => \caracter_reg[0]_i_162_n_6\,
      I4 => \caracter_reg[0]_i_298_n_5\,
      I5 => \caracter_reg[0]_i_163_n_7\,
      O => \caracter[0]_i_156_n_0\
    );
\caracter[0]_i_158\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_93_n_5\,
      I1 => \caracter_reg[0]_i_97_n_4\,
      I2 => \caracter_reg[0]_i_87_n_7\,
      O => \caracter[0]_i_158_n_0\
    );
\caracter[0]_i_159\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_93_n_6\,
      I1 => \caracter_reg[0]_i_97_n_5\,
      I2 => \caracter_reg[0]_i_157_n_4\,
      O => \caracter[0]_i_159_n_0\
    );
\caracter[0]_i_16\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(2),
      I1 => \caracter_reg[0]_i_24_n_7\,
      O => \caracter[0]_i_16_n_0\
    );
\caracter[0]_i_161\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_93_n_7\,
      I1 => \caracter_reg[0]_i_97_n_6\,
      I2 => \caracter_reg[0]_i_157_n_5\,
      O => \caracter[0]_i_161_n_0\
    );
\caracter[0]_i_164\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_160_n_4\,
      I1 => \caracter_reg[0]_i_97_n_7\,
      I2 => \caracter_reg[0]_i_157_n_6\,
      O => \caracter[0]_i_164_n_0\
    );
\caracter[0]_i_165\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter_reg[0]_i_157_n_4\,
      I1 => \caracter_reg[0]_i_97_n_5\,
      I2 => \caracter_reg[0]_i_93_n_6\,
      O => \caracter[0]_i_165_n_0\
    );
\caracter[0]_i_166\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter_reg[0]_i_157_n_6\,
      I1 => \caracter_reg[0]_i_97_n_7\,
      I2 => \caracter_reg[0]_i_160_n_4\,
      O => \caracter[0]_i_166_n_0\
    );
\caracter[0]_i_167\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => euros3(13),
      I1 => euros3(9),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(7),
      O => \caracter[0]_i_167_n_0\
    );
\caracter[0]_i_168\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => euros3(12),
      I1 => euros3(8),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(6),
      O => \caracter[0]_i_168_n_0\
    );
\caracter[0]_i_169\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCFA00FACCA000A0"
    )
        port map (
      I0 => euros3(7),
      I1 => \caracter_reg[0]_i_29_n_4\,
      I2 => euros3(5),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_29_n_6\,
      I5 => euros3(11),
      O => \caracter[0]_i_169_n_0\
    );
\caracter[0]_i_17\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(1),
      O => \caracter[0]_i_17_n_0\
    );
\caracter[0]_i_170\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCFA00FACCA000A0"
    )
        port map (
      I0 => euros3(6),
      I1 => \caracter_reg[0]_i_29_n_5\,
      I2 => euros3(4),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_29_n_7\,
      I5 => euros3(10),
      O => \caracter[0]_i_170_n_0\
    );
\caracter[0]_i_171\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(14),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(8),
      I3 => euros3(10),
      I4 => \caracter[0]_i_167_n_0\,
      O => \caracter[0]_i_171_n_0\
    );
\caracter[0]_i_172\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5659A6A95956A9A6"
    )
        port map (
      I0 => \caracter[0]_i_168_n_0\,
      I1 => euros3(13),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(7),
      I4 => \caracter_reg[0]_i_29_n_4\,
      I5 => euros3(9),
      O => \caracter[0]_i_172_n_0\
    );
\caracter[0]_i_173\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5A665A995A995A66"
    )
        port map (
      I0 => \caracter[0]_i_169_n_0\,
      I1 => euros3(6),
      I2 => \caracter_reg[0]_i_29_n_5\,
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => euros3(8),
      I5 => euros3(12),
      O => \caracter[0]_i_173_n_0\
    );
\caracter[0]_i_174\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9969666999966696"
    )
        port map (
      I0 => \caracter[0]_i_170_n_0\,
      I1 => euros2(5),
      I2 => euros3(7),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_29_n_4\,
      I5 => euros3(11),
      O => \caracter[0]_i_174_n_0\
    );
\caracter[0]_i_175\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(22),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(18),
      I3 => euros3(20),
      O => \caracter[0]_i_175_n_0\
    );
\caracter[0]_i_176\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(21),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(17),
      I3 => euros3(19),
      O => \caracter[0]_i_176_n_0\
    );
\caracter[0]_i_177\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(20),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(16),
      I3 => euros3(18),
      O => \caracter[0]_i_177_n_0\
    );
\caracter[0]_i_178\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(19),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(15),
      I3 => euros3(17),
      O => \caracter[0]_i_178_n_0\
    );
\caracter[0]_i_179\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(23),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(19),
      I3 => euros3(21),
      I4 => \caracter[0]_i_175_n_0\,
      O => \caracter[0]_i_179_n_0\
    );
\caracter[0]_i_18\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(0),
      O => \caracter[0]_i_18_n_0\
    );
\caracter[0]_i_180\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(22),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(18),
      I3 => euros3(20),
      I4 => \caracter[0]_i_176_n_0\,
      O => \caracter[0]_i_180_n_0\
    );
\caracter[0]_i_181\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(21),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(17),
      I3 => euros3(19),
      I4 => \caracter[0]_i_177_n_0\,
      O => \caracter[0]_i_181_n_0\
    );
\caracter[0]_i_182\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(20),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(16),
      I3 => euros3(18),
      I4 => \caracter[0]_i_178_n_0\,
      O => \caracter[0]_i_182_n_0\
    );
\caracter[0]_i_183\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(16),
      I1 => \caracter_reg[0]_i_21_n_3\,
      O => euros2(16)
    );
\caracter[0]_i_184\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(15),
      I1 => \caracter_reg[0]_i_21_n_3\,
      O => euros2(15)
    );
\caracter[0]_i_185\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(14),
      I1 => \caracter_reg[0]_i_21_n_3\,
      O => euros2(14)
    );
\caracter[0]_i_186\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(13),
      I1 => \caracter_reg[0]_i_21_n_3\,
      O => euros2(13)
    );
\caracter[0]_i_187\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(16),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(19),
      O => \caracter[0]_i_187_n_0\
    );
\caracter[0]_i_188\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(15),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(18),
      O => \caracter[0]_i_188_n_0\
    );
\caracter[0]_i_189\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(14),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(17),
      O => \caracter[0]_i_189_n_0\
    );
\caracter[0]_i_190\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(13),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(16),
      O => \caracter[0]_i_190_n_0\
    );
\caracter[0]_i_191\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => euros3(29),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(24),
      O => \caracter[0]_i_191_n_0\
    );
\caracter[0]_i_192\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(28),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(30),
      I3 => euros3(23),
      O => \caracter[0]_i_192_n_0\
    );
\caracter[0]_i_193\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(27),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(29),
      I3 => euros3(22),
      O => \caracter[0]_i_193_n_0\
    );
\caracter[0]_i_194\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(26),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(28),
      I3 => euros3(21),
      O => \caracter[0]_i_194_n_0\
    );
\caracter[0]_i_195\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00870078"
    )
        port map (
      I0 => euros3(24),
      I1 => euros3(29),
      I2 => euros3(30),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => euros3(25),
      O => \caracter[0]_i_195_n_0\
    );
\caracter[0]_i_196\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000E817000017E8"
    )
        port map (
      I0 => euros3(23),
      I1 => euros3(30),
      I2 => euros3(28),
      I3 => euros3(29),
      I4 => \caracter_reg[0]_i_21_n_3\,
      I5 => euros3(24),
      O => \caracter[0]_i_196_n_0\
    );
\caracter[0]_i_197\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A6A9A9A6"
    )
        port map (
      I0 => \caracter[0]_i_193_n_0\,
      I1 => euros3(28),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(30),
      I4 => euros3(23),
      O => \caracter[0]_i_197_n_0\
    );
\caracter[0]_i_198\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(27),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(29),
      I3 => euros3(22),
      I4 => \caracter[0]_i_194_n_0\,
      O => \caracter[0]_i_198_n_0\
    );
\caracter[0]_i_199\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(12),
      I1 => \caracter_reg[0]_i_21_n_3\,
      O => euros2(12)
    );
\caracter[0]_i_200\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(11),
      I1 => \caracter_reg[0]_i_21_n_3\,
      O => euros2(11)
    );
\caracter[0]_i_201\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(10),
      I1 => \caracter_reg[0]_i_21_n_3\,
      O => euros2(10)
    );
\caracter[0]_i_202\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(9),
      I1 => \caracter_reg[0]_i_21_n_3\,
      O => euros2(9)
    );
\caracter[0]_i_203\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(12),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(15),
      O => \caracter[0]_i_203_n_0\
    );
\caracter[0]_i_204\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(11),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(14),
      O => \caracter[0]_i_204_n_0\
    );
\caracter[0]_i_205\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(10),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(13),
      O => \caracter[0]_i_205_n_0\
    );
\caracter[0]_i_206\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(9),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(12),
      O => \caracter[0]_i_206_n_0\
    );
\caracter[0]_i_207\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(25),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(27),
      I3 => euros3(20),
      O => \caracter[0]_i_207_n_0\
    );
\caracter[0]_i_208\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(24),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(26),
      I3 => euros3(19),
      O => \caracter[0]_i_208_n_0\
    );
\caracter[0]_i_209\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(23),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(25),
      I3 => euros3(18),
      O => \caracter[0]_i_209_n_0\
    );
\caracter[0]_i_210\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(22),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(24),
      I3 => euros3(17),
      O => \caracter[0]_i_210_n_0\
    );
\caracter[0]_i_211\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(26),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(28),
      I3 => euros3(21),
      I4 => \caracter[0]_i_207_n_0\,
      O => \caracter[0]_i_211_n_0\
    );
\caracter[0]_i_212\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(25),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(27),
      I3 => euros3(20),
      I4 => \caracter[0]_i_208_n_0\,
      O => \caracter[0]_i_212_n_0\
    );
\caracter[0]_i_213\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(24),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(26),
      I3 => euros3(19),
      I4 => \caracter[0]_i_209_n_0\,
      O => \caracter[0]_i_213_n_0\
    );
\caracter[0]_i_214\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(23),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(25),
      I3 => euros3(18),
      I4 => \caracter[0]_i_210_n_0\,
      O => \caracter[0]_i_214_n_0\
    );
\caracter[0]_i_215\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(18),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(14),
      I3 => euros3(16),
      O => \caracter[0]_i_215_n_0\
    );
\caracter[0]_i_216\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(17),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(13),
      I3 => euros3(15),
      O => \caracter[0]_i_216_n_0\
    );
\caracter[0]_i_217\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(16),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(12),
      I3 => euros3(14),
      O => \caracter[0]_i_217_n_0\
    );
\caracter[0]_i_218\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(15),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(11),
      I3 => euros3(13),
      O => \caracter[0]_i_218_n_0\
    );
\caracter[0]_i_219\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(19),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(15),
      I3 => euros3(17),
      I4 => \caracter[0]_i_215_n_0\,
      O => \caracter[0]_i_219_n_0\
    );
\caracter[0]_i_220\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(18),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(14),
      I3 => euros3(16),
      I4 => \caracter[0]_i_216_n_0\,
      O => \caracter[0]_i_220_n_0\
    );
\caracter[0]_i_221\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(17),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(13),
      I3 => euros3(15),
      I4 => \caracter[0]_i_217_n_0\,
      O => \caracter[0]_i_221_n_0\
    );
\caracter[0]_i_222\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(16),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(12),
      I3 => euros3(14),
      I4 => \caracter[0]_i_218_n_0\,
      O => \caracter[0]_i_222_n_0\
    );
\caracter[0]_i_223\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(26),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(22),
      I3 => euros3(24),
      O => \caracter[0]_i_223_n_0\
    );
\caracter[0]_i_224\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(25),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(21),
      I3 => euros3(23),
      O => \caracter[0]_i_224_n_0\
    );
\caracter[0]_i_225\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(24),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(20),
      I3 => euros3(22),
      O => \caracter[0]_i_225_n_0\
    );
\caracter[0]_i_226\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(23),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(19),
      I3 => euros3(21),
      O => \caracter[0]_i_226_n_0\
    );
\caracter[0]_i_227\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(27),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(23),
      I3 => euros3(25),
      I4 => \caracter[0]_i_223_n_0\,
      O => \caracter[0]_i_227_n_0\
    );
\caracter[0]_i_228\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(26),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(22),
      I3 => euros3(24),
      I4 => \caracter[0]_i_224_n_0\,
      O => \caracter[0]_i_228_n_0\
    );
\caracter[0]_i_229\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(25),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(21),
      I3 => euros3(23),
      I4 => \caracter[0]_i_225_n_0\,
      O => \caracter[0]_i_229_n_0\
    );
\caracter[0]_i_230\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(24),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(20),
      I3 => euros3(22),
      I4 => \caracter[0]_i_226_n_0\,
      O => \caracter[0]_i_230_n_0\
    );
\caracter[0]_i_231\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(17),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(13),
      I3 => euros3(11),
      O => \caracter[0]_i_231_n_0\
    );
\caracter[0]_i_232\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(16),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(12),
      I3 => euros3(10),
      O => \caracter[0]_i_232_n_0\
    );
\caracter[0]_i_233\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(15),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(11),
      I3 => euros3(9),
      O => \caracter[0]_i_233_n_0\
    );
\caracter[0]_i_234\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(14),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(8),
      I3 => euros3(10),
      O => \caracter[0]_i_234_n_0\
    );
\caracter[0]_i_235\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(18),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(14),
      I3 => euros3(12),
      I4 => \caracter[0]_i_231_n_0\,
      O => \caracter[0]_i_235_n_0\
    );
\caracter[0]_i_236\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(17),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(13),
      I3 => euros3(11),
      I4 => \caracter[0]_i_232_n_0\,
      O => \caracter[0]_i_236_n_0\
    );
\caracter[0]_i_237\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(16),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(12),
      I3 => euros3(10),
      I4 => \caracter[0]_i_233_n_0\,
      O => \caracter[0]_i_237_n_0\
    );
\caracter[0]_i_238\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(15),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(11),
      I3 => euros3(9),
      I4 => \caracter[0]_i_234_n_0\,
      O => \caracter[0]_i_238_n_0\
    );
\caracter[0]_i_240\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60990090"
    )
        port map (
      I0 => \caracter_reg[0]_i_113_n_5\,
      I1 => \caracter_reg[0]_i_248_n_7\,
      I2 => \caracter_reg[0]_i_355_n_4\,
      I3 => \caracter_reg[0]_i_112_n_3\,
      I4 => \caracter_reg[0]_i_113_n_6\,
      O => \caracter[0]_i_240_n_0\
    );
\caracter[0]_i_241\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60990090"
    )
        port map (
      I0 => \caracter_reg[0]_i_113_n_6\,
      I1 => \caracter_reg[0]_i_355_n_4\,
      I2 => \caracter_reg[0]_i_355_n_5\,
      I3 => \caracter_reg[0]_i_112_n_3\,
      I4 => \caracter_reg[0]_i_113_n_7\,
      O => \caracter[0]_i_241_n_0\
    );
\caracter[0]_i_242\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60990090"
    )
        port map (
      I0 => \caracter_reg[0]_i_113_n_7\,
      I1 => \caracter_reg[0]_i_355_n_5\,
      I2 => \caracter_reg[0]_i_355_n_6\,
      I3 => \caracter_reg[0]_i_112_n_3\,
      I4 => \caracter_reg[0]_i_250_n_4\,
      O => \caracter[0]_i_242_n_0\
    );
\caracter[0]_i_243\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E8E800E80000E8"
    )
        port map (
      I0 => \caracter_reg[0]_i_250_n_5\,
      I1 => \caracter_reg[0]_i_249_n_4\,
      I2 => \caracter_reg[0]_i_355_n_7\,
      I3 => \caracter_reg[0]_i_250_n_4\,
      I4 => \caracter_reg[0]_i_112_n_3\,
      I5 => \caracter_reg[0]_i_355_n_6\,
      O => \caracter[0]_i_243_n_0\
    );
\caracter[0]_i_244\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"95566AA96AA99556"
    )
        port map (
      I0 => \caracter[0]_i_240_n_0\,
      I1 => \caracter_reg[0]_i_113_n_5\,
      I2 => \caracter_reg[0]_i_112_n_3\,
      I3 => \caracter_reg[0]_i_248_n_7\,
      I4 => \caracter_reg[0]_i_113_n_0\,
      I5 => \caracter_reg[0]_i_248_n_6\,
      O => \caracter[0]_i_244_n_0\
    );
\caracter[0]_i_245\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AA9955695566AA9"
    )
        port map (
      I0 => \caracter[0]_i_241_n_0\,
      I1 => \caracter_reg[0]_i_113_n_6\,
      I2 => \caracter_reg[0]_i_112_n_3\,
      I3 => \caracter_reg[0]_i_355_n_4\,
      I4 => \caracter_reg[0]_i_248_n_7\,
      I5 => \caracter_reg[0]_i_113_n_5\,
      O => \caracter[0]_i_245_n_0\
    );
\caracter[0]_i_246\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AA9955695566AA9"
    )
        port map (
      I0 => \caracter[0]_i_242_n_0\,
      I1 => \caracter_reg[0]_i_113_n_7\,
      I2 => \caracter_reg[0]_i_112_n_3\,
      I3 => \caracter_reg[0]_i_355_n_5\,
      I4 => \caracter_reg[0]_i_355_n_4\,
      I5 => \caracter_reg[0]_i_113_n_6\,
      O => \caracter[0]_i_246_n_0\
    );
\caracter[0]_i_247\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AA9955695566AA9"
    )
        port map (
      I0 => \caracter[0]_i_243_n_0\,
      I1 => \caracter_reg[0]_i_250_n_4\,
      I2 => \caracter_reg[0]_i_112_n_3\,
      I3 => \caracter_reg[0]_i_355_n_6\,
      I4 => \caracter_reg[0]_i_355_n_5\,
      I5 => \caracter_reg[0]_i_113_n_7\,
      O => \caracter[0]_i_247_n_0\
    );
\caracter[0]_i_251\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(7),
      O => \caracter[0]_i_251_n_0\
    );
\caracter[0]_i_252\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(6),
      O => \caracter[0]_i_252_n_0\
    );
\caracter[0]_i_253\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(5),
      O => \caracter[0]_i_253_n_0\
    );
\caracter[0]_i_254\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(6),
      I1 => \caracter_reg[3]_i_141_0\(4),
      O => \caracter[0]_i_254_n_0\
    );
\caracter[0]_i_255\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(7),
      I1 => \caracter_reg[3]_i_141_0\(5),
      I2 => \caracter_reg[3]_i_141_0\(6),
      O => \caracter[0]_i_255_n_0\
    );
\caracter[0]_i_256\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(4),
      I1 => \caracter_reg[3]_i_141_0\(6),
      I2 => \caracter_reg[3]_i_141_0\(7),
      I3 => \caracter_reg[3]_i_141_0\(5),
      O => \caracter[0]_i_256_n_0\
    );
\caracter[0]_i_258\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_129_n_4\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(20),
      O => \caracter[0]_i_258_n_0\
    );
\caracter[0]_i_259\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_129_n_5\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(19),
      O => \caracter[0]_i_259_n_0\
    );
\caracter[0]_i_26\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_22_n_7\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(29),
      O => \caracter[0]_i_26_n_0\
    );
\caracter[0]_i_260\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_129_n_6\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(18),
      O => \caracter[0]_i_260_n_0\
    );
\caracter[0]_i_261\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_129_n_7\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(17),
      O => \caracter[0]_i_261_n_0\
    );
\caracter[0]_i_262\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(20),
      I1 => \caracter_reg[0]_i_129_n_4\,
      I2 => euros3(21),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_67_n_7\,
      O => \caracter[0]_i_262_n_0\
    );
\caracter[0]_i_263\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(19),
      I1 => \caracter_reg[0]_i_129_n_5\,
      I2 => euros3(20),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_129_n_4\,
      O => \caracter[0]_i_263_n_0\
    );
\caracter[0]_i_264\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(18),
      I1 => \caracter_reg[0]_i_129_n_6\,
      I2 => euros3(19),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_129_n_5\,
      O => \caracter[0]_i_264_n_0\
    );
\caracter[0]_i_265\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(17),
      I1 => \caracter_reg[0]_i_129_n_7\,
      I2 => euros3(18),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_129_n_6\,
      O => \caracter[0]_i_265_n_0\
    );
\caracter[0]_i_268\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_276_n_7\,
      I1 => \caracter_reg[0]_i_276_n_5\,
      I2 => \caracter_reg[0]_i_138_n_6\,
      O => \caracter[0]_i_268_n_0\
    );
\caracter[0]_i_269\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_394_n_4\,
      I1 => \caracter_reg[0]_i_276_n_6\,
      I2 => \caracter_reg[0]_i_138_n_7\,
      O => \caracter[0]_i_269_n_0\
    );
\caracter[0]_i_27\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(29),
      I1 => \caracter_reg[0]_i_22_n_7\,
      I2 => euros3(30),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_22_n_6\,
      O => \caracter[0]_i_27_n_0\
    );
\caracter[0]_i_270\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_394_n_5\,
      I1 => \caracter_reg[0]_i_276_n_7\,
      I2 => \caracter_reg[0]_i_276_n_4\,
      O => \caracter[0]_i_270_n_0\
    );
\caracter[0]_i_271\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_394_n_6\,
      I1 => \caracter_reg[0]_i_394_n_4\,
      I2 => \caracter_reg[0]_i_276_n_5\,
      O => \caracter[0]_i_271_n_0\
    );
\caracter[0]_i_272\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_276_n_6\,
      I1 => \caracter_reg[0]_i_276_n_4\,
      I2 => \caracter_reg[0]_i_138_n_5\,
      I3 => \caracter[0]_i_268_n_0\,
      O => \caracter[0]_i_272_n_0\
    );
\caracter[0]_i_273\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_276_n_7\,
      I1 => \caracter_reg[0]_i_276_n_5\,
      I2 => \caracter_reg[0]_i_138_n_6\,
      I3 => \caracter[0]_i_269_n_0\,
      O => \caracter[0]_i_273_n_0\
    );
\caracter[0]_i_274\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_394_n_4\,
      I1 => \caracter_reg[0]_i_276_n_6\,
      I2 => \caracter_reg[0]_i_138_n_7\,
      I3 => \caracter[0]_i_270_n_0\,
      O => \caracter[0]_i_274_n_0\
    );
\caracter[0]_i_275\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_394_n_5\,
      I1 => \caracter_reg[0]_i_276_n_7\,
      I2 => \caracter_reg[0]_i_276_n_4\,
      I3 => \caracter[0]_i_271_n_0\,
      O => \caracter[0]_i_275_n_0\
    );
\caracter[0]_i_277\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"800E08E0"
    )
        port map (
      I0 => \caracter_reg[0]_i_403_n_4\,
      I1 => \caracter_reg[0]_i_288_n_6\,
      I2 => \caracter_reg[0]_i_285_n_7\,
      I3 => \caracter_reg[0]_i_287_n_0\,
      I4 => \caracter_reg[0]_i_288_n_1\,
      O => \caracter[0]_i_277_n_0\
    );
\caracter[0]_i_278\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60990090"
    )
        port map (
      I0 => \caracter_reg[0]_i_288_n_6\,
      I1 => \caracter_reg[0]_i_403_n_4\,
      I2 => \caracter_reg[0]_i_403_n_5\,
      I3 => \caracter_reg[0]_i_287_n_0\,
      I4 => \caracter_reg[0]_i_288_n_7\,
      O => \caracter[0]_i_278_n_0\
    );
\caracter[0]_i_279\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60990090"
    )
        port map (
      I0 => \caracter_reg[0]_i_288_n_7\,
      I1 => \caracter_reg[0]_i_403_n_5\,
      I2 => \caracter_reg[0]_i_403_n_6\,
      I3 => \caracter_reg[0]_i_287_n_0\,
      I4 => \caracter_reg[0]_i_404_n_4\,
      O => \caracter[0]_i_279_n_0\
    );
\caracter[0]_i_280\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60990090"
    )
        port map (
      I0 => \caracter_reg[0]_i_404_n_4\,
      I1 => \caracter_reg[0]_i_403_n_6\,
      I2 => \caracter_reg[0]_i_403_n_7\,
      I3 => \caracter_reg[0]_i_287_n_0\,
      I4 => \caracter_reg[0]_i_404_n_5\,
      O => \caracter[0]_i_280_n_0\
    );
\caracter[0]_i_281\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"366CC993"
    )
        port map (
      I0 => \caracter_reg[0]_i_285_n_7\,
      I1 => \caracter_reg[0]_i_285_n_6\,
      I2 => \caracter_reg[0]_i_287_n_0\,
      I3 => \caracter_reg[0]_i_288_n_1\,
      I4 => \caracter[0]_i_277_n_0\,
      O => \caracter[0]_i_281_n_0\
    );
\caracter[0]_i_282\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669669966996996"
    )
        port map (
      I0 => \caracter[0]_i_278_n_0\,
      I1 => \caracter_reg[0]_i_288_n_1\,
      I2 => \caracter_reg[0]_i_287_n_0\,
      I3 => \caracter_reg[0]_i_285_n_7\,
      I4 => \caracter_reg[0]_i_288_n_6\,
      I5 => \caracter_reg[0]_i_403_n_4\,
      O => \caracter[0]_i_282_n_0\
    );
\caracter[0]_i_283\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AA9955695566AA9"
    )
        port map (
      I0 => \caracter[0]_i_279_n_0\,
      I1 => \caracter_reg[0]_i_288_n_7\,
      I2 => \caracter_reg[0]_i_287_n_0\,
      I3 => \caracter_reg[0]_i_403_n_5\,
      I4 => \caracter_reg[0]_i_403_n_4\,
      I5 => \caracter_reg[0]_i_288_n_6\,
      O => \caracter[0]_i_283_n_0\
    );
\caracter[0]_i_284\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AA9955695566AA9"
    )
        port map (
      I0 => \caracter[0]_i_280_n_0\,
      I1 => \caracter_reg[0]_i_404_n_4\,
      I2 => \caracter_reg[0]_i_287_n_0\,
      I3 => \caracter_reg[0]_i_403_n_6\,
      I4 => \caracter_reg[0]_i_403_n_5\,
      I5 => \caracter_reg[0]_i_288_n_7\,
      O => \caracter[0]_i_284_n_0\
    );
\caracter[0]_i_290\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_301_n_6\,
      I1 => euros2(2),
      I2 => \caracter_reg[0]_i_303_n_5\,
      I3 => \caracter_reg[0]_i_305_n_5\,
      I4 => \caracter[0]_i_437_n_0\,
      O => \caracter[0]_i_290_n_0\
    );
\caracter[0]_i_291\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DDD4D444"
    )
        port map (
      I0 => \caracter[0]_i_438_n_0\,
      I1 => \caracter_reg[0]_i_305_n_6\,
      I2 => euros2(1),
      I3 => \caracter_reg[0]_i_301_n_7\,
      I4 => \caracter_reg[0]_i_303_n_6\,
      O => \caracter[0]_i_291_n_0\
    );
\caracter[0]_i_292\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DDD4D444"
    )
        port map (
      I0 => \caracter[0]_i_440_n_0\,
      I1 => \caracter_reg[0]_i_305_n_7\,
      I2 => \caracter_reg[0]_i_441_n_4\,
      I3 => euros2(0),
      I4 => \caracter_reg[0]_i_303_n_7\,
      O => \caracter[0]_i_292_n_0\
    );
\caracter[0]_i_293\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EBBE822882288228"
    )
        port map (
      I0 => \caracter_reg[0]_i_442_n_4\,
      I1 => \caracter_reg[0]_i_441_n_4\,
      I2 => euros2(0),
      I3 => \caracter_reg[0]_i_303_n_7\,
      I4 => \caracter_reg[0]_i_441_n_5\,
      I5 => \caracter_reg[0]_i_443_n_4\,
      O => \caracter[0]_i_293_n_0\
    );
\caracter[0]_i_294\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \caracter[0]_i_290_n_0\,
      I1 => \caracter_reg[0]_i_305_n_4\,
      I2 => \caracter[0]_i_304_n_0\,
      I3 => \caracter_reg[0]_i_298_n_7\,
      I4 => \caracter_reg[0]_i_303_n_4\,
      I5 => \caracter_reg[0]_i_301_n_5\,
      O => \caracter[0]_i_294_n_0\
    );
\caracter[0]_i_295\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"566AA995A995566A"
    )
        port map (
      I0 => \caracter[0]_i_291_n_0\,
      I1 => \caracter_reg[0]_i_301_n_6\,
      I2 => euros2(2),
      I3 => \caracter_reg[0]_i_303_n_5\,
      I4 => \caracter_reg[0]_i_305_n_5\,
      I5 => \caracter[0]_i_437_n_0\,
      O => \caracter[0]_i_295_n_0\
    );
\caracter[0]_i_296\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A995566A566AA995"
    )
        port map (
      I0 => \caracter[0]_i_292_n_0\,
      I1 => euros2(1),
      I2 => \caracter_reg[0]_i_301_n_7\,
      I3 => \caracter_reg[0]_i_303_n_6\,
      I4 => \caracter_reg[0]_i_305_n_6\,
      I5 => \caracter[0]_i_438_n_0\,
      O => \caracter[0]_i_296_n_0\
    );
\caracter[0]_i_297\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9696966996696969"
    )
        port map (
      I0 => \caracter[0]_i_293_n_0\,
      I1 => \caracter[0]_i_440_n_0\,
      I2 => \caracter_reg[0]_i_305_n_7\,
      I3 => \caracter_reg[0]_i_441_n_4\,
      I4 => euros2(0),
      I5 => \caracter_reg[0]_i_303_n_7\,
      O => \caracter[0]_i_297_n_0\
    );
\caracter[0]_i_299\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_160_n_5\,
      I1 => \caracter_reg[0]_i_162_n_4\,
      I2 => \caracter_reg[0]_i_157_n_7\,
      O => \caracter[0]_i_299_n_0\
    );
\caracter[0]_i_300\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_160_n_6\,
      I1 => \caracter_reg[0]_i_162_n_5\,
      I2 => \caracter_reg[0]_i_298_n_4\,
      O => \caracter[0]_i_300_n_0\
    );
\caracter[0]_i_302\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_160_n_7\,
      I1 => \caracter_reg[0]_i_162_n_6\,
      I2 => \caracter_reg[0]_i_298_n_5\,
      O => \caracter[0]_i_302_n_0\
    );
\caracter[0]_i_304\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_301_n_4\,
      I1 => \caracter_reg[0]_i_162_n_7\,
      I2 => \caracter_reg[0]_i_298_n_6\,
      O => \caracter[0]_i_304_n_0\
    );
\caracter[0]_i_306\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter_reg[0]_i_157_n_7\,
      I1 => \caracter_reg[0]_i_162_n_4\,
      I2 => \caracter_reg[0]_i_160_n_5\,
      O => \caracter[0]_i_306_n_0\
    );
\caracter[0]_i_307\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter_reg[0]_i_298_n_4\,
      I1 => \caracter_reg[0]_i_162_n_5\,
      I2 => \caracter_reg[0]_i_160_n_6\,
      O => \caracter[0]_i_307_n_0\
    );
\caracter[0]_i_308\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter_reg[0]_i_298_n_5\,
      I1 => \caracter_reg[0]_i_162_n_6\,
      I2 => \caracter_reg[0]_i_160_n_7\,
      O => \caracter[0]_i_308_n_0\
    );
\caracter[0]_i_309\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter_reg[0]_i_298_n_6\,
      I1 => \caracter_reg[0]_i_162_n_7\,
      I2 => \caracter_reg[0]_i_301_n_4\,
      O => \caracter[0]_i_309_n_0\
    );
\caracter[0]_i_31\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \caracter_reg[0]_i_76_n_6\,
      I1 => \caracter_reg[0]_i_76_n_4\,
      O => \caracter[0]_i_31_n_0\
    );
\caracter[0]_i_310\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCFA00FACCA000A0"
    )
        port map (
      I0 => euros3(5),
      I1 => \caracter_reg[0]_i_29_n_6\,
      I2 => euros3(3),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_62_n_4\,
      I5 => euros3(9),
      O => \caracter[0]_i_310_n_0\
    );
\caracter[0]_i_311\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCFA00FACCA000A0"
    )
        port map (
      I0 => euros3(2),
      I1 => \caracter_reg[0]_i_62_n_5\,
      I2 => euros3(4),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_29_n_7\,
      I5 => euros3(8),
      O => \caracter[0]_i_311_n_0\
    );
\caracter[0]_i_312\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DFD54F45D5D04540"
    )
        port map (
      I0 => \caracter[0]_i_475_n_0\,
      I1 => \caracter_reg[0]_i_62_n_4\,
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(3),
      I4 => \caracter_reg[0]_i_62_n_6\,
      I5 => euros3(1),
      O => \caracter[0]_i_312_n_0\
    );
\caracter[0]_i_313\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"656A959A6A659A95"
    )
        port map (
      I0 => \caracter[0]_i_475_n_0\,
      I1 => \caracter_reg[0]_i_62_n_6\,
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(1),
      I4 => \caracter_reg[0]_i_62_n_4\,
      I5 => euros3(3),
      O => \caracter[0]_i_313_n_0\
    );
\caracter[0]_i_314\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A6A956595956A9A6"
    )
        port map (
      I0 => \caracter[0]_i_310_n_0\,
      I1 => euros3(10),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(6),
      I4 => \caracter_reg[0]_i_29_n_5\,
      I5 => euros2(4),
      O => \caracter[0]_i_314_n_0\
    );
\caracter[0]_i_315\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A659A95656A959A6"
    )
        port map (
      I0 => \caracter[0]_i_311_n_0\,
      I1 => euros3(9),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros2(5),
      I4 => euros3(3),
      I5 => \caracter_reg[0]_i_62_n_4\,
      O => \caracter[0]_i_315_n_0\
    );
\caracter[0]_i_316\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A6A956595956A9A6"
    )
        port map (
      I0 => \caracter[0]_i_312_n_0\,
      I1 => euros3(8),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(4),
      I4 => \caracter_reg[0]_i_29_n_7\,
      I5 => euros2(2),
      O => \caracter[0]_i_316_n_0\
    );
\caracter[0]_i_317\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696969"
    )
        port map (
      I0 => euros2(3),
      I1 => euros2(1),
      I2 => \caracter[0]_i_475_n_0\,
      I3 => euros2(0),
      I4 => euros2(2),
      O => \caracter[0]_i_317_n_0\
    );
\caracter[0]_i_318\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(8),
      I1 => \caracter_reg[0]_i_21_n_3\,
      O => euros2(8)
    );
\caracter[0]_i_319\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E2"
    )
        port map (
      I0 => euros3(7),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => \caracter_reg[0]_i_29_n_4\,
      O => \caracter[0]_i_319_n_0\
    );
\caracter[0]_i_32\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"65"
    )
        port map (
      I0 => \caracter_reg[0]_i_76_n_4\,
      I1 => \caracter_reg[0]_i_77_n_7\,
      I2 => \caracter_reg[0]_i_76_n_5\,
      O => \caracter[0]_i_32_n_0\
    );
\caracter[0]_i_320\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E2"
    )
        port map (
      I0 => euros3(6),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => \caracter_reg[0]_i_29_n_5\,
      O => \caracter[0]_i_320_n_0\
    );
\caracter[0]_i_321\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_29_n_6\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(5),
      O => \caracter[0]_i_321_n_0\
    );
\caracter[0]_i_322\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(8),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(11),
      O => \caracter[0]_i_322_n_0\
    );
\caracter[0]_i_323\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3A35"
    )
        port map (
      I0 => euros3(7),
      I1 => \caracter_reg[0]_i_29_n_4\,
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(10),
      O => \caracter[0]_i_323_n_0\
    );
\caracter[0]_i_324\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3A35"
    )
        port map (
      I0 => euros3(6),
      I1 => \caracter_reg[0]_i_29_n_5\,
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(9),
      O => \caracter[0]_i_324_n_0\
    );
\caracter[0]_i_325\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3A35"
    )
        port map (
      I0 => euros3(5),
      I1 => \caracter_reg[0]_i_29_n_6\,
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(8),
      O => \caracter[0]_i_325_n_0\
    );
\caracter[0]_i_326\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(14),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(10),
      I3 => euros3(12),
      O => \caracter[0]_i_326_n_0\
    );
\caracter[0]_i_327\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(13),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(9),
      I3 => euros3(11),
      O => \caracter[0]_i_327_n_0\
    );
\caracter[0]_i_328\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(12),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(8),
      I3 => euros3(10),
      O => \caracter[0]_i_328_n_0\
    );
\caracter[0]_i_329\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFCD"
    )
        port map (
      I0 => euros3(9),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(11),
      I3 => euros3(7),
      O => \caracter[0]_i_329_n_0\
    );
\caracter[0]_i_33\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B44B"
    )
        port map (
      I0 => \caracter_reg[0]_i_76_n_4\,
      I1 => \caracter_reg[0]_i_76_n_6\,
      I2 => \caracter_reg[0]_i_77_n_7\,
      I3 => \caracter_reg[0]_i_76_n_5\,
      O => \caracter[0]_i_33_n_0\
    );
\caracter[0]_i_330\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(15),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(11),
      I3 => euros3(13),
      I4 => \caracter[0]_i_326_n_0\,
      O => \caracter[0]_i_330_n_0\
    );
\caracter[0]_i_331\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(14),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(10),
      I3 => euros3(12),
      I4 => \caracter[0]_i_327_n_0\,
      O => \caracter[0]_i_331_n_0\
    );
\caracter[0]_i_332\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(13),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(9),
      I3 => euros3(11),
      I4 => \caracter[0]_i_328_n_0\,
      O => \caracter[0]_i_332_n_0\
    );
\caracter[0]_i_333\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(12),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(8),
      I3 => euros3(10),
      I4 => \caracter[0]_i_329_n_0\,
      O => \caracter[0]_i_333_n_0\
    );
\caracter[0]_i_334\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(21),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(23),
      I3 => euros3(16),
      O => \caracter[0]_i_334_n_0\
    );
\caracter[0]_i_335\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(20),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(22),
      I3 => euros3(15),
      O => \caracter[0]_i_335_n_0\
    );
\caracter[0]_i_336\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(19),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(21),
      I3 => euros3(14),
      O => \caracter[0]_i_336_n_0\
    );
\caracter[0]_i_337\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(18),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(20),
      I3 => euros3(13),
      O => \caracter[0]_i_337_n_0\
    );
\caracter[0]_i_338\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(22),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(24),
      I3 => euros3(17),
      I4 => \caracter[0]_i_334_n_0\,
      O => \caracter[0]_i_338_n_0\
    );
\caracter[0]_i_339\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(21),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(23),
      I3 => euros3(16),
      I4 => \caracter[0]_i_335_n_0\,
      O => \caracter[0]_i_339_n_0\
    );
\caracter[0]_i_340\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(20),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(22),
      I3 => euros3(15),
      I4 => \caracter[0]_i_336_n_0\,
      O => \caracter[0]_i_340_n_0\
    );
\caracter[0]_i_341\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(19),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(21),
      I3 => euros3(14),
      I4 => \caracter[0]_i_337_n_0\,
      O => \caracter[0]_i_341_n_0\
    );
\caracter[0]_i_345\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_29_n_6\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(5),
      O => euros2(5)
    );
\caracter[0]_i_347\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E80000E800E8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_250_n_6\,
      I1 => \caracter_reg[0]_i_249_n_5\,
      I2 => \caracter_reg[3]_i_141_0\(2),
      I3 => \caracter_reg[0]_i_249_n_4\,
      I4 => \caracter_reg[0]_i_355_n_7\,
      I5 => \caracter_reg[0]_i_250_n_5\,
      O => \caracter[0]_i_347_n_0\
    );
\caracter[0]_i_348\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E80000E800E8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_250_n_7\,
      I1 => \caracter_reg[0]_i_249_n_6\,
      I2 => \caracter_reg[3]_i_141_0\(1),
      I3 => \caracter_reg[3]_i_141_0\(2),
      I4 => \caracter_reg[0]_i_249_n_5\,
      I5 => \caracter_reg[0]_i_250_n_6\,
      O => \caracter[0]_i_348_n_0\
    );
\caracter[0]_i_349\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E80000E800E8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_371_n_4\,
      I1 => \caracter_reg[0]_i_249_n_7\,
      I2 => \caracter_reg[3]_i_141_0\(0),
      I3 => \caracter_reg[3]_i_141_0\(1),
      I4 => \caracter_reg[0]_i_249_n_6\,
      I5 => \caracter_reg[0]_i_250_n_7\,
      O => \caracter[0]_i_349_n_0\
    );
\caracter[0]_i_35\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_87_n_4\,
      I1 => \caracter_reg[0]_i_88_n_5\,
      I2 => \caracter_reg[0]_i_89_n_6\,
      I3 => \caracter_reg[0]_i_90_n_5\,
      I4 => \caracter[0]_i_91_n_0\,
      O => \caracter[0]_i_35_n_0\
    );
\caracter[0]_i_350\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80080880"
    )
        port map (
      I0 => \caracter_reg[0]_i_364_n_4\,
      I1 => \caracter_reg[0]_i_371_n_5\,
      I2 => \caracter_reg[3]_i_141_0\(0),
      I3 => \caracter_reg[0]_i_249_n_7\,
      I4 => \caracter_reg[0]_i_371_n_4\,
      O => \caracter[0]_i_350_n_0\
    );
\caracter[0]_i_351\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99969666"
    )
        port map (
      I0 => \caracter[0]_i_347_n_0\,
      I1 => \caracter[0]_i_495_n_0\,
      I2 => \caracter_reg[0]_i_355_n_7\,
      I3 => \caracter_reg[0]_i_249_n_4\,
      I4 => \caracter_reg[0]_i_250_n_5\,
      O => \caracter[0]_i_351_n_0\
    );
\caracter[0]_i_352\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66696999"
    )
        port map (
      I0 => \caracter[0]_i_348_n_0\,
      I1 => \caracter[0]_i_496_n_0\,
      I2 => \caracter_reg[3]_i_141_0\(2),
      I3 => \caracter_reg[0]_i_249_n_5\,
      I4 => \caracter_reg[0]_i_250_n_6\,
      O => \caracter[0]_i_352_n_0\
    );
\caracter[0]_i_353\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66696999"
    )
        port map (
      I0 => \caracter[0]_i_349_n_0\,
      I1 => \caracter[0]_i_497_n_0\,
      I2 => \caracter_reg[3]_i_141_0\(1),
      I3 => \caracter_reg[0]_i_249_n_6\,
      I4 => \caracter_reg[0]_i_250_n_7\,
      O => \caracter[0]_i_353_n_0\
    );
\caracter[0]_i_354\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66696999"
    )
        port map (
      I0 => \caracter[0]_i_350_n_0\,
      I1 => \caracter[0]_i_498_n_0\,
      I2 => \caracter_reg[3]_i_141_0\(0),
      I3 => \caracter_reg[0]_i_249_n_7\,
      I4 => \caracter_reg[0]_i_371_n_4\,
      O => \caracter[0]_i_354_n_0\
    );
\caracter[0]_i_356\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(5),
      I1 => \caracter_reg[3]_i_141_0\(3),
      O => \caracter[0]_i_356_n_0\
    );
\caracter[0]_i_357\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(2),
      I1 => \caracter_reg[3]_i_141_0\(4),
      O => \caracter[0]_i_357_n_0\
    );
\caracter[0]_i_358\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(1),
      I1 => \caracter_reg[3]_i_141_0\(3),
      I2 => \caracter_reg[3]_i_141_0\(7),
      O => \caracter[0]_i_358_n_0\
    );
\caracter[0]_i_359\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(7),
      I1 => \caracter_reg[3]_i_141_0\(1),
      I2 => \caracter_reg[3]_i_141_0\(3),
      O => \caracter[0]_i_359_n_0\
    );
\caracter[0]_i_36\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \caracter_reg[0]_i_90_n_6\,
      I1 => \caracter[0]_i_92_n_0\,
      I2 => \caracter_reg[0]_i_87_n_5\,
      I3 => \caracter_reg[0]_i_88_n_6\,
      I4 => \caracter_reg[0]_i_89_n_7\,
      O => \caracter[0]_i_36_n_0\
    );
\caracter[0]_i_360\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(3),
      I1 => \caracter_reg[3]_i_141_0\(5),
      I2 => \caracter_reg[3]_i_141_0\(4),
      I3 => \caracter_reg[3]_i_141_0\(6),
      O => \caracter[0]_i_360_n_0\
    );
\caracter[0]_i_361\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(4),
      I1 => \caracter_reg[3]_i_141_0\(2),
      I2 => \caracter_reg[3]_i_141_0\(3),
      I3 => \caracter_reg[3]_i_141_0\(5),
      O => \caracter[0]_i_361_n_0\
    );
\caracter[0]_i_362\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E81717E8"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(7),
      I1 => \caracter_reg[3]_i_141_0\(3),
      I2 => \caracter_reg[3]_i_141_0\(1),
      I3 => \caracter_reg[3]_i_141_0\(2),
      I4 => \caracter_reg[3]_i_141_0\(4),
      O => \caracter[0]_i_362_n_0\
    );
\caracter[0]_i_363\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(3),
      I1 => \caracter_reg[3]_i_141_0\(1),
      I2 => \caracter_reg[3]_i_141_0\(7),
      I3 => \caracter_reg[3]_i_141_0\(2),
      I4 => \caracter_reg[3]_i_141_0\(0),
      O => \caracter[0]_i_363_n_0\
    );
\caracter[0]_i_365\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(4),
      I1 => \caracter_reg[3]_i_141_0\(6),
      O => \caracter[0]_i_365_n_0\
    );
\caracter[0]_i_366\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(6),
      I1 => \caracter_reg[3]_i_141_0\(4),
      O => \caracter[0]_i_366_n_0\
    );
\caracter[0]_i_367\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(7),
      O => \caracter[0]_i_367_n_0\
    );
\caracter[0]_i_368\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2D"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(7),
      I1 => \caracter_reg[3]_i_141_0\(5),
      I2 => \caracter_reg[3]_i_141_0\(6),
      O => \caracter[0]_i_368_n_0\
    );
\caracter[0]_i_369\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"D22D"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(6),
      I1 => \caracter_reg[3]_i_141_0\(4),
      I2 => \caracter_reg[3]_i_141_0\(7),
      I3 => \caracter_reg[3]_i_141_0\(5),
      O => \caracter[0]_i_369_n_0\
    );
\caracter[0]_i_37\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_87_n_6\,
      I1 => \caracter_reg[0]_i_88_n_7\,
      I2 => \caracter_reg[0]_i_93_n_4\,
      I3 => \caracter_reg[0]_i_90_n_7\,
      I4 => \caracter[0]_i_94_n_0\,
      O => \caracter[0]_i_37_n_0\
    );
\caracter[0]_i_370\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4DB2B24D"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(7),
      I1 => \caracter_reg[3]_i_141_0\(3),
      I2 => \caracter_reg[3]_i_141_0\(5),
      I3 => \caracter_reg[3]_i_141_0\(4),
      I4 => \caracter_reg[3]_i_141_0\(6),
      O => \caracter[0]_i_370_n_0\
    );
\caracter[0]_i_372\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(4),
      I1 => \caracter_reg[3]_i_141_0\(7),
      O => \caracter[0]_i_372_n_0\
    );
\caracter[0]_i_373\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(3),
      I1 => \caracter_reg[3]_i_141_0\(6),
      O => \caracter[0]_i_373_n_0\
    );
\caracter[0]_i_374\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(2),
      I1 => \caracter_reg[3]_i_141_0\(5),
      O => \caracter[0]_i_374_n_0\
    );
\caracter[0]_i_375\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(1),
      I1 => \caracter_reg[3]_i_141_0\(4),
      O => \caracter[0]_i_375_n_0\
    );
\caracter[0]_i_377\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_267_n_4\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(16),
      O => \caracter[0]_i_377_n_0\
    );
\caracter[0]_i_378\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_267_n_5\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(15),
      O => \caracter[0]_i_378_n_0\
    );
\caracter[0]_i_379\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_267_n_6\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(14),
      O => \caracter[0]_i_379_n_0\
    );
\caracter[0]_i_38\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \caracter_reg[0]_i_95_n_4\,
      I1 => \caracter[0]_i_96_n_0\,
      I2 => \caracter_reg[0]_i_87_n_7\,
      I3 => \caracter_reg[0]_i_97_n_4\,
      I4 => \caracter_reg[0]_i_93_n_5\,
      O => \caracter[0]_i_38_n_0\
    );
\caracter[0]_i_380\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_267_n_7\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(13),
      O => \caracter[0]_i_380_n_0\
    );
\caracter[0]_i_381\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(16),
      I1 => \caracter_reg[0]_i_267_n_4\,
      I2 => euros3(17),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_129_n_7\,
      O => \caracter[0]_i_381_n_0\
    );
\caracter[0]_i_382\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(15),
      I1 => \caracter_reg[0]_i_267_n_5\,
      I2 => euros3(16),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_267_n_4\,
      O => \caracter[0]_i_382_n_0\
    );
\caracter[0]_i_383\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(14),
      I1 => \caracter_reg[0]_i_267_n_6\,
      I2 => euros3(15),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_267_n_5\,
      O => \caracter[0]_i_383_n_0\
    );
\caracter[0]_i_384\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(13),
      I1 => \caracter_reg[0]_i_267_n_7\,
      I2 => euros3(14),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_267_n_6\,
      O => \caracter[0]_i_384_n_0\
    );
\caracter[0]_i_386\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_394_n_7\,
      I1 => \caracter_reg[0]_i_394_n_5\,
      I2 => \caracter_reg[0]_i_276_n_6\,
      O => \caracter[0]_i_386_n_0\
    );
\caracter[0]_i_387\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_532_n_4\,
      I1 => \caracter_reg[0]_i_394_n_6\,
      I2 => \caracter_reg[0]_i_276_n_7\,
      O => \caracter[0]_i_387_n_0\
    );
\caracter[0]_i_388\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_532_n_5\,
      I1 => \caracter_reg[0]_i_394_n_7\,
      I2 => \caracter_reg[0]_i_394_n_4\,
      O => \caracter[0]_i_388_n_0\
    );
\caracter[0]_i_389\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_532_n_6\,
      I1 => \caracter_reg[0]_i_532_n_4\,
      I2 => \caracter_reg[0]_i_394_n_5\,
      O => \caracter[0]_i_389_n_0\
    );
\caracter[0]_i_39\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \caracter[0]_i_35_n_0\,
      I1 => \caracter[0]_i_98_n_0\,
      I2 => \caracter_reg[0]_i_89_n_4\,
      I3 => \caracter_reg[0]_i_99_n_7\,
      I4 => \caracter_reg[0]_i_100_n_6\,
      I5 => \caracter_reg[0]_i_90_n_4\,
      O => \caracter[0]_i_39_n_0\
    );
\caracter[0]_i_390\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_394_n_6\,
      I1 => \caracter_reg[0]_i_394_n_4\,
      I2 => \caracter_reg[0]_i_276_n_5\,
      I3 => \caracter[0]_i_386_n_0\,
      O => \caracter[0]_i_390_n_0\
    );
\caracter[0]_i_391\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_394_n_7\,
      I1 => \caracter_reg[0]_i_394_n_5\,
      I2 => \caracter_reg[0]_i_276_n_6\,
      I3 => \caracter[0]_i_387_n_0\,
      O => \caracter[0]_i_391_n_0\
    );
\caracter[0]_i_392\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_532_n_4\,
      I1 => \caracter_reg[0]_i_394_n_6\,
      I2 => \caracter_reg[0]_i_276_n_7\,
      I3 => \caracter[0]_i_388_n_0\,
      O => \caracter[0]_i_392_n_0\
    );
\caracter[0]_i_393\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_532_n_5\,
      I1 => \caracter_reg[0]_i_394_n_7\,
      I2 => \caracter_reg[0]_i_394_n_4\,
      I3 => \caracter[0]_i_389_n_0\,
      O => \caracter[0]_i_393_n_0\
    );
\caracter[0]_i_395\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E8E800E80000E8"
    )
        port map (
      I0 => \caracter_reg[0]_i_404_n_6\,
      I1 => \caracter_reg[0]_i_287_n_5\,
      I2 => \caracter_reg[0]_i_541_n_4\,
      I3 => \caracter_reg[0]_i_404_n_5\,
      I4 => \caracter_reg[0]_i_287_n_0\,
      I5 => \caracter_reg[0]_i_403_n_7\,
      O => \caracter[0]_i_395_n_0\
    );
\caracter[0]_i_396\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E80000E800E8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_404_n_7\,
      I1 => \caracter_reg[0]_i_287_n_6\,
      I2 => \caracter_reg[0]_i_541_n_5\,
      I3 => \caracter_reg[0]_i_287_n_5\,
      I4 => \caracter_reg[0]_i_541_n_4\,
      I5 => \caracter_reg[0]_i_404_n_6\,
      O => \caracter[0]_i_396_n_0\
    );
\caracter[0]_i_397\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E80000E800E8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_542_n_4\,
      I1 => \caracter_reg[0]_i_287_n_7\,
      I2 => \caracter_reg[0]_i_541_n_6\,
      I3 => \caracter_reg[0]_i_287_n_6\,
      I4 => \caracter_reg[0]_i_541_n_5\,
      I5 => \caracter_reg[0]_i_404_n_7\,
      O => \caracter[0]_i_397_n_0\
    );
\caracter[0]_i_398\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E80000E800E8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_542_n_5\,
      I1 => \caracter_reg[0]_i_416_n_4\,
      I2 => \caracter_reg[0]_i_541_n_7\,
      I3 => \caracter_reg[0]_i_287_n_7\,
      I4 => \caracter_reg[0]_i_541_n_6\,
      I5 => \caracter_reg[0]_i_542_n_4\,
      O => \caracter[0]_i_398_n_0\
    );
\caracter[0]_i_399\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AA9955695566AA9"
    )
        port map (
      I0 => \caracter[0]_i_395_n_0\,
      I1 => \caracter_reg[0]_i_404_n_5\,
      I2 => \caracter_reg[0]_i_287_n_0\,
      I3 => \caracter_reg[0]_i_403_n_7\,
      I4 => \caracter_reg[0]_i_403_n_6\,
      I5 => \caracter_reg[0]_i_404_n_4\,
      O => \caracter[0]_i_399_n_0\
    );
\caracter[0]_i_40\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \caracter[0]_i_36_n_0\,
      I1 => \caracter[0]_i_101_n_0\,
      I2 => \caracter_reg[0]_i_89_n_5\,
      I3 => \caracter_reg[0]_i_88_n_4\,
      I4 => \caracter_reg[0]_i_100_n_7\,
      I5 => \caracter_reg[0]_i_90_n_5\,
      O => \caracter[0]_i_40_n_0\
    );
\caracter[0]_i_400\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99969666"
    )
        port map (
      I0 => \caracter[0]_i_396_n_0\,
      I1 => \caracter[0]_i_543_n_0\,
      I2 => \caracter_reg[0]_i_541_n_4\,
      I3 => \caracter_reg[0]_i_287_n_5\,
      I4 => \caracter_reg[0]_i_404_n_6\,
      O => \caracter[0]_i_400_n_0\
    );
\caracter[0]_i_401\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66696999"
    )
        port map (
      I0 => \caracter[0]_i_397_n_0\,
      I1 => \caracter[0]_i_544_n_0\,
      I2 => \caracter_reg[0]_i_541_n_5\,
      I3 => \caracter_reg[0]_i_287_n_6\,
      I4 => \caracter_reg[0]_i_404_n_7\,
      O => \caracter[0]_i_401_n_0\
    );
\caracter[0]_i_402\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66696999"
    )
        port map (
      I0 => \caracter[0]_i_398_n_0\,
      I1 => \caracter[0]_i_545_n_0\,
      I2 => \caracter_reg[0]_i_541_n_6\,
      I3 => \caracter_reg[0]_i_287_n_7\,
      I4 => \caracter_reg[0]_i_542_n_4\,
      O => \caracter[0]_i_402_n_0\
    );
\caracter[0]_i_405\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => euros3(29),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(27),
      O => \caracter[0]_i_405_n_0\
    );
\caracter[0]_i_406\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => euros3(28),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(26),
      O => \caracter[0]_i_406_n_0\
    );
\caracter[0]_i_407\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => euros3(27),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(25),
      O => \caracter[0]_i_407_n_0\
    );
\caracter[0]_i_408\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => euros3(25),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(27),
      O => \caracter[0]_i_408_n_0\
    );
\caracter[0]_i_409\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00870078"
    )
        port map (
      I0 => euros3(27),
      I1 => euros3(29),
      I2 => euros3(30),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => euros3(28),
      O => \caracter[0]_i_409_n_0\
    );
\caracter[0]_i_41\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \caracter[0]_i_37_n_0\,
      I1 => \caracter_reg[0]_i_90_n_6\,
      I2 => \caracter[0]_i_92_n_0\,
      I3 => \caracter_reg[0]_i_87_n_5\,
      I4 => \caracter_reg[0]_i_88_n_6\,
      I5 => \caracter_reg[0]_i_89_n_7\,
      O => \caracter[0]_i_41_n_0\
    );
\caracter[0]_i_410\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00870078"
    )
        port map (
      I0 => euros3(26),
      I1 => euros3(28),
      I2 => euros3(29),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => euros3(27),
      O => \caracter[0]_i_410_n_0\
    );
\caracter[0]_i_411\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00870078"
    )
        port map (
      I0 => euros3(25),
      I1 => euros3(27),
      I2 => euros3(28),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => euros3(26),
      O => \caracter[0]_i_411_n_0\
    );
\caracter[0]_i_412\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000E817000017E8"
    )
        port map (
      I0 => euros3(24),
      I1 => euros3(26),
      I2 => euros3(30),
      I3 => euros3(27),
      I4 => \caracter_reg[0]_i_21_n_3\,
      I5 => euros3(25),
      O => \caracter[0]_i_412_n_0\
    );
\caracter[0]_i_413\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => euros3(30),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(28),
      O => \caracter[0]_i_413_n_0\
    );
\caracter[0]_i_414\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(30),
      I1 => \caracter_reg[0]_i_21_n_3\,
      O => euros2(30)
    );
\caracter[0]_i_415\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0708"
    )
        port map (
      I0 => euros3(28),
      I1 => euros3(30),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(29),
      O => \caracter[0]_i_415_n_0\
    );
\caracter[0]_i_417\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(30),
      I1 => \caracter_reg[0]_i_21_n_3\,
      O => \caracter[0]_i_417_n_0\
    );
\caracter[0]_i_418\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(29),
      I1 => \caracter_reg[0]_i_21_n_3\,
      O => \caracter[0]_i_418_n_0\
    );
\caracter[0]_i_419\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EF"
    )
        port map (
      I0 => euros3(27),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(29),
      O => \caracter[0]_i_419_n_0\
    );
\caracter[0]_i_42\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \caracter[0]_i_38_n_0\,
      I1 => \caracter[0]_i_102_n_0\,
      I2 => \caracter_reg[0]_i_89_n_7\,
      I3 => \caracter_reg[0]_i_88_n_6\,
      I4 => \caracter_reg[0]_i_87_n_5\,
      I5 => \caracter_reg[0]_i_90_n_7\,
      O => \caracter[0]_i_42_n_0\
    );
\caracter[0]_i_420\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \caracter_reg[0]_i_21_n_3\,
      I1 => euros3(30),
      O => \caracter[0]_i_420_n_0\
    );
\caracter[0]_i_421\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F2FD"
    )
        port map (
      I0 => euros3(30),
      I1 => euros3(28),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(29),
      O => \caracter[0]_i_421_n_0\
    );
\caracter[0]_i_422\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFD2FF2D"
    )
        port map (
      I0 => euros3(29),
      I1 => euros3(27),
      I2 => euros3(30),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => euros3(28),
      O => \caracter[0]_i_422_n_0\
    );
\caracter[0]_i_423\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(30),
      I1 => \caracter_reg[0]_i_21_n_3\,
      O => \caracter[0]_i_423_n_0\
    );
\caracter[0]_i_424\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(29),
      I1 => \caracter_reg[0]_i_21_n_3\,
      O => euros2(29)
    );
\caracter[0]_i_425\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \caracter_reg[0]_i_21_n_3\,
      I1 => euros3(30),
      O => \caracter[0]_i_425_n_0\
    );
\caracter[0]_i_426\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \caracter_reg[0]_i_21_n_3\,
      I1 => euros3(29),
      O => \caracter[0]_i_426_n_0\
    );
\caracter[0]_i_428\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BE282828"
    )
        port map (
      I0 => \caracter_reg[0]_i_442_n_5\,
      I1 => \caracter_reg[0]_i_443_n_4\,
      I2 => \caracter_reg[0]_i_441_n_5\,
      I3 => \caracter_reg[0]_i_441_n_6\,
      I4 => \caracter_reg[0]_i_443_n_5\,
      O => \caracter[0]_i_428_n_0\
    );
\caracter[0]_i_429\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BE282828"
    )
        port map (
      I0 => \caracter_reg[0]_i_442_n_6\,
      I1 => \caracter_reg[0]_i_443_n_5\,
      I2 => \caracter_reg[0]_i_441_n_6\,
      I3 => euros2(0),
      I4 => \caracter_reg[0]_i_443_n_6\,
      O => \caracter[0]_i_429_n_0\
    );
\caracter[0]_i_430\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"28"
    )
        port map (
      I0 => \caracter_reg[0]_i_442_n_7\,
      I1 => \caracter_reg[0]_i_443_n_6\,
      I2 => euros2(0),
      O => \caracter[0]_i_430_n_0\
    );
\caracter[0]_i_431\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[0]_i_578_n_4\,
      I1 => \caracter_reg[0]_i_443_n_7\,
      O => \caracter[0]_i_431_n_0\
    );
\caracter[0]_i_432\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => \caracter[0]_i_428_n_0\,
      I1 => \caracter_reg[0]_i_442_n_4\,
      I2 => \caracter[0]_i_579_n_0\,
      I3 => \caracter_reg[0]_i_441_n_5\,
      I4 => \caracter_reg[0]_i_443_n_4\,
      O => \caracter[0]_i_432_n_0\
    );
\caracter[0]_i_433\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669966996"
    )
        port map (
      I0 => \caracter[0]_i_429_n_0\,
      I1 => \caracter_reg[0]_i_442_n_5\,
      I2 => \caracter_reg[0]_i_443_n_4\,
      I3 => \caracter_reg[0]_i_441_n_5\,
      I4 => \caracter_reg[0]_i_441_n_6\,
      I5 => \caracter_reg[0]_i_443_n_5\,
      O => \caracter[0]_i_433_n_0\
    );
\caracter[0]_i_434\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669966996"
    )
        port map (
      I0 => \caracter[0]_i_430_n_0\,
      I1 => \caracter_reg[0]_i_442_n_6\,
      I2 => \caracter_reg[0]_i_443_n_5\,
      I3 => \caracter_reg[0]_i_441_n_6\,
      I4 => euros2(0),
      I5 => \caracter_reg[0]_i_443_n_6\,
      O => \caracter[0]_i_434_n_0\
    );
\caracter[0]_i_435\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \caracter_reg[0]_i_442_n_7\,
      I1 => \caracter_reg[0]_i_443_n_6\,
      I2 => euros2(0),
      I3 => \caracter[0]_i_431_n_0\,
      O => \caracter[0]_i_435_n_0\
    );
\caracter[0]_i_436\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_62_n_5\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(2),
      O => euros2(2)
    );
\caracter[0]_i_437\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_301_n_5\,
      I1 => \caracter_reg[0]_i_303_n_4\,
      I2 => \caracter_reg[0]_i_298_n_7\,
      O => \caracter[0]_i_437_n_0\
    );
\caracter[0]_i_438\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => \caracter_reg[0]_i_303_n_5\,
      I1 => \caracter_reg[0]_i_62_n_5\,
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(2),
      I4 => \caracter_reg[0]_i_301_n_6\,
      O => \caracter[0]_i_438_n_0\
    );
\caracter[0]_i_439\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_62_n_6\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(1),
      O => euros2(1)
    );
\caracter[0]_i_44\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \caracter_reg[0]_i_112_n_3\,
      I1 => \caracter_reg[0]_i_113_n_0\,
      O => \caracter[0]_i_44_n_0\
    );
\caracter[0]_i_440\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => \caracter_reg[0]_i_303_n_6\,
      I1 => \caracter_reg[0]_i_62_n_6\,
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(1),
      I4 => \caracter_reg[0]_i_301_n_7\,
      O => \caracter[0]_i_440_n_0\
    );
\caracter[0]_i_444\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E2"
    )
        port map (
      I0 => euros3(6),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => \caracter_reg[0]_i_29_n_5\,
      O => euros2(6)
    );
\caracter[0]_i_445\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_29_n_6\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(5),
      O => \caracter[0]_i_445_n_0\
    );
\caracter[0]_i_446\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_29_n_7\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(4),
      O => \caracter[0]_i_446_n_0\
    );
\caracter[0]_i_447\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9999A55A6666A55A"
    )
        port map (
      I0 => euros2(0),
      I1 => \caracter_reg[0]_i_62_n_5\,
      I2 => euros3(2),
      I3 => euros3(6),
      I4 => \caracter_reg[0]_i_21_n_3\,
      I5 => \caracter_reg[0]_i_29_n_5\,
      O => \caracter[0]_i_447_n_0\
    );
\caracter[0]_i_448\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"335ACC5A"
    )
        port map (
      I0 => euros3(5),
      I1 => \caracter_reg[0]_i_29_n_6\,
      I2 => euros3(1),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_62_n_6\,
      O => \caracter[0]_i_448_n_0\
    );
\caracter[0]_i_449\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1DE2"
    )
        port map (
      I0 => euros3(4),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => \caracter_reg[0]_i_29_n_7\,
      I3 => euros2(0),
      O => \caracter[0]_i_449_n_0\
    );
\caracter[0]_i_45\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"044A"
    )
        port map (
      I0 => \caracter_reg[0]_i_114_n_0\,
      I1 => \caracter_reg[0]_i_114_n_5\,
      I2 => \caracter_reg[0]_i_112_n_3\,
      I3 => \caracter_reg[0]_i_113_n_0\,
      O => \caracter[0]_i_45_n_0\
    );
\caracter[0]_i_450\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_62_n_4\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(3),
      O => \caracter[0]_i_450_n_0\
    );
\caracter[0]_i_451\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_29_n_7\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(4),
      O => \caracter[0]_i_451_n_0\
    );
\caracter[0]_i_452\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_62_n_4\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(3),
      O => \caracter[0]_i_452_n_0\
    );
\caracter[0]_i_453\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_62_n_5\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(2),
      O => \caracter[0]_i_453_n_0\
    );
\caracter[0]_i_454\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_62_n_6\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(1),
      O => \caracter[0]_i_454_n_0\
    );
\caracter[0]_i_455\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCA533A5"
    )
        port map (
      I0 => euros3(4),
      I1 => \caracter_reg[0]_i_29_n_7\,
      I2 => euros3(7),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_29_n_4\,
      O => \caracter[0]_i_455_n_0\
    );
\caracter[0]_i_456\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCA533A5"
    )
        port map (
      I0 => euros3(3),
      I1 => \caracter_reg[0]_i_62_n_4\,
      I2 => euros3(6),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_29_n_5\,
      O => \caracter[0]_i_456_n_0\
    );
\caracter[0]_i_457\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCA533A5"
    )
        port map (
      I0 => euros3(2),
      I1 => \caracter_reg[0]_i_62_n_5\,
      I2 => euros3(5),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_29_n_6\,
      O => \caracter[0]_i_457_n_0\
    );
\caracter[0]_i_458\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCA533A5"
    )
        port map (
      I0 => euros3(1),
      I1 => \caracter_reg[0]_i_62_n_6\,
      I2 => euros3(4),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_29_n_7\,
      O => \caracter[0]_i_458_n_0\
    );
\caracter[0]_i_459\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F2FB"
    )
        port map (
      I0 => euros3(6),
      I1 => euros3(8),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(10),
      O => \caracter[0]_i_459_n_0\
    );
\caracter[0]_i_46\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \caracter_reg[0]_i_114_n_6\,
      I1 => \caracter_reg[0]_i_114_n_5\,
      I2 => \caracter_reg[0]_i_113_n_0\,
      I3 => \caracter_reg[0]_i_112_n_3\,
      O => \caracter[0]_i_46_n_0\
    );
\caracter[0]_i_460\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFFF4777CCCF4447"
    )
        port map (
      I0 => \caracter_reg[0]_i_29_n_4\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(9),
      I3 => euros3(7),
      I4 => \caracter_reg[0]_i_29_n_6\,
      I5 => euros3(5),
      O => \caracter[0]_i_460_n_0\
    );
\caracter[0]_i_461\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFFF4777CCCF4447"
    )
        port map (
      I0 => \caracter_reg[0]_i_29_n_5\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(8),
      I3 => euros3(6),
      I4 => \caracter_reg[0]_i_29_n_7\,
      I5 => euros3(4),
      O => \caracter[0]_i_461_n_0\
    );
\caracter[0]_i_462\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"57F7075751F10151"
    )
        port map (
      I0 => euros2(5),
      I1 => euros3(7),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => \caracter_reg[0]_i_29_n_4\,
      I4 => \caracter_reg[0]_i_62_n_4\,
      I5 => euros3(3),
      O => \caracter[0]_i_462_n_0\
    );
\caracter[0]_i_463\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5659A6A95956A9A6"
    )
        port map (
      I0 => \caracter[0]_i_459_n_0\,
      I1 => euros3(11),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(7),
      I4 => \caracter_reg[0]_i_29_n_4\,
      I5 => euros3(9),
      O => \caracter[0]_i_463_n_0\
    );
\caracter[0]_i_464\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5A665A995A995A66"
    )
        port map (
      I0 => \caracter[0]_i_460_n_0\,
      I1 => euros3(6),
      I2 => \caracter_reg[0]_i_29_n_5\,
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => euros3(8),
      I5 => euros3(10),
      O => \caracter[0]_i_464_n_0\
    );
\caracter[0]_i_465\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9969666999966696"
    )
        port map (
      I0 => \caracter[0]_i_461_n_0\,
      I1 => euros2(5),
      I2 => euros3(7),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_29_n_4\,
      I5 => euros3(9),
      O => \caracter[0]_i_465_n_0\
    );
\caracter[0]_i_466\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A65659A9A95956A6"
    )
        port map (
      I0 => \caracter[0]_i_462_n_0\,
      I1 => euros3(6),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => \caracter_reg[0]_i_29_n_5\,
      I4 => euros2(4),
      I5 => euros3(8),
      O => \caracter[0]_i_466_n_0\
    );
\caracter[0]_i_467\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(17),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(19),
      I3 => euros3(12),
      O => \caracter[0]_i_467_n_0\
    );
\caracter[0]_i_468\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(16),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(18),
      I3 => euros3(11),
      O => \caracter[0]_i_468_n_0\
    );
\caracter[0]_i_469\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(15),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(17),
      I3 => euros3(10),
      O => \caracter[0]_i_469_n_0\
    );
\caracter[0]_i_47\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \caracter_reg[0]_i_114_n_7\,
      I1 => \caracter_reg[0]_i_114_n_6\,
      I2 => \caracter_reg[0]_i_113_n_0\,
      I3 => \caracter_reg[0]_i_112_n_3\,
      O => \caracter[0]_i_47_n_0\
    );
\caracter[0]_i_470\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(14),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(16),
      I3 => euros3(9),
      O => \caracter[0]_i_470_n_0\
    );
\caracter[0]_i_471\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(18),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(20),
      I3 => euros3(13),
      I4 => \caracter[0]_i_467_n_0\,
      O => \caracter[0]_i_471_n_0\
    );
\caracter[0]_i_472\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(17),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(19),
      I3 => euros3(12),
      I4 => \caracter[0]_i_468_n_0\,
      O => \caracter[0]_i_472_n_0\
    );
\caracter[0]_i_473\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(16),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(18),
      I3 => euros3(11),
      I4 => \caracter[0]_i_469_n_0\,
      O => \caracter[0]_i_473_n_0\
    );
\caracter[0]_i_474\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(15),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(17),
      I3 => euros3(10),
      I4 => \caracter[0]_i_470_n_0\,
      O => \caracter[0]_i_474_n_0\
    );
\caracter[0]_i_475\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \caracter_reg[0]_i_29_n_4\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(7),
      O => \caracter[0]_i_475_n_0\
    );
\caracter[0]_i_476\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_29_n_7\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(4),
      O => euros2(4)
    );
\caracter[0]_i_477\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_62_n_4\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(3),
      O => euros2(3)
    );
\caracter[0]_i_478\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[0]_i_29_n_4\,
      O => \caracter[0]_i_478_n_0\
    );
\caracter[0]_i_479\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[0]_i_29_n_5\,
      O => \caracter[0]_i_479_n_0\
    );
\caracter[0]_i_48\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter_reg[0]_i_114_n_0\,
      I1 => \caracter_reg[0]_i_112_n_3\,
      I2 => \caracter_reg[0]_i_113_n_0\,
      O => \caracter[0]_i_48_n_0\
    );
\caracter[0]_i_480\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[0]_i_29_n_6\,
      O => \caracter[0]_i_480_n_0\
    );
\caracter[0]_i_481\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => euros2(0),
      O => \caracter[0]_i_481_n_0\
    );
\caracter[0]_i_482\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[0]_i_29_n_7\,
      O => \caracter[0]_i_482_n_0\
    );
\caracter[0]_i_483\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[0]_i_62_n_4\,
      O => \caracter[0]_i_483_n_0\
    );
\caracter[0]_i_484\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[0]_i_62_n_5\,
      O => \caracter[0]_i_484_n_0\
    );
\caracter[0]_i_485\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[0]_i_62_n_6\,
      O => \caracter[0]_i_485_n_0\
    );
\caracter[0]_i_487\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BE282828"
    )
        port map (
      I0 => \caracter_reg[0]_i_608_n_1\,
      I1 => \caracter_reg[0]_i_364_n_4\,
      I2 => \caracter_reg[0]_i_371_n_5\,
      I3 => \caracter_reg[0]_i_371_n_6\,
      I4 => \caracter_reg[0]_i_364_n_5\,
      O => \caracter[0]_i_487_n_0\
    );
\caracter[0]_i_488\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BE282828"
    )
        port map (
      I0 => \caracter_reg[0]_i_608_n_6\,
      I1 => \caracter_reg[0]_i_364_n_5\,
      I2 => \caracter_reg[0]_i_371_n_6\,
      I3 => \caracter_reg[3]_i_141_0\(0),
      I4 => \caracter_reg[0]_i_364_n_6\,
      O => \caracter[0]_i_488_n_0\
    );
\caracter[0]_i_489\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"28"
    )
        port map (
      I0 => \caracter_reg[0]_i_608_n_7\,
      I1 => \caracter_reg[0]_i_364_n_6\,
      I2 => \caracter_reg[3]_i_141_0\(0),
      O => \caracter[0]_i_489_n_0\
    );
\caracter[0]_i_49\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0113"
    )
        port map (
      I0 => \caracter_reg[0]_i_114_n_5\,
      I1 => \caracter_reg[0]_i_114_n_0\,
      I2 => \caracter_reg[0]_i_112_n_3\,
      I3 => \caracter_reg[0]_i_113_n_0\,
      O => \caracter[0]_i_49_n_0\
    );
\caracter[0]_i_490\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[0]_i_609_n_4\,
      I1 => \caracter_reg[0]_i_364_n_7\,
      O => \caracter[0]_i_490_n_0\
    );
\caracter[0]_i_491\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7F80F807F807807F"
    )
        port map (
      I0 => \caracter_reg[0]_i_364_n_5\,
      I1 => \caracter_reg[0]_i_371_n_6\,
      I2 => \caracter_reg[0]_i_608_n_1\,
      I3 => \caracter[0]_i_610_n_0\,
      I4 => \caracter_reg[0]_i_364_n_4\,
      I5 => \caracter_reg[0]_i_371_n_5\,
      O => \caracter[0]_i_491_n_0\
    );
\caracter[0]_i_492\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669966996"
    )
        port map (
      I0 => \caracter[0]_i_488_n_0\,
      I1 => \caracter_reg[0]_i_608_n_1\,
      I2 => \caracter_reg[0]_i_364_n_4\,
      I3 => \caracter_reg[0]_i_371_n_5\,
      I4 => \caracter_reg[0]_i_371_n_6\,
      I5 => \caracter_reg[0]_i_364_n_5\,
      O => \caracter[0]_i_492_n_0\
    );
\caracter[0]_i_493\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669966996"
    )
        port map (
      I0 => \caracter[0]_i_489_n_0\,
      I1 => \caracter_reg[0]_i_608_n_6\,
      I2 => \caracter_reg[0]_i_364_n_5\,
      I3 => \caracter_reg[0]_i_371_n_6\,
      I4 => \caracter_reg[3]_i_141_0\(0),
      I5 => \caracter_reg[0]_i_364_n_6\,
      O => \caracter[0]_i_493_n_0\
    );
\caracter[0]_i_494\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \caracter_reg[0]_i_608_n_7\,
      I1 => \caracter_reg[0]_i_364_n_6\,
      I2 => \caracter_reg[3]_i_141_0\(0),
      I3 => \caracter[0]_i_490_n_0\,
      O => \caracter[0]_i_494_n_0\
    );
\caracter[0]_i_495\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[0]_i_355_n_6\,
      I1 => \caracter_reg[0]_i_112_n_3\,
      I2 => \caracter_reg[0]_i_250_n_4\,
      O => \caracter[0]_i_495_n_0\
    );
\caracter[0]_i_496\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[0]_i_250_n_5\,
      I1 => \caracter_reg[0]_i_355_n_7\,
      I2 => \caracter_reg[0]_i_249_n_4\,
      O => \caracter[0]_i_496_n_0\
    );
\caracter[0]_i_497\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[0]_i_250_n_6\,
      I1 => \caracter_reg[0]_i_249_n_5\,
      I2 => \caracter_reg[3]_i_141_0\(2),
      O => \caracter[0]_i_497_n_0\
    );
\caracter[0]_i_498\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[0]_i_250_n_7\,
      I1 => \caracter_reg[0]_i_249_n_6\,
      I2 => \caracter_reg[3]_i_141_0\(1),
      O => \caracter[0]_i_498_n_0\
    );
\caracter[0]_i_499\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(0),
      I1 => \caracter_reg[3]_i_141_0\(2),
      I2 => \caracter_reg[3]_i_141_0\(6),
      O => \caracter[0]_i_499_n_0\
    );
\caracter[0]_i_50\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"566AA995"
    )
        port map (
      I0 => \caracter[0]_i_46_n_0\,
      I1 => \caracter_reg[0]_i_113_n_0\,
      I2 => \caracter_reg[0]_i_112_n_3\,
      I3 => \caracter_reg[0]_i_114_n_5\,
      I4 => \caracter_reg[0]_i_114_n_0\,
      O => \caracter[0]_i_50_n_0\
    );
\caracter[0]_i_500\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(5),
      I1 => \caracter_reg[3]_i_141_0\(1),
      O => \caracter[0]_i_500_n_0\
    );
\caracter[0]_i_501\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(4),
      I1 => \caracter_reg[3]_i_141_0\(0),
      O => \caracter[0]_i_501_n_0\
    );
\caracter[0]_i_503\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(2),
      I1 => \caracter_reg[3]_i_141_0\(4),
      I2 => \caracter_reg[3]_i_141_0\(6),
      O => \caracter[0]_i_503_n_0\
    );
\caracter[0]_i_504\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(1),
      I1 => \caracter_reg[3]_i_141_0\(3),
      I2 => \caracter_reg[3]_i_141_0\(5),
      O => \caracter[0]_i_504_n_0\
    );
\caracter[0]_i_505\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4D"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(2),
      I1 => \caracter_reg[3]_i_141_0\(0),
      I2 => \caracter_reg[3]_i_141_0\(4),
      O => \caracter[0]_i_505_n_0\
    );
\caracter[0]_i_506\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(1),
      I1 => \caracter_reg[3]_i_141_0\(3),
      O => \caracter[0]_i_506_n_0\
    );
\caracter[0]_i_507\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \caracter[0]_i_503_n_0\,
      I1 => \caracter_reg[3]_i_141_0\(5),
      I2 => \caracter_reg[3]_i_141_0\(3),
      I3 => \caracter_reg[3]_i_141_0\(7),
      O => \caracter[0]_i_507_n_0\
    );
\caracter[0]_i_508\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(2),
      I1 => \caracter_reg[3]_i_141_0\(4),
      I2 => \caracter_reg[3]_i_141_0\(6),
      I3 => \caracter[0]_i_504_n_0\,
      O => \caracter[0]_i_508_n_0\
    );
\caracter[0]_i_509\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(1),
      I1 => \caracter_reg[3]_i_141_0\(3),
      I2 => \caracter_reg[3]_i_141_0\(5),
      I3 => \caracter[0]_i_505_n_0\,
      O => \caracter[0]_i_509_n_0\
    );
\caracter[0]_i_51\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCC9C999"
    )
        port map (
      I0 => \caracter_reg[0]_i_114_n_6\,
      I1 => \caracter_reg[0]_i_114_n_5\,
      I2 => \caracter_reg[0]_i_113_n_0\,
      I3 => \caracter_reg[0]_i_112_n_3\,
      I4 => \caracter_reg[0]_i_114_n_7\,
      O => \caracter[0]_i_51_n_0\
    );
\caracter[0]_i_510\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(2),
      I1 => \caracter_reg[3]_i_141_0\(0),
      I2 => \caracter_reg[3]_i_141_0\(4),
      I3 => \caracter[0]_i_506_n_0\,
      O => \caracter[0]_i_510_n_0\
    );
\caracter[0]_i_511\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(0),
      I1 => \caracter_reg[3]_i_141_0\(3),
      O => \caracter[0]_i_511_n_0\
    );
\caracter[0]_i_512\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(2),
      O => \caracter[0]_i_512_n_0\
    );
\caracter[0]_i_513\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(1),
      O => \caracter[0]_i_513_n_0\
    );
\caracter[0]_i_515\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_385_n_4\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(12),
      O => \caracter[0]_i_515_n_0\
    );
\caracter[0]_i_516\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_385_n_5\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(11),
      O => \caracter[0]_i_516_n_0\
    );
\caracter[0]_i_517\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_385_n_6\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(10),
      O => \caracter[0]_i_517_n_0\
    );
\caracter[0]_i_518\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_385_n_7\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(9),
      O => \caracter[0]_i_518_n_0\
    );
\caracter[0]_i_519\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(12),
      I1 => \caracter_reg[0]_i_385_n_4\,
      I2 => euros3(13),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_267_n_7\,
      O => \caracter[0]_i_519_n_0\
    );
\caracter[0]_i_520\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(11),
      I1 => \caracter_reg[0]_i_385_n_5\,
      I2 => euros3(12),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_385_n_4\,
      O => \caracter[0]_i_520_n_0\
    );
\caracter[0]_i_521\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(10),
      I1 => \caracter_reg[0]_i_385_n_6\,
      I2 => euros3(11),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_385_n_5\,
      O => \caracter[0]_i_521_n_0\
    );
\caracter[0]_i_522\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(9),
      I1 => \caracter_reg[0]_i_385_n_7\,
      I2 => euros3(10),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_385_n_6\,
      O => \caracter[0]_i_522_n_0\
    );
\caracter[0]_i_524\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_532_n_7\,
      I1 => \caracter_reg[0]_i_532_n_5\,
      I2 => \caracter_reg[0]_i_394_n_6\,
      O => \caracter[0]_i_524_n_0\
    );
\caracter[0]_i_525\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_23_n_4\,
      I1 => \caracter_reg[0]_i_532_n_6\,
      I2 => \caracter_reg[0]_i_394_n_7\,
      O => \caracter[0]_i_525_n_0\
    );
\caracter[0]_i_526\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_23_n_5\,
      I1 => \caracter_reg[0]_i_532_n_7\,
      I2 => \caracter_reg[0]_i_532_n_4\,
      O => \caracter[0]_i_526_n_0\
    );
\caracter[0]_i_527\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_23_n_6\,
      I1 => \caracter_reg[0]_i_23_n_4\,
      I2 => \caracter_reg[0]_i_532_n_5\,
      O => \caracter[0]_i_527_n_0\
    );
\caracter[0]_i_528\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_532_n_6\,
      I1 => \caracter_reg[0]_i_532_n_4\,
      I2 => \caracter_reg[0]_i_394_n_5\,
      I3 => \caracter[0]_i_524_n_0\,
      O => \caracter[0]_i_528_n_0\
    );
\caracter[0]_i_529\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_532_n_7\,
      I1 => \caracter_reg[0]_i_532_n_5\,
      I2 => \caracter_reg[0]_i_394_n_6\,
      I3 => \caracter[0]_i_525_n_0\,
      O => \caracter[0]_i_529_n_0\
    );
\caracter[0]_i_53\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_30_n_4\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(28),
      O => \caracter[0]_i_53_n_0\
    );
\caracter[0]_i_530\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_23_n_4\,
      I1 => \caracter_reg[0]_i_532_n_6\,
      I2 => \caracter_reg[0]_i_394_n_7\,
      I3 => \caracter[0]_i_526_n_0\,
      O => \caracter[0]_i_530_n_0\
    );
\caracter[0]_i_531\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_23_n_5\,
      I1 => \caracter_reg[0]_i_532_n_7\,
      I2 => \caracter_reg[0]_i_532_n_4\,
      I3 => \caracter[0]_i_527_n_0\,
      O => \caracter[0]_i_531_n_0\
    );
\caracter[0]_i_533\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E80000E800E8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_542_n_6\,
      I1 => \caracter_reg[0]_i_416_n_5\,
      I2 => \caracter_reg[0]_i_639_n_4\,
      I3 => \caracter_reg[0]_i_416_n_4\,
      I4 => \caracter_reg[0]_i_541_n_7\,
      I5 => \caracter_reg[0]_i_542_n_5\,
      O => \caracter[0]_i_533_n_0\
    );
\caracter[0]_i_534\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_639_n_5\,
      I1 => \caracter_reg[0]_i_416_n_6\,
      I2 => \caracter_reg[0]_i_542_n_7\,
      I3 => \caracter_reg[0]_i_640_n_2\,
      I4 => \caracter[0]_i_641_n_0\,
      O => \caracter[0]_i_534_n_0\
    );
\caracter[0]_i_535\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_639_n_6\,
      I1 => \caracter_reg[0]_i_416_n_7\,
      I2 => \caracter_reg[0]_i_642_n_4\,
      I3 => \caracter_reg[0]_i_640_n_7\,
      I4 => \caracter[0]_i_643_n_0\,
      O => \caracter[0]_i_535_n_0\
    );
\caracter[0]_i_536\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \caracter_reg[0]_i_644_n_4\,
      I1 => \caracter[0]_i_645_n_0\,
      I2 => \caracter_reg[0]_i_639_n_7\,
      I3 => \caracter_reg[0]_i_99_n_4\,
      I4 => \caracter_reg[0]_i_642_n_5\,
      O => \caracter[0]_i_536_n_0\
    );
\caracter[0]_i_537\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66696999"
    )
        port map (
      I0 => \caracter[0]_i_533_n_0\,
      I1 => \caracter[0]_i_646_n_0\,
      I2 => \caracter_reg[0]_i_541_n_7\,
      I3 => \caracter_reg[0]_i_416_n_4\,
      I4 => \caracter_reg[0]_i_542_n_5\,
      O => \caracter[0]_i_537_n_0\
    );
\caracter[0]_i_538\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66696999"
    )
        port map (
      I0 => \caracter[0]_i_534_n_0\,
      I1 => \caracter[0]_i_647_n_0\,
      I2 => \caracter_reg[0]_i_639_n_4\,
      I3 => \caracter_reg[0]_i_416_n_5\,
      I4 => \caracter_reg[0]_i_542_n_6\,
      O => \caracter[0]_i_538_n_0\
    );
\caracter[0]_i_539\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \caracter[0]_i_535_n_0\,
      I1 => \caracter[0]_i_648_n_0\,
      I2 => \caracter_reg[0]_i_542_n_6\,
      I3 => \caracter_reg[0]_i_416_n_5\,
      I4 => \caracter_reg[0]_i_639_n_4\,
      I5 => \caracter_reg[0]_i_640_n_2\,
      O => \caracter[0]_i_539_n_0\
    );
\caracter[0]_i_54\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_30_n_5\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(27),
      O => \caracter[0]_i_54_n_0\
    );
\caracter[0]_i_540\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \caracter[0]_i_536_n_0\,
      I1 => \caracter[0]_i_649_n_0\,
      I2 => \caracter_reg[0]_i_542_n_7\,
      I3 => \caracter_reg[0]_i_416_n_6\,
      I4 => \caracter_reg[0]_i_639_n_5\,
      I5 => \caracter_reg[0]_i_640_n_7\,
      O => \caracter[0]_i_540_n_0\
    );
\caracter[0]_i_543\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[0]_i_403_n_7\,
      I1 => \caracter_reg[0]_i_287_n_0\,
      I2 => \caracter_reg[0]_i_404_n_5\,
      O => \caracter[0]_i_543_n_0\
    );
\caracter[0]_i_544\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[0]_i_404_n_6\,
      I1 => \caracter_reg[0]_i_541_n_4\,
      I2 => \caracter_reg[0]_i_287_n_5\,
      O => \caracter[0]_i_544_n_0\
    );
\caracter[0]_i_545\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[0]_i_404_n_7\,
      I1 => \caracter_reg[0]_i_541_n_5\,
      I2 => \caracter_reg[0]_i_287_n_6\,
      O => \caracter[0]_i_545_n_0\
    );
\caracter[0]_i_546\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => euros3(29),
      I1 => euros3(25),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(23),
      O => \caracter[0]_i_546_n_0\
    );
\caracter[0]_i_547\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => euros3(28),
      I1 => euros3(24),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(22),
      O => \caracter[0]_i_547_n_0\
    );
\caracter[0]_i_548\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => euros3(27),
      I1 => euros3(23),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(21),
      O => \caracter[0]_i_548_n_0\
    );
\caracter[0]_i_549\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => euros3(26),
      I1 => euros3(22),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(20),
      O => \caracter[0]_i_549_n_0\
    );
\caracter[0]_i_55\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_30_n_6\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(26),
      O => \caracter[0]_i_55_n_0\
    );
\caracter[0]_i_550\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A6A9A9A6"
    )
        port map (
      I0 => \caracter[0]_i_546_n_0\,
      I1 => euros3(30),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(24),
      I4 => euros3(26),
      O => \caracter[0]_i_550_n_0\
    );
\caracter[0]_i_551\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F6F90906"
    )
        port map (
      I0 => euros3(29),
      I1 => euros3(25),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(23),
      I4 => \caracter[0]_i_547_n_0\,
      O => \caracter[0]_i_551_n_0\
    );
\caracter[0]_i_552\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F6F90906"
    )
        port map (
      I0 => euros3(28),
      I1 => euros3(24),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(22),
      I4 => \caracter[0]_i_548_n_0\,
      O => \caracter[0]_i_552_n_0\
    );
\caracter[0]_i_553\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F6F90906"
    )
        port map (
      I0 => euros3(27),
      I1 => euros3(23),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(21),
      I4 => \caracter[0]_i_549_n_0\,
      O => \caracter[0]_i_553_n_0\
    );
\caracter[0]_i_554\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(28),
      I1 => \caracter_reg[0]_i_21_n_3\,
      O => euros2(28)
    );
\caracter[0]_i_555\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(27),
      I1 => \caracter_reg[0]_i_21_n_3\,
      O => euros2(27)
    );
\caracter[0]_i_556\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(26),
      I1 => \caracter_reg[0]_i_21_n_3\,
      O => euros2(26)
    );
\caracter[0]_i_557\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(25),
      I1 => \caracter_reg[0]_i_21_n_3\,
      O => euros2(25)
    );
\caracter[0]_i_558\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \caracter_reg[0]_i_21_n_3\,
      I1 => euros3(28),
      O => \caracter[0]_i_558_n_0\
    );
\caracter[0]_i_559\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(27),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(30),
      O => \caracter[0]_i_559_n_0\
    );
\caracter[0]_i_56\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_30_n_7\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(25),
      O => \caracter[0]_i_56_n_0\
    );
\caracter[0]_i_560\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(26),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(29),
      O => \caracter[0]_i_560_n_0\
    );
\caracter[0]_i_561\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(25),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(28),
      O => \caracter[0]_i_561_n_0\
    );
\caracter[0]_i_562\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFCD"
    )
        port map (
      I0 => euros3(30),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(28),
      I3 => euros3(26),
      O => \caracter[0]_i_562_n_0\
    );
\caracter[0]_i_563\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFCD"
    )
        port map (
      I0 => euros3(29),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(27),
      I3 => euros3(25),
      O => \caracter[0]_i_563_n_0\
    );
\caracter[0]_i_564\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(28),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(24),
      I3 => euros3(26),
      O => \caracter[0]_i_564_n_0\
    );
\caracter[0]_i_565\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(27),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(23),
      I3 => euros3(25),
      O => \caracter[0]_i_565_n_0\
    );
\caracter[0]_i_566\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF2BD4FFFFD42B"
    )
        port map (
      I0 => euros3(26),
      I1 => euros3(28),
      I2 => euros3(30),
      I3 => euros3(29),
      I4 => \caracter_reg[0]_i_21_n_3\,
      I5 => euros3(27),
      O => \caracter[0]_i_566_n_0\
    );
\caracter[0]_i_567\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A6A9A9A6"
    )
        port map (
      I0 => \caracter[0]_i_563_n_0\,
      I1 => euros3(30),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(26),
      I4 => euros3(28),
      O => \caracter[0]_i_567_n_0\
    );
\caracter[0]_i_568\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(29),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(27),
      I3 => euros3(25),
      I4 => \caracter[0]_i_564_n_0\,
      O => \caracter[0]_i_568_n_0\
    );
\caracter[0]_i_569\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(28),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(24),
      I3 => euros3(26),
      I4 => \caracter[0]_i_565_n_0\,
      O => \caracter[0]_i_569_n_0\
    );
\caracter[0]_i_57\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(28),
      I1 => \caracter_reg[0]_i_30_n_4\,
      I2 => euros3(29),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_22_n_7\,
      O => \caracter[0]_i_57_n_0\
    );
\caracter[0]_i_570\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[0]_i_578_n_5\,
      I1 => \caracter_reg[0]_i_591_n_4\,
      O => \caracter[0]_i_570_n_0\
    );
\caracter[0]_i_571\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[0]_i_578_n_6\,
      I1 => \caracter_reg[0]_i_591_n_5\,
      O => \caracter[0]_i_571_n_0\
    );
\caracter[0]_i_572\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[0]_i_578_n_7\,
      I1 => \caracter_reg[0]_i_591_n_6\,
      O => \caracter[0]_i_572_n_0\
    );
\caracter[0]_i_573\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[0]_i_591_n_7\,
      I1 => \caracter_reg[0]_i_666_n_4\,
      O => \caracter[0]_i_573_n_0\
    );
\caracter[0]_i_574\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => \caracter_reg[0]_i_578_n_4\,
      I1 => \caracter_reg[0]_i_443_n_7\,
      I2 => \caracter_reg[0]_i_591_n_4\,
      I3 => \caracter_reg[0]_i_578_n_5\,
      O => \caracter[0]_i_574_n_0\
    );
\caracter[0]_i_575\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \caracter_reg[0]_i_591_n_5\,
      I1 => \caracter_reg[0]_i_578_n_6\,
      I2 => \caracter_reg[0]_i_591_n_4\,
      I3 => \caracter_reg[0]_i_578_n_5\,
      O => \caracter[0]_i_575_n_0\
    );
\caracter[0]_i_576\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \caracter_reg[0]_i_591_n_6\,
      I1 => \caracter_reg[0]_i_578_n_7\,
      I2 => \caracter_reg[0]_i_591_n_5\,
      I3 => \caracter_reg[0]_i_578_n_6\,
      O => \caracter[0]_i_576_n_0\
    );
\caracter[0]_i_577\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \caracter_reg[0]_i_666_n_4\,
      I1 => \caracter_reg[0]_i_591_n_7\,
      I2 => \caracter_reg[0]_i_591_n_6\,
      I3 => \caracter_reg[0]_i_578_n_7\,
      O => \caracter[0]_i_577_n_0\
    );
\caracter[0]_i_579\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_303_n_7\,
      I1 => euros2(0),
      I2 => \caracter_reg[0]_i_441_n_4\,
      O => \caracter[0]_i_579_n_0\
    );
\caracter[0]_i_58\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(27),
      I1 => \caracter_reg[0]_i_30_n_5\,
      I2 => euros3(28),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_30_n_4\,
      O => \caracter[0]_i_58_n_0\
    );
\caracter[0]_i_580\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A959"
    )
        port map (
      I0 => euros2(0),
      I1 => euros3(3),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => \caracter_reg[0]_i_62_n_4\,
      O => \caracter[0]_i_580_n_0\
    );
\caracter[0]_i_581\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1D"
    )
        port map (
      I0 => euros3(2),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => \caracter_reg[0]_i_62_n_5\,
      O => \caracter[0]_i_581_n_0\
    );
\caracter[0]_i_582\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1D"
    )
        port map (
      I0 => euros3(1),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => \caracter_reg[0]_i_62_n_6\,
      O => \caracter[0]_i_582_n_0\
    );
\caracter[0]_i_583\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(13),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(15),
      I3 => euros3(8),
      O => \caracter[0]_i_583_n_0\
    );
\caracter[0]_i_584\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(14),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(12),
      I3 => euros3(7),
      O => \caracter[0]_i_584_n_0\
    );
\caracter[0]_i_585\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(13),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(11),
      I3 => euros3(6),
      O => \caracter[0]_i_585_n_0\
    );
\caracter[0]_i_586\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"5440"
    )
        port map (
      I0 => \caracter_reg[0]_i_21_n_3\,
      I1 => euros3(5),
      I2 => euros3(12),
      I3 => euros3(10),
      O => \caracter[0]_i_586_n_0\
    );
\caracter[0]_i_587\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(14),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(16),
      I3 => euros3(9),
      I4 => \caracter[0]_i_583_n_0\,
      O => \caracter[0]_i_587_n_0\
    );
\caracter[0]_i_588\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(13),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(15),
      I3 => euros3(8),
      I4 => \caracter[0]_i_584_n_0\,
      O => \caracter[0]_i_588_n_0\
    );
\caracter[0]_i_589\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5659A6A95956A9A6"
    )
        port map (
      I0 => \caracter[0]_i_585_n_0\,
      I1 => euros3(12),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(14),
      I4 => \caracter_reg[0]_i_29_n_4\,
      I5 => euros3(7),
      O => \caracter[0]_i_589_n_0\
    );
\caracter[0]_i_59\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(26),
      I1 => \caracter_reg[0]_i_30_n_6\,
      I2 => euros3(27),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_30_n_5\,
      O => \caracter[0]_i_59_n_0\
    );
\caracter[0]_i_590\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5659A6A95956A9A6"
    )
        port map (
      I0 => \caracter[0]_i_586_n_0\,
      I1 => euros3(11),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(13),
      I4 => \caracter_reg[0]_i_29_n_5\,
      I5 => euros3(6),
      O => \caracter[0]_i_590_n_0\
    );
\caracter[0]_i_592\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"335FFF5F00053305"
    )
        port map (
      I0 => euros3(4),
      I1 => \caracter_reg[0]_i_29_n_7\,
      I2 => euros3(6),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_29_n_5\,
      I5 => euros2(2),
      O => \caracter[0]_i_592_n_0\
    );
\caracter[0]_i_593\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"57F7075751F10151"
    )
        port map (
      I0 => euros2(5),
      I1 => euros3(3),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => \caracter_reg[0]_i_62_n_4\,
      I4 => \caracter_reg[0]_i_62_n_6\,
      I5 => euros3(1),
      O => \caracter[0]_i_593_n_0\
    );
\caracter[0]_i_594\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"47007703CF44FF47"
    )
        port map (
      I0 => \caracter_reg[0]_i_29_n_7\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(4),
      I3 => euros2(0),
      I4 => euros3(2),
      I5 => \caracter_reg[0]_i_62_n_5\,
      O => \caracter[0]_i_594_n_0\
    );
\caracter[0]_i_595\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00053305"
    )
        port map (
      I0 => euros3(1),
      I1 => \caracter_reg[0]_i_62_n_6\,
      I2 => euros3(3),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_62_n_4\,
      O => \caracter[0]_i_595_n_0\
    );
\caracter[0]_i_596\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9996669666699969"
    )
        port map (
      I0 => \caracter[0]_i_592_n_0\,
      I1 => euros2(5),
      I2 => euros3(3),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_62_n_4\,
      I5 => \caracter[0]_i_475_n_0\,
      O => \caracter[0]_i_596_n_0\
    );
\caracter[0]_i_597\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"56A6A959A95956A6"
    )
        port map (
      I0 => \caracter[0]_i_593_n_0\,
      I1 => euros3(6),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => \caracter_reg[0]_i_29_n_5\,
      I4 => euros2(4),
      I5 => euros2(2),
      O => \caracter[0]_i_597_n_0\
    );
\caracter[0]_i_598\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6996696969969696"
    )
        port map (
      I0 => \caracter[0]_i_594_n_0\,
      I1 => euros2(5),
      I2 => euros2(3),
      I3 => \caracter_reg[0]_i_62_n_6\,
      I4 => \caracter_reg[0]_i_21_n_3\,
      I5 => euros3(1),
      O => \caracter[0]_i_598_n_0\
    );
\caracter[0]_i_599\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6966699996999666"
    )
        port map (
      I0 => euros2(0),
      I1 => euros2(2),
      I2 => \caracter_reg[0]_i_29_n_7\,
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => euros3(4),
      I5 => \caracter[0]_i_595_n_0\,
      O => \caracter[0]_i_599_n_0\
    );
\caracter[0]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8CBC"
    )
        port map (
      I0 => \caracter[0]_i_11_n_0\,
      I1 => \caracter[6]_i_38_0\,
      I2 => \caracter_reg[1]_i_4\,
      I3 => \caracter[0]_i_3\(0),
      O => \^fsm_sequential_current_state_reg[1]_1\
    );
\caracter[0]_i_60\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(25),
      I1 => \caracter_reg[0]_i_30_n_7\,
      I2 => euros3(26),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_30_n_6\,
      O => \caracter[0]_i_60_n_0\
    );
\caracter[0]_i_600\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[0]_i_609_n_5\,
      I1 => \caracter_reg[0]_i_502_n_4\,
      O => \caracter[0]_i_600_n_0\
    );
\caracter[0]_i_601\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[0]_i_609_n_6\,
      I1 => \caracter_reg[0]_i_502_n_5\,
      O => \caracter[0]_i_601_n_0\
    );
\caracter[0]_i_602\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[0]_i_609_n_7\,
      I1 => \caracter_reg[0]_i_502_n_6\,
      O => \caracter[0]_i_602_n_0\
    );
\caracter[0]_i_603\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[0]_i_502_n_7\,
      I1 => \caracter_reg[0]_i_678_n_4\,
      O => \caracter[0]_i_603_n_0\
    );
\caracter[0]_i_604\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => \caracter_reg[0]_i_609_n_4\,
      I1 => \caracter_reg[0]_i_364_n_7\,
      I2 => \caracter_reg[0]_i_502_n_4\,
      I3 => \caracter_reg[0]_i_609_n_5\,
      O => \caracter[0]_i_604_n_0\
    );
\caracter[0]_i_605\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \caracter_reg[0]_i_502_n_5\,
      I1 => \caracter_reg[0]_i_609_n_6\,
      I2 => \caracter_reg[0]_i_502_n_4\,
      I3 => \caracter_reg[0]_i_609_n_5\,
      O => \caracter[0]_i_605_n_0\
    );
\caracter[0]_i_606\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \caracter_reg[0]_i_502_n_6\,
      I1 => \caracter_reg[0]_i_609_n_7\,
      I2 => \caracter_reg[0]_i_502_n_5\,
      I3 => \caracter_reg[0]_i_609_n_6\,
      O => \caracter[0]_i_606_n_0\
    );
\caracter[0]_i_607\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \caracter_reg[0]_i_678_n_4\,
      I1 => \caracter_reg[0]_i_502_n_7\,
      I2 => \caracter_reg[0]_i_502_n_6\,
      I3 => \caracter_reg[0]_i_609_n_7\,
      O => \caracter[0]_i_607_n_0\
    );
\caracter[0]_i_610\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[0]_i_371_n_4\,
      I1 => \caracter_reg[0]_i_249_n_7\,
      I2 => \caracter_reg[3]_i_141_0\(0),
      O => \caracter[0]_i_610_n_0\
    );
\caracter[0]_i_611\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(0),
      O => \caracter[0]_i_611_n_0\
    );
\caracter[0]_i_612\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(1),
      I1 => \caracter_reg[3]_i_141_0\(3),
      I2 => \caracter_reg[3]_i_141_0\(0),
      O => \caracter[0]_i_612_n_0\
    );
\caracter[0]_i_613\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(0),
      I1 => \caracter_reg[3]_i_141_0\(2),
      O => \caracter[0]_i_613_n_0\
    );
\caracter[0]_i_614\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(1),
      O => \caracter[0]_i_614_n_0\
    );
\caracter[0]_i_616\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_523_n_4\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(8),
      O => \caracter[0]_i_616_n_0\
    );
\caracter[0]_i_617\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"02A2"
    )
        port map (
      I0 => \caracter_reg[0]_i_523_n_5\,
      I1 => euros3(7),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => \caracter_reg[0]_i_29_n_4\,
      O => \caracter[0]_i_617_n_0\
    );
\caracter[0]_i_618\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"ABFB"
    )
        port map (
      I0 => \caracter_reg[0]_i_523_n_6\,
      I1 => euros3(6),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => \caracter_reg[0]_i_29_n_5\,
      O => \caracter[0]_i_618_n_0\
    );
\caracter[0]_i_619\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"ABFB"
    )
        port map (
      I0 => \caracter_reg[0]_i_523_n_7\,
      I1 => euros3(5),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => \caracter_reg[0]_i_29_n_6\,
      O => \caracter[0]_i_619_n_0\
    );
\caracter[0]_i_620\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(8),
      I1 => \caracter_reg[0]_i_523_n_4\,
      I2 => euros3(9),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_385_n_7\,
      O => \caracter[0]_i_620_n_0\
    );
\caracter[0]_i_621\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5050CF30AFAF30CF"
    )
        port map (
      I0 => \caracter_reg[0]_i_29_n_4\,
      I1 => euros3(7),
      I2 => \caracter_reg[0]_i_523_n_5\,
      I3 => euros3(8),
      I4 => \caracter_reg[0]_i_21_n_3\,
      I5 => \caracter_reg[0]_i_523_n_4\,
      O => \caracter[0]_i_621_n_0\
    );
\caracter[0]_i_622\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF4700B800B8FF47"
    )
        port map (
      I0 => \caracter_reg[0]_i_29_n_5\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(6),
      I3 => \caracter_reg[0]_i_523_n_6\,
      I4 => \caracter[0]_i_475_n_0\,
      I5 => \caracter_reg[0]_i_523_n_5\,
      O => \caracter[0]_i_622_n_0\
    );
\caracter[0]_i_623\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"D2DDD2222D222DDD"
    )
        port map (
      I0 => euros2(5),
      I1 => \caracter_reg[0]_i_523_n_7\,
      I2 => \caracter_reg[0]_i_29_n_5\,
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => euros3(6),
      I5 => \caracter_reg[0]_i_523_n_6\,
      O => \caracter[0]_i_623_n_0\
    );
\caracter[0]_i_624\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_23_n_7\,
      I1 => \caracter_reg[0]_i_23_n_5\,
      I2 => \caracter_reg[0]_i_532_n_6\,
      O => \caracter[0]_i_624_n_0\
    );
\caracter[0]_i_625\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[0]_i_532_n_6\,
      I1 => \caracter_reg[0]_i_23_n_5\,
      I2 => \caracter_reg[0]_i_23_n_7\,
      O => \caracter[0]_i_625_n_0\
    );
\caracter[0]_i_626\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \caracter_reg[0]_i_23_n_4\,
      I1 => \caracter_reg[0]_i_23_n_7\,
      O => \caracter[0]_i_626_n_0\
    );
\caracter[0]_i_627\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_23_n_6\,
      I1 => \caracter_reg[0]_i_23_n_4\,
      I2 => \caracter_reg[0]_i_532_n_5\,
      I3 => \caracter[0]_i_624_n_0\,
      O => \caracter[0]_i_627_n_0\
    );
\caracter[0]_i_628\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69966969"
    )
        port map (
      I0 => \caracter_reg[0]_i_23_n_7\,
      I1 => \caracter_reg[0]_i_23_n_5\,
      I2 => \caracter_reg[0]_i_532_n_6\,
      I3 => \caracter_reg[0]_i_23_n_6\,
      I4 => \caracter_reg[0]_i_532_n_7\,
      O => \caracter[0]_i_628_n_0\
    );
\caracter[0]_i_629\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => \caracter_reg[0]_i_23_n_7\,
      I1 => \caracter_reg[0]_i_23_n_4\,
      I2 => \caracter_reg[0]_i_532_n_7\,
      I3 => \caracter_reg[0]_i_23_n_6\,
      O => \caracter[0]_i_629_n_0\
    );
\caracter[0]_i_63\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(7),
      O => \caracter[0]_i_63_n_0\
    );
\caracter[0]_i_630\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter_reg[0]_i_23_n_4\,
      I1 => \caracter_reg[0]_i_23_n_7\,
      O => \caracter[0]_i_630_n_0\
    );
\caracter[0]_i_631\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_100_n_4\,
      I1 => \caracter_reg[0]_i_99_n_5\,
      I2 => \caracter_reg[0]_i_642_n_6\,
      I3 => \caracter_reg[0]_i_644_n_5\,
      I4 => \caracter[0]_i_689_n_0\,
      O => \caracter[0]_i_631_n_0\
    );
\caracter[0]_i_632\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_100_n_5\,
      I1 => \caracter_reg[0]_i_99_n_6\,
      I2 => \caracter_reg[0]_i_642_n_7\,
      I3 => \caracter_reg[0]_i_644_n_6\,
      I4 => \caracter[0]_i_690_n_0\,
      O => \caracter[0]_i_632_n_0\
    );
\caracter[0]_i_633\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_100_n_6\,
      I1 => \caracter_reg[0]_i_99_n_7\,
      I2 => \caracter_reg[0]_i_89_n_4\,
      I3 => \caracter_reg[0]_i_644_n_7\,
      I4 => \caracter[0]_i_691_n_0\,
      O => \caracter[0]_i_633_n_0\
    );
\caracter[0]_i_634\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_100_n_7\,
      I1 => \caracter_reg[0]_i_88_n_4\,
      I2 => \caracter_reg[0]_i_89_n_5\,
      I3 => \caracter_reg[0]_i_90_n_4\,
      I4 => \caracter[0]_i_692_n_0\,
      O => \caracter[0]_i_634_n_0\
    );
\caracter[0]_i_635\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \caracter[0]_i_631_n_0\,
      I1 => \caracter_reg[0]_i_644_n_4\,
      I2 => \caracter[0]_i_645_n_0\,
      I3 => \caracter_reg[0]_i_639_n_7\,
      I4 => \caracter_reg[0]_i_99_n_4\,
      I5 => \caracter_reg[0]_i_642_n_5\,
      O => \caracter[0]_i_635_n_0\
    );
\caracter[0]_i_636\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \caracter[0]_i_632_n_0\,
      I1 => \caracter[0]_i_693_n_0\,
      I2 => \caracter_reg[0]_i_642_n_5\,
      I3 => \caracter_reg[0]_i_99_n_4\,
      I4 => \caracter_reg[0]_i_639_n_7\,
      I5 => \caracter_reg[0]_i_644_n_5\,
      O => \caracter[0]_i_636_n_0\
    );
\caracter[0]_i_637\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \caracter[0]_i_633_n_0\,
      I1 => \caracter[0]_i_694_n_0\,
      I2 => \caracter_reg[0]_i_642_n_6\,
      I3 => \caracter_reg[0]_i_99_n_5\,
      I4 => \caracter_reg[0]_i_100_n_4\,
      I5 => \caracter_reg[0]_i_644_n_6\,
      O => \caracter[0]_i_637_n_0\
    );
\caracter[0]_i_638\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \caracter[0]_i_634_n_0\,
      I1 => \caracter[0]_i_695_n_0\,
      I2 => \caracter_reg[0]_i_642_n_7\,
      I3 => \caracter_reg[0]_i_99_n_6\,
      I4 => \caracter_reg[0]_i_100_n_5\,
      I5 => \caracter_reg[0]_i_644_n_7\,
      O => \caracter[0]_i_638_n_0\
    );
\caracter[0]_i_64\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(6),
      I1 => \caracter[3]_i_61_n_0\,
      O => \caracter[0]_i_64_n_0\
    );
\caracter[0]_i_641\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_542_n_6\,
      I1 => \caracter_reg[0]_i_416_n_5\,
      I2 => \caracter_reg[0]_i_639_n_4\,
      O => \caracter[0]_i_641_n_0\
    );
\caracter[0]_i_643\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_542_n_7\,
      I1 => \caracter_reg[0]_i_416_n_6\,
      I2 => \caracter_reg[0]_i_639_n_5\,
      O => \caracter[0]_i_643_n_0\
    );
\caracter[0]_i_645\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_642_n_4\,
      I1 => \caracter_reg[0]_i_416_n_7\,
      I2 => \caracter_reg[0]_i_639_n_6\,
      O => \caracter[0]_i_645_n_0\
    );
\caracter[0]_i_646\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[0]_i_542_n_4\,
      I1 => \caracter_reg[0]_i_541_n_6\,
      I2 => \caracter_reg[0]_i_287_n_7\,
      O => \caracter[0]_i_646_n_0\
    );
\caracter[0]_i_647\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[0]_i_542_n_5\,
      I1 => \caracter_reg[0]_i_541_n_7\,
      I2 => \caracter_reg[0]_i_416_n_4\,
      O => \caracter[0]_i_647_n_0\
    );
\caracter[0]_i_648\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter_reg[0]_i_639_n_5\,
      I1 => \caracter_reg[0]_i_416_n_6\,
      I2 => \caracter_reg[0]_i_542_n_7\,
      O => \caracter[0]_i_648_n_0\
    );
\caracter[0]_i_649\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter_reg[0]_i_639_n_6\,
      I1 => \caracter_reg[0]_i_416_n_7\,
      I2 => \caracter_reg[0]_i_642_n_4\,
      O => \caracter[0]_i_649_n_0\
    );
\caracter[0]_i_65\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(5),
      I1 => \caracter[3]_i_62_n_0\,
      O => \caracter[0]_i_65_n_0\
    );
\caracter[0]_i_650\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => euros3(25),
      I1 => euros3(21),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(19),
      O => \caracter[0]_i_650_n_0\
    );
\caracter[0]_i_651\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => euros3(24),
      I1 => euros3(20),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(18),
      O => \caracter[0]_i_651_n_0\
    );
\caracter[0]_i_652\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => euros3(23),
      I1 => euros3(19),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(17),
      O => \caracter[0]_i_652_n_0\
    );
\caracter[0]_i_653\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => euros3(22),
      I1 => euros3(18),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(16),
      O => \caracter[0]_i_653_n_0\
    );
\caracter[0]_i_654\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F6F90906"
    )
        port map (
      I0 => euros3(26),
      I1 => euros3(22),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(20),
      I4 => \caracter[0]_i_650_n_0\,
      O => \caracter[0]_i_654_n_0\
    );
\caracter[0]_i_655\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F6F90906"
    )
        port map (
      I0 => euros3(25),
      I1 => euros3(21),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(19),
      I4 => \caracter[0]_i_651_n_0\,
      O => \caracter[0]_i_655_n_0\
    );
\caracter[0]_i_656\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F6F90906"
    )
        port map (
      I0 => euros3(24),
      I1 => euros3(20),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(18),
      I4 => \caracter[0]_i_652_n_0\,
      O => \caracter[0]_i_656_n_0\
    );
\caracter[0]_i_657\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F6F90906"
    )
        port map (
      I0 => euros3(23),
      I1 => euros3(19),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(17),
      I4 => \caracter[0]_i_653_n_0\,
      O => \caracter[0]_i_657_n_0\
    );
\caracter[0]_i_658\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(24),
      I1 => \caracter_reg[0]_i_21_n_3\,
      O => euros2(24)
    );
\caracter[0]_i_659\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(23),
      I1 => \caracter_reg[0]_i_21_n_3\,
      O => euros2(23)
    );
\caracter[0]_i_66\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AA55A956"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(4),
      I1 => \caracter_reg[0]_i_13_n_4\,
      I2 => \caracter_reg[0]_i_13_n_5\,
      I3 => \caracter_reg[2]_i_40_n_7\,
      I4 => \caracter[2]_i_41_n_0\,
      O => \caracter[0]_i_66_n_0\
    );
\caracter[0]_i_660\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(22),
      I1 => \caracter_reg[0]_i_21_n_3\,
      O => euros2(22)
    );
\caracter[0]_i_661\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(21),
      I1 => \caracter_reg[0]_i_21_n_3\,
      O => euros2(21)
    );
\caracter[0]_i_662\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(24),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(27),
      O => \caracter[0]_i_662_n_0\
    );
\caracter[0]_i_663\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(23),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(26),
      O => \caracter[0]_i_663_n_0\
    );
\caracter[0]_i_664\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(22),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(25),
      O => \caracter[0]_i_664_n_0\
    );
\caracter[0]_i_665\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(21),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(24),
      O => \caracter[0]_i_665_n_0\
    );
\caracter[0]_i_667\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"5440"
    )
        port map (
      I0 => \caracter_reg[0]_i_21_n_3\,
      I1 => euros3(4),
      I2 => euros3(11),
      I3 => euros3(9),
      O => \caracter[0]_i_667_n_0\
    );
\caracter[0]_i_668\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(10),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(8),
      I3 => euros3(3),
      O => \caracter[0]_i_668_n_0\
    );
\caracter[0]_i_669\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCA0F0ACAC00A00"
    )
        port map (
      I0 => euros3(7),
      I1 => \caracter_reg[0]_i_29_n_4\,
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(9),
      I4 => \caracter_reg[0]_i_62_n_5\,
      I5 => euros3(2),
      O => \caracter[0]_i_669_n_0\
    );
\caracter[0]_i_670\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAFC00FCAAC000C0"
    )
        port map (
      I0 => \caracter_reg[0]_i_62_n_6\,
      I1 => euros3(1),
      I2 => euros3(8),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_29_n_5\,
      I5 => euros3(6),
      O => \caracter[0]_i_670_n_0\
    );
\caracter[0]_i_671\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5659A6A95956A9A6"
    )
        port map (
      I0 => \caracter[0]_i_667_n_0\,
      I1 => euros3(10),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(12),
      I4 => \caracter_reg[0]_i_29_n_6\,
      I5 => euros3(5),
      O => \caracter[0]_i_671_n_0\
    );
\caracter[0]_i_672\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5659A6A95956A9A6"
    )
        port map (
      I0 => \caracter[0]_i_668_n_0\,
      I1 => euros3(9),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(11),
      I4 => \caracter_reg[0]_i_29_n_7\,
      I5 => euros3(4),
      O => \caracter[0]_i_672_n_0\
    );
\caracter[0]_i_673\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5659A6A95956A9A6"
    )
        port map (
      I0 => \caracter[0]_i_669_n_0\,
      I1 => euros3(8),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(10),
      I4 => \caracter_reg[0]_i_62_n_4\,
      I5 => euros3(3),
      O => \caracter[0]_i_673_n_0\
    );
\caracter[0]_i_674\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9969666999966696"
    )
        port map (
      I0 => \caracter[0]_i_670_n_0\,
      I1 => euros2(2),
      I2 => euros3(7),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_29_n_4\,
      I5 => euros3(9),
      O => \caracter[0]_i_674_n_0\
    );
\caracter[0]_i_675\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"656A959A6A659A95"
    )
        port map (
      I0 => euros2(0),
      I1 => \caracter_reg[0]_i_62_n_6\,
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(1),
      I4 => \caracter_reg[0]_i_62_n_4\,
      I5 => euros3(3),
      O => \caracter[0]_i_675_n_0\
    );
\caracter[0]_i_676\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9A95"
    )
        port map (
      I0 => euros2(0),
      I1 => \caracter_reg[0]_i_62_n_5\,
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(2),
      O => \caracter[0]_i_676_n_0\
    );
\caracter[0]_i_677\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1D"
    )
        port map (
      I0 => euros3(1),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => \caracter_reg[0]_i_62_n_6\,
      O => \caracter[0]_i_677_n_0\
    );
\caracter[0]_i_679\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(6),
      I1 => \caracter_reg[3]_i_141_0\(1),
      O => \caracter[0]_i_679_n_0\
    );
\caracter[0]_i_68\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \caracter_reg[0]_i_76_n_7\,
      I1 => \caracter_reg[0]_i_76_n_5\,
      O => \caracter[0]_i_68_n_0\
    );
\caracter[0]_i_680\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(7),
      I1 => \caracter_reg[3]_i_141_0\(2),
      I2 => \caracter_reg[3]_i_141_0\(3),
      O => \caracter[0]_i_680_n_0\
    );
\caracter[0]_i_681\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(1),
      I1 => \caracter_reg[3]_i_141_0\(6),
      I2 => \caracter_reg[3]_i_141_0\(7),
      I3 => \caracter_reg[3]_i_141_0\(2),
      O => \caracter[0]_i_681_n_0\
    );
\caracter[0]_i_682\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"02A2"
    )
        port map (
      I0 => \caracter_reg[0]_i_23_n_5\,
      I1 => euros3(4),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => \caracter_reg[0]_i_29_n_7\,
      O => \caracter[0]_i_682_n_0\
    );
\caracter[0]_i_683\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"02A2"
    )
        port map (
      I0 => \caracter_reg[0]_i_23_n_6\,
      I1 => euros3(3),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => \caracter_reg[0]_i_62_n_4\,
      O => \caracter[0]_i_683_n_0\
    );
\caracter[0]_i_684\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"ABFB"
    )
        port map (
      I0 => \caracter_reg[0]_i_23_n_7\,
      I1 => euros3(2),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => \caracter_reg[0]_i_62_n_5\,
      O => \caracter[0]_i_684_n_0\
    );
\caracter[0]_i_685\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4700B8FFB8FF4700"
    )
        port map (
      I0 => \caracter_reg[0]_i_29_n_7\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(4),
      I3 => \caracter_reg[0]_i_23_n_5\,
      I4 => euros2(5),
      I5 => \caracter_reg[0]_i_523_n_7\,
      O => \caracter[0]_i_685_n_0\
    );
\caracter[0]_i_686\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8FF47004700B8FF"
    )
        port map (
      I0 => \caracter_reg[0]_i_62_n_4\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(3),
      I3 => \caracter_reg[0]_i_23_n_6\,
      I4 => euros2(4),
      I5 => \caracter_reg[0]_i_23_n_5\,
      O => \caracter[0]_i_686_n_0\
    );
\caracter[0]_i_687\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2D222DDDD2DDD222"
    )
        port map (
      I0 => euros2(2),
      I1 => \caracter_reg[0]_i_23_n_7\,
      I2 => \caracter_reg[0]_i_62_n_4\,
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => euros3(3),
      I5 => \caracter_reg[0]_i_23_n_6\,
      O => \caracter[0]_i_687_n_0\
    );
\caracter[0]_i_688\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1DE2"
    )
        port map (
      I0 => euros3(2),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => \caracter_reg[0]_i_62_n_5\,
      I3 => \caracter_reg[0]_i_23_n_7\,
      O => \caracter[0]_i_688_n_0\
    );
\caracter[0]_i_689\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_642_n_5\,
      I1 => \caracter_reg[0]_i_99_n_4\,
      I2 => \caracter_reg[0]_i_639_n_7\,
      O => \caracter[0]_i_689_n_0\
    );
\caracter[0]_i_69\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_138_n_4\,
      I1 => \caracter_reg[0]_i_76_n_6\,
      I2 => \caracter_reg[0]_i_77_n_7\,
      O => \caracter[0]_i_69_n_0\
    );
\caracter[0]_i_690\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_642_n_6\,
      I1 => \caracter_reg[0]_i_99_n_5\,
      I2 => \caracter_reg[0]_i_100_n_4\,
      O => \caracter[0]_i_690_n_0\
    );
\caracter[0]_i_691\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_642_n_7\,
      I1 => \caracter_reg[0]_i_99_n_6\,
      I2 => \caracter_reg[0]_i_100_n_5\,
      O => \caracter[0]_i_691_n_0\
    );
\caracter[0]_i_692\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_89_n_4\,
      I1 => \caracter_reg[0]_i_99_n_7\,
      I2 => \caracter_reg[0]_i_100_n_6\,
      O => \caracter[0]_i_692_n_0\
    );
\caracter[0]_i_693\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter_reg[0]_i_100_n_4\,
      I1 => \caracter_reg[0]_i_99_n_5\,
      I2 => \caracter_reg[0]_i_642_n_6\,
      O => \caracter[0]_i_693_n_0\
    );
\caracter[0]_i_694\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter_reg[0]_i_100_n_5\,
      I1 => \caracter_reg[0]_i_99_n_6\,
      I2 => \caracter_reg[0]_i_642_n_7\,
      O => \caracter[0]_i_694_n_0\
    );
\caracter[0]_i_695\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter_reg[0]_i_100_n_6\,
      I1 => \caracter_reg[0]_i_99_n_7\,
      I2 => \caracter_reg[0]_i_89_n_4\,
      O => \caracter[0]_i_695_n_0\
    );
\caracter[0]_i_696\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => euros3(21),
      I1 => euros3(17),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(15),
      O => \caracter[0]_i_696_n_0\
    );
\caracter[0]_i_697\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => euros3(20),
      I1 => euros3(16),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(14),
      O => \caracter[0]_i_697_n_0\
    );
\caracter[0]_i_698\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => euros3(19),
      I1 => euros3(15),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(13),
      O => \caracter[0]_i_698_n_0\
    );
\caracter[0]_i_699\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(18),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(14),
      I3 => euros3(12),
      O => \caracter[0]_i_699_n_0\
    );
\caracter[0]_i_70\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_138_n_5\,
      I1 => \caracter_reg[0]_i_76_n_7\,
      I2 => \caracter_reg[0]_i_76_n_4\,
      O => \caracter[0]_i_70_n_0\
    );
\caracter[0]_i_700\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F6F90906"
    )
        port map (
      I0 => euros3(22),
      I1 => euros3(18),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(16),
      I4 => \caracter[0]_i_696_n_0\,
      O => \caracter[0]_i_700_n_0\
    );
\caracter[0]_i_701\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F6F90906"
    )
        port map (
      I0 => euros3(21),
      I1 => euros3(17),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(15),
      I4 => \caracter[0]_i_697_n_0\,
      O => \caracter[0]_i_701_n_0\
    );
\caracter[0]_i_702\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F6F90906"
    )
        port map (
      I0 => euros3(20),
      I1 => euros3(16),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(14),
      I4 => \caracter[0]_i_698_n_0\,
      O => \caracter[0]_i_702_n_0\
    );
\caracter[0]_i_703\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F6F90906"
    )
        port map (
      I0 => euros3(19),
      I1 => euros3(15),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(13),
      I4 => \caracter[0]_i_699_n_0\,
      O => \caracter[0]_i_703_n_0\
    );
\caracter[0]_i_704\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(30),
      I1 => \caracter_reg[0]_i_21_n_3\,
      O => \caracter[0]_i_704_n_0\
    );
\caracter[0]_i_705\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(20),
      I1 => \caracter_reg[0]_i_21_n_3\,
      O => euros2(20)
    );
\caracter[0]_i_706\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(19),
      I1 => \caracter_reg[0]_i_21_n_3\,
      O => euros2(19)
    );
\caracter[0]_i_707\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(18),
      I1 => \caracter_reg[0]_i_21_n_3\,
      O => euros2(18)
    );
\caracter[0]_i_708\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(17),
      I1 => \caracter_reg[0]_i_21_n_3\,
      O => euros2(17)
    );
\caracter[0]_i_709\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(20),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(23),
      O => \caracter[0]_i_709_n_0\
    );
\caracter[0]_i_71\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_138_n_6\,
      I1 => \caracter_reg[0]_i_138_n_4\,
      I2 => \caracter_reg[0]_i_76_n_5\,
      O => \caracter[0]_i_71_n_0\
    );
\caracter[0]_i_710\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(19),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(22),
      O => \caracter[0]_i_710_n_0\
    );
\caracter[0]_i_711\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(18),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(21),
      O => \caracter[0]_i_711_n_0\
    );
\caracter[0]_i_712\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(17),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(20),
      O => \caracter[0]_i_712_n_0\
    );
\caracter[0]_i_713\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(26),
      I1 => \caracter_reg[0]_i_21_n_3\,
      O => \caracter[0]_i_713_n_0\
    );
\caracter[0]_i_714\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(29),
      I1 => \caracter_reg[0]_i_21_n_3\,
      O => \caracter[0]_i_714_n_0\
    );
\caracter[0]_i_715\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(28),
      I1 => \caracter_reg[0]_i_21_n_3\,
      O => \caracter[0]_i_715_n_0\
    );
\caracter[0]_i_716\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(27),
      I1 => \caracter_reg[0]_i_21_n_3\,
      O => \caracter[0]_i_716_n_0\
    );
\caracter[0]_i_717\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0708"
    )
        port map (
      I0 => euros3(25),
      I1 => euros3(30),
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(26),
      O => \caracter[0]_i_717_n_0\
    );
\caracter[0]_i_719\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A35CAC5353AC5CA"
    )
        port map (
      I0 => euros3(8),
      I1 => \caracter_reg[0]_i_62_n_6\,
      I2 => \caracter_reg[0]_i_21_n_3\,
      I3 => euros3(1),
      I4 => \caracter_reg[0]_i_29_n_5\,
      I5 => euros3(6),
      O => \caracter[0]_i_719_n_0\
    );
\caracter[0]_i_72\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B44B"
    )
        port map (
      I0 => \caracter_reg[0]_i_76_n_5\,
      I1 => \caracter_reg[0]_i_76_n_7\,
      I2 => \caracter_reg[0]_i_76_n_4\,
      I3 => \caracter_reg[0]_i_76_n_6\,
      O => \caracter[0]_i_72_n_0\
    );
\caracter[0]_i_720\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E2"
    )
        port map (
      I0 => euros3(7),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => \caracter_reg[0]_i_29_n_4\,
      O => euros2(7)
    );
\caracter[0]_i_721\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E2"
    )
        port map (
      I0 => euros3(6),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => \caracter_reg[0]_i_29_n_5\,
      O => \caracter[0]_i_721_n_0\
    );
\caracter[0]_i_722\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_29_n_6\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(5),
      O => \caracter[0]_i_722_n_0\
    );
\caracter[0]_i_723\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6696996999699969"
    )
        port map (
      I0 => \caracter[0]_i_740_n_0\,
      I1 => euros2(1),
      I2 => euros3(8),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => euros2(0),
      I5 => euros2(5),
      O => \caracter[0]_i_723_n_0\
    );
\caracter[0]_i_724\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9999A55A6666A55A"
    )
        port map (
      I0 => euros2(0),
      I1 => \caracter_reg[0]_i_29_n_6\,
      I2 => euros3(5),
      I3 => euros3(7),
      I4 => \caracter_reg[0]_i_21_n_3\,
      I5 => \caracter_reg[0]_i_29_n_4\,
      O => \caracter[0]_i_724_n_0\
    );
\caracter[0]_i_725\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"335ACC5A"
    )
        port map (
      I0 => euros3(6),
      I1 => \caracter_reg[0]_i_29_n_5\,
      I2 => euros3(4),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_29_n_7\,
      O => \caracter[0]_i_725_n_0\
    );
\caracter[0]_i_726\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"335ACC5A"
    )
        port map (
      I0 => euros3(5),
      I1 => \caracter_reg[0]_i_29_n_6\,
      I2 => euros3(3),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_62_n_4\,
      O => \caracter[0]_i_726_n_0\
    );
\caracter[0]_i_728\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(6),
      I1 => \caracter_reg[3]_i_141_0\(1),
      O => \caracter[0]_i_728_n_0\
    );
\caracter[0]_i_729\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(1),
      I1 => \caracter_reg[3]_i_141_0\(6),
      I2 => \caracter_reg[3]_i_141_0\(0),
      I3 => \caracter_reg[3]_i_141_0\(5),
      O => \caracter[0]_i_729_n_0\
    );
\caracter[0]_i_73\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4DB2B24D"
    )
        port map (
      I0 => \caracter_reg[0]_i_77_n_7\,
      I1 => \caracter_reg[0]_i_76_n_6\,
      I2 => \caracter_reg[0]_i_138_n_4\,
      I3 => \caracter_reg[0]_i_76_n_5\,
      I4 => \caracter_reg[0]_i_76_n_7\,
      O => \caracter[0]_i_73_n_0\
    );
\caracter[0]_i_730\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(0),
      I1 => \caracter_reg[3]_i_141_0\(5),
      I2 => \caracter_reg[3]_i_141_0\(7),
      O => \caracter[0]_i_730_n_0\
    );
\caracter[0]_i_731\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(6),
      I1 => \caracter_reg[3]_i_141_0\(4),
      O => \caracter[0]_i_731_n_0\
    );
\caracter[0]_i_732\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(5),
      I1 => \caracter_reg[3]_i_141_0\(3),
      O => \caracter[0]_i_732_n_0\
    );
\caracter[0]_i_733\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_29_n_7\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(4),
      O => \caracter[0]_i_733_n_0\
    );
\caracter[0]_i_734\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_62_n_4\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(3),
      O => \caracter[0]_i_734_n_0\
    );
\caracter[0]_i_735\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_62_n_5\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(2),
      O => \caracter[0]_i_735_n_0\
    );
\caracter[0]_i_736\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"335ACC5A"
    )
        port map (
      I0 => euros3(4),
      I1 => \caracter_reg[0]_i_29_n_7\,
      I2 => euros3(2),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_62_n_5\,
      O => \caracter[0]_i_736_n_0\
    );
\caracter[0]_i_737\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"335ACC5A"
    )
        port map (
      I0 => euros3(3),
      I1 => \caracter_reg[0]_i_62_n_4\,
      I2 => euros3(1),
      I3 => \caracter_reg[0]_i_21_n_3\,
      I4 => \caracter_reg[0]_i_62_n_6\,
      O => \caracter[0]_i_737_n_0\
    );
\caracter[0]_i_738\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1DE2"
    )
        port map (
      I0 => euros3(2),
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => \caracter_reg[0]_i_62_n_5\,
      I3 => euros2(0),
      O => \caracter[0]_i_738_n_0\
    );
\caracter[0]_i_739\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_62_n_6\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(1),
      O => \caracter[0]_i_739_n_0\
    );
\caracter[0]_i_74\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter[0]_i_70_n_0\,
      I1 => \caracter_reg[0]_i_138_n_4\,
      I2 => \caracter_reg[0]_i_76_n_6\,
      I3 => \caracter_reg[0]_i_77_n_7\,
      O => \caracter[0]_i_74_n_0\
    );
\caracter[0]_i_740\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \caracter_reg[0]_i_29_n_5\,
      I1 => \caracter_reg[0]_i_21_n_3\,
      I2 => euros3(6),
      O => \caracter[0]_i_740_n_0\
    );
\caracter[0]_i_741\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(4),
      I1 => \caracter_reg[3]_i_141_0\(2),
      O => \caracter[0]_i_741_n_0\
    );
\caracter[0]_i_742\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(3),
      I1 => \caracter_reg[3]_i_141_0\(1),
      O => \caracter[0]_i_742_n_0\
    );
\caracter[0]_i_743\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(2),
      I1 => \caracter_reg[3]_i_141_0\(0),
      O => \caracter[0]_i_743_n_0\
    );
\caracter[0]_i_75\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_138_n_5\,
      I1 => \caracter_reg[0]_i_76_n_7\,
      I2 => \caracter_reg[0]_i_76_n_4\,
      I3 => \caracter[0]_i_71_n_0\,
      O => \caracter[0]_i_75_n_0\
    );
\caracter[0]_i_79\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_157_n_4\,
      I1 => \caracter_reg[0]_i_97_n_5\,
      I2 => \caracter_reg[0]_i_93_n_6\,
      I3 => \caracter_reg[0]_i_95_n_5\,
      I4 => \caracter[0]_i_158_n_0\,
      O => \caracter[0]_i_79_n_0\
    );
\caracter[0]_i_80\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_157_n_5\,
      I1 => \caracter_reg[0]_i_97_n_6\,
      I2 => \caracter_reg[0]_i_93_n_7\,
      I3 => \caracter[0]_i_159_n_0\,
      I4 => \caracter_reg[0]_i_95_n_6\,
      O => \caracter[0]_i_80_n_0\
    );
\caracter[0]_i_81\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_157_n_6\,
      I1 => \caracter_reg[0]_i_97_n_7\,
      I2 => \caracter_reg[0]_i_160_n_4\,
      I3 => \caracter_reg[0]_i_95_n_7\,
      I4 => \caracter[0]_i_161_n_0\,
      O => \caracter[0]_i_81_n_0\
    );
\caracter[0]_i_82\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_157_n_7\,
      I1 => \caracter_reg[0]_i_162_n_4\,
      I2 => \caracter_reg[0]_i_160_n_5\,
      I3 => \caracter_reg[0]_i_163_n_4\,
      I4 => \caracter[0]_i_164_n_0\,
      O => \caracter[0]_i_82_n_0\
    );
\caracter[0]_i_83\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \caracter[0]_i_79_n_0\,
      I1 => \caracter_reg[0]_i_95_n_4\,
      I2 => \caracter[0]_i_96_n_0\,
      I3 => \caracter_reg[0]_i_87_n_7\,
      I4 => \caracter_reg[0]_i_97_n_4\,
      I5 => \caracter_reg[0]_i_93_n_5\,
      O => \caracter[0]_i_83_n_0\
    );
\caracter[0]_i_84\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \caracter[0]_i_80_n_0\,
      I1 => \caracter[0]_i_165_n_0\,
      I2 => \caracter_reg[0]_i_93_n_5\,
      I3 => \caracter_reg[0]_i_97_n_4\,
      I4 => \caracter_reg[0]_i_87_n_7\,
      I5 => \caracter_reg[0]_i_95_n_5\,
      O => \caracter[0]_i_84_n_0\
    );
\caracter[0]_i_85\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \caracter[0]_i_81_n_0\,
      I1 => \caracter_reg[0]_i_95_n_6\,
      I2 => \caracter[0]_i_159_n_0\,
      I3 => \caracter_reg[0]_i_157_n_5\,
      I4 => \caracter_reg[0]_i_97_n_6\,
      I5 => \caracter_reg[0]_i_93_n_7\,
      O => \caracter[0]_i_85_n_0\
    );
\caracter[0]_i_86\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \caracter[0]_i_82_n_0\,
      I1 => \caracter[0]_i_166_n_0\,
      I2 => \caracter_reg[0]_i_93_n_7\,
      I3 => \caracter_reg[0]_i_97_n_6\,
      I4 => \caracter_reg[0]_i_157_n_5\,
      I5 => \caracter_reg[0]_i_95_n_7\,
      O => \caracter[0]_i_86_n_0\
    );
\caracter[0]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCAF0CAFFCA00CA0"
    )
        port map (
      I0 => \caracter[6]_i_38_0\,
      I1 => \^fsm_sequential_current_state_reg[1]_0\,
      I2 => Q(0),
      I3 => Q(1),
      I4 => \^fsm_sequential_current_state_reg[1]_1\,
      I5 => \caracter[0]_i_4\,
      O => \FSM_sequential_current_state_reg[1]\
    );
\caracter[0]_i_91\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_89_n_5\,
      I1 => \caracter_reg[0]_i_88_n_4\,
      I2 => \caracter_reg[0]_i_100_n_7\,
      O => \caracter[0]_i_91_n_0\
    );
\caracter[0]_i_92\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_89_n_6\,
      I1 => \caracter_reg[0]_i_88_n_5\,
      I2 => \caracter_reg[0]_i_87_n_4\,
      O => \caracter[0]_i_92_n_0\
    );
\caracter[0]_i_94\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_89_n_7\,
      I1 => \caracter_reg[0]_i_88_n_6\,
      I2 => \caracter_reg[0]_i_87_n_5\,
      O => \caracter[0]_i_94_n_0\
    );
\caracter[0]_i_96\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_93_n_4\,
      I1 => \caracter_reg[0]_i_88_n_7\,
      I2 => \caracter_reg[0]_i_87_n_6\,
      O => \caracter[0]_i_96_n_0\
    );
\caracter[0]_i_98\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter_reg[0]_i_100_n_7\,
      I1 => \caracter_reg[0]_i_88_n_4\,
      I2 => \caracter_reg[0]_i_89_n_5\,
      O => \caracter[0]_i_98_n_0\
    );
\caracter[1]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F3F7F7F3FFFFFFFF"
    )
        port map (
      I0 => \caracter[3]_i_11_n_0\,
      I1 => \caracter_reg[1]_i_4\,
      I2 => \caracter[3]_i_12_n_0\,
      I3 => \caracter[3]_i_13_n_0\,
      I4 => \caracter[3]_i_14_n_0\,
      I5 => \caracter[1]_i_15_n_0\,
      O => \FSM_sequential_current_state_reg[0]_0\
    );
\caracter[1]_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"308EEF30EF3008E0"
    )
        port map (
      I0 => \caracter[3]_i_24_n_0\,
      I1 => \caracter[3]_i_25_n_0\,
      I2 => \caracter[3]_i_16_n_0\,
      I3 => \caracter[3]_i_20_n_0\,
      I4 => \caracter[3]_i_23_n_0\,
      I5 => \caracter[3]_i_17_n_0\,
      O => \caracter[1]_i_15_n_0\
    );
\caracter[1]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AA28882AAAAAAAAA"
    )
        port map (
      I0 => \caracter[6]_i_4_1\,
      I1 => \caracter_reg[2]_i_15_n_4\,
      I2 => \caracter_reg[2]_i_15_n_5\,
      I3 => \caracter_reg[2]_i_15_n_6\,
      I4 => \caracter_reg[2]_i_14_n_7\,
      I5 => \caracter[6]_i_38_0\,
      O => \^fsm_sequential_current_state_reg[1]_2\
    );
\caracter[1]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCAF0CAFFCA00CA0"
    )
        port map (
      I0 => \caracter[1]_i_3\,
      I1 => \caracter[1]_i_3_0\,
      I2 => Q(0),
      I3 => Q(1),
      I4 => \^fsm_sequential_current_state_reg[1]_2\,
      I5 => \caracter[1]_i_3_1\,
      O => \reg_reg[0]\
    );
\caracter[2]_i_100\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \caracter[2]_i_116_n_0\,
      I1 => cent(4),
      O => \caracter[2]_i_100_n_0\
    );
\caracter[2]_i_101\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter[3]_i_58_n_0\,
      I1 => \caracter[2]_i_23_n_0\,
      O => \caracter[2]_i_101_n_0\
    );
\caracter[2]_i_102\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => cent(4),
      I1 => \caracter[2]_i_190_n_0\,
      O => \caracter[2]_i_102_n_0\
    );
\caracter[2]_i_103\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(4),
      I1 => \caracter[2]_i_190_n_0\,
      O => \caracter[2]_i_103_n_0\
    );
\caracter[2]_i_104\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => cent(4),
      I1 => \caracter[2]_i_116_n_0\,
      I2 => \caracter[3]_i_58_n_0\,
      O => \caracter[2]_i_104_n_0\
    );
\caracter[2]_i_105\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7887"
    )
        port map (
      I0 => \caracter[2]_i_23_n_0\,
      I1 => \caracter[3]_i_58_n_0\,
      I2 => \caracter[2]_i_116_n_0\,
      I3 => cent(4),
      O => \caracter[2]_i_105_n_0\
    );
\caracter[2]_i_106\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => \caracter[2]_i_190_n_0\,
      I1 => cent(4),
      I2 => \caracter[3]_i_58_n_0\,
      I3 => \caracter[2]_i_23_n_0\,
      O => \caracter[2]_i_106_n_0\
    );
\caracter[2]_i_107\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"2BD4D42B"
    )
        port map (
      I0 => \caracter[2]_i_23_n_0\,
      I1 => \caracter[2]_i_116_n_0\,
      I2 => \caracter[2]_i_219_n_0\,
      I3 => \caracter[2]_i_190_n_0\,
      I4 => cent(4),
      O => \caracter[2]_i_107_n_0\
    );
\caracter[2]_i_108\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter_reg[2]_i_93_n_3\,
      I1 => \caracter[0]_i_11_n_0\,
      I2 => \caracter[2]_i_190_n_0\,
      O => \caracter[2]_i_108_n_0\
    );
\caracter[2]_i_109\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[2]_i_93_n_3\,
      I1 => \caracter[2]_i_190_n_0\,
      I2 => \caracter[0]_i_11_n_0\,
      O => \caracter[2]_i_109_n_0\
    );
\caracter[2]_i_110\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \caracter[0]_i_11_n_0\,
      I1 => \caracter_reg[2]_i_93_n_3\,
      O => \caracter[2]_i_110_n_0\
    );
\caracter[2]_i_111\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \caracter[2]_i_219_n_0\,
      I1 => \caracter_reg[2]_i_93_n_3\,
      I2 => \caracter[2]_i_23_n_0\,
      I3 => \caracter[2]_i_108_n_0\,
      O => \caracter[2]_i_111_n_0\
    );
\caracter[2]_i_112\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"693C"
    )
        port map (
      I0 => \caracter_reg[2]_i_93_n_3\,
      I1 => \caracter[0]_i_11_n_0\,
      I2 => \caracter[2]_i_190_n_0\,
      I3 => \caracter[2]_i_219_n_0\,
      O => \caracter[2]_i_112_n_0\
    );
\caracter[2]_i_113\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"93"
    )
        port map (
      I0 => \caracter[0]_i_11_n_0\,
      I1 => \caracter[2]_i_219_n_0\,
      I2 => \caracter_reg[2]_i_93_n_3\,
      O => \caracter[2]_i_113_n_0\
    );
\caracter[2]_i_114\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[0]_i_11_n_0\,
      I1 => \caracter_reg[2]_i_93_n_3\,
      O => \caracter[2]_i_114_n_0\
    );
\caracter[2]_i_117\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \caracter[3]_i_61_n_0\,
      I1 => \^total_out_reg[5]\,
      I2 => \caracter_reg[3]_i_141_0\(6),
      O => \caracter[2]_i_117_n_0\
    );
\caracter[2]_i_118\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \caracter[3]_i_61_n_0\,
      I1 => \^total_out_reg[5]\,
      I2 => \caracter_reg[3]_i_141_0\(6),
      O => \caracter[2]_i_118_n_0\
    );
\caracter[2]_i_119\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[3]_i_58_n_0\,
      I1 => \caracter[2]_i_116_n_0\,
      O => \caracter[2]_i_119_n_0\
    );
\caracter[2]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5CF05C005CFF5C0F"
    )
        port map (
      I0 => \caracter[2]_i_9_n_0\,
      I1 => \caracter[2]_i_6\,
      I2 => Q(1),
      I3 => Q(0),
      I4 => \caracter[2]_i_6_0\,
      I5 => \caracter[2]_i_8_n_0\,
      O => \reg_reg[1]_0\
    );
\caracter[2]_i_121\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \caracter[3]_i_61_n_0\,
      I1 => \^total_out_reg[5]\,
      I2 => \caracter_reg[3]_i_141_0\(6),
      O => \caracter[2]_i_121_n_0\
    );
\caracter[2]_i_122\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \caracter[3]_i_61_n_0\,
      I1 => \^total_out_reg[5]\,
      I2 => \caracter_reg[3]_i_141_0\(6),
      O => \caracter[2]_i_122_n_0\
    );
\caracter[2]_i_123\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[3]_i_58_n_0\,
      I1 => \caracter[2]_i_116_n_0\,
      O => \caracter[2]_i_123_n_0\
    );
\caracter[2]_i_124\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[2]_i_93_n_3\,
      I1 => \caracter[3]_i_58_n_0\,
      O => \caracter[2]_i_124_n_0\
    );
\caracter[2]_i_125\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8E"
    )
        port map (
      I0 => \caracter[2]_i_23_n_0\,
      I1 => \caracter[3]_i_58_n_0\,
      I2 => \caracter_reg[2]_i_93_n_3\,
      O => \caracter[2]_i_125_n_0\
    );
\caracter[2]_i_126\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4D"
    )
        port map (
      I0 => \caracter_reg[2]_i_93_n_3\,
      I1 => \caracter[2]_i_190_n_0\,
      I2 => cent(4),
      O => \caracter[2]_i_126_n_0\
    );
\caracter[2]_i_127\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \caracter[2]_i_219_n_0\,
      I1 => \caracter_reg[2]_i_93_n_3\,
      I2 => \caracter[2]_i_23_n_0\,
      O => \caracter[2]_i_127_n_0\
    );
\caracter[2]_i_128\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"D2B4"
    )
        port map (
      I0 => cent(4),
      I1 => \caracter[2]_i_116_n_0\,
      I2 => \caracter[3]_i_58_n_0\,
      I3 => \caracter_reg[2]_i_93_n_3\,
      O => \caracter[2]_i_128_n_0\
    );
\caracter[2]_i_129\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \caracter[2]_i_125_n_0\,
      I1 => cent(4),
      I2 => \caracter[2]_i_116_n_0\,
      I3 => \caracter_reg[2]_i_93_n_3\,
      O => \caracter[2]_i_129_n_0\
    );
\caracter[2]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3B33FFFF3FF7FF"
    )
        port map (
      I0 => \caracter[3]_i_11_n_0\,
      I1 => \caracter[6]_i_38_0\,
      I2 => \caracter[2]_i_19_n_0\,
      I3 => \caracter[2]_i_20_n_0\,
      I4 => \caracter[2]_i_21_n_0\,
      I5 => \caracter[3]_i_25_n_0\,
      O => \caracter[2]_i_13_n_0\
    );
\caracter[2]_i_130\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter[2]_i_23_n_0\,
      I1 => \caracter[3]_i_58_n_0\,
      I2 => \caracter_reg[2]_i_93_n_3\,
      I3 => \caracter[2]_i_126_n_0\,
      O => \caracter[2]_i_130_n_0\
    );
\caracter[2]_i_131\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \caracter_reg[2]_i_93_n_3\,
      I1 => \caracter[2]_i_190_n_0\,
      I2 => cent(4),
      I3 => \caracter[2]_i_127_n_0\,
      O => \caracter[2]_i_131_n_0\
    );
\caracter[2]_i_132\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"65"
    )
        port map (
      I0 => \caracter[2]_i_116_n_0\,
      I1 => \caracter[3]_i_58_n_0\,
      I2 => \caracter_reg[2]_i_93_n_3\,
      O => \caracter[2]_i_132_n_0\
    );
\caracter[2]_i_133\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \caracter_reg[0]_i_24_n_4\,
      I1 => \caracter_reg[0]_i_24_n_7\,
      O => \caracter[2]_i_133_n_0\
    );
\caracter[2]_i_134\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66969969"
    )
        port map (
      I0 => \caracter_reg[2]_i_175_n_6\,
      I1 => \caracter_reg[0]_i_24_n_5\,
      I2 => \caracter_reg[2]_i_175_n_7\,
      I3 => \caracter_reg[0]_i_24_n_6\,
      I4 => \caracter_reg[0]_i_24_n_7\,
      O => \caracter[2]_i_134_n_0\
    );
\caracter[2]_i_135\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => \caracter_reg[0]_i_24_n_7\,
      I1 => \caracter_reg[0]_i_24_n_4\,
      I2 => \caracter_reg[2]_i_175_n_7\,
      I3 => \caracter_reg[0]_i_24_n_6\,
      O => \caracter[2]_i_135_n_0\
    );
\caracter[2]_i_136\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter_reg[0]_i_24_n_4\,
      I1 => \caracter_reg[0]_i_24_n_7\,
      O => \caracter[2]_i_136_n_0\
    );
\caracter[2]_i_138\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B190"
    )
        port map (
      I0 => \caracter_reg[2]_i_146_n_7\,
      I1 => \caracter_reg[2]_i_93_n_3\,
      I2 => \caracter_reg[2]_i_73_n_7\,
      I3 => \caracter_reg[2]_i_185_n_4\,
      O => \caracter[2]_i_138_n_0\
    );
\caracter[2]_i_139\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B190"
    )
        port map (
      I0 => \caracter_reg[2]_i_185_n_4\,
      I1 => \caracter_reg[2]_i_93_n_3\,
      I2 => \caracter_reg[2]_i_115_n_4\,
      I3 => \caracter_reg[2]_i_185_n_5\,
      O => \caracter[2]_i_139_n_0\
    );
\caracter[2]_i_140\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B190"
    )
        port map (
      I0 => \caracter_reg[2]_i_185_n_5\,
      I1 => \caracter_reg[2]_i_93_n_3\,
      I2 => \caracter_reg[2]_i_115_n_5\,
      I3 => \caracter_reg[2]_i_185_n_6\,
      O => \caracter[2]_i_140_n_0\
    );
\caracter[2]_i_141\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A832"
    )
        port map (
      I0 => \caracter_reg[2]_i_115_n_6\,
      I1 => \caracter_reg[2]_i_93_n_3\,
      I2 => \caracter_reg[2]_i_186_n_7\,
      I3 => \caracter_reg[2]_i_185_n_6\,
      O => \caracter[2]_i_141_n_0\
    );
\caracter[2]_i_142\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669966996696996"
    )
        port map (
      I0 => \caracter[2]_i_138_n_0\,
      I1 => \caracter_reg[2]_i_73_n_6\,
      I2 => \caracter_reg[2]_i_146_n_6\,
      I3 => \caracter[0]_i_11_n_0\,
      I4 => \caracter_reg[2]_i_93_n_3\,
      I5 => \caracter_reg[2]_i_146_n_7\,
      O => \caracter[2]_i_142_n_0\
    );
\caracter[2]_i_143\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C3873C780F1EF0E1"
    )
        port map (
      I0 => \caracter_reg[2]_i_185_n_5\,
      I1 => \caracter_reg[2]_i_115_n_4\,
      I2 => \caracter_reg[2]_i_73_n_7\,
      I3 => \caracter_reg[2]_i_93_n_3\,
      I4 => \caracter_reg[2]_i_146_n_7\,
      I5 => \caracter_reg[2]_i_185_n_4\,
      O => \caracter[2]_i_143_n_0\
    );
\caracter[2]_i_144\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C3873C780F1EF0E1"
    )
        port map (
      I0 => \caracter_reg[2]_i_185_n_6\,
      I1 => \caracter_reg[2]_i_115_n_5\,
      I2 => \caracter_reg[2]_i_115_n_4\,
      I3 => \caracter_reg[2]_i_93_n_3\,
      I4 => \caracter_reg[2]_i_185_n_4\,
      I5 => \caracter_reg[2]_i_185_n_5\,
      O => \caracter[2]_i_144_n_0\
    );
\caracter[2]_i_145\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C3873C780F1EF0E1"
    )
        port map (
      I0 => \caracter_reg[2]_i_186_n_7\,
      I1 => \caracter_reg[2]_i_115_n_6\,
      I2 => \caracter_reg[2]_i_115_n_5\,
      I3 => \caracter_reg[2]_i_93_n_3\,
      I4 => \caracter_reg[2]_i_185_n_5\,
      I5 => \caracter_reg[2]_i_185_n_6\,
      O => \caracter[2]_i_145_n_0\
    );
\caracter[2]_i_147\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2BB2"
    )
        port map (
      I0 => \caracter_reg[2]_i_93_n_3\,
      I1 => \caracter_reg[2]_i_94_n_7\,
      I2 => \caracter[2]_i_23_n_0\,
      I3 => \caracter[0]_i_11_n_0\,
      O => \caracter[2]_i_147_n_0\
    );
\caracter[2]_i_148\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter[0]_i_11_n_0\,
      I1 => \caracter[2]_i_23_n_0\,
      I2 => \caracter_reg[2]_i_94_n_7\,
      I3 => \caracter_reg[2]_i_93_n_3\,
      O => \caracter[2]_i_148_n_0\
    );
\caracter[2]_i_149\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => \caracter_reg[2]_i_93_n_3\,
      I1 => \caracter[2]_i_219_n_0\,
      I2 => \caracter_reg[2]_i_146_n_5\,
      O => \caracter[2]_i_149_n_0\
    );
\caracter[2]_i_150\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[2]_i_146_n_5\,
      I1 => \caracter[2]_i_219_n_0\,
      I2 => \caracter_reg[2]_i_93_n_3\,
      O => \caracter[2]_i_150_n_0\
    );
\caracter[2]_i_153\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \caracter[3]_i_61_n_0\,
      I1 => \^total_out_reg[5]\,
      I2 => \caracter_reg[3]_i_141_0\(6),
      O => \caracter[2]_i_153_n_0\
    );
\caracter[2]_i_154\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"D"
    )
        port map (
      I0 => \caracter[3]_i_58_n_0\,
      I1 => \caracter[2]_i_23_n_0\,
      O => \caracter[2]_i_154_n_0\
    );
\caracter[2]_i_155\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \caracter[3]_i_61_n_0\,
      I1 => \^total_out_reg[5]\,
      I2 => \caracter_reg[3]_i_141_0\(6),
      O => \caracter[2]_i_155_n_0\
    );
\caracter[2]_i_156\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[3]_i_58_n_0\,
      I1 => \caracter[2]_i_116_n_0\,
      O => \caracter[2]_i_156_n_0\
    );
\caracter[2]_i_157\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1E"
    )
        port map (
      I0 => cent(4),
      I1 => \caracter[2]_i_116_n_0\,
      I2 => \caracter[3]_i_58_n_0\,
      O => \caracter[2]_i_157_n_0\
    );
\caracter[2]_i_158\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => \caracter[3]_i_58_n_0\,
      I1 => \caracter[2]_i_23_n_0\,
      I2 => cent(4),
      I3 => \caracter[2]_i_116_n_0\,
      O => \caracter[2]_i_158_n_0\
    );
\caracter[2]_i_159\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter[0]_i_11_n_0\,
      I1 => \caracter[2]_i_190_n_0\,
      I2 => \caracter[3]_i_58_n_0\,
      O => \caracter[2]_i_159_n_0\
    );
\caracter[2]_i_160\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter[3]_i_58_n_0\,
      I1 => \caracter[2]_i_190_n_0\,
      I2 => \caracter[0]_i_11_n_0\,
      O => \caracter[2]_i_160_n_0\
    );
\caracter[2]_i_161\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \caracter[0]_i_11_n_0\,
      I1 => \caracter[2]_i_23_n_0\,
      O => \caracter[2]_i_161_n_0\
    );
\caracter[2]_i_162\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter[2]_i_159_n_0\,
      I1 => \caracter[2]_i_23_n_0\,
      I2 => \caracter[2]_i_219_n_0\,
      I3 => \caracter[2]_i_116_n_0\,
      O => \caracter[2]_i_162_n_0\
    );
\caracter[2]_i_163\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69699669"
    )
        port map (
      I0 => \caracter[0]_i_11_n_0\,
      I1 => \caracter[2]_i_190_n_0\,
      I2 => \caracter[3]_i_58_n_0\,
      I3 => cent(4),
      I4 => \caracter[2]_i_219_n_0\,
      O => \caracter[2]_i_163_n_0\
    );
\caracter[2]_i_164\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1EE1"
    )
        port map (
      I0 => \caracter[2]_i_23_n_0\,
      I1 => \caracter[0]_i_11_n_0\,
      I2 => \caracter[2]_i_219_n_0\,
      I3 => cent(4),
      O => \caracter[2]_i_164_n_0\
    );
\caracter[2]_i_165\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter[2]_i_23_n_0\,
      I1 => \caracter[0]_i_11_n_0\,
      O => \caracter[2]_i_165_n_0\
    );
\caracter[2]_i_167\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \caracter[2]_i_116_n_0\,
      I1 => cent(4),
      O => \caracter[2]_i_167_n_0\
    );
\caracter[2]_i_168\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter[3]_i_58_n_0\,
      I1 => \caracter[2]_i_23_n_0\,
      O => \caracter[2]_i_168_n_0\
    );
\caracter[2]_i_169\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => cent(4),
      I1 => \caracter[2]_i_190_n_0\,
      O => \caracter[2]_i_169_n_0\
    );
\caracter[2]_i_170\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(4),
      I1 => \caracter[2]_i_190_n_0\,
      O => \caracter[2]_i_170_n_0\
    );
\caracter[2]_i_171\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => cent(4),
      I1 => \caracter[2]_i_116_n_0\,
      I2 => \caracter[3]_i_58_n_0\,
      O => \caracter[2]_i_171_n_0\
    );
\caracter[2]_i_172\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7887"
    )
        port map (
      I0 => \caracter[2]_i_23_n_0\,
      I1 => \caracter[3]_i_58_n_0\,
      I2 => \caracter[2]_i_116_n_0\,
      I3 => cent(4),
      O => \caracter[2]_i_172_n_0\
    );
\caracter[2]_i_173\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => \caracter[2]_i_190_n_0\,
      I1 => cent(4),
      I2 => \caracter[3]_i_58_n_0\,
      I3 => \caracter[2]_i_23_n_0\,
      O => \caracter[2]_i_173_n_0\
    );
\caracter[2]_i_174\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"2BD4D42B"
    )
        port map (
      I0 => \caracter[2]_i_23_n_0\,
      I1 => \caracter[2]_i_116_n_0\,
      I2 => \caracter[2]_i_219_n_0\,
      I3 => \caracter[2]_i_190_n_0\,
      I4 => cent(4),
      O => \caracter[2]_i_174_n_0\
    );
\caracter[2]_i_177\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"82"
    )
        port map (
      I0 => \caracter_reg[2]_i_115_n_7\,
      I1 => \caracter_reg[2]_i_186_n_7\,
      I2 => \caracter_reg[2]_i_93_n_3\,
      O => \caracter[2]_i_177_n_0\
    );
\caracter[2]_i_178\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[2]_i_166_n_4\,
      I1 => \caracter_reg[2]_i_151_n_4\,
      O => \caracter[2]_i_178_n_0\
    );
\caracter[2]_i_179\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[2]_i_166_n_5\,
      I1 => \caracter_reg[2]_i_151_n_5\,
      O => \caracter[2]_i_179_n_0\
    );
\caracter[2]_i_180\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[2]_i_166_n_6\,
      I1 => \caracter_reg[2]_i_151_n_6\,
      O => \caracter[2]_i_180_n_0\
    );
\caracter[2]_i_181\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99966669"
    )
        port map (
      I0 => \caracter[2]_i_177_n_0\,
      I1 => \caracter_reg[2]_i_115_n_6\,
      I2 => \caracter_reg[2]_i_93_n_3\,
      I3 => \caracter_reg[2]_i_186_n_7\,
      I4 => \caracter_reg[2]_i_185_n_6\,
      O => \caracter[2]_i_181_n_0\
    );
\caracter[2]_i_182\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[2]_i_115_n_7\,
      I1 => \caracter_reg[2]_i_186_n_7\,
      I2 => \caracter_reg[2]_i_93_n_3\,
      I3 => \caracter[2]_i_178_n_0\,
      O => \caracter[2]_i_182_n_0\
    );
\caracter[2]_i_183\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => \caracter_reg[2]_i_166_n_4\,
      I1 => \caracter_reg[2]_i_151_n_4\,
      I2 => \caracter_reg[2]_i_151_n_5\,
      I3 => \caracter_reg[2]_i_166_n_5\,
      O => \caracter[2]_i_183_n_0\
    );
\caracter[2]_i_184\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \caracter_reg[2]_i_151_n_6\,
      I1 => \caracter_reg[2]_i_166_n_6\,
      I2 => \caracter_reg[2]_i_151_n_5\,
      I3 => \caracter_reg[2]_i_166_n_5\,
      O => \caracter[2]_i_184_n_0\
    );
\caracter[2]_i_187\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[2]_i_23_n_0\,
      I1 => \caracter[3]_i_58_n_0\,
      O => \caracter[2]_i_187_n_0\
    );
\caracter[2]_i_188\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter[2]_i_116_n_0\,
      I1 => \caracter[2]_i_190_n_0\,
      I2 => cent(4),
      O => \caracter[2]_i_188_n_0\
    );
\caracter[2]_i_189\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter[2]_i_219_n_0\,
      I1 => \caracter[3]_i_58_n_0\,
      I2 => \caracter[2]_i_23_n_0\,
      O => \caracter[2]_i_189_n_0\
    );
\caracter[2]_i_19\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"5559AAA6"
    )
        port map (
      I0 => \caracter_reg[3]_i_21_n_7\,
      I1 => \caracter[3]_i_43_n_0\,
      I2 => \caracter_reg[3]_i_30_n_4\,
      I3 => \caracter_reg[3]_i_30_n_5\,
      I4 => \caracter[3]_i_23_n_0\,
      O => \caracter[2]_i_19_n_0\
    );
\caracter[2]_i_191\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"D42B2BD4"
    )
        port map (
      I0 => \caracter[2]_i_116_n_0\,
      I1 => cent(4),
      I2 => \caracter[2]_i_190_n_0\,
      I3 => \caracter[3]_i_58_n_0\,
      I4 => \caracter[2]_i_23_n_0\,
      O => \caracter[2]_i_191_n_0\
    );
\caracter[2]_i_192\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E81717E817E8E817"
    )
        port map (
      I0 => \caracter[2]_i_219_n_0\,
      I1 => \caracter[3]_i_58_n_0\,
      I2 => \caracter[2]_i_23_n_0\,
      I3 => cent(4),
      I4 => \caracter[2]_i_190_n_0\,
      I5 => \caracter[2]_i_116_n_0\,
      O => \caracter[2]_i_192_n_0\
    );
\caracter[2]_i_193\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96969669"
    )
        port map (
      I0 => \caracter[2]_i_23_n_0\,
      I1 => \caracter[3]_i_58_n_0\,
      I2 => \caracter[2]_i_219_n_0\,
      I3 => \caracter[0]_i_11_n_0\,
      I4 => cent(4),
      O => \caracter[2]_i_193_n_0\
    );
\caracter[2]_i_194\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter[0]_i_11_n_0\,
      I1 => cent(4),
      I2 => \caracter[2]_i_190_n_0\,
      O => \caracter[2]_i_194_n_0\
    );
\caracter[2]_i_197\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \caracter[3]_i_61_n_0\,
      I1 => \^total_out_reg[5]\,
      I2 => \caracter_reg[3]_i_141_0\(6),
      O => \caracter[2]_i_197_n_0\
    );
\caracter[2]_i_198\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"D"
    )
        port map (
      I0 => \caracter[3]_i_58_n_0\,
      I1 => \caracter[2]_i_23_n_0\,
      O => \caracter[2]_i_198_n_0\
    );
\caracter[2]_i_199\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \caracter[3]_i_61_n_0\,
      I1 => \^total_out_reg[5]\,
      I2 => \caracter_reg[3]_i_141_0\(6),
      O => \caracter[2]_i_199_n_0\
    );
\caracter[2]_i_20\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"111FE0E0107A8580"
    )
        port map (
      I0 => \caracter_reg[3]_i_21_n_7\,
      I1 => \caracter[3]_i_22_n_0\,
      I2 => \caracter_reg[3]_i_21_n_4\,
      I3 => \caracter_reg[3]_i_21_n_5\,
      I4 => \caracter_reg[3]_i_21_n_6\,
      I5 => \caracter[3]_i_23_n_0\,
      O => \caracter[2]_i_20_n_0\
    );
\caracter[2]_i_200\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[3]_i_58_n_0\,
      I1 => \caracter[2]_i_116_n_0\,
      O => \caracter[2]_i_200_n_0\
    );
\caracter[2]_i_201\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1E"
    )
        port map (
      I0 => cent(4),
      I1 => \caracter[2]_i_116_n_0\,
      I2 => \caracter[3]_i_58_n_0\,
      O => \caracter[2]_i_201_n_0\
    );
\caracter[2]_i_202\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => \caracter[3]_i_58_n_0\,
      I1 => \caracter[2]_i_23_n_0\,
      I2 => cent(4),
      I3 => \caracter[2]_i_116_n_0\,
      O => \caracter[2]_i_202_n_0\
    );
\caracter[2]_i_203\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter[3]_i_58_n_0\,
      I1 => \caracter[2]_i_190_n_0\,
      I2 => \caracter[0]_i_11_n_0\,
      O => \caracter[2]_i_203_n_0\
    );
\caracter[2]_i_204\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \caracter[0]_i_11_n_0\,
      I1 => \caracter[2]_i_23_n_0\,
      O => \caracter[2]_i_204_n_0\
    );
\caracter[2]_i_205\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter[2]_i_159_n_0\,
      I1 => \caracter[2]_i_23_n_0\,
      I2 => \caracter[2]_i_219_n_0\,
      I3 => \caracter[2]_i_116_n_0\,
      O => \caracter[2]_i_205_n_0\
    );
\caracter[2]_i_206\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69699669"
    )
        port map (
      I0 => \caracter[0]_i_11_n_0\,
      I1 => \caracter[2]_i_190_n_0\,
      I2 => \caracter[3]_i_58_n_0\,
      I3 => cent(4),
      I4 => \caracter[2]_i_219_n_0\,
      O => \caracter[2]_i_206_n_0\
    );
\caracter[2]_i_207\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1EE1"
    )
        port map (
      I0 => \caracter[2]_i_23_n_0\,
      I1 => \caracter[0]_i_11_n_0\,
      I2 => \caracter[2]_i_219_n_0\,
      I3 => cent(4),
      O => \caracter[2]_i_207_n_0\
    );
\caracter[2]_i_208\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter[2]_i_23_n_0\,
      I1 => \caracter[0]_i_11_n_0\,
      O => \caracter[2]_i_208_n_0\
    );
\caracter[2]_i_209\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \caracter_reg[0]_i_112_n_3\,
      I1 => \caracter_reg[0]_i_113_n_0\,
      O => \caracter[2]_i_209_n_0\
    );
\caracter[2]_i_21\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"03FF03FAE8000000"
    )
        port map (
      I0 => \caracter[3]_i_23_n_0\,
      I1 => \caracter[3]_i_22_n_0\,
      I2 => \caracter_reg[3]_i_21_n_7\,
      I3 => \caracter_reg[3]_i_21_n_6\,
      I4 => \caracter_reg[3]_i_21_n_4\,
      I5 => \caracter_reg[3]_i_21_n_5\,
      O => \caracter[2]_i_21_n_0\
    );
\caracter[2]_i_210\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \caracter_reg[0]_i_112_n_3\,
      I1 => \caracter_reg[0]_i_113_n_0\,
      O => \caracter[2]_i_210_n_0\
    );
\caracter[2]_i_211\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[2]_i_166_n_7\,
      I1 => \caracter_reg[2]_i_151_n_7\,
      O => \caracter[2]_i_211_n_0\
    );
\caracter[2]_i_212\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[2]_i_195_n_4\,
      I1 => \caracter[2]_i_190_n_0\,
      O => \caracter[2]_i_212_n_0\
    );
\caracter[2]_i_213\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[2]_i_195_n_5\,
      I1 => \caracter[2]_i_219_n_0\,
      O => \caracter[2]_i_213_n_0\
    );
\caracter[2]_i_214\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \caracter_reg[2]_i_195_n_6\,
      I1 => \caracter[0]_i_11_n_0\,
      O => \caracter[2]_i_214_n_0\
    );
\caracter[2]_i_215\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \caracter_reg[2]_i_151_n_7\,
      I1 => \caracter_reg[2]_i_166_n_7\,
      I2 => \caracter_reg[2]_i_151_n_6\,
      I3 => \caracter_reg[2]_i_166_n_6\,
      O => \caracter[2]_i_215_n_0\
    );
\caracter[2]_i_216\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \caracter[2]_i_190_n_0\,
      I1 => \caracter_reg[2]_i_195_n_4\,
      I2 => \caracter_reg[2]_i_151_n_7\,
      I3 => \caracter_reg[2]_i_166_n_7\,
      O => \caracter[2]_i_216_n_0\
    );
\caracter[2]_i_217\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \caracter[2]_i_219_n_0\,
      I1 => \caracter_reg[2]_i_195_n_5\,
      I2 => \caracter[2]_i_190_n_0\,
      I3 => \caracter_reg[2]_i_195_n_4\,
      O => \caracter[2]_i_217_n_0\
    );
\caracter[2]_i_218\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4BB4"
    )
        port map (
      I0 => \caracter[0]_i_11_n_0\,
      I1 => \caracter_reg[2]_i_195_n_6\,
      I2 => \caracter[2]_i_219_n_0\,
      I3 => \caracter_reg[2]_i_195_n_5\,
      O => \caracter[2]_i_218_n_0\
    );
\caracter[2]_i_22\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[2]_i_31_n_5\,
      I1 => cent(4),
      O => \caracter[2]_i_22_n_0\
    );
\caracter[2]_i_220\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[0]_i_11_n_0\,
      O => \caracter[2]_i_220_n_0\
    );
\caracter[2]_i_221\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter[2]_i_219_n_0\,
      I1 => \caracter[2]_i_23_n_0\,
      O => \caracter[2]_i_221_n_0\
    );
\caracter[2]_i_222\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[0]_i_11_n_0\,
      I1 => \caracter[2]_i_190_n_0\,
      O => \caracter[2]_i_222_n_0\
    );
\caracter[2]_i_223\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => p_1_in(1),
      I1 => \^total_out_reg[5]\,
      I2 => \caracter_reg[3]_i_141_0\(1),
      O => \caracter[2]_i_223_n_0\
    );
\caracter[2]_i_224\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[0]_i_11_n_0\,
      O => \caracter[2]_i_224_n_0\
    );
\caracter[2]_i_226\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[0]_i_11_n_0\,
      O => \caracter[2]_i_226_n_0\
    );
\caracter[2]_i_227\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter[2]_i_219_n_0\,
      I1 => \caracter[2]_i_23_n_0\,
      O => \caracter[2]_i_227_n_0\
    );
\caracter[2]_i_228\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[0]_i_11_n_0\,
      I1 => \caracter[2]_i_190_n_0\,
      O => \caracter[2]_i_228_n_0\
    );
\caracter[2]_i_229\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => p_1_in(1),
      I1 => \^total_out_reg[5]\,
      I2 => \caracter_reg[3]_i_141_0\(1),
      O => \caracter[2]_i_229_n_0\
    );
\caracter[2]_i_230\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[0]_i_11_n_0\,
      O => \caracter[2]_i_230_n_0\
    );
\caracter[2]_i_231\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[2]_i_23_n_0\,
      I1 => \caracter[3]_i_58_n_0\,
      O => \caracter[2]_i_231_n_0\
    );
\caracter[2]_i_232\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter[2]_i_219_n_0\,
      I1 => \caracter[3]_i_58_n_0\,
      I2 => \caracter[2]_i_23_n_0\,
      O => \caracter[2]_i_232_n_0\
    );
\caracter[2]_i_234\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"D42B2BD4"
    )
        port map (
      I0 => \caracter[2]_i_116_n_0\,
      I1 => cent(4),
      I2 => \caracter[2]_i_190_n_0\,
      I3 => \caracter[3]_i_58_n_0\,
      I4 => \caracter[2]_i_23_n_0\,
      O => \caracter[2]_i_234_n_0\
    );
\caracter[2]_i_235\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E81717E817E8E817"
    )
        port map (
      I0 => \caracter[2]_i_219_n_0\,
      I1 => \caracter[3]_i_58_n_0\,
      I2 => \caracter[2]_i_23_n_0\,
      I3 => cent(4),
      I4 => \caracter[2]_i_190_n_0\,
      I5 => \caracter[2]_i_116_n_0\,
      O => \caracter[2]_i_235_n_0\
    );
\caracter[2]_i_236\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96969669"
    )
        port map (
      I0 => \caracter[2]_i_23_n_0\,
      I1 => \caracter[3]_i_58_n_0\,
      I2 => \caracter[2]_i_219_n_0\,
      I3 => \caracter[0]_i_11_n_0\,
      I4 => cent(4),
      O => \caracter[2]_i_236_n_0\
    );
\caracter[2]_i_237\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter[0]_i_11_n_0\,
      I1 => cent(4),
      I2 => \caracter[2]_i_190_n_0\,
      O => \caracter[2]_i_237_n_0\
    );
\caracter[2]_i_26\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[0]_i_11_n_0\,
      O => \caracter[2]_i_26_n_0\
    );
\caracter[2]_i_27\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter[2]_i_23_n_0\,
      I1 => \caracter_reg[2]_i_31_n_6\,
      O => \caracter[2]_i_27_n_0\
    );
\caracter[2]_i_28\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter[2]_i_190_n_0\,
      I1 => \caracter_reg[2]_i_31_n_7\,
      O => \caracter[2]_i_28_n_0\
    );
\caracter[2]_i_29\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter[2]_i_219_n_0\,
      I1 => \caracter_reg[2]_i_36_n_4\,
      O => \caracter[2]_i_29_n_0\
    );
\caracter[2]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0A0CFC0AFAFCFC0"
    )
        port map (
      I0 => \caracter[2]_i_8_n_0\,
      I1 => \caracter[2]_i_9_n_0\,
      I2 => \caracter_reg[2]\,
      I3 => \caracter_reg[2]_0\,
      I4 => \caracter_reg[2]_1\,
      I5 => \caracter_reg[2]_2\,
      O => \FSM_sequential_current_state_reg[1]_3\
    );
\caracter[2]_i_30\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => p_1_in(0),
      I1 => \^total_out_reg[5]\,
      I2 => \caracter_reg[3]_i_141_0\(0),
      O => \caracter[2]_i_30_n_0\
    );
\caracter[2]_i_32\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0E1FFFFF0E10000"
    )
        port map (
      I0 => \caracter_reg[0]_i_13_n_4\,
      I1 => \caracter_reg[0]_i_13_n_5\,
      I2 => \caracter_reg[2]_i_40_n_7\,
      I3 => \caracter[2]_i_41_n_0\,
      I4 => \^total_out_reg[5]\,
      I5 => \caracter_reg[3]_i_141_0\(4),
      O => cent(4)
    );
\caracter[2]_i_33\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E1FFE100"
    )
        port map (
      I0 => \caracter_reg[0]_i_13_n_5\,
      I1 => \caracter[2]_i_41_n_0\,
      I2 => \caracter_reg[0]_i_13_n_4\,
      I3 => \^total_out_reg[5]\,
      I4 => \caracter_reg[3]_i_141_0\(3),
      O => \caracter[2]_i_23_n_0\
    );
\caracter[2]_i_34\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"444444F444F444F4"
    )
        port map (
      I0 => aD2M4dsP(2),
      I1 => \^total_out_reg[5]\,
      I2 => \caracter_reg[3]_i_141_0\(2),
      I3 => \caracter_reg[3]_i_141_0\(7),
      I4 => \caracter_reg[3]_i_141_0\(6),
      I5 => \caracter_reg[3]_i_141_0\(5),
      O => \caracter[2]_i_190_n_0\
    );
\caracter[2]_i_35\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => p_1_in(1),
      I1 => \^total_out_reg[5]\,
      I2 => \caracter_reg[3]_i_141_0\(1),
      O => \caracter[2]_i_219_n_0\
    );
\caracter[2]_i_38\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter_reg[2]_i_37_n_7\,
      I1 => \caracter_reg[2]_i_37_n_5\,
      O => \caracter[2]_i_38_n_0\
    );
\caracter[2]_i_39\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter_reg[2]_i_37_n_6\,
      I1 => \caracter_reg[2]_i_36_n_4\,
      O => \caracter[2]_i_39_n_0\
    );
\caracter[2]_i_41\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000005557FFFF"
    )
        port map (
      I0 => \caracter_reg[2]_i_40_n_6\,
      I1 => \caracter_reg[2]_i_40_n_7\,
      I2 => \caracter_reg[0]_i_13_n_4\,
      I3 => \caracter_reg[0]_i_13_n_5\,
      I4 => \caracter_reg[2]_i_40_n_5\,
      I5 => \caracter_reg[2]_i_40_n_4\,
      O => \caracter[2]_i_41_n_0\
    );
\caracter[2]_i_42\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E1E1E1E5A5A5A5A5"
    )
        port map (
      I0 => \caracter_reg[2]_i_40_n_4\,
      I1 => \caracter_reg[2]_i_40_n_5\,
      I2 => \caracter_reg[0]_i_13_n_5\,
      I3 => \caracter_reg[0]_i_13_n_4\,
      I4 => \caracter_reg[2]_i_40_n_7\,
      I5 => \caracter_reg[2]_i_40_n_6\,
      O => aD2M4dsP(2)
    );
\caracter[2]_i_44\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"32B380FE80FE32B3"
    )
        port map (
      I0 => \caracter_reg[2]_i_70_n_4\,
      I1 => \caracter_reg[2]_i_71_n_3\,
      I2 => \caracter_reg[2]_i_72_n_4\,
      I3 => \caracter_reg[2]_i_73_n_1\,
      I4 => \caracter_reg[2]_i_74_n_7\,
      I5 => \caracter_reg[2]_i_75_n_7\,
      O => \caracter[2]_i_44_n_0\
    );
\caracter[2]_i_45\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"32B380FE80FE32B3"
    )
        port map (
      I0 => \caracter_reg[2]_i_70_n_5\,
      I1 => \caracter_reg[2]_i_71_n_3\,
      I2 => \caracter_reg[2]_i_72_n_5\,
      I3 => \caracter_reg[2]_i_73_n_1\,
      I4 => \caracter_reg[2]_i_70_n_4\,
      I5 => \caracter_reg[2]_i_72_n_4\,
      O => \caracter[2]_i_45_n_0\
    );
\caracter[2]_i_46\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"32B380FE80FE32B3"
    )
        port map (
      I0 => \caracter_reg[2]_i_70_n_6\,
      I1 => \caracter_reg[2]_i_71_n_3\,
      I2 => \caracter_reg[2]_i_72_n_6\,
      I3 => \caracter_reg[2]_i_73_n_1\,
      I4 => \caracter_reg[2]_i_70_n_5\,
      I5 => \caracter_reg[2]_i_72_n_5\,
      O => \caracter[2]_i_46_n_0\
    );
\caracter[2]_i_47\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"69FF217B217B0069"
    )
        port map (
      I0 => \caracter_reg[2]_i_70_n_6\,
      I1 => \caracter_reg[2]_i_71_n_3\,
      I2 => \caracter_reg[2]_i_72_n_6\,
      I3 => \caracter_reg[2]_i_73_n_1\,
      I4 => \caracter_reg[2]_i_72_n_7\,
      I5 => \caracter_reg[2]_i_70_n_7\,
      O => \caracter[2]_i_47_n_0\
    );
\caracter[2]_i_48\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9A5965A665A69A59"
    )
        port map (
      I0 => \caracter[2]_i_44_n_0\,
      I1 => \caracter_reg[2]_i_74_n_7\,
      I2 => \caracter_reg[2]_i_71_n_3\,
      I3 => \caracter_reg[2]_i_75_n_7\,
      I4 => \caracter_reg[2]_i_73_n_1\,
      I5 => \caracter[2]_i_76_n_0\,
      O => \caracter[2]_i_48_n_0\
    );
\caracter[2]_i_49\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9A5965A665A69A59"
    )
        port map (
      I0 => \caracter[2]_i_45_n_0\,
      I1 => \caracter_reg[2]_i_70_n_4\,
      I2 => \caracter_reg[2]_i_71_n_3\,
      I3 => \caracter_reg[2]_i_72_n_4\,
      I4 => \caracter_reg[2]_i_73_n_1\,
      I5 => \caracter[2]_i_77_n_0\,
      O => \caracter[2]_i_49_n_0\
    );
\caracter[2]_i_50\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9A5965A665A69A59"
    )
        port map (
      I0 => \caracter[2]_i_46_n_0\,
      I1 => \caracter_reg[2]_i_70_n_5\,
      I2 => \caracter_reg[2]_i_71_n_3\,
      I3 => \caracter_reg[2]_i_72_n_5\,
      I4 => \caracter_reg[2]_i_73_n_1\,
      I5 => \caracter[2]_i_78_n_0\,
      O => \caracter[2]_i_50_n_0\
    );
\caracter[2]_i_51\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9A5965A665A69A59"
    )
        port map (
      I0 => \caracter[2]_i_47_n_0\,
      I1 => \caracter_reg[2]_i_70_n_6\,
      I2 => \caracter_reg[2]_i_71_n_3\,
      I3 => \caracter_reg[2]_i_72_n_6\,
      I4 => \caracter_reg[2]_i_73_n_1\,
      I5 => \caracter[2]_i_79_n_0\,
      O => \caracter[2]_i_51_n_0\
    );
\caracter[2]_i_52\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"80FE32B332B380FE"
    )
        port map (
      I0 => \caracter_reg[2]_i_74_n_6\,
      I1 => \caracter_reg[2]_i_71_n_3\,
      I2 => \caracter_reg[2]_i_75_n_6\,
      I3 => \caracter_reg[2]_i_73_n_1\,
      I4 => \caracter_reg[2]_i_74_n_1\,
      I5 => \caracter_reg[2]_i_75_n_5\,
      O => \caracter[2]_i_52_n_0\
    );
\caracter[2]_i_53\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"32B380FE80FE32B3"
    )
        port map (
      I0 => \caracter_reg[2]_i_74_n_7\,
      I1 => \caracter_reg[2]_i_71_n_3\,
      I2 => \caracter_reg[2]_i_75_n_7\,
      I3 => \caracter_reg[2]_i_73_n_1\,
      I4 => \caracter_reg[2]_i_74_n_6\,
      I5 => \caracter_reg[2]_i_75_n_6\,
      O => \caracter[2]_i_53_n_0\
    );
\caracter[2]_i_54\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAA995A9955555"
    )
        port map (
      I0 => \caracter_reg[2]_i_80_n_7\,
      I1 => \caracter_reg[2]_i_75_n_5\,
      I2 => \caracter_reg[2]_i_71_n_3\,
      I3 => \caracter_reg[2]_i_74_n_1\,
      I4 => \caracter_reg[2]_i_75_n_4\,
      I5 => \caracter_reg[2]_i_73_n_1\,
      O => \caracter[2]_i_54_n_0\
    );
\caracter[2]_i_55\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"566AA995A995566A"
    )
        port map (
      I0 => \caracter[2]_i_52_n_0\,
      I1 => \caracter_reg[2]_i_74_n_1\,
      I2 => \caracter_reg[2]_i_71_n_3\,
      I3 => \caracter_reg[2]_i_75_n_5\,
      I4 => \caracter_reg[2]_i_73_n_1\,
      I5 => \caracter_reg[2]_i_75_n_4\,
      O => \caracter[2]_i_55_n_0\
    );
\caracter[2]_i_56\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9A5965A665A69A59"
    )
        port map (
      I0 => \caracter[2]_i_53_n_0\,
      I1 => \caracter_reg[2]_i_74_n_6\,
      I2 => \caracter_reg[2]_i_71_n_3\,
      I3 => \caracter_reg[2]_i_75_n_6\,
      I4 => \caracter_reg[2]_i_73_n_1\,
      I5 => \caracter[2]_i_81_n_0\,
      O => \caracter[2]_i_56_n_0\
    );
\caracter[2]_i_57\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(7),
      I1 => \caracter_reg[2]_i_82_n_5\,
      O => \caracter[2]_i_57_n_0\
    );
\caracter[2]_i_58\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(6),
      I1 => \caracter_reg[2]_i_82_n_6\,
      O => \caracter[2]_i_58_n_0\
    );
\caracter[2]_i_59\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(5),
      I1 => \caracter_reg[2]_i_82_n_7\,
      O => \caracter[2]_i_59_n_0\
    );
\caracter[2]_i_60\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(4),
      I1 => \caracter_reg[0]_i_24_n_5\,
      O => \caracter[2]_i_60_n_0\
    );
\caracter[2]_i_62\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BB2B2B22"
    )
        port map (
      I0 => \caracter[2]_i_92_n_0\,
      I1 => \caracter_reg[2]_i_73_n_1\,
      I2 => \caracter_reg[2]_i_93_n_3\,
      I3 => \caracter_reg[2]_i_94_n_4\,
      I4 => \caracter_reg[2]_i_95_n_4\,
      O => \caracter[2]_i_62_n_0\
    );
\caracter[2]_i_63\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7DD7355335531441"
    )
        port map (
      I0 => \caracter_reg[2]_i_73_n_1\,
      I1 => \caracter_reg[2]_i_93_n_3\,
      I2 => \caracter_reg[2]_i_94_n_4\,
      I3 => \caracter_reg[2]_i_95_n_4\,
      I4 => \caracter_reg[2]_i_94_n_5\,
      I5 => \caracter_reg[2]_i_95_n_5\,
      O => \caracter[2]_i_63_n_0\
    );
\caracter[2]_i_64\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7DD7355335531441"
    )
        port map (
      I0 => \caracter_reg[2]_i_73_n_1\,
      I1 => \caracter_reg[2]_i_93_n_3\,
      I2 => \caracter_reg[2]_i_94_n_5\,
      I3 => \caracter_reg[2]_i_95_n_5\,
      I4 => \caracter_reg[2]_i_94_n_6\,
      I5 => \caracter_reg[2]_i_95_n_6\,
      O => \caracter[2]_i_64_n_0\
    );
\caracter[2]_i_65\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7DD7355335531441"
    )
        port map (
      I0 => \caracter_reg[2]_i_73_n_1\,
      I1 => \caracter_reg[2]_i_93_n_3\,
      I2 => \caracter_reg[2]_i_94_n_6\,
      I3 => \caracter_reg[2]_i_95_n_6\,
      I4 => \caracter_reg[2]_i_94_n_7\,
      I5 => \caracter[2]_i_96_n_0\,
      O => \caracter[2]_i_65_n_0\
    );
\caracter[2]_i_66\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A665599A599AA665"
    )
        port map (
      I0 => \caracter[2]_i_62_n_0\,
      I1 => \caracter_reg[2]_i_71_n_3\,
      I2 => \caracter_reg[2]_i_72_n_7\,
      I3 => \caracter_reg[2]_i_70_n_7\,
      I4 => \caracter_reg[2]_i_73_n_1\,
      I5 => \caracter[2]_i_97_n_0\,
      O => \caracter[2]_i_66_n_0\
    );
\caracter[2]_i_67\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A665599A599AA665"
    )
        port map (
      I0 => \caracter[2]_i_63_n_0\,
      I1 => \caracter_reg[2]_i_93_n_3\,
      I2 => \caracter_reg[2]_i_94_n_4\,
      I3 => \caracter_reg[2]_i_95_n_4\,
      I4 => \caracter_reg[2]_i_73_n_1\,
      I5 => \caracter[2]_i_92_n_0\,
      O => \caracter[2]_i_67_n_0\
    );
\caracter[2]_i_68\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"599AA665A665599A"
    )
        port map (
      I0 => \caracter[2]_i_64_n_0\,
      I1 => \caracter_reg[2]_i_93_n_3\,
      I2 => \caracter_reg[2]_i_94_n_5\,
      I3 => \caracter_reg[2]_i_95_n_5\,
      I4 => \caracter_reg[2]_i_73_n_1\,
      I5 => \caracter[2]_i_98_n_0\,
      O => \caracter[2]_i_68_n_0\
    );
\caracter[2]_i_69\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"599AA665A665599A"
    )
        port map (
      I0 => \caracter[2]_i_65_n_0\,
      I1 => \caracter_reg[2]_i_93_n_3\,
      I2 => \caracter_reg[2]_i_94_n_6\,
      I3 => \caracter_reg[2]_i_95_n_6\,
      I4 => \caracter_reg[2]_i_73_n_1\,
      I5 => \caracter[2]_i_99_n_0\,
      O => \caracter[2]_i_69_n_0\
    );
\caracter[2]_i_76\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[2]_i_75_n_6\,
      I1 => \caracter_reg[2]_i_71_n_3\,
      I2 => \caracter_reg[2]_i_74_n_6\,
      O => \caracter[2]_i_76_n_0\
    );
\caracter[2]_i_77\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[2]_i_75_n_7\,
      I1 => \caracter_reg[2]_i_71_n_3\,
      I2 => \caracter_reg[2]_i_74_n_7\,
      O => \caracter[2]_i_77_n_0\
    );
\caracter[2]_i_78\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[2]_i_72_n_4\,
      I1 => \caracter_reg[2]_i_71_n_3\,
      I2 => \caracter_reg[2]_i_70_n_4\,
      O => \caracter[2]_i_78_n_0\
    );
\caracter[2]_i_79\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[2]_i_72_n_5\,
      I1 => \caracter_reg[2]_i_71_n_3\,
      I2 => \caracter_reg[2]_i_70_n_5\,
      O => \caracter[2]_i_79_n_0\
    );
\caracter[2]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"82A2"
    )
        port map (
      I0 => \caracter[2]_i_13_n_0\,
      I1 => \caracter[6]_i_38_0\,
      I2 => \caracter_reg[1]_i_4\,
      I3 => \disp_refresco[1]_4\(0),
      O => \caracter[2]_i_8_n_0\
    );
\caracter[2]_i_81\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[2]_i_75_n_5\,
      I1 => \caracter_reg[2]_i_71_n_3\,
      I2 => \caracter_reg[2]_i_74_n_1\,
      O => \caracter[2]_i_81_n_0\
    );
\caracter[2]_i_84\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"54D580FE80FE54D5"
    )
        port map (
      I0 => \caracter_reg[2]_i_93_n_3\,
      I1 => \caracter[2]_i_190_n_0\,
      I2 => \caracter_reg[2]_i_146_n_4\,
      I3 => \caracter_reg[2]_i_73_n_1\,
      I4 => \caracter_reg[2]_i_94_n_7\,
      I5 => \caracter[2]_i_96_n_0\,
      O => \caracter[2]_i_84_n_0\
    );
\caracter[2]_i_85\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0E8F80FE80FE0E8F"
    )
        port map (
      I0 => \caracter_reg[2]_i_146_n_5\,
      I1 => \caracter[2]_i_219_n_0\,
      I2 => \caracter_reg[2]_i_93_n_3\,
      I3 => \caracter_reg[2]_i_73_n_1\,
      I4 => \caracter_reg[2]_i_146_n_4\,
      I5 => \caracter[2]_i_190_n_0\,
      O => \caracter[2]_i_85_n_0\
    );
\caracter[2]_i_86\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"517520FB20FB5175"
    )
        port map (
      I0 => \caracter_reg[2]_i_93_n_3\,
      I1 => \caracter[0]_i_11_n_0\,
      I2 => \caracter_reg[2]_i_146_n_6\,
      I3 => \caracter_reg[2]_i_73_n_1\,
      I4 => \caracter[2]_i_219_n_0\,
      I5 => \caracter_reg[2]_i_146_n_5\,
      O => \caracter[2]_i_86_n_0\
    );
\caracter[2]_i_87\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"82BE8228"
    )
        port map (
      I0 => \caracter_reg[2]_i_73_n_6\,
      I1 => \caracter_reg[2]_i_146_n_6\,
      I2 => \caracter[0]_i_11_n_0\,
      I3 => \caracter_reg[2]_i_93_n_3\,
      I4 => \caracter_reg[2]_i_146_n_7\,
      O => \caracter[2]_i_87_n_0\
    );
\caracter[2]_i_88\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \caracter[2]_i_84_n_0\,
      I1 => \caracter[2]_i_147_n_0\,
      I2 => \caracter_reg[2]_i_73_n_1\,
      I3 => \caracter_reg[2]_i_93_n_3\,
      I4 => \caracter_reg[2]_i_94_n_6\,
      I5 => \caracter_reg[2]_i_95_n_6\,
      O => \caracter[2]_i_88_n_0\
    );
\caracter[2]_i_89\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"599AA665A665599A"
    )
        port map (
      I0 => \caracter[2]_i_85_n_0\,
      I1 => \caracter_reg[2]_i_93_n_3\,
      I2 => \caracter[2]_i_190_n_0\,
      I3 => \caracter_reg[2]_i_146_n_4\,
      I4 => \caracter_reg[2]_i_73_n_1\,
      I5 => \caracter[2]_i_148_n_0\,
      O => \caracter[2]_i_89_n_0\
    );
\caracter[2]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2EFD0000FFFFFFFF"
    )
        port map (
      I0 => \caracter_reg[2]_i_14_n_7\,
      I1 => \caracter_reg[2]_i_15_n_4\,
      I2 => \caracter_reg[2]_i_15_n_6\,
      I3 => \caracter_reg[2]_i_15_n_5\,
      I4 => \caracter[6]_i_38_0\,
      I5 => \caracter_reg[1]_i_4\,
      O => \caracter[2]_i_9_n_0\
    );
\caracter[2]_i_90\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6996966996696996"
    )
        port map (
      I0 => \caracter[2]_i_86_n_0\,
      I1 => \caracter_reg[2]_i_146_n_4\,
      I2 => \caracter[2]_i_190_n_0\,
      I3 => \caracter_reg[2]_i_93_n_3\,
      I4 => \caracter_reg[2]_i_73_n_1\,
      I5 => \caracter[2]_i_149_n_0\,
      O => \caracter[2]_i_90_n_0\
    );
\caracter[2]_i_91\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"95A96A566A5695A9"
    )
        port map (
      I0 => \caracter[2]_i_87_n_0\,
      I1 => \caracter_reg[2]_i_93_n_3\,
      I2 => \caracter[0]_i_11_n_0\,
      I3 => \caracter_reg[2]_i_146_n_6\,
      I4 => \caracter_reg[2]_i_73_n_1\,
      I5 => \caracter[2]_i_150_n_0\,
      O => \caracter[2]_i_91_n_0\
    );
\caracter[2]_i_92\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[2]_i_70_n_7\,
      I1 => \caracter_reg[2]_i_71_n_3\,
      I2 => \caracter_reg[2]_i_72_n_7\,
      O => \caracter[2]_i_92_n_0\
    );
\caracter[2]_i_96\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter[2]_i_23_n_0\,
      I1 => \caracter[0]_i_11_n_0\,
      O => \caracter[2]_i_96_n_0\
    );
\caracter[2]_i_97\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[2]_i_72_n_6\,
      I1 => \caracter_reg[2]_i_71_n_3\,
      I2 => \caracter_reg[2]_i_70_n_6\,
      O => \caracter[2]_i_97_n_0\
    );
\caracter[2]_i_98\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[2]_i_95_n_4\,
      I1 => \caracter_reg[2]_i_94_n_4\,
      I2 => \caracter_reg[2]_i_93_n_3\,
      O => \caracter[2]_i_98_n_0\
    );
\caracter[2]_i_99\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[2]_i_95_n_5\,
      I1 => \caracter_reg[2]_i_94_n_5\,
      I2 => \caracter_reg[2]_i_93_n_3\,
      O => \caracter[2]_i_99_n_0\
    );
\caracter[3]_i_100\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => cent(4),
      I1 => \caracter[2]_i_116_n_0\,
      O => \caracter[3]_i_100_n_0\
    );
\caracter[3]_i_102\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"87"
    )
        port map (
      I0 => \caracter[2]_i_116_n_0\,
      I1 => cent(4),
      I2 => \caracter[3]_i_58_n_0\,
      O => \caracter[3]_i_102_n_0\
    );
\caracter[3]_i_104\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"D"
    )
        port map (
      I0 => \caracter[2]_i_116_n_0\,
      I1 => cent(4),
      O => \caracter[3]_i_104_n_0\
    );
\caracter[3]_i_105\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \caracter[3]_i_58_n_0\,
      I1 => \caracter[2]_i_23_n_0\,
      O => \caracter[3]_i_105_n_0\
    );
\caracter[3]_i_106\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \caracter[3]_i_61_n_0\,
      I1 => \^total_out_reg[5]\,
      I2 => \caracter_reg[3]_i_141_0\(6),
      O => \caracter[3]_i_106_n_0\
    );
\caracter[3]_i_107\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B4"
    )
        port map (
      I0 => cent(4),
      I1 => \caracter[2]_i_116_n_0\,
      I2 => \caracter[3]_i_58_n_0\,
      O => \caracter[3]_i_107_n_0\
    );
\caracter[3]_i_108\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"E11E"
    )
        port map (
      I0 => \caracter[3]_i_58_n_0\,
      I1 => \caracter[2]_i_23_n_0\,
      I2 => \caracter[2]_i_116_n_0\,
      I3 => cent(4),
      O => \caracter[3]_i_108_n_0\
    );
\caracter[3]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFEFFFEEEF"
    )
        port map (
      I0 => \caracter[3]_i_16_n_0\,
      I1 => \caracter[3]_i_17_n_0\,
      I2 => \^caracter_reg[2]_i_15_0\,
      I3 => \caracter[3]_i_18_n_0\,
      I4 => \caracter[3]_i_19_n_0\,
      I5 => \caracter[3]_i_20_n_0\,
      O => \caracter[3]_i_11_n_0\
    );
\caracter[3]_i_111\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[3]_i_58_n_0\,
      O => \caracter[3]_i_111_n_0\
    );
\caracter[3]_i_112\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \caracter[3]_i_61_n_0\,
      I1 => \^total_out_reg[5]\,
      I2 => \caracter_reg[3]_i_141_0\(6),
      O => \caracter[3]_i_112_n_0\
    );
\caracter[3]_i_113\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \caracter[3]_i_62_n_0\,
      I1 => \^total_out_reg[5]\,
      I2 => \caracter_reg[3]_i_141_0\(5),
      O => \caracter[3]_i_113_n_0\
    );
\caracter[3]_i_115\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E8E800E80000E8"
    )
        port map (
      I0 => \caracter_reg[3]_i_86_n_5\,
      I1 => \caracter[2]_i_190_n_0\,
      I2 => \caracter_reg[3]_i_109_n_6\,
      I3 => \caracter_reg[3]_i_86_n_0\,
      I4 => \caracter_reg[3]_i_123_n_7\,
      I5 => \caracter_reg[3]_i_109_n_5\,
      O => \caracter[3]_i_115_n_0\
    );
\caracter[3]_i_116\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9696960096000000"
    )
        port map (
      I0 => \caracter_reg[3]_i_109_n_6\,
      I1 => \caracter[2]_i_190_n_0\,
      I2 => \caracter_reg[3]_i_86_n_5\,
      I3 => \caracter_reg[3]_i_86_n_6\,
      I4 => \caracter[2]_i_219_n_0\,
      I5 => \caracter_reg[3]_i_109_n_7\,
      O => \caracter[3]_i_116_n_0\
    );
\caracter[3]_i_117\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B20000B200B2B200"
    )
        port map (
      I0 => \caracter_reg[3]_i_86_n_7\,
      I1 => \caracter[0]_i_11_n_0\,
      I2 => \caracter_reg[3]_i_141_n_4\,
      I3 => \caracter_reg[3]_i_109_n_7\,
      I4 => \caracter[2]_i_219_n_0\,
      I5 => \caracter_reg[3]_i_86_n_6\,
      O => \caracter[3]_i_117_n_0\
    );
\caracter[3]_i_118\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08808008"
    )
        port map (
      I0 => \caracter_reg[3]_i_103_n_4\,
      I1 => \caracter_reg[3]_i_141_n_5\,
      I2 => \caracter_reg[3]_i_141_n_4\,
      I3 => \caracter[0]_i_11_n_0\,
      I4 => \caracter_reg[3]_i_86_n_7\,
      O => \caracter[3]_i_118_n_0\
    );
\caracter[3]_i_119\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6996996699669669"
    )
        port map (
      I0 => \caracter[3]_i_115_n_0\,
      I1 => \caracter_reg[3]_i_123_n_6\,
      I2 => \caracter_reg[3]_i_86_n_0\,
      I3 => \caracter_reg[3]_i_109_n_4\,
      I4 => \caracter_reg[3]_i_123_n_7\,
      I5 => \caracter_reg[3]_i_109_n_5\,
      O => \caracter[3]_i_119_n_0\
    );
\caracter[3]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1180FA001100E000"
    )
        port map (
      I0 => \caracter_reg[3]_i_21_n_7\,
      I1 => \caracter[3]_i_22_n_0\,
      I2 => \caracter_reg[3]_i_21_n_4\,
      I3 => \caracter_reg[3]_i_21_n_5\,
      I4 => \caracter_reg[3]_i_21_n_6\,
      I5 => \caracter[3]_i_23_n_0\,
      O => \caracter[3]_i_12_n_0\
    );
\caracter[3]_i_120\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7EE88117"
    )
        port map (
      I0 => \caracter[3]_i_159_n_0\,
      I1 => \caracter_reg[3]_i_86_n_5\,
      I2 => \caracter[2]_i_190_n_0\,
      I3 => \caracter_reg[3]_i_109_n_6\,
      I4 => \caracter[3]_i_160_n_0\,
      O => \caracter[3]_i_120_n_0\
    );
\caracter[3]_i_121\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A995566A"
    )
        port map (
      I0 => \caracter[3]_i_117_n_0\,
      I1 => \caracter_reg[3]_i_109_n_7\,
      I2 => \caracter[2]_i_219_n_0\,
      I3 => \caracter_reg[3]_i_86_n_6\,
      I4 => \caracter[3]_i_161_n_0\,
      O => \caracter[3]_i_121_n_0\
    );
\caracter[3]_i_122\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F87F80F807807F07"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_n_5\,
      I1 => \caracter_reg[3]_i_103_n_4\,
      I2 => \caracter_reg[3]_i_86_n_7\,
      I3 => \caracter[0]_i_11_n_0\,
      I4 => \caracter_reg[3]_i_141_n_4\,
      I5 => \caracter[3]_i_162_n_0\,
      O => \caracter[3]_i_122_n_0\
    );
\caracter[3]_i_124\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \caracter[2]_i_23_n_0\,
      I1 => \caracter[3]_i_58_n_0\,
      O => \caracter[3]_i_124_n_0\
    );
\caracter[3]_i_125\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter[2]_i_190_n_0\,
      I1 => cent(4),
      O => \caracter[3]_i_125_n_0\
    );
\caracter[3]_i_126\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter[2]_i_23_n_0\,
      I1 => \caracter[2]_i_219_n_0\,
      O => \caracter[3]_i_126_n_0\
    );
\caracter[3]_i_127\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[2]_i_23_n_0\,
      I1 => \caracter[2]_i_219_n_0\,
      O => \caracter[3]_i_127_n_0\
    );
\caracter[3]_i_128\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4BB4"
    )
        port map (
      I0 => \caracter[3]_i_58_n_0\,
      I1 => \caracter[2]_i_23_n_0\,
      I2 => cent(4),
      I3 => \caracter[2]_i_116_n_0\,
      O => \caracter[3]_i_128_n_0\
    );
\caracter[3]_i_129\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7887"
    )
        port map (
      I0 => cent(4),
      I1 => \caracter[2]_i_190_n_0\,
      I2 => \caracter[2]_i_23_n_0\,
      I3 => \caracter[3]_i_58_n_0\,
      O => \caracter[3]_i_129_n_0\
    );
\caracter[3]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B42B2BD3AD4A4AB6"
    )
        port map (
      I0 => \caracter[3]_i_24_n_0\,
      I1 => \caracter[3]_i_16_n_0\,
      I2 => \caracter[3]_i_20_n_0\,
      I3 => \caracter[3]_i_23_n_0\,
      I4 => \caracter[3]_i_17_n_0\,
      I5 => \caracter[3]_i_25_n_0\,
      O => \caracter[3]_i_13_n_0\
    );
\caracter[3]_i_130\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \caracter[2]_i_219_n_0\,
      I1 => \caracter[2]_i_23_n_0\,
      I2 => cent(4),
      I3 => \caracter[2]_i_190_n_0\,
      O => \caracter[3]_i_130_n_0\
    );
\caracter[3]_i_131\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6696"
    )
        port map (
      I0 => \caracter[2]_i_219_n_0\,
      I1 => \caracter[2]_i_23_n_0\,
      I2 => \caracter[2]_i_190_n_0\,
      I3 => \caracter[0]_i_11_n_0\,
      O => \caracter[3]_i_131_n_0\
    );
\caracter[3]_i_133\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => \caracter[2]_i_116_n_0\,
      I1 => cent(4),
      I2 => \caracter[2]_i_190_n_0\,
      O => \caracter[3]_i_133_n_0\
    );
\caracter[3]_i_134\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => \caracter[2]_i_23_n_0\,
      I1 => \caracter[3]_i_58_n_0\,
      I2 => \caracter[2]_i_219_n_0\,
      O => \caracter[3]_i_134_n_0\
    );
\caracter[3]_i_135\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => cent(4),
      I1 => \caracter[2]_i_190_n_0\,
      I2 => \caracter[0]_i_11_n_0\,
      O => \caracter[3]_i_135_n_0\
    );
\caracter[3]_i_136\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => cent(4),
      I1 => \caracter[2]_i_190_n_0\,
      I2 => \caracter[0]_i_11_n_0\,
      O => \caracter[3]_i_136_n_0\
    );
\caracter[3]_i_137\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"D42B2BD4"
    )
        port map (
      I0 => \caracter[2]_i_190_n_0\,
      I1 => cent(4),
      I2 => \caracter[2]_i_116_n_0\,
      I3 => \caracter[3]_i_58_n_0\,
      I4 => \caracter[2]_i_23_n_0\,
      O => \caracter[3]_i_137_n_0\
    );
\caracter[3]_i_138\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"718E8E718E71718E"
    )
        port map (
      I0 => \caracter[2]_i_219_n_0\,
      I1 => \caracter[3]_i_58_n_0\,
      I2 => \caracter[2]_i_23_n_0\,
      I3 => cent(4),
      I4 => \caracter[2]_i_190_n_0\,
      I5 => \caracter[2]_i_116_n_0\,
      O => \caracter[3]_i_138_n_0\
    );
\caracter[3]_i_139\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter[3]_i_135_n_0\,
      I1 => \caracter[2]_i_23_n_0\,
      I2 => \caracter[2]_i_219_n_0\,
      I3 => \caracter[3]_i_58_n_0\,
      O => \caracter[3]_i_139_n_0\
    );
\caracter[3]_i_14\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"44AAAAD4D4AAAADD"
    )
        port map (
      I0 => \caracter[3]_i_24_n_0\,
      I1 => \caracter[3]_i_26_n_0\,
      I2 => \caracter[3]_i_27_n_0\,
      I3 => \caracter[3]_i_25_n_0\,
      I4 => \caracter[3]_i_28_n_0\,
      I5 => \caracter[3]_i_29_n_0\,
      O => \caracter[3]_i_14_n_0\
    );
\caracter[3]_i_140\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69696996"
    )
        port map (
      I0 => cent(4),
      I1 => \caracter[2]_i_190_n_0\,
      I2 => \caracter[0]_i_11_n_0\,
      I3 => \caracter[2]_i_219_n_0\,
      I4 => \caracter[2]_i_23_n_0\,
      O => \caracter[3]_i_140_n_0\
    );
\caracter[3]_i_146\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0F1E00000F1EFFFF"
    )
        port map (
      I0 => \caracter_reg[0]_i_13_n_4\,
      I1 => \caracter_reg[0]_i_13_n_5\,
      I2 => \caracter_reg[2]_i_40_n_7\,
      I3 => \caracter[2]_i_41_n_0\,
      I4 => \^total_out_reg[5]\,
      I5 => \caracter_reg[3]_i_141_0\(4),
      O => \caracter[3]_i_146_n_0\
    );
\caracter[3]_i_147\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter[2]_i_23_n_0\,
      I1 => \caracter[2]_i_116_n_0\,
      O => \caracter[3]_i_147_n_0\
    );
\caracter[3]_i_148\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[2]_i_190_n_0\,
      I1 => \caracter[3]_i_58_n_0\,
      O => \caracter[3]_i_148_n_0\
    );
\caracter[3]_i_149\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter[2]_i_219_n_0\,
      I1 => cent(4),
      O => \caracter[3]_i_149_n_0\
    );
\caracter[3]_i_15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"444A"
    )
        port map (
      I0 => \caracter_reg[2]_i_15_n_4\,
      I1 => \caracter_reg[2]_i_14_n_7\,
      I2 => \caracter_reg[2]_i_15_n_6\,
      I3 => \caracter_reg[2]_i_15_n_5\,
      O => \^caracter_reg[2]_i_15_0\
    );
\caracter[3]_i_151\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0880"
    )
        port map (
      I0 => \caracter_reg[3]_i_103_n_5\,
      I1 => \caracter_reg[3]_i_141_n_6\,
      I2 => \caracter_reg[3]_i_103_n_4\,
      I3 => \caracter_reg[3]_i_141_n_5\,
      O => \caracter[3]_i_151_n_0\
    );
\caracter[3]_i_152\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"2828BE28"
    )
        port map (
      I0 => \caracter_reg[3]_i_188_n_2\,
      I1 => \caracter_reg[3]_i_103_n_5\,
      I2 => \caracter_reg[3]_i_141_n_6\,
      I3 => \caracter_reg[3]_i_103_n_6\,
      I4 => \caracter[0]_i_11_n_0\,
      O => \caracter[3]_i_152_n_0\
    );
\caracter[3]_i_153\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"82"
    )
        port map (
      I0 => \caracter_reg[3]_i_188_n_7\,
      I1 => \caracter_reg[3]_i_103_n_6\,
      I2 => \caracter[0]_i_11_n_0\,
      O => \caracter[3]_i_153_n_0\
    );
\caracter[3]_i_154\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[3]_i_189_n_4\,
      I1 => \caracter_reg[3]_i_103_n_7\,
      O => \caracter[3]_i_154_n_0\
    );
\caracter[3]_i_155\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F087870F"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_n_6\,
      I1 => \caracter_reg[3]_i_103_n_5\,
      I2 => \caracter[3]_i_190_n_0\,
      I3 => \caracter_reg[3]_i_103_n_4\,
      I4 => \caracter_reg[3]_i_141_n_5\,
      O => \caracter[3]_i_155_n_0\
    );
\caracter[3]_i_156\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => \caracter[3]_i_152_n_0\,
      I1 => \caracter_reg[3]_i_141_n_5\,
      I2 => \caracter_reg[3]_i_103_n_4\,
      I3 => \caracter_reg[3]_i_141_n_6\,
      I4 => \caracter_reg[3]_i_103_n_5\,
      O => \caracter[3]_i_156_n_0\
    );
\caracter[3]_i_157\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6996699696696996"
    )
        port map (
      I0 => \caracter[3]_i_153_n_0\,
      I1 => \caracter_reg[3]_i_188_n_2\,
      I2 => \caracter_reg[3]_i_103_n_5\,
      I3 => \caracter_reg[3]_i_141_n_6\,
      I4 => \caracter_reg[3]_i_103_n_6\,
      I5 => \caracter[0]_i_11_n_0\,
      O => \caracter[3]_i_157_n_0\
    );
\caracter[3]_i_158\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[3]_i_188_n_7\,
      I1 => \caracter_reg[3]_i_103_n_6\,
      I2 => \caracter[0]_i_11_n_0\,
      I3 => \caracter[3]_i_154_n_0\,
      O => \caracter[3]_i_158_n_0\
    );
\caracter[3]_i_159\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \caracter_reg[3]_i_109_n_7\,
      I1 => \caracter[2]_i_219_n_0\,
      I2 => \caracter_reg[3]_i_86_n_6\,
      O => \caracter[3]_i_159_n_0\
    );
\caracter[3]_i_16\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"010101FFFE00FE00"
    )
        port map (
      I0 => \caracter_reg[3]_i_21_n_7\,
      I1 => \caracter_reg[3]_i_30_n_5\,
      I2 => \caracter_reg[3]_i_30_n_4\,
      I3 => \caracter_reg[3]_i_21_n_4\,
      I4 => \caracter_reg[3]_i_21_n_5\,
      I5 => \caracter_reg[3]_i_21_n_6\,
      O => \caracter[3]_i_16_n_0\
    );
\caracter[3]_i_160\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[3]_i_109_n_5\,
      I1 => \caracter_reg[3]_i_123_n_7\,
      I2 => \caracter_reg[3]_i_86_n_0\,
      O => \caracter[3]_i_160_n_0\
    );
\caracter[3]_i_161\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[3]_i_86_n_5\,
      I1 => \caracter[2]_i_190_n_0\,
      I2 => \caracter_reg[3]_i_109_n_6\,
      O => \caracter[3]_i_161_n_0\
    );
\caracter[3]_i_162\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[3]_i_86_n_6\,
      I1 => \caracter[2]_i_219_n_0\,
      I2 => \caracter_reg[3]_i_109_n_7\,
      O => \caracter[3]_i_162_n_0\
    );
\caracter[3]_i_164\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[3]_i_58_n_0\,
      O => \caracter[3]_i_164_n_0\
    );
\caracter[3]_i_166\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter[0]_i_11_n_0\,
      I1 => \caracter[2]_i_190_n_0\,
      I2 => \caracter[2]_i_116_n_0\,
      O => \caracter[3]_i_166_n_0\
    );
\caracter[3]_i_167\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter[3]_i_58_n_0\,
      I1 => \caracter[2]_i_219_n_0\,
      O => \caracter[3]_i_167_n_0\
    );
\caracter[3]_i_168\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(4),
      I1 => \caracter[0]_i_11_n_0\,
      O => \caracter[3]_i_168_n_0\
    );
\caracter[3]_i_17\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFF1000AFFF5000A"
    )
        port map (
      I0 => \caracter_reg[3]_i_21_n_4\,
      I1 => \caracter_reg[3]_i_21_n_5\,
      I2 => \caracter_reg[3]_i_30_n_4\,
      I3 => \caracter_reg[3]_i_30_n_5\,
      I4 => \caracter_reg[3]_i_21_n_7\,
      I5 => \caracter_reg[3]_i_21_n_6\,
      O => \caracter[3]_i_17_n_0\
    );
\caracter[3]_i_170\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[0]_i_11_n_0\,
      O => \caracter[3]_i_170_n_0\
    );
\caracter[3]_i_171\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter[0]_i_11_n_0\,
      I1 => \caracter[2]_i_219_n_0\,
      I2 => \caracter[2]_i_23_n_0\,
      O => \caracter[3]_i_171_n_0\
    );
\caracter[3]_i_172\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[0]_i_11_n_0\,
      I1 => \caracter[2]_i_190_n_0\,
      O => \caracter[3]_i_172_n_0\
    );
\caracter[3]_i_173\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => p_1_in(1),
      I1 => \^total_out_reg[5]\,
      I2 => \caracter_reg[3]_i_141_0\(1),
      O => \caracter[3]_i_173_n_0\
    );
\caracter[3]_i_174\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[0]_i_11_n_0\,
      O => \caracter[3]_i_174_n_0\
    );
\caracter[3]_i_175\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[0]_i_11_n_0\,
      O => \caracter[3]_i_175_n_0\
    );
\caracter[3]_i_176\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[0]_i_11_n_0\,
      I1 => \caracter[2]_i_23_n_0\,
      O => \caracter[3]_i_176_n_0\
    );
\caracter[3]_i_177\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBBBB0BBB0BBB0B"
    )
        port map (
      I0 => aD2M4dsP(2),
      I1 => \^total_out_reg[5]\,
      I2 => \caracter_reg[3]_i_141_0\(2),
      I3 => \caracter_reg[3]_i_141_0\(7),
      I4 => \caracter_reg[3]_i_141_0\(6),
      I5 => \caracter_reg[3]_i_141_0\(5),
      O => \caracter[3]_i_177_n_0\
    );
\caracter[3]_i_178\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => p_1_in(1),
      I1 => \^total_out_reg[5]\,
      I2 => \caracter_reg[3]_i_141_0\(1),
      O => \caracter[3]_i_178_n_0\
    );
\caracter[3]_i_179\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[0]_i_11_n_0\,
      O => \caracter[3]_i_179_n_0\
    );
\caracter[3]_i_18\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0FE10FE50FA50FA5"
    )
        port map (
      I0 => \caracter_reg[3]_i_21_n_4\,
      I1 => \caracter_reg[3]_i_21_n_5\,
      I2 => \caracter_reg[3]_i_30_n_4\,
      I3 => \caracter_reg[3]_i_30_n_5\,
      I4 => \caracter_reg[3]_i_21_n_7\,
      I5 => \caracter_reg[3]_i_21_n_6\,
      O => \caracter[3]_i_18_n_0\
    );
\caracter[3]_i_180\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[3]_i_189_n_5\,
      I1 => \caracter_reg[3]_i_132_n_4\,
      O => \caracter[3]_i_180_n_0\
    );
\caracter[3]_i_181\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[3]_i_189_n_6\,
      I1 => \caracter_reg[3]_i_132_n_5\,
      O => \caracter[3]_i_181_n_0\
    );
\caracter[3]_i_182\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[3]_i_189_n_7\,
      I1 => \caracter_reg[3]_i_132_n_6\,
      O => \caracter[3]_i_182_n_0\
    );
\caracter[3]_i_183\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \caracter_reg[3]_i_191_n_4\,
      I1 => \caracter[0]_i_11_n_0\,
      O => \caracter[3]_i_183_n_0\
    );
\caracter[3]_i_184\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => \caracter_reg[3]_i_189_n_4\,
      I1 => \caracter_reg[3]_i_103_n_7\,
      I2 => \caracter_reg[3]_i_132_n_4\,
      I3 => \caracter_reg[3]_i_189_n_5\,
      O => \caracter[3]_i_184_n_0\
    );
\caracter[3]_i_185\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \caracter_reg[3]_i_132_n_5\,
      I1 => \caracter_reg[3]_i_189_n_6\,
      I2 => \caracter_reg[3]_i_132_n_4\,
      I3 => \caracter_reg[3]_i_189_n_5\,
      O => \caracter[3]_i_185_n_0\
    );
\caracter[3]_i_186\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \caracter_reg[3]_i_132_n_6\,
      I1 => \caracter_reg[3]_i_189_n_7\,
      I2 => \caracter_reg[3]_i_132_n_5\,
      I3 => \caracter_reg[3]_i_189_n_6\,
      O => \caracter[3]_i_186_n_0\
    );
\caracter[3]_i_187\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4BB4"
    )
        port map (
      I0 => \caracter[0]_i_11_n_0\,
      I1 => \caracter_reg[3]_i_191_n_4\,
      I2 => \caracter_reg[3]_i_132_n_6\,
      I3 => \caracter_reg[3]_i_189_n_7\,
      O => \caracter[3]_i_187_n_0\
    );
\caracter[3]_i_19\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2BBFFFF200022BB0"
    )
        port map (
      I0 => \string_cent_decenas[1]5\(1),
      I1 => \caracter[3]_i_31_n_0\,
      I2 => \caracter[3]_i_32_n_0\,
      I3 => \caracter_reg[2]_i_15_n_6\,
      I4 => \caracter_reg[2]_i_15_n_5\,
      I5 => \caracter[3]_i_33_n_0\,
      O => \caracter[3]_i_19_n_0\
    );
\caracter[3]_i_190\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[3]_i_86_n_7\,
      I1 => \caracter[0]_i_11_n_0\,
      I2 => \caracter_reg[3]_i_141_n_4\,
      O => \caracter[3]_i_190_n_0\
    );
\caracter[3]_i_193\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter[2]_i_219_n_0\,
      I1 => \caracter[2]_i_116_n_0\,
      O => \caracter[3]_i_193_n_0\
    );
\caracter[3]_i_194\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[3]_i_58_n_0\,
      O => \caracter[3]_i_194_n_0\
    );
\caracter[3]_i_197\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \caracter[2]_i_116_n_0\,
      I1 => \caracter[2]_i_219_n_0\,
      I2 => \caracter[2]_i_190_n_0\,
      O => \caracter[3]_i_197_n_0\
    );
\caracter[3]_i_199\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[2]_i_116_n_0\,
      I1 => \caracter[2]_i_219_n_0\,
      O => \caracter[3]_i_199_n_0\
    );
\caracter[3]_i_20\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4A4A4A4A4A4A4AAA"
    )
        port map (
      I0 => \caracter_reg[3]_i_21_n_5\,
      I1 => \caracter_reg[3]_i_21_n_4\,
      I2 => \caracter_reg[3]_i_21_n_6\,
      I3 => \caracter_reg[3]_i_21_n_7\,
      I4 => \caracter_reg[3]_i_30_n_5\,
      I5 => \caracter_reg[3]_i_30_n_4\,
      O => \caracter[3]_i_20_n_0\
    );
\caracter[3]_i_200\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[3]_i_58_n_0\,
      I1 => \caracter[0]_i_11_n_0\,
      O => \caracter[3]_i_200_n_0\
    );
\caracter[3]_i_202\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[3]_i_58_n_0\,
      O => \caracter[3]_i_202_n_0\
    );
\caracter[3]_i_203\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6669"
    )
        port map (
      I0 => \caracter[2]_i_219_n_0\,
      I1 => \caracter[2]_i_116_n_0\,
      I2 => \caracter[3]_i_58_n_0\,
      I3 => \caracter[0]_i_11_n_0\,
      O => \caracter[3]_i_203_n_0\
    );
\caracter[3]_i_204\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[3]_i_58_n_0\,
      I1 => \caracter[0]_i_11_n_0\,
      O => \caracter[3]_i_204_n_0\
    );
\caracter[3]_i_205\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[2]_i_116_n_0\,
      I1 => cent(4),
      O => \caracter[3]_i_205_n_0\
    );
\caracter[3]_i_206\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter[3]_i_58_n_0\,
      I1 => \caracter[2]_i_23_n_0\,
      O => \caracter[3]_i_206_n_0\
    );
\caracter[3]_i_210\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[2]_i_190_n_0\,
      I1 => cent(4),
      O => \caracter[3]_i_210_n_0\
    );
\caracter[3]_i_211\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[2]_i_23_n_0\,
      I1 => \caracter[2]_i_219_n_0\,
      O => \caracter[3]_i_211_n_0\
    );
\caracter[3]_i_212\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter[2]_i_190_n_0\,
      I1 => \caracter[0]_i_11_n_0\,
      O => \caracter[3]_i_212_n_0\
    );
\caracter[3]_i_22\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \caracter_reg[3]_i_30_n_4\,
      I1 => \caracter_reg[3]_i_30_n_5\,
      O => \caracter[3]_i_22_n_0\
    );
\caracter[3]_i_23\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"40D00D40F4FDDFF4"
    )
        port map (
      I0 => \caracter[3]_i_41_n_0\,
      I1 => \caracter[3]_i_42_n_0\,
      I2 => \caracter_reg[3]_i_30_n_4\,
      I3 => \caracter[3]_i_43_n_0\,
      I4 => \caracter_reg[3]_i_30_n_5\,
      I5 => \^caracter_reg[2]_i_15_0\,
      O => \caracter[3]_i_23_n_0\
    );
\caracter[3]_i_24\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAA56655995"
    )
        port map (
      I0 => \caracter[3]_i_44_n_0\,
      I1 => \string_cent_decenas[1]5\(1),
      I2 => \string_cent_decenas[1]5\(0),
      I3 => \caracter[0]_i_11_n_0\,
      I4 => \caracter[3]_i_45_n_0\,
      I5 => \caracter[3]_i_11_n_0\,
      O => \caracter[3]_i_24_n_0\
    );
\caracter[3]_i_25\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9696969669699669"
    )
        port map (
      I0 => \^caracter_reg[2]_i_15_0\,
      I1 => \caracter[3]_i_18_n_0\,
      I2 => \caracter[3]_i_19_n_0\,
      I3 => \caracter[3]_i_46_n_0\,
      I4 => \caracter[3]_i_44_n_0\,
      I5 => \caracter[3]_i_11_n_0\,
      O => \caracter[3]_i_25_n_0\
    );
\caracter[3]_i_26\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66699666"
    )
        port map (
      I0 => \string_cent_decenas[1]5\(1),
      I1 => \caracter[3]_i_45_n_0\,
      I2 => \string_cent_decenas[1]5\(0),
      I3 => \caracter[0]_i_11_n_0\,
      I4 => \caracter[3]_i_11_n_0\,
      O => \caracter[3]_i_26_n_0\
    );
\caracter[3]_i_27\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7E577E56A86AA86B"
    )
        port map (
      I0 => \caracter[3]_i_16_n_0\,
      I1 => \caracter[3]_i_17_n_0\,
      I2 => \caracter[3]_i_23_n_0\,
      I3 => \caracter[3]_i_20_n_0\,
      I4 => \caracter[3]_i_47_n_0\,
      I5 => \caracter[3]_i_48_n_0\,
      O => \caracter[3]_i_27_n_0\
    );
\caracter[3]_i_28\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4DDFF333"
    )
        port map (
      I0 => \caracter[3]_i_48_n_0\,
      I1 => \caracter[3]_i_20_n_0\,
      I2 => \caracter[3]_i_23_n_0\,
      I3 => \caracter[3]_i_17_n_0\,
      I4 => \caracter[3]_i_16_n_0\,
      O => \caracter[3]_i_28_n_0\
    );
\caracter[3]_i_29\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A55A5AA5A5AAAA59"
    )
        port map (
      I0 => \caracter[3]_i_48_n_0\,
      I1 => \caracter[3]_i_47_n_0\,
      I2 => \caracter[3]_i_20_n_0\,
      I3 => \caracter[3]_i_23_n_0\,
      I4 => \caracter[3]_i_17_n_0\,
      I5 => \caracter[3]_i_16_n_0\,
      O => \caracter[3]_i_29_n_0\
    );
\caracter[3]_i_31\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00E2"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(0),
      I1 => \^total_out_reg[5]\,
      I2 => p_1_in(0),
      I3 => \string_cent_decenas[1]5\(0),
      O => \caracter[3]_i_31_n_0\
    );
\caracter[3]_i_32\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0057"
    )
        port map (
      I0 => \caracter_reg[2]_i_15_n_4\,
      I1 => \caracter_reg[2]_i_15_n_5\,
      I2 => \caracter_reg[2]_i_15_n_6\,
      I3 => \caracter_reg[2]_i_14_n_7\,
      O => \caracter[3]_i_32_n_0\
    );
\caracter[3]_i_33\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0F0F0F0F5A58F0F0"
    )
        port map (
      I0 => \caracter_reg[3]_i_21_n_6\,
      I1 => \caracter_reg[3]_i_21_n_7\,
      I2 => \caracter_reg[3]_i_30_n_5\,
      I3 => \caracter_reg[3]_i_30_n_4\,
      I4 => \caracter_reg[3]_i_21_n_5\,
      I5 => \caracter_reg[3]_i_21_n_4\,
      O => \caracter[3]_i_33_n_0\
    );
\caracter[3]_i_35\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[3]_i_58_n_0\,
      O => cent(5)
    );
\caracter[3]_i_37\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[3]_i_59_n_5\,
      O => \caracter[3]_i_37_n_0\
    );
\caracter[3]_i_38\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter[2]_i_116_n_0\,
      I1 => \caracter_reg[3]_i_59_n_6\,
      O => \caracter[3]_i_38_n_0\
    );
\caracter[3]_i_39\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[3]_i_58_n_0\,
      I1 => \caracter_reg[3]_i_59_n_7\,
      O => \caracter[3]_i_39_n_0\
    );
\caracter[3]_i_40\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(4),
      I1 => \caracter_reg[3]_i_60_n_5\,
      O => \caracter[3]_i_40_n_0\
    );
\caracter[3]_i_41\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"32660000FFFF3266"
    )
        port map (
      I0 => \caracter_reg[2]_i_14_n_7\,
      I1 => \caracter_reg[2]_i_15_n_6\,
      I2 => \caracter_reg[2]_i_15_n_5\,
      I3 => \caracter_reg[2]_i_15_n_4\,
      I4 => \caracter[3]_i_31_n_0\,
      I5 => \string_cent_decenas[1]5\(1),
      O => \caracter[3]_i_41_n_0\
    );
\caracter[3]_i_42\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6675"
    )
        port map (
      I0 => \caracter_reg[2]_i_15_n_5\,
      I1 => \caracter_reg[2]_i_15_n_6\,
      I2 => \caracter_reg[2]_i_15_n_4\,
      I3 => \caracter_reg[2]_i_14_n_7\,
      O => \caracter[3]_i_42_n_0\
    );
\caracter[3]_i_43\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAAAAAAAAA"
    )
        port map (
      I0 => \caracter_reg[3]_i_21_n_4\,
      I1 => \caracter_reg[3]_i_21_n_5\,
      I2 => \caracter_reg[3]_i_30_n_4\,
      I3 => \caracter_reg[3]_i_30_n_5\,
      I4 => \caracter_reg[3]_i_21_n_7\,
      I5 => \caracter_reg[3]_i_21_n_6\,
      O => \caracter[3]_i_43_n_0\
    );
\caracter[3]_i_44\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A6656559599A9AA6"
    )
        port map (
      I0 => \caracter[3]_i_33_n_0\,
      I1 => \string_cent_decenas[1]5\(1),
      I2 => \caracter[3]_i_31_n_0\,
      I3 => \caracter[3]_i_32_n_0\,
      I4 => \caracter_reg[2]_i_15_n_6\,
      I5 => \caracter_reg[2]_i_15_n_5\,
      O => \caracter[3]_i_44_n_0\
    );
\caracter[3]_i_45\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CD99"
    )
        port map (
      I0 => \caracter_reg[2]_i_14_n_7\,
      I1 => \caracter_reg[2]_i_15_n_6\,
      I2 => \caracter_reg[2]_i_15_n_5\,
      I3 => \caracter_reg[2]_i_15_n_4\,
      O => \caracter[3]_i_45_n_0\
    );
\caracter[3]_i_46\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1428"
    )
        port map (
      I0 => \string_cent_decenas[1]5\(1),
      I1 => \string_cent_decenas[1]5\(0),
      I2 => \caracter[0]_i_11_n_0\,
      I3 => \caracter[3]_i_45_n_0\,
      O => \caracter[3]_i_46_n_0\
    );
\caracter[3]_i_47\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0110044002200880"
    )
        port map (
      I0 => \caracter[3]_i_42_n_0\,
      I1 => \caracter[3]_i_45_n_0\,
      I2 => \caracter[0]_i_11_n_0\,
      I3 => \string_cent_decenas[1]5\(0),
      I4 => \string_cent_decenas[1]5\(1),
      I5 => \caracter[3]_i_33_n_0\,
      O => \caracter[3]_i_47_n_0\
    );
\caracter[3]_i_48\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"D24BB4D22DB44B2D"
    )
        port map (
      I0 => \caracter[3]_i_42_n_0\,
      I1 => \caracter[3]_i_41_n_0\,
      I2 => \caracter_reg[3]_i_30_n_4\,
      I3 => \caracter[3]_i_43_n_0\,
      I4 => \caracter_reg[3]_i_30_n_5\,
      I5 => \^caracter_reg[2]_i_15_0\,
      O => \caracter[3]_i_48_n_0\
    );
\caracter[3]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCAF0CAFFCA00CA0"
    )
        port map (
      I0 => \caracter[6]_i_4_0\,
      I1 => \caracter[3]_i_2\,
      I2 => Q(0),
      I3 => Q(1),
      I4 => \caracter[6]_i_4_2\,
      I5 => \^fsm_sequential_current_state_reg[0]\,
      O => \reg_reg[0]_1\
    );
\caracter[3]_i_52\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[0]_i_11_n_0\,
      O => \disp_dinero[2]_7\(0)
    );
\caracter[3]_i_53\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter[2]_i_23_n_0\,
      I1 => \caracter_reg[3]_i_60_n_6\,
      O => \caracter[3]_i_53_n_0\
    );
\caracter[3]_i_54\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter[2]_i_190_n_0\,
      I1 => \caracter_reg[3]_i_60_n_7\,
      O => \caracter[3]_i_54_n_0\
    );
\caracter[3]_i_55\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => p_1_in(1),
      I1 => \^total_out_reg[5]\,
      I2 => \caracter_reg[3]_i_141_0\(1),
      O => \caracter[3]_i_55_n_0\
    );
\caracter[3]_i_56\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => p_1_in(0),
      I1 => \^total_out_reg[5]\,
      I2 => \caracter_reg[3]_i_141_0\(0),
      O => \caracter[3]_i_56_n_0\
    );
\caracter[3]_i_57\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter[3]_i_61_n_0\,
      I1 => \^total_out_reg[5]\,
      I2 => \caracter_reg[3]_i_141_0\(6),
      O => \caracter[2]_i_116_n_0\
    );
\caracter[3]_i_58\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \caracter[3]_i_62_n_0\,
      I1 => \^total_out_reg[5]\,
      I2 => \caracter_reg[3]_i_141_0\(5),
      O => \caracter[3]_i_58_n_0\
    );
\caracter[3]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C0CCC0C8C0C8C0CC"
    )
        port map (
      I0 => \caracter[3]_i_11_n_0\,
      I1 => \caracter[3]_i_3\,
      I2 => \caracter[3]_i_3_0\,
      I3 => \caracter[3]_i_12_n_0\,
      I4 => \caracter[3]_i_13_n_0\,
      I5 => \caracter[3]_i_14_n_0\,
      O => \^fsm_sequential_current_state_reg[0]\
    );
\caracter[3]_i_61\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222CCCCCCCCC"
    )
        port map (
      I0 => \caracter_reg[2]_i_40_n_4\,
      I1 => \caracter_reg[2]_i_40_n_5\,
      I2 => \caracter_reg[0]_i_13_n_5\,
      I3 => \caracter_reg[0]_i_13_n_4\,
      I4 => \caracter_reg[2]_i_40_n_7\,
      I5 => \caracter_reg[2]_i_40_n_6\,
      O => \caracter[3]_i_61_n_0\
    );
\caracter[3]_i_62\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"555655560002AAAA"
    )
        port map (
      I0 => \caracter_reg[2]_i_40_n_6\,
      I1 => \caracter_reg[2]_i_40_n_7\,
      I2 => \caracter_reg[0]_i_13_n_4\,
      I3 => \caracter_reg[0]_i_13_n_5\,
      I4 => \caracter_reg[2]_i_40_n_5\,
      I5 => \caracter_reg[2]_i_40_n_4\,
      O => \caracter[3]_i_62_n_0\
    );
\caracter[3]_i_63\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \caracter_reg[3]_i_60_n_4\,
      I1 => \caracter_reg[3]_i_60_n_7\,
      O => \caracter[3]_i_63_n_0\
    );
\caracter[3]_i_64\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66969969"
    )
        port map (
      I0 => \caracter_reg[3]_i_75_n_6\,
      I1 => \caracter_reg[3]_i_60_n_5\,
      I2 => \caracter_reg[3]_i_75_n_7\,
      I3 => \caracter_reg[3]_i_60_n_6\,
      I4 => \caracter_reg[3]_i_60_n_7\,
      O => \caracter[3]_i_64_n_0\
    );
\caracter[3]_i_65\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => \caracter_reg[3]_i_60_n_7\,
      I1 => \caracter_reg[3]_i_60_n_4\,
      I2 => \caracter_reg[3]_i_75_n_7\,
      I3 => \caracter_reg[3]_i_60_n_6\,
      O => \caracter[3]_i_65_n_0\
    );
\caracter[3]_i_66\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter_reg[3]_i_60_n_4\,
      I1 => \caracter_reg[3]_i_60_n_7\,
      O => \caracter[3]_i_66_n_0\
    );
\caracter[3]_i_68\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"28"
    )
        port map (
      I0 => \caracter_reg[3]_i_85_n_1\,
      I1 => \caracter_reg[3]_i_86_n_0\,
      I2 => \caracter_reg[3]_i_87_n_1\,
      O => \caracter[3]_i_68_n_0\
    );
\caracter[3]_i_69\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \caracter_reg[3]_i_85_n_6\,
      I1 => \caracter_reg[3]_i_85_n_1\,
      I2 => \caracter_reg[3]_i_87_n_1\,
      I3 => \caracter_reg[3]_i_86_n_0\,
      O => \caracter[3]_i_69_n_0\
    );
\caracter[3]_i_70\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \caracter_reg[3]_i_85_n_7\,
      I1 => \caracter_reg[3]_i_85_n_6\,
      I2 => \caracter_reg[3]_i_87_n_1\,
      I3 => \caracter_reg[3]_i_86_n_0\,
      O => \caracter[3]_i_70_n_0\
    );
\caracter[3]_i_71\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \caracter_reg[3]_i_86_n_0\,
      I1 => \caracter_reg[3]_i_87_n_1\,
      O => \caracter[3]_i_71_n_0\
    );
\caracter[3]_i_72\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter_reg[3]_i_85_n_1\,
      I1 => \caracter_reg[3]_i_87_n_1\,
      I2 => \caracter_reg[3]_i_86_n_0\,
      O => \caracter[3]_i_72_n_0\
    );
\caracter[3]_i_73\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A995"
    )
        port map (
      I0 => \caracter[3]_i_69_n_0\,
      I1 => \caracter_reg[3]_i_87_n_1\,
      I2 => \caracter_reg[3]_i_86_n_0\,
      I3 => \caracter_reg[3]_i_85_n_1\,
      O => \caracter[3]_i_73_n_0\
    );
\caracter[3]_i_74\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCC9C999"
    )
        port map (
      I0 => \caracter_reg[3]_i_85_n_6\,
      I1 => \caracter_reg[3]_i_85_n_1\,
      I2 => \caracter_reg[3]_i_87_n_1\,
      I3 => \caracter_reg[3]_i_86_n_0\,
      I4 => \caracter_reg[3]_i_85_n_7\,
      O => \caracter[3]_i_74_n_0\
    );
\caracter[3]_i_77\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \caracter_reg[3]_i_99_n_4\,
      I1 => \caracter_reg[3]_i_85_n_7\,
      I2 => \caracter_reg[3]_i_87_n_1\,
      I3 => \caracter_reg[3]_i_86_n_0\,
      O => \caracter[3]_i_77_n_0\
    );
\caracter[3]_i_78\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \caracter_reg[3]_i_99_n_5\,
      I1 => \caracter_reg[3]_i_99_n_4\,
      I2 => \caracter_reg[3]_i_87_n_1\,
      I3 => \caracter_reg[3]_i_86_n_0\,
      O => \caracter[3]_i_78_n_0\
    );
\caracter[3]_i_79\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \caracter_reg[3]_i_99_n_6\,
      I1 => \caracter_reg[3]_i_99_n_5\,
      I2 => \caracter_reg[3]_i_87_n_1\,
      I3 => \caracter_reg[3]_i_86_n_0\,
      O => \caracter[3]_i_79_n_0\
    );
\caracter[3]_i_80\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"044A"
    )
        port map (
      I0 => \caracter_reg[3]_i_99_n_6\,
      I1 => \caracter_reg[3]_i_99_n_7\,
      I2 => \caracter_reg[3]_i_86_n_0\,
      I3 => \caracter_reg[3]_i_87_n_1\,
      O => \caracter[3]_i_80_n_0\
    );
\caracter[3]_i_81\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0E1E1C3"
    )
        port map (
      I0 => \caracter_reg[3]_i_99_n_4\,
      I1 => \caracter_reg[3]_i_85_n_7\,
      I2 => \caracter_reg[3]_i_85_n_6\,
      I3 => \caracter_reg[3]_i_87_n_1\,
      I4 => \caracter_reg[3]_i_86_n_0\,
      O => \caracter[3]_i_81_n_0\
    );
\caracter[3]_i_82\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0E1E1C3"
    )
        port map (
      I0 => \caracter_reg[3]_i_99_n_5\,
      I1 => \caracter_reg[3]_i_99_n_4\,
      I2 => \caracter_reg[3]_i_85_n_7\,
      I3 => \caracter_reg[3]_i_87_n_1\,
      I4 => \caracter_reg[3]_i_86_n_0\,
      O => \caracter[3]_i_82_n_0\
    );
\caracter[3]_i_83\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0E1E1C3"
    )
        port map (
      I0 => \caracter_reg[3]_i_99_n_6\,
      I1 => \caracter_reg[3]_i_99_n_5\,
      I2 => \caracter_reg[3]_i_99_n_4\,
      I3 => \caracter_reg[3]_i_87_n_1\,
      I4 => \caracter_reg[3]_i_86_n_0\,
      O => \caracter[3]_i_83_n_0\
    );
\caracter[3]_i_84\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0E1E1C3"
    )
        port map (
      I0 => \caracter_reg[3]_i_99_n_7\,
      I1 => \caracter_reg[3]_i_99_n_6\,
      I2 => \caracter_reg[3]_i_99_n_5\,
      I3 => \caracter_reg[3]_i_87_n_1\,
      I4 => \caracter_reg[3]_i_86_n_0\,
      O => \caracter[3]_i_84_n_0\
    );
\caracter[3]_i_88\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \caracter_reg[3]_i_86_n_0\,
      I1 => \caracter_reg[3]_i_87_n_1\,
      O => \caracter[3]_i_88_n_0\
    );
\caracter[3]_i_89\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \caracter_reg[3]_i_86_n_0\,
      I1 => \caracter_reg[3]_i_87_n_1\,
      O => \caracter[3]_i_89_n_0\
    );
\caracter[3]_i_91\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"90660060"
    )
        port map (
      I0 => \caracter_reg[3]_i_87_n_1\,
      I1 => \caracter_reg[3]_i_99_n_7\,
      I2 => \caracter_reg[3]_i_123_n_4\,
      I3 => \caracter_reg[3]_i_86_n_0\,
      I4 => \caracter_reg[3]_i_87_n_6\,
      O => \caracter[3]_i_91_n_0\
    );
\caracter[3]_i_92\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60990090"
    )
        port map (
      I0 => \caracter_reg[3]_i_87_n_6\,
      I1 => \caracter_reg[3]_i_123_n_4\,
      I2 => \caracter_reg[3]_i_123_n_5\,
      I3 => \caracter_reg[3]_i_86_n_0\,
      I4 => \caracter_reg[3]_i_87_n_7\,
      O => \caracter[3]_i_92_n_0\
    );
\caracter[3]_i_93\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60990090"
    )
        port map (
      I0 => \caracter_reg[3]_i_87_n_7\,
      I1 => \caracter_reg[3]_i_123_n_5\,
      I2 => \caracter_reg[3]_i_123_n_6\,
      I3 => \caracter_reg[3]_i_86_n_0\,
      I4 => \caracter_reg[3]_i_109_n_4\,
      O => \caracter[3]_i_93_n_0\
    );
\caracter[3]_i_94\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08E0800E"
    )
        port map (
      I0 => \caracter_reg[3]_i_109_n_5\,
      I1 => \caracter_reg[3]_i_123_n_7\,
      I2 => \caracter_reg[3]_i_109_n_4\,
      I3 => \caracter_reg[3]_i_86_n_0\,
      I4 => \caracter_reg[3]_i_123_n_6\,
      O => \caracter[3]_i_94_n_0\
    );
\caracter[3]_i_95\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"566AA995"
    )
        port map (
      I0 => \caracter[3]_i_91_n_0\,
      I1 => \caracter_reg[3]_i_87_n_1\,
      I2 => \caracter_reg[3]_i_86_n_0\,
      I3 => \caracter_reg[3]_i_99_n_7\,
      I4 => \caracter_reg[3]_i_99_n_6\,
      O => \caracter[3]_i_95_n_0\
    );
\caracter[3]_i_96\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"95566AA96AA99556"
    )
        port map (
      I0 => \caracter[3]_i_92_n_0\,
      I1 => \caracter_reg[3]_i_87_n_6\,
      I2 => \caracter_reg[3]_i_86_n_0\,
      I3 => \caracter_reg[3]_i_123_n_4\,
      I4 => \caracter_reg[3]_i_99_n_7\,
      I5 => \caracter_reg[3]_i_87_n_1\,
      O => \caracter[3]_i_96_n_0\
    );
\caracter[3]_i_97\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AA9955695566AA9"
    )
        port map (
      I0 => \caracter[3]_i_93_n_0\,
      I1 => \caracter_reg[3]_i_87_n_7\,
      I2 => \caracter_reg[3]_i_86_n_0\,
      I3 => \caracter_reg[3]_i_123_n_5\,
      I4 => \caracter_reg[3]_i_123_n_4\,
      I5 => \caracter_reg[3]_i_87_n_6\,
      O => \caracter[3]_i_97_n_0\
    );
\caracter[3]_i_98\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AA9955695566AA9"
    )
        port map (
      I0 => \caracter[3]_i_94_n_0\,
      I1 => \caracter_reg[3]_i_109_n_4\,
      I2 => \caracter_reg[3]_i_86_n_0\,
      I3 => \caracter_reg[3]_i_123_n_6\,
      I4 => \caracter_reg[3]_i_123_n_5\,
      I5 => \caracter_reg[3]_i_87_n_7\,
      O => \caracter[3]_i_98_n_0\
    );
\caracter[4]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF88888880"
    )
        port map (
      I0 => \caracter_reg[3]_i_141_0\(5),
      I1 => \caracter_reg[3]_i_141_0\(6),
      I2 => \caracter_reg[3]_i_141_0\(3),
      I3 => \caracter_reg[3]_i_141_0\(4),
      I4 => \caracter_reg[3]_i_141_0\(2),
      I5 => \caracter_reg[3]_i_141_0\(7),
      O => \^total_out_reg[5]\
    );
\caracter[6]_i_18\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFAAABFFFFAFAB"
    )
        port map (
      I0 => \caracter_reg[6]_i_28_n_0\,
      I1 => \^fsm_sequential_current_state_reg[1]_1\,
      I2 => Q(1),
      I3 => Q(0),
      I4 => \caracter[6]_i_4\,
      I5 => \caracter[0]_i_4\,
      O => \reg_reg[1]\
    );
\caracter[6]_i_20\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCAF0CAFFCA00CA0"
    )
        port map (
      I0 => \^fsm_sequential_current_state_reg[0]\,
      I1 => \caracter[6]_i_4_0\,
      I2 => Q(0),
      I3 => Q(1),
      I4 => \caracter[6]_i_4_1\,
      I5 => \caracter[6]_i_4_2\,
      O => \reg_reg[0]_0\
    );
\caracter[6]_i_37\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"04C4FFFF04C40000"
    )
        port map (
      I0 => \disp_refresco[5]_3\(1),
      I1 => \caracter_reg[1]_i_4\,
      I2 => \caracter[6]_i_38_0\,
      I3 => \^total_out_reg[5]\,
      I4 => Q(0),
      I5 => \caracter[2]_i_9_n_0\,
      O => \caracter[6]_i_37_n_0\
    );
\caracter[6]_i_38\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0002FA0AF002FA0"
    )
        port map (
      I0 => \caracter[2]_i_13_n_0\,
      I1 => \disp_refresco[1]_4\(0),
      I2 => Q(0),
      I3 => \caracter_reg[1]_i_4\,
      I4 => \caracter[6]_i_38_0\,
      I5 => \^total_out_reg[5]\,
      O => \caracter[6]_i_38_n_0\
    );
\caracter_reg[0]_i_100\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_87_n_0\,
      CO(3) => \caracter_reg[0]_i_100_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_100_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_231_n_0\,
      DI(2) => \caracter[0]_i_232_n_0\,
      DI(1) => \caracter[0]_i_233_n_0\,
      DI(0) => \caracter[0]_i_234_n_0\,
      O(3) => \caracter_reg[0]_i_100_n_4\,
      O(2) => \caracter_reg[0]_i_100_n_5\,
      O(1) => \caracter_reg[0]_i_100_n_6\,
      O(0) => \caracter_reg[0]_i_100_n_7\,
      S(3) => \caracter[0]_i_235_n_0\,
      S(2) => \caracter[0]_i_236_n_0\,
      S(1) => \caracter[0]_i_237_n_0\,
      S(0) => \caracter[0]_i_238_n_0\
    );
\caracter_reg[0]_i_103\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_239_n_0\,
      CO(3) => \caracter_reg[0]_i_103_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_103_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_240_n_0\,
      DI(2) => \caracter[0]_i_241_n_0\,
      DI(1) => \caracter[0]_i_242_n_0\,
      DI(0) => \caracter[0]_i_243_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[0]_i_103_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[0]_i_244_n_0\,
      S(2) => \caracter[0]_i_245_n_0\,
      S(1) => \caracter[0]_i_246_n_0\,
      S(0) => \caracter[0]_i_247_n_0\
    );
\caracter_reg[0]_i_112\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_249_n_0\,
      CO(3 downto 1) => \NLW_caracter_reg[0]_i_112_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \caracter_reg[0]_i_112_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_caracter_reg[0]_i_112_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
\caracter_reg[0]_i_113\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_250_n_0\,
      CO(3) => \caracter_reg[0]_i_113_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_113_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => \caracter_reg[3]_i_141_0\(7 downto 5),
      O(3) => \NLW_caracter_reg[0]_i_113_O_UNCONNECTED\(3),
      O(2) => \caracter_reg[0]_i_113_n_5\,
      O(1) => \caracter_reg[0]_i_113_n_6\,
      O(0) => \caracter_reg[0]_i_113_n_7\,
      S(3) => '1',
      S(2) => \caracter[0]_i_251_n_0\,
      S(1) => \caracter[0]_i_252_n_0\,
      S(0) => \caracter[0]_i_253_n_0\
    );
\caracter_reg[0]_i_114\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_248_n_0\,
      CO(3) => \caracter_reg[0]_i_114_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_114_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \caracter_reg[3]_i_141_0\(6),
      DI(0) => \caracter[0]_i_254_n_0\,
      O(3) => \NLW_caracter_reg[0]_i_114_O_UNCONNECTED\(3),
      O(2) => \caracter_reg[0]_i_114_n_5\,
      O(1) => \caracter_reg[0]_i_114_n_6\,
      O(0) => \caracter_reg[0]_i_114_n_7\,
      S(3) => '1',
      S(2) => \caracter_reg[3]_i_141_0\(7),
      S(1) => \caracter[0]_i_255_n_0\,
      S(0) => \caracter[0]_i_256_n_0\
    );
\caracter_reg[0]_i_115\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_257_n_0\,
      CO(3) => \caracter_reg[0]_i_115_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_115_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_258_n_0\,
      DI(2) => \caracter[0]_i_259_n_0\,
      DI(1) => \caracter[0]_i_260_n_0\,
      DI(0) => \caracter[0]_i_261_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[0]_i_115_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[0]_i_262_n_0\,
      S(2) => \caracter[0]_i_263_n_0\,
      S(1) => \caracter[0]_i_264_n_0\,
      S(0) => \caracter[0]_i_265_n_0\
    );
\caracter_reg[0]_i_124\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_266_n_0\,
      CO(3) => \caracter_reg[0]_i_124_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_124_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => euros3(20 downto 17),
      S(3) => \caracter_reg[0]_i_21_n_3\,
      S(2) => \caracter_reg[0]_i_21_n_3\,
      S(1) => \caracter_reg[0]_i_21_n_3\,
      S(0) => \caracter_reg[0]_i_21_n_3\
    );
\caracter_reg[0]_i_129\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_267_n_0\,
      CO(3) => \caracter_reg[0]_i_129_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_129_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_268_n_0\,
      DI(2) => \caracter[0]_i_269_n_0\,
      DI(1) => \caracter[0]_i_270_n_0\,
      DI(0) => \caracter[0]_i_271_n_0\,
      O(3) => \caracter_reg[0]_i_129_n_4\,
      O(2) => \caracter_reg[0]_i_129_n_5\,
      O(1) => \caracter_reg[0]_i_129_n_6\,
      O(0) => \caracter_reg[0]_i_129_n_7\,
      S(3) => \caracter[0]_i_272_n_0\,
      S(2) => \caracter[0]_i_273_n_0\,
      S(1) => \caracter[0]_i_274_n_0\,
      S(0) => \caracter[0]_i_275_n_0\
    );
\caracter_reg[0]_i_13\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[0]_i_13_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_13_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '1',
      DI(3 downto 0) => \caracter_reg[3]_i_141_0\(3 downto 0),
      O(3) => \caracter_reg[0]_i_13_n_4\,
      O(2) => \caracter_reg[0]_i_13_n_5\,
      O(1 downto 0) => p_1_in(1 downto 0),
      S(3) => \caracter[0]_i_15_n_0\,
      S(2) => \caracter[0]_i_16_n_0\,
      S(1) => \caracter[0]_i_17_n_0\,
      S(0) => \caracter[0]_i_18_n_0\
    );
\caracter_reg[0]_i_138\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_276_n_0\,
      CO(3) => \caracter_reg[0]_i_138_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_138_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_277_n_0\,
      DI(2) => \caracter[0]_i_278_n_0\,
      DI(1) => \caracter[0]_i_279_n_0\,
      DI(0) => \caracter[0]_i_280_n_0\,
      O(3) => \caracter_reg[0]_i_138_n_4\,
      O(2) => \caracter_reg[0]_i_138_n_5\,
      O(1) => \caracter_reg[0]_i_138_n_6\,
      O(0) => \caracter_reg[0]_i_138_n_7\,
      S(3) => \caracter[0]_i_281_n_0\,
      S(2) => \caracter[0]_i_282_n_0\,
      S(1) => \caracter[0]_i_283_n_0\,
      S(0) => \caracter[0]_i_284_n_0\
    );
\caracter_reg[0]_i_148\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_289_n_0\,
      CO(3) => \caracter_reg[0]_i_148_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_148_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_290_n_0\,
      DI(2) => \caracter[0]_i_291_n_0\,
      DI(1) => \caracter[0]_i_292_n_0\,
      DI(0) => \caracter[0]_i_293_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[0]_i_148_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[0]_i_294_n_0\,
      S(2) => \caracter[0]_i_295_n_0\,
      S(1) => \caracter[0]_i_296_n_0\,
      S(0) => \caracter[0]_i_297_n_0\
    );
\caracter_reg[0]_i_157\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_298_n_0\,
      CO(3) => \caracter_reg[0]_i_157_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_157_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_310_n_0\,
      DI(2) => \caracter[0]_i_311_n_0\,
      DI(1) => \caracter[0]_i_312_n_0\,
      DI(0) => \caracter[0]_i_313_n_0\,
      O(3) => \caracter_reg[0]_i_157_n_4\,
      O(2) => \caracter_reg[0]_i_157_n_5\,
      O(1) => \caracter_reg[0]_i_157_n_6\,
      O(0) => \caracter_reg[0]_i_157_n_7\,
      S(3) => \caracter[0]_i_314_n_0\,
      S(2) => \caracter[0]_i_315_n_0\,
      S(1) => \caracter[0]_i_316_n_0\,
      S(0) => \caracter[0]_i_317_n_0\
    );
\caracter_reg[0]_i_160\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_301_n_0\,
      CO(3) => \caracter_reg[0]_i_160_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_160_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => euros2(8),
      DI(2) => \caracter[0]_i_319_n_0\,
      DI(1) => \caracter[0]_i_320_n_0\,
      DI(0) => \caracter[0]_i_321_n_0\,
      O(3) => \caracter_reg[0]_i_160_n_4\,
      O(2) => \caracter_reg[0]_i_160_n_5\,
      O(1) => \caracter_reg[0]_i_160_n_6\,
      O(0) => \caracter_reg[0]_i_160_n_7\,
      S(3) => \caracter[0]_i_322_n_0\,
      S(2) => \caracter[0]_i_323_n_0\,
      S(1) => \caracter[0]_i_324_n_0\,
      S(0) => \caracter[0]_i_325_n_0\
    );
\caracter_reg[0]_i_162\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_303_n_0\,
      CO(3) => \caracter_reg[0]_i_162_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_162_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_326_n_0\,
      DI(2) => \caracter[0]_i_327_n_0\,
      DI(1) => \caracter[0]_i_328_n_0\,
      DI(0) => \caracter[0]_i_329_n_0\,
      O(3) => \caracter_reg[0]_i_162_n_4\,
      O(2) => \caracter_reg[0]_i_162_n_5\,
      O(1) => \caracter_reg[0]_i_162_n_6\,
      O(0) => \caracter_reg[0]_i_162_n_7\,
      S(3) => \caracter[0]_i_330_n_0\,
      S(2) => \caracter[0]_i_331_n_0\,
      S(1) => \caracter[0]_i_332_n_0\,
      S(0) => \caracter[0]_i_333_n_0\
    );
\caracter_reg[0]_i_163\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_305_n_0\,
      CO(3) => \caracter_reg[0]_i_163_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_163_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_334_n_0\,
      DI(2) => \caracter[0]_i_335_n_0\,
      DI(1) => \caracter[0]_i_336_n_0\,
      DI(0) => \caracter[0]_i_337_n_0\,
      O(3) => \caracter_reg[0]_i_163_n_4\,
      O(2) => \caracter_reg[0]_i_163_n_5\,
      O(1) => \caracter_reg[0]_i_163_n_6\,
      O(0) => \caracter_reg[0]_i_163_n_7\,
      S(3) => \caracter[0]_i_338_n_0\,
      S(2) => \caracter[0]_i_339_n_0\,
      S(1) => \caracter[0]_i_340_n_0\,
      S(0) => \caracter[0]_i_341_n_0\
    );
\caracter_reg[0]_i_19\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_25_n_0\,
      CO(3 downto 1) => \NLW_caracter_reg[0]_i_19_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \caracter_reg[0]_i_19_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \caracter[0]_i_26_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[0]_i_19_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => B"000",
      S(0) => \caracter[0]_i_27_n_0\
    );
\caracter_reg[0]_i_20\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_28_n_0\,
      CO(3 downto 0) => \NLW_caracter_reg[0]_i_20_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 2) => \NLW_caracter_reg[0]_i_20_O_UNCONNECTED\(3 downto 2),
      O(1 downto 0) => euros3(30 downto 29),
      S(3 downto 2) => B"00",
      S(1) => \caracter_reg[0]_i_21_n_3\,
      S(0) => \caracter_reg[0]_i_21_n_3\
    );
\caracter_reg[0]_i_21\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_29_n_0\,
      CO(3 downto 1) => \NLW_caracter_reg[0]_i_21_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \caracter_reg[0]_i_21_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_caracter_reg[0]_i_21_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
\caracter_reg[0]_i_22\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_30_n_0\,
      CO(3 downto 0) => \NLW_caracter_reg[0]_i_22_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \caracter[0]_i_31_n_0\,
      O(3 downto 2) => \NLW_caracter_reg[0]_i_22_O_UNCONNECTED\(3 downto 2),
      O(1) => \caracter_reg[0]_i_22_n_6\,
      O(0) => \caracter_reg[0]_i_22_n_7\,
      S(3 downto 2) => B"00",
      S(1) => \caracter[0]_i_32_n_0\,
      S(0) => \caracter[0]_i_33_n_0\
    );
\caracter_reg[0]_i_23\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_34_n_0\,
      CO(3) => \caracter_reg[0]_i_23_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_23_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_35_n_0\,
      DI(2) => \caracter[0]_i_36_n_0\,
      DI(1) => \caracter[0]_i_37_n_0\,
      DI(0) => \caracter[0]_i_38_n_0\,
      O(3) => \caracter_reg[0]_i_23_n_4\,
      O(2) => \caracter_reg[0]_i_23_n_5\,
      O(1) => \caracter_reg[0]_i_23_n_6\,
      O(0) => \caracter_reg[0]_i_23_n_7\,
      S(3) => \caracter[0]_i_39_n_0\,
      S(2) => \caracter[0]_i_40_n_0\,
      S(1) => \caracter[0]_i_41_n_0\,
      S(0) => \caracter[0]_i_42_n_0\
    );
\caracter_reg[0]_i_239\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_346_n_0\,
      CO(3) => \caracter_reg[0]_i_239_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_239_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_347_n_0\,
      DI(2) => \caracter[0]_i_348_n_0\,
      DI(1) => \caracter[0]_i_349_n_0\,
      DI(0) => \caracter[0]_i_350_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[0]_i_239_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[0]_i_351_n_0\,
      S(2) => \caracter[0]_i_352_n_0\,
      S(1) => \caracter[0]_i_353_n_0\,
      S(0) => \caracter[0]_i_354_n_0\
    );
\caracter_reg[0]_i_24\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_43_n_0\,
      CO(3) => \caracter_reg[0]_i_24_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_24_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_44_n_0\,
      DI(2) => \caracter[0]_i_45_n_0\,
      DI(1) => \caracter[0]_i_46_n_0\,
      DI(0) => \caracter[0]_i_47_n_0\,
      O(3) => \caracter_reg[0]_i_24_n_4\,
      O(2) => \caracter_reg[0]_i_24_n_5\,
      O(1) => \caracter_reg[0]_i_24_n_6\,
      O(0) => \caracter_reg[0]_i_24_n_7\,
      S(3) => \caracter[0]_i_48_n_0\,
      S(2) => \caracter[0]_i_49_n_0\,
      S(1) => \caracter[0]_i_50_n_0\,
      S(0) => \caracter[0]_i_51_n_0\
    );
\caracter_reg[0]_i_248\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_355_n_0\,
      CO(3) => \caracter_reg[0]_i_248_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_248_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_356_n_0\,
      DI(2) => \caracter[0]_i_357_n_0\,
      DI(1) => \caracter[0]_i_358_n_0\,
      DI(0) => \caracter[0]_i_359_n_0\,
      O(3) => \caracter_reg[0]_i_248_n_4\,
      O(2) => \caracter_reg[0]_i_248_n_5\,
      O(1) => \caracter_reg[0]_i_248_n_6\,
      O(0) => \caracter_reg[0]_i_248_n_7\,
      S(3) => \caracter[0]_i_360_n_0\,
      S(2) => \caracter[0]_i_361_n_0\,
      S(1) => \caracter[0]_i_362_n_0\,
      S(0) => \caracter[0]_i_363_n_0\
    );
\caracter_reg[0]_i_249\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_364_n_0\,
      CO(3) => \caracter_reg[0]_i_249_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_249_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => '1',
      DI(2) => \caracter_reg[3]_i_141_0\(6),
      DI(1) => \caracter[0]_i_365_n_0\,
      DI(0) => \caracter[0]_i_366_n_0\,
      O(3) => \caracter_reg[0]_i_249_n_4\,
      O(2) => \caracter_reg[0]_i_249_n_5\,
      O(1) => \caracter_reg[0]_i_249_n_6\,
      O(0) => \caracter_reg[0]_i_249_n_7\,
      S(3) => \caracter[0]_i_367_n_0\,
      S(2) => \caracter[0]_i_368_n_0\,
      S(1) => \caracter[0]_i_369_n_0\,
      S(0) => \caracter[0]_i_370_n_0\
    );
\caracter_reg[0]_i_25\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_52_n_0\,
      CO(3) => \caracter_reg[0]_i_25_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_25_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_53_n_0\,
      DI(2) => \caracter[0]_i_54_n_0\,
      DI(1) => \caracter[0]_i_55_n_0\,
      DI(0) => \caracter[0]_i_56_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[0]_i_25_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[0]_i_57_n_0\,
      S(2) => \caracter[0]_i_58_n_0\,
      S(1) => \caracter[0]_i_59_n_0\,
      S(0) => \caracter[0]_i_60_n_0\
    );
\caracter_reg[0]_i_250\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_371_n_0\,
      CO(3) => \caracter_reg[0]_i_250_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_250_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => \caracter_reg[3]_i_141_0\(4 downto 1),
      O(3) => \caracter_reg[0]_i_250_n_4\,
      O(2) => \caracter_reg[0]_i_250_n_5\,
      O(1) => \caracter_reg[0]_i_250_n_6\,
      O(0) => \caracter_reg[0]_i_250_n_7\,
      S(3) => \caracter[0]_i_372_n_0\,
      S(2) => \caracter[0]_i_373_n_0\,
      S(1) => \caracter[0]_i_374_n_0\,
      S(0) => \caracter[0]_i_375_n_0\
    );
\caracter_reg[0]_i_257\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_376_n_0\,
      CO(3) => \caracter_reg[0]_i_257_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_257_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_377_n_0\,
      DI(2) => \caracter[0]_i_378_n_0\,
      DI(1) => \caracter[0]_i_379_n_0\,
      DI(0) => \caracter[0]_i_380_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[0]_i_257_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[0]_i_381_n_0\,
      S(2) => \caracter[0]_i_382_n_0\,
      S(1) => \caracter[0]_i_383_n_0\,
      S(0) => \caracter[0]_i_384_n_0\
    );
\caracter_reg[0]_i_266\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_342_n_0\,
      CO(3) => \caracter_reg[0]_i_266_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_266_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => euros3(16 downto 13),
      S(3) => \caracter_reg[0]_i_21_n_3\,
      S(2) => \caracter_reg[0]_i_21_n_3\,
      S(1) => \caracter_reg[0]_i_21_n_3\,
      S(0) => \caracter_reg[0]_i_21_n_3\
    );
\caracter_reg[0]_i_267\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_385_n_0\,
      CO(3) => \caracter_reg[0]_i_267_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_267_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_386_n_0\,
      DI(2) => \caracter[0]_i_387_n_0\,
      DI(1) => \caracter[0]_i_388_n_0\,
      DI(0) => \caracter[0]_i_389_n_0\,
      O(3) => \caracter_reg[0]_i_267_n_4\,
      O(2) => \caracter_reg[0]_i_267_n_5\,
      O(1) => \caracter_reg[0]_i_267_n_6\,
      O(0) => \caracter_reg[0]_i_267_n_7\,
      S(3) => \caracter[0]_i_390_n_0\,
      S(2) => \caracter[0]_i_391_n_0\,
      S(1) => \caracter[0]_i_392_n_0\,
      S(0) => \caracter[0]_i_393_n_0\
    );
\caracter_reg[0]_i_276\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_394_n_0\,
      CO(3) => \caracter_reg[0]_i_276_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_276_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_395_n_0\,
      DI(2) => \caracter[0]_i_396_n_0\,
      DI(1) => \caracter[0]_i_397_n_0\,
      DI(0) => \caracter[0]_i_398_n_0\,
      O(3) => \caracter_reg[0]_i_276_n_4\,
      O(2) => \caracter_reg[0]_i_276_n_5\,
      O(1) => \caracter_reg[0]_i_276_n_6\,
      O(0) => \caracter_reg[0]_i_276_n_7\,
      S(3) => \caracter[0]_i_399_n_0\,
      S(2) => \caracter[0]_i_400_n_0\,
      S(1) => \caracter[0]_i_401_n_0\,
      S(0) => \caracter[0]_i_402_n_0\
    );
\caracter_reg[0]_i_28\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_61_n_0\,
      CO(3) => \caracter_reg[0]_i_28_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_28_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => euros3(28 downto 25),
      S(3) => \caracter_reg[0]_i_21_n_3\,
      S(2) => \caracter_reg[0]_i_21_n_3\,
      S(1) => \caracter_reg[0]_i_21_n_3\,
      S(0) => \caracter_reg[0]_i_21_n_3\
    );
\caracter_reg[0]_i_285\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_403_n_0\,
      CO(3) => \caracter_reg[0]_i_285_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_285_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_405_n_0\,
      DI(2) => \caracter[0]_i_406_n_0\,
      DI(1) => \caracter[0]_i_407_n_0\,
      DI(0) => \caracter[0]_i_408_n_0\,
      O(3) => \caracter_reg[0]_i_285_n_4\,
      O(2) => \caracter_reg[0]_i_285_n_5\,
      O(1) => \caracter_reg[0]_i_285_n_6\,
      O(0) => \caracter_reg[0]_i_285_n_7\,
      S(3) => \caracter[0]_i_409_n_0\,
      S(2) => \caracter[0]_i_410_n_0\,
      S(1) => \caracter[0]_i_411_n_0\,
      S(0) => \caracter[0]_i_412_n_0\
    );
\caracter_reg[0]_i_286\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_285_n_0\,
      CO(3) => \NLW_caracter_reg[0]_i_286_CO_UNCONNECTED\(3),
      CO(2) => \caracter_reg[0]_i_286_n_1\,
      CO(1 downto 0) => \NLW_caracter_reg[0]_i_286_CO_UNCONNECTED\(1 downto 0),
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \caracter[0]_i_413_n_0\,
      O(3 downto 2) => \NLW_caracter_reg[0]_i_286_O_UNCONNECTED\(3 downto 2),
      O(1) => \caracter_reg[0]_i_286_n_6\,
      O(0) => \caracter_reg[0]_i_286_n_7\,
      S(3 downto 2) => B"01",
      S(1) => euros2(30),
      S(0) => \caracter[0]_i_415_n_0\
    );
\caracter_reg[0]_i_287\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_416_n_0\,
      CO(3) => \caracter_reg[0]_i_287_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_287_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \caracter[0]_i_417_n_0\,
      DI(1) => \caracter[0]_i_418_n_0\,
      DI(0) => \caracter[0]_i_419_n_0\,
      O(3) => \NLW_caracter_reg[0]_i_287_O_UNCONNECTED\(3),
      O(2) => \caracter_reg[0]_i_287_n_5\,
      O(1) => \caracter_reg[0]_i_287_n_6\,
      O(0) => \caracter_reg[0]_i_287_n_7\,
      S(3) => '1',
      S(2) => \caracter[0]_i_420_n_0\,
      S(1) => \caracter[0]_i_421_n_0\,
      S(0) => \caracter[0]_i_422_n_0\
    );
\caracter_reg[0]_i_288\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_404_n_0\,
      CO(3) => \NLW_caracter_reg[0]_i_288_CO_UNCONNECTED\(3),
      CO(2) => \caracter_reg[0]_i_288_n_1\,
      CO(1 downto 0) => \NLW_caracter_reg[0]_i_288_CO_UNCONNECTED\(1 downto 0),
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \caracter[0]_i_423_n_0\,
      DI(0) => euros2(29),
      O(3 downto 2) => \NLW_caracter_reg[0]_i_288_O_UNCONNECTED\(3 downto 2),
      O(1) => \caracter_reg[0]_i_288_n_6\,
      O(0) => \caracter_reg[0]_i_288_n_7\,
      S(3 downto 2) => B"01",
      S(1) => \caracter[0]_i_425_n_0\,
      S(0) => \caracter[0]_i_426_n_0\
    );
\caracter_reg[0]_i_289\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_427_n_0\,
      CO(3) => \caracter_reg[0]_i_289_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_289_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_428_n_0\,
      DI(2) => \caracter[0]_i_429_n_0\,
      DI(1) => \caracter[0]_i_430_n_0\,
      DI(0) => \caracter[0]_i_431_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[0]_i_289_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[0]_i_432_n_0\,
      S(2) => \caracter[0]_i_433_n_0\,
      S(1) => \caracter[0]_i_434_n_0\,
      S(0) => \caracter[0]_i_435_n_0\
    );
\caracter_reg[0]_i_29\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_62_n_0\,
      CO(3) => \caracter_reg[0]_i_29_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_29_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => \caracter_reg[3]_i_141_0\(7 downto 4),
      O(3) => \caracter_reg[0]_i_29_n_4\,
      O(2) => \caracter_reg[0]_i_29_n_5\,
      O(1) => \caracter_reg[0]_i_29_n_6\,
      O(0) => \caracter_reg[0]_i_29_n_7\,
      S(3) => \caracter[0]_i_63_n_0\,
      S(2) => \caracter[0]_i_64_n_0\,
      S(1) => \caracter[0]_i_65_n_0\,
      S(0) => \caracter[0]_i_66_n_0\
    );
\caracter_reg[0]_i_298\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[0]_i_298_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_298_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => euros2(6),
      DI(2) => \caracter[0]_i_445_n_0\,
      DI(1) => \caracter[0]_i_446_n_0\,
      DI(0) => '0',
      O(3) => \caracter_reg[0]_i_298_n_4\,
      O(2) => \caracter_reg[0]_i_298_n_5\,
      O(1) => \caracter_reg[0]_i_298_n_6\,
      O(0) => \caracter_reg[0]_i_298_n_7\,
      S(3) => \caracter[0]_i_447_n_0\,
      S(2) => \caracter[0]_i_448_n_0\,
      S(1) => \caracter[0]_i_449_n_0\,
      S(0) => \caracter[0]_i_450_n_0\
    );
\caracter_reg[0]_i_30\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_67_n_0\,
      CO(3) => \caracter_reg[0]_i_30_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_30_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_68_n_0\,
      DI(2) => \caracter[0]_i_69_n_0\,
      DI(1) => \caracter[0]_i_70_n_0\,
      DI(0) => \caracter[0]_i_71_n_0\,
      O(3) => \caracter_reg[0]_i_30_n_4\,
      O(2) => \caracter_reg[0]_i_30_n_5\,
      O(1) => \caracter_reg[0]_i_30_n_6\,
      O(0) => \caracter_reg[0]_i_30_n_7\,
      S(3) => \caracter[0]_i_72_n_0\,
      S(2) => \caracter[0]_i_73_n_0\,
      S(1) => \caracter[0]_i_74_n_0\,
      S(0) => \caracter[0]_i_75_n_0\
    );
\caracter_reg[0]_i_301\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_441_n_0\,
      CO(3) => \caracter_reg[0]_i_301_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_301_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_451_n_0\,
      DI(2) => \caracter[0]_i_452_n_0\,
      DI(1) => \caracter[0]_i_453_n_0\,
      DI(0) => \caracter[0]_i_454_n_0\,
      O(3) => \caracter_reg[0]_i_301_n_4\,
      O(2) => \caracter_reg[0]_i_301_n_5\,
      O(1) => \caracter_reg[0]_i_301_n_6\,
      O(0) => \caracter_reg[0]_i_301_n_7\,
      S(3) => \caracter[0]_i_455_n_0\,
      S(2) => \caracter[0]_i_456_n_0\,
      S(1) => \caracter[0]_i_457_n_0\,
      S(0) => \caracter[0]_i_458_n_0\
    );
\caracter_reg[0]_i_303\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_443_n_0\,
      CO(3) => \caracter_reg[0]_i_303_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_303_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_459_n_0\,
      DI(2) => \caracter[0]_i_460_n_0\,
      DI(1) => \caracter[0]_i_461_n_0\,
      DI(0) => \caracter[0]_i_462_n_0\,
      O(3) => \caracter_reg[0]_i_303_n_4\,
      O(2) => \caracter_reg[0]_i_303_n_5\,
      O(1) => \caracter_reg[0]_i_303_n_6\,
      O(0) => \caracter_reg[0]_i_303_n_7\,
      S(3) => \caracter[0]_i_463_n_0\,
      S(2) => \caracter[0]_i_464_n_0\,
      S(1) => \caracter[0]_i_465_n_0\,
      S(0) => \caracter[0]_i_466_n_0\
    );
\caracter_reg[0]_i_305\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_442_n_0\,
      CO(3) => \caracter_reg[0]_i_305_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_305_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_467_n_0\,
      DI(2) => \caracter[0]_i_468_n_0\,
      DI(1) => \caracter[0]_i_469_n_0\,
      DI(0) => \caracter[0]_i_470_n_0\,
      O(3) => \caracter_reg[0]_i_305_n_4\,
      O(2) => \caracter_reg[0]_i_305_n_5\,
      O(1) => \caracter_reg[0]_i_305_n_6\,
      O(0) => \caracter_reg[0]_i_305_n_7\,
      S(3) => \caracter[0]_i_471_n_0\,
      S(2) => \caracter[0]_i_472_n_0\,
      S(1) => \caracter[0]_i_473_n_0\,
      S(0) => \caracter[0]_i_474_n_0\
    );
\caracter_reg[0]_i_34\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_78_n_0\,
      CO(3) => \caracter_reg[0]_i_34_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_34_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_79_n_0\,
      DI(2) => \caracter[0]_i_80_n_0\,
      DI(1) => \caracter[0]_i_81_n_0\,
      DI(0) => \caracter[0]_i_82_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[0]_i_34_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[0]_i_83_n_0\,
      S(2) => \caracter[0]_i_84_n_0\,
      S(1) => \caracter[0]_i_85_n_0\,
      S(0) => \caracter[0]_i_86_n_0\
    );
\caracter_reg[0]_i_342\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_343_n_0\,
      CO(3) => \caracter_reg[0]_i_342_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_342_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => euros3(12 downto 9),
      S(3) => \caracter_reg[0]_i_21_n_3\,
      S(2) => \caracter_reg[0]_i_21_n_3\,
      S(1) => \caracter_reg[0]_i_21_n_3\,
      S(0) => \caracter_reg[0]_i_21_n_3\
    );
\caracter_reg[0]_i_343\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_344_n_0\,
      CO(3) => \caracter_reg[0]_i_343_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_343_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => euros3(8 downto 5),
      S(3) => \caracter_reg[0]_i_21_n_3\,
      S(2) => \caracter[0]_i_478_n_0\,
      S(1) => \caracter[0]_i_479_n_0\,
      S(0) => \caracter[0]_i_480_n_0\
    );
\caracter_reg[0]_i_344\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[0]_i_344_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_344_CO_UNCONNECTED\(2 downto 0),
      CYINIT => \caracter[0]_i_481_n_0\,
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => euros3(4 downto 1),
      S(3) => \caracter[0]_i_482_n_0\,
      S(2) => \caracter[0]_i_483_n_0\,
      S(1) => \caracter[0]_i_484_n_0\,
      S(0) => \caracter[0]_i_485_n_0\
    );
\caracter_reg[0]_i_346\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_486_n_0\,
      CO(3) => \caracter_reg[0]_i_346_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_346_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_487_n_0\,
      DI(2) => \caracter[0]_i_488_n_0\,
      DI(1) => \caracter[0]_i_489_n_0\,
      DI(0) => \caracter[0]_i_490_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[0]_i_346_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[0]_i_491_n_0\,
      S(2) => \caracter[0]_i_492_n_0\,
      S(1) => \caracter[0]_i_493_n_0\,
      S(0) => \caracter[0]_i_494_n_0\
    );
\caracter_reg[0]_i_355\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[0]_i_355_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_355_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 1) => \caracter_reg[3]_i_141_0\(6 downto 4),
      DI(0) => '0',
      O(3) => \caracter_reg[0]_i_355_n_4\,
      O(2) => \caracter_reg[0]_i_355_n_5\,
      O(1) => \caracter_reg[0]_i_355_n_6\,
      O(0) => \caracter_reg[0]_i_355_n_7\,
      S(3) => \caracter[0]_i_499_n_0\,
      S(2) => \caracter[0]_i_500_n_0\,
      S(1) => \caracter[0]_i_501_n_0\,
      S(0) => \caracter_reg[3]_i_141_0\(3)
    );
\caracter_reg[0]_i_364\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_502_n_0\,
      CO(3) => \caracter_reg[0]_i_364_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_364_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_503_n_0\,
      DI(2) => \caracter[0]_i_504_n_0\,
      DI(1) => \caracter[0]_i_505_n_0\,
      DI(0) => \caracter[0]_i_506_n_0\,
      O(3) => \caracter_reg[0]_i_364_n_4\,
      O(2) => \caracter_reg[0]_i_364_n_5\,
      O(1) => \caracter_reg[0]_i_364_n_6\,
      O(0) => \caracter_reg[0]_i_364_n_7\,
      S(3) => \caracter[0]_i_507_n_0\,
      S(2) => \caracter[0]_i_508_n_0\,
      S(1) => \caracter[0]_i_509_n_0\,
      S(0) => \caracter[0]_i_510_n_0\
    );
\caracter_reg[0]_i_371\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[0]_i_371_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_371_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter_reg[3]_i_141_0\(0),
      DI(2 downto 0) => B"001",
      O(3) => \caracter_reg[0]_i_371_n_4\,
      O(2) => \caracter_reg[0]_i_371_n_5\,
      O(1) => \caracter_reg[0]_i_371_n_6\,
      O(0) => \NLW_caracter_reg[0]_i_371_O_UNCONNECTED\(0),
      S(3) => \caracter[0]_i_511_n_0\,
      S(2) => \caracter[0]_i_512_n_0\,
      S(1) => \caracter[0]_i_513_n_0\,
      S(0) => \caracter_reg[3]_i_141_0\(0)
    );
\caracter_reg[0]_i_376\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_514_n_0\,
      CO(3) => \caracter_reg[0]_i_376_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_376_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_515_n_0\,
      DI(2) => \caracter[0]_i_516_n_0\,
      DI(1) => \caracter[0]_i_517_n_0\,
      DI(0) => \caracter[0]_i_518_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[0]_i_376_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[0]_i_519_n_0\,
      S(2) => \caracter[0]_i_520_n_0\,
      S(1) => \caracter[0]_i_521_n_0\,
      S(0) => \caracter[0]_i_522_n_0\
    );
\caracter_reg[0]_i_385\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_523_n_0\,
      CO(3) => \caracter_reg[0]_i_385_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_385_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_524_n_0\,
      DI(2) => \caracter[0]_i_525_n_0\,
      DI(1) => \caracter[0]_i_526_n_0\,
      DI(0) => \caracter[0]_i_527_n_0\,
      O(3) => \caracter_reg[0]_i_385_n_4\,
      O(2) => \caracter_reg[0]_i_385_n_5\,
      O(1) => \caracter_reg[0]_i_385_n_6\,
      O(0) => \caracter_reg[0]_i_385_n_7\,
      S(3) => \caracter[0]_i_528_n_0\,
      S(2) => \caracter[0]_i_529_n_0\,
      S(1) => \caracter[0]_i_530_n_0\,
      S(0) => \caracter[0]_i_531_n_0\
    );
\caracter_reg[0]_i_394\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_532_n_0\,
      CO(3) => \caracter_reg[0]_i_394_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_394_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_533_n_0\,
      DI(2) => \caracter[0]_i_534_n_0\,
      DI(1) => \caracter[0]_i_535_n_0\,
      DI(0) => \caracter[0]_i_536_n_0\,
      O(3) => \caracter_reg[0]_i_394_n_4\,
      O(2) => \caracter_reg[0]_i_394_n_5\,
      O(1) => \caracter_reg[0]_i_394_n_6\,
      O(0) => \caracter_reg[0]_i_394_n_7\,
      S(3) => \caracter[0]_i_537_n_0\,
      S(2) => \caracter[0]_i_538_n_0\,
      S(1) => \caracter[0]_i_539_n_0\,
      S(0) => \caracter[0]_i_540_n_0\
    );
\caracter_reg[0]_i_403\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_541_n_0\,
      CO(3) => \caracter_reg[0]_i_403_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_403_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_546_n_0\,
      DI(2) => \caracter[0]_i_547_n_0\,
      DI(1) => \caracter[0]_i_548_n_0\,
      DI(0) => \caracter[0]_i_549_n_0\,
      O(3) => \caracter_reg[0]_i_403_n_4\,
      O(2) => \caracter_reg[0]_i_403_n_5\,
      O(1) => \caracter_reg[0]_i_403_n_6\,
      O(0) => \caracter_reg[0]_i_403_n_7\,
      S(3) => \caracter[0]_i_550_n_0\,
      S(2) => \caracter[0]_i_551_n_0\,
      S(1) => \caracter[0]_i_552_n_0\,
      S(0) => \caracter[0]_i_553_n_0\
    );
\caracter_reg[0]_i_404\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_542_n_0\,
      CO(3) => \caracter_reg[0]_i_404_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_404_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => euros2(28 downto 25),
      O(3) => \caracter_reg[0]_i_404_n_4\,
      O(2) => \caracter_reg[0]_i_404_n_5\,
      O(1) => \caracter_reg[0]_i_404_n_6\,
      O(0) => \caracter_reg[0]_i_404_n_7\,
      S(3) => \caracter[0]_i_558_n_0\,
      S(2) => \caracter[0]_i_559_n_0\,
      S(1) => \caracter[0]_i_560_n_0\,
      S(0) => \caracter[0]_i_561_n_0\
    );
\caracter_reg[0]_i_416\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_99_n_0\,
      CO(3) => \caracter_reg[0]_i_416_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_416_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_562_n_0\,
      DI(2) => \caracter[0]_i_563_n_0\,
      DI(1) => \caracter[0]_i_564_n_0\,
      DI(0) => \caracter[0]_i_565_n_0\,
      O(3) => \caracter_reg[0]_i_416_n_4\,
      O(2) => \caracter_reg[0]_i_416_n_5\,
      O(1) => \caracter_reg[0]_i_416_n_6\,
      O(0) => \caracter_reg[0]_i_416_n_7\,
      S(3) => \caracter[0]_i_566_n_0\,
      S(2) => \caracter[0]_i_567_n_0\,
      S(1) => \caracter[0]_i_568_n_0\,
      S(0) => \caracter[0]_i_569_n_0\
    );
\caracter_reg[0]_i_427\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[0]_i_427_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_427_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_570_n_0\,
      DI(2) => \caracter[0]_i_571_n_0\,
      DI(1) => \caracter[0]_i_572_n_0\,
      DI(0) => \caracter[0]_i_573_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[0]_i_427_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[0]_i_574_n_0\,
      S(2) => \caracter[0]_i_575_n_0\,
      S(1) => \caracter[0]_i_576_n_0\,
      S(0) => \caracter[0]_i_577_n_0\
    );
\caracter_reg[0]_i_43\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_103_n_0\,
      CO(3) => \caracter_reg[0]_i_43_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_43_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_104_n_0\,
      DI(2) => \caracter[0]_i_105_n_0\,
      DI(1) => \caracter[0]_i_106_n_0\,
      DI(0) => \caracter[0]_i_107_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[0]_i_43_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[0]_i_108_n_0\,
      S(2) => \caracter[0]_i_109_n_0\,
      S(1) => \caracter[0]_i_110_n_0\,
      S(0) => \caracter[0]_i_111_n_0\
    );
\caracter_reg[0]_i_441\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[0]_i_441_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_441_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => euros2(0),
      DI(2 downto 0) => B"001",
      O(3) => \caracter_reg[0]_i_441_n_4\,
      O(2) => \caracter_reg[0]_i_441_n_5\,
      O(1) => \caracter_reg[0]_i_441_n_6\,
      O(0) => \NLW_caracter_reg[0]_i_441_O_UNCONNECTED\(0),
      S(3) => \caracter[0]_i_580_n_0\,
      S(2) => \caracter[0]_i_581_n_0\,
      S(1) => \caracter[0]_i_582_n_0\,
      S(0) => euros2(0)
    );
\caracter_reg[0]_i_442\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_578_n_0\,
      CO(3) => \caracter_reg[0]_i_442_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_442_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_583_n_0\,
      DI(2) => \caracter[0]_i_584_n_0\,
      DI(1) => \caracter[0]_i_585_n_0\,
      DI(0) => \caracter[0]_i_586_n_0\,
      O(3) => \caracter_reg[0]_i_442_n_4\,
      O(2) => \caracter_reg[0]_i_442_n_5\,
      O(1) => \caracter_reg[0]_i_442_n_6\,
      O(0) => \caracter_reg[0]_i_442_n_7\,
      S(3) => \caracter[0]_i_587_n_0\,
      S(2) => \caracter[0]_i_588_n_0\,
      S(1) => \caracter[0]_i_589_n_0\,
      S(0) => \caracter[0]_i_590_n_0\
    );
\caracter_reg[0]_i_443\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_591_n_0\,
      CO(3) => \caracter_reg[0]_i_443_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_443_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_592_n_0\,
      DI(2) => \caracter[0]_i_593_n_0\,
      DI(1) => \caracter[0]_i_594_n_0\,
      DI(0) => \caracter[0]_i_595_n_0\,
      O(3) => \caracter_reg[0]_i_443_n_4\,
      O(2) => \caracter_reg[0]_i_443_n_5\,
      O(1) => \caracter_reg[0]_i_443_n_6\,
      O(0) => \caracter_reg[0]_i_443_n_7\,
      S(3) => \caracter[0]_i_596_n_0\,
      S(2) => \caracter[0]_i_597_n_0\,
      S(1) => \caracter[0]_i_598_n_0\,
      S(0) => \caracter[0]_i_599_n_0\
    );
\caracter_reg[0]_i_486\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[0]_i_486_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_486_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_600_n_0\,
      DI(2) => \caracter[0]_i_601_n_0\,
      DI(1) => \caracter[0]_i_602_n_0\,
      DI(0) => \caracter[0]_i_603_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[0]_i_486_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[0]_i_604_n_0\,
      S(2) => \caracter[0]_i_605_n_0\,
      S(1) => \caracter[0]_i_606_n_0\,
      S(0) => \caracter[0]_i_607_n_0\
    );
\caracter_reg[0]_i_502\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[0]_i_502_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_502_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_611_n_0\,
      DI(2) => \caracter_reg[3]_i_141_0\(0),
      DI(1 downto 0) => B"01",
      O(3) => \caracter_reg[0]_i_502_n_4\,
      O(2) => \caracter_reg[0]_i_502_n_5\,
      O(1) => \caracter_reg[0]_i_502_n_6\,
      O(0) => \caracter_reg[0]_i_502_n_7\,
      S(3) => \caracter[0]_i_612_n_0\,
      S(2) => \caracter[0]_i_613_n_0\,
      S(1) => \caracter[0]_i_614_n_0\,
      S(0) => \caracter_reg[3]_i_141_0\(0)
    );
\caracter_reg[0]_i_514\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_615_n_0\,
      CO(3) => \caracter_reg[0]_i_514_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_514_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_616_n_0\,
      DI(2) => \caracter[0]_i_617_n_0\,
      DI(1) => \caracter[0]_i_618_n_0\,
      DI(0) => \caracter[0]_i_619_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[0]_i_514_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[0]_i_620_n_0\,
      S(2) => \caracter[0]_i_621_n_0\,
      S(1) => \caracter[0]_i_622_n_0\,
      S(0) => \caracter[0]_i_623_n_0\
    );
\caracter_reg[0]_i_52\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_115_n_0\,
      CO(3) => \caracter_reg[0]_i_52_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_52_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_116_n_0\,
      DI(2) => \caracter[0]_i_117_n_0\,
      DI(1) => \caracter[0]_i_118_n_0\,
      DI(0) => \caracter[0]_i_119_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[0]_i_52_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[0]_i_120_n_0\,
      S(2) => \caracter[0]_i_121_n_0\,
      S(1) => \caracter[0]_i_122_n_0\,
      S(0) => \caracter[0]_i_123_n_0\
    );
\caracter_reg[0]_i_523\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[0]_i_523_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_523_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_624_n_0\,
      DI(2) => \caracter[0]_i_625_n_0\,
      DI(1) => \caracter[0]_i_626_n_0\,
      DI(0) => '0',
      O(3) => \caracter_reg[0]_i_523_n_4\,
      O(2) => \caracter_reg[0]_i_523_n_5\,
      O(1) => \caracter_reg[0]_i_523_n_6\,
      O(0) => \caracter_reg[0]_i_523_n_7\,
      S(3) => \caracter[0]_i_627_n_0\,
      S(2) => \caracter[0]_i_628_n_0\,
      S(1) => \caracter[0]_i_629_n_0\,
      S(0) => \caracter[0]_i_630_n_0\
    );
\caracter_reg[0]_i_532\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_23_n_0\,
      CO(3) => \caracter_reg[0]_i_532_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_532_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_631_n_0\,
      DI(2) => \caracter[0]_i_632_n_0\,
      DI(1) => \caracter[0]_i_633_n_0\,
      DI(0) => \caracter[0]_i_634_n_0\,
      O(3) => \caracter_reg[0]_i_532_n_4\,
      O(2) => \caracter_reg[0]_i_532_n_5\,
      O(1) => \caracter_reg[0]_i_532_n_6\,
      O(0) => \caracter_reg[0]_i_532_n_7\,
      S(3) => \caracter[0]_i_635_n_0\,
      S(2) => \caracter[0]_i_636_n_0\,
      S(1) => \caracter[0]_i_637_n_0\,
      S(0) => \caracter[0]_i_638_n_0\
    );
\caracter_reg[0]_i_541\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_639_n_0\,
      CO(3) => \caracter_reg[0]_i_541_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_541_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_650_n_0\,
      DI(2) => \caracter[0]_i_651_n_0\,
      DI(1) => \caracter[0]_i_652_n_0\,
      DI(0) => \caracter[0]_i_653_n_0\,
      O(3) => \caracter_reg[0]_i_541_n_4\,
      O(2) => \caracter_reg[0]_i_541_n_5\,
      O(1) => \caracter_reg[0]_i_541_n_6\,
      O(0) => \caracter_reg[0]_i_541_n_7\,
      S(3) => \caracter[0]_i_654_n_0\,
      S(2) => \caracter[0]_i_655_n_0\,
      S(1) => \caracter[0]_i_656_n_0\,
      S(0) => \caracter[0]_i_657_n_0\
    );
\caracter_reg[0]_i_542\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_642_n_0\,
      CO(3) => \caracter_reg[0]_i_542_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_542_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => euros2(24 downto 21),
      O(3) => \caracter_reg[0]_i_542_n_4\,
      O(2) => \caracter_reg[0]_i_542_n_5\,
      O(1) => \caracter_reg[0]_i_542_n_6\,
      O(0) => \caracter_reg[0]_i_542_n_7\,
      S(3) => \caracter[0]_i_662_n_0\,
      S(2) => \caracter[0]_i_663_n_0\,
      S(1) => \caracter[0]_i_664_n_0\,
      S(0) => \caracter[0]_i_665_n_0\
    );
\caracter_reg[0]_i_578\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_666_n_0\,
      CO(3) => \caracter_reg[0]_i_578_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_578_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_667_n_0\,
      DI(2) => \caracter[0]_i_668_n_0\,
      DI(1) => \caracter[0]_i_669_n_0\,
      DI(0) => \caracter[0]_i_670_n_0\,
      O(3) => \caracter_reg[0]_i_578_n_4\,
      O(2) => \caracter_reg[0]_i_578_n_5\,
      O(1) => \caracter_reg[0]_i_578_n_6\,
      O(0) => \caracter_reg[0]_i_578_n_7\,
      S(3) => \caracter[0]_i_671_n_0\,
      S(2) => \caracter[0]_i_672_n_0\,
      S(1) => \caracter[0]_i_673_n_0\,
      S(0) => \caracter[0]_i_674_n_0\
    );
\caracter_reg[0]_i_591\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[0]_i_591_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_591_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_481_n_0\,
      DI(2) => euros2(0),
      DI(1 downto 0) => B"01",
      O(3) => \caracter_reg[0]_i_591_n_4\,
      O(2) => \caracter_reg[0]_i_591_n_5\,
      O(1) => \caracter_reg[0]_i_591_n_6\,
      O(0) => \caracter_reg[0]_i_591_n_7\,
      S(3) => \caracter[0]_i_675_n_0\,
      S(2) => \caracter[0]_i_676_n_0\,
      S(1) => \caracter[0]_i_677_n_0\,
      S(0) => euros2(0)
    );
\caracter_reg[0]_i_608\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_609_n_0\,
      CO(3) => \NLW_caracter_reg[0]_i_608_CO_UNCONNECTED\(3),
      CO(2) => \caracter_reg[0]_i_608_n_1\,
      CO(1 downto 0) => \NLW_caracter_reg[0]_i_608_CO_UNCONNECTED\(1 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 2) => \NLW_caracter_reg[0]_i_608_O_UNCONNECTED\(3 downto 2),
      O(1) => \caracter_reg[0]_i_608_n_6\,
      O(0) => \caracter_reg[0]_i_608_n_7\,
      S(3 downto 2) => B"01",
      S(1 downto 0) => \caracter_reg[3]_i_141_0\(7 downto 6)
    );
\caracter_reg[0]_i_609\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_678_n_0\,
      CO(3) => \caracter_reg[0]_i_609_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_609_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \caracter_reg[3]_i_141_0\(3),
      DI(0) => \caracter[0]_i_679_n_0\,
      O(3) => \caracter_reg[0]_i_609_n_4\,
      O(2) => \caracter_reg[0]_i_609_n_5\,
      O(1) => \caracter_reg[0]_i_609_n_6\,
      O(0) => \caracter_reg[0]_i_609_n_7\,
      S(3 downto 2) => \caracter_reg[3]_i_141_0\(5 downto 4),
      S(1) => \caracter[0]_i_680_n_0\,
      S(0) => \caracter[0]_i_681_n_0\
    );
\caracter_reg[0]_i_61\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_124_n_0\,
      CO(3) => \caracter_reg[0]_i_61_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_61_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => euros3(24 downto 21),
      S(3) => \caracter_reg[0]_i_21_n_3\,
      S(2) => \caracter_reg[0]_i_21_n_3\,
      S(1) => \caracter_reg[0]_i_21_n_3\,
      S(0) => \caracter_reg[0]_i_21_n_3\
    );
\caracter_reg[0]_i_615\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[0]_i_615_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_615_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_682_n_0\,
      DI(2) => \caracter[0]_i_683_n_0\,
      DI(1) => \caracter[0]_i_684_n_0\,
      DI(0) => '0',
      O(3 downto 0) => \NLW_caracter_reg[0]_i_615_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[0]_i_685_n_0\,
      S(2) => \caracter[0]_i_686_n_0\,
      S(1) => \caracter[0]_i_687_n_0\,
      S(0) => \caracter[0]_i_688_n_0\
    );
\caracter_reg[0]_i_62\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[0]_i_62_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_62_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '1',
      DI(3 downto 0) => \caracter_reg[3]_i_141_0\(3 downto 0),
      O(3) => \caracter_reg[0]_i_62_n_4\,
      O(2) => \caracter_reg[0]_i_62_n_5\,
      O(1) => \caracter_reg[0]_i_62_n_6\,
      O(0) => euros2(0),
      S(3) => \caracter[0]_i_125_n_0\,
      S(2) => \caracter[0]_i_126_n_0\,
      S(1) => \caracter[0]_i_127_n_0\,
      S(0) => \caracter[0]_i_128_n_0\
    );
\caracter_reg[0]_i_639\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_100_n_0\,
      CO(3) => \caracter_reg[0]_i_639_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_639_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_696_n_0\,
      DI(2) => \caracter[0]_i_697_n_0\,
      DI(1) => \caracter[0]_i_698_n_0\,
      DI(0) => \caracter[0]_i_699_n_0\,
      O(3) => \caracter_reg[0]_i_639_n_4\,
      O(2) => \caracter_reg[0]_i_639_n_5\,
      O(1) => \caracter_reg[0]_i_639_n_6\,
      O(0) => \caracter_reg[0]_i_639_n_7\,
      S(3) => \caracter[0]_i_700_n_0\,
      S(2) => \caracter[0]_i_701_n_0\,
      S(1) => \caracter[0]_i_702_n_0\,
      S(0) => \caracter[0]_i_703_n_0\
    );
\caracter_reg[0]_i_640\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_644_n_0\,
      CO(3 downto 2) => \NLW_caracter_reg[0]_i_640_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \caracter_reg[0]_i_640_n_2\,
      CO(0) => \NLW_caracter_reg[0]_i_640_CO_UNCONNECTED\(0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_caracter_reg[0]_i_640_O_UNCONNECTED\(3 downto 1),
      O(0) => \caracter_reg[0]_i_640_n_7\,
      S(3 downto 1) => B"001",
      S(0) => \caracter[0]_i_704_n_0\
    );
\caracter_reg[0]_i_642\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_89_n_0\,
      CO(3) => \caracter_reg[0]_i_642_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_642_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => euros2(20 downto 17),
      O(3) => \caracter_reg[0]_i_642_n_4\,
      O(2) => \caracter_reg[0]_i_642_n_5\,
      O(1) => \caracter_reg[0]_i_642_n_6\,
      O(0) => \caracter_reg[0]_i_642_n_7\,
      S(3) => \caracter[0]_i_709_n_0\,
      S(2) => \caracter[0]_i_710_n_0\,
      S(1) => \caracter[0]_i_711_n_0\,
      S(0) => \caracter[0]_i_712_n_0\
    );
\caracter_reg[0]_i_644\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_90_n_0\,
      CO(3) => \caracter_reg[0]_i_644_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_644_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \caracter[0]_i_713_n_0\,
      O(3) => \caracter_reg[0]_i_644_n_4\,
      O(2) => \caracter_reg[0]_i_644_n_5\,
      O(1) => \caracter_reg[0]_i_644_n_6\,
      O(0) => \caracter_reg[0]_i_644_n_7\,
      S(3) => \caracter[0]_i_714_n_0\,
      S(2) => \caracter[0]_i_715_n_0\,
      S(1) => \caracter[0]_i_716_n_0\,
      S(0) => \caracter[0]_i_717_n_0\
    );
\caracter_reg[0]_i_666\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_718_n_0\,
      CO(3) => \caracter_reg[0]_i_666_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_666_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_719_n_0\,
      DI(2) => euros2(7),
      DI(1) => \caracter[0]_i_721_n_0\,
      DI(0) => \caracter[0]_i_722_n_0\,
      O(3) => \caracter_reg[0]_i_666_n_4\,
      O(2 downto 0) => \NLW_caracter_reg[0]_i_666_O_UNCONNECTED\(2 downto 0),
      S(3) => \caracter[0]_i_723_n_0\,
      S(2) => \caracter[0]_i_724_n_0\,
      S(1) => \caracter[0]_i_725_n_0\,
      S(0) => \caracter[0]_i_726_n_0\
    );
\caracter_reg[0]_i_67\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_129_n_0\,
      CO(3) => \caracter_reg[0]_i_67_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_67_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_130_n_0\,
      DI(2) => \caracter[0]_i_131_n_0\,
      DI(1) => \caracter[0]_i_132_n_0\,
      DI(0) => \caracter[0]_i_133_n_0\,
      O(3) => \caracter_reg[0]_i_67_n_4\,
      O(2) => \caracter_reg[0]_i_67_n_5\,
      O(1) => \caracter_reg[0]_i_67_n_6\,
      O(0) => \caracter_reg[0]_i_67_n_7\,
      S(3) => \caracter[0]_i_134_n_0\,
      S(2) => \caracter[0]_i_135_n_0\,
      S(1) => \caracter[0]_i_136_n_0\,
      S(0) => \caracter[0]_i_137_n_0\
    );
\caracter_reg[0]_i_678\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_727_n_0\,
      CO(3) => \caracter_reg[0]_i_678_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_678_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_728_n_0\,
      DI(2 downto 0) => \caracter_reg[3]_i_141_0\(7 downto 5),
      O(3) => \caracter_reg[0]_i_678_n_4\,
      O(2 downto 0) => \NLW_caracter_reg[0]_i_678_O_UNCONNECTED\(2 downto 0),
      S(3) => \caracter[0]_i_729_n_0\,
      S(2) => \caracter[0]_i_730_n_0\,
      S(1) => \caracter[0]_i_731_n_0\,
      S(0) => \caracter[0]_i_732_n_0\
    );
\caracter_reg[0]_i_718\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[0]_i_718_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_718_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_733_n_0\,
      DI(2) => \caracter[0]_i_734_n_0\,
      DI(1) => \caracter[0]_i_735_n_0\,
      DI(0) => '0',
      O(3 downto 0) => \NLW_caracter_reg[0]_i_718_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[0]_i_736_n_0\,
      S(2) => \caracter[0]_i_737_n_0\,
      S(1) => \caracter[0]_i_738_n_0\,
      S(0) => \caracter[0]_i_739_n_0\
    );
\caracter_reg[0]_i_727\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[0]_i_727_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_727_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 1) => \caracter_reg[3]_i_141_0\(4 downto 2),
      DI(0) => '0',
      O(3 downto 0) => \NLW_caracter_reg[0]_i_727_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[0]_i_741_n_0\,
      S(2) => \caracter[0]_i_742_n_0\,
      S(1) => \caracter[0]_i_743_n_0\,
      S(0) => \caracter_reg[3]_i_141_0\(1)
    );
\caracter_reg[0]_i_76\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_138_n_0\,
      CO(3) => \caracter_reg[0]_i_76_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_76_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_139_n_0\,
      DI(2) => \caracter[0]_i_140_n_0\,
      DI(1) => \caracter[0]_i_141_n_0\,
      DI(0) => \caracter[0]_i_142_n_0\,
      O(3) => \caracter_reg[0]_i_76_n_4\,
      O(2) => \caracter_reg[0]_i_76_n_5\,
      O(1) => \caracter_reg[0]_i_76_n_6\,
      O(0) => \caracter_reg[0]_i_76_n_7\,
      S(3) => \caracter[0]_i_143_n_0\,
      S(2) => \caracter[0]_i_144_n_0\,
      S(1) => \caracter[0]_i_145_n_0\,
      S(0) => \caracter[0]_i_146_n_0\
    );
\caracter_reg[0]_i_77\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_76_n_0\,
      CO(3 downto 0) => \NLW_caracter_reg[0]_i_77_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_caracter_reg[0]_i_77_O_UNCONNECTED\(3 downto 1),
      O(0) => \caracter_reg[0]_i_77_n_7\,
      S(3 downto 1) => B"000",
      S(0) => \caracter[0]_i_147_n_0\
    );
\caracter_reg[0]_i_78\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_148_n_0\,
      CO(3) => \caracter_reg[0]_i_78_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_78_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_149_n_0\,
      DI(2) => \caracter[0]_i_150_n_0\,
      DI(1) => \caracter[0]_i_151_n_0\,
      DI(0) => \caracter[0]_i_152_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[0]_i_78_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[0]_i_153_n_0\,
      S(2) => \caracter[0]_i_154_n_0\,
      S(1) => \caracter[0]_i_155_n_0\,
      S(0) => \caracter[0]_i_156_n_0\
    );
\caracter_reg[0]_i_87\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_157_n_0\,
      CO(3) => \caracter_reg[0]_i_87_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_87_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_167_n_0\,
      DI(2) => \caracter[0]_i_168_n_0\,
      DI(1) => \caracter[0]_i_169_n_0\,
      DI(0) => \caracter[0]_i_170_n_0\,
      O(3) => \caracter_reg[0]_i_87_n_4\,
      O(2) => \caracter_reg[0]_i_87_n_5\,
      O(1) => \caracter_reg[0]_i_87_n_6\,
      O(0) => \caracter_reg[0]_i_87_n_7\,
      S(3) => \caracter[0]_i_171_n_0\,
      S(2) => \caracter[0]_i_172_n_0\,
      S(1) => \caracter[0]_i_173_n_0\,
      S(0) => \caracter[0]_i_174_n_0\
    );
\caracter_reg[0]_i_88\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_97_n_0\,
      CO(3) => \caracter_reg[0]_i_88_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_88_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_175_n_0\,
      DI(2) => \caracter[0]_i_176_n_0\,
      DI(1) => \caracter[0]_i_177_n_0\,
      DI(0) => \caracter[0]_i_178_n_0\,
      O(3) => \caracter_reg[0]_i_88_n_4\,
      O(2) => \caracter_reg[0]_i_88_n_5\,
      O(1) => \caracter_reg[0]_i_88_n_6\,
      O(0) => \caracter_reg[0]_i_88_n_7\,
      S(3) => \caracter[0]_i_179_n_0\,
      S(2) => \caracter[0]_i_180_n_0\,
      S(1) => \caracter[0]_i_181_n_0\,
      S(0) => \caracter[0]_i_182_n_0\
    );
\caracter_reg[0]_i_89\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_93_n_0\,
      CO(3) => \caracter_reg[0]_i_89_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_89_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => euros2(16 downto 13),
      O(3) => \caracter_reg[0]_i_89_n_4\,
      O(2) => \caracter_reg[0]_i_89_n_5\,
      O(1) => \caracter_reg[0]_i_89_n_6\,
      O(0) => \caracter_reg[0]_i_89_n_7\,
      S(3) => \caracter[0]_i_187_n_0\,
      S(2) => \caracter[0]_i_188_n_0\,
      S(1) => \caracter[0]_i_189_n_0\,
      S(0) => \caracter[0]_i_190_n_0\
    );
\caracter_reg[0]_i_90\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_95_n_0\,
      CO(3) => \caracter_reg[0]_i_90_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_90_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_191_n_0\,
      DI(2) => \caracter[0]_i_192_n_0\,
      DI(1) => \caracter[0]_i_193_n_0\,
      DI(0) => \caracter[0]_i_194_n_0\,
      O(3) => \caracter_reg[0]_i_90_n_4\,
      O(2) => \caracter_reg[0]_i_90_n_5\,
      O(1) => \caracter_reg[0]_i_90_n_6\,
      O(0) => \caracter_reg[0]_i_90_n_7\,
      S(3) => \caracter[0]_i_195_n_0\,
      S(2) => \caracter[0]_i_196_n_0\,
      S(1) => \caracter[0]_i_197_n_0\,
      S(0) => \caracter[0]_i_198_n_0\
    );
\caracter_reg[0]_i_93\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_160_n_0\,
      CO(3) => \caracter_reg[0]_i_93_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_93_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => euros2(12 downto 9),
      O(3) => \caracter_reg[0]_i_93_n_4\,
      O(2) => \caracter_reg[0]_i_93_n_5\,
      O(1) => \caracter_reg[0]_i_93_n_6\,
      O(0) => \caracter_reg[0]_i_93_n_7\,
      S(3) => \caracter[0]_i_203_n_0\,
      S(2) => \caracter[0]_i_204_n_0\,
      S(1) => \caracter[0]_i_205_n_0\,
      S(0) => \caracter[0]_i_206_n_0\
    );
\caracter_reg[0]_i_95\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_163_n_0\,
      CO(3) => \caracter_reg[0]_i_95_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_95_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_207_n_0\,
      DI(2) => \caracter[0]_i_208_n_0\,
      DI(1) => \caracter[0]_i_209_n_0\,
      DI(0) => \caracter[0]_i_210_n_0\,
      O(3) => \caracter_reg[0]_i_95_n_4\,
      O(2) => \caracter_reg[0]_i_95_n_5\,
      O(1) => \caracter_reg[0]_i_95_n_6\,
      O(0) => \caracter_reg[0]_i_95_n_7\,
      S(3) => \caracter[0]_i_211_n_0\,
      S(2) => \caracter[0]_i_212_n_0\,
      S(1) => \caracter[0]_i_213_n_0\,
      S(0) => \caracter[0]_i_214_n_0\
    );
\caracter_reg[0]_i_97\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_162_n_0\,
      CO(3) => \caracter_reg[0]_i_97_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_97_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_215_n_0\,
      DI(2) => \caracter[0]_i_216_n_0\,
      DI(1) => \caracter[0]_i_217_n_0\,
      DI(0) => \caracter[0]_i_218_n_0\,
      O(3) => \caracter_reg[0]_i_97_n_4\,
      O(2) => \caracter_reg[0]_i_97_n_5\,
      O(1) => \caracter_reg[0]_i_97_n_6\,
      O(0) => \caracter_reg[0]_i_97_n_7\,
      S(3) => \caracter[0]_i_219_n_0\,
      S(2) => \caracter[0]_i_220_n_0\,
      S(1) => \caracter[0]_i_221_n_0\,
      S(0) => \caracter[0]_i_222_n_0\
    );
\caracter_reg[0]_i_99\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_88_n_0\,
      CO(3) => \caracter_reg[0]_i_99_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[0]_i_99_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_223_n_0\,
      DI(2) => \caracter[0]_i_224_n_0\,
      DI(1) => \caracter[0]_i_225_n_0\,
      DI(0) => \caracter[0]_i_226_n_0\,
      O(3) => \caracter_reg[0]_i_99_n_4\,
      O(2) => \caracter_reg[0]_i_99_n_5\,
      O(1) => \caracter_reg[0]_i_99_n_6\,
      O(0) => \caracter_reg[0]_i_99_n_7\,
      S(3) => \caracter[0]_i_227_n_0\,
      S(2) => \caracter[0]_i_228_n_0\,
      S(1) => \caracter[0]_i_229_n_0\,
      S(0) => \caracter[0]_i_230_n_0\
    );
\caracter_reg[2]_i_115\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[2]_i_166_n_0\,
      CO(3) => \caracter_reg[2]_i_115_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[2]_i_115_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[2]_i_167_n_0\,
      DI(2) => \caracter[2]_i_168_n_0\,
      DI(1) => \caracter[2]_i_169_n_0\,
      DI(0) => \caracter[2]_i_170_n_0\,
      O(3) => \caracter_reg[2]_i_115_n_4\,
      O(2) => \caracter_reg[2]_i_115_n_5\,
      O(1) => \caracter_reg[2]_i_115_n_6\,
      O(0) => \caracter_reg[2]_i_115_n_7\,
      S(3) => \caracter[2]_i_171_n_0\,
      S(2) => \caracter[2]_i_172_n_0\,
      S(1) => \caracter[2]_i_173_n_0\,
      S(0) => \caracter[2]_i_174_n_0\
    );
\caracter_reg[2]_i_137\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[2]_i_176_n_0\,
      CO(3) => \caracter_reg[2]_i_137_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[2]_i_137_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[2]_i_177_n_0\,
      DI(2) => \caracter[2]_i_178_n_0\,
      DI(1) => \caracter[2]_i_179_n_0\,
      DI(0) => \caracter[2]_i_180_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[2]_i_137_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[2]_i_181_n_0\,
      S(2) => \caracter[2]_i_182_n_0\,
      S(1) => \caracter[2]_i_183_n_0\,
      S(0) => \caracter[2]_i_184_n_0\
    );
\caracter_reg[2]_i_14\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[2]_i_15_n_0\,
      CO(3 downto 0) => \NLW_caracter_reg[2]_i_14_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_caracter_reg[2]_i_14_O_UNCONNECTED\(3 downto 1),
      O(0) => \caracter_reg[2]_i_14_n_7\,
      S(3 downto 1) => B"000",
      S(0) => \caracter[2]_i_22_n_0\
    );
\caracter_reg[2]_i_146\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[2]_i_185_n_0\,
      CO(3) => \caracter_reg[2]_i_146_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[2]_i_146_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[2]_i_187_n_0\,
      DI(2) => \caracter[2]_i_188_n_0\,
      DI(1) => \caracter[2]_i_189_n_0\,
      DI(0) => \caracter[2]_i_190_n_0\,
      O(3) => \caracter_reg[2]_i_146_n_4\,
      O(2) => \caracter_reg[2]_i_146_n_5\,
      O(1) => \caracter_reg[2]_i_146_n_6\,
      O(0) => \caracter_reg[2]_i_146_n_7\,
      S(3) => \caracter[2]_i_191_n_0\,
      S(2) => \caracter[2]_i_192_n_0\,
      S(1) => \caracter[2]_i_193_n_0\,
      S(0) => \caracter[2]_i_194_n_0\
    );
\caracter_reg[2]_i_15\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[2]_i_15_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[2]_i_15_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '1',
      DI(3) => \caracter[2]_i_23_n_0\,
      DI(2) => \caracter[2]_i_190_n_0\,
      DI(1) => \caracter[2]_i_219_n_0\,
      DI(0) => \caracter[2]_i_26_n_0\,
      O(3) => \caracter_reg[2]_i_15_n_4\,
      O(2) => \caracter_reg[2]_i_15_n_5\,
      O(1) => \caracter_reg[2]_i_15_n_6\,
      O(0) => \string_cent_decenas[1]5\(0),
      S(3) => \caracter[2]_i_27_n_0\,
      S(2) => \caracter[2]_i_28_n_0\,
      S(1) => \caracter[2]_i_29_n_0\,
      S(0) => \caracter[2]_i_30_n_0\
    );
\caracter_reg[2]_i_151\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[2]_i_195_n_0\,
      CO(3) => \caracter_reg[2]_i_151_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[2]_i_151_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[2]_i_116_n_0\,
      DI(2) => \caracter[2]_i_197_n_0\,
      DI(1) => \caracter[3]_i_58_n_0\,
      DI(0) => \caracter[2]_i_198_n_0\,
      O(3) => \caracter_reg[2]_i_151_n_4\,
      O(2) => \caracter_reg[2]_i_151_n_5\,
      O(1) => \caracter_reg[2]_i_151_n_6\,
      O(0) => \caracter_reg[2]_i_151_n_7\,
      S(3) => \caracter[2]_i_199_n_0\,
      S(2) => \caracter[2]_i_200_n_0\,
      S(1) => \caracter[2]_i_201_n_0\,
      S(0) => \caracter[2]_i_202_n_0\
    );
\caracter_reg[2]_i_166\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[2]_i_166_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[2]_i_166_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[2]_i_159_n_0\,
      DI(2) => \caracter[2]_i_203_n_0\,
      DI(1) => \caracter[2]_i_204_n_0\,
      DI(0) => '0',
      O(3) => \caracter_reg[2]_i_166_n_4\,
      O(2) => \caracter_reg[2]_i_166_n_5\,
      O(1) => \caracter_reg[2]_i_166_n_6\,
      O(0) => \caracter_reg[2]_i_166_n_7\,
      S(3) => \caracter[2]_i_205_n_0\,
      S(2) => \caracter[2]_i_206_n_0\,
      S(1) => \caracter[2]_i_207_n_0\,
      S(0) => \caracter[2]_i_208_n_0\
    );
\caracter_reg[2]_i_175\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_24_n_0\,
      CO(3 downto 0) => \NLW_caracter_reg[2]_i_175_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 2) => \NLW_caracter_reg[2]_i_175_O_UNCONNECTED\(3 downto 2),
      O(1) => \caracter_reg[2]_i_175_n_6\,
      O(0) => \caracter_reg[2]_i_175_n_7\,
      S(3 downto 2) => B"00",
      S(1) => \caracter[2]_i_209_n_0\,
      S(0) => \caracter[2]_i_210_n_0\
    );
\caracter_reg[2]_i_176\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[2]_i_176_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[2]_i_176_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[2]_i_211_n_0\,
      DI(2) => \caracter[2]_i_212_n_0\,
      DI(1) => \caracter[2]_i_213_n_0\,
      DI(0) => \caracter[2]_i_214_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[2]_i_176_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[2]_i_215_n_0\,
      S(2) => \caracter[2]_i_216_n_0\,
      S(1) => \caracter[2]_i_217_n_0\,
      S(0) => \caracter[2]_i_218_n_0\
    );
\caracter_reg[2]_i_185\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[2]_i_185_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[2]_i_185_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[2]_i_219_n_0\,
      DI(2) => \caracter[2]_i_220_n_0\,
      DI(1 downto 0) => B"01",
      O(3) => \caracter_reg[2]_i_185_n_4\,
      O(2) => \caracter_reg[2]_i_185_n_5\,
      O(1) => \caracter_reg[2]_i_185_n_6\,
      O(0) => \NLW_caracter_reg[2]_i_185_O_UNCONNECTED\(0),
      S(3) => \caracter[2]_i_221_n_0\,
      S(2) => \caracter[2]_i_222_n_0\,
      S(1) => \caracter[2]_i_223_n_0\,
      S(0) => \caracter[2]_i_224_n_0\
    );
\caracter_reg[2]_i_186\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[2]_i_186_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[2]_i_186_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[2]_i_219_n_0\,
      DI(2) => \caracter[2]_i_226_n_0\,
      DI(1 downto 0) => B"01",
      O(3 downto 1) => \NLW_caracter_reg[2]_i_186_O_UNCONNECTED\(3 downto 1),
      O(0) => \caracter_reg[2]_i_186_n_7\,
      S(3) => \caracter[2]_i_227_n_0\,
      S(2) => \caracter[2]_i_228_n_0\,
      S(1) => \caracter[2]_i_229_n_0\,
      S(0) => \caracter[2]_i_230_n_0\
    );
\caracter_reg[2]_i_195\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[2]_i_186_n_0\,
      CO(3) => \caracter_reg[2]_i_195_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[2]_i_195_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[2]_i_231_n_0\,
      DI(2) => \caracter[2]_i_188_n_0\,
      DI(1) => \caracter[2]_i_232_n_0\,
      DI(0) => \caracter[2]_i_190_n_0\,
      O(3) => \caracter_reg[2]_i_195_n_4\,
      O(2) => \caracter_reg[2]_i_195_n_5\,
      O(1) => \caracter_reg[2]_i_195_n_6\,
      O(0) => \NLW_caracter_reg[2]_i_195_O_UNCONNECTED\(0),
      S(3) => \caracter[2]_i_234_n_0\,
      S(2) => \caracter[2]_i_235_n_0\,
      S(1) => \caracter[2]_i_236_n_0\,
      S(0) => \caracter[2]_i_237_n_0\
    );
\caracter_reg[2]_i_31\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3 downto 0) => \NLW_caracter_reg[2]_i_31_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \caracter_reg[2]_i_37_n_6\,
      DI(0) => '0',
      O(3) => \NLW_caracter_reg[2]_i_31_O_UNCONNECTED\(3),
      O(2) => \caracter_reg[2]_i_31_n_5\,
      O(1) => \caracter_reg[2]_i_31_n_6\,
      O(0) => \caracter_reg[2]_i_31_n_7\,
      S(3) => '0',
      S(2) => \caracter[2]_i_38_n_0\,
      S(1) => \caracter[2]_i_39_n_0\,
      S(0) => \caracter_reg[2]_i_37_n_7\
    );
\caracter_reg[2]_i_36\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[2]_i_43_n_0\,
      CO(3) => \caracter_reg[2]_i_36_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[2]_i_36_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[2]_i_44_n_0\,
      DI(2) => \caracter[2]_i_45_n_0\,
      DI(1) => \caracter[2]_i_46_n_0\,
      DI(0) => \caracter[2]_i_47_n_0\,
      O(3) => \caracter_reg[2]_i_36_n_4\,
      O(2 downto 0) => \NLW_caracter_reg[2]_i_36_O_UNCONNECTED\(2 downto 0),
      S(3) => \caracter[2]_i_48_n_0\,
      S(2) => \caracter[2]_i_49_n_0\,
      S(1) => \caracter[2]_i_50_n_0\,
      S(0) => \caracter[2]_i_51_n_0\
    );
\caracter_reg[2]_i_37\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[2]_i_36_n_0\,
      CO(3 downto 0) => \NLW_caracter_reg[2]_i_37_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \caracter[2]_i_52_n_0\,
      DI(0) => \caracter[2]_i_53_n_0\,
      O(3) => \NLW_caracter_reg[2]_i_37_O_UNCONNECTED\(3),
      O(2) => \caracter_reg[2]_i_37_n_5\,
      O(1) => \caracter_reg[2]_i_37_n_6\,
      O(0) => \caracter_reg[2]_i_37_n_7\,
      S(3) => '0',
      S(2) => \caracter[2]_i_54_n_0\,
      S(1) => \caracter[2]_i_55_n_0\,
      S(0) => \caracter[2]_i_56_n_0\
    );
\caracter_reg[2]_i_40\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_13_n_0\,
      CO(3 downto 0) => \NLW_caracter_reg[2]_i_40_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => \caracter_reg[3]_i_141_0\(6 downto 4),
      O(3) => \caracter_reg[2]_i_40_n_4\,
      O(2) => \caracter_reg[2]_i_40_n_5\,
      O(1) => \caracter_reg[2]_i_40_n_6\,
      O(0) => \caracter_reg[2]_i_40_n_7\,
      S(3) => \caracter[2]_i_57_n_0\,
      S(2) => \caracter[2]_i_58_n_0\,
      S(1) => \caracter[2]_i_59_n_0\,
      S(0) => \caracter[2]_i_60_n_0\
    );
\caracter_reg[2]_i_43\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[2]_i_61_n_0\,
      CO(3) => \caracter_reg[2]_i_43_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[2]_i_43_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[2]_i_62_n_0\,
      DI(2) => \caracter[2]_i_63_n_0\,
      DI(1) => \caracter[2]_i_64_n_0\,
      DI(0) => \caracter[2]_i_65_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[2]_i_43_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[2]_i_66_n_0\,
      S(2) => \caracter[2]_i_67_n_0\,
      S(1) => \caracter[2]_i_68_n_0\,
      S(0) => \caracter[2]_i_69_n_0\
    );
\caracter_reg[2]_i_61\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[2]_i_83_n_0\,
      CO(3) => \caracter_reg[2]_i_61_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[2]_i_61_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[2]_i_84_n_0\,
      DI(2) => \caracter[2]_i_85_n_0\,
      DI(1) => \caracter[2]_i_86_n_0\,
      DI(0) => \caracter[2]_i_87_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[2]_i_61_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[2]_i_88_n_0\,
      S(2) => \caracter[2]_i_89_n_0\,
      S(1) => \caracter[2]_i_90_n_0\,
      S(0) => \caracter[2]_i_91_n_0\
    );
\caracter_reg[2]_i_70\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[2]_i_95_n_0\,
      CO(3) => \caracter_reg[2]_i_70_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[2]_i_70_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[2]_i_100_n_0\,
      DI(2) => \caracter[2]_i_101_n_0\,
      DI(1) => \caracter[2]_i_102_n_0\,
      DI(0) => \caracter[2]_i_103_n_0\,
      O(3) => \caracter_reg[2]_i_70_n_4\,
      O(2) => \caracter_reg[2]_i_70_n_5\,
      O(1) => \caracter_reg[2]_i_70_n_6\,
      O(0) => \caracter_reg[2]_i_70_n_7\,
      S(3) => \caracter[2]_i_104_n_0\,
      S(2) => \caracter[2]_i_105_n_0\,
      S(1) => \caracter[2]_i_106_n_0\,
      S(0) => \caracter[2]_i_107_n_0\
    );
\caracter_reg[2]_i_71\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[2]_i_94_n_0\,
      CO(3 downto 1) => \NLW_caracter_reg[2]_i_71_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \caracter_reg[2]_i_71_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_caracter_reg[2]_i_71_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
\caracter_reg[2]_i_72\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[2]_i_72_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[2]_i_72_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[2]_i_108_n_0\,
      DI(2) => \caracter[2]_i_109_n_0\,
      DI(1) => \caracter[2]_i_110_n_0\,
      DI(0) => '0',
      O(3) => \caracter_reg[2]_i_72_n_4\,
      O(2) => \caracter_reg[2]_i_72_n_5\,
      O(1) => \caracter_reg[2]_i_72_n_6\,
      O(0) => \caracter_reg[2]_i_72_n_7\,
      S(3) => \caracter[2]_i_111_n_0\,
      S(2) => \caracter[2]_i_112_n_0\,
      S(1) => \caracter[2]_i_113_n_0\,
      S(0) => \caracter[2]_i_114_n_0\
    );
\caracter_reg[2]_i_73\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[2]_i_115_n_0\,
      CO(3) => \NLW_caracter_reg[2]_i_73_CO_UNCONNECTED\(3),
      CO(2) => \caracter_reg[2]_i_73_n_1\,
      CO(1 downto 0) => \NLW_caracter_reg[2]_i_73_CO_UNCONNECTED\(1 downto 0),
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \caracter[2]_i_116_n_0\,
      DI(0) => \caracter[2]_i_117_n_0\,
      O(3 downto 2) => \NLW_caracter_reg[2]_i_73_O_UNCONNECTED\(3 downto 2),
      O(1) => \caracter_reg[2]_i_73_n_6\,
      O(0) => \caracter_reg[2]_i_73_n_7\,
      S(3 downto 2) => B"01",
      S(1) => \caracter[2]_i_118_n_0\,
      S(0) => \caracter[2]_i_119_n_0\
    );
\caracter_reg[2]_i_74\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[2]_i_70_n_0\,
      CO(3) => \NLW_caracter_reg[2]_i_74_CO_UNCONNECTED\(3),
      CO(2) => \caracter_reg[2]_i_74_n_1\,
      CO(1 downto 0) => \NLW_caracter_reg[2]_i_74_CO_UNCONNECTED\(1 downto 0),
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \caracter[2]_i_116_n_0\,
      DI(0) => \caracter[2]_i_121_n_0\,
      O(3 downto 2) => \NLW_caracter_reg[2]_i_74_O_UNCONNECTED\(3 downto 2),
      O(1) => \caracter_reg[2]_i_74_n_6\,
      O(0) => \caracter_reg[2]_i_74_n_7\,
      S(3 downto 2) => B"01",
      S(1) => \caracter[2]_i_122_n_0\,
      S(0) => \caracter[2]_i_123_n_0\
    );
\caracter_reg[2]_i_75\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[2]_i_72_n_0\,
      CO(3) => \caracter_reg[2]_i_75_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[2]_i_75_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[2]_i_124_n_0\,
      DI(2) => \caracter[2]_i_125_n_0\,
      DI(1) => \caracter[2]_i_126_n_0\,
      DI(0) => \caracter[2]_i_127_n_0\,
      O(3) => \caracter_reg[2]_i_75_n_4\,
      O(2) => \caracter_reg[2]_i_75_n_5\,
      O(1) => \caracter_reg[2]_i_75_n_6\,
      O(0) => \caracter_reg[2]_i_75_n_7\,
      S(3) => \caracter[2]_i_128_n_0\,
      S(2) => \caracter[2]_i_129_n_0\,
      S(1) => \caracter[2]_i_130_n_0\,
      S(0) => \caracter[2]_i_131_n_0\
    );
\caracter_reg[2]_i_80\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[2]_i_75_n_0\,
      CO(3 downto 0) => \NLW_caracter_reg[2]_i_80_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_caracter_reg[2]_i_80_O_UNCONNECTED\(3 downto 1),
      O(0) => \caracter_reg[2]_i_80_n_7\,
      S(3 downto 1) => B"000",
      S(0) => \caracter[2]_i_132_n_0\
    );
\caracter_reg[2]_i_82\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3 downto 0) => \NLW_caracter_reg[2]_i_82_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \caracter[2]_i_133_n_0\,
      DI(0) => '0',
      O(3) => \NLW_caracter_reg[2]_i_82_O_UNCONNECTED\(3),
      O(2) => \caracter_reg[2]_i_82_n_5\,
      O(1) => \caracter_reg[2]_i_82_n_6\,
      O(0) => \caracter_reg[2]_i_82_n_7\,
      S(3) => '0',
      S(2) => \caracter[2]_i_134_n_0\,
      S(1) => \caracter[2]_i_135_n_0\,
      S(0) => \caracter[2]_i_136_n_0\
    );
\caracter_reg[2]_i_83\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[2]_i_137_n_0\,
      CO(3) => \caracter_reg[2]_i_83_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[2]_i_83_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[2]_i_138_n_0\,
      DI(2) => \caracter[2]_i_139_n_0\,
      DI(1) => \caracter[2]_i_140_n_0\,
      DI(0) => \caracter[2]_i_141_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[2]_i_83_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[2]_i_142_n_0\,
      S(2) => \caracter[2]_i_143_n_0\,
      S(1) => \caracter[2]_i_144_n_0\,
      S(0) => \caracter[2]_i_145_n_0\
    );
\caracter_reg[2]_i_93\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[2]_i_151_n_0\,
      CO(3 downto 1) => \NLW_caracter_reg[2]_i_93_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \caracter_reg[2]_i_93_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_caracter_reg[2]_i_93_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
\caracter_reg[2]_i_94\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[2]_i_146_n_0\,
      CO(3) => \caracter_reg[2]_i_94_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[2]_i_94_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[2]_i_116_n_0\,
      DI(2) => \caracter[2]_i_153_n_0\,
      DI(1) => \caracter[3]_i_58_n_0\,
      DI(0) => \caracter[2]_i_154_n_0\,
      O(3) => \caracter_reg[2]_i_94_n_4\,
      O(2) => \caracter_reg[2]_i_94_n_5\,
      O(1) => \caracter_reg[2]_i_94_n_6\,
      O(0) => \caracter_reg[2]_i_94_n_7\,
      S(3) => \caracter[2]_i_155_n_0\,
      S(2) => \caracter[2]_i_156_n_0\,
      S(1) => \caracter[2]_i_157_n_0\,
      S(0) => \caracter[2]_i_158_n_0\
    );
\caracter_reg[2]_i_95\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[2]_i_95_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[2]_i_95_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[2]_i_159_n_0\,
      DI(2) => \caracter[2]_i_160_n_0\,
      DI(1) => \caracter[2]_i_161_n_0\,
      DI(0) => '0',
      O(3) => \caracter_reg[2]_i_95_n_4\,
      O(2) => \caracter_reg[2]_i_95_n_5\,
      O(1) => \caracter_reg[2]_i_95_n_6\,
      O(0) => \NLW_caracter_reg[2]_i_95_O_UNCONNECTED\(0),
      S(3) => \caracter[2]_i_162_n_0\,
      S(2) => \caracter[2]_i_163_n_0\,
      S(1) => \caracter[2]_i_164_n_0\,
      S(0) => \caracter[2]_i_165_n_0\
    );
\caracter_reg[3]_i_103\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[3]_i_132_n_0\,
      CO(3) => \caracter_reg[3]_i_103_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[3]_i_103_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[3]_i_133_n_0\,
      DI(2) => \caracter[3]_i_134_n_0\,
      DI(1) => \caracter[3]_i_135_n_0\,
      DI(0) => \caracter[3]_i_136_n_0\,
      O(3) => \caracter_reg[3]_i_103_n_4\,
      O(2) => \caracter_reg[3]_i_103_n_5\,
      O(1) => \caracter_reg[3]_i_103_n_6\,
      O(0) => \caracter_reg[3]_i_103_n_7\,
      S(3) => \caracter[3]_i_137_n_0\,
      S(2) => \caracter[3]_i_138_n_0\,
      S(1) => \caracter[3]_i_139_n_0\,
      S(0) => \caracter[3]_i_140_n_0\
    );
\caracter_reg[3]_i_109\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[3]_i_141_n_0\,
      CO(3) => \caracter_reg[3]_i_109_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[3]_i_109_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => cent(4),
      DI(2) => \caracter[2]_i_23_n_0\,
      DI(1) => \caracter[2]_i_190_n_0\,
      DI(0) => \caracter[2]_i_219_n_0\,
      O(3) => \caracter_reg[3]_i_109_n_4\,
      O(2) => \caracter_reg[3]_i_109_n_5\,
      O(1) => \caracter_reg[3]_i_109_n_6\,
      O(0) => \caracter_reg[3]_i_109_n_7\,
      S(3) => \caracter[3]_i_146_n_0\,
      S(2) => \caracter[3]_i_147_n_0\,
      S(1) => \caracter[3]_i_148_n_0\,
      S(0) => \caracter[3]_i_149_n_0\
    );
\caracter_reg[3]_i_114\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[3]_i_150_n_0\,
      CO(3) => \caracter_reg[3]_i_114_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[3]_i_114_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[3]_i_151_n_0\,
      DI(2) => \caracter[3]_i_152_n_0\,
      DI(1) => \caracter[3]_i_153_n_0\,
      DI(0) => \caracter[3]_i_154_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[3]_i_114_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[3]_i_155_n_0\,
      S(2) => \caracter[3]_i_156_n_0\,
      S(1) => \caracter[3]_i_157_n_0\,
      S(0) => \caracter[3]_i_158_n_0\
    );
\caracter_reg[3]_i_123\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[3]_i_123_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[3]_i_123_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[2]_i_116_n_0\,
      DI(2) => \caracter[3]_i_164_n_0\,
      DI(1) => cent(4),
      DI(0) => '0',
      O(3) => \caracter_reg[3]_i_123_n_4\,
      O(2) => \caracter_reg[3]_i_123_n_5\,
      O(1) => \caracter_reg[3]_i_123_n_6\,
      O(0) => \caracter_reg[3]_i_123_n_7\,
      S(3) => \caracter[3]_i_166_n_0\,
      S(2) => \caracter[3]_i_167_n_0\,
      S(1) => \caracter[3]_i_168_n_0\,
      S(0) => \caracter[2]_i_23_n_0\
    );
\caracter_reg[3]_i_132\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[3]_i_132_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[3]_i_132_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[0]_i_11_n_0\,
      DI(2) => \caracter[3]_i_170_n_0\,
      DI(1 downto 0) => B"01",
      O(3) => \caracter_reg[3]_i_132_n_4\,
      O(2) => \caracter_reg[3]_i_132_n_5\,
      O(1) => \caracter_reg[3]_i_132_n_6\,
      O(0) => \NLW_caracter_reg[3]_i_132_O_UNCONNECTED\(0),
      S(3) => \caracter[3]_i_171_n_0\,
      S(2) => \caracter[3]_i_172_n_0\,
      S(1) => \caracter[3]_i_173_n_0\,
      S(0) => \caracter[3]_i_174_n_0\
    );
\caracter_reg[3]_i_141\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[3]_i_141_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[3]_i_141_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[3]_i_175_n_0\,
      DI(2 downto 0) => B"001",
      O(3) => \caracter_reg[3]_i_141_n_4\,
      O(2) => \caracter_reg[3]_i_141_n_5\,
      O(1) => \caracter_reg[3]_i_141_n_6\,
      O(0) => \NLW_caracter_reg[3]_i_141_O_UNCONNECTED\(0),
      S(3) => \caracter[3]_i_176_n_0\,
      S(2) => \caracter[3]_i_177_n_0\,
      S(1) => \caracter[3]_i_178_n_0\,
      S(0) => \caracter[3]_i_179_n_0\
    );
\caracter_reg[3]_i_150\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[3]_i_150_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[3]_i_150_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[3]_i_180_n_0\,
      DI(2) => \caracter[3]_i_181_n_0\,
      DI(1) => \caracter[3]_i_182_n_0\,
      DI(0) => \caracter[3]_i_183_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[3]_i_150_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[3]_i_184_n_0\,
      S(2) => \caracter[3]_i_185_n_0\,
      S(1) => \caracter[3]_i_186_n_0\,
      S(0) => \caracter[3]_i_187_n_0\
    );
\caracter_reg[3]_i_188\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[3]_i_189_n_0\,
      CO(3 downto 2) => \NLW_caracter_reg[3]_i_188_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \caracter_reg[3]_i_188_n_2\,
      CO(0) => \NLW_caracter_reg[3]_i_188_CO_UNCONNECTED\(0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_caracter_reg[3]_i_188_O_UNCONNECTED\(3 downto 1),
      O(0) => \caracter_reg[3]_i_188_n_7\,
      S(3 downto 1) => B"001",
      S(0) => \caracter[2]_i_116_n_0\
    );
\caracter_reg[3]_i_189\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[3]_i_191_n_0\,
      CO(3) => \caracter_reg[3]_i_189_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[3]_i_189_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \caracter[3]_i_193_n_0\,
      O(3) => \caracter_reg[3]_i_189_n_4\,
      O(2) => \caracter_reg[3]_i_189_n_5\,
      O(1) => \caracter_reg[3]_i_189_n_6\,
      O(0) => \caracter_reg[3]_i_189_n_7\,
      S(3) => \caracter[3]_i_194_n_0\,
      S(2) => cent(4),
      S(1) => \caracter[2]_i_23_n_0\,
      S(0) => \caracter[3]_i_197_n_0\
    );
\caracter_reg[3]_i_191\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[3]_i_198_n_0\,
      CO(3) => \caracter_reg[3]_i_191_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[3]_i_191_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[3]_i_199_n_0\,
      DI(2) => \caracter[3]_i_200_n_0\,
      DI(1) => \caracter[2]_i_116_n_0\,
      DI(0) => \caracter[3]_i_202_n_0\,
      O(3) => \caracter_reg[3]_i_191_n_4\,
      O(2 downto 0) => \NLW_caracter_reg[3]_i_191_O_UNCONNECTED\(2 downto 0),
      S(3) => \caracter[3]_i_203_n_0\,
      S(2) => \caracter[3]_i_204_n_0\,
      S(1) => \caracter[3]_i_205_n_0\,
      S(0) => \caracter[3]_i_206_n_0\
    );
\caracter_reg[3]_i_198\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[3]_i_198_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[3]_i_198_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => cent(4),
      DI(2) => \caracter[2]_i_23_n_0\,
      DI(1) => \caracter[2]_i_190_n_0\,
      DI(0) => '0',
      O(3 downto 0) => \NLW_caracter_reg[3]_i_198_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[3]_i_210_n_0\,
      S(2) => \caracter[3]_i_211_n_0\,
      S(1) => \caracter[3]_i_212_n_0\,
      S(0) => \caracter[2]_i_219_n_0\
    );
\caracter_reg[3]_i_21\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[3]_i_30_n_0\,
      CO(3 downto 0) => \NLW_caracter_reg[3]_i_21_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \caracter[2]_i_116_n_0\,
      DI(1 downto 0) => cent(5 downto 4),
      O(3) => \caracter_reg[3]_i_21_n_4\,
      O(2) => \caracter_reg[3]_i_21_n_5\,
      O(1) => \caracter_reg[3]_i_21_n_6\,
      O(0) => \caracter_reg[3]_i_21_n_7\,
      S(3) => \caracter[3]_i_37_n_0\,
      S(2) => \caracter[3]_i_38_n_0\,
      S(1) => \caracter[3]_i_39_n_0\,
      S(0) => \caracter[3]_i_40_n_0\
    );
\caracter_reg[3]_i_30\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[3]_i_30_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[3]_i_30_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '1',
      DI(3) => \caracter[2]_i_23_n_0\,
      DI(2) => \caracter[2]_i_190_n_0\,
      DI(1) => \caracter[2]_i_219_n_0\,
      DI(0) => \disp_dinero[2]_7\(0),
      O(3) => \caracter_reg[3]_i_30_n_4\,
      O(2) => \caracter_reg[3]_i_30_n_5\,
      O(1) => \string_cent_decenas[1]5\(1),
      O(0) => \NLW_caracter_reg[3]_i_30_O_UNCONNECTED\(0),
      S(3) => \caracter[3]_i_53_n_0\,
      S(2) => \caracter[3]_i_54_n_0\,
      S(1) => \caracter[3]_i_55_n_0\,
      S(0) => \caracter[3]_i_56_n_0\
    );
\caracter_reg[3]_i_59\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3 downto 0) => \NLW_caracter_reg[3]_i_59_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \caracter[3]_i_63_n_0\,
      DI(0) => '0',
      O(3) => \NLW_caracter_reg[3]_i_59_O_UNCONNECTED\(3),
      O(2) => \caracter_reg[3]_i_59_n_5\,
      O(1) => \caracter_reg[3]_i_59_n_6\,
      O(0) => \caracter_reg[3]_i_59_n_7\,
      S(3) => '0',
      S(2) => \caracter[3]_i_64_n_0\,
      S(1) => \caracter[3]_i_65_n_0\,
      S(0) => \caracter[3]_i_66_n_0\
    );
\caracter_reg[3]_i_60\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[3]_i_67_n_0\,
      CO(3) => \caracter_reg[3]_i_60_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[3]_i_60_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \caracter[3]_i_68_n_0\,
      DI(1) => \caracter[3]_i_69_n_0\,
      DI(0) => \caracter[3]_i_70_n_0\,
      O(3) => \caracter_reg[3]_i_60_n_4\,
      O(2) => \caracter_reg[3]_i_60_n_5\,
      O(1) => \caracter_reg[3]_i_60_n_6\,
      O(0) => \caracter_reg[3]_i_60_n_7\,
      S(3) => \caracter[3]_i_71_n_0\,
      S(2) => \caracter[3]_i_72_n_0\,
      S(1) => \caracter[3]_i_73_n_0\,
      S(0) => \caracter[3]_i_74_n_0\
    );
\caracter_reg[3]_i_67\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[3]_i_76_n_0\,
      CO(3) => \caracter_reg[3]_i_67_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[3]_i_67_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[3]_i_77_n_0\,
      DI(2) => \caracter[3]_i_78_n_0\,
      DI(1) => \caracter[3]_i_79_n_0\,
      DI(0) => \caracter[3]_i_80_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[3]_i_67_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[3]_i_81_n_0\,
      S(2) => \caracter[3]_i_82_n_0\,
      S(1) => \caracter[3]_i_83_n_0\,
      S(0) => \caracter[3]_i_84_n_0\
    );
\caracter_reg[3]_i_75\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[3]_i_60_n_0\,
      CO(3 downto 0) => \NLW_caracter_reg[3]_i_75_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 2) => \NLW_caracter_reg[3]_i_75_O_UNCONNECTED\(3 downto 2),
      O(1) => \caracter_reg[3]_i_75_n_6\,
      O(0) => \caracter_reg[3]_i_75_n_7\,
      S(3 downto 2) => B"00",
      S(1) => \caracter[3]_i_88_n_0\,
      S(0) => \caracter[3]_i_89_n_0\
    );
\caracter_reg[3]_i_76\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[3]_i_90_n_0\,
      CO(3) => \caracter_reg[3]_i_76_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[3]_i_76_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[3]_i_91_n_0\,
      DI(2) => \caracter[3]_i_92_n_0\,
      DI(1) => \caracter[3]_i_93_n_0\,
      DI(0) => \caracter[3]_i_94_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[3]_i_76_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[3]_i_95_n_0\,
      S(2) => \caracter[3]_i_96_n_0\,
      S(1) => \caracter[3]_i_97_n_0\,
      S(0) => \caracter[3]_i_98_n_0\
    );
\caracter_reg[3]_i_85\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[3]_i_99_n_0\,
      CO(3) => \NLW_caracter_reg[3]_i_85_CO_UNCONNECTED\(3),
      CO(2) => \caracter_reg[3]_i_85_n_1\,
      CO(1 downto 0) => \NLW_caracter_reg[3]_i_85_CO_UNCONNECTED\(1 downto 0),
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \caracter[3]_i_100_n_0\,
      O(3 downto 2) => \NLW_caracter_reg[3]_i_85_O_UNCONNECTED\(3 downto 2),
      O(1) => \caracter_reg[3]_i_85_n_6\,
      O(0) => \caracter_reg[3]_i_85_n_7\,
      S(3 downto 2) => B"01",
      S(1) => \caracter[2]_i_116_n_0\,
      S(0) => \caracter[3]_i_102_n_0\
    );
\caracter_reg[3]_i_86\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[3]_i_103_n_0\,
      CO(3) => \caracter_reg[3]_i_86_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[3]_i_86_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 2) => B"01",
      DI(1) => \caracter[3]_i_104_n_0\,
      DI(0) => \caracter[3]_i_105_n_0\,
      O(3) => \NLW_caracter_reg[3]_i_86_O_UNCONNECTED\(3),
      O(2) => \caracter_reg[3]_i_86_n_5\,
      O(1) => \caracter_reg[3]_i_86_n_6\,
      O(0) => \caracter_reg[3]_i_86_n_7\,
      S(3) => '1',
      S(2) => \caracter[3]_i_106_n_0\,
      S(1) => \caracter[3]_i_107_n_0\,
      S(0) => \caracter[3]_i_108_n_0\
    );
\caracter_reg[3]_i_87\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[3]_i_109_n_0\,
      CO(3) => \NLW_caracter_reg[3]_i_87_CO_UNCONNECTED\(3),
      CO(2) => \caracter_reg[3]_i_87_n_1\,
      CO(1 downto 0) => \NLW_caracter_reg[3]_i_87_CO_UNCONNECTED\(1 downto 0),
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \caracter[2]_i_116_n_0\,
      DI(0) => \caracter[3]_i_111_n_0\,
      O(3 downto 2) => \NLW_caracter_reg[3]_i_87_O_UNCONNECTED\(3 downto 2),
      O(1) => \caracter_reg[3]_i_87_n_6\,
      O(0) => \caracter_reg[3]_i_87_n_7\,
      S(3 downto 2) => B"01",
      S(1) => \caracter[3]_i_112_n_0\,
      S(0) => \caracter[3]_i_113_n_0\
    );
\caracter_reg[3]_i_90\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[3]_i_114_n_0\,
      CO(3) => \caracter_reg[3]_i_90_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[3]_i_90_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[3]_i_115_n_0\,
      DI(2) => \caracter[3]_i_116_n_0\,
      DI(1) => \caracter[3]_i_117_n_0\,
      DI(0) => \caracter[3]_i_118_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[3]_i_90_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[3]_i_119_n_0\,
      S(2) => \caracter[3]_i_120_n_0\,
      S(1) => \caracter[3]_i_121_n_0\,
      S(0) => \caracter[3]_i_122_n_0\
    );
\caracter_reg[3]_i_99\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[3]_i_123_n_0\,
      CO(3) => \caracter_reg[3]_i_99_n_0\,
      CO(2 downto 0) => \NLW_caracter_reg[3]_i_99_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3) => \caracter[3]_i_124_n_0\,
      DI(2) => \caracter[3]_i_125_n_0\,
      DI(1) => \caracter[3]_i_126_n_0\,
      DI(0) => \caracter[3]_i_127_n_0\,
      O(3) => \caracter_reg[3]_i_99_n_4\,
      O(2) => \caracter_reg[3]_i_99_n_5\,
      O(1) => \caracter_reg[3]_i_99_n_6\,
      O(0) => \caracter_reg[3]_i_99_n_7\,
      S(3) => \caracter[3]_i_128_n_0\,
      S(2) => \caracter[3]_i_129_n_0\,
      S(1) => \caracter[3]_i_130_n_0\,
      S(0) => \caracter[3]_i_131_n_0\
    );
\caracter_reg[6]_i_28\: unisim.vcomponents.MUXF7
     port map (
      I0 => \caracter[6]_i_37_n_0\,
      I1 => \caracter[6]_i_38_n_0\,
      O => \caracter_reg[6]_i_28_n_0\,
      S => \caracter[6]_i_18_0\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity synchrnzr is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    monedas_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end synchrnzr;

architecture STRUCTURE of synchrnzr is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  attribute srl_name : string;
  attribute srl_name of sync_out_reg_srl2 : label is "\inst_sincronizador/inst_synchrnzr0/sync_out_reg_srl2 ";
begin
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => monedas_IBUF(0),
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
sync_out_reg_srl2: unisim.vcomponents.SRL16E
    generic map(
      INIT => X"0000"
    )
        port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => clk_BUFG,
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg[0]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity synchrnzr_0 is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    monedas_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of synchrnzr_0 : entity is "synchrnzr";
end synchrnzr_0;

architecture STRUCTURE of synchrnzr_0 is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  attribute srl_name : string;
  attribute srl_name of sync_out_reg_srl2 : label is "\inst_sincronizador/inst_synchrnzr1/sync_out_reg_srl2 ";
begin
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => monedas_IBUF(0),
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
sync_out_reg_srl2: unisim.vcomponents.SRL16E
    generic map(
      INIT => X"0000"
    )
        port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => clk_BUFG,
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg[0]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity synchrnzr_1 is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    monedas_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of synchrnzr_1 : entity is "synchrnzr";
end synchrnzr_1;

architecture STRUCTURE of synchrnzr_1 is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  attribute srl_name : string;
  attribute srl_name of sync_out_reg_srl2 : label is "\inst_sincronizador/inst_synchrnzr2/sync_out_reg_srl2 ";
begin
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => monedas_IBUF(0),
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
sync_out_reg_srl2: unisim.vcomponents.SRL16E
    generic map(
      INIT => X"0000"
    )
        port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => clk_BUFG,
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg[0]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity synchrnzr_2 is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    monedas_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of synchrnzr_2 : entity is "synchrnzr";
end synchrnzr_2;

architecture STRUCTURE of synchrnzr_2 is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  attribute srl_name : string;
  attribute srl_name of sync_out_reg_srl2 : label is "\inst_sincronizador/inst_synchrnzr3/sync_out_reg_srl2 ";
begin
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => monedas_IBUF(0),
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
sync_out_reg_srl2: unisim.vcomponents.SRL16E
    generic map(
      INIT => X"0000"
    )
        port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => clk_BUFG,
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg[0]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity synchrnzr_3 is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    monedas_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of synchrnzr_3 : entity is "synchrnzr";
end synchrnzr_3;

architecture STRUCTURE of synchrnzr_3 is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  attribute srl_name : string;
  attribute srl_name of sync_out_reg_srl2 : label is "\inst_sincronizador/inst_synchrnzr4/sync_out_reg_srl2 ";
begin
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => monedas_IBUF(0),
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
sync_out_reg_srl2: unisim.vcomponents.SRL16E
    generic map(
      INIT => X"0000"
    )
        port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => clk_BUFG,
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg[0]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity synchrnzr_4 is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    ref_option_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of synchrnzr_4 : entity is "synchrnzr";
end synchrnzr_4;

architecture STRUCTURE of synchrnzr_4 is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  attribute srl_name : string;
  attribute srl_name of sync_out_reg_srl2 : label is "\inst_sincronizador/inst_synchrnzr5/sync_out_reg_srl2 ";
begin
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => ref_option_IBUF(0),
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
sync_out_reg_srl2: unisim.vcomponents.SRL16E
    generic map(
      INIT => X"0000"
    )
        port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => clk_BUFG,
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg[0]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity synchrnzr_5 is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    ref_option_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of synchrnzr_5 : entity is "synchrnzr";
end synchrnzr_5;

architecture STRUCTURE of synchrnzr_5 is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  attribute srl_name : string;
  attribute srl_name of sync_out_reg_srl2 : label is "\inst_sincronizador/inst_synchrnzr6/sync_out_reg_srl2 ";
begin
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => ref_option_IBUF(0),
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
sync_out_reg_srl2: unisim.vcomponents.SRL16E
    generic map(
      INIT => X"0000"
    )
        port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => clk_BUFG,
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg[0]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity synchrnzr_6 is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    ref_option_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of synchrnzr_6 : entity is "synchrnzr";
end synchrnzr_6;

architecture STRUCTURE of synchrnzr_6 is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  attribute srl_name : string;
  attribute srl_name of sync_out_reg_srl2 : label is "\inst_sincronizador/inst_synchrnzr7/sync_out_reg_srl2 ";
begin
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => ref_option_IBUF(0),
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
sync_out_reg_srl2: unisim.vcomponents.SRL16E
    generic map(
      INIT => X"0000"
    )
        port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => clk_BUFG,
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg[0]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity synchrnzr_7 is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    ref_option_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of synchrnzr_7 : entity is "synchrnzr";
end synchrnzr_7;

architecture STRUCTURE of synchrnzr_7 is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  attribute srl_name : string;
  attribute srl_name of sync_out_reg_srl2 : label is "\inst_sincronizador/inst_synchrnzr8/sync_out_reg_srl2 ";
begin
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => ref_option_IBUF(0),
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
sync_out_reg_srl2: unisim.vcomponents.SRL16E
    generic map(
      INIT => X"0000"
    )
        port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => clk_BUFG,
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg[0]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity synchrnzr_8 is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    button_ok_IBUF : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of synchrnzr_8 : entity is "synchrnzr";
end synchrnzr_8;

architecture STRUCTURE of synchrnzr_8 is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  attribute srl_name : string;
  attribute srl_name of sync_out_reg_srl2 : label is "\inst_sincronizador/inst_synchrnzr9/sync_out_reg_srl2 ";
begin
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => button_ok_IBUF,
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
sync_out_reg_srl2: unisim.vcomponents.SRL16E
    generic map(
      INIT => X"0000"
    )
        port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => clk_BUFG,
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg[0]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity detectorflancos is
  port (
    sreg : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \sreg_reg[1]\ : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \refresco__0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \sreg_reg[1]_0\ : out STD_LOGIC;
    \sreg_reg[1]_1\ : out STD_LOGIC;
    \sreg_reg[1]_2\ : out STD_LOGIC;
    \sreg_reg[1]_3\ : out STD_LOGIC;
    \sreg_reg[1]_4\ : out STD_LOGIC;
    \sreg_reg[0]\ : out STD_LOGIC;
    \sreg_reg[0]_0\ : out STD_LOGIC;
    \FSM_onehot_current_state_reg[0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \sreg_reg[0]_1\ : out STD_LOGIC_VECTOR ( 6 downto 0 );
    \sreg_reg[0]_2\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    \sreg_reg[0]_3\ : in STD_LOGIC;
    \sreg_reg[0]_4\ : in STD_LOGIC;
    \sreg_reg[0]_5\ : in STD_LOGIC;
    \sreg_reg[0]_6\ : in STD_LOGIC;
    \sreg_reg[0]_7\ : in STD_LOGIC;
    \sreg_reg[0]_8\ : in STD_LOGIC;
    \sreg_reg[0]_9\ : in STD_LOGIC;
    \sreg_reg[0]_10\ : in STD_LOGIC;
    \sreg_reg[0]_11\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
end detectorflancos;

architecture STRUCTURE of detectorflancos is
  signal inst_edgedtctr0_n_2 : STD_LOGIC;
  signal inst_edgedtctr0_n_3 : STD_LOGIC;
  signal inst_edgedtctr1_n_0 : STD_LOGIC;
  signal inst_edgedtctr2_n_4 : STD_LOGIC;
  signal inst_edgedtctr3_n_2 : STD_LOGIC;
  signal inst_edgedtctr3_n_3 : STD_LOGIC;
  signal inst_edgedtctr4_n_5 : STD_LOGIC;
  signal inst_edgedtctr5_n_0 : STD_LOGIC;
  signal inst_edgedtctr5_n_2 : STD_LOGIC;
  signal inst_edgedtctr5_n_3 : STD_LOGIC;
  signal inst_edgedtctr6_n_0 : STD_LOGIC;
  signal inst_edgedtctr6_n_2 : STD_LOGIC;
  signal inst_edgedtctr6_n_3 : STD_LOGIC;
  signal inst_edgedtctr7_n_0 : STD_LOGIC;
  signal inst_edgedtctr7_n_2 : STD_LOGIC;
  signal inst_edgedtctr7_n_3 : STD_LOGIC;
  signal \^refresco__0\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal sreg_0 : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal sreg_1 : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \^sreg_reg[1]_1\ : STD_LOGIC;
begin
  \refresco__0\(2 downto 0) <= \^refresco__0\(2 downto 0);
  \sreg_reg[1]_1\ <= \^sreg_reg[1]_1\;
inst_edgedtctr0: entity work.edgedtctr
     port map (
      clk_BUFG => clk_BUFG,
      sreg(2 downto 0) => sreg_1(2 downto 0),
      \sreg_reg[0]_0\ => inst_edgedtctr0_n_3,
      \sreg_reg[0]_1\ => \sreg_reg[0]_2\,
      \sreg_reg[2]_0\(1) => \sreg_reg[0]_1\(2),
      \sreg_reg[2]_0\(0) => \sreg_reg[0]_1\(0),
      \sreg_reg[2]_1\ => inst_edgedtctr0_n_2,
      \valor_moneda_reg[0]\ => inst_edgedtctr3_n_2,
      \valor_moneda_reg[2]\ => inst_edgedtctr4_n_5,
      \valor_moneda_reg[2]_0\ => inst_edgedtctr2_n_4,
      \valor_moneda_reg[2]_1\ => inst_edgedtctr3_n_3,
      \valor_moneda_reg[2]_2\ => inst_edgedtctr1_n_0
    );
inst_edgedtctr1: entity work.edgedtctr_9
     port map (
      clk_BUFG => clk_BUFG,
      \sreg_reg[0]_0\ => \sreg_reg[0]_3\,
      \sreg_reg[2]_0\ => inst_edgedtctr1_n_0
    );
inst_edgedtctr2: entity work.edgedtctr_10
     port map (
      clk_BUFG => clk_BUFG,
      sreg(2 downto 0) => sreg_0(2 downto 0),
      \sreg_reg[0]_0\(0) => \sreg_reg[0]_1\(3),
      \sreg_reg[0]_1\ => \sreg_reg[0]_4\,
      \sreg_reg[2]_0\ => inst_edgedtctr2_n_4,
      \valor_moneda_reg[3]\ => inst_edgedtctr3_n_3,
      \valor_moneda_reg[3]_0\ => inst_edgedtctr1_n_0,
      \valor_moneda_reg[3]_1\ => inst_edgedtctr0_n_3
    );
inst_edgedtctr3: entity work.edgedtctr_11
     port map (
      clk_BUFG => clk_BUFG,
      sreg(2 downto 0) => sreg_0(2 downto 0),
      \sreg_reg[0]_0\ => inst_edgedtctr3_n_2,
      \sreg_reg[0]_1\ => \sreg_reg[0]_5\,
      \sreg_reg[2]_0\(1) => \sreg_reg[0]_1\(4),
      \sreg_reg[2]_0\(0) => \sreg_reg[0]_1\(1),
      \sreg_reg[2]_1\ => inst_edgedtctr3_n_3,
      \valor_moneda_reg[1]\ => inst_edgedtctr0_n_3,
      \valor_moneda_reg[1]_0\ => inst_edgedtctr2_n_4,
      \valor_moneda_reg[1]_1\ => inst_edgedtctr1_n_0
    );
inst_edgedtctr4: entity work.edgedtctr_12
     port map (
      clk_BUFG => clk_BUFG,
      sreg(2 downto 0) => sreg_1(2 downto 0),
      \sreg_reg[0]_0\(1 downto 0) => \sreg_reg[0]_1\(6 downto 5),
      \sreg_reg[0]_1\ => \sreg_reg[0]_6\,
      \sreg_reg[2]_0\ => inst_edgedtctr4_n_5,
      \valor_moneda_reg[5]\ => inst_edgedtctr3_n_2,
      \valor_moneda_reg[5]_0\ => inst_edgedtctr0_n_2,
      \valor_moneda_reg[5]_1\ => inst_edgedtctr1_n_0
    );
inst_edgedtctr5: entity work.edgedtctr_13
     port map (
      D(0) => D(1),
      \FSM_sequential_current_state_reg[0]\ => \^sreg_reg[1]_1\,
      Q(1) => inst_edgedtctr5_n_2,
      Q(0) => inst_edgedtctr5_n_3,
      clk_BUFG => clk_BUFG,
      \disp_next_reg[7][6]\ => inst_edgedtctr7_n_0,
      \disp_next_reg[7][6]_0\(1) => inst_edgedtctr7_n_2,
      \disp_next_reg[7][6]_0\(0) => inst_edgedtctr7_n_3,
      \refresco__0\(0) => \^refresco__0\(2),
      \sreg_reg[0]_0\ => inst_edgedtctr5_n_0,
      \sreg_reg[0]_1\ => \sreg_reg[0]_11\,
      \sreg_reg[1]_0\ => \sreg_reg[1]\
    );
inst_edgedtctr6: entity work.edgedtctr_14
     port map (
      Q(1) => inst_edgedtctr6_n_2,
      Q(0) => inst_edgedtctr6_n_3,
      clk_BUFG => clk_BUFG,
      \disp_next_reg[5][0]\(1) => inst_edgedtctr5_n_2,
      \disp_next_reg[5][0]\(0) => inst_edgedtctr5_n_3,
      \disp_next_reg[5][0]_0\ => inst_edgedtctr5_n_0,
      \disp_next_reg[6][3]\ => inst_edgedtctr7_n_0,
      \disp_next_reg[6][3]_0\(1) => inst_edgedtctr7_n_2,
      \disp_next_reg[6][3]_0\(0) => inst_edgedtctr7_n_3,
      \refresco__0\(0) => \^refresco__0\(1),
      \sreg_reg[0]_0\ => inst_edgedtctr6_n_0,
      \sreg_reg[0]_1\ => \sreg_reg[0]_10\,
      \sreg_reg[1]_0\ => \sreg_reg[1]_0\,
      \sreg_reg[1]_1\ => \sreg_reg[1]_2\
    );
inst_edgedtctr7: entity work.edgedtctr_15
     port map (
      D(0) => D(0),
      Q(1) => inst_edgedtctr7_n_2,
      Q(0) => inst_edgedtctr7_n_3,
      clk_BUFG => clk_BUFG,
      \disp_next_reg[2][0]\ => inst_edgedtctr5_n_0,
      \disp_next_reg[2][0]_0\(1) => inst_edgedtctr5_n_2,
      \disp_next_reg[2][0]_0\(0) => inst_edgedtctr5_n_3,
      \disp_next_reg[3][0]\ => inst_edgedtctr6_n_0,
      \disp_next_reg[3][0]_0\(1) => inst_edgedtctr6_n_2,
      \disp_next_reg[3][0]_0\(0) => inst_edgedtctr6_n_3,
      \refresco__0\(0) => \^refresco__0\(0),
      \sreg_reg[0]_0\ => inst_edgedtctr7_n_0,
      \sreg_reg[0]_1\ => \sreg_reg[0]_9\,
      \sreg_reg[1]_0\ => \^sreg_reg[1]_1\,
      \sreg_reg[1]_1\ => \sreg_reg[1]_3\
    );
inst_edgedtctr8: entity work.edgedtctr_16
     port map (
      E(0) => E(0),
      clk_BUFG => clk_BUFG,
      \refresco__0\(2 downto 0) => \^refresco__0\(2 downto 0),
      \sreg_reg[0]_0\ => \sreg_reg[0]\,
      \sreg_reg[0]_1\ => \sreg_reg[0]_8\,
      \sreg_reg[1]_0\ => \sreg_reg[1]_4\
    );
inst_edgedtctr9: entity work.edgedtctr_17
     port map (
      D(1 downto 0) => sreg(1 downto 0),
      \FSM_onehot_current_state_reg[0]\(0) => \FSM_onehot_current_state_reg[0]\(0),
      Q(1 downto 0) => Q(1 downto 0),
      clk_BUFG => clk_BUFG,
      sreg(0) => sreg(2),
      \sreg_reg[0]_0\ => \sreg_reg[0]_0\,
      \sreg_reg[0]_1\ => \sreg_reg[0]_7\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity display is
  port (
    dot_OBUF : out STD_LOGIC;
    \charact_dot_reg[0]_0\ : out STD_LOGIC;
    \reg_reg[0]\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \reg_reg[2]\ : out STD_LOGIC;
    \charact_dot_reg[0]_1\ : out STD_LOGIC;
    \reg_reg[2]_0\ : out STD_LOGIC;
    \reg_reg[2]_1\ : out STD_LOGIC;
    \reg_reg[2]_2\ : out STD_LOGIC;
    \reg_reg[1]\ : out STD_LOGIC;
    \reg_reg[2]_3\ : out STD_LOGIC;
    elem_OBUF : out STD_LOGIC_VECTOR ( 7 downto 0 );
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    \reg_reg[0]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    digit_OBUF : out STD_LOGIC_VECTOR ( 6 downto 0 );
    \reg_reg[2]_4\ : out STD_LOGIC;
    \reg_reg[0]_1\ : out STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    output0 : in STD_LOGIC;
    \caracter_reg[4]_0\ : in STD_LOGIC;
    \caracter_reg[4]_1\ : in STD_LOGIC;
    \caracter[6]_i_4\ : in STD_LOGIC;
    \caracter[6]_i_4_0\ : in STD_LOGIC;
    \caracter[6]_i_4_1\ : in STD_LOGIC;
    \caracter[6]_i_16\ : in STD_LOGIC;
    current_state : in STD_LOGIC_VECTOR ( 0 to 0 );
    reset_IBUF : in STD_LOGIC;
    dot_reg_0 : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 5 downto 0 )
  );
end display;

architecture STRUCTURE of display is
  signal Inst_contador_n_19 : STD_LOGIC;
  signal Inst_contador_n_22 : STD_LOGIC;
  signal caracter : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \^charact_dot_reg[0]_0\ : STD_LOGIC;
  signal dot : STD_LOGIC;
begin
  \charact_dot_reg[0]_0\ <= \^charact_dot_reg[0]_0\;
Inst_contador: entity work.\contador__parameterized1\
     port map (
      CO(0) => CO(0),
      Q(2 downto 0) => Q(2 downto 0),
      \caracter[6]_i_16\ => \caracter[6]_i_16\,
      \caracter[6]_i_4\ => Inst_contador_n_22,
      \caracter[6]_i_4_0\ => \caracter[6]_i_4\,
      \caracter[6]_i_4_1\ => \caracter[6]_i_4_0\,
      \caracter[6]_i_4_2\ => \caracter[6]_i_4_1\,
      \caracter_reg[4]\ => \caracter_reg[4]_0\,
      \caracter_reg[4]_0\ => \caracter_reg[4]_1\,
      \charact_dot_reg[0]\ => \charact_dot_reg[0]_1\,
      \charact_dot_reg[0]_0\ => Inst_contador_n_19,
      \charact_dot_reg[0]_1\ => \^charact_dot_reg[0]_0\,
      clk_BUFG => clk_BUFG,
      current_state(0) => current_state(0),
      dot => dot,
      dot_reg => dot_reg_0,
      elem_OBUF(7 downto 0) => elem_OBUF(7 downto 0),
      output0 => output0,
      \reg_reg[0]_0\ => \reg_reg[0]\,
      \reg_reg[0]_1\(0) => \reg_reg[0]_0\(0),
      \reg_reg[0]_2\ => \reg_reg[0]_1\,
      \reg_reg[1]_0\ => \reg_reg[1]\,
      \reg_reg[2]_0\ => \reg_reg[2]\,
      \reg_reg[2]_1\ => \reg_reg[2]_0\,
      \reg_reg[2]_2\ => \reg_reg[2]_1\,
      \reg_reg[2]_3\ => \reg_reg[2]_2\,
      \reg_reg[2]_4\ => \reg_reg[2]_3\,
      \reg_reg[2]_5\ => \reg_reg[2]_4\,
      reset_IBUF => reset_IBUF
    );
Inst_digito: entity work.digito
     port map (
      Q(5) => caracter(6),
      Q(4 downto 0) => caracter(4 downto 0),
      digit_OBUF(3) => digit_OBUF(4),
      digit_OBUF(2 downto 0) => digit_OBUF(2 downto 0)
    );
\caracter_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => D(0),
      Q => caracter(0)
    );
\caracter_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => D(1),
      Q => caracter(1)
    );
\caracter_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => D(2),
      Q => caracter(2)
    );
\caracter_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => D(3),
      Q => caracter(3)
    );
\caracter_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => D(4),
      Q => caracter(4)
    );
\caracter_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => D(5),
      Q => caracter(6)
    );
\charact_dot_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => Inst_contador_n_19,
      Q => \^charact_dot_reg[0]_0\,
      R => '0'
    );
\digit_OBUF[3]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFFB6FB3EA9CFFFF"
    )
        port map (
      I0 => caracter(3),
      I1 => caracter(2),
      I2 => caracter(0),
      I3 => caracter(1),
      I4 => caracter(4),
      I5 => caracter(6),
      O => digit_OBUF(3)
    );
\digit_OBUF[5]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFFFF7EBFFD3B3D3"
    )
        port map (
      I0 => caracter(0),
      I1 => caracter(4),
      I2 => caracter(6),
      I3 => caracter(1),
      I4 => caracter(3),
      I5 => caracter(2),
      O => digit_OBUF(5)
    );
\digit_OBUF[6]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F7FFC90FFFCFF16F"
    )
        port map (
      I0 => caracter(0),
      I1 => caracter(2),
      I2 => caracter(4),
      I3 => caracter(6),
      I4 => caracter(3),
      I5 => caracter(1),
      O => digit_OBUF(6)
    );
dot_reg: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_BUFG,
      CE => dot,
      D => Inst_contador_n_22,
      PRE => output0,
      Q => dot_OBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gestion_dinero is
  port (
    \FSM_sequential_current_state_reg[1]\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[1]_0\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[1]_1\ : out STD_LOGIC;
    \reg_reg[1]\ : out STD_LOGIC;
    \reg_reg[0]\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[1]_2\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[1]_3\ : out STD_LOGIC;
    \reg_reg[0]_0\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[0]\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[0]_0\ : out STD_LOGIC;
    \reg_reg[0]_1\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[1]_4\ : out STD_LOGIC;
    \reg_reg[1]_0\ : out STD_LOGIC;
    \total_out_reg[5]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \reg_reg[0]_2\ : out STD_LOGIC;
    \reg_reg[0]_3\ : out STD_LOGIC;
    \FSM_onehot_current_state_reg[3]_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    refresco_OBUF : out STD_LOGIC;
    \FSM_sequential_current_state_reg[0]_1\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[1]_5\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[1]_6\ : out STD_LOGIC;
    \disp_dinero[3]_6\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter_reg[2]_i_15\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    DI : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \total_reg[7]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \FSM_sequential_current_state_reg[1]_7\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \caracter[0]_i_4\ : in STD_LOGIC;
    \caracter[6]_i_4\ : in STD_LOGIC;
    \FSM_sequential_current_state_reg[1]_8\ : in STD_LOGIC;
    \caracter[0]_i_3\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter[1]_i_3\ : in STD_LOGIC;
    \caracter[1]_i_3_0\ : in STD_LOGIC;
    \caracter[6]_i_4_0\ : in STD_LOGIC;
    \caracter[6]_i_4_1\ : in STD_LOGIC;
    \caracter[3]_i_2\ : in STD_LOGIC;
    \caracter_reg[2]\ : in STD_LOGIC;
    \caracter_reg[2]_0\ : in STD_LOGIC;
    \caracter_reg[2]_1\ : in STD_LOGIC;
    \caracter_reg[2]_2\ : in STD_LOGIC;
    \caracter[2]_i_6\ : in STD_LOGIC;
    \caracter[6]_i_18\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_refresco[1]_4\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_refresco[5]_3\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \caracter_reg[2]_i_11\ : in STD_LOGIC;
    \disp_refresco[6]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    sreg : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \FSM_onehot_current_state_reg[0]_0\ : in STD_LOGIC;
    clr_dinero_r : in STD_LOGIC;
    \total_reg[7]_1\ : in STD_LOGIC_VECTOR ( 6 downto 0 );
    \caracter[3]_i_3\ : in STD_LOGIC;
    \caracter[3]_i_3_0\ : in STD_LOGIC;
    \FSM_sequential_current_state_reg[0]_2\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \FSM_sequential_current_state_reg[0]_3\ : in STD_LOGIC;
    \FSM_sequential_current_state_reg[0]_4\ : in STD_LOGIC;
    \FSM_sequential_current_state_reg[1]_9\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    \FSM_onehot_current_state_reg[2]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end gestion_dinero;

architecture STRUCTURE of gestion_dinero is
  signal \FSM_onehot_current_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[1]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[3]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[3]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[3]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[3]_i_5_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[3]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_onehot_current_state[3]_i_7_n_0\ : STD_LOGIC;
  signal \^fsm_onehot_current_state_reg[3]_0\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \FSM_sequential_current_state[1]_i_3_n_0\ : STD_LOGIC;
  signal \^fsm_sequential_current_state_reg[0]_0\ : STD_LOGIC;
  signal \^fsm_sequential_current_state_reg[1]_2\ : STD_LOGIC;
  signal bdf : STD_LOGIC;
  signal \caracter[2]_i_18_n_0\ : STD_LOGIC;
  signal clr_dinero : STD_LOGIC;
  signal \i__carry__0_i_1_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_2_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_4_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_5_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_6_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_7_n_0\ : STD_LOGIC;
  signal \i__carry_i_5_n_0\ : STD_LOGIC;
  signal \i__carry_i_6_n_0\ : STD_LOGIC;
  signal \i__carry_i_7_n_0\ : STD_LOGIC;
  signal \i__carry_i_8_n_0\ : STD_LOGIC;
  signal p_1_in : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal total : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal total0 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \total[7]_i_1_n_0\ : STD_LOGIC;
  signal \total_inferred__0/i__carry__0_n_4\ : STD_LOGIC;
  signal \total_inferred__0/i__carry__0_n_5\ : STD_LOGIC;
  signal \total_inferred__0/i__carry__0_n_6\ : STD_LOGIC;
  signal \total_inferred__0/i__carry__0_n_7\ : STD_LOGIC;
  signal \total_inferred__0/i__carry_n_0\ : STD_LOGIC;
  signal \total_inferred__0/i__carry_n_4\ : STD_LOGIC;
  signal \total_inferred__0/i__carry_n_5\ : STD_LOGIC;
  signal \total_inferred__0/i__carry_n_6\ : STD_LOGIC;
  signal \total_inferred__0/i__carry_n_7\ : STD_LOGIC;
  signal \total_out[3]_i_3_n_0\ : STD_LOGIC;
  signal \total_out[3]_i_4_n_0\ : STD_LOGIC;
  signal \total_out[3]_i_5_n_0\ : STD_LOGIC;
  signal \total_out[3]_i_6_n_0\ : STD_LOGIC;
  signal \total_out[7]_i_4_n_0\ : STD_LOGIC;
  signal \total_out[7]_i_5_n_0\ : STD_LOGIC;
  signal \total_out[7]_i_6_n_0\ : STD_LOGIC;
  signal \total_out_reg[3]_i_2_n_0\ : STD_LOGIC;
  signal \^total_out_reg[5]_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal total_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_total_inferred__0/i__carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_total_inferred__0/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_total_out_reg[3]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_total_out_reg[7]_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_onehot_current_state[3]_i_5\ : label is "soft_lutpair45";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[0]\ : label is "s2:1000,s1:0010,s0:0001,s3:0100";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[1]\ : label is "s2:1000,s1:0010,s0:0001,s3:0100";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[2]\ : label is "s2:1000,s1:0010,s0:0001,s3:0100";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[3]\ : label is "s2:1000,s1:0010,s0:0001,s3:0100";
  attribute SOFT_HLUTNM of \FSM_sequential_current_state[1]_i_1\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \caracter[2]_i_18\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \caracter[6]_i_27\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \caracter[6]_i_32\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of refresco_OBUF_inst_i_1 : label is "soft_lutpair45";
  attribute OPT_MODIFIED : string;
  attribute OPT_MODIFIED of \total_inferred__0/i__carry\ : label is "SWEEP";
  attribute OPT_MODIFIED of \total_inferred__0/i__carry__0\ : label is "SWEEP";
  attribute OPT_MODIFIED of \total_out_reg[3]_i_2\ : label is "SWEEP";
  attribute OPT_MODIFIED of \total_out_reg[7]_i_3\ : label is "SWEEP";
begin
  \FSM_onehot_current_state_reg[3]_0\(2 downto 0) <= \^fsm_onehot_current_state_reg[3]_0\(2 downto 0);
  \FSM_sequential_current_state_reg[0]_0\ <= \^fsm_sequential_current_state_reg[0]_0\;
  \FSM_sequential_current_state_reg[1]_2\ <= \^fsm_sequential_current_state_reg[1]_2\;
  \total_out_reg[5]_0\(0) <= \^total_out_reg[5]_0\(0);
\FSM_onehot_current_state[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000000000FFDF"
    )
        port map (
      I0 => \^fsm_onehot_current_state_reg[3]_0\(1),
      I1 => sreg(0),
      I2 => sreg(2),
      I3 => sreg(1),
      I4 => \FSM_onehot_current_state[3]_i_5_n_0\,
      I5 => \^fsm_onehot_current_state_reg[3]_0\(0),
      O => \FSM_onehot_current_state[0]_i_1_n_0\
    );
\FSM_onehot_current_state[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0100000000000000"
    )
        port map (
      I0 => \FSM_onehot_current_state[3]_i_7_n_0\,
      I1 => total_reg(7),
      I2 => total_reg(1),
      I3 => total_reg(6),
      I4 => total_reg(2),
      I5 => \^fsm_onehot_current_state_reg[3]_0\(0),
      O => \FSM_onehot_current_state[1]_i_1_n_0\
    );
\FSM_onehot_current_state[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF1F1F1"
    )
        port map (
      I0 => \FSM_onehot_current_state_reg[0]_0\,
      I1 => \FSM_onehot_current_state[3]_i_5_n_0\,
      I2 => \FSM_onehot_current_state[3]_i_6_n_0\,
      I3 => \^fsm_onehot_current_state_reg[3]_0\(0),
      I4 => total_reg(7),
      O => \FSM_onehot_current_state[3]_i_1_n_0\
    );
\FSM_onehot_current_state[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8AAAAAAAAAAAA"
    )
        port map (
      I0 => \^fsm_onehot_current_state_reg[3]_0\(0),
      I1 => \FSM_onehot_current_state[3]_i_7_n_0\,
      I2 => total_reg(7),
      I3 => total_reg(1),
      I4 => total_reg(6),
      I5 => total_reg(2),
      O => \FSM_onehot_current_state[3]_i_2_n_0\
    );
\FSM_onehot_current_state[3]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \FSM_sequential_current_state_reg[1]_7\,
      I1 => clr_dinero_r,
      O => \FSM_onehot_current_state[3]_i_3_n_0\
    );
\FSM_onehot_current_state[3]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \^fsm_onehot_current_state_reg[3]_0\(2),
      I1 => \^fsm_onehot_current_state_reg[3]_0\(1),
      I2 => bdf,
      O => \FSM_onehot_current_state[3]_i_5_n_0\
    );
\FSM_onehot_current_state[3]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FE00000000000000"
    )
        port map (
      I0 => total_reg(4),
      I1 => total_reg(3),
      I2 => total_reg(2),
      I3 => total_reg(5),
      I4 => \^fsm_onehot_current_state_reg[3]_0\(0),
      I5 => total_reg(6),
      O => \FSM_onehot_current_state[3]_i_6_n_0\
    );
\FSM_onehot_current_state[3]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEF"
    )
        port map (
      I0 => total_reg(3),
      I1 => total_reg(4),
      I2 => total_reg(5),
      I3 => total_reg(0),
      O => \FSM_onehot_current_state[3]_i_7_n_0\
    );
\FSM_onehot_current_state_reg[0]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_BUFG,
      CE => \FSM_onehot_current_state[3]_i_1_n_0\,
      D => \FSM_onehot_current_state[0]_i_1_n_0\,
      PRE => \FSM_onehot_current_state[3]_i_3_n_0\,
      Q => \^fsm_onehot_current_state_reg[3]_0\(0)
    );
\FSM_onehot_current_state_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \FSM_onehot_current_state[3]_i_1_n_0\,
      CLR => \FSM_onehot_current_state[3]_i_3_n_0\,
      D => \FSM_onehot_current_state[1]_i_1_n_0\,
      Q => \^fsm_onehot_current_state_reg[3]_0\(1)
    );
\FSM_onehot_current_state_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \FSM_onehot_current_state[3]_i_1_n_0\,
      CLR => \FSM_onehot_current_state[3]_i_3_n_0\,
      D => \FSM_onehot_current_state_reg[2]_0\(0),
      Q => bdf
    );
\FSM_onehot_current_state_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \FSM_onehot_current_state[3]_i_1_n_0\,
      CLR => \FSM_onehot_current_state[3]_i_3_n_0\,
      D => \FSM_onehot_current_state[3]_i_2_n_0\,
      Q => \^fsm_onehot_current_state_reg[3]_0\(2)
    );
\FSM_sequential_current_state[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0F0F00FD0F0FFFFD"
    )
        port map (
      I0 => \FSM_sequential_current_state_reg[0]_2\(0),
      I1 => \FSM_sequential_current_state_reg[0]_3\,
      I2 => \FSM_sequential_current_state[1]_i_3_n_0\,
      I3 => \FSM_sequential_current_state_reg[1]_7\,
      I4 => \FSM_sequential_current_state_reg[1]_8\,
      I5 => \FSM_sequential_current_state_reg[0]_4\,
      O => \FSM_sequential_current_state_reg[1]_5\
    );
\FSM_sequential_current_state[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF4A"
    )
        port map (
      I0 => \FSM_sequential_current_state_reg[1]_7\,
      I1 => \FSM_sequential_current_state_reg[1]_8\,
      I2 => \FSM_sequential_current_state[1]_i_3_n_0\,
      I3 => \FSM_sequential_current_state_reg[1]_9\,
      O => \FSM_sequential_current_state_reg[1]_6\
    );
\FSM_sequential_current_state[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAA003000000000"
    )
        port map (
      I0 => bdf,
      I1 => sreg(0),
      I2 => sreg(2),
      I3 => sreg(1),
      I4 => \FSM_sequential_current_state_reg[1]_7\,
      I5 => \FSM_sequential_current_state_reg[1]_8\,
      O => \FSM_sequential_current_state[1]_i_3_n_0\
    );
\caracter[1]_i_14\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5FF53FF30FF0F00F"
    )
        port map (
      I0 => \^total_out_reg[5]_0\(0),
      I1 => \disp_refresco[5]_3\(0),
      I2 => Q(0),
      I3 => \caracter_reg[2]_i_11\,
      I4 => \FSM_sequential_current_state_reg[1]_7\,
      I5 => \FSM_sequential_current_state_reg[1]_8\,
      O => \reg_reg[0]_3\
    );
\caracter[2]_i_17\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8282C3C33C00C3C3"
    )
        port map (
      I0 => \^total_out_reg[5]_0\(0),
      I1 => Q(0),
      I2 => \caracter_reg[2]_i_11\,
      I3 => \disp_refresco[6]_1\(0),
      I4 => \FSM_sequential_current_state_reg[1]_8\,
      I5 => \FSM_sequential_current_state_reg[1]_7\,
      O => \reg_reg[0]_2\
    );
\caracter[2]_i_18\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8F"
    )
        port map (
      I0 => \^total_out_reg[5]_0\(0),
      I1 => \FSM_sequential_current_state_reg[1]_7\,
      I2 => \FSM_sequential_current_state_reg[1]_8\,
      O => \caracter[2]_i_18_n_0\
    );
\caracter[6]_i_27\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4373"
    )
        port map (
      I0 => \^total_out_reg[5]_0\(0),
      I1 => \FSM_sequential_current_state_reg[1]_7\,
      I2 => \FSM_sequential_current_state_reg[1]_8\,
      I3 => \disp_refresco[5]_3\(0),
      O => \^fsm_sequential_current_state_reg[1]_2\
    );
\caracter[6]_i_32\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2F"
    )
        port map (
      I0 => \FSM_sequential_current_state_reg[1]_8\,
      I1 => \^total_out_reg[5]_0\(0),
      I2 => \FSM_sequential_current_state_reg[1]_7\,
      O => \^fsm_sequential_current_state_reg[0]_0\
    );
\i__carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^fsm_onehot_current_state_reg[3]_0\(0),
      I1 => \total_reg[7]_1\(6),
      O => \i__carry__0_i_1_n_0\
    );
\i__carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => total_reg(5),
      I1 => \^fsm_onehot_current_state_reg[3]_0\(0),
      O => \i__carry__0_i_2_n_0\
    );
\i__carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^fsm_onehot_current_state_reg[3]_0\(0),
      I1 => total_reg(7),
      O => \i__carry__0_i_4_n_0\
    );
\i__carry__0_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \total_reg[7]_1\(6),
      I1 => total_reg(6),
      I2 => \^fsm_onehot_current_state_reg[3]_0\(0),
      O => \i__carry__0_i_5_n_0\
    );
\i__carry__0_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"48"
    )
        port map (
      I0 => \total_reg[7]_1\(5),
      I1 => \^fsm_onehot_current_state_reg[3]_0\(0),
      I2 => total_reg(5),
      O => \i__carry__0_i_6_n_0\
    );
\i__carry__0_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \total_reg[7]_1\(4),
      I1 => total_reg(4),
      I2 => \^fsm_onehot_current_state_reg[3]_0\(0),
      O => \i__carry__0_i_7_n_0\
    );
\i__carry_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \total_reg[7]_1\(3),
      I1 => total_reg(3),
      I2 => \^fsm_onehot_current_state_reg[3]_0\(0),
      O => \i__carry_i_5_n_0\
    );
\i__carry_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \total_reg[7]_1\(2),
      I1 => total_reg(2),
      I2 => \^fsm_onehot_current_state_reg[3]_0\(0),
      O => \i__carry_i_6_n_0\
    );
\i__carry_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \total_reg[7]_1\(1),
      I1 => total_reg(1),
      I2 => \^fsm_onehot_current_state_reg[3]_0\(0),
      O => \i__carry_i_7_n_0\
    );
\i__carry_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \total_reg[7]_1\(0),
      I1 => total_reg(0),
      I2 => \^fsm_onehot_current_state_reg[3]_0\(0),
      O => \i__carry_i_8_n_0\
    );
inst_int_to_string: entity work.int_to_string
     port map (
      \FSM_sequential_current_state_reg[0]\ => \FSM_sequential_current_state_reg[0]\,
      \FSM_sequential_current_state_reg[0]_0\ => \FSM_sequential_current_state_reg[0]_1\,
      \FSM_sequential_current_state_reg[1]\ => \FSM_sequential_current_state_reg[1]\,
      \FSM_sequential_current_state_reg[1]_0\ => \FSM_sequential_current_state_reg[1]_0\,
      \FSM_sequential_current_state_reg[1]_1\ => \FSM_sequential_current_state_reg[1]_1\,
      \FSM_sequential_current_state_reg[1]_2\ => \FSM_sequential_current_state_reg[1]_3\,
      \FSM_sequential_current_state_reg[1]_3\ => \FSM_sequential_current_state_reg[1]_4\,
      Q(1 downto 0) => Q(1 downto 0),
      \caracter[0]_i_3\(0) => \caracter[0]_i_3\(0),
      \caracter[0]_i_4\ => \caracter[0]_i_4\,
      \caracter[1]_i_3\ => \^fsm_sequential_current_state_reg[1]_2\,
      \caracter[1]_i_3_0\ => \caracter[1]_i_3\,
      \caracter[1]_i_3_1\ => \caracter[1]_i_3_0\,
      \caracter[2]_i_6\ => \caracter[2]_i_18_n_0\,
      \caracter[2]_i_6_0\ => \caracter[2]_i_6\,
      \caracter[3]_i_2\ => \caracter[3]_i_2\,
      \caracter[3]_i_3\ => \caracter[3]_i_3\,
      \caracter[3]_i_3_0\ => \caracter[3]_i_3_0\,
      \caracter[6]_i_18_0\(0) => \caracter[6]_i_18\(0),
      \caracter[6]_i_38_0\ => \FSM_sequential_current_state_reg[1]_7\,
      \caracter[6]_i_4\ => \caracter[6]_i_4\,
      \caracter[6]_i_4_0\ => \^fsm_sequential_current_state_reg[0]_0\,
      \caracter[6]_i_4_1\ => \caracter[6]_i_4_0\,
      \caracter[6]_i_4_2\ => \caracter[6]_i_4_1\,
      \caracter_reg[1]_i_4\ => \FSM_sequential_current_state_reg[1]_8\,
      \caracter_reg[2]\ => \caracter_reg[2]\,
      \caracter_reg[2]_0\ => \caracter_reg[2]_0\,
      \caracter_reg[2]_1\ => \caracter_reg[2]_1\,
      \caracter_reg[2]_2\ => \caracter_reg[2]_2\,
      \caracter_reg[2]_i_15_0\ => \caracter_reg[2]_i_15\(0),
      \caracter_reg[3]_i_141_0\(7 downto 0) => total(7 downto 0),
      \disp_dinero[3]_6\(0) => \disp_dinero[3]_6\(0),
      \disp_refresco[1]_4\(0) => \disp_refresco[1]_4\(0),
      \disp_refresco[5]_3\(1 downto 0) => \disp_refresco[5]_3\(1 downto 0),
      \reg_reg[0]\ => \reg_reg[0]\,
      \reg_reg[0]_0\ => \reg_reg[0]_0\,
      \reg_reg[0]_1\ => \reg_reg[0]_1\,
      \reg_reg[1]\ => \reg_reg[1]\,
      \reg_reg[1]_0\ => \reg_reg[1]_0\,
      \total_out_reg[5]\ => \^total_out_reg[5]_0\(0)
    );
refresco_OBUF_inst_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => bdf,
      I1 => \^fsm_onehot_current_state_reg[3]_0\(1),
      O => refresco_OBUF
    );
\total[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"01FF"
    )
        port map (
      I0 => bdf,
      I1 => \^fsm_onehot_current_state_reg[3]_0\(1),
      I2 => \^fsm_onehot_current_state_reg[3]_0\(2),
      I3 => \^fsm_onehot_current_state_reg[3]_0\(0),
      O => \total[7]_i_1_n_0\
    );
\total_inferred__0/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \total_inferred__0/i__carry_n_0\,
      CO(2 downto 0) => \NLW_total_inferred__0/i__carry_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => DI(3 downto 0),
      O(3) => \total_inferred__0/i__carry_n_4\,
      O(2) => \total_inferred__0/i__carry_n_5\,
      O(1) => \total_inferred__0/i__carry_n_6\,
      O(0) => \total_inferred__0/i__carry_n_7\,
      S(3) => \i__carry_i_5_n_0\,
      S(2) => \i__carry_i_6_n_0\,
      S(1) => \i__carry_i_7_n_0\,
      S(0) => \i__carry_i_8_n_0\
    );
\total_inferred__0/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \total_inferred__0/i__carry_n_0\,
      CO(3 downto 0) => \NLW_total_inferred__0/i__carry__0_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \i__carry__0_i_1_n_0\,
      DI(1) => \i__carry__0_i_2_n_0\,
      DI(0) => \total_reg[7]_0\(0),
      O(3) => \total_inferred__0/i__carry__0_n_4\,
      O(2) => \total_inferred__0/i__carry__0_n_5\,
      O(1) => \total_inferred__0/i__carry__0_n_6\,
      O(0) => \total_inferred__0/i__carry__0_n_7\,
      S(3) => \i__carry__0_i_4_n_0\,
      S(2) => \i__carry__0_i_5_n_0\,
      S(1) => \i__carry__0_i_6_n_0\,
      S(0) => \i__carry__0_i_7_n_0\
    );
\total_out[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00020000"
    )
        port map (
      I0 => total0(0),
      I1 => bdf,
      I2 => \^fsm_onehot_current_state_reg[3]_0\(1),
      I3 => \^fsm_onehot_current_state_reg[3]_0\(2),
      I4 => \^fsm_onehot_current_state_reg[3]_0\(0),
      I5 => total(0),
      O => p_1_in(0)
    );
\total_out[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00020000"
    )
        port map (
      I0 => total0(1),
      I1 => bdf,
      I2 => \^fsm_onehot_current_state_reg[3]_0\(1),
      I3 => \^fsm_onehot_current_state_reg[3]_0\(2),
      I4 => \^fsm_onehot_current_state_reg[3]_0\(0),
      I5 => total(1),
      O => p_1_in(1)
    );
\total_out[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00020000"
    )
        port map (
      I0 => total0(2),
      I1 => bdf,
      I2 => \^fsm_onehot_current_state_reg[3]_0\(1),
      I3 => \^fsm_onehot_current_state_reg[3]_0\(2),
      I4 => \^fsm_onehot_current_state_reg[3]_0\(0),
      I5 => total(2),
      O => p_1_in(2)
    );
\total_out[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00020000"
    )
        port map (
      I0 => total0(3),
      I1 => bdf,
      I2 => \^fsm_onehot_current_state_reg[3]_0\(1),
      I3 => \^fsm_onehot_current_state_reg[3]_0\(2),
      I4 => \^fsm_onehot_current_state_reg[3]_0\(0),
      I5 => total(3),
      O => p_1_in(3)
    );
\total_out[3]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(3),
      I1 => \total_reg[7]_1\(3),
      O => \total_out[3]_i_3_n_0\
    );
\total_out[3]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(2),
      I1 => \total_reg[7]_1\(2),
      O => \total_out[3]_i_4_n_0\
    );
\total_out[3]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(1),
      I1 => \total_reg[7]_1\(1),
      O => \total_out[3]_i_5_n_0\
    );
\total_out[3]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(0),
      I1 => \total_reg[7]_1\(0),
      O => \total_out[3]_i_6_n_0\
    );
\total_out[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00020000"
    )
        port map (
      I0 => total0(4),
      I1 => bdf,
      I2 => \^fsm_onehot_current_state_reg[3]_0\(1),
      I3 => \^fsm_onehot_current_state_reg[3]_0\(2),
      I4 => \^fsm_onehot_current_state_reg[3]_0\(0),
      I5 => total(4),
      O => p_1_in(4)
    );
\total_out[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00020000"
    )
        port map (
      I0 => total0(5),
      I1 => bdf,
      I2 => \^fsm_onehot_current_state_reg[3]_0\(1),
      I3 => \^fsm_onehot_current_state_reg[3]_0\(2),
      I4 => \^fsm_onehot_current_state_reg[3]_0\(0),
      I5 => total(5),
      O => p_1_in(5)
    );
\total_out[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00020000"
    )
        port map (
      I0 => total0(6),
      I1 => bdf,
      I2 => \^fsm_onehot_current_state_reg[3]_0\(1),
      I3 => \^fsm_onehot_current_state_reg[3]_0\(2),
      I4 => \^fsm_onehot_current_state_reg[3]_0\(0),
      I5 => total(6),
      O => p_1_in(6)
    );
\total_out[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clr_dinero_r,
      I1 => \FSM_sequential_current_state_reg[1]_7\,
      O => clr_dinero
    );
\total_out[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00020000"
    )
        port map (
      I0 => total0(7),
      I1 => bdf,
      I2 => \^fsm_onehot_current_state_reg[3]_0\(1),
      I3 => \^fsm_onehot_current_state_reg[3]_0\(2),
      I4 => \^fsm_onehot_current_state_reg[3]_0\(0),
      I5 => total(7),
      O => p_1_in(7)
    );
\total_out[7]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(6),
      I1 => \total_reg[7]_1\(6),
      O => \total_out[7]_i_4_n_0\
    );
\total_out[7]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(5),
      I1 => \total_reg[7]_1\(5),
      O => \total_out[7]_i_5_n_0\
    );
\total_out[7]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(4),
      I1 => \total_reg[7]_1\(4),
      O => \total_out[7]_i_6_n_0\
    );
\total_out_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => clr_dinero,
      D => p_1_in(0),
      Q => total(0),
      R => '0'
    );
\total_out_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => clr_dinero,
      D => p_1_in(1),
      Q => total(1),
      R => '0'
    );
\total_out_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => clr_dinero,
      D => p_1_in(2),
      Q => total(2),
      R => '0'
    );
\total_out_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => clr_dinero,
      D => p_1_in(3),
      Q => total(3),
      R => '0'
    );
\total_out_reg[3]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \total_out_reg[3]_i_2_n_0\,
      CO(2 downto 0) => \NLW_total_out_reg[3]_i_2_CO_UNCONNECTED\(2 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => total_reg(3 downto 0),
      O(3 downto 0) => total0(3 downto 0),
      S(3) => \total_out[3]_i_3_n_0\,
      S(2) => \total_out[3]_i_4_n_0\,
      S(1) => \total_out[3]_i_5_n_0\,
      S(0) => \total_out[3]_i_6_n_0\
    );
\total_out_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => clr_dinero,
      D => p_1_in(4),
      Q => total(4),
      R => '0'
    );
\total_out_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => clr_dinero,
      D => p_1_in(5),
      Q => total(5),
      R => '0'
    );
\total_out_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => clr_dinero,
      D => p_1_in(6),
      Q => total(6),
      R => '0'
    );
\total_out_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => clr_dinero,
      D => p_1_in(7),
      Q => total(7),
      R => '0'
    );
\total_out_reg[7]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \total_out_reg[3]_i_2_n_0\,
      CO(3 downto 0) => \NLW_total_out_reg[7]_i_3_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => total_reg(6 downto 4),
      O(3 downto 0) => total0(7 downto 4),
      S(3) => total_reg(7),
      S(2) => \total_out[7]_i_4_n_0\,
      S(1) => \total_out[7]_i_5_n_0\,
      S(0) => \total_out[7]_i_6_n_0\
    );
\total_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \total[7]_i_1_n_0\,
      CLR => \FSM_onehot_current_state[3]_i_3_n_0\,
      D => \total_inferred__0/i__carry_n_7\,
      Q => total_reg(0)
    );
\total_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \total[7]_i_1_n_0\,
      CLR => \FSM_onehot_current_state[3]_i_3_n_0\,
      D => \total_inferred__0/i__carry_n_6\,
      Q => total_reg(1)
    );
\total_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \total[7]_i_1_n_0\,
      CLR => \FSM_onehot_current_state[3]_i_3_n_0\,
      D => \total_inferred__0/i__carry_n_5\,
      Q => total_reg(2)
    );
\total_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \total[7]_i_1_n_0\,
      CLR => \FSM_onehot_current_state[3]_i_3_n_0\,
      D => \total_inferred__0/i__carry_n_4\,
      Q => total_reg(3)
    );
\total_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \total[7]_i_1_n_0\,
      CLR => \FSM_onehot_current_state[3]_i_3_n_0\,
      D => \total_inferred__0/i__carry__0_n_7\,
      Q => total_reg(4)
    );
\total_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \total[7]_i_1_n_0\,
      CLR => \FSM_onehot_current_state[3]_i_3_n_0\,
      D => \total_inferred__0/i__carry__0_n_6\,
      Q => total_reg(5)
    );
\total_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \total[7]_i_1_n_0\,
      CLR => \FSM_onehot_current_state[3]_i_3_n_0\,
      D => \total_inferred__0/i__carry__0_n_5\,
      Q => total_reg(6)
    );
\total_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \total[7]_i_1_n_0\,
      CLR => \FSM_onehot_current_state[3]_i_3_n_0\,
      D => \total_inferred__0/i__carry__0_n_4\,
      Q => total_reg(7)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity prescaler is
  port (
    output0 : out STD_LOGIC;
    clk : out STD_LOGIC;
    clk_100Mh_IBUF_BUFG : in STD_LOGIC;
    reset_IBUF : in STD_LOGIC
  );
end prescaler;

architecture STRUCTURE of prescaler is
  signal \^clk\ : STD_LOGIC;
  signal uut_n_1 : STD_LOGIC;
begin
  clk <= \^clk\;
clk_pre_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      D => uut_n_1,
      Q => \^clk\,
      R => '0'
    );
uut: entity work.contador
     port map (
      clk => \^clk\,
      clk_100Mh_IBUF_BUFG => clk_100Mh_IBUF_BUFG,
      \reg_reg[10]_0\ => uut_n_1,
      reset => output0,
      reset_IBUF => reset_IBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity sincronizador is
  port (
    \sreg_reg[0]\ : out STD_LOGIC;
    \sreg_reg[0]_0\ : out STD_LOGIC;
    \sreg_reg[0]_1\ : out STD_LOGIC;
    \sreg_reg[0]_2\ : out STD_LOGIC;
    \sreg_reg[0]_3\ : out STD_LOGIC;
    \sreg_reg[0]_4\ : out STD_LOGIC;
    \sreg_reg[0]_5\ : out STD_LOGIC;
    \sreg_reg[0]_6\ : out STD_LOGIC;
    \sreg_reg[0]_7\ : out STD_LOGIC;
    \sreg_reg[0]_8\ : out STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    monedas_IBUF : in STD_LOGIC_VECTOR ( 4 downto 0 );
    button_ok_IBUF : in STD_LOGIC;
    ref_option_IBUF : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
end sincronizador;

architecture STRUCTURE of sincronizador is
begin
inst_synchrnzr0: entity work.synchrnzr
     port map (
      clk_BUFG => clk_BUFG,
      monedas_IBUF(0) => monedas_IBUF(4),
      \sreg_reg[0]_0\ => \sreg_reg[0]\
    );
inst_synchrnzr1: entity work.synchrnzr_0
     port map (
      clk_BUFG => clk_BUFG,
      monedas_IBUF(0) => monedas_IBUF(3),
      \sreg_reg[0]_0\ => \sreg_reg[0]_0\
    );
inst_synchrnzr2: entity work.synchrnzr_1
     port map (
      clk_BUFG => clk_BUFG,
      monedas_IBUF(0) => monedas_IBUF(2),
      \sreg_reg[0]_0\ => \sreg_reg[0]_1\
    );
inst_synchrnzr3: entity work.synchrnzr_2
     port map (
      clk_BUFG => clk_BUFG,
      monedas_IBUF(0) => monedas_IBUF(1),
      \sreg_reg[0]_0\ => \sreg_reg[0]_2\
    );
inst_synchrnzr4: entity work.synchrnzr_3
     port map (
      clk_BUFG => clk_BUFG,
      monedas_IBUF(0) => monedas_IBUF(0),
      \sreg_reg[0]_0\ => \sreg_reg[0]_3\
    );
inst_synchrnzr5: entity work.synchrnzr_4
     port map (
      clk_BUFG => clk_BUFG,
      ref_option_IBUF(0) => ref_option_IBUF(3),
      \sreg_reg[0]_0\ => \sreg_reg[0]_8\
    );
inst_synchrnzr6: entity work.synchrnzr_5
     port map (
      clk_BUFG => clk_BUFG,
      ref_option_IBUF(0) => ref_option_IBUF(2),
      \sreg_reg[0]_0\ => \sreg_reg[0]_7\
    );
inst_synchrnzr7: entity work.synchrnzr_6
     port map (
      clk_BUFG => clk_BUFG,
      ref_option_IBUF(0) => ref_option_IBUF(1),
      \sreg_reg[0]_0\ => \sreg_reg[0]_6\
    );
inst_synchrnzr8: entity work.synchrnzr_7
     port map (
      clk_BUFG => clk_BUFG,
      ref_option_IBUF(0) => ref_option_IBUF(0),
      \sreg_reg[0]_0\ => \sreg_reg[0]_5\
    );
inst_synchrnzr9: entity work.synchrnzr_8
     port map (
      button_ok_IBUF => button_ok_IBUF,
      clk_BUFG => clk_BUFG,
      \sreg_reg[0]_0\ => \sreg_reg[0]_4\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity fsm is
  port (
    current_state : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \FSM_sequential_current_state_reg[1]_0\ : out STD_LOGIC;
    \reg_reg[1]\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[1]_1\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[1]_2\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[0]_0\ : out STD_LOGIC;
    \reg_reg[0]\ : out STD_LOGIC;
    \FSM_onehot_current_state_reg[3]\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    refresco_OBUF : out STD_LOGIC;
    \FSM_sequential_current_state_reg[1]_3\ : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 5 downto 0 );
    \caracter[3]_i_7\ : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter[2]_i_17\ : in STD_LOGIC;
    \caracter[0]_i_5\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    output0 : in STD_LOGIC;
    \refresco__0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \caracter[6]_i_36\ : in STD_LOGIC;
    \caracter[6]_i_38\ : in STD_LOGIC;
    DI : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \total_reg[7]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter[6]_i_27\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \caracter_reg[4]\ : in STD_LOGIC;
    \caracter_reg[4]_0\ : in STD_LOGIC;
    \caracter_reg[0]\ : in STD_LOGIC;
    \caracter_reg[2]\ : in STD_LOGIC;
    \caracter_reg[1]\ : in STD_LOGIC;
    \caracter[6]_i_18\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter[0]_i_3\ : in STD_LOGIC;
    \caracter[0]_i_3_0\ : in STD_LOGIC;
    \caracter_reg[2]_i_11\ : in STD_LOGIC;
    \caracter[6]_i_4\ : in STD_LOGIC;
    sreg : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \FSM_onehot_current_state_reg[0]\ : in STD_LOGIC;
    \total_reg[7]_0\ : in STD_LOGIC_VECTOR ( 6 downto 0 );
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter_reg[4]_1\ : in STD_LOGIC;
    \caracter_reg[0]_0\ : in STD_LOGIC;
    \FSM_sequential_current_state_reg[0]_1\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \FSM_onehot_current_state_reg[2]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \FSM_sequential_current_state_reg[0]_2\ : in STD_LOGIC;
    \FSM_sequential_current_state_reg[0]_3\ : in STD_LOGIC;
    \FSM_sequential_current_state_reg[1]_4\ : in STD_LOGIC
  );
end fsm;

architecture STRUCTURE of fsm is
  signal \^fsm_sequential_current_state_reg[1]_1\ : STD_LOGIC;
  signal \^fsm_sequential_current_state_reg[1]_3\ : STD_LOGIC;
  signal Inst_gestion_dinero_n_0 : STD_LOGIC;
  signal Inst_gestion_dinero_n_1 : STD_LOGIC;
  signal Inst_gestion_dinero_n_10 : STD_LOGIC;
  signal Inst_gestion_dinero_n_11 : STD_LOGIC;
  signal Inst_gestion_dinero_n_12 : STD_LOGIC;
  signal Inst_gestion_dinero_n_14 : STD_LOGIC;
  signal Inst_gestion_dinero_n_15 : STD_LOGIC;
  signal Inst_gestion_dinero_n_2 : STD_LOGIC;
  signal Inst_gestion_dinero_n_20 : STD_LOGIC;
  signal Inst_gestion_dinero_n_21 : STD_LOGIC;
  signal Inst_gestion_dinero_n_22 : STD_LOGIC;
  signal Inst_gestion_dinero_n_3 : STD_LOGIC;
  signal Inst_gestion_dinero_n_4 : STD_LOGIC;
  signal Inst_gestion_dinero_n_5 : STD_LOGIC;
  signal Inst_gestion_dinero_n_6 : STD_LOGIC;
  signal Inst_gestion_dinero_n_7 : STD_LOGIC;
  signal Inst_gestion_dinero_n_8 : STD_LOGIC;
  signal Inst_gestion_dinero_n_9 : STD_LOGIC;
  signal Inst_gestion_refresco_n_10 : STD_LOGIC;
  signal Inst_gestion_refresco_n_11 : STD_LOGIC;
  signal Inst_gestion_refresco_n_13 : STD_LOGIC;
  signal Inst_gestion_refresco_n_14 : STD_LOGIC;
  signal Inst_gestion_refresco_n_16 : STD_LOGIC;
  signal Inst_gestion_refresco_n_4 : STD_LOGIC;
  signal Inst_gestion_refresco_n_5 : STD_LOGIC;
  signal Inst_gestion_refresco_n_8 : STD_LOGIC;
  signal Inst_gestion_refresco_n_9 : STD_LOGIC;
  signal \caracter[3]_i_10_n_0\ : STD_LOGIC;
  signal clr_dinero_r : STD_LOGIC;
  signal \^current_state\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \disp_dinero[2]_7\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \disp_dinero[3]_6\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \disp_dinero[5]_8\ : STD_LOGIC_VECTOR ( 4 to 4 );
  signal \disp_refresco[1]_4\ : STD_LOGIC_VECTOR ( 6 to 6 );
  signal \disp_refresco[2]_5\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \disp_refresco[5]_3\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \disp_refresco[6]_1\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_current_state_reg[0]\ : label is "s1:01,s2:10,s0:00,s3:11";
  attribute FSM_ENCODED_STATES of \FSM_sequential_current_state_reg[1]\ : label is "s1:01,s2:10,s0:00,s3:11";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \caracter[3]_i_10\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \caracter[6]_i_30\ : label is "soft_lutpair52";
begin
  \FSM_sequential_current_state_reg[1]_1\ <= \^fsm_sequential_current_state_reg[1]_1\;
  \FSM_sequential_current_state_reg[1]_3\ <= \^fsm_sequential_current_state_reg[1]_3\;
  current_state(1 downto 0) <= \^current_state\(1 downto 0);
\FSM_sequential_current_state_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => Inst_gestion_dinero_n_21,
      Q => \^current_state\(0)
    );
\FSM_sequential_current_state_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => Inst_gestion_dinero_n_22,
      Q => \^current_state\(1)
    );
Inst_gestion_dinero: entity work.gestion_dinero
     port map (
      DI(3 downto 0) => DI(3 downto 0),
      \FSM_onehot_current_state_reg[0]_0\ => \FSM_onehot_current_state_reg[0]\,
      \FSM_onehot_current_state_reg[2]_0\(0) => \FSM_onehot_current_state_reg[2]\(0),
      \FSM_onehot_current_state_reg[3]_0\(2 downto 0) => \FSM_onehot_current_state_reg[3]\(2 downto 0),
      \FSM_sequential_current_state_reg[0]\ => Inst_gestion_dinero_n_8,
      \FSM_sequential_current_state_reg[0]_0\ => Inst_gestion_dinero_n_9,
      \FSM_sequential_current_state_reg[0]_1\ => Inst_gestion_dinero_n_20,
      \FSM_sequential_current_state_reg[0]_2\(0) => \FSM_sequential_current_state_reg[0]_1\(1),
      \FSM_sequential_current_state_reg[0]_3\ => \FSM_sequential_current_state_reg[0]_2\,
      \FSM_sequential_current_state_reg[0]_4\ => \FSM_sequential_current_state_reg[0]_3\,
      \FSM_sequential_current_state_reg[1]\ => Inst_gestion_dinero_n_0,
      \FSM_sequential_current_state_reg[1]_0\ => Inst_gestion_dinero_n_1,
      \FSM_sequential_current_state_reg[1]_1\ => Inst_gestion_dinero_n_2,
      \FSM_sequential_current_state_reg[1]_2\ => Inst_gestion_dinero_n_5,
      \FSM_sequential_current_state_reg[1]_3\ => Inst_gestion_dinero_n_6,
      \FSM_sequential_current_state_reg[1]_4\ => Inst_gestion_dinero_n_11,
      \FSM_sequential_current_state_reg[1]_5\ => Inst_gestion_dinero_n_21,
      \FSM_sequential_current_state_reg[1]_6\ => Inst_gestion_dinero_n_22,
      \FSM_sequential_current_state_reg[1]_7\ => \^current_state\(1),
      \FSM_sequential_current_state_reg[1]_8\ => \^current_state\(0),
      \FSM_sequential_current_state_reg[1]_9\ => \FSM_sequential_current_state_reg[1]_4\,
      Q(1 downto 0) => Q(1 downto 0),
      \caracter[0]_i_3\(0) => \disp_refresco[2]_5\(0),
      \caracter[0]_i_4\ => Inst_gestion_refresco_n_5,
      \caracter[1]_i_3\ => \^fsm_sequential_current_state_reg[1]_1\,
      \caracter[1]_i_3_0\ => Inst_gestion_refresco_n_8,
      \caracter[2]_i_6\ => Inst_gestion_refresco_n_9,
      \caracter[3]_i_2\ => \caracter[3]_i_10_n_0\,
      \caracter[3]_i_3\ => Inst_gestion_refresco_n_4,
      \caracter[3]_i_3_0\ => \^fsm_sequential_current_state_reg[1]_3\,
      \caracter[6]_i_18\(0) => \caracter[6]_i_18\(0),
      \caracter[6]_i_4\ => Inst_gestion_refresco_n_16,
      \caracter[6]_i_4_0\ => Inst_gestion_refresco_n_11,
      \caracter[6]_i_4_1\ => Inst_gestion_refresco_n_10,
      \caracter_reg[2]\ => \caracter_reg[4]\,
      \caracter_reg[2]_0\ => Inst_gestion_refresco_n_13,
      \caracter_reg[2]_1\ => \caracter_reg[4]_0\,
      \caracter_reg[2]_2\ => Inst_gestion_refresco_n_14,
      \caracter_reg[2]_i_11\ => \caracter_reg[2]_i_11\,
      \caracter_reg[2]_i_15\(0) => \disp_dinero[2]_7\(3),
      clk_BUFG => clk_BUFG,
      clr_dinero_r => clr_dinero_r,
      \disp_dinero[3]_6\(0) => \disp_dinero[3]_6\(0),
      \disp_refresco[1]_4\(0) => \disp_refresco[1]_4\(6),
      \disp_refresco[5]_3\(1) => \disp_refresco[5]_3\(4),
      \disp_refresco[5]_3\(0) => \disp_refresco[5]_3\(0),
      \disp_refresco[6]_1\(0) => \disp_refresco[6]_1\(3),
      refresco_OBUF => refresco_OBUF,
      \reg_reg[0]\ => Inst_gestion_dinero_n_4,
      \reg_reg[0]_0\ => Inst_gestion_dinero_n_7,
      \reg_reg[0]_1\ => Inst_gestion_dinero_n_10,
      \reg_reg[0]_2\ => Inst_gestion_dinero_n_14,
      \reg_reg[0]_3\ => Inst_gestion_dinero_n_15,
      \reg_reg[1]\ => Inst_gestion_dinero_n_3,
      \reg_reg[1]_0\ => Inst_gestion_dinero_n_12,
      sreg(2 downto 0) => sreg(2 downto 0),
      \total_out_reg[5]_0\(0) => \disp_dinero[5]_8\(4),
      \total_reg[7]_0\(0) => \total_reg[7]\(0),
      \total_reg[7]_1\(6 downto 0) => \total_reg[7]_0\(6 downto 0)
    );
Inst_gestion_refresco: entity work.gestion_refresco
     port map (
      CO(0) => CO(0),
      D(5 downto 0) => D(5 downto 0),
      E(0) => E(0),
      \FSM_sequential_current_state_reg[0]\ => Inst_gestion_refresco_n_4,
      \FSM_sequential_current_state_reg[0]_0\ => Inst_gestion_refresco_n_5,
      \FSM_sequential_current_state_reg[0]_1\ => Inst_gestion_refresco_n_9,
      \FSM_sequential_current_state_reg[0]_2\ => Inst_gestion_refresco_n_10,
      \FSM_sequential_current_state_reg[0]_3\ => Inst_gestion_refresco_n_13,
      \FSM_sequential_current_state_reg[0]_4\ => Inst_gestion_refresco_n_16,
      \FSM_sequential_current_state_reg[0]_5\ => \FSM_sequential_current_state_reg[0]_0\,
      \FSM_sequential_current_state_reg[1]\ => \FSM_sequential_current_state_reg[1]_0\,
      \FSM_sequential_current_state_reg[1]_0\ => Inst_gestion_refresco_n_8,
      \FSM_sequential_current_state_reg[1]_1\ => Inst_gestion_refresco_n_11,
      \FSM_sequential_current_state_reg[1]_2\ => \FSM_sequential_current_state_reg[1]_2\,
      Q(2 downto 0) => Q(2 downto 0),
      \caracter[0]_i_3_0\ => Inst_gestion_dinero_n_1,
      \caracter[0]_i_3_1\ => \caracter[0]_i_3\,
      \caracter[0]_i_3_2\ => \caracter[0]_i_3_0\,
      \caracter[0]_i_5_0\ => \caracter[0]_i_5\,
      \caracter[1]_i_2_0\(0) => \disp_dinero[5]_8\(4),
      \caracter[1]_i_2_1\ => Inst_gestion_dinero_n_15,
      \caracter[1]_i_9\ => Inst_gestion_dinero_n_20,
      \caracter[2]_i_17\ => \caracter[2]_i_17\,
      \caracter[2]_i_3\ => Inst_gestion_dinero_n_14,
      \caracter[3]_i_3_0\ => Inst_gestion_dinero_n_9,
      \caracter[3]_i_3_1\ => \caracter[3]_i_10_n_0\,
      \caracter[3]_i_7_0\ => \caracter[3]_i_7\,
      \caracter[4]_i_2_0\(0) => \caracter[6]_i_18\(0),
      \caracter[4]_i_4_0\(1 downto 0) => \FSM_sequential_current_state_reg[0]_1\(1 downto 0),
      \caracter[6]_i_20\(0) => \disp_dinero[2]_7\(3),
      \caracter[6]_i_27\ => \caracter[6]_i_27\,
      \caracter[6]_i_36_0\ => \caracter[6]_i_36\,
      \caracter[6]_i_38\ => \caracter[6]_i_38\,
      \caracter[6]_i_4_0\ => Inst_gestion_dinero_n_5,
      \caracter[6]_i_4_1\ => \caracter[6]_i_4\,
      \caracter_reg[0]\ => Inst_gestion_dinero_n_0,
      \caracter_reg[0]_0\ => Inst_gestion_dinero_n_2,
      \caracter_reg[0]_1\ => Inst_gestion_dinero_n_3,
      \caracter_reg[0]_2\ => \caracter_reg[0]\,
      \caracter_reg[0]_3\ => Inst_gestion_dinero_n_7,
      \caracter_reg[0]_4\ => \caracter_reg[0]_0\,
      \caracter_reg[1]\ => Inst_gestion_dinero_n_4,
      \caracter_reg[1]_0\ => \caracter_reg[1]\,
      \caracter_reg[1]_1\ => Inst_gestion_dinero_n_6,
      \caracter_reg[1]_2\ => \^current_state\(1),
      \caracter_reg[1]_3\ => \^current_state\(0),
      \caracter_reg[1]_i_6_0\ => \caracter_reg[2]_i_11\,
      \caracter_reg[2]\ => \caracter_reg[2]\,
      \caracter_reg[2]_0\ => Inst_gestion_dinero_n_11,
      \caracter_reg[2]_i_2_0\ => Inst_gestion_dinero_n_12,
      \caracter_reg[3]\ => Inst_gestion_dinero_n_10,
      \caracter_reg[3]_0\ => Inst_gestion_dinero_n_8,
      \caracter_reg[4]\ => \caracter_reg[4]\,
      \caracter_reg[4]_0\ => \caracter_reg[4]_0\,
      \caracter_reg[4]_1\ => \caracter_reg[4]_1\,
      \disp_dinero[3]_6\(0) => \disp_dinero[3]_6\(0),
      \disp_refresco[1]_4\(0) => \disp_refresco[1]_4\(6),
      \disp_refresco[6]_1\(0) => \disp_refresco[6]_1\(3),
      \refresco__0\(2 downto 0) => \refresco__0\(2 downto 0),
      \reg_reg[0]\ => \reg_reg[0]\,
      \reg_reg[1]\ => \reg_reg[1]\,
      \reg_reg[1]_0\ => Inst_gestion_refresco_n_14,
      \sreg_reg[0]\(1) => \disp_refresco[5]_3\(4),
      \sreg_reg[0]\(0) => \disp_refresco[5]_3\(0),
      \sreg_reg[1]\(0) => \disp_refresco[2]_5\(0)
    );
\caracter[3]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \^current_state\(0),
      I1 => \^current_state\(1),
      O => \caracter[3]_i_10_n_0\
    );
\caracter[4]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^current_state\(1),
      I1 => \^current_state\(0),
      O => \^fsm_sequential_current_state_reg[1]_1\
    );
\caracter[6]_i_30\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \^current_state\(1),
      I1 => \^current_state\(0),
      O => \^fsm_sequential_current_state_reg[1]_3\
    );
clr_dinero_r_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => '1',
      Q => clr_dinero_r
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity top is
  port (
    clk_100Mh : in STD_LOGIC;
    monedas : in STD_LOGIC_VECTOR ( 4 downto 0 );
    ref_option : in STD_LOGIC_VECTOR ( 3 downto 0 );
    button_ok : in STD_LOGIC;
    reset : in STD_LOGIC;
    devolver : out STD_LOGIC;
    refresco : out STD_LOGIC;
    digit : out STD_LOGIC_VECTOR ( 6 downto 0 );
    dot : out STD_LOGIC;
    elem : out STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of top : entity is true;
  attribute ECO_CHECKSUM : string;
  attribute ECO_CHECKSUM of top : entity is "d0e89ba0";
  attribute digit_number : integer;
  attribute digit_number of top : entity is 8;
  attribute division_prescaler : integer;
  attribute division_prescaler of top : entity is 250000;
  attribute monedas_types : integer;
  attribute monedas_types of top : entity is 5;
  attribute num_refrescos : integer;
  attribute num_refrescos of top : entity is 4;
  attribute size_counter : integer;
  attribute size_counter of top : entity is 3;
  attribute size_prescaler : integer;
  attribute size_prescaler of top : entity is 17;
  attribute width_word : integer;
  attribute width_word of top : entity is 8;
end top;

architecture STRUCTURE of top is
  signal \Inst_gestion_dinero/aux\ : STD_LOGIC;
  signal \Inst_gestion_refresco/disp_next[1]_0\ : STD_LOGIC;
  signal button_ok_IBUF : STD_LOGIC;
  signal caracter : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal caracter2 : STD_LOGIC_VECTOR ( 3 to 3 );
  signal clk : STD_LOGIC;
  signal clk_100Mh_IBUF : STD_LOGIC;
  signal clk_100Mh_IBUF_BUFG : STD_LOGIC;
  signal clk_BUFG : STD_LOGIC;
  signal current_state : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal devolver_OBUF : STD_LOGIC;
  signal digit_OBUF : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal dot_OBUF : STD_LOGIC;
  signal elem_OBUF : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal inst_decodmonedas_n_0 : STD_LOGIC;
  signal inst_decodmonedas_n_1 : STD_LOGIC;
  signal inst_decodmonedas_n_11 : STD_LOGIC;
  signal inst_decodmonedas_n_12 : STD_LOGIC;
  signal inst_decodmonedas_n_13 : STD_LOGIC;
  signal inst_decodmonedas_n_2 : STD_LOGIC;
  signal inst_decodmonedas_n_3 : STD_LOGIC;
  signal inst_detectorflancos_n_10 : STD_LOGIC;
  signal inst_detectorflancos_n_11 : STD_LOGIC;
  signal inst_detectorflancos_n_12 : STD_LOGIC;
  signal inst_detectorflancos_n_13 : STD_LOGIC;
  signal inst_detectorflancos_n_14 : STD_LOGIC;
  signal inst_detectorflancos_n_15 : STD_LOGIC;
  signal inst_detectorflancos_n_16 : STD_LOGIC;
  signal inst_detectorflancos_n_17 : STD_LOGIC;
  signal inst_detectorflancos_n_18 : STD_LOGIC;
  signal inst_detectorflancos_n_19 : STD_LOGIC;
  signal inst_detectorflancos_n_20 : STD_LOGIC;
  signal inst_detectorflancos_n_21 : STD_LOGIC;
  signal inst_detectorflancos_n_22 : STD_LOGIC;
  signal inst_detectorflancos_n_23 : STD_LOGIC;
  signal inst_detectorflancos_n_24 : STD_LOGIC;
  signal inst_detectorflancos_n_3 : STD_LOGIC;
  signal inst_detectorflancos_n_4 : STD_LOGIC;
  signal inst_detectorflancos_n_5 : STD_LOGIC;
  signal inst_display_n_1 : STD_LOGIC;
  signal inst_display_n_10 : STD_LOGIC;
  signal inst_display_n_11 : STD_LOGIC;
  signal inst_display_n_12 : STD_LOGIC;
  signal inst_display_n_2 : STD_LOGIC;
  signal inst_display_n_30 : STD_LOGIC;
  signal inst_display_n_31 : STD_LOGIC;
  signal inst_display_n_6 : STD_LOGIC;
  signal inst_display_n_7 : STD_LOGIC;
  signal inst_display_n_8 : STD_LOGIC;
  signal inst_display_n_9 : STD_LOGIC;
  signal \inst_edgedtctr9/sreg\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal inst_fsm_n_12 : STD_LOGIC;
  signal inst_fsm_n_2 : STD_LOGIC;
  signal inst_fsm_n_3 : STD_LOGIC;
  signal inst_fsm_n_4 : STD_LOGIC;
  signal inst_fsm_n_5 : STD_LOGIC;
  signal inst_fsm_n_6 : STD_LOGIC;
  signal inst_fsm_n_7 : STD_LOGIC;
  signal inst_fsm_n_9 : STD_LOGIC;
  signal inst_sincronizador_n_0 : STD_LOGIC;
  signal inst_sincronizador_n_1 : STD_LOGIC;
  signal inst_sincronizador_n_2 : STD_LOGIC;
  signal inst_sincronizador_n_3 : STD_LOGIC;
  signal inst_sincronizador_n_4 : STD_LOGIC;
  signal inst_sincronizador_n_5 : STD_LOGIC;
  signal inst_sincronizador_n_6 : STD_LOGIC;
  signal inst_sincronizador_n_7 : STD_LOGIC;
  signal inst_sincronizador_n_8 : STD_LOGIC;
  signal inst_sincronizador_n_9 : STD_LOGIC;
  signal monedas_IBUF : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal output0 : STD_LOGIC;
  signal plusOp : STD_LOGIC_VECTOR ( 1 to 1 );
  signal ref_option_IBUF : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal refresco_OBUF : STD_LOGIC;
  signal \refresco__0\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal reg : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal reset_IBUF : STD_LOGIC;
  signal valor_moneda : STD_LOGIC_VECTOR ( 6 downto 0 );
begin
button_ok_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => button_ok,
      O => button_ok_IBUF
    );
clk_100Mh_IBUF_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => clk_100Mh_IBUF,
      O => clk_100Mh_IBUF_BUFG
    );
clk_100Mh_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => clk_100Mh,
      O => clk_100Mh_IBUF
    );
clk_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => clk,
      O => clk_BUFG
    );
devolver_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => devolver_OBUF,
      O => devolver
    );
\digit_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digit_OBUF(0),
      O => digit(0)
    );
\digit_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digit_OBUF(1),
      O => digit(1)
    );
\digit_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digit_OBUF(2),
      O => digit(2)
    );
\digit_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digit_OBUF(3),
      O => digit(3)
    );
\digit_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digit_OBUF(4),
      O => digit(4)
    );
\digit_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digit_OBUF(5),
      O => digit(5)
    );
\digit_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digit_OBUF(6),
      O => digit(6)
    );
dot_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => dot_OBUF,
      O => dot
    );
\elem_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => elem_OBUF(0),
      O => elem(0)
    );
\elem_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => elem_OBUF(1),
      O => elem(1)
    );
\elem_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => elem_OBUF(2),
      O => elem(2)
    );
\elem_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => elem_OBUF(3),
      O => elem(3)
    );
\elem_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => elem_OBUF(4),
      O => elem(4)
    );
\elem_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => elem_OBUF(5),
      O => elem(5)
    );
\elem_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => elem_OBUF(6),
      O => elem(6)
    );
\elem_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => elem_OBUF(7),
      O => elem(7)
    );
inst_decodmonedas: entity work.decod_monedas
     port map (
      D(6) => inst_detectorflancos_n_18,
      D(5) => inst_detectorflancos_n_19,
      D(4) => inst_detectorflancos_n_20,
      D(3) => inst_detectorflancos_n_21,
      D(2) => inst_detectorflancos_n_22,
      D(1) => inst_detectorflancos_n_23,
      D(0) => inst_detectorflancos_n_24,
      DI(3) => inst_decodmonedas_n_0,
      DI(2) => inst_decodmonedas_n_1,
      DI(1) => inst_decodmonedas_n_2,
      DI(0) => inst_decodmonedas_n_3,
      \FSM_sequential_current_state_reg[1]\ => inst_decodmonedas_n_13,
      Q(6 downto 0) => valor_moneda(6 downto 0),
      clk_BUFG => clk_BUFG,
      current_state(1 downto 0) => current_state(1 downto 0),
      \total_reg[3]\(0) => \Inst_gestion_dinero/aux\,
      \valor_moneda_reg[4]_0\(0) => inst_decodmonedas_n_11,
      \valor_moneda_reg[5]_0\ => inst_decodmonedas_n_12
    );
inst_detectorflancos: entity work.detectorflancos
     port map (
      D(1) => inst_detectorflancos_n_4,
      D(0) => inst_detectorflancos_n_5,
      E(0) => \Inst_gestion_refresco/disp_next[1]_0\,
      \FSM_onehot_current_state_reg[0]\(0) => inst_detectorflancos_n_17,
      Q(1) => inst_fsm_n_9,
      Q(0) => \Inst_gestion_dinero/aux\,
      clk_BUFG => clk_BUFG,
      \refresco__0\(2 downto 0) => \refresco__0\(3 downto 1),
      sreg(2 downto 0) => \inst_edgedtctr9/sreg\(2 downto 0),
      \sreg_reg[0]\ => inst_detectorflancos_n_15,
      \sreg_reg[0]_0\ => inst_detectorflancos_n_16,
      \sreg_reg[0]_1\(6) => inst_detectorflancos_n_18,
      \sreg_reg[0]_1\(5) => inst_detectorflancos_n_19,
      \sreg_reg[0]_1\(4) => inst_detectorflancos_n_20,
      \sreg_reg[0]_1\(3) => inst_detectorflancos_n_21,
      \sreg_reg[0]_1\(2) => inst_detectorflancos_n_22,
      \sreg_reg[0]_1\(1) => inst_detectorflancos_n_23,
      \sreg_reg[0]_1\(0) => inst_detectorflancos_n_24,
      \sreg_reg[0]_10\ => inst_sincronizador_n_8,
      \sreg_reg[0]_11\ => inst_sincronizador_n_9,
      \sreg_reg[0]_2\ => inst_sincronizador_n_0,
      \sreg_reg[0]_3\ => inst_sincronizador_n_1,
      \sreg_reg[0]_4\ => inst_sincronizador_n_2,
      \sreg_reg[0]_5\ => inst_sincronizador_n_3,
      \sreg_reg[0]_6\ => inst_sincronizador_n_4,
      \sreg_reg[0]_7\ => inst_sincronizador_n_5,
      \sreg_reg[0]_8\ => inst_sincronizador_n_6,
      \sreg_reg[0]_9\ => inst_sincronizador_n_7,
      \sreg_reg[1]\ => inst_detectorflancos_n_3,
      \sreg_reg[1]_0\ => inst_detectorflancos_n_10,
      \sreg_reg[1]_1\ => inst_detectorflancos_n_11,
      \sreg_reg[1]_2\ => inst_detectorflancos_n_12,
      \sreg_reg[1]_3\ => inst_detectorflancos_n_13,
      \sreg_reg[1]_4\ => inst_detectorflancos_n_14
    );
inst_display: entity work.display
     port map (
      CO(0) => caracter2(3),
      D(5) => caracter(6),
      D(4 downto 0) => caracter(4 downto 0),
      Q(2 downto 0) => reg(2 downto 0),
      \caracter[6]_i_16\ => inst_fsm_n_6,
      \caracter[6]_i_4\ => inst_fsm_n_12,
      \caracter[6]_i_4_0\ => inst_fsm_n_2,
      \caracter[6]_i_4_1\ => inst_fsm_n_7,
      \caracter_reg[4]_0\ => inst_fsm_n_4,
      \caracter_reg[4]_1\ => inst_fsm_n_5,
      \charact_dot_reg[0]_0\ => inst_display_n_1,
      \charact_dot_reg[0]_1\ => inst_display_n_7,
      clk_BUFG => clk_BUFG,
      current_state(0) => current_state(1),
      digit_OBUF(6 downto 0) => digit_OBUF(6 downto 0),
      dot_OBUF => dot_OBUF,
      dot_reg_0 => inst_fsm_n_3,
      elem_OBUF(7 downto 0) => elem_OBUF(7 downto 0),
      output0 => output0,
      \reg_reg[0]\ => inst_display_n_2,
      \reg_reg[0]_0\(0) => plusOp(1),
      \reg_reg[0]_1\ => inst_display_n_31,
      \reg_reg[1]\ => inst_display_n_11,
      \reg_reg[2]\ => inst_display_n_6,
      \reg_reg[2]_0\ => inst_display_n_8,
      \reg_reg[2]_1\ => inst_display_n_9,
      \reg_reg[2]_2\ => inst_display_n_10,
      \reg_reg[2]_3\ => inst_display_n_12,
      \reg_reg[2]_4\ => inst_display_n_30,
      reset_IBUF => reset_IBUF
    );
inst_fsm: entity work.fsm
     port map (
      CO(0) => caracter2(3),
      D(5) => caracter(6),
      D(4 downto 0) => caracter(4 downto 0),
      DI(3) => inst_decodmonedas_n_0,
      DI(2) => inst_decodmonedas_n_1,
      DI(1) => inst_decodmonedas_n_2,
      DI(0) => inst_decodmonedas_n_3,
      E(0) => \Inst_gestion_refresco/disp_next[1]_0\,
      \FSM_onehot_current_state_reg[0]\ => inst_detectorflancos_n_16,
      \FSM_onehot_current_state_reg[2]\(0) => inst_detectorflancos_n_17,
      \FSM_onehot_current_state_reg[3]\(2) => devolver_OBUF,
      \FSM_onehot_current_state_reg[3]\(1) => inst_fsm_n_9,
      \FSM_onehot_current_state_reg[3]\(0) => \Inst_gestion_dinero/aux\,
      \FSM_sequential_current_state_reg[0]_0\ => inst_fsm_n_6,
      \FSM_sequential_current_state_reg[0]_1\(1) => inst_detectorflancos_n_4,
      \FSM_sequential_current_state_reg[0]_1\(0) => inst_detectorflancos_n_5,
      \FSM_sequential_current_state_reg[0]_2\ => inst_detectorflancos_n_15,
      \FSM_sequential_current_state_reg[0]_3\ => inst_decodmonedas_n_12,
      \FSM_sequential_current_state_reg[1]_0\ => inst_fsm_n_2,
      \FSM_sequential_current_state_reg[1]_1\ => inst_fsm_n_4,
      \FSM_sequential_current_state_reg[1]_2\ => inst_fsm_n_5,
      \FSM_sequential_current_state_reg[1]_3\ => inst_fsm_n_12,
      \FSM_sequential_current_state_reg[1]_4\ => inst_decodmonedas_n_13,
      Q(2 downto 0) => reg(2 downto 0),
      \caracter[0]_i_3\ => inst_display_n_11,
      \caracter[0]_i_3_0\ => inst_display_n_31,
      \caracter[0]_i_5\ => inst_detectorflancos_n_11,
      \caracter[2]_i_17\ => inst_detectorflancos_n_12,
      \caracter[3]_i_7\ => inst_detectorflancos_n_3,
      \caracter[6]_i_18\(0) => plusOp(1),
      \caracter[6]_i_27\ => inst_detectorflancos_n_10,
      \caracter[6]_i_36\ => inst_detectorflancos_n_14,
      \caracter[6]_i_38\ => inst_detectorflancos_n_13,
      \caracter[6]_i_4\ => inst_display_n_9,
      \caracter_reg[0]\ => inst_display_n_8,
      \caracter_reg[0]_0\ => inst_display_n_6,
      \caracter_reg[1]\ => inst_display_n_10,
      \caracter_reg[2]\ => inst_display_n_12,
      \caracter_reg[2]_i_11\ => inst_display_n_1,
      \caracter_reg[4]\ => inst_display_n_30,
      \caracter_reg[4]_0\ => inst_display_n_7,
      \caracter_reg[4]_1\ => inst_display_n_2,
      clk_BUFG => clk_BUFG,
      current_state(1 downto 0) => current_state(1 downto 0),
      output0 => output0,
      refresco_OBUF => refresco_OBUF,
      \refresco__0\(2 downto 0) => \refresco__0\(3 downto 1),
      \reg_reg[0]\ => inst_fsm_n_7,
      \reg_reg[1]\ => inst_fsm_n_3,
      sreg(2 downto 0) => \inst_edgedtctr9/sreg\(2 downto 0),
      \total_reg[7]\(0) => inst_decodmonedas_n_11,
      \total_reg[7]_0\(6 downto 0) => valor_moneda(6 downto 0)
    );
inst_prescaler: entity work.prescaler
     port map (
      clk => clk,
      clk_100Mh_IBUF_BUFG => clk_100Mh_IBUF_BUFG,
      output0 => output0,
      reset_IBUF => reset_IBUF
    );
inst_sincronizador: entity work.sincronizador
     port map (
      button_ok_IBUF => button_ok_IBUF,
      clk_BUFG => clk_BUFG,
      monedas_IBUF(4 downto 0) => monedas_IBUF(4 downto 0),
      ref_option_IBUF(3 downto 0) => ref_option_IBUF(3 downto 0),
      \sreg_reg[0]\ => inst_sincronizador_n_0,
      \sreg_reg[0]_0\ => inst_sincronizador_n_1,
      \sreg_reg[0]_1\ => inst_sincronizador_n_2,
      \sreg_reg[0]_2\ => inst_sincronizador_n_3,
      \sreg_reg[0]_3\ => inst_sincronizador_n_4,
      \sreg_reg[0]_4\ => inst_sincronizador_n_5,
      \sreg_reg[0]_5\ => inst_sincronizador_n_6,
      \sreg_reg[0]_6\ => inst_sincronizador_n_7,
      \sreg_reg[0]_7\ => inst_sincronizador_n_8,
      \sreg_reg[0]_8\ => inst_sincronizador_n_9
    );
\monedas_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => monedas(0),
      O => monedas_IBUF(0)
    );
\monedas_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => monedas(1),
      O => monedas_IBUF(1)
    );
\monedas_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => monedas(2),
      O => monedas_IBUF(2)
    );
\monedas_IBUF[3]_inst\: unisim.vcomponents.IBUF
     port map (
      I => monedas(3),
      O => monedas_IBUF(3)
    );
\monedas_IBUF[4]_inst\: unisim.vcomponents.IBUF
     port map (
      I => monedas(4),
      O => monedas_IBUF(4)
    );
\ref_option_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ref_option(0),
      O => ref_option_IBUF(0)
    );
\ref_option_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ref_option(1),
      O => ref_option_IBUF(1)
    );
\ref_option_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ref_option(2),
      O => ref_option_IBUF(2)
    );
\ref_option_IBUF[3]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ref_option(3),
      O => ref_option_IBUF(3)
    );
refresco_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => refresco_OBUF,
      O => refresco
    );
reset_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => reset,
      O => reset_IBUF
    );
end STRUCTURE;
