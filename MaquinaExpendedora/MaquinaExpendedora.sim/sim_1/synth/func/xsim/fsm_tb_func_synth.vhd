-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Sun Jan  3 12:41:46 2021
-- Host        : LAPTOP-7BL7BHFF running 64-bit major release  (build 9200)
-- Command     : write_vhdl -mode funcsim -nolib -force -file
--               C:/Users/Inees/Desktop/Trabajo_SED/MaquinaExpendedora_aux/MaquinaExpendedora_aux.sim/sim_1/synth/func/xsim/fsm_tb_func_synth.vhd
-- Design      : fsm
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tcsg324-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gestion_refresco is
  port (
    \disp_refresco[5]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_aux_reg[6][3]_i_2_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_aux_reg[6][3]_i_2_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_aux_reg[6][3]_i_2_2\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_refresco[7]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_aux_reg[6][3]_i_2_3\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp[1]_OBUF\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \disp[4]_OBUF\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \disp[3]_OBUF\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \disp[8]_OBUF\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \disp[7]_OBUF\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \disp[5]_OBUF\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \disp[6]_OBUF\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \disp[2]_OBUF\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp[1][1]\ : in STD_LOGIC;
    \disp[3][1]\ : in STD_LOGIC;
    ref_option_IBUF : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \disp_dinero[3]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp[2][3]\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end gestion_refresco;

architecture STRUCTURE of gestion_refresco is
  signal \^e\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \disp[3][1]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[2][0]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[2][1]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[2][4]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[3][0]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[3][1]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[3][6]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[4][1]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[4][4]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[4][6]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[5][0]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[6][1]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[6][2]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[6][3]_i_1_n_0\ : STD_LOGIC;
  signal \^disp_aux_reg[6][3]_i_2_0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^disp_aux_reg[6][3]_i_2_1\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^disp_aux_reg[6][3]_i_2_2\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^disp_aux_reg[6][3]_i_2_3\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \disp_aux_reg[6][6]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[7][3]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[8][0]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[8][1]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[8][2]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[8][3]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[8][6]_i_1_n_0\ : STD_LOGIC;
  signal \disp_refresco[2]\ : STD_LOGIC_VECTOR ( 4 to 4 );
  signal \disp_refresco[3]\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \disp_refresco[4]\ : STD_LOGIC_VECTOR ( 4 to 4 );
  signal \disp_refresco[6]\ : STD_LOGIC_VECTOR ( 6 downto 1 );
  signal \^disp_refresco[7]\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \disp_refresco[8]\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \disp[1][1]_INST_0_i_1\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \disp[1][2]_INST_0_i_1\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \disp[1][6]_INST_0_i_1\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \disp[2][3]_INST_0_i_1\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \disp[2][4]_INST_0_i_1\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \disp[3][0]_INST_0_i_1\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \disp[3][4]_INST_0_i_1\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \disp[3][6]_INST_0_i_1\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \disp[4][0]_INST_0_i_1\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \disp[4][3]_INST_0_i_1\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \disp[4][4]_INST_0_i_1\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \disp[4][6]_INST_0_i_1\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \disp[5][1]_INST_0_i_1\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \disp[5][2]_INST_0_i_1\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \disp[5][3]_INST_0_i_1\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \disp[6][1]_INST_0_i_1\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \disp[6][2]_INST_0_i_1\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \disp[6][3]_INST_0_i_1\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \disp[6][6]_INST_0_i_1\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \disp[7][2]_INST_0_i_1\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \disp[7][3]_INST_0_i_1\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \disp[8][0]_INST_0_i_1\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \disp[8][1]_INST_0_i_1\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \disp[8][2]_INST_0_i_1\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \disp[8][3]_INST_0_i_1\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \disp[8][6]_INST_0_i_1\ : label is "soft_lutpair53";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[2][0]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[2][0]_i_1\ : label is "soft_lutpair42";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[2][1]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[2][1]_i_1\ : label is "soft_lutpair40";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[2][4]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[2][4]_i_1\ : label is "soft_lutpair50";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[3][0]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[3][0]_i_1\ : label is "soft_lutpair36";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[3][1]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[3][1]_i_1\ : label is "soft_lutpair42";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[3][6]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[3][6]_i_1\ : label is "soft_lutpair41";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[4][1]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[4][1]_i_1\ : label is "soft_lutpair44";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[4][4]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[4][6]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[4][6]_i_1\ : label is "soft_lutpair36";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[5][0]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[5][0]_i_1\ : label is "soft_lutpair38";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[6][1]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[6][1]_i_1\ : label is "soft_lutpair50";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[6][2]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[6][2]_i_1\ : label is "soft_lutpair43";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[6][3]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[6][3]_i_1\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \disp_aux_reg[6][3]_i_2\ : label is "soft_lutpair41";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[6][6]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[6][6]_i_1\ : label is "soft_lutpair37";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[7][3]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[7][3]_i_1\ : label is "soft_lutpair44";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[8][0]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[8][0]_i_1\ : label is "soft_lutpair56";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[8][1]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[8][1]_i_1\ : label is "soft_lutpair43";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[8][2]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[8][2]_i_1\ : label is "soft_lutpair56";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[8][3]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[8][3]_i_1\ : label is "soft_lutpair37";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[8][6]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[8][6]_i_1\ : label is "soft_lutpair38";
begin
  E(0) <= \^e\(0);
  \disp_aux_reg[6][3]_i_2_0\(1 downto 0) <= \^disp_aux_reg[6][3]_i_2_0\(1 downto 0);
  \disp_aux_reg[6][3]_i_2_1\(0) <= \^disp_aux_reg[6][3]_i_2_1\(0);
  \disp_aux_reg[6][3]_i_2_2\(0) <= \^disp_aux_reg[6][3]_i_2_2\(0);
  \disp_aux_reg[6][3]_i_2_3\(1 downto 0) <= \^disp_aux_reg[6][3]_i_2_3\(1 downto 0);
  \disp_refresco[7]\(0) <= \^disp_refresco[7]\(0);
\disp[1][1]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F5DD"
    )
        port map (
      I0 => Q(0),
      I1 => \^disp_aux_reg[6][3]_i_2_3\(1),
      I2 => \disp[1][1]\,
      I3 => Q(1),
      O => \disp[1]_OBUF\(1)
    );
\disp[1][2]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3BFB"
    )
        port map (
      I0 => \^disp_aux_reg[6][3]_i_2_2\(0),
      I1 => Q(0),
      I2 => Q(1),
      I3 => \disp[1][1]\,
      O => \disp[1]_OBUF\(2)
    );
\disp[1][3]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => Q(1),
      I1 => \^disp_aux_reg[6][3]_i_2_3\(1),
      I2 => Q(0),
      O => \disp[1]_OBUF\(3)
    );
\disp[1][6]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => \^disp_aux_reg[6][3]_i_2_1\(0),
      I1 => Q(0),
      I2 => Q(1),
      O => \disp[1]_OBUF\(0)
    );
\disp[2][3]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"E200"
    )
        port map (
      I0 => \disp_refresco[8]\(1),
      I1 => Q(1),
      I2 => \disp[2][3]\(0),
      I3 => Q(0),
      O => \disp[2]_OBUF\(0)
    );
\disp[2][4]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => Q(1),
      I1 => \disp_refresco[2]\(4),
      I2 => Q(0),
      O => \disp[2]_OBUF\(1)
    );
\disp[3][0]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"E2FF"
    )
        port map (
      I0 => \disp_refresco[3]\(0),
      I1 => Q(1),
      I2 => \disp_dinero[3]\(0),
      I3 => Q(0),
      O => \disp[3]_OBUF\(0)
    );
\disp[3][1]_INST_0_i_1\: unisim.vcomponents.MUXF7
     port map (
      I0 => \disp[3][1]_INST_0_i_2_n_0\,
      I1 => \disp[3][1]\,
      O => \disp[3]_OBUF\(1),
      S => Q(1)
    );
\disp[3][1]_INST_0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \disp_refresco[3]\(1),
      I1 => Q(0),
      O => \disp[3][1]_INST_0_i_2_n_0\
    );
\disp[3][4]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"CB"
    )
        port map (
      I0 => \disp_refresco[4]\(4),
      I1 => Q(0),
      I2 => Q(1),
      O => \disp[3]_OBUF\(2)
    );
\disp[3][6]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => Q(1),
      I1 => \^disp_aux_reg[6][3]_i_2_1\(0),
      I2 => Q(0),
      O => \disp[3]_OBUF\(3)
    );
\disp[4][0]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"31"
    )
        port map (
      I0 => Q(0),
      I1 => Q(1),
      I2 => \^disp_aux_reg[6][3]_i_2_0\(1),
      O => \disp[4]_OBUF\(0)
    );
\disp[4][3]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2CEC"
    )
        port map (
      I0 => \disp_refresco[8]\(3),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \disp[1][1]\,
      O => \disp[4]_OBUF\(1)
    );
\disp[4][4]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \disp_refresco[4]\(4),
      I1 => Q(0),
      I2 => Q(1),
      O => \disp[4]_OBUF\(2)
    );
\disp[4][6]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => Q(1),
      I1 => \^disp_aux_reg[6][3]_i_2_0\(1),
      I2 => Q(0),
      O => \disp[2]_OBUF\(2)
    );
\disp[5][1]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => Q(1),
      I1 => \disp_refresco[8]\(3),
      I2 => Q(0),
      O => \disp[5]_OBUF\(0)
    );
\disp[5][2]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \disp_refresco[6]\(2),
      I1 => Q(0),
      I2 => Q(1),
      O => \disp[5]_OBUF\(1)
    );
\disp[5][3]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4A"
    )
        port map (
      I0 => Q(1),
      I1 => \disp_refresco[8]\(3),
      I2 => Q(0),
      O => \disp[5]_OBUF\(2)
    );
\disp[6][1]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => Q(1),
      I1 => \disp_refresco[6]\(1),
      I2 => Q(0),
      O => \disp[6]_OBUF\(1)
    );
\disp[6][2]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => Q(1),
      I1 => \disp_refresco[6]\(2),
      I2 => Q(0),
      O => \disp[6]_OBUF\(2)
    );
\disp[6][3]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \disp_refresco[6]\(3),
      I1 => Q(0),
      I2 => Q(1),
      O => \disp[6]_OBUF\(3)
    );
\disp[6][6]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => Q(1),
      I1 => \disp_refresco[6]\(6),
      I2 => Q(0),
      O => \disp[5]_OBUF\(3)
    );
\disp[7][2]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"31"
    )
        port map (
      I0 => Q(0),
      I1 => Q(1),
      I2 => \disp_refresco[8]\(0),
      O => \disp[7]_OBUF\(2)
    );
\disp[7][3]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^disp_refresco[7]\(0),
      I1 => Q(0),
      I2 => Q(1),
      O => \disp[7]_OBUF\(1)
    );
\disp[8][0]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \disp_refresco[8]\(0),
      I1 => Q(0),
      I2 => Q(1),
      O => \disp[6]_OBUF\(0)
    );
\disp[8][1]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"31"
    )
        port map (
      I0 => Q(0),
      I1 => Q(1),
      I2 => \disp_refresco[8]\(1),
      O => \disp[8]_OBUF\(0)
    );
\disp[8][2]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^disp_aux_reg[6][3]_i_2_2\(0),
      I1 => Q(0),
      I2 => Q(1),
      O => \disp[8]_OBUF\(1)
    );
\disp[8][3]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \disp_refresco[8]\(3),
      I1 => Q(0),
      I2 => Q(1),
      O => \disp[8]_OBUF\(2)
    );
\disp[8][6]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => Q(1),
      I1 => \disp_refresco[8]\(6),
      I2 => Q(0),
      O => \disp[7]_OBUF\(0)
    );
\disp_aux_reg[2][0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[2][0]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \^disp_aux_reg[6][3]_i_2_3\(0)
    );
\disp_aux_reg[2][0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0105"
    )
        port map (
      I0 => ref_option_IBUF(1),
      I1 => ref_option_IBUF(2),
      I2 => ref_option_IBUF(3),
      I3 => ref_option_IBUF(0),
      O => \disp_aux_reg[2][0]_i_1_n_0\
    );
\disp_aux_reg[2][1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[2][1]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \^disp_aux_reg[6][3]_i_2_3\(1)
    );
\disp_aux_reg[2][1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEEA"
    )
        port map (
      I0 => ref_option_IBUF(1),
      I1 => ref_option_IBUF(0),
      I2 => ref_option_IBUF(3),
      I3 => ref_option_IBUF(2),
      O => \disp_aux_reg[2][1]_i_1_n_0\
    );
\disp_aux_reg[2][4]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[2][4]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \disp_refresco[2]\(4)
    );
\disp_aux_reg[2][4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => ref_option_IBUF(1),
      I1 => ref_option_IBUF(2),
      I2 => ref_option_IBUF(3),
      O => \disp_aux_reg[2][4]_i_1_n_0\
    );
\disp_aux_reg[3][0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[3][0]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \disp_refresco[3]\(0)
    );
\disp_aux_reg[3][0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0111"
    )
        port map (
      I0 => ref_option_IBUF(1),
      I1 => ref_option_IBUF(2),
      I2 => ref_option_IBUF(3),
      I3 => ref_option_IBUF(0),
      O => \disp_aux_reg[3][0]_i_1_n_0\
    );
\disp_aux_reg[3][1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[3][1]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \disp_refresco[3]\(1)
    );
\disp_aux_reg[3][1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEC"
    )
        port map (
      I0 => ref_option_IBUF(1),
      I1 => ref_option_IBUF(3),
      I2 => ref_option_IBUF(2),
      I3 => ref_option_IBUF(0),
      O => \disp_aux_reg[3][1]_i_1_n_0\
    );
\disp_aux_reg[3][6]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[3][6]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \^disp_aux_reg[6][3]_i_2_1\(0)
    );
\disp_aux_reg[3][6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0017"
    )
        port map (
      I0 => ref_option_IBUF(2),
      I1 => ref_option_IBUF(3),
      I2 => ref_option_IBUF(0),
      I3 => ref_option_IBUF(1),
      O => \disp_aux_reg[3][6]_i_1_n_0\
    );
\disp_aux_reg[4][1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[4][1]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \^disp_aux_reg[6][3]_i_2_0\(0)
    );
\disp_aux_reg[4][1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEFC"
    )
        port map (
      I0 => ref_option_IBUF(0),
      I1 => ref_option_IBUF(3),
      I2 => ref_option_IBUF(2),
      I3 => ref_option_IBUF(1),
      O => \disp_aux_reg[4][1]_i_1_n_0\
    );
\disp_aux_reg[4][4]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[4][4]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \disp_refresco[4]\(4)
    );
\disp_aux_reg[4][4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => ref_option_IBUF(1),
      I1 => ref_option_IBUF(3),
      I2 => ref_option_IBUF(0),
      O => \disp_aux_reg[4][4]_i_1_n_0\
    );
\disp_aux_reg[4][6]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[4][6]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \^disp_aux_reg[6][3]_i_2_0\(1)
    );
\disp_aux_reg[4][6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0117"
    )
        port map (
      I0 => ref_option_IBUF(2),
      I1 => ref_option_IBUF(0),
      I2 => ref_option_IBUF(3),
      I3 => ref_option_IBUF(1),
      O => \disp_aux_reg[4][6]_i_1_n_0\
    );
\disp_aux_reg[5][0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[5][0]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \disp_refresco[5]\(0)
    );
\disp_aux_reg[5][0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0013"
    )
        port map (
      I0 => ref_option_IBUF(2),
      I1 => ref_option_IBUF(0),
      I2 => ref_option_IBUF(3),
      I3 => ref_option_IBUF(1),
      O => \disp_aux_reg[5][0]_i_1_n_0\
    );
\disp_aux_reg[6][1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[6][1]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \disp_refresco[6]\(1)
    );
\disp_aux_reg[6][1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => ref_option_IBUF(3),
      I1 => ref_option_IBUF(2),
      I2 => ref_option_IBUF(1),
      O => \disp_aux_reg[6][1]_i_1_n_0\
    );
\disp_aux_reg[6][2]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[6][2]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \disp_refresco[6]\(2)
    );
\disp_aux_reg[6][2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0105"
    )
        port map (
      I0 => ref_option_IBUF(0),
      I1 => ref_option_IBUF(2),
      I2 => ref_option_IBUF(3),
      I3 => ref_option_IBUF(1),
      O => \disp_aux_reg[6][2]_i_1_n_0\
    );
\disp_aux_reg[6][3]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[6][3]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \disp_refresco[6]\(3)
    );
\disp_aux_reg[6][3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFF8"
    )
        port map (
      I0 => ref_option_IBUF(0),
      I1 => ref_option_IBUF(3),
      I2 => ref_option_IBUF(2),
      I3 => ref_option_IBUF(1),
      O => \disp_aux_reg[6][3]_i_1_n_0\
    );
\disp_aux_reg[6][3]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => ref_option_IBUF(3),
      I1 => ref_option_IBUF(1),
      I2 => ref_option_IBUF(2),
      I3 => ref_option_IBUF(0),
      O => \^e\(0)
    );
\disp_aux_reg[6][6]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[6][6]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \disp_refresco[6]\(6)
    );
\disp_aux_reg[6][6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0115"
    )
        port map (
      I0 => ref_option_IBUF(0),
      I1 => ref_option_IBUF(2),
      I2 => ref_option_IBUF(3),
      I3 => ref_option_IBUF(1),
      O => \disp_aux_reg[6][6]_i_1_n_0\
    );
\disp_aux_reg[7][3]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[7][3]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \^disp_refresco[7]\(0)
    );
\disp_aux_reg[7][3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEF8"
    )
        port map (
      I0 => ref_option_IBUF(2),
      I1 => ref_option_IBUF(0),
      I2 => ref_option_IBUF(3),
      I3 => ref_option_IBUF(1),
      O => \disp_aux_reg[7][3]_i_1_n_0\
    );
\disp_aux_reg[8][0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[8][0]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \disp_refresco[8]\(0)
    );
\disp_aux_reg[8][0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => ref_option_IBUF(0),
      I1 => ref_option_IBUF(2),
      I2 => ref_option_IBUF(1),
      O => \disp_aux_reg[8][0]_i_1_n_0\
    );
\disp_aux_reg[8][1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[8][1]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \disp_refresco[8]\(1)
    );
\disp_aux_reg[8][1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEC"
    )
        port map (
      I0 => ref_option_IBUF(0),
      I1 => ref_option_IBUF(3),
      I2 => ref_option_IBUF(2),
      I3 => ref_option_IBUF(1),
      O => \disp_aux_reg[8][1]_i_1_n_0\
    );
\disp_aux_reg[8][2]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[8][2]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \^disp_aux_reg[6][3]_i_2_2\(0)
    );
\disp_aux_reg[8][2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => ref_option_IBUF(0),
      I1 => ref_option_IBUF(2),
      I2 => ref_option_IBUF(3),
      O => \disp_aux_reg[8][2]_i_1_n_0\
    );
\disp_aux_reg[8][3]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[8][3]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \disp_refresco[8]\(3)
    );
\disp_aux_reg[8][3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEE8"
    )
        port map (
      I0 => ref_option_IBUF(1),
      I1 => ref_option_IBUF(3),
      I2 => ref_option_IBUF(0),
      I3 => ref_option_IBUF(2),
      O => \disp_aux_reg[8][3]_i_1_n_0\
    );
\disp_aux_reg[8][6]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[8][6]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \disp_refresco[8]\(6)
    );
\disp_aux_reg[8][6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0007"
    )
        port map (
      I0 => ref_option_IBUF(1),
      I1 => ref_option_IBUF(3),
      I2 => ref_option_IBUF(2),
      I3 => ref_option_IBUF(0),
      O => \disp_aux_reg[8][6]_i_1_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity int_to_string is
  port (
    total_reg_26_sp_1 : out STD_LOGIC;
    \disp_dinero[3]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \string_cent_decenas[1]5__185_carry_0\ : out STD_LOGIC;
    \disp[5]_OBUF\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \disp[3]_OBUF\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp[2]_OBUF\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \current_state_reg[0]\ : out STD_LOGIC;
    total_reg : in STD_LOGIC_VECTOR ( 30 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_refresco[5]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_refresco[7]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_refresco[3]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_refresco[2]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_refresco[4]\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end int_to_string;

architecture STRUCTURE of int_to_string is
  signal cent : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \disp[2][1]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \disp[2][2]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \disp[3][1]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_10_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_11_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_12_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_13_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_14_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_15_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_16_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_17_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_18_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_19_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_20_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_21_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_22_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_23_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_7_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_8_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_9_n_0\ : STD_LOGIC;
  signal \disp[5][4]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \disp[5][4]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \disp[5][4]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \disp[5][4]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \disp[5][4]_INST_0_i_7_n_0\ : STD_LOGIC;
  signal \disp[5][4]_INST_0_i_8_n_0\ : STD_LOGIC;
  signal \disp_dinero[2]\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \euros1__167_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \euros1__167_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \euros1__167_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \euros1__167_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \euros1__167_carry__0_n_0\ : STD_LOGIC;
  signal \euros1__167_carry__0_n_1\ : STD_LOGIC;
  signal \euros1__167_carry__0_n_2\ : STD_LOGIC;
  signal \euros1__167_carry__0_n_3\ : STD_LOGIC;
  signal \euros1__167_carry__0_n_4\ : STD_LOGIC;
  signal \euros1__167_carry__0_n_5\ : STD_LOGIC;
  signal \euros1__167_carry__0_n_6\ : STD_LOGIC;
  signal \euros1__167_carry__0_n_7\ : STD_LOGIC;
  signal \euros1__167_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \euros1__167_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \euros1__167_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \euros1__167_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \euros1__167_carry__1_n_0\ : STD_LOGIC;
  signal \euros1__167_carry__1_n_1\ : STD_LOGIC;
  signal \euros1__167_carry__1_n_2\ : STD_LOGIC;
  signal \euros1__167_carry__1_n_3\ : STD_LOGIC;
  signal \euros1__167_carry__1_n_4\ : STD_LOGIC;
  signal \euros1__167_carry__1_n_5\ : STD_LOGIC;
  signal \euros1__167_carry__1_n_6\ : STD_LOGIC;
  signal \euros1__167_carry__1_n_7\ : STD_LOGIC;
  signal \euros1__167_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \euros1__167_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \euros1__167_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \euros1__167_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \euros1__167_carry__2_n_0\ : STD_LOGIC;
  signal \euros1__167_carry__2_n_1\ : STD_LOGIC;
  signal \euros1__167_carry__2_n_2\ : STD_LOGIC;
  signal \euros1__167_carry__2_n_3\ : STD_LOGIC;
  signal \euros1__167_carry__2_n_4\ : STD_LOGIC;
  signal \euros1__167_carry__2_n_5\ : STD_LOGIC;
  signal \euros1__167_carry__2_n_6\ : STD_LOGIC;
  signal \euros1__167_carry__2_n_7\ : STD_LOGIC;
  signal \euros1__167_carry__3_i_1_n_0\ : STD_LOGIC;
  signal \euros1__167_carry__3_i_2_n_0\ : STD_LOGIC;
  signal \euros1__167_carry__3_i_3_n_0\ : STD_LOGIC;
  signal \euros1__167_carry__3_i_4_n_0\ : STD_LOGIC;
  signal \euros1__167_carry__3_n_0\ : STD_LOGIC;
  signal \euros1__167_carry__3_n_1\ : STD_LOGIC;
  signal \euros1__167_carry__3_n_2\ : STD_LOGIC;
  signal \euros1__167_carry__3_n_3\ : STD_LOGIC;
  signal \euros1__167_carry__3_n_4\ : STD_LOGIC;
  signal \euros1__167_carry__3_n_5\ : STD_LOGIC;
  signal \euros1__167_carry__3_n_6\ : STD_LOGIC;
  signal \euros1__167_carry__3_n_7\ : STD_LOGIC;
  signal \euros1__167_carry__4_i_1_n_0\ : STD_LOGIC;
  signal \euros1__167_carry__4_i_2_n_0\ : STD_LOGIC;
  signal \euros1__167_carry__4_n_3\ : STD_LOGIC;
  signal \euros1__167_carry__4_n_6\ : STD_LOGIC;
  signal \euros1__167_carry__4_n_7\ : STD_LOGIC;
  signal \euros1__167_carry_i_1_n_0\ : STD_LOGIC;
  signal \euros1__167_carry_i_2_n_0\ : STD_LOGIC;
  signal \euros1__167_carry_i_3_n_0\ : STD_LOGIC;
  signal \euros1__167_carry_n_0\ : STD_LOGIC;
  signal \euros1__167_carry_n_1\ : STD_LOGIC;
  signal \euros1__167_carry_n_2\ : STD_LOGIC;
  signal \euros1__167_carry_n_3\ : STD_LOGIC;
  signal \euros1__167_carry_n_4\ : STD_LOGIC;
  signal \euros1__167_carry_n_5\ : STD_LOGIC;
  signal \euros1__167_carry_n_6\ : STD_LOGIC;
  signal \euros1__1_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__0_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__0_n_1\ : STD_LOGIC;
  signal \euros1__1_carry__0_n_2\ : STD_LOGIC;
  signal \euros1__1_carry__0_n_3\ : STD_LOGIC;
  signal \euros1__1_carry__0_n_4\ : STD_LOGIC;
  signal \euros1__1_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__1_i_7_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__1_i_8_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__1_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__1_n_1\ : STD_LOGIC;
  signal \euros1__1_carry__1_n_2\ : STD_LOGIC;
  signal \euros1__1_carry__1_n_3\ : STD_LOGIC;
  signal \euros1__1_carry__1_n_4\ : STD_LOGIC;
  signal \euros1__1_carry__1_n_5\ : STD_LOGIC;
  signal \euros1__1_carry__1_n_6\ : STD_LOGIC;
  signal \euros1__1_carry__1_n_7\ : STD_LOGIC;
  signal \euros1__1_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__2_i_5_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__2_i_6_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__2_i_7_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__2_i_8_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__2_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__2_n_1\ : STD_LOGIC;
  signal \euros1__1_carry__2_n_2\ : STD_LOGIC;
  signal \euros1__1_carry__2_n_3\ : STD_LOGIC;
  signal \euros1__1_carry__2_n_4\ : STD_LOGIC;
  signal \euros1__1_carry__2_n_5\ : STD_LOGIC;
  signal \euros1__1_carry__2_n_6\ : STD_LOGIC;
  signal \euros1__1_carry__2_n_7\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_1_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_2_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_3_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_4_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_5_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_6_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_7_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_8_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__3_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__3_n_1\ : STD_LOGIC;
  signal \euros1__1_carry__3_n_2\ : STD_LOGIC;
  signal \euros1__1_carry__3_n_3\ : STD_LOGIC;
  signal \euros1__1_carry__3_n_4\ : STD_LOGIC;
  signal \euros1__1_carry__3_n_5\ : STD_LOGIC;
  signal \euros1__1_carry__3_n_6\ : STD_LOGIC;
  signal \euros1__1_carry__3_n_7\ : STD_LOGIC;
  signal \euros1__1_carry__4_i_1_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__4_i_2_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__4_i_3_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__4_i_4_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__4_i_5_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__4_i_6_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__4_i_7_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__4_i_8_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__4_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__4_n_1\ : STD_LOGIC;
  signal \euros1__1_carry__4_n_2\ : STD_LOGIC;
  signal \euros1__1_carry__4_n_3\ : STD_LOGIC;
  signal \euros1__1_carry__4_n_4\ : STD_LOGIC;
  signal \euros1__1_carry__4_n_5\ : STD_LOGIC;
  signal \euros1__1_carry__4_n_6\ : STD_LOGIC;
  signal \euros1__1_carry__4_n_7\ : STD_LOGIC;
  signal \euros1__1_carry__5_i_1_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__5_i_2_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__5_i_3_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__5_i_4_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__5_i_5_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__5_i_6_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__5_i_7_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__5_i_8_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__5_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__5_n_1\ : STD_LOGIC;
  signal \euros1__1_carry__5_n_2\ : STD_LOGIC;
  signal \euros1__1_carry__5_n_3\ : STD_LOGIC;
  signal \euros1__1_carry__5_n_4\ : STD_LOGIC;
  signal \euros1__1_carry__5_n_5\ : STD_LOGIC;
  signal \euros1__1_carry__5_n_6\ : STD_LOGIC;
  signal \euros1__1_carry__5_n_7\ : STD_LOGIC;
  signal \euros1__1_carry__6_i_1_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__6_i_2_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__6_i_3_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__6_i_4_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__6_i_5_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__6_i_6_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__6_i_7_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__6_i_8_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__6_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__6_n_1\ : STD_LOGIC;
  signal \euros1__1_carry__6_n_2\ : STD_LOGIC;
  signal \euros1__1_carry__6_n_3\ : STD_LOGIC;
  signal \euros1__1_carry__6_n_4\ : STD_LOGIC;
  signal \euros1__1_carry__6_n_5\ : STD_LOGIC;
  signal \euros1__1_carry__6_n_6\ : STD_LOGIC;
  signal \euros1__1_carry__6_n_7\ : STD_LOGIC;
  signal \euros1__1_carry__7_i_1_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__7_n_3\ : STD_LOGIC;
  signal \euros1__1_carry__7_n_6\ : STD_LOGIC;
  signal \euros1__1_carry__7_n_7\ : STD_LOGIC;
  signal \euros1__1_carry_i_1_n_0\ : STD_LOGIC;
  signal \euros1__1_carry_i_2_n_0\ : STD_LOGIC;
  signal \euros1__1_carry_i_3_n_0\ : STD_LOGIC;
  signal \euros1__1_carry_n_0\ : STD_LOGIC;
  signal \euros1__1_carry_n_1\ : STD_LOGIC;
  signal \euros1__1_carry_n_2\ : STD_LOGIC;
  signal \euros1__1_carry_n_3\ : STD_LOGIC;
  signal \euros1__229_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \euros1__229_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \euros1__229_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \euros1__229_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \euros1__229_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \euros1__229_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \euros1__229_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \euros1__229_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \euros1__229_carry__0_n_0\ : STD_LOGIC;
  signal \euros1__229_carry__0_n_1\ : STD_LOGIC;
  signal \euros1__229_carry__0_n_2\ : STD_LOGIC;
  signal \euros1__229_carry__0_n_3\ : STD_LOGIC;
  signal \euros1__229_carry__0_n_4\ : STD_LOGIC;
  signal \euros1__229_carry__0_n_5\ : STD_LOGIC;
  signal \euros1__229_carry__0_n_6\ : STD_LOGIC;
  signal \euros1__229_carry__0_n_7\ : STD_LOGIC;
  signal \euros1__229_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \euros1__229_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \euros1__229_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \euros1__229_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \euros1__229_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \euros1__229_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \euros1__229_carry__1_i_7_n_0\ : STD_LOGIC;
  signal \euros1__229_carry__1_i_8_n_0\ : STD_LOGIC;
  signal \euros1__229_carry__1_n_0\ : STD_LOGIC;
  signal \euros1__229_carry__1_n_1\ : STD_LOGIC;
  signal \euros1__229_carry__1_n_2\ : STD_LOGIC;
  signal \euros1__229_carry__1_n_3\ : STD_LOGIC;
  signal \euros1__229_carry__1_n_4\ : STD_LOGIC;
  signal \euros1__229_carry__1_n_5\ : STD_LOGIC;
  signal \euros1__229_carry__1_n_6\ : STD_LOGIC;
  signal \euros1__229_carry__1_n_7\ : STD_LOGIC;
  signal \euros1__229_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \euros1__229_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \euros1__229_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \euros1__229_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \euros1__229_carry__2_i_5_n_0\ : STD_LOGIC;
  signal \euros1__229_carry__2_i_6_n_0\ : STD_LOGIC;
  signal \euros1__229_carry__2_i_7_n_0\ : STD_LOGIC;
  signal \euros1__229_carry__2_n_1\ : STD_LOGIC;
  signal \euros1__229_carry__2_n_2\ : STD_LOGIC;
  signal \euros1__229_carry__2_n_3\ : STD_LOGIC;
  signal \euros1__229_carry__2_n_4\ : STD_LOGIC;
  signal \euros1__229_carry__2_n_5\ : STD_LOGIC;
  signal \euros1__229_carry__2_n_6\ : STD_LOGIC;
  signal \euros1__229_carry__2_n_7\ : STD_LOGIC;
  signal \euros1__229_carry_i_1_n_0\ : STD_LOGIC;
  signal \euros1__229_carry_i_2_n_0\ : STD_LOGIC;
  signal \euros1__229_carry_i_3_n_0\ : STD_LOGIC;
  signal \euros1__229_carry_n_0\ : STD_LOGIC;
  signal \euros1__229_carry_n_1\ : STD_LOGIC;
  signal \euros1__229_carry_n_2\ : STD_LOGIC;
  signal \euros1__229_carry_n_3\ : STD_LOGIC;
  signal \euros1__229_carry_n_4\ : STD_LOGIC;
  signal \euros1__229_carry_n_5\ : STD_LOGIC;
  signal \euros1__229_carry_n_6\ : STD_LOGIC;
  signal \euros1__229_carry_n_7\ : STD_LOGIC;
  signal \euros1__274_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__0_i_9_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__0_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__0_n_1\ : STD_LOGIC;
  signal \euros1__274_carry__0_n_2\ : STD_LOGIC;
  signal \euros1__274_carry__0_n_3\ : STD_LOGIC;
  signal \euros1__274_carry__1_i_10_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__1_i_11_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__1_i_12_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__1_i_13_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__1_i_14_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__1_i_15_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__1_i_7_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__1_i_8_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__1_i_9_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__1_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__1_n_1\ : STD_LOGIC;
  signal \euros1__274_carry__1_n_2\ : STD_LOGIC;
  signal \euros1__274_carry__1_n_3\ : STD_LOGIC;
  signal \euros1__274_carry__2_i_10_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__2_i_11_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__2_i_12_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__2_i_13_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__2_i_14_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__2_i_15_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__2_i_16_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__2_i_5_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__2_i_6_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__2_i_7_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__2_i_8_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__2_i_9_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__2_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__2_n_1\ : STD_LOGIC;
  signal \euros1__274_carry__2_n_2\ : STD_LOGIC;
  signal \euros1__274_carry__2_n_3\ : STD_LOGIC;
  signal \euros1__274_carry__3_i_10_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__3_i_11_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__3_i_12_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__3_i_13_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__3_i_14_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__3_i_1_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__3_i_2_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__3_i_3_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__3_i_4_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__3_i_5_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__3_i_6_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__3_i_7_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__3_i_8_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__3_i_9_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__3_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__3_n_1\ : STD_LOGIC;
  signal \euros1__274_carry__3_n_2\ : STD_LOGIC;
  signal \euros1__274_carry__3_n_3\ : STD_LOGIC;
  signal \euros1__274_carry__4_i_10_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__4_i_11_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__4_i_12_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__4_i_13_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__4_i_14_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__4_i_15_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__4_i_1_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__4_i_2_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__4_i_3_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__4_i_4_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__4_i_5_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__4_i_6_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__4_i_7_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__4_i_8_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__4_i_9_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__4_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__4_n_1\ : STD_LOGIC;
  signal \euros1__274_carry__4_n_2\ : STD_LOGIC;
  signal \euros1__274_carry__4_n_3\ : STD_LOGIC;
  signal \euros1__274_carry__4_n_4\ : STD_LOGIC;
  signal \euros1__274_carry__4_n_5\ : STD_LOGIC;
  signal \euros1__274_carry__4_n_6\ : STD_LOGIC;
  signal \euros1__274_carry__4_n_7\ : STD_LOGIC;
  signal \euros1__274_carry__5_i_1_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__5_i_2_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__5_i_3_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__5_i_4_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__5_i_5_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__5_i_6_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__5_i_7_n_0\ : STD_LOGIC;
  signal \euros1__274_carry__5_n_3\ : STD_LOGIC;
  signal \euros1__274_carry__5_n_6\ : STD_LOGIC;
  signal \euros1__274_carry__5_n_7\ : STD_LOGIC;
  signal \euros1__274_carry_i_1_n_0\ : STD_LOGIC;
  signal \euros1__274_carry_i_2_n_0\ : STD_LOGIC;
  signal \euros1__274_carry_i_3_n_0\ : STD_LOGIC;
  signal \euros1__274_carry_i_4_n_0\ : STD_LOGIC;
  signal \euros1__274_carry_i_5_n_0\ : STD_LOGIC;
  signal \euros1__274_carry_i_6_n_0\ : STD_LOGIC;
  signal \euros1__274_carry_i_7_n_0\ : STD_LOGIC;
  signal \euros1__274_carry_i_8_n_0\ : STD_LOGIC;
  signal \euros1__274_carry_n_0\ : STD_LOGIC;
  signal \euros1__274_carry_n_1\ : STD_LOGIC;
  signal \euros1__274_carry_n_2\ : STD_LOGIC;
  signal \euros1__274_carry_n_3\ : STD_LOGIC;
  signal \euros1__331_carry_i_1_n_0\ : STD_LOGIC;
  signal \euros1__331_carry_i_2_n_0\ : STD_LOGIC;
  signal \euros1__331_carry_i_3_n_0\ : STD_LOGIC;
  signal \euros1__331_carry_i_4_n_0\ : STD_LOGIC;
  signal \euros1__331_carry_n_2\ : STD_LOGIC;
  signal \euros1__331_carry_n_3\ : STD_LOGIC;
  signal \euros1__331_carry_n_5\ : STD_LOGIC;
  signal \euros1__331_carry_n_6\ : STD_LOGIC;
  signal \euros1__331_carry_n_7\ : STD_LOGIC;
  signal \euros1__337_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \euros1__337_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \euros1__337_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \euros1__337_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \euros1__337_carry__0_n_1\ : STD_LOGIC;
  signal \euros1__337_carry__0_n_2\ : STD_LOGIC;
  signal \euros1__337_carry__0_n_3\ : STD_LOGIC;
  signal \euros1__337_carry__0_n_4\ : STD_LOGIC;
  signal \euros1__337_carry__0_n_5\ : STD_LOGIC;
  signal \euros1__337_carry__0_n_6\ : STD_LOGIC;
  signal \euros1__337_carry__0_n_7\ : STD_LOGIC;
  signal \euros1__337_carry_i_1_n_0\ : STD_LOGIC;
  signal \euros1__337_carry_i_2_n_0\ : STD_LOGIC;
  signal \euros1__337_carry_i_3_n_0\ : STD_LOGIC;
  signal \euros1__337_carry_i_4_n_0\ : STD_LOGIC;
  signal \euros1__337_carry_n_0\ : STD_LOGIC;
  signal \euros1__337_carry_n_1\ : STD_LOGIC;
  signal \euros1__337_carry_n_2\ : STD_LOGIC;
  signal \euros1__337_carry_n_3\ : STD_LOGIC;
  signal \euros1__337_carry_n_4\ : STD_LOGIC;
  signal \euros1__337_carry_n_5\ : STD_LOGIC;
  signal \euros1__90_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__0_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__0_n_1\ : STD_LOGIC;
  signal \euros1__90_carry__0_n_2\ : STD_LOGIC;
  signal \euros1__90_carry__0_n_3\ : STD_LOGIC;
  signal \euros1__90_carry__0_n_4\ : STD_LOGIC;
  signal \euros1__90_carry__0_n_5\ : STD_LOGIC;
  signal \euros1__90_carry__0_n_6\ : STD_LOGIC;
  signal \euros1__90_carry__0_n_7\ : STD_LOGIC;
  signal \euros1__90_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__1_i_7_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__1_i_8_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__1_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__1_n_1\ : STD_LOGIC;
  signal \euros1__90_carry__1_n_2\ : STD_LOGIC;
  signal \euros1__90_carry__1_n_3\ : STD_LOGIC;
  signal \euros1__90_carry__1_n_4\ : STD_LOGIC;
  signal \euros1__90_carry__1_n_5\ : STD_LOGIC;
  signal \euros1__90_carry__1_n_6\ : STD_LOGIC;
  signal \euros1__90_carry__1_n_7\ : STD_LOGIC;
  signal \euros1__90_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__2_i_5_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__2_i_6_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__2_i_7_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__2_i_8_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__2_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__2_n_1\ : STD_LOGIC;
  signal \euros1__90_carry__2_n_2\ : STD_LOGIC;
  signal \euros1__90_carry__2_n_3\ : STD_LOGIC;
  signal \euros1__90_carry__2_n_4\ : STD_LOGIC;
  signal \euros1__90_carry__2_n_5\ : STD_LOGIC;
  signal \euros1__90_carry__2_n_6\ : STD_LOGIC;
  signal \euros1__90_carry__2_n_7\ : STD_LOGIC;
  signal \euros1__90_carry__3_i_1_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__3_i_2_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__3_i_3_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__3_i_4_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__3_i_5_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__3_i_6_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__3_i_7_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__3_i_8_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__3_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__3_n_1\ : STD_LOGIC;
  signal \euros1__90_carry__3_n_2\ : STD_LOGIC;
  signal \euros1__90_carry__3_n_3\ : STD_LOGIC;
  signal \euros1__90_carry__3_n_4\ : STD_LOGIC;
  signal \euros1__90_carry__3_n_5\ : STD_LOGIC;
  signal \euros1__90_carry__3_n_6\ : STD_LOGIC;
  signal \euros1__90_carry__3_n_7\ : STD_LOGIC;
  signal \euros1__90_carry__4_i_1_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__4_i_2_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__4_i_3_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__4_i_4_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__4_i_5_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__4_i_6_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__4_i_7_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__4_i_8_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__4_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__4_n_1\ : STD_LOGIC;
  signal \euros1__90_carry__4_n_2\ : STD_LOGIC;
  signal \euros1__90_carry__4_n_3\ : STD_LOGIC;
  signal \euros1__90_carry__4_n_4\ : STD_LOGIC;
  signal \euros1__90_carry__4_n_5\ : STD_LOGIC;
  signal \euros1__90_carry__4_n_6\ : STD_LOGIC;
  signal \euros1__90_carry__4_n_7\ : STD_LOGIC;
  signal \euros1__90_carry__5_i_1_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__5_i_2_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__5_i_3_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__5_i_4_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__5_i_5_n_0\ : STD_LOGIC;
  signal \euros1__90_carry__5_n_2\ : STD_LOGIC;
  signal \euros1__90_carry__5_n_3\ : STD_LOGIC;
  signal \euros1__90_carry__5_n_5\ : STD_LOGIC;
  signal \euros1__90_carry__5_n_6\ : STD_LOGIC;
  signal \euros1__90_carry__5_n_7\ : STD_LOGIC;
  signal \euros1__90_carry_i_1_n_0\ : STD_LOGIC;
  signal \euros1__90_carry_i_2_n_0\ : STD_LOGIC;
  signal \euros1__90_carry_i_3_n_0\ : STD_LOGIC;
  signal \euros1__90_carry_i_4_n_0\ : STD_LOGIC;
  signal \euros1__90_carry_n_0\ : STD_LOGIC;
  signal \euros1__90_carry_n_1\ : STD_LOGIC;
  signal \euros1__90_carry_n_2\ : STD_LOGIC;
  signal \euros1__90_carry_n_3\ : STD_LOGIC;
  signal \euros1__90_carry_n_4\ : STD_LOGIC;
  signal \euros1__90_carry_n_5\ : STD_LOGIC;
  signal \euros1__90_carry_n_6\ : STD_LOGIC;
  signal \euros1__90_carry_n_7\ : STD_LOGIC;
  signal \i___0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \i___0_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \i___0_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \i___0_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \i___0_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \i___0_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \i___0_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \i___0_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \i___0_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \i___0_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \i___0_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \i___0_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \i___0_carry_i_2_n_0\ : STD_LOGIC;
  signal \i___0_carry_i_3_n_0\ : STD_LOGIC;
  signal \i___0_carry_i_4_n_0\ : STD_LOGIC;
  signal \i___0_carry_i_5_n_0\ : STD_LOGIC;
  signal \i___0_carry_i_6_n_0\ : STD_LOGIC;
  signal \i___106_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \i___106_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \i___106_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \i___106_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \i___106_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \i___106_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \i___106_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \i___106_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \i___106_carry__0_i_9_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_10_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_11_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_12_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_7_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_8_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_9_n_0\ : STD_LOGIC;
  signal \i___106_carry__2_i_10_n_0\ : STD_LOGIC;
  signal \i___106_carry__2_i_11_n_0\ : STD_LOGIC;
  signal \i___106_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \i___106_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \i___106_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \i___106_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \i___106_carry__2_i_5_n_0\ : STD_LOGIC;
  signal \i___106_carry__2_i_6_n_0\ : STD_LOGIC;
  signal \i___106_carry__2_i_7_n_0\ : STD_LOGIC;
  signal \i___106_carry__2_i_8_n_0\ : STD_LOGIC;
  signal \i___106_carry__2_i_9_n_0\ : STD_LOGIC;
  signal \i___106_carry__3_i_1_n_0\ : STD_LOGIC;
  signal \i___106_carry__3_i_2_n_0\ : STD_LOGIC;
  signal \i___106_carry__3_i_3_n_0\ : STD_LOGIC;
  signal \i___106_carry__3_i_4_n_0\ : STD_LOGIC;
  signal \i___106_carry__3_i_5_n_0\ : STD_LOGIC;
  signal \i___106_carry__3_i_6_n_0\ : STD_LOGIC;
  signal \i___106_carry__3_i_7_n_0\ : STD_LOGIC;
  signal \i___106_carry__3_i_8_n_0\ : STD_LOGIC;
  signal \i___106_carry__4_i_1_n_0\ : STD_LOGIC;
  signal \i___106_carry__4_i_2_n_0\ : STD_LOGIC;
  signal \i___106_carry__4_i_3_n_0\ : STD_LOGIC;
  signal \i___106_carry__4_i_4_n_0\ : STD_LOGIC;
  signal \i___106_carry__4_i_5_n_0\ : STD_LOGIC;
  signal \i___106_carry__4_i_6_n_0\ : STD_LOGIC;
  signal \i___106_carry__4_i_7_n_0\ : STD_LOGIC;
  signal \i___106_carry__5_i_1_n_0\ : STD_LOGIC;
  signal \i___106_carry__5_i_2_n_0\ : STD_LOGIC;
  signal \i___106_carry_i_1_n_0\ : STD_LOGIC;
  signal \i___106_carry_i_2_n_0\ : STD_LOGIC;
  signal \i___106_carry_i_3_n_0\ : STD_LOGIC;
  signal \i___106_carry_i_4_n_0\ : STD_LOGIC;
  signal \i___106_carry_i_5_n_0\ : STD_LOGIC;
  signal \i___106_carry_i_6_n_0\ : STD_LOGIC;
  signal \i___106_carry_i_7_n_0\ : STD_LOGIC;
  signal \i___106_carry_i_8_n_0\ : STD_LOGIC;
  signal \i___163_carry_i_1_n_0\ : STD_LOGIC;
  signal \i___163_carry_i_2_n_0\ : STD_LOGIC;
  signal \i___163_carry_i_3_n_0\ : STD_LOGIC;
  signal \i___163_carry_i_4_n_0\ : STD_LOGIC;
  signal \i___169_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \i___169_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \i___169_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \i___169_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \i___169_carry_i_2_n_0\ : STD_LOGIC;
  signal \i___169_carry_i_3_n_0\ : STD_LOGIC;
  signal \i___169_carry_i_4_n_0\ : STD_LOGIC;
  signal \i___169_carry_i_5_n_0\ : STD_LOGIC;
  signal \i___169_carry_i_6_n_0\ : STD_LOGIC;
  signal \i___169_carry_i_7_n_0\ : STD_LOGIC;
  signal \i___27_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \i___27_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \i___27_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \i___27_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \i___27_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \i___27_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \i___27_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \i___27_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \i___27_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \i___27_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \i___27_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \i___27_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \i___27_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \i___27_carry_i_1_n_0\ : STD_LOGIC;
  signal \i___27_carry_i_2_n_0\ : STD_LOGIC;
  signal \i___27_carry_i_3_n_0\ : STD_LOGIC;
  signal \i___27_carry_i_4_n_0\ : STD_LOGIC;
  signal \i___27_carry_i_5_n_0\ : STD_LOGIC;
  signal \i___27_carry_i_6_n_0\ : STD_LOGIC;
  signal \i___56_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \i___56_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \i___56_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \i___56_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \i___56_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \i___56_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \i___56_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \i___56_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \i___56_carry_i_1_n_0\ : STD_LOGIC;
  signal \i___56_carry_i_2_n_0\ : STD_LOGIC;
  signal \i___56_carry_i_3_n_0\ : STD_LOGIC;
  signal \i___56_carry_i_4_n_0\ : STD_LOGIC;
  signal \i___56_carry_i_5_n_0\ : STD_LOGIC;
  signal \i___80_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \i___80_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \i___80_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \i___80_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \i___80_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \i___80_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \i___80_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \i___80_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \i___80_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \i___80_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \i___80_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \i___80_carry_i_1_n_0\ : STD_LOGIC;
  signal \i___80_carry_i_2_n_0\ : STD_LOGIC;
  signal \i___80_carry_i_3_n_0\ : STD_LOGIC;
  signal \i___80_carry_i_4_n_0\ : STD_LOGIC;
  signal p_1_in : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \string_cent_decenas[1]5\ : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \string_cent_decenas[1]5__123_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__0_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__0_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__0_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__1_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__1_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__1_i_9_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__1_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__1_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__1_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__2_i_10_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__2_i_11_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__2_i_12_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__2_i_13_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__2_i_14_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__2_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__2_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__2_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__2_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__2_i_9_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__2_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__2_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__2_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__3_i_10_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__3_i_11_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__3_i_12_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__3_i_13_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__3_i_14_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__3_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__3_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__3_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__3_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__3_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__3_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__3_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__3_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__3_i_9_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__3_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__3_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__3_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__4_i_10_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__4_i_11_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__4_i_12_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__4_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__4_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__4_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__4_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__4_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__4_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__4_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__4_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__4_i_9_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__4_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__4_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__4_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__4_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__5_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__5_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__5_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__5_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__5_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__5_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__5_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__5_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__5_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__5_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry__5_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__123_carry_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__179_carry_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__179_carry_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__179_carry_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__179_carry_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__179_carry_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__179_carry_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__179_carry_n_7\ : STD_LOGIC;
  signal \^string_cent_decenas[1]5__185_carry_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__185_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__185_carry__0_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__185_carry_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__185_carry_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__185_carry_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__185_carry_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__185_carry_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__185_carry_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__185_carry_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__185_carry_i_9_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__185_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__185_carry_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__185_carry_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__185_carry_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__185_carry_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__185_carry_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__185_carry_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__1_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__1_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__1_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__1_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry__0_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry__0_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry__0_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry__0_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry__0_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry__0_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry__0_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry__1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry__1_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry__1_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry__1_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry__1_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry__1_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry__1_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry__1_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__54_carry_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry__0_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry__0_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry__0_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry__0_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry__0_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry__0_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry__0_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry__1_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry__1_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry__1_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry__1_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__78_carry_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry__0_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry__0_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry__0_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry__0_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry__0_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry__0_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry__0_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry__1_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry_i_8_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__99_carry_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_9_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__2_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__2_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__0_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__0_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__0_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__1_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__1_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__1_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__2_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__2_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__2_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__3_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__3_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__3_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__5_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__5_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__5_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___163_carry_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___163_carry_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___163_carry_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___163_carry_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___163_carry_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___169_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___169_carry_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___169_carry_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___169_carry_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___169_carry_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___169_carry_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry_n_7\ : STD_LOGIC;
  signal total_reg_26_sn_1 : STD_LOGIC;
  signal \NLW_euros1__167_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_euros1__167_carry__4_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_euros1__167_carry__4_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_euros1__1_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_euros1__1_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_euros1__1_carry__7_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_euros1__1_carry__7_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_euros1__229_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_euros1__274_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_euros1__274_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_euros1__274_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_euros1__274_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_euros1__274_carry__3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_euros1__274_carry__5_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_euros1__274_carry__5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_euros1__331_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_euros1__331_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_euros1__337_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_euros1__90_carry__5_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_euros1__90_carry__5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_string_cent_decenas[1]5__123_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5__123_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5__123_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5__123_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5__123_carry__3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5__123_carry__3_i_11_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_string_cent_decenas[1]5__123_carry__3_i_11_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5__123_carry__4_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5__123_carry__5_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_string_cent_decenas[1]5__123_carry__5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_string_cent_decenas[1]5__179_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_string_cent_decenas[1]5__179_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_string_cent_decenas[1]5__185_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5__185_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_string_cent_decenas[1]5__27_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_string_cent_decenas[1]5__27_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_string_cent_decenas[1]5__54_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_string_cent_decenas[1]5__78_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_string_cent_decenas[1]5__78_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_string_cent_decenas[1]5__78_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_string_cent_decenas[1]5__99_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5__99_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_string_cent_decenas[1]5__99_carry_i_8_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_string_cent_decenas[1]5__99_carry_i_8_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_string_cent_decenas[1]5_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___0_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__5_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___163_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___163_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___169_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___169_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___27_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___27_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___27_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___56_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___56_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___56_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___80_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___80_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \disp[2][1]_INST_0_i_2\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \disp[2][2]_INST_0_i_2\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \disp[2][3]_INST_0_i_2\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \disp[3][2]_INST_0_i_4\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \disp[3][3]_INST_0_i_14\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \disp[3][3]_INST_0_i_20\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \disp[3][3]_INST_0_i_22\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \disp[3][3]_INST_0_i_23\ : label is "soft_lutpair4";
  attribute HLUTNM : string;
  attribute HLUTNM of \euros1__1_carry__0_i_2\ : label is "lutpair0";
  attribute HLUTNM of \euros1__1_carry__1_i_1\ : label is "lutpair3";
  attribute HLUTNM of \euros1__1_carry__1_i_2\ : label is "lutpair2";
  attribute HLUTNM of \euros1__1_carry__1_i_3\ : label is "lutpair1";
  attribute HLUTNM of \euros1__1_carry__1_i_4\ : label is "lutpair0";
  attribute HLUTNM of \euros1__1_carry__1_i_5\ : label is "lutpair4";
  attribute HLUTNM of \euros1__1_carry__1_i_6\ : label is "lutpair3";
  attribute HLUTNM of \euros1__1_carry__1_i_7\ : label is "lutpair2";
  attribute HLUTNM of \euros1__1_carry__1_i_8\ : label is "lutpair1";
  attribute HLUTNM of \euros1__1_carry__2_i_1\ : label is "lutpair7";
  attribute HLUTNM of \euros1__1_carry__2_i_2\ : label is "lutpair6";
  attribute HLUTNM of \euros1__1_carry__2_i_3\ : label is "lutpair5";
  attribute HLUTNM of \euros1__1_carry__2_i_4\ : label is "lutpair4";
  attribute HLUTNM of \euros1__1_carry__2_i_5\ : label is "lutpair8";
  attribute HLUTNM of \euros1__1_carry__2_i_6\ : label is "lutpair7";
  attribute HLUTNM of \euros1__1_carry__2_i_7\ : label is "lutpair6";
  attribute HLUTNM of \euros1__1_carry__2_i_8\ : label is "lutpair5";
  attribute HLUTNM of \euros1__1_carry__3_i_1\ : label is "lutpair11";
  attribute HLUTNM of \euros1__1_carry__3_i_2\ : label is "lutpair10";
  attribute HLUTNM of \euros1__1_carry__3_i_3\ : label is "lutpair9";
  attribute HLUTNM of \euros1__1_carry__3_i_4\ : label is "lutpair8";
  attribute HLUTNM of \euros1__1_carry__3_i_5\ : label is "lutpair12";
  attribute HLUTNM of \euros1__1_carry__3_i_6\ : label is "lutpair11";
  attribute HLUTNM of \euros1__1_carry__3_i_7\ : label is "lutpair10";
  attribute HLUTNM of \euros1__1_carry__3_i_8\ : label is "lutpair9";
  attribute HLUTNM of \euros1__1_carry__4_i_1\ : label is "lutpair15";
  attribute HLUTNM of \euros1__1_carry__4_i_2\ : label is "lutpair14";
  attribute HLUTNM of \euros1__1_carry__4_i_3\ : label is "lutpair13";
  attribute HLUTNM of \euros1__1_carry__4_i_4\ : label is "lutpair12";
  attribute HLUTNM of \euros1__1_carry__4_i_5\ : label is "lutpair16";
  attribute HLUTNM of \euros1__1_carry__4_i_6\ : label is "lutpair15";
  attribute HLUTNM of \euros1__1_carry__4_i_7\ : label is "lutpair14";
  attribute HLUTNM of \euros1__1_carry__4_i_8\ : label is "lutpair13";
  attribute HLUTNM of \euros1__1_carry__5_i_1\ : label is "lutpair19";
  attribute HLUTNM of \euros1__1_carry__5_i_2\ : label is "lutpair18";
  attribute HLUTNM of \euros1__1_carry__5_i_3\ : label is "lutpair17";
  attribute HLUTNM of \euros1__1_carry__5_i_4\ : label is "lutpair16";
  attribute HLUTNM of \euros1__1_carry__5_i_5\ : label is "lutpair20";
  attribute HLUTNM of \euros1__1_carry__5_i_6\ : label is "lutpair19";
  attribute HLUTNM of \euros1__1_carry__5_i_7\ : label is "lutpair18";
  attribute HLUTNM of \euros1__1_carry__5_i_8\ : label is "lutpair17";
  attribute HLUTNM of \euros1__1_carry__6_i_3\ : label is "lutpair21";
  attribute HLUTNM of \euros1__1_carry__6_i_4\ : label is "lutpair20";
  attribute HLUTNM of \euros1__1_carry__6_i_8\ : label is "lutpair21";
  attribute HLUTNM of \euros1__229_carry__0_i_1\ : label is "lutpair43";
  attribute HLUTNM of \euros1__229_carry__0_i_2\ : label is "lutpair42";
  attribute HLUTNM of \euros1__229_carry__0_i_3\ : label is "lutpair41";
  attribute HLUTNM of \euros1__229_carry__0_i_5\ : label is "lutpair44";
  attribute HLUTNM of \euros1__229_carry__0_i_6\ : label is "lutpair43";
  attribute HLUTNM of \euros1__229_carry__0_i_7\ : label is "lutpair42";
  attribute HLUTNM of \euros1__229_carry__0_i_8\ : label is "lutpair41";
  attribute HLUTNM of \euros1__229_carry__1_i_1\ : label is "lutpair47";
  attribute HLUTNM of \euros1__229_carry__1_i_2\ : label is "lutpair46";
  attribute HLUTNM of \euros1__229_carry__1_i_3\ : label is "lutpair45";
  attribute HLUTNM of \euros1__229_carry__1_i_4\ : label is "lutpair44";
  attribute HLUTNM of \euros1__229_carry__1_i_5\ : label is "lutpair48";
  attribute HLUTNM of \euros1__229_carry__1_i_6\ : label is "lutpair47";
  attribute HLUTNM of \euros1__229_carry__1_i_7\ : label is "lutpair46";
  attribute HLUTNM of \euros1__229_carry__1_i_8\ : label is "lutpair45";
  attribute HLUTNM of \euros1__229_carry__2_i_1\ : label is "lutpair50";
  attribute HLUTNM of \euros1__229_carry__2_i_2\ : label is "lutpair49";
  attribute HLUTNM of \euros1__229_carry__2_i_3\ : label is "lutpair48";
  attribute HLUTNM of \euros1__229_carry__2_i_6\ : label is "lutpair50";
  attribute HLUTNM of \euros1__229_carry__2_i_7\ : label is "lutpair49";
  attribute HLUTNM of \euros1__274_carry__0_i_3\ : label is "lutpair52";
  attribute HLUTNM of \euros1__274_carry__0_i_4\ : label is "lutpair51";
  attribute HLUTNM of \euros1__274_carry__0_i_8\ : label is "lutpair52";
  attribute SOFT_HLUTNM of \euros1__274_carry__0_i_9\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \euros1__274_carry__1_i_10\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \euros1__274_carry__1_i_11\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \euros1__274_carry__1_i_12\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \euros1__274_carry__1_i_13\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \euros1__274_carry__1_i_14\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \euros1__274_carry__1_i_15\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \euros1__274_carry__1_i_9\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \euros1__274_carry__2_i_10\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \euros1__274_carry__2_i_11\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \euros1__274_carry__2_i_12\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \euros1__274_carry__2_i_13\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \euros1__274_carry__2_i_14\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \euros1__274_carry__2_i_15\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \euros1__274_carry__2_i_16\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \euros1__274_carry__2_i_9\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \euros1__274_carry__3_i_11\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \euros1__274_carry__3_i_13\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \euros1__274_carry__3_i_14\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \euros1__274_carry__3_i_9\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \euros1__274_carry__4_i_10\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \euros1__274_carry__4_i_11\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \euros1__274_carry__4_i_13\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \euros1__274_carry__4_i_14\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \euros1__274_carry__4_i_15\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \euros1__274_carry__4_i_9\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \euros1__274_carry__5_i_4\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \euros1__274_carry__5_i_5\ : label is "soft_lutpair15";
  attribute HLUTNM of \euros1__274_carry_i_5\ : label is "lutpair51";
  attribute HLUTNM of \euros1__90_carry__0_i_1\ : label is "lutpair22";
  attribute HLUTNM of \euros1__90_carry__0_i_5\ : label is "lutpair23";
  attribute HLUTNM of \euros1__90_carry__0_i_6\ : label is "lutpair22";
  attribute HLUTNM of \euros1__90_carry__1_i_1\ : label is "lutpair26";
  attribute HLUTNM of \euros1__90_carry__1_i_2\ : label is "lutpair25";
  attribute HLUTNM of \euros1__90_carry__1_i_3\ : label is "lutpair24";
  attribute HLUTNM of \euros1__90_carry__1_i_4\ : label is "lutpair23";
  attribute HLUTNM of \euros1__90_carry__1_i_5\ : label is "lutpair27";
  attribute HLUTNM of \euros1__90_carry__1_i_6\ : label is "lutpair26";
  attribute HLUTNM of \euros1__90_carry__1_i_7\ : label is "lutpair25";
  attribute HLUTNM of \euros1__90_carry__1_i_8\ : label is "lutpair24";
  attribute HLUTNM of \euros1__90_carry__2_i_1\ : label is "lutpair30";
  attribute HLUTNM of \euros1__90_carry__2_i_2\ : label is "lutpair29";
  attribute HLUTNM of \euros1__90_carry__2_i_3\ : label is "lutpair28";
  attribute HLUTNM of \euros1__90_carry__2_i_4\ : label is "lutpair27";
  attribute HLUTNM of \euros1__90_carry__2_i_5\ : label is "lutpair31";
  attribute HLUTNM of \euros1__90_carry__2_i_6\ : label is "lutpair30";
  attribute HLUTNM of \euros1__90_carry__2_i_7\ : label is "lutpair29";
  attribute HLUTNM of \euros1__90_carry__2_i_8\ : label is "lutpair28";
  attribute HLUTNM of \euros1__90_carry__3_i_1\ : label is "lutpair34";
  attribute HLUTNM of \euros1__90_carry__3_i_2\ : label is "lutpair33";
  attribute HLUTNM of \euros1__90_carry__3_i_3\ : label is "lutpair32";
  attribute HLUTNM of \euros1__90_carry__3_i_4\ : label is "lutpair31";
  attribute HLUTNM of \euros1__90_carry__3_i_5\ : label is "lutpair35";
  attribute HLUTNM of \euros1__90_carry__3_i_6\ : label is "lutpair34";
  attribute HLUTNM of \euros1__90_carry__3_i_7\ : label is "lutpair33";
  attribute HLUTNM of \euros1__90_carry__3_i_8\ : label is "lutpair32";
  attribute HLUTNM of \euros1__90_carry__4_i_1\ : label is "lutpair38";
  attribute HLUTNM of \euros1__90_carry__4_i_2\ : label is "lutpair37";
  attribute HLUTNM of \euros1__90_carry__4_i_3\ : label is "lutpair36";
  attribute HLUTNM of \euros1__90_carry__4_i_4\ : label is "lutpair35";
  attribute HLUTNM of \euros1__90_carry__4_i_5\ : label is "lutpair39";
  attribute HLUTNM of \euros1__90_carry__4_i_6\ : label is "lutpair38";
  attribute HLUTNM of \euros1__90_carry__4_i_7\ : label is "lutpair37";
  attribute HLUTNM of \euros1__90_carry__4_i_8\ : label is "lutpair36";
  attribute HLUTNM of \euros1__90_carry__5_i_1\ : label is "lutpair40";
  attribute HLUTNM of \euros1__90_carry__5_i_2\ : label is "lutpair39";
  attribute HLUTNM of \euros1__90_carry__5_i_5\ : label is "lutpair40";
  attribute HLUTNM of \i___106_carry__0_i_3\ : label is "lutpair63";
  attribute HLUTNM of \i___106_carry__0_i_4\ : label is "lutpair62";
  attribute HLUTNM of \i___106_carry__0_i_8\ : label is "lutpair63";
  attribute SOFT_HLUTNM of \i___106_carry__1_i_10\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \i___106_carry__1_i_12\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \i___106_carry__1_i_9\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \i___106_carry__2_i_10\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \i___106_carry__2_i_11\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \i___106_carry__2_i_9\ : label is "soft_lutpair23";
  attribute HLUTNM of \i___106_carry_i_5\ : label is "lutpair62";
  attribute HLUTNM of \i___27_carry__0_i_3\ : label is "lutpair61";
  attribute HLUTNM of \i___27_carry__0_i_8\ : label is "lutpair61";
  attribute HLUTNM of \string_cent_decenas[1]5__123_carry__0_i_1\ : label is "lutpair57";
  attribute HLUTNM of \string_cent_decenas[1]5__123_carry__0_i_2\ : label is "lutpair56";
  attribute HLUTNM of \string_cent_decenas[1]5__123_carry__0_i_5\ : label is "lutpair58";
  attribute HLUTNM of \string_cent_decenas[1]5__123_carry__0_i_6\ : label is "lutpair57";
  attribute HLUTNM of \string_cent_decenas[1]5__123_carry__0_i_7\ : label is "lutpair56";
  attribute HLUTNM of \string_cent_decenas[1]5__123_carry__1_i_2\ : label is "lutpair60";
  attribute HLUTNM of \string_cent_decenas[1]5__123_carry__1_i_3\ : label is "lutpair59";
  attribute HLUTNM of \string_cent_decenas[1]5__123_carry__1_i_4\ : label is "lutpair58";
  attribute HLUTNM of \string_cent_decenas[1]5__123_carry__1_i_7\ : label is "lutpair60";
  attribute HLUTNM of \string_cent_decenas[1]5__123_carry__1_i_8\ : label is "lutpair59";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__123_carry__1_i_9\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__123_carry__2_i_10\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__123_carry__2_i_11\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__123_carry__2_i_12\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__123_carry__2_i_13\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__123_carry__2_i_14\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__123_carry__2_i_9\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__123_carry__3_i_10\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__123_carry__3_i_12\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__123_carry__3_i_13\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__123_carry__3_i_14\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__123_carry__3_i_9\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__123_carry__4_i_10\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__123_carry__4_i_11\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__123_carry__4_i_12\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__123_carry__4_i_9\ : label is "soft_lutpair20";
  attribute HLUTNM of \string_cent_decenas[1]5__27_carry_i_1\ : label is "lutpair53";
  attribute HLUTNM of \string_cent_decenas[1]5__78_carry_i_4\ : label is "lutpair53";
  attribute HLUTNM of \string_cent_decenas[1]5__99_carry__0_i_2\ : label is "lutpair55";
  attribute HLUTNM of \string_cent_decenas[1]5__99_carry__0_i_3\ : label is "lutpair54";
  attribute HLUTNM of \string_cent_decenas[1]5__99_carry__0_i_7\ : label is "lutpair55";
  attribute HLUTNM of \string_cent_decenas[1]5__99_carry__0_i_8\ : label is "lutpair54";
begin
  \string_cent_decenas[1]5__185_carry_0\ <= \^string_cent_decenas[1]5__185_carry_0\;
  total_reg_26_sp_1 <= total_reg_26_sn_1;
\disp[2][0]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B833"
    )
        port map (
      I0 => \disp_dinero[2]\(0),
      I1 => Q(1),
      I2 => \disp_refresco[2]\(0),
      I3 => Q(0),
      O => \disp[2]_OBUF\(0)
    );
\disp[2][1]_INST_0_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"222EFFFF"
    )
        port map (
      I0 => \disp_refresco[2]\(1),
      I1 => Q(1),
      I2 => \disp[2][1]_INST_0_i_2_n_0\,
      I3 => \^string_cent_decenas[1]5__185_carry_0\,
      I4 => Q(0),
      O => \disp[2]_OBUF\(1)
    );
\disp[2][1]_INST_0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CD99"
    )
        port map (
      I0 => \string_cent_decenas[1]5__185_carry__0_n_7\,
      I1 => \string_cent_decenas[1]5__185_carry_n_6\,
      I2 => \string_cent_decenas[1]5__185_carry_n_5\,
      I3 => \string_cent_decenas[1]5__185_carry_n_4\,
      O => \disp[2][1]_INST_0_i_2_n_0\
    );
\disp[2][2]_INST_0_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"222E0000"
    )
        port map (
      I0 => \disp_refresco[4]\(0),
      I1 => Q(1),
      I2 => \disp[2][2]_INST_0_i_2_n_0\,
      I3 => \^string_cent_decenas[1]5__185_carry_0\,
      I4 => Q(0),
      O => \disp[2]_OBUF\(2)
    );
\disp[2][2]_INST_0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6675"
    )
        port map (
      I0 => \string_cent_decenas[1]5__185_carry_n_5\,
      I1 => \string_cent_decenas[1]5__185_carry_n_6\,
      I2 => \string_cent_decenas[1]5__185_carry_n_4\,
      I3 => \string_cent_decenas[1]5__185_carry__0_n_7\,
      O => \disp[2][2]_INST_0_i_2_n_0\
    );
\disp[2][3]_INST_0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"444A"
    )
        port map (
      I0 => \string_cent_decenas[1]5__185_carry_n_4\,
      I1 => \string_cent_decenas[1]5__185_carry__0_n_7\,
      I2 => \string_cent_decenas[1]5__185_carry_n_6\,
      I3 => \string_cent_decenas[1]5__185_carry_n_5\,
      O => \^string_cent_decenas[1]5__185_carry_0\
    );
\disp[3][0]_INST_0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8706E7469D189F1E"
    )
        port map (
      I0 => \disp[3][3]_INST_0_i_15_n_0\,
      I1 => \disp[3][3]_INST_0_i_12_n_0\,
      I2 => \disp[3][3]_INST_0_i_13_n_0\,
      I3 => \disp[3][3]_INST_0_i_10_n_0\,
      I4 => \disp[3][3]_INST_0_i_14_n_0\,
      I5 => \disp[3][3]_INST_0_i_11_n_0\,
      O => \disp_dinero[3]\(0)
    );
\disp[3][1]_INST_0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => Q(0),
      I1 => \disp[3][3]_INST_0_i_2_n_0\,
      I2 => \disp[3][1]_INST_0_i_4_n_0\,
      I3 => \disp[3][3]_INST_0_i_4_n_0\,
      O => \current_state_reg[0]\
    );
\disp[3][1]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6C1313CB7E9393EF"
    )
        port map (
      I0 => \disp[3][3]_INST_0_i_12_n_0\,
      I1 => \disp[3][3]_INST_0_i_5_n_0\,
      I2 => \disp[3][3]_INST_0_i_6_n_0\,
      I3 => \disp[3][3]_INST_0_i_7_n_0\,
      I4 => \disp[3][2]_INST_0_i_3_n_0\,
      I5 => \disp[3][3]_INST_0_i_11_n_0\,
      O => \disp[3][1]_INST_0_i_4_n_0\
    );
\disp[3][2]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BEAE"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_2_n_0\,
      I1 => Q(1),
      I2 => Q(0),
      I3 => \disp_refresco[3]\(0),
      O => \disp[3]_OBUF\(0)
    );
\disp[3][2]_INST_0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"002A8000002AA800"
    )
        port map (
      I0 => Q(1),
      I1 => \disp[3][2]_INST_0_i_3_n_0\,
      I2 => \disp[3][3]_INST_0_i_7_n_0\,
      I3 => \disp[3][3]_INST_0_i_6_n_0\,
      I4 => \disp[3][3]_INST_0_i_5_n_0\,
      I5 => \disp[3][3]_INST_0_i_12_n_0\,
      O => \disp[3][2]_INST_0_i_2_n_0\
    );
\disp[3][2]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"D040400DFDF4F4DF"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_4_n_0\,
      I1 => \disp[2][2]_INST_0_i_2_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_4\,
      I3 => \disp[3][2]_INST_0_i_5_n_0\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_5\,
      I5 => \^string_cent_decenas[1]5__185_carry_0\,
      O => \disp[3][2]_INST_0_i_3_n_0\
    );
\disp[3][2]_INST_0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"045D"
    )
        port map (
      I0 => \disp[2][1]_INST_0_i_2_n_0\,
      I1 => \disp_dinero[2]\(0),
      I2 => cent(0),
      I3 => \string_cent_decenas[1]5\(1),
      O => \disp[3][2]_INST_0_i_4_n_0\
    );
\disp[3][2]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000005557FFFF"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_4\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_5\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_7\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_6\,
      I5 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_4\,
      O => \disp[3][2]_INST_0_i_5_n_0\
    );
\disp[3][3]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"20E02020E0E0E0E0"
    )
        port map (
      I0 => \disp_refresco[7]\(0),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \disp[3][3]_INST_0_i_2_n_0\,
      I4 => \disp[3][3]_INST_0_i_3_n_0\,
      I5 => \disp[3][3]_INST_0_i_4_n_0\,
      O => \disp[3]_OBUF\(1)
    );
\disp[3][3]_INST_0_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5FFA055FAAA0FAA9"
    )
        port map (
      I0 => \disp[3][3]_INST_0_i_17_n_0\,
      I1 => \disp[3][3]_INST_0_i_18_n_0\,
      I2 => \disp[3][3]_INST_0_i_7_n_0\,
      I3 => \disp[3][2]_INST_0_i_3_n_0\,
      I4 => \disp[3][3]_INST_0_i_5_n_0\,
      I5 => \disp[3][3]_INST_0_i_6_n_0\,
      O => \disp[3][3]_INST_0_i_10_n_0\
    );
\disp[3][3]_INST_0_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAA65569559"
    )
        port map (
      I0 => \disp[3][3]_INST_0_i_19_n_0\,
      I1 => \string_cent_decenas[1]5\(1),
      I2 => cent(0),
      I3 => \disp_dinero[2]\(0),
      I4 => \disp[2][1]_INST_0_i_2_n_0\,
      I5 => \disp[3][3]_INST_0_i_2_n_0\,
      O => \disp[3][3]_INST_0_i_11_n_0\
    );
\disp[3][3]_INST_0_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969696996966996"
    )
        port map (
      I0 => \^string_cent_decenas[1]5__185_carry_0\,
      I1 => \disp[3][3]_INST_0_i_8_n_0\,
      I2 => \disp[3][3]_INST_0_i_9_n_0\,
      I3 => \disp[3][3]_INST_0_i_20_n_0\,
      I4 => \disp[3][3]_INST_0_i_19_n_0\,
      I5 => \disp[3][3]_INST_0_i_2_n_0\,
      O => \disp[3][3]_INST_0_i_12_n_0\
    );
\disp[3][3]_INST_0_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9999669666669969"
    )
        port map (
      I0 => \disp[3][3]_INST_0_i_21_n_0\,
      I1 => \disp[3][3]_INST_0_i_17_n_0\,
      I2 => \disp[3][3]_INST_0_i_20_n_0\,
      I3 => \disp[3][3]_INST_0_i_19_n_0\,
      I4 => \disp[3][3]_INST_0_i_2_n_0\,
      I5 => \disp[3][3]_INST_0_i_4_n_0\,
      O => \disp[3][3]_INST_0_i_13_n_0\
    );
\disp[3][3]_INST_0_i_14\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69666696"
    )
        port map (
      I0 => \string_cent_decenas[1]5\(1),
      I1 => \disp[2][1]_INST_0_i_2_n_0\,
      I2 => cent(0),
      I3 => \disp_dinero[2]\(0),
      I4 => \disp[3][3]_INST_0_i_2_n_0\,
      O => \disp[3][3]_INST_0_i_14_n_0\
    );
\disp[3][3]_INST_0_i_15\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"781FF87F"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_3_n_0\,
      I1 => \disp[3][3]_INST_0_i_7_n_0\,
      I2 => \disp[3][3]_INST_0_i_6_n_0\,
      I3 => \disp[3][3]_INST_0_i_5_n_0\,
      I4 => \disp[3][3]_INST_0_i_17_n_0\,
      O => \disp[3][3]_INST_0_i_15_n_0\
    );
\disp[3][3]_INST_0_i_16\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"11EE11EA55AA55AA"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_4\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_6\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_7\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_5\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_4\,
      I5 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_5\,
      O => \disp[3][3]_INST_0_i_16_n_0\
    );
\disp[3][3]_INST_0_i_17\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4BD2D2B4B42D2D4B"
    )
        port map (
      I0 => \disp[2][2]_INST_0_i_2_n_0\,
      I1 => \disp[3][2]_INST_0_i_4_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_4\,
      I3 => \disp[3][2]_INST_0_i_5_n_0\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_5\,
      I5 => \^string_cent_decenas[1]5__185_carry_0\,
      O => \disp[3][3]_INST_0_i_17_n_0\
    );
\disp[3][3]_INST_0_i_18\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1001400420028008"
    )
        port map (
      I0 => \disp[2][2]_INST_0_i_2_n_0\,
      I1 => \disp[2][1]_INST_0_i_2_n_0\,
      I2 => \disp_dinero[2]\(0),
      I3 => cent(0),
      I4 => \string_cent_decenas[1]5\(1),
      I5 => \disp[3][3]_INST_0_i_16_n_0\,
      O => \disp[3][3]_INST_0_i_18_n_0\
    );
\disp[3][3]_INST_0_i_19\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A6656559599A9AA6"
    )
        port map (
      I0 => \disp[3][3]_INST_0_i_16_n_0\,
      I1 => \string_cent_decenas[1]5\(1),
      I2 => \disp[3][3]_INST_0_i_22_n_0\,
      I3 => \disp[3][3]_INST_0_i_23_n_0\,
      I4 => \string_cent_decenas[1]5__185_carry_n_6\,
      I5 => \string_cent_decenas[1]5__185_carry_n_5\,
      O => \disp[3][3]_INST_0_i_19_n_0\
    );
\disp[3][3]_INST_0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEFFFFFFFEFEFEFF"
    )
        port map (
      I0 => \disp[3][3]_INST_0_i_5_n_0\,
      I1 => \disp[3][3]_INST_0_i_6_n_0\,
      I2 => \disp[3][3]_INST_0_i_7_n_0\,
      I3 => \^string_cent_decenas[1]5__185_carry_0\,
      I4 => \disp[3][3]_INST_0_i_8_n_0\,
      I5 => \disp[3][3]_INST_0_i_9_n_0\,
      O => \disp[3][3]_INST_0_i_2_n_0\
    );
\disp[3][3]_INST_0_i_20\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4182"
    )
        port map (
      I0 => \string_cent_decenas[1]5\(1),
      I1 => cent(0),
      I2 => \disp_dinero[2]\(0),
      I3 => \disp[2][1]_INST_0_i_2_n_0\,
      O => \disp[3][3]_INST_0_i_20_n_0\
    );
\disp[3][3]_INST_0_i_21\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00F0F0FFEE0E0E00"
    )
        port map (
      I0 => \disp[3][3]_INST_0_i_6_n_0\,
      I1 => \disp[3][3]_INST_0_i_5_n_0\,
      I2 => \disp[3][3]_INST_0_i_9_n_0\,
      I3 => \disp[3][3]_INST_0_i_8_n_0\,
      I4 => \^string_cent_decenas[1]5__185_carry_0\,
      I5 => \disp[3][3]_INST_0_i_7_n_0\,
      O => \disp[3][3]_INST_0_i_21_n_0\
    );
\disp[3][3]_INST_0_i_22\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \disp_dinero[2]\(0),
      I1 => cent(0),
      O => \disp[3][3]_INST_0_i_22_n_0\
    );
\disp[3][3]_INST_0_i_23\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0057"
    )
        port map (
      I0 => \string_cent_decenas[1]5__185_carry_n_4\,
      I1 => \string_cent_decenas[1]5__185_carry_n_5\,
      I2 => \string_cent_decenas[1]5__185_carry_n_6\,
      I3 => \string_cent_decenas[1]5__185_carry__0_n_7\,
      O => \disp[3][3]_INST_0_i_23_n_0\
    );
\disp[3][3]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AABCAABFFFFFFFFF"
    )
        port map (
      I0 => \disp[3][3]_INST_0_i_10_n_0\,
      I1 => \disp[3][3]_INST_0_i_11_n_0\,
      I2 => \disp[3][3]_INST_0_i_12_n_0\,
      I3 => \disp[3][3]_INST_0_i_13_n_0\,
      I4 => \disp[3][3]_INST_0_i_14_n_0\,
      I5 => \disp[3][3]_INST_0_i_15_n_0\,
      O => \disp[3][3]_INST_0_i_3_n_0\
    );
\disp[3][3]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7757575777777757"
    )
        port map (
      I0 => \disp[3][3]_INST_0_i_5_n_0\,
      I1 => \disp[3][3]_INST_0_i_6_n_0\,
      I2 => \disp[3][3]_INST_0_i_7_n_0\,
      I3 => \^string_cent_decenas[1]5__185_carry_0\,
      I4 => \disp[3][3]_INST_0_i_8_n_0\,
      I5 => \disp[3][3]_INST_0_i_9_n_0\,
      O => \disp[3][3]_INST_0_i_4_n_0\
    );
\disp[3][3]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4A4A4A4A4A4A4AAA"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_4\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_6\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_7\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_5\,
      I5 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_4\,
      O => \disp[3][3]_INST_0_i_5_n_0\
    );
\disp[3][3]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0003FFFC55570000"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_4\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_5\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_7\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_6\,
      I5 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_4\,
      O => \disp[3][3]_INST_0_i_6_n_0\
    );
\disp[3][3]_INST_0_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FC03FC03FD00FF00"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_4\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_5\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_7\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_6\,
      I5 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_4\,
      O => \disp[3][3]_INST_0_i_7_n_0\
    );
\disp[3][3]_INST_0_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3C3C3C3C393B3333"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_4\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_5\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_7\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_6\,
      I5 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_4\,
      O => \disp[3][3]_INST_0_i_8_n_0\
    );
\disp[3][3]_INST_0_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFEF8AEF8A0000"
    )
        port map (
      I0 => \string_cent_decenas[1]5\(1),
      I1 => cent(0),
      I2 => \disp_dinero[2]\(0),
      I3 => \disp[2][1]_INST_0_i_2_n_0\,
      I4 => \disp[2][2]_INST_0_i_2_n_0\,
      I5 => \disp[3][3]_INST_0_i_16_n_0\,
      O => \disp[3][3]_INST_0_i_9_n_0\
    );
\disp[5][0]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"14FFFF001400FF00"
    )
        port map (
      I0 => total_reg_26_sn_1,
      I1 => total_reg(0),
      I2 => p_1_in(0),
      I3 => Q(1),
      I4 => Q(0),
      I5 => \disp_refresco[5]\(0),
      O => \disp[5]_OBUF\(0)
    );
\disp[5][4]_INST_0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000004"
    )
        port map (
      I0 => \disp[5][4]_INST_0_i_3_n_0\,
      I1 => \disp[5][4]_INST_0_i_4_n_0\,
      I2 => \disp[5][4]_INST_0_i_5_n_0\,
      I3 => \disp[5][4]_INST_0_i_6_n_0\,
      I4 => \disp[5][4]_INST_0_i_7_n_0\,
      I5 => \disp[5][4]_INST_0_i_8_n_0\,
      O => total_reg_26_sn_1
    );
\disp[5][4]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => total_reg(26),
      I1 => total_reg(25),
      I2 => total_reg(27),
      I3 => total_reg(30),
      I4 => total_reg(29),
      I5 => total_reg(28),
      O => \disp[5][4]_INST_0_i_3_n_0\
    );
\disp[5][4]_INST_0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => total_reg(9),
      I1 => total_reg(11),
      I2 => total_reg(18),
      I3 => total_reg(20),
      O => \disp[5][4]_INST_0_i_4_n_0\
    );
\disp[5][4]_INST_0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => total_reg(10),
      I1 => total_reg(12),
      I2 => total_reg(17),
      I3 => total_reg(19),
      O => \disp[5][4]_INST_0_i_5_n_0\
    );
\disp[5][4]_INST_0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => total_reg(13),
      I1 => total_reg(15),
      I2 => total_reg(8),
      I3 => total_reg(14),
      O => \disp[5][4]_INST_0_i_6_n_0\
    );
\disp[5][4]_INST_0_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => total_reg(7),
      I1 => total_reg(16),
      I2 => total_reg(24),
      I3 => total_reg(22),
      I4 => total_reg(23),
      I5 => total_reg(21),
      O => \disp[5][4]_INST_0_i_7_n_0\
    );
\disp[5][4]_INST_0_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FE000000"
    )
        port map (
      I0 => total_reg(4),
      I1 => total_reg(2),
      I2 => total_reg(3),
      I3 => total_reg(5),
      I4 => total_reg(6),
      O => \disp[5][4]_INST_0_i_8_n_0\
    );
\euros1__167_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \euros1__167_carry_n_0\,
      CO(2) => \euros1__167_carry_n_1\,
      CO(1) => \euros1__167_carry_n_2\,
      CO(0) => \euros1__167_carry_n_3\,
      CYINIT => '0',
      DI(3) => total_reg(0),
      DI(2 downto 0) => B"001",
      O(3) => \euros1__167_carry_n_4\,
      O(2) => \euros1__167_carry_n_5\,
      O(1) => \euros1__167_carry_n_6\,
      O(0) => \NLW_euros1__167_carry_O_UNCONNECTED\(0),
      S(3) => \euros1__167_carry_i_1_n_0\,
      S(2) => \euros1__167_carry_i_2_n_0\,
      S(1) => \euros1__167_carry_i_3_n_0\,
      S(0) => total_reg(0)
    );
\euros1__167_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__167_carry_n_0\,
      CO(3) => \euros1__167_carry__0_n_0\,
      CO(2) => \euros1__167_carry__0_n_1\,
      CO(1) => \euros1__167_carry__0_n_2\,
      CO(0) => \euros1__167_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => total_reg(4 downto 1),
      O(3) => \euros1__167_carry__0_n_4\,
      O(2) => \euros1__167_carry__0_n_5\,
      O(1) => \euros1__167_carry__0_n_6\,
      O(0) => \euros1__167_carry__0_n_7\,
      S(3) => \euros1__167_carry__0_i_1_n_0\,
      S(2) => \euros1__167_carry__0_i_2_n_0\,
      S(1) => \euros1__167_carry__0_i_3_n_0\,
      S(0) => \euros1__167_carry__0_i_4_n_0\
    );
\euros1__167_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_reg(4),
      I1 => total_reg(7),
      O => \euros1__167_carry__0_i_1_n_0\
    );
\euros1__167_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_reg(3),
      I1 => total_reg(6),
      O => \euros1__167_carry__0_i_2_n_0\
    );
\euros1__167_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_reg(2),
      I1 => total_reg(5),
      O => \euros1__167_carry__0_i_3_n_0\
    );
\euros1__167_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_reg(1),
      I1 => total_reg(4),
      O => \euros1__167_carry__0_i_4_n_0\
    );
\euros1__167_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__167_carry__0_n_0\,
      CO(3) => \euros1__167_carry__1_n_0\,
      CO(2) => \euros1__167_carry__1_n_1\,
      CO(1) => \euros1__167_carry__1_n_2\,
      CO(0) => \euros1__167_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => total_reg(8 downto 5),
      O(3) => \euros1__167_carry__1_n_4\,
      O(2) => \euros1__167_carry__1_n_5\,
      O(1) => \euros1__167_carry__1_n_6\,
      O(0) => \euros1__167_carry__1_n_7\,
      S(3) => \euros1__167_carry__1_i_1_n_0\,
      S(2) => \euros1__167_carry__1_i_2_n_0\,
      S(1) => \euros1__167_carry__1_i_3_n_0\,
      S(0) => \euros1__167_carry__1_i_4_n_0\
    );
\euros1__167_carry__1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_reg(8),
      I1 => total_reg(11),
      O => \euros1__167_carry__1_i_1_n_0\
    );
\euros1__167_carry__1_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_reg(7),
      I1 => total_reg(10),
      O => \euros1__167_carry__1_i_2_n_0\
    );
\euros1__167_carry__1_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_reg(6),
      I1 => total_reg(9),
      O => \euros1__167_carry__1_i_3_n_0\
    );
\euros1__167_carry__1_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_reg(5),
      I1 => total_reg(8),
      O => \euros1__167_carry__1_i_4_n_0\
    );
\euros1__167_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__167_carry__1_n_0\,
      CO(3) => \euros1__167_carry__2_n_0\,
      CO(2) => \euros1__167_carry__2_n_1\,
      CO(1) => \euros1__167_carry__2_n_2\,
      CO(0) => \euros1__167_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => total_reg(12 downto 9),
      O(3) => \euros1__167_carry__2_n_4\,
      O(2) => \euros1__167_carry__2_n_5\,
      O(1) => \euros1__167_carry__2_n_6\,
      O(0) => \euros1__167_carry__2_n_7\,
      S(3) => \euros1__167_carry__2_i_1_n_0\,
      S(2) => \euros1__167_carry__2_i_2_n_0\,
      S(1) => \euros1__167_carry__2_i_3_n_0\,
      S(0) => \euros1__167_carry__2_i_4_n_0\
    );
\euros1__167_carry__2_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_reg(12),
      I1 => total_reg(15),
      O => \euros1__167_carry__2_i_1_n_0\
    );
\euros1__167_carry__2_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_reg(11),
      I1 => total_reg(14),
      O => \euros1__167_carry__2_i_2_n_0\
    );
\euros1__167_carry__2_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_reg(10),
      I1 => total_reg(13),
      O => \euros1__167_carry__2_i_3_n_0\
    );
\euros1__167_carry__2_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_reg(9),
      I1 => total_reg(12),
      O => \euros1__167_carry__2_i_4_n_0\
    );
\euros1__167_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__167_carry__2_n_0\,
      CO(3) => \euros1__167_carry__3_n_0\,
      CO(2) => \euros1__167_carry__3_n_1\,
      CO(1) => \euros1__167_carry__3_n_2\,
      CO(0) => \euros1__167_carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => total_reg(16 downto 13),
      O(3) => \euros1__167_carry__3_n_4\,
      O(2) => \euros1__167_carry__3_n_5\,
      O(1) => \euros1__167_carry__3_n_6\,
      O(0) => \euros1__167_carry__3_n_7\,
      S(3) => \euros1__167_carry__3_i_1_n_0\,
      S(2) => \euros1__167_carry__3_i_2_n_0\,
      S(1) => \euros1__167_carry__3_i_3_n_0\,
      S(0) => \euros1__167_carry__3_i_4_n_0\
    );
\euros1__167_carry__3_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_reg(16),
      I1 => total_reg(19),
      O => \euros1__167_carry__3_i_1_n_0\
    );
\euros1__167_carry__3_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_reg(15),
      I1 => total_reg(18),
      O => \euros1__167_carry__3_i_2_n_0\
    );
\euros1__167_carry__3_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_reg(14),
      I1 => total_reg(17),
      O => \euros1__167_carry__3_i_3_n_0\
    );
\euros1__167_carry__3_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_reg(13),
      I1 => total_reg(16),
      O => \euros1__167_carry__3_i_4_n_0\
    );
\euros1__167_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__167_carry__3_n_0\,
      CO(3 downto 1) => \NLW_euros1__167_carry__4_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \euros1__167_carry__4_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => total_reg(17),
      O(3 downto 2) => \NLW_euros1__167_carry__4_O_UNCONNECTED\(3 downto 2),
      O(1) => \euros1__167_carry__4_n_6\,
      O(0) => \euros1__167_carry__4_n_7\,
      S(3 downto 2) => B"00",
      S(1) => \euros1__167_carry__4_i_1_n_0\,
      S(0) => \euros1__167_carry__4_i_2_n_0\
    );
\euros1__167_carry__4_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_reg(21),
      I1 => total_reg(18),
      O => \euros1__167_carry__4_i_1_n_0\
    );
\euros1__167_carry__4_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_reg(17),
      I1 => total_reg(20),
      O => \euros1__167_carry__4_i_2_n_0\
    );
\euros1__167_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_reg(0),
      I1 => total_reg(3),
      O => \euros1__167_carry_i_1_n_0\
    );
\euros1__167_carry_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => total_reg(2),
      O => \euros1__167_carry_i_2_n_0\
    );
\euros1__167_carry_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => total_reg(1),
      O => \euros1__167_carry_i_3_n_0\
    );
\euros1__1_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \euros1__1_carry_n_0\,
      CO(2) => \euros1__1_carry_n_1\,
      CO(1) => \euros1__1_carry_n_2\,
      CO(0) => \euros1__1_carry_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => total_reg(4 downto 2),
      DI(0) => '0',
      O(3 downto 0) => \NLW_euros1__1_carry_O_UNCONNECTED\(3 downto 0),
      S(3) => \euros1__1_carry_i_1_n_0\,
      S(2) => \euros1__1_carry_i_2_n_0\,
      S(1) => \euros1__1_carry_i_3_n_0\,
      S(0) => total_reg(1)
    );
\euros1__1_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__1_carry_n_0\,
      CO(3) => \euros1__1_carry__0_n_0\,
      CO(2) => \euros1__1_carry__0_n_1\,
      CO(1) => \euros1__1_carry__0_n_2\,
      CO(0) => \euros1__1_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__1_carry__0_i_1_n_0\,
      DI(2 downto 0) => total_reg(7 downto 5),
      O(3) => \euros1__1_carry__0_n_4\,
      O(2 downto 0) => \NLW_euros1__1_carry__0_O_UNCONNECTED\(2 downto 0),
      S(3) => \euros1__1_carry__0_i_2_n_0\,
      S(2) => \euros1__1_carry__0_i_3_n_0\,
      S(1) => \euros1__1_carry__0_i_4_n_0\,
      S(0) => \euros1__1_carry__0_i_5_n_0\
    );
\euros1__1_carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => total_reg(8),
      I1 => total_reg(1),
      I2 => total_reg(6),
      O => \euros1__1_carry__0_i_1_n_0\
    );
\euros1__1_carry__0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => total_reg(6),
      I1 => total_reg(1),
      I2 => total_reg(8),
      I3 => total_reg(0),
      I4 => total_reg(5),
      O => \euros1__1_carry__0_i_2_n_0\
    );
\euros1__1_carry__0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => total_reg(0),
      I1 => total_reg(5),
      I2 => total_reg(7),
      O => \euros1__1_carry__0_i_3_n_0\
    );
\euros1__1_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(6),
      I1 => total_reg(4),
      O => \euros1__1_carry__0_i_4_n_0\
    );
\euros1__1_carry__0_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(5),
      I1 => total_reg(3),
      O => \euros1__1_carry__0_i_5_n_0\
    );
\euros1__1_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__1_carry__0_n_0\,
      CO(3) => \euros1__1_carry__1_n_0\,
      CO(2) => \euros1__1_carry__1_n_1\,
      CO(1) => \euros1__1_carry__1_n_2\,
      CO(0) => \euros1__1_carry__1_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__1_carry__1_i_1_n_0\,
      DI(2) => \euros1__1_carry__1_i_2_n_0\,
      DI(1) => \euros1__1_carry__1_i_3_n_0\,
      DI(0) => \euros1__1_carry__1_i_4_n_0\,
      O(3) => \euros1__1_carry__1_n_4\,
      O(2) => \euros1__1_carry__1_n_5\,
      O(1) => \euros1__1_carry__1_n_6\,
      O(0) => \euros1__1_carry__1_n_7\,
      S(3) => \euros1__1_carry__1_i_5_n_0\,
      S(2) => \euros1__1_carry__1_i_6_n_0\,
      S(1) => \euros1__1_carry__1_i_7_n_0\,
      S(0) => \euros1__1_carry__1_i_8_n_0\
    );
\euros1__1_carry__1_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_reg(4),
      I1 => total_reg(9),
      I2 => total_reg(11),
      O => \euros1__1_carry__1_i_1_n_0\
    );
\euros1__1_carry__1_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_reg(3),
      I1 => total_reg(8),
      I2 => total_reg(10),
      O => \euros1__1_carry__1_i_2_n_0\
    );
\euros1__1_carry__1_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_reg(2),
      I1 => total_reg(7),
      I2 => total_reg(9),
      O => \euros1__1_carry__1_i_3_n_0\
    );
\euros1__1_carry__1_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_reg(6),
      I1 => total_reg(1),
      I2 => total_reg(8),
      O => \euros1__1_carry__1_i_4_n_0\
    );
\euros1__1_carry__1_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(5),
      I1 => total_reg(10),
      I2 => total_reg(12),
      I3 => \euros1__1_carry__1_i_1_n_0\,
      O => \euros1__1_carry__1_i_5_n_0\
    );
\euros1__1_carry__1_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(4),
      I1 => total_reg(9),
      I2 => total_reg(11),
      I3 => \euros1__1_carry__1_i_2_n_0\,
      O => \euros1__1_carry__1_i_6_n_0\
    );
\euros1__1_carry__1_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(3),
      I1 => total_reg(8),
      I2 => total_reg(10),
      I3 => \euros1__1_carry__1_i_3_n_0\,
      O => \euros1__1_carry__1_i_7_n_0\
    );
\euros1__1_carry__1_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(2),
      I1 => total_reg(7),
      I2 => total_reg(9),
      I3 => \euros1__1_carry__1_i_4_n_0\,
      O => \euros1__1_carry__1_i_8_n_0\
    );
\euros1__1_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__1_carry__1_n_0\,
      CO(3) => \euros1__1_carry__2_n_0\,
      CO(2) => \euros1__1_carry__2_n_1\,
      CO(1) => \euros1__1_carry__2_n_2\,
      CO(0) => \euros1__1_carry__2_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__1_carry__2_i_1_n_0\,
      DI(2) => \euros1__1_carry__2_i_2_n_0\,
      DI(1) => \euros1__1_carry__2_i_3_n_0\,
      DI(0) => \euros1__1_carry__2_i_4_n_0\,
      O(3) => \euros1__1_carry__2_n_4\,
      O(2) => \euros1__1_carry__2_n_5\,
      O(1) => \euros1__1_carry__2_n_6\,
      O(0) => \euros1__1_carry__2_n_7\,
      S(3) => \euros1__1_carry__2_i_5_n_0\,
      S(2) => \euros1__1_carry__2_i_6_n_0\,
      S(1) => \euros1__1_carry__2_i_7_n_0\,
      S(0) => \euros1__1_carry__2_i_8_n_0\
    );
\euros1__1_carry__2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_reg(15),
      I1 => total_reg(13),
      I2 => total_reg(8),
      O => \euros1__1_carry__2_i_1_n_0\
    );
\euros1__1_carry__2_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_reg(14),
      I1 => total_reg(12),
      I2 => total_reg(7),
      O => \euros1__1_carry__2_i_2_n_0\
    );
\euros1__1_carry__2_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_reg(6),
      I1 => total_reg(11),
      I2 => total_reg(13),
      O => \euros1__1_carry__2_i_3_n_0\
    );
\euros1__1_carry__2_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_reg(5),
      I1 => total_reg(10),
      I2 => total_reg(12),
      O => \euros1__1_carry__2_i_4_n_0\
    );
\euros1__1_carry__2_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(9),
      I1 => total_reg(14),
      I2 => total_reg(16),
      I3 => \euros1__1_carry__2_i_1_n_0\,
      O => \euros1__1_carry__2_i_5_n_0\
    );
\euros1__1_carry__2_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(15),
      I1 => total_reg(13),
      I2 => total_reg(8),
      I3 => \euros1__1_carry__2_i_2_n_0\,
      O => \euros1__1_carry__2_i_6_n_0\
    );
\euros1__1_carry__2_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(14),
      I1 => total_reg(12),
      I2 => total_reg(7),
      I3 => \euros1__1_carry__2_i_3_n_0\,
      O => \euros1__1_carry__2_i_7_n_0\
    );
\euros1__1_carry__2_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(6),
      I1 => total_reg(11),
      I2 => total_reg(13),
      I3 => \euros1__1_carry__2_i_4_n_0\,
      O => \euros1__1_carry__2_i_8_n_0\
    );
\euros1__1_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__1_carry__2_n_0\,
      CO(3) => \euros1__1_carry__3_n_0\,
      CO(2) => \euros1__1_carry__3_n_1\,
      CO(1) => \euros1__1_carry__3_n_2\,
      CO(0) => \euros1__1_carry__3_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__1_carry__3_i_1_n_0\,
      DI(2) => \euros1__1_carry__3_i_2_n_0\,
      DI(1) => \euros1__1_carry__3_i_3_n_0\,
      DI(0) => \euros1__1_carry__3_i_4_n_0\,
      O(3) => \euros1__1_carry__3_n_4\,
      O(2) => \euros1__1_carry__3_n_5\,
      O(1) => \euros1__1_carry__3_n_6\,
      O(0) => \euros1__1_carry__3_n_7\,
      S(3) => \euros1__1_carry__3_i_5_n_0\,
      S(2) => \euros1__1_carry__3_i_6_n_0\,
      S(1) => \euros1__1_carry__3_i_7_n_0\,
      S(0) => \euros1__1_carry__3_i_8_n_0\
    );
\euros1__1_carry__3_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_reg(19),
      I1 => total_reg(17),
      I2 => total_reg(12),
      O => \euros1__1_carry__3_i_1_n_0\
    );
\euros1__1_carry__3_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_reg(18),
      I1 => total_reg(16),
      I2 => total_reg(11),
      O => \euros1__1_carry__3_i_2_n_0\
    );
\euros1__1_carry__3_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_reg(10),
      I1 => total_reg(15),
      I2 => total_reg(17),
      O => \euros1__1_carry__3_i_3_n_0\
    );
\euros1__1_carry__3_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_reg(9),
      I1 => total_reg(14),
      I2 => total_reg(16),
      O => \euros1__1_carry__3_i_4_n_0\
    );
\euros1__1_carry__3_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(13),
      I1 => total_reg(18),
      I2 => total_reg(20),
      I3 => \euros1__1_carry__3_i_1_n_0\,
      O => \euros1__1_carry__3_i_5_n_0\
    );
\euros1__1_carry__3_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(19),
      I1 => total_reg(17),
      I2 => total_reg(12),
      I3 => \euros1__1_carry__3_i_2_n_0\,
      O => \euros1__1_carry__3_i_6_n_0\
    );
\euros1__1_carry__3_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(18),
      I1 => total_reg(16),
      I2 => total_reg(11),
      I3 => \euros1__1_carry__3_i_3_n_0\,
      O => \euros1__1_carry__3_i_7_n_0\
    );
\euros1__1_carry__3_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(10),
      I1 => total_reg(15),
      I2 => total_reg(17),
      I3 => \euros1__1_carry__3_i_4_n_0\,
      O => \euros1__1_carry__3_i_8_n_0\
    );
\euros1__1_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__1_carry__3_n_0\,
      CO(3) => \euros1__1_carry__4_n_0\,
      CO(2) => \euros1__1_carry__4_n_1\,
      CO(1) => \euros1__1_carry__4_n_2\,
      CO(0) => \euros1__1_carry__4_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__1_carry__4_i_1_n_0\,
      DI(2) => \euros1__1_carry__4_i_2_n_0\,
      DI(1) => \euros1__1_carry__4_i_3_n_0\,
      DI(0) => \euros1__1_carry__4_i_4_n_0\,
      O(3) => \euros1__1_carry__4_n_4\,
      O(2) => \euros1__1_carry__4_n_5\,
      O(1) => \euros1__1_carry__4_n_6\,
      O(0) => \euros1__1_carry__4_n_7\,
      S(3) => \euros1__1_carry__4_i_5_n_0\,
      S(2) => \euros1__1_carry__4_i_6_n_0\,
      S(1) => \euros1__1_carry__4_i_7_n_0\,
      S(0) => \euros1__1_carry__4_i_8_n_0\
    );
\euros1__1_carry__4_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_reg(16),
      I1 => total_reg(21),
      I2 => total_reg(23),
      O => \euros1__1_carry__4_i_1_n_0\
    );
\euros1__1_carry__4_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_reg(22),
      I1 => total_reg(20),
      I2 => total_reg(15),
      O => \euros1__1_carry__4_i_2_n_0\
    );
\euros1__1_carry__4_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_reg(21),
      I1 => total_reg(19),
      I2 => total_reg(14),
      O => \euros1__1_carry__4_i_3_n_0\
    );
\euros1__1_carry__4_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_reg(13),
      I1 => total_reg(18),
      I2 => total_reg(20),
      O => \euros1__1_carry__4_i_4_n_0\
    );
\euros1__1_carry__4_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(24),
      I1 => total_reg(22),
      I2 => total_reg(17),
      I3 => \euros1__1_carry__4_i_1_n_0\,
      O => \euros1__1_carry__4_i_5_n_0\
    );
\euros1__1_carry__4_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(16),
      I1 => total_reg(21),
      I2 => total_reg(23),
      I3 => \euros1__1_carry__4_i_2_n_0\,
      O => \euros1__1_carry__4_i_6_n_0\
    );
\euros1__1_carry__4_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(22),
      I1 => total_reg(20),
      I2 => total_reg(15),
      I3 => \euros1__1_carry__4_i_3_n_0\,
      O => \euros1__1_carry__4_i_7_n_0\
    );
\euros1__1_carry__4_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(21),
      I1 => total_reg(19),
      I2 => total_reg(14),
      I3 => \euros1__1_carry__4_i_4_n_0\,
      O => \euros1__1_carry__4_i_8_n_0\
    );
\euros1__1_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__1_carry__4_n_0\,
      CO(3) => \euros1__1_carry__5_n_0\,
      CO(2) => \euros1__1_carry__5_n_1\,
      CO(1) => \euros1__1_carry__5_n_2\,
      CO(0) => \euros1__1_carry__5_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__1_carry__5_i_1_n_0\,
      DI(2) => \euros1__1_carry__5_i_2_n_0\,
      DI(1) => \euros1__1_carry__5_i_3_n_0\,
      DI(0) => \euros1__1_carry__5_i_4_n_0\,
      O(3) => \euros1__1_carry__5_n_4\,
      O(2) => \euros1__1_carry__5_n_5\,
      O(1) => \euros1__1_carry__5_n_6\,
      O(0) => \euros1__1_carry__5_n_7\,
      S(3) => \euros1__1_carry__5_i_5_n_0\,
      S(2) => \euros1__1_carry__5_i_6_n_0\,
      S(1) => \euros1__1_carry__5_i_7_n_0\,
      S(0) => \euros1__1_carry__5_i_8_n_0\
    );
\euros1__1_carry__5_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_reg(27),
      I1 => total_reg(20),
      I2 => total_reg(25),
      O => \euros1__1_carry__5_i_1_n_0\
    );
\euros1__1_carry__5_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_reg(26),
      I1 => total_reg(19),
      I2 => total_reg(24),
      O => \euros1__1_carry__5_i_2_n_0\
    );
\euros1__1_carry__5_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_reg(25),
      I1 => total_reg(18),
      I2 => total_reg(23),
      O => \euros1__1_carry__5_i_3_n_0\
    );
\euros1__1_carry__5_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_reg(24),
      I1 => total_reg(22),
      I2 => total_reg(17),
      O => \euros1__1_carry__5_i_4_n_0\
    );
\euros1__1_carry__5_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(28),
      I1 => total_reg(21),
      I2 => total_reg(26),
      I3 => \euros1__1_carry__5_i_1_n_0\,
      O => \euros1__1_carry__5_i_5_n_0\
    );
\euros1__1_carry__5_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(27),
      I1 => total_reg(20),
      I2 => total_reg(25),
      I3 => \euros1__1_carry__5_i_2_n_0\,
      O => \euros1__1_carry__5_i_6_n_0\
    );
\euros1__1_carry__5_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(26),
      I1 => total_reg(19),
      I2 => total_reg(24),
      I3 => \euros1__1_carry__5_i_3_n_0\,
      O => \euros1__1_carry__5_i_7_n_0\
    );
\euros1__1_carry__5_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(25),
      I1 => total_reg(18),
      I2 => total_reg(23),
      I3 => \euros1__1_carry__5_i_4_n_0\,
      O => \euros1__1_carry__5_i_8_n_0\
    );
\euros1__1_carry__6\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__1_carry__5_n_0\,
      CO(3) => \euros1__1_carry__6_n_0\,
      CO(2) => \euros1__1_carry__6_n_1\,
      CO(1) => \euros1__1_carry__6_n_2\,
      CO(0) => \euros1__1_carry__6_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__1_carry__6_i_1_n_0\,
      DI(2) => \euros1__1_carry__6_i_2_n_0\,
      DI(1) => \euros1__1_carry__6_i_3_n_0\,
      DI(0) => \euros1__1_carry__6_i_4_n_0\,
      O(3) => \euros1__1_carry__6_n_4\,
      O(2) => \euros1__1_carry__6_n_5\,
      O(1) => \euros1__1_carry__6_n_6\,
      O(0) => \euros1__1_carry__6_n_7\,
      S(3) => \euros1__1_carry__6_i_5_n_0\,
      S(2) => \euros1__1_carry__6_i_6_n_0\,
      S(1) => \euros1__1_carry__6_i_7_n_0\,
      S(0) => \euros1__1_carry__6_i_8_n_0\
    );
\euros1__1_carry__6_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => total_reg(29),
      I1 => total_reg(24),
      O => \euros1__1_carry__6_i_1_n_0\
    );
\euros1__1_carry__6_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_reg(30),
      I1 => total_reg(23),
      I2 => total_reg(28),
      O => \euros1__1_carry__6_i_2_n_0\
    );
\euros1__1_carry__6_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_reg(29),
      I1 => total_reg(22),
      I2 => total_reg(27),
      O => \euros1__1_carry__6_i_3_n_0\
    );
\euros1__1_carry__6_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_reg(28),
      I1 => total_reg(21),
      I2 => total_reg(26),
      O => \euros1__1_carry__6_i_4_n_0\
    );
\euros1__1_carry__6_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => total_reg(24),
      I1 => total_reg(29),
      I2 => total_reg(25),
      I3 => total_reg(30),
      O => \euros1__1_carry__6_i_5_n_0\
    );
\euros1__1_carry__6_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E81717E8"
    )
        port map (
      I0 => total_reg(28),
      I1 => total_reg(23),
      I2 => total_reg(30),
      I3 => total_reg(24),
      I4 => total_reg(29),
      O => \euros1__1_carry__6_i_6_n_0\
    );
\euros1__1_carry__6_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__1_carry__6_i_3_n_0\,
      I1 => total_reg(30),
      I2 => total_reg(23),
      I3 => total_reg(28),
      O => \euros1__1_carry__6_i_7_n_0\
    );
\euros1__1_carry__6_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(29),
      I1 => total_reg(22),
      I2 => total_reg(27),
      I3 => \euros1__1_carry__6_i_4_n_0\,
      O => \euros1__1_carry__6_i_8_n_0\
    );
\euros1__1_carry__7\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__1_carry__6_n_0\,
      CO(3 downto 1) => \NLW_euros1__1_carry__7_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \euros1__1_carry__7_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => total_reg(26),
      O(3 downto 2) => \NLW_euros1__1_carry__7_O_UNCONNECTED\(3 downto 2),
      O(1) => \euros1__1_carry__7_n_6\,
      O(0) => \euros1__1_carry__7_n_7\,
      S(3 downto 2) => B"00",
      S(1) => total_reg(27),
      S(0) => \euros1__1_carry__7_i_1_n_0\
    );
\euros1__1_carry__7_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => total_reg(25),
      I1 => total_reg(30),
      I2 => total_reg(26),
      O => \euros1__1_carry__7_i_1_n_0\
    );
\euros1__1_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(4),
      I1 => total_reg(2),
      O => \euros1__1_carry_i_1_n_0\
    );
\euros1__1_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(3),
      I1 => total_reg(1),
      O => \euros1__1_carry_i_2_n_0\
    );
\euros1__1_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(2),
      I1 => total_reg(0),
      O => \euros1__1_carry_i_3_n_0\
    );
\euros1__229_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \euros1__229_carry_n_0\,
      CO(2) => \euros1__229_carry_n_1\,
      CO(1) => \euros1__229_carry_n_2\,
      CO(0) => \euros1__229_carry_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => total_reg(6 downto 4),
      DI(0) => '0',
      O(3) => \euros1__229_carry_n_4\,
      O(2) => \euros1__229_carry_n_5\,
      O(1) => \euros1__229_carry_n_6\,
      O(0) => \euros1__229_carry_n_7\,
      S(3) => \euros1__229_carry_i_1_n_0\,
      S(2) => \euros1__229_carry_i_2_n_0\,
      S(1) => \euros1__229_carry_i_3_n_0\,
      S(0) => total_reg(3)
    );
\euros1__229_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__229_carry_n_0\,
      CO(3) => \euros1__229_carry__0_n_0\,
      CO(2) => \euros1__229_carry__0_n_1\,
      CO(1) => \euros1__229_carry__0_n_2\,
      CO(0) => \euros1__229_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__229_carry__0_i_1_n_0\,
      DI(2) => \euros1__229_carry__0_i_2_n_0\,
      DI(1) => \euros1__229_carry__0_i_3_n_0\,
      DI(0) => \euros1__229_carry__0_i_4_n_0\,
      O(3) => \euros1__229_carry__0_n_4\,
      O(2) => \euros1__229_carry__0_n_5\,
      O(1) => \euros1__229_carry__0_n_6\,
      O(0) => \euros1__229_carry__0_n_7\,
      S(3) => \euros1__229_carry__0_i_5_n_0\,
      S(2) => \euros1__229_carry__0_i_6_n_0\,
      S(1) => \euros1__229_carry__0_i_7_n_0\,
      S(0) => \euros1__229_carry__0_i_8_n_0\
    );
\euros1__229_carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_reg(5),
      I1 => total_reg(3),
      I2 => total_reg(9),
      O => \euros1__229_carry__0_i_1_n_0\
    );
\euros1__229_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_reg(8),
      I1 => total_reg(2),
      I2 => total_reg(4),
      O => \euros1__229_carry__0_i_2_n_0\
    );
\euros1__229_carry__0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_reg(3),
      I1 => total_reg(1),
      I2 => total_reg(7),
      O => \euros1__229_carry__0_i_3_n_0\
    );
\euros1__229_carry__0_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => total_reg(7),
      I1 => total_reg(1),
      I2 => total_reg(3),
      O => \euros1__229_carry__0_i_4_n_0\
    );
\euros1__229_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(6),
      I1 => total_reg(4),
      I2 => total_reg(10),
      I3 => \euros1__229_carry__0_i_1_n_0\,
      O => \euros1__229_carry__0_i_5_n_0\
    );
\euros1__229_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(5),
      I1 => total_reg(3),
      I2 => total_reg(9),
      I3 => \euros1__229_carry__0_i_2_n_0\,
      O => \euros1__229_carry__0_i_6_n_0\
    );
\euros1__229_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(8),
      I1 => total_reg(2),
      I2 => total_reg(4),
      I3 => \euros1__229_carry__0_i_3_n_0\,
      O => \euros1__229_carry__0_i_7_n_0\
    );
\euros1__229_carry__0_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => total_reg(3),
      I1 => total_reg(1),
      I2 => total_reg(7),
      I3 => total_reg(0),
      I4 => total_reg(2),
      O => \euros1__229_carry__0_i_8_n_0\
    );
\euros1__229_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__229_carry__0_n_0\,
      CO(3) => \euros1__229_carry__1_n_0\,
      CO(2) => \euros1__229_carry__1_n_1\,
      CO(1) => \euros1__229_carry__1_n_2\,
      CO(0) => \euros1__229_carry__1_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__229_carry__1_i_1_n_0\,
      DI(2) => \euros1__229_carry__1_i_2_n_0\,
      DI(1) => \euros1__229_carry__1_i_3_n_0\,
      DI(0) => \euros1__229_carry__1_i_4_n_0\,
      O(3) => \euros1__229_carry__1_n_4\,
      O(2) => \euros1__229_carry__1_n_5\,
      O(1) => \euros1__229_carry__1_n_6\,
      O(0) => \euros1__229_carry__1_n_7\,
      S(3) => \euros1__229_carry__1_i_5_n_0\,
      S(2) => \euros1__229_carry__1_i_6_n_0\,
      S(1) => \euros1__229_carry__1_i_7_n_0\,
      S(0) => \euros1__229_carry__1_i_8_n_0\
    );
\euros1__229_carry__1_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_reg(9),
      I1 => total_reg(7),
      I2 => total_reg(13),
      O => \euros1__229_carry__1_i_1_n_0\
    );
\euros1__229_carry__1_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_reg(8),
      I1 => total_reg(6),
      I2 => total_reg(12),
      O => \euros1__229_carry__1_i_2_n_0\
    );
\euros1__229_carry__1_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_reg(11),
      I1 => total_reg(5),
      I2 => total_reg(7),
      O => \euros1__229_carry__1_i_3_n_0\
    );
\euros1__229_carry__1_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_reg(6),
      I1 => total_reg(4),
      I2 => total_reg(10),
      O => \euros1__229_carry__1_i_4_n_0\
    );
\euros1__229_carry__1_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(10),
      I1 => total_reg(8),
      I2 => total_reg(14),
      I3 => \euros1__229_carry__1_i_1_n_0\,
      O => \euros1__229_carry__1_i_5_n_0\
    );
\euros1__229_carry__1_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(9),
      I1 => total_reg(7),
      I2 => total_reg(13),
      I3 => \euros1__229_carry__1_i_2_n_0\,
      O => \euros1__229_carry__1_i_6_n_0\
    );
\euros1__229_carry__1_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(8),
      I1 => total_reg(6),
      I2 => total_reg(12),
      I3 => \euros1__229_carry__1_i_3_n_0\,
      O => \euros1__229_carry__1_i_7_n_0\
    );
\euros1__229_carry__1_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(11),
      I1 => total_reg(5),
      I2 => total_reg(7),
      I3 => \euros1__229_carry__1_i_4_n_0\,
      O => \euros1__229_carry__1_i_8_n_0\
    );
\euros1__229_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__229_carry__1_n_0\,
      CO(3) => \NLW_euros1__229_carry__2_CO_UNCONNECTED\(3),
      CO(2) => \euros1__229_carry__2_n_1\,
      CO(1) => \euros1__229_carry__2_n_2\,
      CO(0) => \euros1__229_carry__2_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \euros1__229_carry__2_i_1_n_0\,
      DI(1) => \euros1__229_carry__2_i_2_n_0\,
      DI(0) => \euros1__229_carry__2_i_3_n_0\,
      O(3) => \euros1__229_carry__2_n_4\,
      O(2) => \euros1__229_carry__2_n_5\,
      O(1) => \euros1__229_carry__2_n_6\,
      O(0) => \euros1__229_carry__2_n_7\,
      S(3) => \euros1__229_carry__2_i_4_n_0\,
      S(2) => \euros1__229_carry__2_i_5_n_0\,
      S(1) => \euros1__229_carry__2_i_6_n_0\,
      S(0) => \euros1__229_carry__2_i_7_n_0\
    );
\euros1__229_carry__2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_reg(12),
      I1 => total_reg(10),
      I2 => total_reg(16),
      O => \euros1__229_carry__2_i_1_n_0\
    );
\euros1__229_carry__2_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_reg(11),
      I1 => total_reg(9),
      I2 => total_reg(15),
      O => \euros1__229_carry__2_i_2_n_0\
    );
\euros1__229_carry__2_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_reg(10),
      I1 => total_reg(8),
      I2 => total_reg(14),
      O => \euros1__229_carry__2_i_3_n_0\
    );
\euros1__229_carry__2_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => total_reg(17),
      I1 => total_reg(11),
      I2 => total_reg(13),
      I3 => total_reg(14),
      I4 => total_reg(12),
      I5 => total_reg(18),
      O => \euros1__229_carry__2_i_4_n_0\
    );
\euros1__229_carry__2_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__229_carry__2_i_1_n_0\,
      I1 => total_reg(13),
      I2 => total_reg(11),
      I3 => total_reg(17),
      O => \euros1__229_carry__2_i_5_n_0\
    );
\euros1__229_carry__2_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(12),
      I1 => total_reg(10),
      I2 => total_reg(16),
      I3 => \euros1__229_carry__2_i_2_n_0\,
      O => \euros1__229_carry__2_i_6_n_0\
    );
\euros1__229_carry__2_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(11),
      I1 => total_reg(9),
      I2 => total_reg(15),
      I3 => \euros1__229_carry__2_i_3_n_0\,
      O => \euros1__229_carry__2_i_7_n_0\
    );
\euros1__229_carry_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => total_reg(0),
      I1 => total_reg(2),
      I2 => total_reg(6),
      O => \euros1__229_carry_i_1_n_0\
    );
\euros1__229_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(5),
      I1 => total_reg(1),
      O => \euros1__229_carry_i_2_n_0\
    );
\euros1__229_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(4),
      I1 => total_reg(0),
      O => \euros1__229_carry_i_3_n_0\
    );
\euros1__274_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \euros1__274_carry_n_0\,
      CO(2) => \euros1__274_carry_n_1\,
      CO(1) => \euros1__274_carry_n_2\,
      CO(0) => \euros1__274_carry_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__274_carry_i_1_n_0\,
      DI(2) => \euros1__274_carry_i_2_n_0\,
      DI(1) => \euros1__274_carry_i_3_n_0\,
      DI(0) => \euros1__274_carry_i_4_n_0\,
      O(3 downto 0) => \NLW_euros1__274_carry_O_UNCONNECTED\(3 downto 0),
      S(3) => \euros1__274_carry_i_5_n_0\,
      S(2) => \euros1__274_carry_i_6_n_0\,
      S(1) => \euros1__274_carry_i_7_n_0\,
      S(0) => \euros1__274_carry_i_8_n_0\
    );
\euros1__274_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__274_carry_n_0\,
      CO(3) => \euros1__274_carry__0_n_0\,
      CO(2) => \euros1__274_carry__0_n_1\,
      CO(1) => \euros1__274_carry__0_n_2\,
      CO(0) => \euros1__274_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__274_carry__0_i_1_n_0\,
      DI(2) => \euros1__274_carry__0_i_2_n_0\,
      DI(1) => \euros1__274_carry__0_i_3_n_0\,
      DI(0) => \euros1__274_carry__0_i_4_n_0\,
      O(3 downto 0) => \NLW_euros1__274_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \euros1__274_carry__0_i_5_n_0\,
      S(2) => \euros1__274_carry__0_i_6_n_0\,
      S(1) => \euros1__274_carry__0_i_7_n_0\,
      S(0) => \euros1__274_carry__0_i_8_n_0\
    );
\euros1__274_carry__0_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BE282828"
    )
        port map (
      I0 => \euros1__1_carry__2_n_5\,
      I1 => \euros1__90_carry__0_n_4\,
      I2 => \euros1__167_carry_n_5\,
      I3 => \euros1__167_carry_n_6\,
      I4 => \euros1__90_carry__0_n_5\,
      O => \euros1__274_carry__0_i_1_n_0\
    );
\euros1__274_carry__0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BE282828"
    )
        port map (
      I0 => \euros1__1_carry__2_n_6\,
      I1 => \euros1__90_carry__0_n_5\,
      I2 => \euros1__167_carry_n_6\,
      I3 => total_reg(0),
      I4 => \euros1__90_carry__0_n_6\,
      O => \euros1__274_carry__0_i_2_n_0\
    );
\euros1__274_carry__0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"28"
    )
        port map (
      I0 => \euros1__1_carry__2_n_7\,
      I1 => \euros1__90_carry__0_n_6\,
      I2 => total_reg(0),
      O => \euros1__274_carry__0_i_3_n_0\
    );
\euros1__274_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \euros1__1_carry__1_n_4\,
      I1 => \euros1__90_carry__0_n_7\,
      O => \euros1__274_carry__0_i_4_n_0\
    );
\euros1__274_carry__0_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => \euros1__274_carry__0_i_1_n_0\,
      I1 => \euros1__1_carry__2_n_4\,
      I2 => \euros1__274_carry__0_i_9_n_0\,
      I3 => \euros1__167_carry_n_5\,
      I4 => \euros1__90_carry__0_n_4\,
      O => \euros1__274_carry__0_i_5_n_0\
    );
\euros1__274_carry__0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669966996"
    )
        port map (
      I0 => \euros1__274_carry__0_i_2_n_0\,
      I1 => \euros1__1_carry__2_n_5\,
      I2 => \euros1__90_carry__0_n_4\,
      I3 => \euros1__167_carry_n_5\,
      I4 => \euros1__167_carry_n_6\,
      I5 => \euros1__90_carry__0_n_5\,
      O => \euros1__274_carry__0_i_6_n_0\
    );
\euros1__274_carry__0_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669966996"
    )
        port map (
      I0 => \euros1__274_carry__0_i_3_n_0\,
      I1 => \euros1__1_carry__2_n_6\,
      I2 => \euros1__90_carry__0_n_5\,
      I3 => \euros1__167_carry_n_6\,
      I4 => total_reg(0),
      I5 => \euros1__90_carry__0_n_6\,
      O => \euros1__274_carry__0_i_7_n_0\
    );
\euros1__274_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__1_carry__2_n_7\,
      I1 => \euros1__90_carry__0_n_6\,
      I2 => total_reg(0),
      I3 => \euros1__274_carry__0_i_4_n_0\,
      O => \euros1__274_carry__0_i_8_n_0\
    );
\euros1__274_carry__0_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__167_carry_n_4\,
      I1 => \euros1__90_carry__1_n_7\,
      I2 => total_reg(0),
      O => \euros1__274_carry__0_i_9_n_0\
    );
\euros1__274_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__274_carry__0_n_0\,
      CO(3) => \euros1__274_carry__1_n_0\,
      CO(2) => \euros1__274_carry__1_n_1\,
      CO(1) => \euros1__274_carry__1_n_2\,
      CO(0) => \euros1__274_carry__1_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__274_carry__1_i_1_n_0\,
      DI(2) => \euros1__274_carry__1_i_2_n_0\,
      DI(1) => \euros1__274_carry__1_i_3_n_0\,
      DI(0) => \euros1__274_carry__1_i_4_n_0\,
      O(3 downto 0) => \NLW_euros1__274_carry__1_O_UNCONNECTED\(3 downto 0),
      S(3) => \euros1__274_carry__1_i_5_n_0\,
      S(2) => \euros1__274_carry__1_i_6_n_0\,
      S(1) => \euros1__274_carry__1_i_7_n_0\,
      S(0) => \euros1__274_carry__1_i_8_n_0\
    );
\euros1__274_carry__1_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => total_reg(2),
      I1 => \euros1__90_carry__1_n_5\,
      I2 => \euros1__167_carry__0_n_6\,
      I3 => \euros1__1_carry__3_n_5\,
      I4 => \euros1__274_carry__1_i_9_n_0\,
      O => \euros1__274_carry__1_i_1_n_0\
    );
\euros1__274_carry__1_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__167_carry__0_n_6\,
      I1 => \euros1__90_carry__1_n_5\,
      I2 => total_reg(2),
      O => \euros1__274_carry__1_i_10_n_0\
    );
\euros1__274_carry__1_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__167_carry__0_n_7\,
      I1 => \euros1__90_carry__1_n_6\,
      I2 => total_reg(1),
      O => \euros1__274_carry__1_i_11_n_0\
    );
\euros1__274_carry__1_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \euros1__229_carry_n_7\,
      I1 => \euros1__90_carry__1_n_4\,
      I2 => \euros1__167_carry__0_n_5\,
      O => \euros1__274_carry__1_i_12_n_0\
    );
\euros1__274_carry__1_i_13\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => total_reg(2),
      I1 => \euros1__90_carry__1_n_5\,
      I2 => \euros1__167_carry__0_n_6\,
      O => \euros1__274_carry__1_i_13_n_0\
    );
\euros1__274_carry__1_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => total_reg(1),
      I1 => \euros1__90_carry__1_n_6\,
      I2 => \euros1__167_carry__0_n_7\,
      O => \euros1__274_carry__1_i_14_n_0\
    );
\euros1__274_carry__1_i_15\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => total_reg(0),
      I1 => \euros1__90_carry__1_n_7\,
      I2 => \euros1__167_carry_n_4\,
      O => \euros1__274_carry__1_i_15_n_0\
    );
\euros1__274_carry__1_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => total_reg(1),
      I1 => \euros1__90_carry__1_n_6\,
      I2 => \euros1__167_carry__0_n_7\,
      I3 => \euros1__1_carry__3_n_6\,
      I4 => \euros1__274_carry__1_i_10_n_0\,
      O => \euros1__274_carry__1_i_2_n_0\
    );
\euros1__274_carry__1_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => total_reg(0),
      I1 => \euros1__90_carry__1_n_7\,
      I2 => \euros1__167_carry_n_4\,
      I3 => \euros1__1_carry__3_n_7\,
      I4 => \euros1__274_carry__1_i_11_n_0\,
      O => \euros1__274_carry__1_i_3_n_0\
    );
\euros1__274_carry__1_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EBBE822882288228"
    )
        port map (
      I0 => \euros1__1_carry__2_n_4\,
      I1 => total_reg(0),
      I2 => \euros1__90_carry__1_n_7\,
      I3 => \euros1__167_carry_n_4\,
      I4 => \euros1__167_carry_n_5\,
      I5 => \euros1__90_carry__0_n_4\,
      O => \euros1__274_carry__1_i_4_n_0\
    );
\euros1__274_carry__1_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__274_carry__1_i_1_n_0\,
      I1 => \euros1__274_carry__1_i_12_n_0\,
      I2 => \euros1__167_carry__0_n_4\,
      I3 => \euros1__90_carry__2_n_7\,
      I4 => \euros1__229_carry_n_6\,
      I5 => \euros1__1_carry__3_n_4\,
      O => \euros1__274_carry__1_i_5_n_0\
    );
\euros1__274_carry__1_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__274_carry__1_i_2_n_0\,
      I1 => \euros1__274_carry__1_i_13_n_0\,
      I2 => \euros1__167_carry__0_n_5\,
      I3 => \euros1__90_carry__1_n_4\,
      I4 => \euros1__229_carry_n_7\,
      I5 => \euros1__1_carry__3_n_5\,
      O => \euros1__274_carry__1_i_6_n_0\
    );
\euros1__274_carry__1_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__274_carry__1_i_3_n_0\,
      I1 => \euros1__274_carry__1_i_14_n_0\,
      I2 => \euros1__167_carry__0_n_6\,
      I3 => \euros1__90_carry__1_n_5\,
      I4 => total_reg(2),
      I5 => \euros1__1_carry__3_n_6\,
      O => \euros1__274_carry__1_i_7_n_0\
    );
\euros1__274_carry__1_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__274_carry__1_i_4_n_0\,
      I1 => \euros1__274_carry__1_i_15_n_0\,
      I2 => \euros1__167_carry__0_n_7\,
      I3 => \euros1__90_carry__1_n_6\,
      I4 => total_reg(1),
      I5 => \euros1__1_carry__3_n_7\,
      O => \euros1__274_carry__1_i_8_n_0\
    );
\euros1__274_carry__1_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__167_carry__0_n_5\,
      I1 => \euros1__90_carry__1_n_4\,
      I2 => \euros1__229_carry_n_7\,
      O => \euros1__274_carry__1_i_9_n_0\
    );
\euros1__274_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__274_carry__1_n_0\,
      CO(3) => \euros1__274_carry__2_n_0\,
      CO(2) => \euros1__274_carry__2_n_1\,
      CO(1) => \euros1__274_carry__2_n_2\,
      CO(0) => \euros1__274_carry__2_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__274_carry__2_i_1_n_0\,
      DI(2) => \euros1__274_carry__2_i_2_n_0\,
      DI(1) => \euros1__274_carry__2_i_3_n_0\,
      DI(0) => \euros1__274_carry__2_i_4_n_0\,
      O(3 downto 0) => \NLW_euros1__274_carry__2_O_UNCONNECTED\(3 downto 0),
      S(3) => \euros1__274_carry__2_i_5_n_0\,
      S(2) => \euros1__274_carry__2_i_6_n_0\,
      S(1) => \euros1__274_carry__2_i_7_n_0\,
      S(0) => \euros1__274_carry__2_i_8_n_0\
    );
\euros1__274_carry__2_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \euros1__229_carry_n_4\,
      I1 => \euros1__90_carry__2_n_5\,
      I2 => \euros1__167_carry__1_n_6\,
      I3 => \euros1__1_carry__4_n_5\,
      I4 => \euros1__274_carry__2_i_9_n_0\,
      O => \euros1__274_carry__2_i_1_n_0\
    );
\euros1__274_carry__2_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__167_carry__1_n_6\,
      I1 => \euros1__90_carry__2_n_5\,
      I2 => \euros1__229_carry_n_4\,
      O => \euros1__274_carry__2_i_10_n_0\
    );
\euros1__274_carry__2_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__167_carry__1_n_7\,
      I1 => \euros1__90_carry__2_n_6\,
      I2 => \euros1__229_carry_n_5\,
      O => \euros1__274_carry__2_i_11_n_0\
    );
\euros1__274_carry__2_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__167_carry__0_n_4\,
      I1 => \euros1__90_carry__2_n_7\,
      I2 => \euros1__229_carry_n_6\,
      O => \euros1__274_carry__2_i_12_n_0\
    );
\euros1__274_carry__2_i_13\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \euros1__229_carry__0_n_7\,
      I1 => \euros1__90_carry__2_n_4\,
      I2 => \euros1__167_carry__1_n_5\,
      O => \euros1__274_carry__2_i_13_n_0\
    );
\euros1__274_carry__2_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \euros1__229_carry_n_4\,
      I1 => \euros1__90_carry__2_n_5\,
      I2 => \euros1__167_carry__1_n_6\,
      O => \euros1__274_carry__2_i_14_n_0\
    );
\euros1__274_carry__2_i_15\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \euros1__229_carry_n_5\,
      I1 => \euros1__90_carry__2_n_6\,
      I2 => \euros1__167_carry__1_n_7\,
      O => \euros1__274_carry__2_i_15_n_0\
    );
\euros1__274_carry__2_i_16\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \euros1__229_carry_n_6\,
      I1 => \euros1__90_carry__2_n_7\,
      I2 => \euros1__167_carry__0_n_4\,
      O => \euros1__274_carry__2_i_16_n_0\
    );
\euros1__274_carry__2_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \euros1__229_carry_n_5\,
      I1 => \euros1__90_carry__2_n_6\,
      I2 => \euros1__167_carry__1_n_7\,
      I3 => \euros1__1_carry__4_n_6\,
      I4 => \euros1__274_carry__2_i_10_n_0\,
      O => \euros1__274_carry__2_i_2_n_0\
    );
\euros1__274_carry__2_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \euros1__229_carry_n_6\,
      I1 => \euros1__90_carry__2_n_7\,
      I2 => \euros1__167_carry__0_n_4\,
      I3 => \euros1__1_carry__4_n_7\,
      I4 => \euros1__274_carry__2_i_11_n_0\,
      O => \euros1__274_carry__2_i_3_n_0\
    );
\euros1__274_carry__2_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \euros1__229_carry_n_7\,
      I1 => \euros1__90_carry__1_n_4\,
      I2 => \euros1__167_carry__0_n_5\,
      I3 => \euros1__1_carry__3_n_4\,
      I4 => \euros1__274_carry__2_i_12_n_0\,
      O => \euros1__274_carry__2_i_4_n_0\
    );
\euros1__274_carry__2_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__274_carry__2_i_1_n_0\,
      I1 => \euros1__274_carry__2_i_13_n_0\,
      I2 => \euros1__167_carry__1_n_4\,
      I3 => \euros1__90_carry__3_n_7\,
      I4 => \euros1__229_carry__0_n_6\,
      I5 => \euros1__1_carry__4_n_4\,
      O => \euros1__274_carry__2_i_5_n_0\
    );
\euros1__274_carry__2_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__274_carry__2_i_2_n_0\,
      I1 => \euros1__274_carry__2_i_14_n_0\,
      I2 => \euros1__167_carry__1_n_5\,
      I3 => \euros1__90_carry__2_n_4\,
      I4 => \euros1__229_carry__0_n_7\,
      I5 => \euros1__1_carry__4_n_5\,
      O => \euros1__274_carry__2_i_6_n_0\
    );
\euros1__274_carry__2_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__274_carry__2_i_3_n_0\,
      I1 => \euros1__274_carry__2_i_15_n_0\,
      I2 => \euros1__167_carry__1_n_6\,
      I3 => \euros1__90_carry__2_n_5\,
      I4 => \euros1__229_carry_n_4\,
      I5 => \euros1__1_carry__4_n_6\,
      O => \euros1__274_carry__2_i_7_n_0\
    );
\euros1__274_carry__2_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__274_carry__2_i_4_n_0\,
      I1 => \euros1__274_carry__2_i_16_n_0\,
      I2 => \euros1__167_carry__1_n_7\,
      I3 => \euros1__90_carry__2_n_6\,
      I4 => \euros1__229_carry_n_5\,
      I5 => \euros1__1_carry__4_n_7\,
      O => \euros1__274_carry__2_i_8_n_0\
    );
\euros1__274_carry__2_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__167_carry__1_n_5\,
      I1 => \euros1__90_carry__2_n_4\,
      I2 => \euros1__229_carry__0_n_7\,
      O => \euros1__274_carry__2_i_9_n_0\
    );
\euros1__274_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__274_carry__2_n_0\,
      CO(3) => \euros1__274_carry__3_n_0\,
      CO(2) => \euros1__274_carry__3_n_1\,
      CO(1) => \euros1__274_carry__3_n_2\,
      CO(0) => \euros1__274_carry__3_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__274_carry__3_i_1_n_0\,
      DI(2) => \euros1__274_carry__3_i_2_n_0\,
      DI(1) => \euros1__274_carry__3_i_3_n_0\,
      DI(0) => \euros1__274_carry__3_i_4_n_0\,
      O(3 downto 0) => \NLW_euros1__274_carry__3_O_UNCONNECTED\(3 downto 0),
      S(3) => \euros1__274_carry__3_i_5_n_0\,
      S(2) => \euros1__274_carry__3_i_6_n_0\,
      S(1) => \euros1__274_carry__3_i_7_n_0\,
      S(0) => \euros1__274_carry__3_i_8_n_0\
    );
\euros1__274_carry__3_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \euros1__1_carry__5_n_5\,
      I1 => \euros1__274_carry__3_i_9_n_0\,
      I2 => \euros1__229_carry__0_n_4\,
      I3 => \euros1__90_carry__3_n_5\,
      I4 => \euros1__167_carry__2_n_6\,
      O => \euros1__274_carry__3_i_1_n_0\
    );
\euros1__274_carry__3_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__167_carry__2_n_6\,
      I1 => \euros1__90_carry__3_n_5\,
      I2 => \euros1__229_carry__0_n_4\,
      O => \euros1__274_carry__3_i_10_n_0\
    );
\euros1__274_carry__3_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__167_carry__2_n_7\,
      I1 => \euros1__90_carry__3_n_6\,
      I2 => \euros1__229_carry__0_n_5\,
      O => \euros1__274_carry__3_i_11_n_0\
    );
\euros1__274_carry__3_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__167_carry__1_n_4\,
      I1 => \euros1__90_carry__3_n_7\,
      I2 => \euros1__229_carry__0_n_6\,
      O => \euros1__274_carry__3_i_12_n_0\
    );
\euros1__274_carry__3_i_13\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \euros1__229_carry__1_n_7\,
      I1 => \euros1__90_carry__3_n_4\,
      I2 => \euros1__167_carry__2_n_5\,
      O => \euros1__274_carry__3_i_13_n_0\
    );
\euros1__274_carry__3_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \euros1__229_carry__0_n_5\,
      I1 => \euros1__90_carry__3_n_6\,
      I2 => \euros1__167_carry__2_n_7\,
      O => \euros1__274_carry__3_i_14_n_0\
    );
\euros1__274_carry__3_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \euros1__229_carry__0_n_5\,
      I1 => \euros1__90_carry__3_n_6\,
      I2 => \euros1__167_carry__2_n_7\,
      I3 => \euros1__1_carry__5_n_6\,
      I4 => \euros1__274_carry__3_i_10_n_0\,
      O => \euros1__274_carry__3_i_2_n_0\
    );
\euros1__274_carry__3_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \euros1__1_carry__5_n_7\,
      I1 => \euros1__274_carry__3_i_11_n_0\,
      I2 => \euros1__229_carry__0_n_6\,
      I3 => \euros1__90_carry__3_n_7\,
      I4 => \euros1__167_carry__1_n_4\,
      O => \euros1__274_carry__3_i_3_n_0\
    );
\euros1__274_carry__3_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \euros1__229_carry__0_n_7\,
      I1 => \euros1__90_carry__2_n_4\,
      I2 => \euros1__167_carry__1_n_5\,
      I3 => \euros1__1_carry__4_n_4\,
      I4 => \euros1__274_carry__3_i_12_n_0\,
      O => \euros1__274_carry__3_i_4_n_0\
    );
\euros1__274_carry__3_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__274_carry__3_i_1_n_0\,
      I1 => \euros1__274_carry__3_i_13_n_0\,
      I2 => \euros1__167_carry__2_n_4\,
      I3 => \euros1__90_carry__4_n_7\,
      I4 => \euros1__229_carry__1_n_6\,
      I5 => \euros1__1_carry__5_n_4\,
      O => \euros1__274_carry__3_i_5_n_0\
    );
\euros1__274_carry__3_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \euros1__274_carry__3_i_2_n_0\,
      I1 => \euros1__1_carry__5_n_5\,
      I2 => \euros1__274_carry__3_i_9_n_0\,
      I3 => \euros1__229_carry__0_n_4\,
      I4 => \euros1__90_carry__3_n_5\,
      I5 => \euros1__167_carry__2_n_6\,
      O => \euros1__274_carry__3_i_6_n_0\
    );
\euros1__274_carry__3_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__274_carry__3_i_3_n_0\,
      I1 => \euros1__274_carry__3_i_14_n_0\,
      I2 => \euros1__167_carry__2_n_6\,
      I3 => \euros1__90_carry__3_n_5\,
      I4 => \euros1__229_carry__0_n_4\,
      I5 => \euros1__1_carry__5_n_6\,
      O => \euros1__274_carry__3_i_7_n_0\
    );
\euros1__274_carry__3_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \euros1__274_carry__3_i_4_n_0\,
      I1 => \euros1__1_carry__5_n_7\,
      I2 => \euros1__274_carry__3_i_11_n_0\,
      I3 => \euros1__229_carry__0_n_6\,
      I4 => \euros1__90_carry__3_n_7\,
      I5 => \euros1__167_carry__1_n_4\,
      O => \euros1__274_carry__3_i_8_n_0\
    );
\euros1__274_carry__3_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__167_carry__2_n_5\,
      I1 => \euros1__90_carry__3_n_4\,
      I2 => \euros1__229_carry__1_n_7\,
      O => \euros1__274_carry__3_i_9_n_0\
    );
\euros1__274_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__274_carry__3_n_0\,
      CO(3) => \euros1__274_carry__4_n_0\,
      CO(2) => \euros1__274_carry__4_n_1\,
      CO(1) => \euros1__274_carry__4_n_2\,
      CO(0) => \euros1__274_carry__4_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__274_carry__4_i_1_n_0\,
      DI(2) => \euros1__274_carry__4_i_2_n_0\,
      DI(1) => \euros1__274_carry__4_i_3_n_0\,
      DI(0) => \euros1__274_carry__4_i_4_n_0\,
      O(3) => \euros1__274_carry__4_n_4\,
      O(2) => \euros1__274_carry__4_n_5\,
      O(1) => \euros1__274_carry__4_n_6\,
      O(0) => \euros1__274_carry__4_n_7\,
      S(3) => \euros1__274_carry__4_i_5_n_0\,
      S(2) => \euros1__274_carry__4_i_6_n_0\,
      S(1) => \euros1__274_carry__4_i_7_n_0\,
      S(0) => \euros1__274_carry__4_i_8_n_0\
    );
\euros1__274_carry__4_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \euros1__229_carry__1_n_4\,
      I1 => \euros1__90_carry__4_n_5\,
      I2 => \euros1__167_carry__3_n_6\,
      I3 => \euros1__1_carry__6_n_5\,
      I4 => \euros1__274_carry__4_i_9_n_0\,
      O => \euros1__274_carry__4_i_1_n_0\
    );
\euros1__274_carry__4_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__167_carry__3_n_6\,
      I1 => \euros1__90_carry__4_n_5\,
      I2 => \euros1__229_carry__1_n_4\,
      O => \euros1__274_carry__4_i_10_n_0\
    );
\euros1__274_carry__4_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__167_carry__3_n_7\,
      I1 => \euros1__90_carry__4_n_6\,
      I2 => \euros1__229_carry__1_n_5\,
      O => \euros1__274_carry__4_i_11_n_0\
    );
\euros1__274_carry__4_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__167_carry__2_n_4\,
      I1 => \euros1__90_carry__4_n_7\,
      I2 => \euros1__229_carry__1_n_6\,
      O => \euros1__274_carry__4_i_12_n_0\
    );
\euros1__274_carry__4_i_13\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \euros1__229_carry__2_n_7\,
      I1 => \euros1__90_carry__4_n_4\,
      I2 => \euros1__167_carry__3_n_5\,
      O => \euros1__274_carry__4_i_13_n_0\
    );
\euros1__274_carry__4_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \euros1__229_carry__1_n_4\,
      I1 => \euros1__90_carry__4_n_5\,
      I2 => \euros1__167_carry__3_n_6\,
      O => \euros1__274_carry__4_i_14_n_0\
    );
\euros1__274_carry__4_i_15\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \euros1__229_carry__1_n_5\,
      I1 => \euros1__90_carry__4_n_6\,
      I2 => \euros1__167_carry__3_n_7\,
      O => \euros1__274_carry__4_i_15_n_0\
    );
\euros1__274_carry__4_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \euros1__229_carry__1_n_5\,
      I1 => \euros1__90_carry__4_n_6\,
      I2 => \euros1__167_carry__3_n_7\,
      I3 => \euros1__1_carry__6_n_6\,
      I4 => \euros1__274_carry__4_i_10_n_0\,
      O => \euros1__274_carry__4_i_2_n_0\
    );
\euros1__274_carry__4_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \euros1__229_carry__1_n_6\,
      I1 => \euros1__90_carry__4_n_7\,
      I2 => \euros1__167_carry__2_n_4\,
      I3 => \euros1__274_carry__4_i_11_n_0\,
      I4 => \euros1__1_carry__6_n_7\,
      O => \euros1__274_carry__4_i_3_n_0\
    );
\euros1__274_carry__4_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \euros1__229_carry__1_n_7\,
      I1 => \euros1__90_carry__3_n_4\,
      I2 => \euros1__167_carry__2_n_5\,
      I3 => \euros1__1_carry__5_n_4\,
      I4 => \euros1__274_carry__4_i_12_n_0\,
      O => \euros1__274_carry__4_i_4_n_0\
    );
\euros1__274_carry__4_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__274_carry__4_i_1_n_0\,
      I1 => \euros1__274_carry__4_i_13_n_0\,
      I2 => \euros1__167_carry__3_n_4\,
      I3 => \euros1__90_carry__5_n_7\,
      I4 => \euros1__229_carry__2_n_6\,
      I5 => \euros1__1_carry__6_n_4\,
      O => \euros1__274_carry__4_i_5_n_0\
    );
\euros1__274_carry__4_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__274_carry__4_i_2_n_0\,
      I1 => \euros1__274_carry__4_i_14_n_0\,
      I2 => \euros1__167_carry__3_n_5\,
      I3 => \euros1__90_carry__4_n_4\,
      I4 => \euros1__229_carry__2_n_7\,
      I5 => \euros1__1_carry__6_n_5\,
      O => \euros1__274_carry__4_i_6_n_0\
    );
\euros1__274_carry__4_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__274_carry__4_i_3_n_0\,
      I1 => \euros1__274_carry__4_i_15_n_0\,
      I2 => \euros1__167_carry__3_n_6\,
      I3 => \euros1__90_carry__4_n_5\,
      I4 => \euros1__229_carry__1_n_4\,
      I5 => \euros1__1_carry__6_n_6\,
      O => \euros1__274_carry__4_i_7_n_0\
    );
\euros1__274_carry__4_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \euros1__274_carry__4_i_4_n_0\,
      I1 => \euros1__1_carry__6_n_7\,
      I2 => \euros1__274_carry__4_i_11_n_0\,
      I3 => \euros1__229_carry__1_n_6\,
      I4 => \euros1__90_carry__4_n_7\,
      I5 => \euros1__167_carry__2_n_4\,
      O => \euros1__274_carry__4_i_8_n_0\
    );
\euros1__274_carry__4_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__167_carry__3_n_5\,
      I1 => \euros1__90_carry__4_n_4\,
      I2 => \euros1__229_carry__2_n_7\,
      O => \euros1__274_carry__4_i_9_n_0\
    );
\euros1__274_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__274_carry__4_n_0\,
      CO(3 downto 1) => \NLW_euros1__274_carry__5_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \euros1__274_carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \euros1__274_carry__5_i_1_n_0\,
      O(3 downto 2) => \NLW_euros1__274_carry__5_O_UNCONNECTED\(3 downto 2),
      O(1) => \euros1__274_carry__5_n_6\,
      O(0) => \euros1__274_carry__5_n_7\,
      S(3 downto 2) => B"00",
      S(1) => \euros1__274_carry__5_i_2_n_0\,
      S(0) => \euros1__274_carry__5_i_3_n_0\
    );
\euros1__274_carry__5_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \euros1__229_carry__2_n_7\,
      I1 => \euros1__90_carry__4_n_4\,
      I2 => \euros1__167_carry__3_n_5\,
      I3 => \euros1__1_carry__6_n_4\,
      I4 => \euros1__274_carry__5_i_4_n_0\,
      O => \euros1__274_carry__5_i_1_n_0\
    );
\euros1__274_carry__5_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E8818117177E7EE8"
    )
        port map (
      I0 => \euros1__274_carry__5_i_5_n_0\,
      I1 => \euros1__1_carry__7_n_7\,
      I2 => \euros1__229_carry__2_n_5\,
      I3 => \euros1__90_carry__5_n_6\,
      I4 => \euros1__167_carry__4_n_7\,
      I5 => \euros1__274_carry__5_i_6_n_0\,
      O => \euros1__274_carry__5_i_2_n_0\
    );
\euros1__274_carry__5_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \euros1__274_carry__5_i_1_n_0\,
      I1 => \euros1__1_carry__7_n_7\,
      I2 => \euros1__274_carry__5_i_7_n_0\,
      I3 => \euros1__229_carry__2_n_6\,
      I4 => \euros1__90_carry__5_n_7\,
      I5 => \euros1__167_carry__3_n_4\,
      O => \euros1__274_carry__5_i_3_n_0\
    );
\euros1__274_carry__5_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__167_carry__3_n_4\,
      I1 => \euros1__90_carry__5_n_7\,
      I2 => \euros1__229_carry__2_n_6\,
      O => \euros1__274_carry__5_i_4_n_0\
    );
\euros1__274_carry__5_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \euros1__229_carry__2_n_6\,
      I1 => \euros1__90_carry__5_n_7\,
      I2 => \euros1__167_carry__3_n_4\,
      O => \euros1__274_carry__5_i_5_n_0\
    );
\euros1__274_carry__5_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__167_carry__4_n_6\,
      I1 => \euros1__229_carry__2_n_4\,
      I2 => \euros1__1_carry__7_n_6\,
      I3 => \euros1__90_carry__5_n_5\,
      O => \euros1__274_carry__5_i_6_n_0\
    );
\euros1__274_carry__5_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__167_carry__4_n_7\,
      I1 => \euros1__90_carry__5_n_6\,
      I2 => \euros1__229_carry__2_n_5\,
      O => \euros1__274_carry__5_i_7_n_0\
    );
\euros1__274_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \euros1__1_carry__1_n_5\,
      I1 => \euros1__90_carry_n_4\,
      O => \euros1__274_carry_i_1_n_0\
    );
\euros1__274_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \euros1__1_carry__1_n_6\,
      I1 => \euros1__90_carry_n_5\,
      O => \euros1__274_carry_i_2_n_0\
    );
\euros1__274_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \euros1__1_carry__1_n_7\,
      I1 => \euros1__90_carry_n_6\,
      O => \euros1__274_carry_i_3_n_0\
    );
\euros1__274_carry_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \euros1__90_carry_n_7\,
      I1 => \euros1__1_carry__0_n_4\,
      O => \euros1__274_carry_i_4_n_0\
    );
\euros1__274_carry_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => \euros1__1_carry__1_n_4\,
      I1 => \euros1__90_carry__0_n_7\,
      I2 => \euros1__90_carry_n_4\,
      I3 => \euros1__1_carry__1_n_5\,
      O => \euros1__274_carry_i_5_n_0\
    );
\euros1__274_carry_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \euros1__90_carry_n_5\,
      I1 => \euros1__1_carry__1_n_6\,
      I2 => \euros1__90_carry_n_4\,
      I3 => \euros1__1_carry__1_n_5\,
      O => \euros1__274_carry_i_6_n_0\
    );
\euros1__274_carry_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \euros1__90_carry_n_6\,
      I1 => \euros1__1_carry__1_n_7\,
      I2 => \euros1__90_carry_n_5\,
      I3 => \euros1__1_carry__1_n_6\,
      O => \euros1__274_carry_i_7_n_0\
    );
\euros1__274_carry_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \euros1__1_carry__0_n_4\,
      I1 => \euros1__90_carry_n_7\,
      I2 => \euros1__90_carry_n_6\,
      I3 => \euros1__1_carry__1_n_7\,
      O => \euros1__274_carry_i_8_n_0\
    );
\euros1__331_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3 downto 2) => \NLW_euros1__331_carry_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \euros1__331_carry_n_2\,
      CO(0) => \euros1__331_carry_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \euros1__331_carry_i_1_n_0\,
      DI(0) => '0',
      O(3) => \NLW_euros1__331_carry_O_UNCONNECTED\(3),
      O(2) => \euros1__331_carry_n_5\,
      O(1) => \euros1__331_carry_n_6\,
      O(0) => \euros1__331_carry_n_7\,
      S(3) => '0',
      S(2) => \euros1__331_carry_i_2_n_0\,
      S(1) => \euros1__331_carry_i_3_n_0\,
      S(0) => \euros1__331_carry_i_4_n_0\
    );
\euros1__331_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \euros1__274_carry__4_n_4\,
      I1 => \euros1__274_carry__4_n_7\,
      O => \euros1__331_carry_i_1_n_0\
    );
\euros1__331_carry_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66969969"
    )
        port map (
      I0 => \euros1__274_carry__5_n_6\,
      I1 => \euros1__274_carry__4_n_5\,
      I2 => \euros1__274_carry__5_n_7\,
      I3 => \euros1__274_carry__4_n_6\,
      I4 => \euros1__274_carry__4_n_7\,
      O => \euros1__331_carry_i_2_n_0\
    );
\euros1__331_carry_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => \euros1__274_carry__4_n_7\,
      I1 => \euros1__274_carry__4_n_4\,
      I2 => \euros1__274_carry__5_n_7\,
      I3 => \euros1__274_carry__4_n_6\,
      O => \euros1__331_carry_i_3_n_0\
    );
\euros1__331_carry_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \euros1__274_carry__4_n_4\,
      I1 => \euros1__274_carry__4_n_7\,
      O => \euros1__331_carry_i_4_n_0\
    );
\euros1__337_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \euros1__337_carry_n_0\,
      CO(2) => \euros1__337_carry_n_1\,
      CO(1) => \euros1__337_carry_n_2\,
      CO(0) => \euros1__337_carry_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => total_reg(3 downto 0),
      O(3) => \euros1__337_carry_n_4\,
      O(2) => \euros1__337_carry_n_5\,
      O(1 downto 0) => p_1_in(1 downto 0),
      S(3) => \euros1__337_carry_i_1_n_0\,
      S(2) => \euros1__337_carry_i_2_n_0\,
      S(1) => \euros1__337_carry_i_3_n_0\,
      S(0) => \euros1__337_carry_i_4_n_0\
    );
\euros1__337_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__337_carry_n_0\,
      CO(3) => \NLW_euros1__337_carry__0_CO_UNCONNECTED\(3),
      CO(2) => \euros1__337_carry__0_n_1\,
      CO(1) => \euros1__337_carry__0_n_2\,
      CO(0) => \euros1__337_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => total_reg(6 downto 4),
      O(3) => \euros1__337_carry__0_n_4\,
      O(2) => \euros1__337_carry__0_n_5\,
      O(1) => \euros1__337_carry__0_n_6\,
      O(0) => \euros1__337_carry__0_n_7\,
      S(3) => \euros1__337_carry__0_i_1_n_0\,
      S(2) => \euros1__337_carry__0_i_2_n_0\,
      S(1) => \euros1__337_carry__0_i_3_n_0\,
      S(0) => \euros1__337_carry__0_i_4_n_0\
    );
\euros1__337_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_reg(7),
      I1 => \euros1__331_carry_n_5\,
      O => \euros1__337_carry__0_i_1_n_0\
    );
\euros1__337_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_reg(6),
      I1 => \euros1__331_carry_n_6\,
      O => \euros1__337_carry__0_i_2_n_0\
    );
\euros1__337_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_reg(5),
      I1 => \euros1__331_carry_n_7\,
      O => \euros1__337_carry__0_i_3_n_0\
    );
\euros1__337_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_reg(4),
      I1 => \euros1__274_carry__4_n_5\,
      O => \euros1__337_carry__0_i_4_n_0\
    );
\euros1__337_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_reg(3),
      I1 => \euros1__274_carry__4_n_6\,
      O => \euros1__337_carry_i_1_n_0\
    );
\euros1__337_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_reg(2),
      I1 => \euros1__274_carry__4_n_7\,
      O => \euros1__337_carry_i_2_n_0\
    );
\euros1__337_carry_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => total_reg(1),
      O => \euros1__337_carry_i_3_n_0\
    );
\euros1__337_carry_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => total_reg(0),
      O => \euros1__337_carry_i_4_n_0\
    );
\euros1__90_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \euros1__90_carry_n_0\,
      CO(2) => \euros1__90_carry_n_1\,
      CO(1) => \euros1__90_carry_n_2\,
      CO(0) => \euros1__90_carry_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__90_carry_i_1_n_0\,
      DI(2) => total_reg(0),
      DI(1 downto 0) => B"01",
      O(3) => \euros1__90_carry_n_4\,
      O(2) => \euros1__90_carry_n_5\,
      O(1) => \euros1__90_carry_n_6\,
      O(0) => \euros1__90_carry_n_7\,
      S(3) => \euros1__90_carry_i_2_n_0\,
      S(2) => \euros1__90_carry_i_3_n_0\,
      S(1) => \euros1__90_carry_i_4_n_0\,
      S(0) => total_reg(0)
    );
\euros1__90_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__90_carry_n_0\,
      CO(3) => \euros1__90_carry__0_n_0\,
      CO(2) => \euros1__90_carry__0_n_1\,
      CO(1) => \euros1__90_carry__0_n_2\,
      CO(0) => \euros1__90_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__90_carry__0_i_1_n_0\,
      DI(2) => \euros1__90_carry__0_i_2_n_0\,
      DI(1) => \euros1__90_carry__0_i_3_n_0\,
      DI(0) => \euros1__90_carry__0_i_4_n_0\,
      O(3) => \euros1__90_carry__0_n_4\,
      O(2) => \euros1__90_carry__0_n_5\,
      O(1) => \euros1__90_carry__0_n_6\,
      O(0) => \euros1__90_carry__0_n_7\,
      S(3) => \euros1__90_carry__0_i_5_n_0\,
      S(2) => \euros1__90_carry__0_i_6_n_0\,
      S(1) => \euros1__90_carry__0_i_7_n_0\,
      S(0) => \euros1__90_carry__0_i_8_n_0\
    );
\euros1__90_carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => total_reg(2),
      I1 => total_reg(4),
      I2 => total_reg(6),
      O => \euros1__90_carry__0_i_1_n_0\
    );
\euros1__90_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => total_reg(5),
      I1 => total_reg(3),
      I2 => total_reg(1),
      O => \euros1__90_carry__0_i_2_n_0\
    );
\euros1__90_carry__0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => total_reg(0),
      I1 => total_reg(2),
      I2 => total_reg(4),
      O => \euros1__90_carry__0_i_3_n_0\
    );
\euros1__90_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => total_reg(3),
      I1 => total_reg(1),
      O => \euros1__90_carry__0_i_4_n_0\
    );
\euros1__90_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(3),
      I1 => total_reg(5),
      I2 => total_reg(7),
      I3 => \euros1__90_carry__0_i_1_n_0\,
      O => \euros1__90_carry__0_i_5_n_0\
    );
\euros1__90_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(2),
      I1 => total_reg(4),
      I2 => total_reg(6),
      I3 => \euros1__90_carry__0_i_2_n_0\,
      O => \euros1__90_carry__0_i_6_n_0\
    );
\euros1__90_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(5),
      I1 => total_reg(3),
      I2 => total_reg(1),
      I3 => \euros1__90_carry__0_i_3_n_0\,
      O => \euros1__90_carry__0_i_7_n_0\
    );
\euros1__90_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(0),
      I1 => total_reg(2),
      I2 => total_reg(4),
      I3 => \euros1__90_carry__0_i_4_n_0\,
      O => \euros1__90_carry__0_i_8_n_0\
    );
\euros1__90_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__90_carry__0_n_0\,
      CO(3) => \euros1__90_carry__1_n_0\,
      CO(2) => \euros1__90_carry__1_n_1\,
      CO(1) => \euros1__90_carry__1_n_2\,
      CO(0) => \euros1__90_carry__1_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__90_carry__1_i_1_n_0\,
      DI(2) => \euros1__90_carry__1_i_2_n_0\,
      DI(1) => \euros1__90_carry__1_i_3_n_0\,
      DI(0) => \euros1__90_carry__1_i_4_n_0\,
      O(3) => \euros1__90_carry__1_n_4\,
      O(2) => \euros1__90_carry__1_n_5\,
      O(1) => \euros1__90_carry__1_n_6\,
      O(0) => \euros1__90_carry__1_n_7\,
      S(3) => \euros1__90_carry__1_i_5_n_0\,
      S(2) => \euros1__90_carry__1_i_6_n_0\,
      S(1) => \euros1__90_carry__1_i_7_n_0\,
      S(0) => \euros1__90_carry__1_i_8_n_0\
    );
\euros1__90_carry__1_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => total_reg(6),
      I1 => total_reg(8),
      I2 => total_reg(10),
      O => \euros1__90_carry__1_i_1_n_0\
    );
\euros1__90_carry__1_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4D"
    )
        port map (
      I0 => total_reg(7),
      I1 => total_reg(5),
      I2 => total_reg(9),
      O => \euros1__90_carry__1_i_2_n_0\
    );
\euros1__90_carry__1_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => total_reg(4),
      I1 => total_reg(6),
      I2 => total_reg(8),
      O => \euros1__90_carry__1_i_3_n_0\
    );
\euros1__90_carry__1_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => total_reg(3),
      I1 => total_reg(5),
      I2 => total_reg(7),
      O => \euros1__90_carry__1_i_4_n_0\
    );
\euros1__90_carry__1_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(7),
      I1 => total_reg(9),
      I2 => total_reg(11),
      I3 => \euros1__90_carry__1_i_1_n_0\,
      O => \euros1__90_carry__1_i_5_n_0\
    );
\euros1__90_carry__1_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(6),
      I1 => total_reg(8),
      I2 => total_reg(10),
      I3 => \euros1__90_carry__1_i_2_n_0\,
      O => \euros1__90_carry__1_i_6_n_0\
    );
\euros1__90_carry__1_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(7),
      I1 => total_reg(5),
      I2 => total_reg(9),
      I3 => \euros1__90_carry__1_i_3_n_0\,
      O => \euros1__90_carry__1_i_7_n_0\
    );
\euros1__90_carry__1_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(4),
      I1 => total_reg(6),
      I2 => total_reg(8),
      I3 => \euros1__90_carry__1_i_4_n_0\,
      O => \euros1__90_carry__1_i_8_n_0\
    );
\euros1__90_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__90_carry__1_n_0\,
      CO(3) => \euros1__90_carry__2_n_0\,
      CO(2) => \euros1__90_carry__2_n_1\,
      CO(1) => \euros1__90_carry__2_n_2\,
      CO(0) => \euros1__90_carry__2_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__90_carry__2_i_1_n_0\,
      DI(2) => \euros1__90_carry__2_i_2_n_0\,
      DI(1) => \euros1__90_carry__2_i_3_n_0\,
      DI(0) => \euros1__90_carry__2_i_4_n_0\,
      O(3) => \euros1__90_carry__2_n_4\,
      O(2) => \euros1__90_carry__2_n_5\,
      O(1) => \euros1__90_carry__2_n_6\,
      O(0) => \euros1__90_carry__2_n_7\,
      S(3) => \euros1__90_carry__2_i_5_n_0\,
      S(2) => \euros1__90_carry__2_i_6_n_0\,
      S(1) => \euros1__90_carry__2_i_7_n_0\,
      S(0) => \euros1__90_carry__2_i_8_n_0\
    );
\euros1__90_carry__2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => total_reg(10),
      I1 => total_reg(12),
      I2 => total_reg(14),
      O => \euros1__90_carry__2_i_1_n_0\
    );
\euros1__90_carry__2_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => total_reg(13),
      I1 => total_reg(11),
      I2 => total_reg(9),
      O => \euros1__90_carry__2_i_2_n_0\
    );
\euros1__90_carry__2_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => total_reg(8),
      I1 => total_reg(10),
      I2 => total_reg(12),
      O => \euros1__90_carry__2_i_3_n_0\
    );
\euros1__90_carry__2_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => total_reg(7),
      I1 => total_reg(9),
      I2 => total_reg(11),
      O => \euros1__90_carry__2_i_4_n_0\
    );
\euros1__90_carry__2_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(11),
      I1 => total_reg(13),
      I2 => total_reg(15),
      I3 => \euros1__90_carry__2_i_1_n_0\,
      O => \euros1__90_carry__2_i_5_n_0\
    );
\euros1__90_carry__2_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(10),
      I1 => total_reg(12),
      I2 => total_reg(14),
      I3 => \euros1__90_carry__2_i_2_n_0\,
      O => \euros1__90_carry__2_i_6_n_0\
    );
\euros1__90_carry__2_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(13),
      I1 => total_reg(11),
      I2 => total_reg(9),
      I3 => \euros1__90_carry__2_i_3_n_0\,
      O => \euros1__90_carry__2_i_7_n_0\
    );
\euros1__90_carry__2_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(8),
      I1 => total_reg(10),
      I2 => total_reg(12),
      I3 => \euros1__90_carry__2_i_4_n_0\,
      O => \euros1__90_carry__2_i_8_n_0\
    );
\euros1__90_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__90_carry__2_n_0\,
      CO(3) => \euros1__90_carry__3_n_0\,
      CO(2) => \euros1__90_carry__3_n_1\,
      CO(1) => \euros1__90_carry__3_n_2\,
      CO(0) => \euros1__90_carry__3_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__90_carry__3_i_1_n_0\,
      DI(2) => \euros1__90_carry__3_i_2_n_0\,
      DI(1) => \euros1__90_carry__3_i_3_n_0\,
      DI(0) => \euros1__90_carry__3_i_4_n_0\,
      O(3) => \euros1__90_carry__3_n_4\,
      O(2) => \euros1__90_carry__3_n_5\,
      O(1) => \euros1__90_carry__3_n_6\,
      O(0) => \euros1__90_carry__3_n_7\,
      S(3) => \euros1__90_carry__3_i_5_n_0\,
      S(2) => \euros1__90_carry__3_i_6_n_0\,
      S(1) => \euros1__90_carry__3_i_7_n_0\,
      S(0) => \euros1__90_carry__3_i_8_n_0\
    );
\euros1__90_carry__3_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => total_reg(14),
      I1 => total_reg(16),
      I2 => total_reg(18),
      O => \euros1__90_carry__3_i_1_n_0\
    );
\euros1__90_carry__3_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => total_reg(13),
      I1 => total_reg(15),
      I2 => total_reg(17),
      O => \euros1__90_carry__3_i_2_n_0\
    );
\euros1__90_carry__3_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => total_reg(12),
      I1 => total_reg(14),
      I2 => total_reg(16),
      O => \euros1__90_carry__3_i_3_n_0\
    );
\euros1__90_carry__3_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => total_reg(11),
      I1 => total_reg(13),
      I2 => total_reg(15),
      O => \euros1__90_carry__3_i_4_n_0\
    );
\euros1__90_carry__3_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(15),
      I1 => total_reg(17),
      I2 => total_reg(19),
      I3 => \euros1__90_carry__3_i_1_n_0\,
      O => \euros1__90_carry__3_i_5_n_0\
    );
\euros1__90_carry__3_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(14),
      I1 => total_reg(16),
      I2 => total_reg(18),
      I3 => \euros1__90_carry__3_i_2_n_0\,
      O => \euros1__90_carry__3_i_6_n_0\
    );
\euros1__90_carry__3_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(13),
      I1 => total_reg(15),
      I2 => total_reg(17),
      I3 => \euros1__90_carry__3_i_3_n_0\,
      O => \euros1__90_carry__3_i_7_n_0\
    );
\euros1__90_carry__3_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(12),
      I1 => total_reg(14),
      I2 => total_reg(16),
      I3 => \euros1__90_carry__3_i_4_n_0\,
      O => \euros1__90_carry__3_i_8_n_0\
    );
\euros1__90_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__90_carry__3_n_0\,
      CO(3) => \euros1__90_carry__4_n_0\,
      CO(2) => \euros1__90_carry__4_n_1\,
      CO(1) => \euros1__90_carry__4_n_2\,
      CO(0) => \euros1__90_carry__4_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__90_carry__4_i_1_n_0\,
      DI(2) => \euros1__90_carry__4_i_2_n_0\,
      DI(1) => \euros1__90_carry__4_i_3_n_0\,
      DI(0) => \euros1__90_carry__4_i_4_n_0\,
      O(3) => \euros1__90_carry__4_n_4\,
      O(2) => \euros1__90_carry__4_n_5\,
      O(1) => \euros1__90_carry__4_n_6\,
      O(0) => \euros1__90_carry__4_n_7\,
      S(3) => \euros1__90_carry__4_i_5_n_0\,
      S(2) => \euros1__90_carry__4_i_6_n_0\,
      S(1) => \euros1__90_carry__4_i_7_n_0\,
      S(0) => \euros1__90_carry__4_i_8_n_0\
    );
\euros1__90_carry__4_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => total_reg(18),
      I1 => total_reg(20),
      I2 => total_reg(22),
      O => \euros1__90_carry__4_i_1_n_0\
    );
\euros1__90_carry__4_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => total_reg(17),
      I1 => total_reg(19),
      I2 => total_reg(21),
      O => \euros1__90_carry__4_i_2_n_0\
    );
\euros1__90_carry__4_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => total_reg(16),
      I1 => total_reg(18),
      I2 => total_reg(20),
      O => \euros1__90_carry__4_i_3_n_0\
    );
\euros1__90_carry__4_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => total_reg(15),
      I1 => total_reg(17),
      I2 => total_reg(19),
      O => \euros1__90_carry__4_i_4_n_0\
    );
\euros1__90_carry__4_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(19),
      I1 => total_reg(21),
      I2 => total_reg(23),
      I3 => \euros1__90_carry__4_i_1_n_0\,
      O => \euros1__90_carry__4_i_5_n_0\
    );
\euros1__90_carry__4_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(18),
      I1 => total_reg(20),
      I2 => total_reg(22),
      I3 => \euros1__90_carry__4_i_2_n_0\,
      O => \euros1__90_carry__4_i_6_n_0\
    );
\euros1__90_carry__4_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(17),
      I1 => total_reg(19),
      I2 => total_reg(21),
      I3 => \euros1__90_carry__4_i_3_n_0\,
      O => \euros1__90_carry__4_i_7_n_0\
    );
\euros1__90_carry__4_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(16),
      I1 => total_reg(18),
      I2 => total_reg(20),
      I3 => \euros1__90_carry__4_i_4_n_0\,
      O => \euros1__90_carry__4_i_8_n_0\
    );
\euros1__90_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__90_carry__4_n_0\,
      CO(3 downto 2) => \NLW_euros1__90_carry__5_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \euros1__90_carry__5_n_2\,
      CO(0) => \euros1__90_carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \euros1__90_carry__5_i_1_n_0\,
      DI(0) => \euros1__90_carry__5_i_2_n_0\,
      O(3) => \NLW_euros1__90_carry__5_O_UNCONNECTED\(3),
      O(2) => \euros1__90_carry__5_n_5\,
      O(1) => \euros1__90_carry__5_n_6\,
      O(0) => \euros1__90_carry__5_n_7\,
      S(3) => '0',
      S(2) => \euros1__90_carry__5_i_3_n_0\,
      S(1) => \euros1__90_carry__5_i_4_n_0\,
      S(0) => \euros1__90_carry__5_i_5_n_0\
    );
\euros1__90_carry__5_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => total_reg(20),
      I1 => total_reg(22),
      I2 => total_reg(24),
      O => \euros1__90_carry__5_i_1_n_0\
    );
\euros1__90_carry__5_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => total_reg(19),
      I1 => total_reg(21),
      I2 => total_reg(23),
      O => \euros1__90_carry__5_i_2_n_0\
    );
\euros1__90_carry__5_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669696996969669"
    )
        port map (
      I0 => total_reg(24),
      I1 => total_reg(22),
      I2 => total_reg(26),
      I3 => total_reg(25),
      I4 => total_reg(23),
      I5 => total_reg(21),
      O => \euros1__90_carry__5_i_3_n_0\
    );
\euros1__90_carry__5_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__90_carry__5_i_1_n_0\,
      I1 => total_reg(23),
      I2 => total_reg(21),
      I3 => total_reg(25),
      O => \euros1__90_carry__5_i_4_n_0\
    );
\euros1__90_carry__5_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_reg(20),
      I1 => total_reg(22),
      I2 => total_reg(24),
      I3 => \euros1__90_carry__5_i_2_n_0\,
      O => \euros1__90_carry__5_i_5_n_0\
    );
\euros1__90_carry_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => total_reg(0),
      O => \euros1__90_carry_i_1_n_0\
    );
\euros1__90_carry_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => total_reg(3),
      I1 => total_reg(1),
      I2 => total_reg(0),
      O => \euros1__90_carry_i_2_n_0\
    );
\euros1__90_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_reg(0),
      I1 => total_reg(2),
      O => \euros1__90_carry_i_3_n_0\
    );
\euros1__90_carry_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => total_reg(1),
      O => \euros1__90_carry_i_4_n_0\
    );
\i___0_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(1),
      I1 => cent(6),
      O => \i___0_carry__0_i_1_n_0\
    );
\i___0_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(5),
      I1 => cent(0),
      O => \i___0_carry__0_i_2_n_0\
    );
\i___0_carry__0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBB8B8B8888B888"
    )
        port map (
      I0 => total_reg(6),
      I1 => total_reg_26_sn_1,
      I2 => \euros1__337_carry__0_n_6\,
      I3 => \euros1__337_carry__0_n_4\,
      I4 => \string_cent_decenas[1]5_carry__1_i_8_n_0\,
      I5 => \euros1__337_carry__0_n_5\,
      O => cent(6)
    );
\i___0_carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => cent(6),
      I1 => cent(1),
      I2 => cent(5),
      I3 => cent(0),
      O => \i___0_carry__0_i_4_n_0\
    );
\i___0_carry__0_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(5),
      I1 => cent(0),
      O => \i___0_carry__0_i_5_n_0\
    );
\i___0_carry__0_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(6),
      I1 => cent(4),
      O => \i___0_carry__0_i_6_n_0\
    );
\i___0_carry__0_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(5),
      I1 => cent(3),
      O => \i___0_carry__0_i_7_n_0\
    );
\i___0_carry__1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => cent(1),
      I1 => cent(6),
      O => \i___0_carry__1_i_1_n_0\
    );
\i___0_carry__1_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF0000CD30CD30"
    )
        port map (
      I0 => \euros1__337_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5_carry__1_i_8_n_0\,
      I2 => \euros1__337_carry__0_n_4\,
      I3 => \euros1__337_carry__0_n_6\,
      I4 => total_reg(5),
      I5 => total_reg_26_sn_1,
      O => \i___0_carry__1_i_2_n_0\
    );
\i___0_carry__1_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAF0F0F0C3"
    )
        port map (
      I0 => total_reg(4),
      I1 => \string_cent_decenas[1]5__185_carry_i_9_n_0\,
      I2 => \euros1__337_carry__0_n_7\,
      I3 => \euros1__337_carry_n_5\,
      I4 => \euros1__337_carry_n_4\,
      I5 => total_reg_26_sn_1,
      O => \i___0_carry__1_i_3_n_0\
    );
\i___0_carry__1_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8B8B88B"
    )
        port map (
      I0 => total_reg(3),
      I1 => total_reg_26_sn_1,
      I2 => \euros1__337_carry_n_4\,
      I3 => \string_cent_decenas[1]5__185_carry_i_9_n_0\,
      I4 => \euros1__337_carry_n_5\,
      O => \i___0_carry__1_i_4_n_0\
    );
\i___0_carry__1_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"87"
    )
        port map (
      I0 => cent(6),
      I1 => cent(1),
      I2 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      O => \i___0_carry__1_i_5_n_0\
    );
\i___0_carry__2_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBB8B8B8888B888"
    )
        port map (
      I0 => total_reg(6),
      I1 => total_reg_26_sn_1,
      I2 => \euros1__337_carry__0_n_6\,
      I3 => \euros1__337_carry__0_n_4\,
      I4 => \string_cent_decenas[1]5_carry__1_i_8_n_0\,
      I5 => \euros1__337_carry__0_n_5\,
      O => \i___0_carry__2_i_1_n_0\
    );
\i___0_carry_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAF0F0F0C3"
    )
        port map (
      I0 => total_reg(4),
      I1 => \string_cent_decenas[1]5__185_carry_i_9_n_0\,
      I2 => \euros1__337_carry__0_n_7\,
      I3 => \euros1__337_carry_n_5\,
      I4 => \euros1__337_carry_n_4\,
      I5 => total_reg_26_sn_1,
      O => cent(4)
    );
\i___0_carry_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      O => \i___0_carry_i_2_n_0\
    );
\i___0_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I1 => cent(4),
      O => \i___0_carry_i_3_n_0\
    );
\i___0_carry_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(3),
      I1 => cent(1),
      O => \i___0_carry_i_4_n_0\
    );
\i___0_carry_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I1 => cent(0),
      O => \i___0_carry_i_5_n_0\
    );
\i___0_carry_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_reg(1),
      I1 => total_reg_26_sn_1,
      I2 => p_1_in(1),
      O => \i___0_carry_i_6_n_0\
    );
\i___106_carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0880"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_6\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_5\,
      O => \i___106_carry__0_i_1_n_0\
    );
\i___106_carry__0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BE282828"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___0_carry__2_n_2\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_5\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_6\,
      I3 => cent(0),
      I4 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_6\,
      O => \i___106_carry__0_i_2_n_0\
    );
\i___106_carry__0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"28"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___0_carry__2_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_6\,
      I2 => cent(0),
      O => \i___106_carry__0_i_3_n_0\
    );
\i___106_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_4\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_7\,
      O => \i___106_carry__0_i_4_n_0\
    );
\i___106_carry__0_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F087870F"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_5\,
      I2 => \i___106_carry__0_i_9_n_0\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_5\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_4\,
      O => \i___106_carry__0_i_5_n_0\
    );
\i___106_carry__0_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => \i___106_carry__0_i_2_n_0\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_5\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_6\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_5\,
      O => \i___106_carry__0_i_6_n_0\
    );
\i___106_carry__0_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669966996"
    )
        port map (
      I0 => \i___106_carry__0_i_3_n_0\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___0_carry__2_n_2\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_5\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_6\,
      I4 => cent(0),
      I5 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_6\,
      O => \i___106_carry__0_i_7_n_0\
    );
\i___106_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___0_carry__2_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_6\,
      I2 => cent(0),
      I3 => \i___106_carry__0_i_4_n_0\,
      O => \i___106_carry__0_i_8_n_0\
    );
\i___106_carry__0_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_7\,
      I1 => cent(0),
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_4\,
      O => \i___106_carry__0_i_9_n_0\
    );
\i___106_carry__1_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6900000069696900"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_5\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_5\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_6\,
      I5 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      O => \i___106_carry__1_i_1_n_0\
    );
\i___106_carry__1_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_7\,
      O => \i___106_carry__1_i_10_n_0\
    );
\i___106_carry__1_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_5\,
      I1 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_6\,
      O => \i___106_carry__1_i_11_n_0\
    );
\i___106_carry__1_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_6\,
      I1 => cent(1),
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_7\,
      O => \i___106_carry__1_i_12_n_0\
    );
\i___106_carry__1_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969690069000000"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_6\,
      I1 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_5\,
      I3 => cent(1),
      I4 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_7\,
      I5 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_6\,
      O => \i___106_carry__1_i_2_n_0\
    );
\i___106_carry__1_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E80000E800E8E800"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_4\,
      I2 => cent(0),
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_7\,
      I4 => cent(1),
      I5 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_6\,
      O => \i___106_carry__1_i_3_n_0\
    );
\i___106_carry__1_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80080880"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_4\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_5\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_4\,
      I3 => cent(0),
      I4 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_7\,
      O => \i___106_carry__1_i_4_n_0\
    );
\i___106_carry__1_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AA9955695566AA9"
    )
        port map (
      I0 => \i___106_carry__1_i_1_n_0\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_7\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_5\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_4\,
      I5 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_6\,
      O => \i___106_carry__1_i_5_n_0\
    );
\i___106_carry__1_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"244DDBB2"
    )
        port map (
      I0 => \i___106_carry__1_i_9_n_0\,
      I1 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_6\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_5\,
      I4 => \i___106_carry__1_i_10_n_0\,
      O => \i___106_carry__1_i_6_n_0\
    );
\i___106_carry__1_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A995566A"
    )
        port map (
      I0 => \i___106_carry__1_i_3_n_0\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_6\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_7\,
      I3 => cent(1),
      I4 => \i___106_carry__1_i_11_n_0\,
      O => \i___106_carry__1_i_7_n_0\
    );
\i___106_carry__1_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FF8F8808007077F"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_4\,
      I2 => cent(0),
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_4\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_7\,
      I5 => \i___106_carry__1_i_12_n_0\,
      O => \i___106_carry__1_i_8_n_0\
    );
\i___106_carry__1_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_7\,
      I2 => cent(1),
      O => \i___106_carry__1_i_9_n_0\
    );
\i___106_carry__2_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"90006660"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_6\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_4\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__2_i_1_n_0\
    );
\i___106_carry__2_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_4\,
      O => \i___106_carry__2_i_10_n_0\
    );
\i___106_carry__2_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_5\,
      O => \i___106_carry__2_i_11_n_0\
    );
\i___106_carry__2_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60009990"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_4\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_6\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_7\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_5\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__2_i_2_n_0\
    );
\i___106_carry__2_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60009990"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_7\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_6\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__2_i_3_n_0\
    );
\i___106_carry__2_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60009990"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_4\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_5\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_7\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__2_i_4_n_0\
    );
\i___106_carry__2_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"56A96A95"
    )
        port map (
      I0 => \i___106_carry__2_i_1_n_0\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_6\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_7\,
      O => \i___106_carry__2_i_5_n_0\
    );
\i___106_carry__2_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0E7070F1F18F8F0E"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_7\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_4\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_6\,
      I5 => \i___106_carry__2_i_9_n_0\,
      O => \i___106_carry__2_i_6_n_0\
    );
\i___106_carry__2_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0FE1780F780FF01E"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_4\,
      I2 => \i___106_carry__2_i_10_n_0\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_5\,
      I5 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_7\,
      O => \i___106_carry__2_i_7_n_0\
    );
\i___106_carry__2_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0E7070F1F18F8F0E"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_5\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_6\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_4\,
      I5 => \i___106_carry__2_i_11_n_0\,
      O => \i___106_carry__2_i_8_n_0\
    );
\i___106_carry__2_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_7\,
      O => \i___106_carry__2_i_9_n_0\
    );
\i___106_carry__3_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1062"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__3_i_1_n_0\
    );
\i___106_carry__3_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1062"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_4\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_5\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__3_i_2_n_0\
    );
\i___106_carry__3_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1062"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_6\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__3_i_3_n_0\
    );
\i___106_carry__3_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_6\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      O => \i___106_carry__3_i_4_n_0\
    );
\i___106_carry__3_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEF80107"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_4\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_7\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_6\,
      O => \i___106_carry__3_i_5_n_0\
    );
\i___106_carry__3_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEF80107"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_7\,
      O => \i___106_carry__3_i_6_n_0\
    );
\i___106_carry__3_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEF80107"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_5\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_4\,
      O => \i___106_carry__3_i_7_n_0\
    );
\i___106_carry__3_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEF80107"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_6\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_5\,
      O => \i___106_carry__3_i_8_n_0\
    );
\i___106_carry__4_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"48"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__4_i_1_n_0\
    );
\i___106_carry__4_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1062"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_1\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_6\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__4_i_2_n_0\
    );
\i___106_carry__4_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1062"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_7\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__4_i_3_n_0\
    );
\i___106_carry__4_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"95"
    )
        port map (
      I0 => '0',
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__4_i_4_n_0\
    );
\i___106_carry__4_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_1\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__4_i_5_n_0\
    );
\i___106_carry__4_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0107"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_1\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      O => \i___106_carry__4_i_6_n_0\
    );
\i___106_carry__4_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEF80107"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_6\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_1\,
      O => \i___106_carry__4_i_7_n_0\
    );
\i___106_carry__5_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      O => \i___106_carry__5_i_1_n_0\
    );
\i___106_carry__5_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"95"
    )
        port map (
      I0 => '0',
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__5_i_2_n_0\
    );
\i___106_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_4\,
      O => \i___106_carry_i_1_n_0\
    );
\i___106_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_5\,
      O => \i___106_carry_i_2_n_0\
    );
\i___106_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_6\,
      O => \i___106_carry_i_3_n_0\
    );
\i___106_carry_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => cent(0),
      I1 => \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_4\,
      O => \i___106_carry_i_4_n_0\
    );
\i___106_carry_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_4\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_7\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_5\,
      O => \i___106_carry_i_5_n_0\
    );
\i___106_carry_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_6\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_5\,
      O => \i___106_carry_i_6_n_0\
    );
\i___106_carry_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_7\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_5\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_6\,
      O => \i___106_carry_i_7_n_0\
    );
\i___106_carry_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_4\,
      I1 => cent(0),
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_6\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_7\,
      O => \i___106_carry_i_8_n_0\
    );
\i___163_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_4\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_7\,
      O => \i___163_carry_i_1_n_0\
    );
\i___163_carry_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66969969"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___106_carry__5_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_5\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___106_carry__5_n_7\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_6\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_7\,
      O => \i___163_carry_i_2_n_0\
    );
\i___163_carry_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_4\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___106_carry__5_n_7\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_6\,
      O => \i___163_carry_i_3_n_0\
    );
\i___163_carry_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_4\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_7\,
      O => \i___163_carry_i_4_n_0\
    );
\i___169_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___163_carry_n_5\,
      O => \i___169_carry__0_i_1_n_0\
    );
\i___169_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(6),
      I1 => \string_cent_decenas[1]5_inferred__0/i___163_carry_n_6\,
      O => \i___169_carry__0_i_2_n_0\
    );
\i___169_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(5),
      I1 => \string_cent_decenas[1]5_inferred__0/i___163_carry_n_7\,
      O => \i___169_carry__0_i_3_n_0\
    );
\i___169_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(4),
      I1 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_5\,
      O => \i___169_carry__0_i_4_n_0\
    );
\i___169_carry_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      O => cent(2)
    );
\i___169_carry_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_reg(1),
      I1 => total_reg_26_sn_1,
      I2 => p_1_in(1),
      O => \i___169_carry_i_2_n_0\
    );
\i___169_carry_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_reg(0),
      I1 => total_reg_26_sn_1,
      I2 => p_1_in(0),
      O => \i___169_carry_i_3_n_0\
    );
\i___169_carry_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(3),
      I1 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_6\,
      O => \i___169_carry_i_4_n_0\
    );
\i___169_carry_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_7\,
      O => \i___169_carry_i_5_n_0\
    );
\i___169_carry_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(1),
      O => \i___169_carry_i_6_n_0\
    );
\i___169_carry_i_7\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(0),
      O => \i___169_carry_i_7_n_0\
    );
\i___27_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(3),
      I1 => cent(5),
      O => \i___27_carry__0_i_1_n_0\
    );
\i___27_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => cent(1),
      I1 => cent(5),
      I2 => cent(3),
      O => \i___27_carry__0_i_2_n_0\
    );
\i___27_carry__0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8E"
    )
        port map (
      I0 => cent(0),
      I1 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I2 => cent(4),
      O => \i___27_carry__0_i_3_n_0\
    );
\i___27_carry__0_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => cent(4),
      I1 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I2 => cent(0),
      O => \i___27_carry__0_i_4_n_0\
    );
\i___27_carry__0_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"17E8E817"
    )
        port map (
      I0 => cent(4),
      I1 => cent(6),
      I2 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I3 => cent(5),
      I4 => cent(3),
      O => \i___27_carry__0_i_5_n_0\
    );
\i___27_carry__0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"718E8E718E71718E"
    )
        port map (
      I0 => cent(3),
      I1 => cent(5),
      I2 => cent(1),
      I3 => cent(4),
      I4 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I5 => cent(6),
      O => \i___27_carry__0_i_6_n_0\
    );
\i___27_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \i___27_carry__0_i_3_n_0\,
      I1 => cent(3),
      I2 => cent(5),
      I3 => cent(1),
      O => \i___27_carry__0_i_7_n_0\
    );
\i___27_carry__0_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69696996"
    )
        port map (
      I0 => cent(0),
      I1 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I2 => cent(4),
      I3 => cent(1),
      I4 => cent(3),
      O => \i___27_carry__0_i_8_n_0\
    );
\i___27_carry__1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => cent(4),
      I1 => cent(6),
      O => \i___27_carry__1_i_1_n_0\
    );
\i___27_carry__1_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => cent(3),
      I1 => cent(5),
      O => \i___27_carry__1_i_2_n_0\
    );
\i___27_carry__1_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(6),
      O => \i___27_carry__1_i_3_n_0\
    );
\i___27_carry__1_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2D"
    )
        port map (
      I0 => cent(6),
      I1 => cent(4),
      I2 => cent(5),
      O => \i___27_carry__1_i_4_n_0\
    );
\i___27_carry__1_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B44B"
    )
        port map (
      I0 => cent(3),
      I1 => cent(5),
      I2 => cent(6),
      I3 => cent(4),
      O => \i___27_carry__1_i_5_n_0\
    );
\i___27_carry_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(0),
      O => \i___27_carry_i_1_n_0\
    );
\i___27_carry_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_reg(0),
      I1 => total_reg_26_sn_1,
      I2 => p_1_in(0),
      O => \i___27_carry_i_2_n_0\
    );
\i___27_carry_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => cent(0),
      I1 => cent(1),
      I2 => cent(3),
      O => \i___27_carry_i_3_n_0\
    );
\i___27_carry_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(0),
      I1 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      O => \i___27_carry_i_4_n_0\
    );
\i___27_carry_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(1),
      O => \i___27_carry_i_5_n_0\
    );
\i___27_carry_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_reg(0),
      I1 => total_reg_26_sn_1,
      I2 => p_1_in(0),
      O => \i___27_carry_i_6_n_0\
    );
\i___56_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      O => \i___56_carry__0_i_1_n_0\
    );
\i___56_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_reg(1),
      I1 => total_reg_26_sn_1,
      I2 => p_1_in(1),
      O => \i___56_carry__0_i_2_n_0\
    );
\i___56_carry__0_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(4),
      O => \i___56_carry__0_i_3_n_0\
    );
\i___56_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(3),
      I1 => cent(6),
      O => \i___56_carry__0_i_4_n_0\
    );
\i___56_carry__0_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I1 => cent(5),
      O => \i___56_carry__0_i_5_n_0\
    );
\i___56_carry__0_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(1),
      I1 => cent(4),
      O => \i___56_carry__0_i_6_n_0\
    );
\i___56_carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(6),
      O => \i___56_carry__1_i_1_n_0\
    );
\i___56_carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(5),
      O => \i___56_carry__1_i_2_n_0\
    );
\i___56_carry_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_reg(0),
      I1 => total_reg_26_sn_1,
      I2 => p_1_in(0),
      O => \i___56_carry_i_1_n_0\
    );
\i___56_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(0),
      I1 => cent(3),
      O => \i___56_carry_i_2_n_0\
    );
\i___56_carry_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4774"
    )
        port map (
      I0 => total_reg(2),
      I1 => total_reg_26_sn_1,
      I2 => \euros1__337_carry_n_5\,
      I3 => \string_cent_decenas[1]5__185_carry_i_9_n_0\,
      O => \i___56_carry_i_3_n_0\
    );
\i___56_carry_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(1),
      O => \i___56_carry_i_4_n_0\
    );
\i___56_carry_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_reg(0),
      I1 => total_reg_26_sn_1,
      I2 => p_1_in(0),
      O => \i___56_carry_i_5_n_0\
    );
\i___80_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => cent(3),
      I1 => cent(5),
      O => \i___80_carry__0_i_1_n_0\
    );
\i___80_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(5),
      I1 => cent(3),
      O => \i___80_carry__0_i_2_n_0\
    );
\i___80_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I1 => cent(4),
      O => \i___80_carry__0_i_3_n_0\
    );
\i___80_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(3),
      I1 => cent(1),
      O => \i___80_carry__0_i_4_n_0\
    );
\i___80_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => cent(5),
      I1 => cent(3),
      I2 => cent(4),
      I3 => cent(6),
      O => \i___80_carry__0_i_5_n_0\
    );
\i___80_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4BB4"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I1 => cent(4),
      I2 => cent(3),
      I3 => cent(5),
      O => \i___80_carry__0_i_6_n_0\
    );
\i___80_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7887"
    )
        port map (
      I0 => cent(1),
      I1 => cent(3),
      I2 => cent(4),
      I3 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      O => \i___80_carry__0_i_7_n_0\
    );
\i___80_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6966"
    )
        port map (
      I0 => cent(1),
      I1 => cent(3),
      I2 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I3 => cent(0),
      O => \i___80_carry__0_i_8_n_0\
    );
\i___80_carry__1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => cent(4),
      I1 => cent(6),
      O => \i___80_carry__1_i_1_n_0\
    );
\i___80_carry__1_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBB8B8B8888B888"
    )
        port map (
      I0 => total_reg(6),
      I1 => total_reg_26_sn_1,
      I2 => \euros1__337_carry__0_n_6\,
      I3 => \euros1__337_carry__0_n_4\,
      I4 => \string_cent_decenas[1]5_carry__1_i_8_n_0\,
      I5 => \euros1__337_carry__0_n_5\,
      O => \i___80_carry__1_i_2_n_0\
    );
\i___80_carry__1_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => cent(6),
      I1 => cent(4),
      I2 => cent(5),
      O => \i___80_carry__1_i_3_n_0\
    );
\i___80_carry_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => cent(0),
      I1 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I2 => cent(6),
      O => \i___80_carry_i_1_n_0\
    );
\i___80_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(5),
      I1 => cent(1),
      O => \i___80_carry_i_2_n_0\
    );
\i___80_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(4),
      I1 => cent(0),
      O => \i___80_carry_i_3_n_0\
    );
\i___80_carry_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8B8B88B"
    )
        port map (
      I0 => total_reg(3),
      I1 => total_reg_26_sn_1,
      I2 => \euros1__337_carry_n_4\,
      I3 => \string_cent_decenas[1]5__185_carry_i_9_n_0\,
      I4 => \euros1__337_carry_n_5\,
      O => \i___80_carry_i_4_n_0\
    );
\string_cent_decenas[1]5__123_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5__123_carry_n_0\,
      CO(2) => \string_cent_decenas[1]5__123_carry_n_1\,
      CO(1) => \string_cent_decenas[1]5__123_carry_n_2\,
      CO(0) => \string_cent_decenas[1]5__123_carry_n_3\,
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__123_carry_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__123_carry_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__123_carry_i_3_n_0\,
      DI(0) => \string_cent_decenas[1]5__123_carry_i_4_n_0\,
      O(3 downto 0) => \NLW_string_cent_decenas[1]5__123_carry_O_UNCONNECTED\(3 downto 0),
      S(3) => \string_cent_decenas[1]5__123_carry_i_5_n_0\,
      S(2) => \string_cent_decenas[1]5__123_carry_i_6_n_0\,
      S(1) => \string_cent_decenas[1]5__123_carry_i_7_n_0\,
      S(0) => \string_cent_decenas[1]5__123_carry_i_8_n_0\
    );
\string_cent_decenas[1]5__123_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__123_carry_n_0\,
      CO(3) => \string_cent_decenas[1]5__123_carry__0_n_0\,
      CO(2) => \string_cent_decenas[1]5__123_carry__0_n_1\,
      CO(1) => \string_cent_decenas[1]5__123_carry__0_n_2\,
      CO(0) => \string_cent_decenas[1]5__123_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__123_carry__0_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__123_carry__0_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__123_carry__0_i_3_n_0\,
      DI(0) => \string_cent_decenas[1]5__123_carry__0_i_4_n_0\,
      O(3 downto 0) => \NLW_string_cent_decenas[1]5__123_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \string_cent_decenas[1]5__123_carry__0_i_5_n_0\,
      S(2) => \string_cent_decenas[1]5__123_carry__0_i_6_n_0\,
      S(1) => \string_cent_decenas[1]5__123_carry__0_i_7_n_0\,
      S(0) => \string_cent_decenas[1]5__123_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5__123_carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"82"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry__0_n_7\,
      I1 => \string_cent_decenas[1]5_carry_n_7\,
      I2 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__123_carry__0_i_1_n_0\
    );
\string_cent_decenas[1]5__123_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry_n_4\,
      I1 => \string_cent_decenas[1]5_carry__1_n_4\,
      O => \string_cent_decenas[1]5__123_carry__0_i_2_n_0\
    );
\string_cent_decenas[1]5__123_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry_n_5\,
      I1 => \string_cent_decenas[1]5_carry__1_n_5\,
      O => \string_cent_decenas[1]5__123_carry__0_i_3_n_0\
    );
\string_cent_decenas[1]5__123_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry_n_6\,
      I1 => \string_cent_decenas[1]5_carry__1_n_6\,
      O => \string_cent_decenas[1]5__123_carry__0_i_4_n_0\
    );
\string_cent_decenas[1]5__123_carry__0_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99966669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry__0_n_6\,
      I1 => \string_cent_decenas[1]5__54_carry_n_6\,
      I2 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      I3 => \string_cent_decenas[1]5_carry_n_7\,
      I4 => \string_cent_decenas[1]5__123_carry__0_i_1_n_0\,
      O => \string_cent_decenas[1]5__123_carry__0_i_5_n_0\
    );
\string_cent_decenas[1]5__123_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry__0_n_7\,
      I1 => \string_cent_decenas[1]5_carry_n_7\,
      I2 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      I3 => \string_cent_decenas[1]5__123_carry__0_i_2_n_0\,
      O => \string_cent_decenas[1]5__123_carry__0_i_6_n_0\
    );
\string_cent_decenas[1]5__123_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry_n_4\,
      I1 => \string_cent_decenas[1]5_carry__1_n_4\,
      I2 => \string_cent_decenas[1]5_carry__1_n_5\,
      I3 => \string_cent_decenas[1]5__27_carry_n_5\,
      O => \string_cent_decenas[1]5__123_carry__0_i_7_n_0\
    );
\string_cent_decenas[1]5__123_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__1_n_6\,
      I1 => \string_cent_decenas[1]5__27_carry_n_6\,
      I2 => \string_cent_decenas[1]5_carry__1_n_5\,
      I3 => \string_cent_decenas[1]5__27_carry_n_5\,
      O => \string_cent_decenas[1]5__123_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5__123_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__123_carry__0_n_0\,
      CO(3) => \string_cent_decenas[1]5__123_carry__1_n_0\,
      CO(2) => \string_cent_decenas[1]5__123_carry__1_n_1\,
      CO(1) => \string_cent_decenas[1]5__123_carry__1_n_2\,
      CO(0) => \string_cent_decenas[1]5__123_carry__1_n_3\,
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__123_carry__1_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__123_carry__1_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__123_carry__1_i_3_n_0\,
      DI(0) => \string_cent_decenas[1]5__123_carry__1_i_4_n_0\,
      O(3 downto 0) => \NLW_string_cent_decenas[1]5__123_carry__1_O_UNCONNECTED\(3 downto 0),
      S(3) => \string_cent_decenas[1]5__123_carry__1_i_5_n_0\,
      S(2) => \string_cent_decenas[1]5__123_carry__1_i_6_n_0\,
      S(1) => \string_cent_decenas[1]5__123_carry__1_i_7_n_0\,
      S(0) => \string_cent_decenas[1]5__123_carry__1_i_8_n_0\
    );
\string_cent_decenas[1]5__123_carry__1_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8B82"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry__1_n_7\,
      I1 => \string_cent_decenas[1]5__54_carry__0_n_7\,
      I2 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      I3 => \string_cent_decenas[1]5__54_carry_n_4\,
      O => \string_cent_decenas[1]5__123_carry__1_i_1_n_0\
    );
\string_cent_decenas[1]5__123_carry__1_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8B82"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry__0_n_4\,
      I1 => \string_cent_decenas[1]5__54_carry_n_4\,
      I2 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      I3 => \string_cent_decenas[1]5__54_carry_n_5\,
      O => \string_cent_decenas[1]5__123_carry__1_i_2_n_0\
    );
\string_cent_decenas[1]5__123_carry__1_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8B82"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5__54_carry_n_5\,
      I2 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      I3 => \string_cent_decenas[1]5__54_carry_n_6\,
      O => \string_cent_decenas[1]5__123_carry__1_i_3_n_0\
    );
\string_cent_decenas[1]5__123_carry__1_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8B82"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry__0_n_6\,
      I1 => \string_cent_decenas[1]5__54_carry_n_6\,
      I2 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      I3 => \string_cent_decenas[1]5_carry_n_7\,
      O => \string_cent_decenas[1]5__123_carry__1_i_4_n_0\
    );
\string_cent_decenas[1]5__123_carry__1_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3CC3F00F87781EE1"
    )
        port map (
      I0 => \string_cent_decenas[1]5__54_carry_n_4\,
      I1 => \string_cent_decenas[1]5__27_carry__1_n_7\,
      I2 => \string_cent_decenas[1]5__27_carry__1_n_6\,
      I3 => \string_cent_decenas[1]5__123_carry__1_i_9_n_0\,
      I4 => \string_cent_decenas[1]5__54_carry__0_n_7\,
      I5 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__123_carry__1_i_5_n_0\
    );
\string_cent_decenas[1]5__123_carry__1_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96969669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__123_carry__1_i_2_n_0\,
      I1 => \string_cent_decenas[1]5__27_carry__1_n_7\,
      I2 => \string_cent_decenas[1]5__54_carry__0_n_7\,
      I3 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      I4 => \string_cent_decenas[1]5__54_carry_n_4\,
      O => \string_cent_decenas[1]5__123_carry__1_i_6_n_0\
    );
\string_cent_decenas[1]5__123_carry__1_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99966669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry__0_n_4\,
      I1 => \string_cent_decenas[1]5__54_carry_n_4\,
      I2 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      I3 => \string_cent_decenas[1]5__54_carry_n_5\,
      I4 => \string_cent_decenas[1]5__123_carry__1_i_3_n_0\,
      O => \string_cent_decenas[1]5__123_carry__1_i_7_n_0\
    );
\string_cent_decenas[1]5__123_carry__1_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99966669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5__54_carry_n_5\,
      I2 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      I3 => \string_cent_decenas[1]5__54_carry_n_6\,
      I4 => \string_cent_decenas[1]5__123_carry__1_i_4_n_0\,
      O => \string_cent_decenas[1]5__123_carry__1_i_8_n_0\
    );
\string_cent_decenas[1]5__123_carry__1_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \string_cent_decenas[1]5__54_carry__0_n_6\,
      I1 => cent(0),
      I2 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__123_carry__1_i_9_n_0\
    );
\string_cent_decenas[1]5__123_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__123_carry__1_n_0\,
      CO(3) => \string_cent_decenas[1]5__123_carry__2_n_0\,
      CO(2) => \string_cent_decenas[1]5__123_carry__2_n_1\,
      CO(1) => \string_cent_decenas[1]5__123_carry__2_n_2\,
      CO(0) => \string_cent_decenas[1]5__123_carry__2_n_3\,
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__123_carry__2_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__123_carry__2_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__123_carry__2_i_3_n_0\,
      DI(0) => \string_cent_decenas[1]5__123_carry__2_i_4_n_0\,
      O(3 downto 0) => \NLW_string_cent_decenas[1]5__123_carry__2_O_UNCONNECTED\(3 downto 0),
      S(3) => \string_cent_decenas[1]5__123_carry__2_i_5_n_0\,
      S(2) => \string_cent_decenas[1]5__123_carry__2_i_6_n_0\,
      S(1) => \string_cent_decenas[1]5__123_carry__2_i_7_n_0\,
      S(0) => \string_cent_decenas[1]5__123_carry__2_i_8_n_0\
    );
\string_cent_decenas[1]5__123_carry__2_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3140403173FDFD73"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I1 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      I2 => \string_cent_decenas[1]5__54_carry__0_n_4\,
      I3 => \string_cent_decenas[1]5__54_carry__1_n_7\,
      I4 => \string_cent_decenas[1]5__123_carry__2_i_9_n_0\,
      I5 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      O => \string_cent_decenas[1]5__123_carry__2_i_1_n_0\
    );
\string_cent_decenas[1]5__123_carry__2_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B22B"
    )
        port map (
      I0 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      I1 => \string_cent_decenas[1]5__54_carry__1_n_7\,
      I2 => cent(3),
      I3 => cent(0),
      O => \string_cent_decenas[1]5__123_carry__2_i_10_n_0\
    );
\string_cent_decenas[1]5__123_carry__2_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \string_cent_decenas[1]5__54_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      I2 => cent(1),
      O => \string_cent_decenas[1]5__123_carry__2_i_11_n_0\
    );
\string_cent_decenas[1]5__123_carry__2_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => cent(0),
      I1 => cent(3),
      I2 => \string_cent_decenas[1]5__54_carry__1_n_7\,
      I3 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__123_carry__2_i_12_n_0\
    );
\string_cent_decenas[1]5__123_carry__2_i_13\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \string_cent_decenas[1]5__54_carry__0_n_4\,
      I1 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I2 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__123_carry__2_i_13_n_0\
    );
\string_cent_decenas[1]5__123_carry__2_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \string_cent_decenas[1]5__54_carry__0_n_5\,
      I1 => cent(1),
      I2 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__123_carry__2_i_14_n_0\
    );
\string_cent_decenas[1]5__123_carry__2_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"80FE32B332B380FE"
    )
        port map (
      I0 => \string_cent_decenas[1]5__54_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      I2 => cent(1),
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I5 => \string_cent_decenas[1]5__54_carry__0_n_4\,
      O => \string_cent_decenas[1]5__123_carry__2_i_2_n_0\
    );
\string_cent_decenas[1]5__123_carry__2_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"32B380FE80FE32B3"
    )
        port map (
      I0 => \string_cent_decenas[1]5__54_carry__0_n_6\,
      I1 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      I2 => cent(0),
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => cent(1),
      I5 => \string_cent_decenas[1]5__54_carry__0_n_5\,
      O => \string_cent_decenas[1]5__123_carry__2_i_3_n_0\
    );
\string_cent_decenas[1]5__123_carry__2_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7D416900"
    )
        port map (
      I0 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      I1 => cent(0),
      I2 => \string_cent_decenas[1]5__54_carry__0_n_6\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_6\,
      I4 => \string_cent_decenas[1]5__54_carry__0_n_7\,
      O => \string_cent_decenas[1]5__123_carry__2_i_4_n_0\
    );
\string_cent_decenas[1]5__123_carry__2_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__123_carry__2_i_1_n_0\,
      I1 => \string_cent_decenas[1]5__123_carry__2_i_10_n_0\,
      I2 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I3 => \string_cent_decenas[1]5__78_carry_n_6\,
      I4 => \string_cent_decenas[1]5__54_carry__1_n_6\,
      I5 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__123_carry__2_i_5_n_0\
    );
\string_cent_decenas[1]5__123_carry__2_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"42D4D4BDBD2B2B42"
    )
        port map (
      I0 => \string_cent_decenas[1]5__123_carry__2_i_11_n_0\,
      I1 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I2 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      I3 => \string_cent_decenas[1]5__54_carry__0_n_4\,
      I4 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I5 => \string_cent_decenas[1]5__123_carry__2_i_12_n_0\,
      O => \string_cent_decenas[1]5__123_carry__2_i_6_n_0\
    );
\string_cent_decenas[1]5__123_carry__2_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9A5965A665A69A59"
    )
        port map (
      I0 => \string_cent_decenas[1]5__123_carry__2_i_3_n_0\,
      I1 => \string_cent_decenas[1]5__54_carry__0_n_5\,
      I2 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      I3 => cent(1),
      I4 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I5 => \string_cent_decenas[1]5__123_carry__2_i_13_n_0\,
      O => \string_cent_decenas[1]5__123_carry__2_i_7_n_0\
    );
\string_cent_decenas[1]5__123_carry__2_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"65A69A599A5965A6"
    )
        port map (
      I0 => \string_cent_decenas[1]5__123_carry__2_i_4_n_0\,
      I1 => \string_cent_decenas[1]5__54_carry__0_n_6\,
      I2 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      I3 => cent(0),
      I4 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I5 => \string_cent_decenas[1]5__123_carry__2_i_14_n_0\,
      O => \string_cent_decenas[1]5__123_carry__2_i_8_n_0\
    );
\string_cent_decenas[1]5__123_carry__2_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(3),
      I1 => cent(0),
      O => \string_cent_decenas[1]5__123_carry__2_i_9_n_0\
    );
\string_cent_decenas[1]5__123_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__123_carry__2_n_0\,
      CO(3) => \string_cent_decenas[1]5__123_carry__3_n_0\,
      CO(2) => \string_cent_decenas[1]5__123_carry__3_n_1\,
      CO(1) => \string_cent_decenas[1]5__123_carry__3_n_2\,
      CO(0) => \string_cent_decenas[1]5__123_carry__3_n_3\,
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__123_carry__3_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__123_carry__3_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__123_carry__3_i_3_n_0\,
      DI(0) => \string_cent_decenas[1]5__123_carry__3_i_4_n_0\,
      O(3 downto 0) => \NLW_string_cent_decenas[1]5__123_carry__3_O_UNCONNECTED\(3 downto 0),
      S(3) => \string_cent_decenas[1]5__123_carry__3_i_5_n_0\,
      S(2) => \string_cent_decenas[1]5__123_carry__3_i_6_n_0\,
      S(1) => \string_cent_decenas[1]5__123_carry__3_i_7_n_0\,
      S(0) => \string_cent_decenas[1]5__123_carry__3_i_8_n_0\
    );
\string_cent_decenas[1]5__123_carry__3_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"D4FF00D4"
    )
        port map (
      I0 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      I1 => \string_cent_decenas[1]5__54_carry__1_n_4\,
      I2 => \string_cent_decenas[1]5__78_carry_n_4\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__123_carry__3_i_9_n_0\,
      O => \string_cent_decenas[1]5__123_carry__3_i_1_n_0\
    );
\string_cent_decenas[1]5__123_carry__3_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      I1 => \string_cent_decenas[1]5__54_carry__1_n_4\,
      I2 => \string_cent_decenas[1]5__78_carry_n_4\,
      O => \string_cent_decenas[1]5__123_carry__3_i_10_n_0\
    );
\string_cent_decenas[1]5__123_carry__3_i_11\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__54_carry__1_n_0\,
      CO(3 downto 1) => \NLW_string_cent_decenas[1]5__123_carry__3_i_11_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \string_cent_decenas[1]5__123_carry__3_i_11_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_string_cent_decenas[1]5__123_carry__3_i_11_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
\string_cent_decenas[1]5__123_carry__3_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \string_cent_decenas[1]5__78_carry__0_n_6\,
      I1 => \string_cent_decenas[1]5__123_carry__3_i_11_n_3\,
      I2 => \string_cent_decenas[1]5__99_carry_n_6\,
      O => \string_cent_decenas[1]5__123_carry__3_i_12_n_0\
    );
\string_cent_decenas[1]5__123_carry__3_i_13\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      I1 => \string_cent_decenas[1]5__54_carry__1_n_5\,
      I2 => \string_cent_decenas[1]5__78_carry_n_5\,
      O => \string_cent_decenas[1]5__123_carry__3_i_13_n_0\
    );
\string_cent_decenas[1]5__123_carry__3_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \string_cent_decenas[1]5__78_carry_n_5\,
      I1 => \string_cent_decenas[1]5__54_carry__1_n_5\,
      I2 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__123_carry__3_i_14_n_0\
    );
\string_cent_decenas[1]5__123_carry__3_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"54D580FE80FE54D5"
    )
        port map (
      I0 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      I1 => \string_cent_decenas[1]5__54_carry__1_n_5\,
      I2 => \string_cent_decenas[1]5__78_carry_n_5\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__54_carry__1_n_4\,
      I5 => \string_cent_decenas[1]5__78_carry_n_4\,
      O => \string_cent_decenas[1]5__123_carry__3_i_2_n_0\
    );
\string_cent_decenas[1]5__123_carry__3_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"54D580FE80FE54D5"
    )
        port map (
      I0 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      I1 => \string_cent_decenas[1]5__54_carry__1_n_6\,
      I2 => \string_cent_decenas[1]5__78_carry_n_6\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__54_carry__1_n_5\,
      I5 => \string_cent_decenas[1]5__78_carry_n_5\,
      O => \string_cent_decenas[1]5__123_carry__3_i_3_n_0\
    );
\string_cent_decenas[1]5__123_carry__3_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"54D580FE80FE54D5"
    )
        port map (
      I0 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      I1 => \string_cent_decenas[1]5__54_carry__1_n_7\,
      I2 => \string_cent_decenas[1]5__123_carry__2_i_9_n_0\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__54_carry__1_n_6\,
      I5 => \string_cent_decenas[1]5__78_carry_n_6\,
      O => \string_cent_decenas[1]5__123_carry__3_i_4_n_0\
    );
\string_cent_decenas[1]5__123_carry__3_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"188E8EE7E7717118"
    )
        port map (
      I0 => \string_cent_decenas[1]5__123_carry__3_i_10_n_0\,
      I1 => \string_cent_decenas[1]5__123_carry__3_i_11_n_3\,
      I2 => \string_cent_decenas[1]5__99_carry_n_7\,
      I3 => \string_cent_decenas[1]5__78_carry__0_n_7\,
      I4 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I5 => \string_cent_decenas[1]5__123_carry__3_i_12_n_0\,
      O => \string_cent_decenas[1]5__123_carry__3_i_5_n_0\
    );
\string_cent_decenas[1]5__123_carry__3_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__123_carry__3_i_2_n_0\,
      I1 => \string_cent_decenas[1]5__123_carry__3_i_10_n_0\,
      I2 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I3 => \string_cent_decenas[1]5__78_carry__0_n_7\,
      I4 => \string_cent_decenas[1]5__123_carry__3_i_11_n_3\,
      I5 => \string_cent_decenas[1]5__99_carry_n_7\,
      O => \string_cent_decenas[1]5__123_carry__3_i_6_n_0\
    );
\string_cent_decenas[1]5__123_carry__3_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__123_carry__3_i_3_n_0\,
      I1 => \string_cent_decenas[1]5__123_carry__3_i_13_n_0\,
      I2 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I3 => \string_cent_decenas[1]5__78_carry_n_4\,
      I4 => \string_cent_decenas[1]5__54_carry__1_n_4\,
      I5 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__123_carry__3_i_7_n_0\
    );
\string_cent_decenas[1]5__123_carry__3_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"188E8EE7E7717118"
    )
        port map (
      I0 => \string_cent_decenas[1]5__123_carry__2_i_10_n_0\,
      I1 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      I2 => \string_cent_decenas[1]5__54_carry__1_n_6\,
      I3 => \string_cent_decenas[1]5__78_carry_n_6\,
      I4 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I5 => \string_cent_decenas[1]5__123_carry__3_i_14_n_0\,
      O => \string_cent_decenas[1]5__123_carry__3_i_8_n_0\
    );
\string_cent_decenas[1]5__123_carry__3_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \string_cent_decenas[1]5__78_carry__0_n_7\,
      I1 => \string_cent_decenas[1]5__123_carry__3_i_11_n_3\,
      I2 => \string_cent_decenas[1]5__99_carry_n_7\,
      O => \string_cent_decenas[1]5__123_carry__3_i_9_n_0\
    );
\string_cent_decenas[1]5__123_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__123_carry__3_n_0\,
      CO(3) => \string_cent_decenas[1]5__123_carry__4_n_0\,
      CO(2) => \string_cent_decenas[1]5__123_carry__4_n_1\,
      CO(1) => \string_cent_decenas[1]5__123_carry__4_n_2\,
      CO(0) => \string_cent_decenas[1]5__123_carry__4_n_3\,
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__123_carry__4_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__123_carry__4_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__123_carry__4_i_3_n_0\,
      DI(0) => \string_cent_decenas[1]5__123_carry__4_i_4_n_0\,
      O(3) => \string_cent_decenas[1]5__123_carry__4_n_4\,
      O(2 downto 0) => \NLW_string_cent_decenas[1]5__123_carry__4_O_UNCONNECTED\(2 downto 0),
      S(3) => \string_cent_decenas[1]5__123_carry__4_i_5_n_0\,
      S(2) => \string_cent_decenas[1]5__123_carry__4_i_6_n_0\,
      S(1) => \string_cent_decenas[1]5__123_carry__4_i_7_n_0\,
      S(0) => \string_cent_decenas[1]5__123_carry__4_i_8_n_0\
    );
\string_cent_decenas[1]5__123_carry__4_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"54D580FE80FE54D5"
    )
        port map (
      I0 => \string_cent_decenas[1]5__123_carry__3_i_11_n_3\,
      I1 => \string_cent_decenas[1]5__99_carry_n_4\,
      I2 => \string_cent_decenas[1]5__78_carry__0_n_4\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__99_carry__0_n_7\,
      I5 => \string_cent_decenas[1]5__78_carry__1_n_7\,
      O => \string_cent_decenas[1]5__123_carry__4_i_1_n_0\
    );
\string_cent_decenas[1]5__123_carry__4_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \string_cent_decenas[1]5__123_carry__3_i_11_n_3\,
      I1 => \string_cent_decenas[1]5__99_carry_n_4\,
      I2 => \string_cent_decenas[1]5__78_carry__0_n_4\,
      O => \string_cent_decenas[1]5__123_carry__4_i_10_n_0\
    );
\string_cent_decenas[1]5__123_carry__4_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \string_cent_decenas[1]5__123_carry__3_i_11_n_3\,
      I1 => \string_cent_decenas[1]5__99_carry_n_5\,
      I2 => \string_cent_decenas[1]5__78_carry__0_n_5\,
      O => \string_cent_decenas[1]5__123_carry__4_i_11_n_0\
    );
\string_cent_decenas[1]5__123_carry__4_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \string_cent_decenas[1]5__123_carry__3_i_11_n_3\,
      I1 => \string_cent_decenas[1]5__99_carry_n_6\,
      I2 => \string_cent_decenas[1]5__78_carry__0_n_6\,
      O => \string_cent_decenas[1]5__123_carry__4_i_12_n_0\
    );
\string_cent_decenas[1]5__123_carry__4_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"54D580FE80FE54D5"
    )
        port map (
      I0 => \string_cent_decenas[1]5__123_carry__3_i_11_n_3\,
      I1 => \string_cent_decenas[1]5__99_carry_n_5\,
      I2 => \string_cent_decenas[1]5__78_carry__0_n_5\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__99_carry_n_4\,
      I5 => \string_cent_decenas[1]5__78_carry__0_n_4\,
      O => \string_cent_decenas[1]5__123_carry__4_i_2_n_0\
    );
\string_cent_decenas[1]5__123_carry__4_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"54D580FE80FE54D5"
    )
        port map (
      I0 => \string_cent_decenas[1]5__123_carry__3_i_11_n_3\,
      I1 => \string_cent_decenas[1]5__99_carry_n_6\,
      I2 => \string_cent_decenas[1]5__78_carry__0_n_6\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__99_carry_n_5\,
      I5 => \string_cent_decenas[1]5__78_carry__0_n_5\,
      O => \string_cent_decenas[1]5__123_carry__4_i_3_n_0\
    );
\string_cent_decenas[1]5__123_carry__4_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"54D580FE80FE54D5"
    )
        port map (
      I0 => \string_cent_decenas[1]5__123_carry__3_i_11_n_3\,
      I1 => \string_cent_decenas[1]5__99_carry_n_7\,
      I2 => \string_cent_decenas[1]5__78_carry__0_n_7\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__99_carry_n_6\,
      I5 => \string_cent_decenas[1]5__78_carry__0_n_6\,
      O => \string_cent_decenas[1]5__123_carry__4_i_4_n_0\
    );
\string_cent_decenas[1]5__123_carry__4_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__123_carry__4_i_1_n_0\,
      I1 => \string_cent_decenas[1]5__123_carry__4_i_9_n_0\,
      I2 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I3 => \string_cent_decenas[1]5__78_carry__1_n_6\,
      I4 => \string_cent_decenas[1]5__123_carry__3_i_11_n_3\,
      I5 => \string_cent_decenas[1]5__99_carry__0_n_6\,
      O => \string_cent_decenas[1]5__123_carry__4_i_5_n_0\
    );
\string_cent_decenas[1]5__123_carry__4_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__123_carry__4_i_2_n_0\,
      I1 => \string_cent_decenas[1]5__123_carry__4_i_10_n_0\,
      I2 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I3 => \string_cent_decenas[1]5__78_carry__1_n_7\,
      I4 => \string_cent_decenas[1]5__123_carry__3_i_11_n_3\,
      I5 => \string_cent_decenas[1]5__99_carry__0_n_7\,
      O => \string_cent_decenas[1]5__123_carry__4_i_6_n_0\
    );
\string_cent_decenas[1]5__123_carry__4_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__123_carry__4_i_3_n_0\,
      I1 => \string_cent_decenas[1]5__123_carry__4_i_11_n_0\,
      I2 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I3 => \string_cent_decenas[1]5__78_carry__0_n_4\,
      I4 => \string_cent_decenas[1]5__123_carry__3_i_11_n_3\,
      I5 => \string_cent_decenas[1]5__99_carry_n_4\,
      O => \string_cent_decenas[1]5__123_carry__4_i_7_n_0\
    );
\string_cent_decenas[1]5__123_carry__4_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__123_carry__4_i_4_n_0\,
      I1 => \string_cent_decenas[1]5__123_carry__4_i_12_n_0\,
      I2 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I3 => \string_cent_decenas[1]5__78_carry__0_n_5\,
      I4 => \string_cent_decenas[1]5__123_carry__3_i_11_n_3\,
      I5 => \string_cent_decenas[1]5__99_carry_n_5\,
      O => \string_cent_decenas[1]5__123_carry__4_i_8_n_0\
    );
\string_cent_decenas[1]5__123_carry__4_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \string_cent_decenas[1]5__123_carry__3_i_11_n_3\,
      I1 => \string_cent_decenas[1]5__99_carry__0_n_7\,
      I2 => \string_cent_decenas[1]5__78_carry__1_n_7\,
      O => \string_cent_decenas[1]5__123_carry__4_i_9_n_0\
    );
\string_cent_decenas[1]5__123_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__123_carry__4_n_0\,
      CO(3 downto 2) => \NLW_string_cent_decenas[1]5__123_carry__5_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \string_cent_decenas[1]5__123_carry__5_n_2\,
      CO(0) => \string_cent_decenas[1]5__123_carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \string_cent_decenas[1]5__123_carry__5_i_1_n_0\,
      DI(0) => \string_cent_decenas[1]5__123_carry__5_i_2_n_0\,
      O(3) => \NLW_string_cent_decenas[1]5__123_carry__5_O_UNCONNECTED\(3),
      O(2) => \string_cent_decenas[1]5__123_carry__5_n_5\,
      O(1) => \string_cent_decenas[1]5__123_carry__5_n_6\,
      O(0) => \string_cent_decenas[1]5__123_carry__5_n_7\,
      S(3) => '0',
      S(2) => \string_cent_decenas[1]5__123_carry__5_i_3_n_0\,
      S(1) => \string_cent_decenas[1]5__123_carry__5_i_4_n_0\,
      S(0) => \string_cent_decenas[1]5__123_carry__5_i_5_n_0\
    );
\string_cent_decenas[1]5__123_carry__5_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"80FE54D554D580FE"
    )
        port map (
      I0 => \string_cent_decenas[1]5__123_carry__3_i_11_n_3\,
      I1 => \string_cent_decenas[1]5__99_carry__0_n_6\,
      I2 => \string_cent_decenas[1]5__78_carry__1_n_6\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__99_carry__0_n_5\,
      I5 => \string_cent_decenas[1]5__78_carry__1_n_1\,
      O => \string_cent_decenas[1]5__123_carry__5_i_1_n_0\
    );
\string_cent_decenas[1]5__123_carry__5_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"54D580FE80FE54D5"
    )
        port map (
      I0 => \string_cent_decenas[1]5__123_carry__3_i_11_n_3\,
      I1 => \string_cent_decenas[1]5__99_carry__0_n_7\,
      I2 => \string_cent_decenas[1]5__78_carry__1_n_7\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__99_carry__0_n_6\,
      I5 => \string_cent_decenas[1]5__78_carry__1_n_6\,
      O => \string_cent_decenas[1]5__123_carry__5_i_2_n_0\
    );
\string_cent_decenas[1]5__123_carry__5_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEF80107E0801F7F"
    )
        port map (
      I0 => \string_cent_decenas[1]5__99_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5__123_carry__3_i_11_n_3\,
      I2 => \string_cent_decenas[1]5__99_carry__0_n_4\,
      I3 => \string_cent_decenas[1]5__78_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__99_carry__1_n_7\,
      I5 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      O => \string_cent_decenas[1]5__123_carry__5_i_3_n_0\
    );
\string_cent_decenas[1]5__123_carry__5_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \string_cent_decenas[1]5__123_carry__5_i_1_n_0\,
      I1 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5__99_carry__0_n_4\,
      I3 => \string_cent_decenas[1]5__123_carry__3_i_11_n_3\,
      I4 => \string_cent_decenas[1]5__78_carry__1_n_1\,
      I5 => \string_cent_decenas[1]5__99_carry__0_n_5\,
      O => \string_cent_decenas[1]5__123_carry__5_i_4_n_0\
    );
\string_cent_decenas[1]5__123_carry__5_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6996966996696996"
    )
        port map (
      I0 => \string_cent_decenas[1]5__123_carry__5_i_2_n_0\,
      I1 => \string_cent_decenas[1]5__123_carry__5_i_6_n_0\,
      I2 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I3 => \string_cent_decenas[1]5__78_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__123_carry__3_i_11_n_3\,
      I5 => \string_cent_decenas[1]5__99_carry__0_n_5\,
      O => \string_cent_decenas[1]5__123_carry__5_i_5_n_0\
    );
\string_cent_decenas[1]5__123_carry__5_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \string_cent_decenas[1]5__123_carry__3_i_11_n_3\,
      I1 => \string_cent_decenas[1]5__99_carry__0_n_6\,
      I2 => \string_cent_decenas[1]5__78_carry__1_n_6\,
      O => \string_cent_decenas[1]5__123_carry__5_i_6_n_0\
    );
\string_cent_decenas[1]5__123_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry_n_7\,
      I1 => \string_cent_decenas[1]5_carry__1_n_7\,
      O => \string_cent_decenas[1]5__123_carry_i_1_n_0\
    );
\string_cent_decenas[1]5__123_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__0_n_4\,
      I1 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      O => \string_cent_decenas[1]5__123_carry_i_2_n_0\
    );
\string_cent_decenas[1]5__123_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__0_n_5\,
      I1 => cent(1),
      O => \string_cent_decenas[1]5__123_carry_i_3_n_0\
    );
\string_cent_decenas[1]5__123_carry_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__0_n_6\,
      I1 => cent(0),
      O => \string_cent_decenas[1]5__123_carry_i_4_n_0\
    );
\string_cent_decenas[1]5__123_carry_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__1_n_7\,
      I1 => \string_cent_decenas[1]5__27_carry_n_7\,
      I2 => \string_cent_decenas[1]5_carry__1_n_6\,
      I3 => \string_cent_decenas[1]5__27_carry_n_6\,
      O => \string_cent_decenas[1]5__123_carry_i_5_n_0\
    );
\string_cent_decenas[1]5__123_carry_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4BB4"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I1 => \string_cent_decenas[1]5_carry__0_n_4\,
      I2 => \string_cent_decenas[1]5_carry__1_n_7\,
      I3 => \string_cent_decenas[1]5__27_carry_n_7\,
      O => \string_cent_decenas[1]5__123_carry_i_6_n_0\
    );
\string_cent_decenas[1]5__123_carry_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7887"
    )
        port map (
      I0 => cent(1),
      I1 => \string_cent_decenas[1]5_carry__0_n_5\,
      I2 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I3 => \string_cent_decenas[1]5_carry__0_n_4\,
      O => \string_cent_decenas[1]5__123_carry_i_7_n_0\
    );
\string_cent_decenas[1]5__123_carry_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => cent(0),
      I1 => \string_cent_decenas[1]5_carry__0_n_6\,
      I2 => cent(1),
      I3 => \string_cent_decenas[1]5_carry__0_n_5\,
      O => \string_cent_decenas[1]5__123_carry_i_8_n_0\
    );
\string_cent_decenas[1]5__179_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3 downto 2) => \NLW_string_cent_decenas[1]5__179_carry_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \string_cent_decenas[1]5__179_carry_n_2\,
      CO(0) => \string_cent_decenas[1]5__179_carry_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \string_cent_decenas[1]5__123_carry__5_n_6\,
      DI(0) => '0',
      O(3) => \NLW_string_cent_decenas[1]5__179_carry_O_UNCONNECTED\(3),
      O(2) => \string_cent_decenas[1]5__179_carry_n_5\,
      O(1) => \string_cent_decenas[1]5__179_carry_n_6\,
      O(0) => \string_cent_decenas[1]5__179_carry_n_7\,
      S(3) => '0',
      S(2) => \string_cent_decenas[1]5__179_carry_i_1_n_0\,
      S(1) => \string_cent_decenas[1]5__179_carry_i_2_n_0\,
      S(0) => \string_cent_decenas[1]5__123_carry__5_n_7\
    );
\string_cent_decenas[1]5__179_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \string_cent_decenas[1]5__123_carry__5_n_7\,
      I1 => \string_cent_decenas[1]5__123_carry__5_n_5\,
      O => \string_cent_decenas[1]5__179_carry_i_1_n_0\
    );
\string_cent_decenas[1]5__179_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \string_cent_decenas[1]5__123_carry__5_n_6\,
      I1 => \string_cent_decenas[1]5__123_carry__4_n_4\,
      O => \string_cent_decenas[1]5__179_carry_i_2_n_0\
    );
\string_cent_decenas[1]5__185_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5__185_carry_n_0\,
      CO(2) => \string_cent_decenas[1]5__185_carry_n_1\,
      CO(1) => \string_cent_decenas[1]5__185_carry_n_2\,
      CO(0) => \string_cent_decenas[1]5__185_carry_n_3\,
      CYINIT => '1',
      DI(3) => cent(3),
      DI(2) => \string_cent_decenas[1]5__185_carry_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__185_carry_i_3_n_0\,
      DI(0) => \string_cent_decenas[1]5__185_carry_i_4_n_0\,
      O(3) => \string_cent_decenas[1]5__185_carry_n_4\,
      O(2) => \string_cent_decenas[1]5__185_carry_n_5\,
      O(1) => \string_cent_decenas[1]5__185_carry_n_6\,
      O(0) => \disp_dinero[2]\(0),
      S(3) => \string_cent_decenas[1]5__185_carry_i_5_n_0\,
      S(2) => \string_cent_decenas[1]5__185_carry_i_6_n_0\,
      S(1) => \string_cent_decenas[1]5__185_carry_i_7_n_0\,
      S(0) => \string_cent_decenas[1]5__185_carry_i_8_n_0\
    );
\string_cent_decenas[1]5__185_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__185_carry_n_0\,
      CO(3 downto 0) => \NLW_string_cent_decenas[1]5__185_carry__0_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_string_cent_decenas[1]5__185_carry__0_O_UNCONNECTED\(3 downto 1),
      O(0) => \string_cent_decenas[1]5__185_carry__0_n_7\,
      S(3 downto 1) => B"000",
      S(0) => \string_cent_decenas[1]5__185_carry__0_i_1_n_0\
    );
\string_cent_decenas[1]5__185_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \string_cent_decenas[1]5__179_carry_n_5\,
      I1 => cent(4),
      O => \string_cent_decenas[1]5__185_carry__0_i_1_n_0\
    );
\string_cent_decenas[1]5__185_carry_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8B8B88B"
    )
        port map (
      I0 => total_reg(3),
      I1 => total_reg_26_sn_1,
      I2 => \euros1__337_carry_n_4\,
      I3 => \string_cent_decenas[1]5__185_carry_i_9_n_0\,
      I4 => \euros1__337_carry_n_5\,
      O => cent(3)
    );
\string_cent_decenas[1]5__185_carry_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      O => \string_cent_decenas[1]5__185_carry_i_2_n_0\
    );
\string_cent_decenas[1]5__185_carry_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_reg(1),
      I1 => total_reg_26_sn_1,
      I2 => p_1_in(1),
      O => \string_cent_decenas[1]5__185_carry_i_3_n_0\
    );
\string_cent_decenas[1]5__185_carry_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_reg(0),
      I1 => total_reg_26_sn_1,
      I2 => p_1_in(0),
      O => \string_cent_decenas[1]5__185_carry_i_4_n_0\
    );
\string_cent_decenas[1]5__185_carry_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(3),
      I1 => \string_cent_decenas[1]5__179_carry_n_6\,
      O => \string_cent_decenas[1]5__185_carry_i_5_n_0\
    );
\string_cent_decenas[1]5__185_carry_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I1 => \string_cent_decenas[1]5__179_carry_n_7\,
      O => \string_cent_decenas[1]5__185_carry_i_6_n_0\
    );
\string_cent_decenas[1]5__185_carry_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(1),
      I1 => \string_cent_decenas[1]5__123_carry__4_n_4\,
      O => \string_cent_decenas[1]5__185_carry_i_7_n_0\
    );
\string_cent_decenas[1]5__185_carry_i_8\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(0),
      O => \string_cent_decenas[1]5__185_carry_i_8_n_0\
    );
\string_cent_decenas[1]5__185_carry_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001555555555555"
    )
        port map (
      I0 => \euros1__337_carry__0_n_4\,
      I1 => \euros1__337_carry_n_5\,
      I2 => \euros1__337_carry_n_4\,
      I3 => \euros1__337_carry__0_n_7\,
      I4 => \euros1__337_carry__0_n_6\,
      I5 => \euros1__337_carry__0_n_5\,
      O => \string_cent_decenas[1]5__185_carry_i_9_n_0\
    );
\string_cent_decenas[1]5__27_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5__27_carry_n_0\,
      CO(2) => \string_cent_decenas[1]5__27_carry_n_1\,
      CO(1) => \string_cent_decenas[1]5__27_carry_n_2\,
      CO(0) => \string_cent_decenas[1]5__27_carry_n_3\,
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__27_carry_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__27_carry_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__27_carry_i_3_n_0\,
      DI(0) => '0',
      O(3) => \string_cent_decenas[1]5__27_carry_n_4\,
      O(2) => \string_cent_decenas[1]5__27_carry_n_5\,
      O(1) => \string_cent_decenas[1]5__27_carry_n_6\,
      O(0) => \string_cent_decenas[1]5__27_carry_n_7\,
      S(3) => \string_cent_decenas[1]5__27_carry_i_4_n_0\,
      S(2) => \string_cent_decenas[1]5__27_carry_i_5_n_0\,
      S(1) => \string_cent_decenas[1]5__27_carry_i_6_n_0\,
      S(0) => \string_cent_decenas[1]5__27_carry_i_7_n_0\
    );
\string_cent_decenas[1]5__27_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__27_carry_n_0\,
      CO(3) => \string_cent_decenas[1]5__27_carry__0_n_0\,
      CO(2) => \string_cent_decenas[1]5__27_carry__0_n_1\,
      CO(1) => \string_cent_decenas[1]5__27_carry__0_n_2\,
      CO(0) => \string_cent_decenas[1]5__27_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__27_carry__0_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__27_carry__0_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__27_carry__0_i_3_n_0\,
      DI(0) => \string_cent_decenas[1]5__27_carry__0_i_4_n_0\,
      O(3) => \string_cent_decenas[1]5__27_carry__0_n_4\,
      O(2) => \string_cent_decenas[1]5__27_carry__0_n_5\,
      O(1) => \string_cent_decenas[1]5__27_carry__0_n_6\,
      O(0) => \string_cent_decenas[1]5__27_carry__0_n_7\,
      S(3) => \string_cent_decenas[1]5__27_carry__0_i_5_n_0\,
      S(2) => \string_cent_decenas[1]5__27_carry__0_i_6_n_0\,
      S(1) => \string_cent_decenas[1]5__27_carry__0_i_7_n_0\,
      S(0) => \string_cent_decenas[1]5__27_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5__27_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => cent(4),
      I1 => cent(6),
      O => \string_cent_decenas[1]5__27_carry__0_i_1_n_0\
    );
\string_cent_decenas[1]5__27_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => cent(3),
      I1 => cent(5),
      O => \string_cent_decenas[1]5__27_carry__0_i_2_n_0\
    );
\string_cent_decenas[1]5__27_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(4),
      I1 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      O => \string_cent_decenas[1]5__27_carry__0_i_3_n_0\
    );
\string_cent_decenas[1]5__27_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(4),
      I1 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      O => \string_cent_decenas[1]5__27_carry__0_i_4_n_0\
    );
\string_cent_decenas[1]5__27_carry__0_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4B"
    )
        port map (
      I0 => cent(6),
      I1 => cent(4),
      I2 => cent(5),
      O => \string_cent_decenas[1]5__27_carry__0_i_5_n_0\
    );
\string_cent_decenas[1]5__27_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B44B"
    )
        port map (
      I0 => cent(5),
      I1 => cent(3),
      I2 => cent(6),
      I3 => cent(4),
      O => \string_cent_decenas[1]5__27_carry__0_i_6_n_0\
    );
\string_cent_decenas[1]5__27_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"E11E"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I1 => cent(4),
      I2 => cent(5),
      I3 => cent(3),
      O => \string_cent_decenas[1]5__27_carry__0_i_7_n_0\
    );
\string_cent_decenas[1]5__27_carry__0_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8E71718E"
    )
        port map (
      I0 => cent(6),
      I1 => cent(1),
      I2 => cent(3),
      I3 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I4 => cent(4),
      O => \string_cent_decenas[1]5__27_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5__27_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__27_carry__0_n_0\,
      CO(3) => \NLW_string_cent_decenas[1]5__27_carry__1_CO_UNCONNECTED\(3),
      CO(2) => \string_cent_decenas[1]5__27_carry__1_n_1\,
      CO(1) => \NLW_string_cent_decenas[1]5__27_carry__1_CO_UNCONNECTED\(1),
      CO(0) => \string_cent_decenas[1]5__27_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"001",
      DI(0) => cent(5),
      O(3 downto 2) => \NLW_string_cent_decenas[1]5__27_carry__1_O_UNCONNECTED\(3 downto 2),
      O(1) => \string_cent_decenas[1]5__27_carry__1_n_6\,
      O(0) => \string_cent_decenas[1]5__27_carry__1_n_7\,
      S(3 downto 2) => B"01",
      S(1) => \string_cent_decenas[1]5__27_carry__1_i_1_n_0\,
      S(0) => \string_cent_decenas[1]5__27_carry__1_i_2_n_0\
    );
\string_cent_decenas[1]5__27_carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(6),
      O => \string_cent_decenas[1]5__27_carry__1_i_1_n_0\
    );
\string_cent_decenas[1]5__27_carry__1_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(5),
      I1 => cent(6),
      O => \string_cent_decenas[1]5__27_carry__1_i_2_n_0\
    );
\string_cent_decenas[1]5__27_carry_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => cent(0),
      I1 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I2 => cent(5),
      O => \string_cent_decenas[1]5__27_carry_i_1_n_0\
    );
\string_cent_decenas[1]5__27_carry_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => cent(5),
      I1 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I2 => cent(0),
      O => \string_cent_decenas[1]5__27_carry_i_2_n_0\
    );
\string_cent_decenas[1]5__27_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => cent(3),
      I1 => cent(0),
      O => \string_cent_decenas[1]5__27_carry_i_3_n_0\
    );
\string_cent_decenas[1]5__27_carry_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry_i_1_n_0\,
      I1 => cent(1),
      I2 => cent(6),
      I3 => cent(3),
      O => \string_cent_decenas[1]5__27_carry_i_4_n_0\
    );
\string_cent_decenas[1]5__27_carry_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96699696"
    )
        port map (
      I0 => cent(0),
      I1 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I2 => cent(5),
      I3 => cent(1),
      I4 => cent(4),
      O => \string_cent_decenas[1]5__27_carry_i_5_n_0\
    );
\string_cent_decenas[1]5__27_carry_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => cent(0),
      I1 => cent(3),
      I2 => cent(1),
      I3 => cent(4),
      O => \string_cent_decenas[1]5__27_carry_i_6_n_0\
    );
\string_cent_decenas[1]5__27_carry_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(3),
      I1 => cent(0),
      O => \string_cent_decenas[1]5__27_carry_i_7_n_0\
    );
\string_cent_decenas[1]5__54_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5__54_carry_n_0\,
      CO(2) => \string_cent_decenas[1]5__54_carry_n_1\,
      CO(1) => \string_cent_decenas[1]5__54_carry_n_2\,
      CO(0) => \string_cent_decenas[1]5__54_carry_n_3\,
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__54_carry_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__54_carry_i_2_n_0\,
      DI(1 downto 0) => B"01",
      O(3) => \string_cent_decenas[1]5__54_carry_n_4\,
      O(2) => \string_cent_decenas[1]5__54_carry_n_5\,
      O(1) => \string_cent_decenas[1]5__54_carry_n_6\,
      O(0) => \NLW_string_cent_decenas[1]5__54_carry_O_UNCONNECTED\(0),
      S(3) => \string_cent_decenas[1]5__54_carry_i_3_n_0\,
      S(2) => \string_cent_decenas[1]5__54_carry_i_4_n_0\,
      S(1) => \string_cent_decenas[1]5__54_carry_i_5_n_0\,
      S(0) => \string_cent_decenas[1]5__54_carry_i_6_n_0\
    );
\string_cent_decenas[1]5__54_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__54_carry_n_0\,
      CO(3) => \string_cent_decenas[1]5__54_carry__0_n_0\,
      CO(2) => \string_cent_decenas[1]5__54_carry__0_n_1\,
      CO(1) => \string_cent_decenas[1]5__54_carry__0_n_2\,
      CO(0) => \string_cent_decenas[1]5__54_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__54_carry__0_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5_carry__0_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__54_carry__0_i_2_n_0\,
      DI(0) => \string_cent_decenas[1]5__54_carry__0_i_3_n_0\,
      O(3) => \string_cent_decenas[1]5__54_carry__0_n_4\,
      O(2) => \string_cent_decenas[1]5__54_carry__0_n_5\,
      O(1) => \string_cent_decenas[1]5__54_carry__0_n_6\,
      O(0) => \string_cent_decenas[1]5__54_carry__0_n_7\,
      S(3) => \string_cent_decenas[1]5__54_carry__0_i_4_n_0\,
      S(2) => \string_cent_decenas[1]5__54_carry__0_i_5_n_0\,
      S(1) => \string_cent_decenas[1]5__54_carry__0_i_6_n_0\,
      S(0) => \string_cent_decenas[1]5__54_carry__0_i_7_n_0\
    );
\string_cent_decenas[1]5__54_carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => cent(4),
      I1 => cent(6),
      I2 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      O => \string_cent_decenas[1]5__54_carry__0_i_1_n_0\
    );
\string_cent_decenas[1]5__54_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => cent(1),
      I1 => cent(5),
      I2 => cent(3),
      O => \string_cent_decenas[1]5__54_carry__0_i_2_n_0\
    );
\string_cent_decenas[1]5__54_carry__0_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      O => \string_cent_decenas[1]5__54_carry__0_i_3_n_0\
    );
\string_cent_decenas[1]5__54_carry__0_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8E71718E"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I1 => cent(6),
      I2 => cent(4),
      I3 => cent(5),
      I4 => cent(3),
      O => \string_cent_decenas[1]5__54_carry__0_i_4_n_0\
    );
\string_cent_decenas[1]5__54_carry__0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"718E8E718E71718E"
    )
        port map (
      I0 => cent(1),
      I1 => cent(3),
      I2 => cent(5),
      I3 => cent(4),
      I4 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I5 => cent(6),
      O => \string_cent_decenas[1]5__54_carry__0_i_5_n_0\
    );
\string_cent_decenas[1]5__54_carry__0_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69966969"
    )
        port map (
      I0 => cent(3),
      I1 => cent(5),
      I2 => cent(1),
      I3 => cent(4),
      I4 => cent(0),
      O => \string_cent_decenas[1]5__54_carry__0_i_6_n_0\
    );
\string_cent_decenas[1]5__54_carry__0_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => cent(0),
      I1 => cent(4),
      I2 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      O => \string_cent_decenas[1]5__54_carry__0_i_7_n_0\
    );
\string_cent_decenas[1]5__54_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__54_carry__0_n_0\,
      CO(3) => \string_cent_decenas[1]5__54_carry__1_n_0\,
      CO(2) => \string_cent_decenas[1]5__54_carry__1_n_1\,
      CO(1) => \string_cent_decenas[1]5__54_carry__1_n_2\,
      CO(0) => \string_cent_decenas[1]5__54_carry__1_n_3\,
      CYINIT => '0',
      DI(3) => '1',
      DI(2) => cent(5),
      DI(1) => \string_cent_decenas[1]5__54_carry__1_i_1_n_0\,
      DI(0) => \string_cent_decenas[1]5__54_carry__1_i_2_n_0\,
      O(3) => \string_cent_decenas[1]5__54_carry__1_n_4\,
      O(2) => \string_cent_decenas[1]5__54_carry__1_n_5\,
      O(1) => \string_cent_decenas[1]5__54_carry__1_n_6\,
      O(0) => \string_cent_decenas[1]5__54_carry__1_n_7\,
      S(3) => \string_cent_decenas[1]5__54_carry__1_i_3_n_0\,
      S(2) => \string_cent_decenas[1]5__54_carry__1_i_4_n_0\,
      S(1) => \string_cent_decenas[1]5__54_carry__1_i_5_n_0\,
      S(0) => \string_cent_decenas[1]5__54_carry__1_i_6_n_0\
    );
\string_cent_decenas[1]5__54_carry__1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => cent(6),
      I1 => cent(4),
      O => \string_cent_decenas[1]5__54_carry__1_i_1_n_0\
    );
\string_cent_decenas[1]5__54_carry__1_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => cent(5),
      I1 => cent(3),
      O => \string_cent_decenas[1]5__54_carry__1_i_2_n_0\
    );
\string_cent_decenas[1]5__54_carry__1_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(6),
      O => \string_cent_decenas[1]5__54_carry__1_i_3_n_0\
    );
\string_cent_decenas[1]5__54_carry__1_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(5),
      I1 => cent(6),
      O => \string_cent_decenas[1]5__54_carry__1_i_4_n_0\
    );
\string_cent_decenas[1]5__54_carry__1_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E1"
    )
        port map (
      I0 => cent(6),
      I1 => cent(4),
      I2 => cent(5),
      O => \string_cent_decenas[1]5__54_carry__1_i_5_n_0\
    );
\string_cent_decenas[1]5__54_carry__1_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1EE1"
    )
        port map (
      I0 => cent(5),
      I1 => cent(3),
      I2 => cent(6),
      I3 => cent(4),
      O => \string_cent_decenas[1]5__54_carry__1_i_6_n_0\
    );
\string_cent_decenas[1]5__54_carry_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_reg(1),
      I1 => total_reg_26_sn_1,
      I2 => p_1_in(1),
      O => \string_cent_decenas[1]5__54_carry_i_1_n_0\
    );
\string_cent_decenas[1]5__54_carry_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_reg(0),
      I1 => total_reg_26_sn_1,
      I2 => p_1_in(0),
      O => \string_cent_decenas[1]5__54_carry_i_2_n_0\
    );
\string_cent_decenas[1]5__54_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(1),
      I1 => cent(3),
      O => \string_cent_decenas[1]5__54_carry_i_3_n_0\
    );
\string_cent_decenas[1]5__54_carry_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(0),
      I1 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      O => \string_cent_decenas[1]5__54_carry_i_4_n_0\
    );
\string_cent_decenas[1]5__54_carry_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(1),
      O => \string_cent_decenas[1]5__54_carry_i_5_n_0\
    );
\string_cent_decenas[1]5__54_carry_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_reg(0),
      I1 => total_reg_26_sn_1,
      I2 => p_1_in(0),
      O => \string_cent_decenas[1]5__54_carry_i_6_n_0\
    );
\string_cent_decenas[1]5__78_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5__78_carry_n_0\,
      CO(2) => \string_cent_decenas[1]5__78_carry_n_1\,
      CO(1) => \string_cent_decenas[1]5__78_carry_n_2\,
      CO(0) => \string_cent_decenas[1]5__78_carry_n_3\,
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__27_carry_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__78_carry_i_1_n_0\,
      DI(1) => \string_cent_decenas[1]5__78_carry_i_2_n_0\,
      DI(0) => '0',
      O(3) => \string_cent_decenas[1]5__78_carry_n_4\,
      O(2) => \string_cent_decenas[1]5__78_carry_n_5\,
      O(1) => \string_cent_decenas[1]5__78_carry_n_6\,
      O(0) => \NLW_string_cent_decenas[1]5__78_carry_O_UNCONNECTED\(0),
      S(3) => \string_cent_decenas[1]5__78_carry_i_3_n_0\,
      S(2) => \string_cent_decenas[1]5__78_carry_i_4_n_0\,
      S(1) => \string_cent_decenas[1]5__78_carry_i_5_n_0\,
      S(0) => \string_cent_decenas[1]5__78_carry_i_6_n_0\
    );
\string_cent_decenas[1]5__78_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__78_carry_n_0\,
      CO(3) => \string_cent_decenas[1]5__78_carry__0_n_0\,
      CO(2) => \string_cent_decenas[1]5__78_carry__0_n_1\,
      CO(1) => \string_cent_decenas[1]5__78_carry__0_n_2\,
      CO(0) => \string_cent_decenas[1]5__78_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__78_carry__0_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__78_carry__0_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__78_carry__0_i_3_n_0\,
      DI(0) => \string_cent_decenas[1]5__78_carry__0_i_4_n_0\,
      O(3) => \string_cent_decenas[1]5__78_carry__0_n_4\,
      O(2) => \string_cent_decenas[1]5__78_carry__0_n_5\,
      O(1) => \string_cent_decenas[1]5__78_carry__0_n_6\,
      O(0) => \string_cent_decenas[1]5__78_carry__0_n_7\,
      S(3) => \string_cent_decenas[1]5__78_carry__0_i_5_n_0\,
      S(2) => \string_cent_decenas[1]5__78_carry__0_i_6_n_0\,
      S(1) => \string_cent_decenas[1]5__78_carry__0_i_7_n_0\,
      S(0) => \string_cent_decenas[1]5__78_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5__78_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => cent(4),
      I1 => cent(6),
      O => \string_cent_decenas[1]5__78_carry__0_i_1_n_0\
    );
\string_cent_decenas[1]5__78_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => cent(3),
      I1 => cent(5),
      O => \string_cent_decenas[1]5__78_carry__0_i_2_n_0\
    );
\string_cent_decenas[1]5__78_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(4),
      I1 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      O => \string_cent_decenas[1]5__78_carry__0_i_3_n_0\
    );
\string_cent_decenas[1]5__78_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(4),
      I1 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      O => \string_cent_decenas[1]5__78_carry__0_i_4_n_0\
    );
\string_cent_decenas[1]5__78_carry__0_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4B"
    )
        port map (
      I0 => cent(6),
      I1 => cent(4),
      I2 => cent(5),
      O => \string_cent_decenas[1]5__78_carry__0_i_5_n_0\
    );
\string_cent_decenas[1]5__78_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B44B"
    )
        port map (
      I0 => cent(5),
      I1 => cent(3),
      I2 => cent(6),
      I3 => cent(4),
      O => \string_cent_decenas[1]5__78_carry__0_i_6_n_0\
    );
\string_cent_decenas[1]5__78_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"E11E"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I1 => cent(4),
      I2 => cent(5),
      I3 => cent(3),
      O => \string_cent_decenas[1]5__78_carry__0_i_7_n_0\
    );
\string_cent_decenas[1]5__78_carry__0_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8E71718E"
    )
        port map (
      I0 => cent(6),
      I1 => cent(1),
      I2 => cent(3),
      I3 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I4 => cent(4),
      O => \string_cent_decenas[1]5__78_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5__78_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__78_carry__0_n_0\,
      CO(3) => \NLW_string_cent_decenas[1]5__78_carry__1_CO_UNCONNECTED\(3),
      CO(2) => \string_cent_decenas[1]5__78_carry__1_n_1\,
      CO(1) => \NLW_string_cent_decenas[1]5__78_carry__1_CO_UNCONNECTED\(1),
      CO(0) => \string_cent_decenas[1]5__78_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"001",
      DI(0) => cent(5),
      O(3 downto 2) => \NLW_string_cent_decenas[1]5__78_carry__1_O_UNCONNECTED\(3 downto 2),
      O(1) => \string_cent_decenas[1]5__78_carry__1_n_6\,
      O(0) => \string_cent_decenas[1]5__78_carry__1_n_7\,
      S(3 downto 2) => B"01",
      S(1) => \string_cent_decenas[1]5__78_carry__1_i_1_n_0\,
      S(0) => \string_cent_decenas[1]5__78_carry__1_i_2_n_0\
    );
\string_cent_decenas[1]5__78_carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(6),
      O => \string_cent_decenas[1]5__78_carry__1_i_1_n_0\
    );
\string_cent_decenas[1]5__78_carry__1_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(5),
      I1 => cent(6),
      O => \string_cent_decenas[1]5__78_carry__1_i_2_n_0\
    );
\string_cent_decenas[1]5__78_carry_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => cent(5),
      I1 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I2 => cent(0),
      O => \string_cent_decenas[1]5__78_carry_i_1_n_0\
    );
\string_cent_decenas[1]5__78_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => cent(3),
      I1 => cent(0),
      O => \string_cent_decenas[1]5__78_carry_i_2_n_0\
    );
\string_cent_decenas[1]5__78_carry_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry_i_1_n_0\,
      I1 => cent(1),
      I2 => cent(6),
      I3 => cent(3),
      O => \string_cent_decenas[1]5__78_carry_i_3_n_0\
    );
\string_cent_decenas[1]5__78_carry_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96699696"
    )
        port map (
      I0 => cent(0),
      I1 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I2 => cent(5),
      I3 => cent(1),
      I4 => cent(4),
      O => \string_cent_decenas[1]5__78_carry_i_4_n_0\
    );
\string_cent_decenas[1]5__78_carry_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => cent(0),
      I1 => cent(3),
      I2 => cent(1),
      I3 => cent(4),
      O => \string_cent_decenas[1]5__78_carry_i_5_n_0\
    );
\string_cent_decenas[1]5__78_carry_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(3),
      I1 => cent(0),
      O => \string_cent_decenas[1]5__78_carry_i_6_n_0\
    );
\string_cent_decenas[1]5__99_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5__99_carry_n_0\,
      CO(2) => \string_cent_decenas[1]5__99_carry_n_1\,
      CO(1) => \string_cent_decenas[1]5__99_carry_n_2\,
      CO(0) => \string_cent_decenas[1]5__99_carry_n_3\,
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__99_carry_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__99_carry_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__99_carry_i_3_n_0\,
      DI(0) => '0',
      O(3) => \string_cent_decenas[1]5__99_carry_n_4\,
      O(2) => \string_cent_decenas[1]5__99_carry_n_5\,
      O(1) => \string_cent_decenas[1]5__99_carry_n_6\,
      O(0) => \string_cent_decenas[1]5__99_carry_n_7\,
      S(3) => \string_cent_decenas[1]5__99_carry_i_4_n_0\,
      S(2) => \string_cent_decenas[1]5__99_carry_i_5_n_0\,
      S(1) => \string_cent_decenas[1]5__99_carry_i_6_n_0\,
      S(0) => \string_cent_decenas[1]5__99_carry_i_7_n_0\
    );
\string_cent_decenas[1]5__99_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__99_carry_n_0\,
      CO(3) => \string_cent_decenas[1]5__99_carry__0_n_0\,
      CO(2) => \string_cent_decenas[1]5__99_carry__0_n_1\,
      CO(1) => \string_cent_decenas[1]5__99_carry__0_n_2\,
      CO(0) => \string_cent_decenas[1]5__99_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__99_carry__0_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__99_carry__0_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__99_carry__0_i_3_n_0\,
      DI(0) => \string_cent_decenas[1]5__99_carry__0_i_4_n_0\,
      O(3) => \string_cent_decenas[1]5__99_carry__0_n_4\,
      O(2) => \string_cent_decenas[1]5__99_carry__0_n_5\,
      O(1) => \string_cent_decenas[1]5__99_carry__0_n_6\,
      O(0) => \string_cent_decenas[1]5__99_carry__0_n_7\,
      S(3) => \string_cent_decenas[1]5__99_carry__0_i_5_n_0\,
      S(2) => \string_cent_decenas[1]5__99_carry__0_i_6_n_0\,
      S(1) => \string_cent_decenas[1]5__99_carry__0_i_7_n_0\,
      S(0) => \string_cent_decenas[1]5__99_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5__99_carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => cent(4),
      I1 => cent(6),
      I2 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__99_carry__0_i_1_n_0\
    );
\string_cent_decenas[1]5__99_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => cent(3),
      I1 => cent(5),
      I2 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__99_carry__0_i_2_n_0\
    );
\string_cent_decenas[1]5__99_carry__0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => cent(4),
      I1 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I2 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__99_carry__0_i_3_n_0\
    );
\string_cent_decenas[1]5__99_carry__0_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => cent(3),
      I1 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      I2 => cent(1),
      O => \string_cent_decenas[1]5__99_carry__0_i_4_n_0\
    );
\string_cent_decenas[1]5__99_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4B2D"
    )
        port map (
      I0 => cent(6),
      I1 => cent(4),
      I2 => cent(5),
      I3 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__99_carry__0_i_5_n_0\
    );
\string_cent_decenas[1]5__99_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \string_cent_decenas[1]5__99_carry__0_i_2_n_0\,
      I1 => cent(4),
      I2 => cent(6),
      I3 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__99_carry__0_i_6_n_0\
    );
\string_cent_decenas[1]5__99_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => cent(3),
      I1 => cent(5),
      I2 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      I3 => \string_cent_decenas[1]5__99_carry__0_i_3_n_0\,
      O => \string_cent_decenas[1]5__99_carry__0_i_7_n_0\
    );
\string_cent_decenas[1]5__99_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => cent(4),
      I1 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I2 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      I3 => \string_cent_decenas[1]5__99_carry__0_i_4_n_0\,
      O => \string_cent_decenas[1]5__99_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5__99_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__99_carry__0_n_0\,
      CO(3 downto 0) => \NLW_string_cent_decenas[1]5__99_carry__1_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_string_cent_decenas[1]5__99_carry__1_O_UNCONNECTED\(3 downto 1),
      O(0) => \string_cent_decenas[1]5__99_carry__1_n_7\,
      S(3 downto 1) => B"000",
      S(0) => \string_cent_decenas[1]5__99_carry__1_i_1_n_0\
    );
\string_cent_decenas[1]5__99_carry__1_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"93"
    )
        port map (
      I0 => cent(5),
      I1 => cent(6),
      I2 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__99_carry__1_i_1_n_0\
    );
\string_cent_decenas[1]5__99_carry_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8E"
    )
        port map (
      I0 => cent(0),
      I1 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I2 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__99_carry_i_1_n_0\
    );
\string_cent_decenas[1]5__99_carry_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      I1 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I2 => cent(0),
      O => \string_cent_decenas[1]5__99_carry_i_2_n_0\
    );
\string_cent_decenas[1]5__99_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      I1 => cent(0),
      O => \string_cent_decenas[1]5__99_carry_i_3_n_0\
    );
\string_cent_decenas[1]5__99_carry_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => cent(3),
      I1 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      I2 => cent(1),
      I3 => \string_cent_decenas[1]5__99_carry_i_1_n_0\,
      O => \string_cent_decenas[1]5__99_carry_i_4_n_0\
    );
\string_cent_decenas[1]5__99_carry_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6966"
    )
        port map (
      I0 => cent(0),
      I1 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I2 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      I3 => cent(1),
      O => \string_cent_decenas[1]5__99_carry_i_5_n_0\
    );
\string_cent_decenas[1]5__99_carry_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"63"
    )
        port map (
      I0 => cent(0),
      I1 => cent(1),
      I2 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__99_carry_i_6_n_0\
    );
\string_cent_decenas[1]5__99_carry_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(0),
      I1 => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__99_carry_i_7_n_0\
    );
\string_cent_decenas[1]5__99_carry_i_8\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_carry__1_n_0\,
      CO(3 downto 1) => \NLW_string_cent_decenas[1]5__99_carry_i_8_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \string_cent_decenas[1]5__99_carry_i_8_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_string_cent_decenas[1]5__99_carry_i_8_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
\string_cent_decenas[1]5_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5_carry_n_0\,
      CO(2) => \string_cent_decenas[1]5_carry_n_1\,
      CO(1) => \string_cent_decenas[1]5_carry_n_2\,
      CO(0) => \string_cent_decenas[1]5_carry_n_3\,
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5_carry_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5_carry_i_2_n_0\,
      DI(1 downto 0) => B"01",
      O(3 downto 1) => \NLW_string_cent_decenas[1]5_carry_O_UNCONNECTED\(3 downto 1),
      O(0) => \string_cent_decenas[1]5_carry_n_7\,
      S(3) => \string_cent_decenas[1]5_carry_i_3_n_0\,
      S(2) => \string_cent_decenas[1]5_carry_i_4_n_0\,
      S(1) => \string_cent_decenas[1]5_carry_i_5_n_0\,
      S(0) => \string_cent_decenas[1]5_carry_i_6_n_0\
    );
\string_cent_decenas[1]5_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_carry_n_0\,
      CO(3) => \string_cent_decenas[1]5_carry__0_n_0\,
      CO(2) => \string_cent_decenas[1]5_carry__0_n_1\,
      CO(1) => \string_cent_decenas[1]5_carry__0_n_2\,
      CO(0) => \string_cent_decenas[1]5_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5_carry__0_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5_carry__0_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5_carry__0_i_3_n_0\,
      DI(0) => \string_cent_decenas[1]5_carry__0_i_4_n_0\,
      O(3) => \string_cent_decenas[1]5_carry__0_n_4\,
      O(2) => \string_cent_decenas[1]5_carry__0_n_5\,
      O(1) => \string_cent_decenas[1]5_carry__0_n_6\,
      O(0) => \NLW_string_cent_decenas[1]5_carry__0_O_UNCONNECTED\(0),
      S(3) => \string_cent_decenas[1]5_carry__0_i_5_n_0\,
      S(2) => \string_cent_decenas[1]5_carry__0_i_6_n_0\,
      S(1) => \string_cent_decenas[1]5_carry__0_i_7_n_0\,
      S(0) => \string_cent_decenas[1]5_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5_carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => cent(4),
      I1 => cent(6),
      I2 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      O => \string_cent_decenas[1]5_carry__0_i_1_n_0\
    );
\string_cent_decenas[1]5_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => cent(6),
      I1 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I2 => cent(4),
      O => \string_cent_decenas[1]5_carry__0_i_2_n_0\
    );
\string_cent_decenas[1]5_carry__0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => cent(1),
      I1 => cent(5),
      I2 => cent(3),
      O => \string_cent_decenas[1]5_carry__0_i_3_n_0\
    );
\string_cent_decenas[1]5_carry__0_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      O => \string_cent_decenas[1]5_carry__0_i_4_n_0\
    );
\string_cent_decenas[1]5_carry__0_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8E71718E"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I1 => cent(6),
      I2 => cent(4),
      I3 => cent(5),
      I4 => cent(3),
      O => \string_cent_decenas[1]5_carry__0_i_5_n_0\
    );
\string_cent_decenas[1]5_carry__0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"718E8E718E71718E"
    )
        port map (
      I0 => cent(1),
      I1 => cent(3),
      I2 => cent(5),
      I3 => cent(4),
      I4 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I5 => cent(6),
      O => \string_cent_decenas[1]5_carry__0_i_6_n_0\
    );
\string_cent_decenas[1]5_carry__0_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69966969"
    )
        port map (
      I0 => cent(3),
      I1 => cent(5),
      I2 => cent(1),
      I3 => cent(4),
      I4 => cent(0),
      O => \string_cent_decenas[1]5_carry__0_i_7_n_0\
    );
\string_cent_decenas[1]5_carry__0_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => cent(0),
      I1 => cent(4),
      I2 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      O => \string_cent_decenas[1]5_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_carry__0_n_0\,
      CO(3) => \string_cent_decenas[1]5_carry__1_n_0\,
      CO(2) => \string_cent_decenas[1]5_carry__1_n_1\,
      CO(1) => \string_cent_decenas[1]5_carry__1_n_2\,
      CO(0) => \string_cent_decenas[1]5_carry__1_n_3\,
      CYINIT => '0',
      DI(3) => '1',
      DI(2) => cent(5),
      DI(1) => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      DI(0) => \string_cent_decenas[1]5_carry__1_i_3_n_0\,
      O(3) => \string_cent_decenas[1]5_carry__1_n_4\,
      O(2) => \string_cent_decenas[1]5_carry__1_n_5\,
      O(1) => \string_cent_decenas[1]5_carry__1_n_6\,
      O(0) => \string_cent_decenas[1]5_carry__1_n_7\,
      S(3) => \string_cent_decenas[1]5_carry__1_i_4_n_0\,
      S(2) => \string_cent_decenas[1]5_carry__1_i_5_n_0\,
      S(1) => \string_cent_decenas[1]5_carry__1_i_6_n_0\,
      S(0) => \string_cent_decenas[1]5_carry__1_i_7_n_0\
    );
\string_cent_decenas[1]5_carry__1_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF0000CD30CD30"
    )
        port map (
      I0 => \euros1__337_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5_carry__1_i_8_n_0\,
      I2 => \euros1__337_carry__0_n_4\,
      I3 => \euros1__337_carry__0_n_6\,
      I4 => total_reg(5),
      I5 => total_reg_26_sn_1,
      O => cent(5)
    );
\string_cent_decenas[1]5_carry__1_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => cent(6),
      I1 => cent(4),
      O => \string_cent_decenas[1]5_carry__1_i_2_n_0\
    );
\string_cent_decenas[1]5_carry__1_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => cent(5),
      I1 => cent(3),
      O => \string_cent_decenas[1]5_carry__1_i_3_n_0\
    );
\string_cent_decenas[1]5_carry__1_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(6),
      O => \string_cent_decenas[1]5_carry__1_i_4_n_0\
    );
\string_cent_decenas[1]5_carry__1_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(5),
      I1 => cent(6),
      O => \string_cent_decenas[1]5_carry__1_i_5_n_0\
    );
\string_cent_decenas[1]5_carry__1_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E1"
    )
        port map (
      I0 => cent(6),
      I1 => cent(4),
      I2 => cent(5),
      O => \string_cent_decenas[1]5_carry__1_i_6_n_0\
    );
\string_cent_decenas[1]5_carry__1_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1EE1"
    )
        port map (
      I0 => cent(5),
      I1 => cent(3),
      I2 => cent(6),
      I3 => cent(4),
      O => \string_cent_decenas[1]5_carry__1_i_7_n_0\
    );
\string_cent_decenas[1]5_carry__1_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \euros1__337_carry_n_5\,
      I1 => \euros1__337_carry_n_4\,
      I2 => \euros1__337_carry__0_n_7\,
      O => \string_cent_decenas[1]5_carry__1_i_8_n_0\
    );
\string_cent_decenas[1]5_carry_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_reg(1),
      I1 => total_reg_26_sn_1,
      I2 => p_1_in(1),
      O => \string_cent_decenas[1]5_carry_i_1_n_0\
    );
\string_cent_decenas[1]5_carry_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_reg(0),
      I1 => total_reg_26_sn_1,
      I2 => p_1_in(0),
      O => \string_cent_decenas[1]5_carry_i_2_n_0\
    );
\string_cent_decenas[1]5_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(1),
      I1 => cent(3),
      O => \string_cent_decenas[1]5_carry_i_3_n_0\
    );
\string_cent_decenas[1]5_carry_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(0),
      I1 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      O => \string_cent_decenas[1]5_carry_i_4_n_0\
    );
\string_cent_decenas[1]5_carry_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(1),
      O => \string_cent_decenas[1]5_carry_i_5_n_0\
    );
\string_cent_decenas[1]5_carry_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_reg(0),
      I1 => total_reg_26_sn_1,
      I2 => p_1_in(0),
      O => \string_cent_decenas[1]5_carry_i_6_n_0\
    );
\string_cent_decenas[1]5_carry_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_reg(1),
      I1 => total_reg_26_sn_1,
      I2 => p_1_in(1),
      O => cent(1)
    );
\string_cent_decenas[1]5_carry_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_reg(0),
      I1 => total_reg_26_sn_1,
      I2 => p_1_in(0),
      O => cent(0)
    );
\string_cent_decenas[1]5_carry_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4774"
    )
        port map (
      I0 => total_reg(2),
      I1 => total_reg_26_sn_1,
      I2 => \euros1__337_carry_n_5\,
      I3 => \string_cent_decenas[1]5__185_carry_i_9_n_0\,
      O => \string_cent_decenas[1]5_carry_i_9_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___0_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___0_carry_n_0\,
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___0_carry_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___0_carry_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___0_carry_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => cent(4 downto 3),
      DI(1) => \i___0_carry_i_2_n_0\,
      DI(0) => '0',
      O(3 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___0_carry_O_UNCONNECTED\(3 downto 0),
      S(3) => \i___0_carry_i_3_n_0\,
      S(2) => \i___0_carry_i_4_n_0\,
      S(1) => \i___0_carry_i_5_n_0\,
      S(0) => \i___0_carry_i_6_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___0_carry_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_0\,
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \i___0_carry__0_i_1_n_0\,
      DI(2) => \i___0_carry__0_i_2_n_0\,
      DI(1 downto 0) => cent(6 downto 5),
      O(3) => \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_4\,
      O(2 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__0_O_UNCONNECTED\(2 downto 0),
      S(3) => \i___0_carry__0_i_4_n_0\,
      S(2) => \i___0_carry__0_i_5_n_0\,
      S(1) => \i___0_carry__0_i_6_n_0\,
      S(0) => \i___0_carry__0_i_7_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_0\,
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \i___0_carry__1_i_1_n_0\,
      O(3) => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_4\,
      O(2) => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_7\,
      S(3) => \i___0_carry__1_i_2_n_0\,
      S(2) => \i___0_carry__1_i_3_n_0\,
      S(1) => \i___0_carry__1_i_4_n_0\,
      S(0) => \i___0_carry__1_i_5_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_0\,
      CO(3 downto 2) => \NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__2_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___0_carry__2_n_2\,
      CO(0) => \NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__2_CO_UNCONNECTED\(0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__2_O_UNCONNECTED\(3 downto 1),
      O(0) => \string_cent_decenas[1]5_inferred__0/i___0_carry__2_n_7\,
      S(3 downto 1) => B"001",
      S(0) => \i___0_carry__2_i_1_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___106_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___106_carry_n_0\,
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___106_carry_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___106_carry_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___106_carry_n_3\,
      CYINIT => '0',
      DI(3) => \i___106_carry_i_1_n_0\,
      DI(2) => \i___106_carry_i_2_n_0\,
      DI(1) => \i___106_carry_i_3_n_0\,
      DI(0) => \i___106_carry_i_4_n_0\,
      O(3 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry_O_UNCONNECTED\(3 downto 0),
      S(3) => \i___106_carry_i_5_n_0\,
      S(2) => \i___106_carry_i_6_n_0\,
      S(1) => \i___106_carry_i_7_n_0\,
      S(0) => \i___106_carry_i_8_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___106_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___106_carry_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___106_carry__0_n_0\,
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___106_carry__0_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___106_carry__0_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___106_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \i___106_carry__0_i_1_n_0\,
      DI(2) => \i___106_carry__0_i_2_n_0\,
      DI(1) => \i___106_carry__0_i_3_n_0\,
      DI(0) => \i___106_carry__0_i_4_n_0\,
      O(3 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \i___106_carry__0_i_5_n_0\,
      S(2) => \i___106_carry__0_i_6_n_0\,
      S(1) => \i___106_carry__0_i_7_n_0\,
      S(0) => \i___106_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___106_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___106_carry__0_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___106_carry__1_n_0\,
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___106_carry__1_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___106_carry__1_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___106_carry__1_n_3\,
      CYINIT => '0',
      DI(3) => \i___106_carry__1_i_1_n_0\,
      DI(2) => \i___106_carry__1_i_2_n_0\,
      DI(1) => \i___106_carry__1_i_3_n_0\,
      DI(0) => \i___106_carry__1_i_4_n_0\,
      O(3 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__1_O_UNCONNECTED\(3 downto 0),
      S(3) => \i___106_carry__1_i_5_n_0\,
      S(2) => \i___106_carry__1_i_6_n_0\,
      S(1) => \i___106_carry__1_i_7_n_0\,
      S(0) => \i___106_carry__1_i_8_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___106_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___106_carry__1_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___106_carry__2_n_0\,
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___106_carry__2_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___106_carry__2_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___106_carry__2_n_3\,
      CYINIT => '0',
      DI(3) => \i___106_carry__2_i_1_n_0\,
      DI(2) => \i___106_carry__2_i_2_n_0\,
      DI(1) => \i___106_carry__2_i_3_n_0\,
      DI(0) => \i___106_carry__2_i_4_n_0\,
      O(3 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__2_O_UNCONNECTED\(3 downto 0),
      S(3) => \i___106_carry__2_i_5_n_0\,
      S(2) => \i___106_carry__2_i_6_n_0\,
      S(1) => \i___106_carry__2_i_7_n_0\,
      S(0) => \i___106_carry__2_i_8_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___106_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___106_carry__2_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___106_carry__3_n_0\,
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___106_carry__3_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___106_carry__3_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___106_carry__3_n_3\,
      CYINIT => '0',
      DI(3) => \i___106_carry__3_i_1_n_0\,
      DI(2) => \i___106_carry__3_i_2_n_0\,
      DI(1) => \i___106_carry__3_i_3_n_0\,
      DI(0) => \i___106_carry__3_i_4_n_0\,
      O(3 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__3_O_UNCONNECTED\(3 downto 0),
      S(3) => \i___106_carry__3_i_5_n_0\,
      S(2) => \i___106_carry__3_i_6_n_0\,
      S(1) => \i___106_carry__3_i_7_n_0\,
      S(0) => \i___106_carry__3_i_8_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___106_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___106_carry__3_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_0\,
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \i___106_carry__4_i_1_n_0\,
      DI(1) => \i___106_carry__4_i_2_n_0\,
      DI(0) => \i___106_carry__4_i_3_n_0\,
      O(3) => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_4\,
      O(2) => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_7\,
      S(3) => \i___106_carry__4_i_4_n_0\,
      S(2) => \i___106_carry__4_i_5_n_0\,
      S(1) => \i___106_carry__4_i_6_n_0\,
      S(0) => \i___106_carry__4_i_7_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___106_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_0\,
      CO(3 downto 1) => \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__5_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___106_carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 2) => \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__5_O_UNCONNECTED\(3 downto 2),
      O(1) => \string_cent_decenas[1]5_inferred__0/i___106_carry__5_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___106_carry__5_n_7\,
      S(3 downto 2) => B"00",
      S(1) => \i___106_carry__5_i_1_n_0\,
      S(0) => \i___106_carry__5_i_2_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___163_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3 downto 2) => \NLW_string_cent_decenas[1]5_inferred__0/i___163_carry_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___163_carry_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___163_carry_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \i___163_carry_i_1_n_0\,
      DI(0) => '0',
      O(3) => \NLW_string_cent_decenas[1]5_inferred__0/i___163_carry_O_UNCONNECTED\(3),
      O(2) => \string_cent_decenas[1]5_inferred__0/i___163_carry_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i___163_carry_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___163_carry_n_7\,
      S(3) => '0',
      S(2) => \i___163_carry_i_2_n_0\,
      S(1) => \i___163_carry_i_3_n_0\,
      S(0) => \i___163_carry_i_4_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___169_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_0\,
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_3\,
      CYINIT => '1',
      DI(3 downto 2) => cent(3 downto 2),
      DI(1) => \i___169_carry_i_2_n_0\,
      DI(0) => \i___169_carry_i_3_n_0\,
      O(3) => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_4\,
      O(2) => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_5\,
      O(1) => \string_cent_decenas[1]5\(1),
      O(0) => \NLW_string_cent_decenas[1]5_inferred__0/i___169_carry_O_UNCONNECTED\(0),
      S(3) => \i___169_carry_i_4_n_0\,
      S(2) => \i___169_carry_i_5_n_0\,
      S(1) => \i___169_carry_i_6_n_0\,
      S(0) => \i___169_carry_i_7_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___169_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_0\,
      CO(3) => \NLW_string_cent_decenas[1]5_inferred__0/i___169_carry__0_CO_UNCONNECTED\(3),
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => cent(6 downto 4),
      O(3) => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_4\,
      O(2) => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_7\,
      S(3) => \i___169_carry__0_i_1_n_0\,
      S(2) => \i___169_carry__0_i_2_n_0\,
      S(1) => \i___169_carry__0_i_3_n_0\,
      S(0) => \i___169_carry__0_i_4_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___27_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_0\,
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_3\,
      CYINIT => '0',
      DI(3) => \i___27_carry_i_1_n_0\,
      DI(2) => \i___27_carry_i_2_n_0\,
      DI(1 downto 0) => B"01",
      O(3) => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_4\,
      O(2) => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_6\,
      O(0) => \NLW_string_cent_decenas[1]5_inferred__0/i___27_carry_O_UNCONNECTED\(0),
      S(3) => \i___27_carry_i_3_n_0\,
      S(2) => \i___27_carry_i_4_n_0\,
      S(1) => \i___27_carry_i_5_n_0\,
      S(0) => \i___27_carry_i_6_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___27_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_0\,
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \i___27_carry__0_i_1_n_0\,
      DI(2) => \i___27_carry__0_i_2_n_0\,
      DI(1) => \i___27_carry__0_i_3_n_0\,
      DI(0) => \i___27_carry__0_i_4_n_0\,
      O(3) => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_4\,
      O(2) => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_7\,
      S(3) => \i___27_carry__0_i_5_n_0\,
      S(2) => \i___27_carry__0_i_6_n_0\,
      S(1) => \i___27_carry__0_i_7_n_0\,
      S(0) => \i___27_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___27_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      CO(2) => \NLW_string_cent_decenas[1]5_inferred__0/i___27_carry__1_CO_UNCONNECTED\(2),
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"01",
      DI(1) => \i___27_carry__1_i_1_n_0\,
      DI(0) => \i___27_carry__1_i_2_n_0\,
      O(3) => \NLW_string_cent_decenas[1]5_inferred__0/i___27_carry__1_O_UNCONNECTED\(3),
      O(2) => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_7\,
      S(3) => '1',
      S(2) => \i___27_carry__1_i_3_n_0\,
      S(1) => \i___27_carry__1_i_4_n_0\,
      S(0) => \i___27_carry__1_i_5_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___56_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_0\,
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_3\,
      CYINIT => '0',
      DI(3) => \i___56_carry_i_1_n_0\,
      DI(2 downto 0) => B"001",
      O(3) => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_4\,
      O(2) => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_6\,
      O(0) => \NLW_string_cent_decenas[1]5_inferred__0/i___56_carry_O_UNCONNECTED\(0),
      S(3) => \i___56_carry_i_2_n_0\,
      S(2) => \i___56_carry_i_3_n_0\,
      S(1) => \i___56_carry_i_4_n_0\,
      S(0) => \i___56_carry_i_5_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___56_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_0\,
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => cent(4 downto 3),
      DI(1) => \i___56_carry__0_i_1_n_0\,
      DI(0) => \i___56_carry__0_i_2_n_0\,
      O(3) => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_4\,
      O(2) => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_7\,
      S(3) => \i___56_carry__0_i_3_n_0\,
      S(2) => \i___56_carry__0_i_4_n_0\,
      S(1) => \i___56_carry__0_i_5_n_0\,
      S(0) => \i___56_carry__0_i_6_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___56_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_0\,
      CO(3) => \NLW_string_cent_decenas[1]5_inferred__0/i___56_carry__1_CO_UNCONNECTED\(3),
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      CO(1) => \NLW_string_cent_decenas[1]5_inferred__0/i___56_carry__1_CO_UNCONNECTED\(1),
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1 downto 0) => cent(6 downto 5),
      O(3 downto 2) => \NLW_string_cent_decenas[1]5_inferred__0/i___56_carry__1_O_UNCONNECTED\(3 downto 2),
      O(1) => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_7\,
      S(3 downto 2) => B"01",
      S(1) => \i___56_carry__1_i_1_n_0\,
      S(0) => \i___56_carry__1_i_2_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___80_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_0\,
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => cent(6 downto 4),
      DI(0) => '0',
      O(3) => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_4\,
      O(2) => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_7\,
      S(3) => \i___80_carry_i_1_n_0\,
      S(2) => \i___80_carry_i_2_n_0\,
      S(1) => \i___80_carry_i_3_n_0\,
      S(0) => \i___80_carry_i_4_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___80_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_0\,
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \i___80_carry__0_i_1_n_0\,
      DI(2) => \i___80_carry__0_i_2_n_0\,
      DI(1) => \i___80_carry__0_i_3_n_0\,
      DI(0) => \i___80_carry__0_i_4_n_0\,
      O(3) => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_4\,
      O(2) => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_7\,
      S(3) => \i___80_carry__0_i_5_n_0\,
      S(2) => \i___80_carry__0_i_6_n_0\,
      S(1) => \i___80_carry__0_i_7_n_0\,
      S(0) => \i___80_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___80_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_0\,
      CO(3) => \NLW_string_cent_decenas[1]5_inferred__0/i___80_carry__1_CO_UNCONNECTED\(3),
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_1\,
      CO(1) => \NLW_string_cent_decenas[1]5_inferred__0/i___80_carry__1_CO_UNCONNECTED\(1),
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \i___80_carry__1_i_1_n_0\,
      O(3 downto 2) => \NLW_string_cent_decenas[1]5_inferred__0/i___80_carry__1_O_UNCONNECTED\(3 downto 2),
      O(1) => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_7\,
      S(3 downto 2) => B"01",
      S(1) => \i___80_carry__1_i_2_n_0\,
      S(0) => \i___80_carry__1_i_3_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gestion_dinero is
  port (
    tot_OBUF : out STD_LOGIC_VECTOR ( 7 downto 0 );
    clr_dinero_OBUF : out STD_LOGIC;
    \string_cent_decenas[1]5__185_carry\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    st_dinero_OBUF : out STD_LOGIC_VECTOR ( 1 downto 0 );
    devolver_OBUF : out STD_LOGIC;
    \total_reg[26]_0\ : out STD_LOGIC;
    \disp_dinero[3]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    bdf_din_OBUF : out STD_LOGIC;
    \disp[5]_OBUF\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp[4]_OBUF\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp[3]_OBUF\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp[2]_OBUF\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \current_state_reg[0]\ : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    clk_IBUF_BUFG : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 );
    v_mon_OBUF : in STD_LOGIC_VECTOR ( 7 downto 0 );
    valor_moneda_IBUF : in STD_LOGIC_VECTOR ( 22 downto 0 );
    button_ok_IBUF : in STD_LOGIC;
    \disp_refresco[8]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_refresco[4]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_refresco[5]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_refresco[7]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_refresco[3]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_refresco[2]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    clr_dinero_r : in STD_LOGIC;
    \current_state_reg[0]_0\ : in STD_LOGIC;
    \current_state_reg[0]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end gestion_dinero;

architecture STRUCTURE of gestion_dinero is
  signal \FSM_sequential_next_state_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_10_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_11_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_12_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_13_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_14_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_5_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_7_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_8_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_9_n_0\ : STD_LOGIC;
  signal \^bdf_din_obuf\ : STD_LOGIC;
  signal \^clr_dinero_obuf\ : STD_LOGIC;
  signal current_state : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \next_state__0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^st_dinero_obuf\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \tot[0]_i_3_n_0\ : STD_LOGIC;
  signal \tot[0]_i_4_n_0\ : STD_LOGIC;
  signal \tot[0]_i_5_n_0\ : STD_LOGIC;
  signal \tot[0]_i_6_n_0\ : STD_LOGIC;
  signal \tot[4]_i_3_n_0\ : STD_LOGIC;
  signal \tot[4]_i_4_n_0\ : STD_LOGIC;
  signal \tot[4]_i_5_n_0\ : STD_LOGIC;
  signal \tot[4]_i_6_n_0\ : STD_LOGIC;
  signal \^tot_obuf\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \tot_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \tot_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \tot_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \tot_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \tot_reg[4]_i_2_n_1\ : STD_LOGIC;
  signal \tot_reg[4]_i_2_n_2\ : STD_LOGIC;
  signal \tot_reg[4]_i_2_n_3\ : STD_LOGIC;
  signal total0 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \total[0]_i_10_n_0\ : STD_LOGIC;
  signal \total[0]_i_1_n_0\ : STD_LOGIC;
  signal \total[0]_i_3_n_0\ : STD_LOGIC;
  signal \total[0]_i_4_n_0\ : STD_LOGIC;
  signal \total[0]_i_5_n_0\ : STD_LOGIC;
  signal \total[0]_i_6_n_0\ : STD_LOGIC;
  signal \total[0]_i_7_n_0\ : STD_LOGIC;
  signal \total[0]_i_8_n_0\ : STD_LOGIC;
  signal \total[0]_i_9_n_0\ : STD_LOGIC;
  signal \total[12]_i_2_n_0\ : STD_LOGIC;
  signal \total[12]_i_3_n_0\ : STD_LOGIC;
  signal \total[12]_i_4_n_0\ : STD_LOGIC;
  signal \total[12]_i_5_n_0\ : STD_LOGIC;
  signal \total[12]_i_6_n_0\ : STD_LOGIC;
  signal \total[12]_i_7_n_0\ : STD_LOGIC;
  signal \total[12]_i_8_n_0\ : STD_LOGIC;
  signal \total[12]_i_9_n_0\ : STD_LOGIC;
  signal \total[16]_i_2_n_0\ : STD_LOGIC;
  signal \total[16]_i_3_n_0\ : STD_LOGIC;
  signal \total[16]_i_4_n_0\ : STD_LOGIC;
  signal \total[16]_i_5_n_0\ : STD_LOGIC;
  signal \total[16]_i_6_n_0\ : STD_LOGIC;
  signal \total[16]_i_7_n_0\ : STD_LOGIC;
  signal \total[16]_i_8_n_0\ : STD_LOGIC;
  signal \total[16]_i_9_n_0\ : STD_LOGIC;
  signal \total[20]_i_2_n_0\ : STD_LOGIC;
  signal \total[20]_i_3_n_0\ : STD_LOGIC;
  signal \total[20]_i_4_n_0\ : STD_LOGIC;
  signal \total[20]_i_5_n_0\ : STD_LOGIC;
  signal \total[20]_i_6_n_0\ : STD_LOGIC;
  signal \total[20]_i_7_n_0\ : STD_LOGIC;
  signal \total[20]_i_8_n_0\ : STD_LOGIC;
  signal \total[20]_i_9_n_0\ : STD_LOGIC;
  signal \total[24]_i_2_n_0\ : STD_LOGIC;
  signal \total[24]_i_3_n_0\ : STD_LOGIC;
  signal \total[24]_i_4_n_0\ : STD_LOGIC;
  signal \total[24]_i_5_n_0\ : STD_LOGIC;
  signal \total[24]_i_6_n_0\ : STD_LOGIC;
  signal \total[24]_i_7_n_0\ : STD_LOGIC;
  signal \total[24]_i_8_n_0\ : STD_LOGIC;
  signal \total[24]_i_9_n_0\ : STD_LOGIC;
  signal \total[28]_i_2_n_0\ : STD_LOGIC;
  signal \total[28]_i_3_n_0\ : STD_LOGIC;
  signal \total[28]_i_4_n_0\ : STD_LOGIC;
  signal \total[28]_i_5_n_0\ : STD_LOGIC;
  signal \total[28]_i_6_n_0\ : STD_LOGIC;
  signal \total[4]_i_2_n_0\ : STD_LOGIC;
  signal \total[4]_i_3_n_0\ : STD_LOGIC;
  signal \total[4]_i_4_n_0\ : STD_LOGIC;
  signal \total[4]_i_5_n_0\ : STD_LOGIC;
  signal \total[4]_i_6_n_0\ : STD_LOGIC;
  signal \total[4]_i_7_n_0\ : STD_LOGIC;
  signal \total[4]_i_8_n_0\ : STD_LOGIC;
  signal \total[4]_i_9_n_0\ : STD_LOGIC;
  signal \total[8]_i_2_n_0\ : STD_LOGIC;
  signal \total[8]_i_3_n_0\ : STD_LOGIC;
  signal \total[8]_i_4_n_0\ : STD_LOGIC;
  signal \total[8]_i_5_n_0\ : STD_LOGIC;
  signal \total[8]_i_6_n_0\ : STD_LOGIC;
  signal \total[8]_i_7_n_0\ : STD_LOGIC;
  signal \total[8]_i_8_n_0\ : STD_LOGIC;
  signal \total[8]_i_9_n_0\ : STD_LOGIC;
  signal total_aux : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal total_reg : STD_LOGIC_VECTOR ( 30 downto 0 );
  signal \total_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \total_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \total_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \total_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \total_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \total_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \total_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \total_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \total_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \total_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \total_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \total_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \total_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \total_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \total_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \total_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \total_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \total_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \total_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \total_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \total_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \total_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \total_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \total_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \total_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \total_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \total_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \total_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \total_reg[20]_i_1_n_4\ : STD_LOGIC;
  signal \total_reg[20]_i_1_n_5\ : STD_LOGIC;
  signal \total_reg[20]_i_1_n_6\ : STD_LOGIC;
  signal \total_reg[20]_i_1_n_7\ : STD_LOGIC;
  signal \total_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \total_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \total_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \total_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \total_reg[24]_i_1_n_4\ : STD_LOGIC;
  signal \total_reg[24]_i_1_n_5\ : STD_LOGIC;
  signal \total_reg[24]_i_1_n_6\ : STD_LOGIC;
  signal \total_reg[24]_i_1_n_7\ : STD_LOGIC;
  signal \^total_reg[26]_0\ : STD_LOGIC;
  signal \total_reg[28]_i_1_n_2\ : STD_LOGIC;
  signal \total_reg[28]_i_1_n_3\ : STD_LOGIC;
  signal \total_reg[28]_i_1_n_5\ : STD_LOGIC;
  signal \total_reg[28]_i_1_n_6\ : STD_LOGIC;
  signal \total_reg[28]_i_1_n_7\ : STD_LOGIC;
  signal \total_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \total_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \total_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \total_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \total_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \total_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \total_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \total_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \total_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \total_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \total_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \total_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \total_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \total_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \total_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \total_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal \NLW_tot_reg[4]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_total_reg[28]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_total_reg[28]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_current_state_reg[0]\ : label is "s2:11,s1:01,s0:00,s3:10";
  attribute FSM_ENCODED_STATES of \FSM_sequential_current_state_reg[1]\ : label is "s2:11,s1:01,s0:00,s3:10";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \FSM_sequential_next_state_reg[0]\ : label is "LD";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_next_state_reg[0]_i_1\ : label is "soft_lutpair31";
  attribute XILINX_LEGACY_PRIM of \FSM_sequential_next_state_reg[1]\ : label is "LD";
  attribute SOFT_HLUTNM of \FSM_sequential_next_state_reg[1]_i_2\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of bdf_din_OBUF_inst_i_1 : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of devolver_OBUF_inst_i_1 : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \disp[4][1]_INST_0_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \disp[5][4]_INST_0_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of refresco_OBUF_inst_i_1 : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \tot[0]_i_1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \tot[5]_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \tot[6]_i_1\ : label is "soft_lutpair31";
begin
  bdf_din_OBUF <= \^bdf_din_obuf\;
  clr_dinero_OBUF <= \^clr_dinero_obuf\;
  st_dinero_OBUF(1 downto 0) <= \^st_dinero_obuf\(1 downto 0);
  tot_OBUF(7 downto 0) <= \^tot_obuf\(7 downto 0);
  \total_reg[26]_0\ <= \^total_reg[26]_0\;
\FSM_sequential_current_state_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => AR(0),
      D => \next_state__0\(0),
      Q => current_state(0)
    );
\FSM_sequential_current_state_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => AR(0),
      D => \next_state__0\(1),
      Q => \^st_dinero_obuf\(1)
    );
\FSM_sequential_next_state_reg[0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \FSM_sequential_next_state_reg[0]_i_1_n_0\,
      G => \FSM_sequential_next_state_reg[1]_i_2_n_0\,
      GE => '1',
      Q => \next_state__0\(0)
    );
\FSM_sequential_next_state_reg[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^st_dinero_obuf\(1),
      I1 => current_state(0),
      O => \FSM_sequential_next_state_reg[0]_i_1_n_0\
    );
\FSM_sequential_next_state_reg[1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \FSM_sequential_next_state_reg[1]_i_1_n_0\,
      G => \FSM_sequential_next_state_reg[1]_i_2_n_0\,
      GE => '1',
      Q => \next_state__0\(1)
    );
\FSM_sequential_next_state_reg[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4545454545004545"
    )
        port map (
      I0 => \^st_dinero_obuf\(1),
      I1 => button_ok_IBUF,
      I2 => current_state(0),
      I3 => \FSM_sequential_next_state_reg[1]_i_3_n_0\,
      I4 => \FSM_sequential_next_state_reg[1]_i_4_n_0\,
      I5 => \FSM_sequential_next_state_reg[1]_i_5_n_0\,
      O => \FSM_sequential_next_state_reg[1]_i_1_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => total_reg(10),
      I1 => total_reg(8),
      O => \FSM_sequential_next_state_reg[1]_i_10_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => total_reg(15),
      I1 => total_reg(13),
      O => \FSM_sequential_next_state_reg[1]_i_11_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_12\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => total_reg(11),
      I1 => total_reg(9),
      O => \FSM_sequential_next_state_reg[1]_i_12_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_13\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => total_reg(14),
      I1 => total_reg(12),
      O => \FSM_sequential_next_state_reg[1]_i_13_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => total_reg(3),
      I1 => total_reg(4),
      I2 => total_reg(0),
      I3 => total_reg(1),
      O => \FSM_sequential_next_state_reg[1]_i_14_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AAA3"
    )
        port map (
      I0 => button_ok_IBUF,
      I1 => \^total_reg[26]_0\,
      I2 => \^st_dinero_obuf\(1),
      I3 => current_state(0),
      O => \FSM_sequential_next_state_reg[1]_i_2_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \FSM_sequential_next_state_reg[1]_i_6_n_0\,
      I1 => total_reg(24),
      I2 => current_state(0),
      I3 => total_reg(28),
      I4 => total_reg(29),
      I5 => \FSM_sequential_next_state_reg[1]_i_7_n_0\,
      O => \FSM_sequential_next_state_reg[1]_i_3_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000001000"
    )
        port map (
      I0 => total_reg(22),
      I1 => total_reg(20),
      I2 => \FSM_sequential_next_state_reg[1]_i_8_n_0\,
      I3 => \FSM_sequential_next_state_reg[1]_i_9_n_0\,
      I4 => total_reg(18),
      I5 => total_reg(16),
      O => \FSM_sequential_next_state_reg[1]_i_4_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFF7FFF"
    )
        port map (
      I0 => \FSM_sequential_next_state_reg[1]_i_10_n_0\,
      I1 => \FSM_sequential_next_state_reg[1]_i_11_n_0\,
      I2 => \FSM_sequential_next_state_reg[1]_i_12_n_0\,
      I3 => \FSM_sequential_next_state_reg[1]_i_13_n_0\,
      I4 => total_reg(7),
      I5 => total_reg(30),
      O => \FSM_sequential_next_state_reg[1]_i_5_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => total_reg(27),
      I1 => total_reg(25),
      I2 => total_reg(26),
      O => \FSM_sequential_next_state_reg[1]_i_6_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BFFF"
    )
        port map (
      I0 => \FSM_sequential_next_state_reg[1]_i_14_n_0\,
      I1 => total_reg(5),
      I2 => total_reg(6),
      I3 => total_reg(2),
      O => \FSM_sequential_next_state_reg[1]_i_7_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => total_reg(19),
      I1 => total_reg(17),
      O => \FSM_sequential_next_state_reg[1]_i_8_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => total_reg(23),
      I1 => total_reg(21),
      O => \FSM_sequential_next_state_reg[1]_i_9_n_0\
    );
bdf_din_OBUF_inst_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^st_dinero_obuf\(1),
      I1 => current_state(0),
      O => \^bdf_din_obuf\
    );
clr_dinero_OBUF_inst_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => Q(1),
      I1 => clr_dinero_r,
      O => \^clr_dinero_obuf\
    );
devolver_OBUF_inst_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^st_dinero_obuf\(1),
      I1 => current_state(0),
      O => devolver_OBUF
    );
\disp[4][1]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7C4C"
    )
        port map (
      I0 => \^total_reg[26]_0\,
      I1 => Q(1),
      I2 => Q(0),
      I3 => \disp_refresco[4]\(0),
      O => \disp[4]_OBUF\(0)
    );
\disp[4][2]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => \^total_reg[26]_0\,
      I1 => Q(1),
      I2 => Q(0),
      O => \disp[4]_OBUF\(1)
    );
\disp[5][4]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"50CF"
    )
        port map (
      I0 => \^total_reg[26]_0\,
      I1 => \disp_refresco[8]\(0),
      I2 => Q(0),
      I3 => Q(1),
      O => \disp[5]_OBUF\(1)
    );
inst_int_to_string: entity work.int_to_string
     port map (
      Q(1 downto 0) => Q(1 downto 0),
      \current_state_reg[0]\ => \current_state_reg[0]\,
      \disp[2]_OBUF\(2 downto 0) => \disp[2]_OBUF\(2 downto 0),
      \disp[3]_OBUF\(1 downto 0) => \disp[3]_OBUF\(1 downto 0),
      \disp[5]_OBUF\(0) => \disp[5]_OBUF\(0),
      \disp_dinero[3]\(0) => \disp_dinero[3]\(0),
      \disp_refresco[2]\(1 downto 0) => \disp_refresco[2]\(1 downto 0),
      \disp_refresco[3]\(0) => \disp_refresco[3]\(0),
      \disp_refresco[4]\(0) => \disp_refresco[4]\(1),
      \disp_refresco[5]\(0) => \disp_refresco[5]\(0),
      \disp_refresco[7]\(0) => \disp_refresco[7]\(0),
      \string_cent_decenas[1]5__185_carry_0\ => \string_cent_decenas[1]5__185_carry\(0),
      total_reg(30 downto 0) => total_reg(30 downto 0),
      total_reg_26_sp_1 => \^total_reg[26]_0\
    );
\next_state_reg[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FB73D951EA62C840"
    )
        port map (
      I0 => Q(0),
      I1 => Q(1),
      I2 => \current_state_reg[0]_0\,
      I3 => \^bdf_din_obuf\,
      I4 => button_ok_IBUF,
      I5 => \current_state_reg[0]_1\(0),
      O => E(0)
    );
refresco_OBUF_inst_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^st_dinero_obuf\(1),
      I1 => current_state(0),
      O => \^st_dinero_obuf\(0)
    );
\tot[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE02"
    )
        port map (
      I0 => total0(0),
      I1 => \^st_dinero_obuf\(1),
      I2 => current_state(0),
      I3 => \^tot_obuf\(0),
      O => total_aux(0)
    );
\tot[0]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(3),
      I1 => v_mon_OBUF(3),
      O => \tot[0]_i_3_n_0\
    );
\tot[0]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(2),
      I1 => v_mon_OBUF(2),
      O => \tot[0]_i_4_n_0\
    );
\tot[0]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(1),
      I1 => v_mon_OBUF(1),
      O => \tot[0]_i_5_n_0\
    );
\tot[0]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(0),
      I1 => v_mon_OBUF(0),
      O => \tot[0]_i_6_n_0\
    );
\tot[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE02"
    )
        port map (
      I0 => total0(1),
      I1 => \^st_dinero_obuf\(1),
      I2 => current_state(0),
      I3 => \^tot_obuf\(1),
      O => total_aux(1)
    );
\tot[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE02"
    )
        port map (
      I0 => total0(2),
      I1 => \^st_dinero_obuf\(1),
      I2 => current_state(0),
      I3 => \^tot_obuf\(2),
      O => total_aux(2)
    );
\tot[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE02"
    )
        port map (
      I0 => total0(3),
      I1 => \^st_dinero_obuf\(1),
      I2 => current_state(0),
      I3 => \^tot_obuf\(3),
      O => total_aux(3)
    );
\tot[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE02"
    )
        port map (
      I0 => total0(4),
      I1 => \^st_dinero_obuf\(1),
      I2 => current_state(0),
      I3 => \^tot_obuf\(4),
      O => total_aux(4)
    );
\tot[4]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => v_mon_OBUF(7),
      I1 => total_reg(7),
      O => \tot[4]_i_3_n_0\
    );
\tot[4]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(6),
      I1 => v_mon_OBUF(6),
      O => \tot[4]_i_4_n_0\
    );
\tot[4]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(5),
      I1 => v_mon_OBUF(5),
      O => \tot[4]_i_5_n_0\
    );
\tot[4]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(4),
      I1 => v_mon_OBUF(4),
      O => \tot[4]_i_6_n_0\
    );
\tot[5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE02"
    )
        port map (
      I0 => total0(5),
      I1 => \^st_dinero_obuf\(1),
      I2 => current_state(0),
      I3 => \^tot_obuf\(5),
      O => total_aux(5)
    );
\tot[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE02"
    )
        port map (
      I0 => total0(6),
      I1 => \^st_dinero_obuf\(1),
      I2 => current_state(0),
      I3 => \^tot_obuf\(6),
      O => total_aux(6)
    );
\tot[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE02"
    )
        port map (
      I0 => total0(7),
      I1 => \^st_dinero_obuf\(1),
      I2 => current_state(0),
      I3 => \^tot_obuf\(7),
      O => total_aux(7)
    );
\tot_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \^clr_dinero_obuf\,
      D => total_aux(0),
      Q => \^tot_obuf\(0),
      R => '0'
    );
\tot_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \tot_reg[0]_i_2_n_0\,
      CO(2) => \tot_reg[0]_i_2_n_1\,
      CO(1) => \tot_reg[0]_i_2_n_2\,
      CO(0) => \tot_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => total_reg(3 downto 0),
      O(3 downto 0) => total0(3 downto 0),
      S(3) => \tot[0]_i_3_n_0\,
      S(2) => \tot[0]_i_4_n_0\,
      S(1) => \tot[0]_i_5_n_0\,
      S(0) => \tot[0]_i_6_n_0\
    );
\tot_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \^clr_dinero_obuf\,
      D => total_aux(1),
      Q => \^tot_obuf\(1),
      R => '0'
    );
\tot_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \^clr_dinero_obuf\,
      D => total_aux(2),
      Q => \^tot_obuf\(2),
      R => '0'
    );
\tot_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \^clr_dinero_obuf\,
      D => total_aux(3),
      Q => \^tot_obuf\(3),
      R => '0'
    );
\tot_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \^clr_dinero_obuf\,
      D => total_aux(4),
      Q => \^tot_obuf\(4),
      R => '0'
    );
\tot_reg[4]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \tot_reg[0]_i_2_n_0\,
      CO(3) => \NLW_tot_reg[4]_i_2_CO_UNCONNECTED\(3),
      CO(2) => \tot_reg[4]_i_2_n_1\,
      CO(1) => \tot_reg[4]_i_2_n_2\,
      CO(0) => \tot_reg[4]_i_2_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => total_reg(6 downto 4),
      O(3 downto 0) => total0(7 downto 4),
      S(3) => \tot[4]_i_3_n_0\,
      S(2) => \tot[4]_i_4_n_0\,
      S(1) => \tot[4]_i_5_n_0\,
      S(0) => \tot[4]_i_6_n_0\
    );
\tot_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \^clr_dinero_obuf\,
      D => total_aux(5),
      Q => \^tot_obuf\(5),
      R => '0'
    );
\tot_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \^clr_dinero_obuf\,
      D => total_aux(6),
      Q => \^tot_obuf\(6),
      R => '0'
    );
\tot_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \^clr_dinero_obuf\,
      D => total_aux(7),
      Q => \^tot_obuf\(7),
      R => '0'
    );
\total[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \^st_dinero_obuf\(1),
      I1 => current_state(0),
      O => \total[0]_i_1_n_0\
    );
\total[0]_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => v_mon_OBUF(0),
      I1 => \^st_dinero_obuf\(1),
      I2 => total_reg(0),
      O => \total[0]_i_10_n_0\
    );
\total[0]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => v_mon_OBUF(3),
      I1 => \^st_dinero_obuf\(1),
      O => \total[0]_i_3_n_0\
    );
\total[0]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => v_mon_OBUF(2),
      I1 => \^st_dinero_obuf\(1),
      O => \total[0]_i_4_n_0\
    );
\total[0]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => v_mon_OBUF(1),
      I1 => \^st_dinero_obuf\(1),
      O => \total[0]_i_5_n_0\
    );
\total[0]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => v_mon_OBUF(0),
      I1 => \^st_dinero_obuf\(1),
      O => \total[0]_i_6_n_0\
    );
\total[0]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => v_mon_OBUF(3),
      I1 => \^st_dinero_obuf\(1),
      I2 => total_reg(3),
      O => \total[0]_i_7_n_0\
    );
\total[0]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => v_mon_OBUF(2),
      I1 => \^st_dinero_obuf\(1),
      I2 => total_reg(2),
      O => \total[0]_i_8_n_0\
    );
\total[0]_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => v_mon_OBUF(1),
      I1 => \^st_dinero_obuf\(1),
      I2 => total_reg(1),
      O => \total[0]_i_9_n_0\
    );
\total[12]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => valor_moneda_IBUF(7),
      I1 => \^st_dinero_obuf\(1),
      O => \total[12]_i_2_n_0\
    );
\total[12]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => valor_moneda_IBUF(6),
      I1 => \^st_dinero_obuf\(1),
      O => \total[12]_i_3_n_0\
    );
\total[12]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => valor_moneda_IBUF(5),
      I1 => \^st_dinero_obuf\(1),
      O => \total[12]_i_4_n_0\
    );
\total[12]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => valor_moneda_IBUF(4),
      I1 => \^st_dinero_obuf\(1),
      O => \total[12]_i_5_n_0\
    );
\total[12]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => valor_moneda_IBUF(7),
      I1 => \^st_dinero_obuf\(1),
      I2 => total_reg(15),
      O => \total[12]_i_6_n_0\
    );
\total[12]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => valor_moneda_IBUF(6),
      I1 => \^st_dinero_obuf\(1),
      I2 => total_reg(14),
      O => \total[12]_i_7_n_0\
    );
\total[12]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => valor_moneda_IBUF(5),
      I1 => \^st_dinero_obuf\(1),
      I2 => total_reg(13),
      O => \total[12]_i_8_n_0\
    );
\total[12]_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => valor_moneda_IBUF(4),
      I1 => \^st_dinero_obuf\(1),
      I2 => total_reg(12),
      O => \total[12]_i_9_n_0\
    );
\total[16]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => valor_moneda_IBUF(11),
      I1 => \^st_dinero_obuf\(1),
      O => \total[16]_i_2_n_0\
    );
\total[16]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => valor_moneda_IBUF(10),
      I1 => \^st_dinero_obuf\(1),
      O => \total[16]_i_3_n_0\
    );
\total[16]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => valor_moneda_IBUF(9),
      I1 => \^st_dinero_obuf\(1),
      O => \total[16]_i_4_n_0\
    );
\total[16]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => valor_moneda_IBUF(8),
      I1 => \^st_dinero_obuf\(1),
      O => \total[16]_i_5_n_0\
    );
\total[16]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => valor_moneda_IBUF(11),
      I1 => \^st_dinero_obuf\(1),
      I2 => total_reg(19),
      O => \total[16]_i_6_n_0\
    );
\total[16]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => valor_moneda_IBUF(10),
      I1 => \^st_dinero_obuf\(1),
      I2 => total_reg(18),
      O => \total[16]_i_7_n_0\
    );
\total[16]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => valor_moneda_IBUF(9),
      I1 => \^st_dinero_obuf\(1),
      I2 => total_reg(17),
      O => \total[16]_i_8_n_0\
    );
\total[16]_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => valor_moneda_IBUF(8),
      I1 => \^st_dinero_obuf\(1),
      I2 => total_reg(16),
      O => \total[16]_i_9_n_0\
    );
\total[20]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => valor_moneda_IBUF(15),
      I1 => \^st_dinero_obuf\(1),
      O => \total[20]_i_2_n_0\
    );
\total[20]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => valor_moneda_IBUF(14),
      I1 => \^st_dinero_obuf\(1),
      O => \total[20]_i_3_n_0\
    );
\total[20]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => valor_moneda_IBUF(13),
      I1 => \^st_dinero_obuf\(1),
      O => \total[20]_i_4_n_0\
    );
\total[20]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => valor_moneda_IBUF(12),
      I1 => \^st_dinero_obuf\(1),
      O => \total[20]_i_5_n_0\
    );
\total[20]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => valor_moneda_IBUF(15),
      I1 => \^st_dinero_obuf\(1),
      I2 => total_reg(23),
      O => \total[20]_i_6_n_0\
    );
\total[20]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => valor_moneda_IBUF(14),
      I1 => \^st_dinero_obuf\(1),
      I2 => total_reg(22),
      O => \total[20]_i_7_n_0\
    );
\total[20]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => valor_moneda_IBUF(13),
      I1 => \^st_dinero_obuf\(1),
      I2 => total_reg(21),
      O => \total[20]_i_8_n_0\
    );
\total[20]_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => valor_moneda_IBUF(12),
      I1 => \^st_dinero_obuf\(1),
      I2 => total_reg(20),
      O => \total[20]_i_9_n_0\
    );
\total[24]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => valor_moneda_IBUF(19),
      I1 => \^st_dinero_obuf\(1),
      O => \total[24]_i_2_n_0\
    );
\total[24]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => valor_moneda_IBUF(18),
      I1 => \^st_dinero_obuf\(1),
      O => \total[24]_i_3_n_0\
    );
\total[24]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => valor_moneda_IBUF(17),
      I1 => \^st_dinero_obuf\(1),
      O => \total[24]_i_4_n_0\
    );
\total[24]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => valor_moneda_IBUF(16),
      I1 => \^st_dinero_obuf\(1),
      O => \total[24]_i_5_n_0\
    );
\total[24]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => valor_moneda_IBUF(19),
      I1 => \^st_dinero_obuf\(1),
      I2 => total_reg(27),
      O => \total[24]_i_6_n_0\
    );
\total[24]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => valor_moneda_IBUF(18),
      I1 => \^st_dinero_obuf\(1),
      I2 => total_reg(26),
      O => \total[24]_i_7_n_0\
    );
\total[24]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => valor_moneda_IBUF(17),
      I1 => \^st_dinero_obuf\(1),
      I2 => total_reg(25),
      O => \total[24]_i_8_n_0\
    );
\total[24]_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => valor_moneda_IBUF(16),
      I1 => \^st_dinero_obuf\(1),
      I2 => total_reg(24),
      O => \total[24]_i_9_n_0\
    );
\total[28]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => valor_moneda_IBUF(21),
      I1 => \^st_dinero_obuf\(1),
      O => \total[28]_i_2_n_0\
    );
\total[28]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => valor_moneda_IBUF(20),
      I1 => \^st_dinero_obuf\(1),
      O => \total[28]_i_3_n_0\
    );
\total[28]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => total_reg(30),
      I1 => \^st_dinero_obuf\(1),
      I2 => valor_moneda_IBUF(22),
      O => \total[28]_i_4_n_0\
    );
\total[28]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => valor_moneda_IBUF(21),
      I1 => \^st_dinero_obuf\(1),
      I2 => total_reg(29),
      O => \total[28]_i_5_n_0\
    );
\total[28]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => valor_moneda_IBUF(20),
      I1 => \^st_dinero_obuf\(1),
      I2 => total_reg(28),
      O => \total[28]_i_6_n_0\
    );
\total[4]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => v_mon_OBUF(7),
      I1 => \^st_dinero_obuf\(1),
      O => \total[4]_i_2_n_0\
    );
\total[4]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => v_mon_OBUF(6),
      I1 => \^st_dinero_obuf\(1),
      O => \total[4]_i_3_n_0\
    );
\total[4]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => v_mon_OBUF(5),
      I1 => \^st_dinero_obuf\(1),
      O => \total[4]_i_4_n_0\
    );
\total[4]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => v_mon_OBUF(4),
      I1 => \^st_dinero_obuf\(1),
      O => \total[4]_i_5_n_0\
    );
\total[4]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => v_mon_OBUF(7),
      I1 => \^st_dinero_obuf\(1),
      I2 => total_reg(7),
      O => \total[4]_i_6_n_0\
    );
\total[4]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => v_mon_OBUF(6),
      I1 => \^st_dinero_obuf\(1),
      I2 => total_reg(6),
      O => \total[4]_i_7_n_0\
    );
\total[4]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => v_mon_OBUF(5),
      I1 => \^st_dinero_obuf\(1),
      I2 => total_reg(5),
      O => \total[4]_i_8_n_0\
    );
\total[4]_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => v_mon_OBUF(4),
      I1 => \^st_dinero_obuf\(1),
      I2 => total_reg(4),
      O => \total[4]_i_9_n_0\
    );
\total[8]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => valor_moneda_IBUF(3),
      I1 => \^st_dinero_obuf\(1),
      O => \total[8]_i_2_n_0\
    );
\total[8]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => valor_moneda_IBUF(2),
      I1 => \^st_dinero_obuf\(1),
      O => \total[8]_i_3_n_0\
    );
\total[8]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => valor_moneda_IBUF(1),
      I1 => \^st_dinero_obuf\(1),
      O => \total[8]_i_4_n_0\
    );
\total[8]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => valor_moneda_IBUF(0),
      I1 => \^st_dinero_obuf\(1),
      O => \total[8]_i_5_n_0\
    );
\total[8]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => valor_moneda_IBUF(3),
      I1 => \^st_dinero_obuf\(1),
      I2 => total_reg(11),
      O => \total[8]_i_6_n_0\
    );
\total[8]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => valor_moneda_IBUF(2),
      I1 => \^st_dinero_obuf\(1),
      I2 => total_reg(10),
      O => \total[8]_i_7_n_0\
    );
\total[8]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => valor_moneda_IBUF(1),
      I1 => \^st_dinero_obuf\(1),
      I2 => total_reg(9),
      O => \total[8]_i_8_n_0\
    );
\total[8]_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => valor_moneda_IBUF(0),
      I1 => \^st_dinero_obuf\(1),
      I2 => total_reg(8),
      O => \total[8]_i_9_n_0\
    );
\total_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[0]_i_1_n_0\,
      CLR => AR(0),
      D => \total_reg[0]_i_2_n_7\,
      Q => total_reg(0)
    );
\total_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \total_reg[0]_i_2_n_0\,
      CO(2) => \total_reg[0]_i_2_n_1\,
      CO(1) => \total_reg[0]_i_2_n_2\,
      CO(0) => \total_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3) => \total[0]_i_3_n_0\,
      DI(2) => \total[0]_i_4_n_0\,
      DI(1) => \total[0]_i_5_n_0\,
      DI(0) => \total[0]_i_6_n_0\,
      O(3) => \total_reg[0]_i_2_n_4\,
      O(2) => \total_reg[0]_i_2_n_5\,
      O(1) => \total_reg[0]_i_2_n_6\,
      O(0) => \total_reg[0]_i_2_n_7\,
      S(3) => \total[0]_i_7_n_0\,
      S(2) => \total[0]_i_8_n_0\,
      S(1) => \total[0]_i_9_n_0\,
      S(0) => \total[0]_i_10_n_0\
    );
\total_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[0]_i_1_n_0\,
      CLR => AR(0),
      D => \total_reg[8]_i_1_n_5\,
      Q => total_reg(10)
    );
\total_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[0]_i_1_n_0\,
      CLR => AR(0),
      D => \total_reg[8]_i_1_n_4\,
      Q => total_reg(11)
    );
\total_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[0]_i_1_n_0\,
      CLR => AR(0),
      D => \total_reg[12]_i_1_n_7\,
      Q => total_reg(12)
    );
\total_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \total_reg[8]_i_1_n_0\,
      CO(3) => \total_reg[12]_i_1_n_0\,
      CO(2) => \total_reg[12]_i_1_n_1\,
      CO(1) => \total_reg[12]_i_1_n_2\,
      CO(0) => \total_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => \total[12]_i_2_n_0\,
      DI(2) => \total[12]_i_3_n_0\,
      DI(1) => \total[12]_i_4_n_0\,
      DI(0) => \total[12]_i_5_n_0\,
      O(3) => \total_reg[12]_i_1_n_4\,
      O(2) => \total_reg[12]_i_1_n_5\,
      O(1) => \total_reg[12]_i_1_n_6\,
      O(0) => \total_reg[12]_i_1_n_7\,
      S(3) => \total[12]_i_6_n_0\,
      S(2) => \total[12]_i_7_n_0\,
      S(1) => \total[12]_i_8_n_0\,
      S(0) => \total[12]_i_9_n_0\
    );
\total_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[0]_i_1_n_0\,
      CLR => AR(0),
      D => \total_reg[12]_i_1_n_6\,
      Q => total_reg(13)
    );
\total_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[0]_i_1_n_0\,
      CLR => AR(0),
      D => \total_reg[12]_i_1_n_5\,
      Q => total_reg(14)
    );
\total_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[0]_i_1_n_0\,
      CLR => AR(0),
      D => \total_reg[12]_i_1_n_4\,
      Q => total_reg(15)
    );
\total_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[0]_i_1_n_0\,
      CLR => AR(0),
      D => \total_reg[16]_i_1_n_7\,
      Q => total_reg(16)
    );
\total_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \total_reg[12]_i_1_n_0\,
      CO(3) => \total_reg[16]_i_1_n_0\,
      CO(2) => \total_reg[16]_i_1_n_1\,
      CO(1) => \total_reg[16]_i_1_n_2\,
      CO(0) => \total_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => \total[16]_i_2_n_0\,
      DI(2) => \total[16]_i_3_n_0\,
      DI(1) => \total[16]_i_4_n_0\,
      DI(0) => \total[16]_i_5_n_0\,
      O(3) => \total_reg[16]_i_1_n_4\,
      O(2) => \total_reg[16]_i_1_n_5\,
      O(1) => \total_reg[16]_i_1_n_6\,
      O(0) => \total_reg[16]_i_1_n_7\,
      S(3) => \total[16]_i_6_n_0\,
      S(2) => \total[16]_i_7_n_0\,
      S(1) => \total[16]_i_8_n_0\,
      S(0) => \total[16]_i_9_n_0\
    );
\total_reg[17]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[0]_i_1_n_0\,
      CLR => AR(0),
      D => \total_reg[16]_i_1_n_6\,
      Q => total_reg(17)
    );
\total_reg[18]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[0]_i_1_n_0\,
      CLR => AR(0),
      D => \total_reg[16]_i_1_n_5\,
      Q => total_reg(18)
    );
\total_reg[19]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[0]_i_1_n_0\,
      CLR => AR(0),
      D => \total_reg[16]_i_1_n_4\,
      Q => total_reg(19)
    );
\total_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[0]_i_1_n_0\,
      CLR => AR(0),
      D => \total_reg[0]_i_2_n_6\,
      Q => total_reg(1)
    );
\total_reg[20]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[0]_i_1_n_0\,
      CLR => AR(0),
      D => \total_reg[20]_i_1_n_7\,
      Q => total_reg(20)
    );
\total_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \total_reg[16]_i_1_n_0\,
      CO(3) => \total_reg[20]_i_1_n_0\,
      CO(2) => \total_reg[20]_i_1_n_1\,
      CO(1) => \total_reg[20]_i_1_n_2\,
      CO(0) => \total_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => \total[20]_i_2_n_0\,
      DI(2) => \total[20]_i_3_n_0\,
      DI(1) => \total[20]_i_4_n_0\,
      DI(0) => \total[20]_i_5_n_0\,
      O(3) => \total_reg[20]_i_1_n_4\,
      O(2) => \total_reg[20]_i_1_n_5\,
      O(1) => \total_reg[20]_i_1_n_6\,
      O(0) => \total_reg[20]_i_1_n_7\,
      S(3) => \total[20]_i_6_n_0\,
      S(2) => \total[20]_i_7_n_0\,
      S(1) => \total[20]_i_8_n_0\,
      S(0) => \total[20]_i_9_n_0\
    );
\total_reg[21]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[0]_i_1_n_0\,
      CLR => AR(0),
      D => \total_reg[20]_i_1_n_6\,
      Q => total_reg(21)
    );
\total_reg[22]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[0]_i_1_n_0\,
      CLR => AR(0),
      D => \total_reg[20]_i_1_n_5\,
      Q => total_reg(22)
    );
\total_reg[23]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[0]_i_1_n_0\,
      CLR => AR(0),
      D => \total_reg[20]_i_1_n_4\,
      Q => total_reg(23)
    );
\total_reg[24]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[0]_i_1_n_0\,
      CLR => AR(0),
      D => \total_reg[24]_i_1_n_7\,
      Q => total_reg(24)
    );
\total_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \total_reg[20]_i_1_n_0\,
      CO(3) => \total_reg[24]_i_1_n_0\,
      CO(2) => \total_reg[24]_i_1_n_1\,
      CO(1) => \total_reg[24]_i_1_n_2\,
      CO(0) => \total_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => \total[24]_i_2_n_0\,
      DI(2) => \total[24]_i_3_n_0\,
      DI(1) => \total[24]_i_4_n_0\,
      DI(0) => \total[24]_i_5_n_0\,
      O(3) => \total_reg[24]_i_1_n_4\,
      O(2) => \total_reg[24]_i_1_n_5\,
      O(1) => \total_reg[24]_i_1_n_6\,
      O(0) => \total_reg[24]_i_1_n_7\,
      S(3) => \total[24]_i_6_n_0\,
      S(2) => \total[24]_i_7_n_0\,
      S(1) => \total[24]_i_8_n_0\,
      S(0) => \total[24]_i_9_n_0\
    );
\total_reg[25]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[0]_i_1_n_0\,
      CLR => AR(0),
      D => \total_reg[24]_i_1_n_6\,
      Q => total_reg(25)
    );
\total_reg[26]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[0]_i_1_n_0\,
      CLR => AR(0),
      D => \total_reg[24]_i_1_n_5\,
      Q => total_reg(26)
    );
\total_reg[27]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[0]_i_1_n_0\,
      CLR => AR(0),
      D => \total_reg[24]_i_1_n_4\,
      Q => total_reg(27)
    );
\total_reg[28]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[0]_i_1_n_0\,
      CLR => AR(0),
      D => \total_reg[28]_i_1_n_7\,
      Q => total_reg(28)
    );
\total_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \total_reg[24]_i_1_n_0\,
      CO(3 downto 2) => \NLW_total_reg[28]_i_1_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \total_reg[28]_i_1_n_2\,
      CO(0) => \total_reg[28]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \total[28]_i_2_n_0\,
      DI(0) => \total[28]_i_3_n_0\,
      O(3) => \NLW_total_reg[28]_i_1_O_UNCONNECTED\(3),
      O(2) => \total_reg[28]_i_1_n_5\,
      O(1) => \total_reg[28]_i_1_n_6\,
      O(0) => \total_reg[28]_i_1_n_7\,
      S(3) => '0',
      S(2) => \total[28]_i_4_n_0\,
      S(1) => \total[28]_i_5_n_0\,
      S(0) => \total[28]_i_6_n_0\
    );
\total_reg[29]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[0]_i_1_n_0\,
      CLR => AR(0),
      D => \total_reg[28]_i_1_n_6\,
      Q => total_reg(29)
    );
\total_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[0]_i_1_n_0\,
      CLR => AR(0),
      D => \total_reg[0]_i_2_n_5\,
      Q => total_reg(2)
    );
\total_reg[30]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[0]_i_1_n_0\,
      CLR => AR(0),
      D => \total_reg[28]_i_1_n_5\,
      Q => total_reg(30)
    );
\total_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[0]_i_1_n_0\,
      CLR => AR(0),
      D => \total_reg[0]_i_2_n_4\,
      Q => total_reg(3)
    );
\total_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[0]_i_1_n_0\,
      CLR => AR(0),
      D => \total_reg[4]_i_1_n_7\,
      Q => total_reg(4)
    );
\total_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \total_reg[0]_i_2_n_0\,
      CO(3) => \total_reg[4]_i_1_n_0\,
      CO(2) => \total_reg[4]_i_1_n_1\,
      CO(1) => \total_reg[4]_i_1_n_2\,
      CO(0) => \total_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => \total[4]_i_2_n_0\,
      DI(2) => \total[4]_i_3_n_0\,
      DI(1) => \total[4]_i_4_n_0\,
      DI(0) => \total[4]_i_5_n_0\,
      O(3) => \total_reg[4]_i_1_n_4\,
      O(2) => \total_reg[4]_i_1_n_5\,
      O(1) => \total_reg[4]_i_1_n_6\,
      O(0) => \total_reg[4]_i_1_n_7\,
      S(3) => \total[4]_i_6_n_0\,
      S(2) => \total[4]_i_7_n_0\,
      S(1) => \total[4]_i_8_n_0\,
      S(0) => \total[4]_i_9_n_0\
    );
\total_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[0]_i_1_n_0\,
      CLR => AR(0),
      D => \total_reg[4]_i_1_n_6\,
      Q => total_reg(5)
    );
\total_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[0]_i_1_n_0\,
      CLR => AR(0),
      D => \total_reg[4]_i_1_n_5\,
      Q => total_reg(6)
    );
\total_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[0]_i_1_n_0\,
      CLR => AR(0),
      D => \total_reg[4]_i_1_n_4\,
      Q => total_reg(7)
    );
\total_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[0]_i_1_n_0\,
      CLR => AR(0),
      D => \total_reg[8]_i_1_n_7\,
      Q => total_reg(8)
    );
\total_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \total_reg[4]_i_1_n_0\,
      CO(3) => \total_reg[8]_i_1_n_0\,
      CO(2) => \total_reg[8]_i_1_n_1\,
      CO(1) => \total_reg[8]_i_1_n_2\,
      CO(0) => \total_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => \total[8]_i_2_n_0\,
      DI(2) => \total[8]_i_3_n_0\,
      DI(1) => \total[8]_i_4_n_0\,
      DI(0) => \total[8]_i_5_n_0\,
      O(3) => \total_reg[8]_i_1_n_4\,
      O(2) => \total_reg[8]_i_1_n_5\,
      O(1) => \total_reg[8]_i_1_n_6\,
      O(0) => \total_reg[8]_i_1_n_7\,
      S(3) => \total[8]_i_6_n_0\,
      S(2) => \total[8]_i_7_n_0\,
      S(1) => \total[8]_i_8_n_0\,
      S(0) => \total[8]_i_9_n_0\
    );
\total_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \total[0]_i_1_n_0\,
      CLR => AR(0),
      D => \total_reg[8]_i_1_n_6\,
      Q => total_reg(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity fsm is
  port (
    RESET : in STD_LOGIC;
    clk : in STD_LOGIC;
    valor_moneda : in STD_LOGIC_VECTOR ( 30 downto 0 );
    button_ok : in STD_LOGIC;
    ref_option : in STD_LOGIC_VECTOR ( 3 downto 0 );
    devolver : out STD_LOGIC;
    refresco : out STD_LOGIC;
    \disp[8]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \disp[7]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \disp[6]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \disp[5]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \disp[4]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \disp[3]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \disp[2]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \disp[1]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    st : out STD_LOGIC_VECTOR ( 1 downto 0 );
    st_dinero : out STD_LOGIC_VECTOR ( 1 downto 0 );
    v_mon : out STD_LOGIC_VECTOR ( 7 downto 0 );
    tot : out STD_LOGIC_VECTOR ( 10 downto 0 );
    clr_dinero : inout STD_LOGIC;
    bdf_din : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of fsm : entity is true;
  attribute num_refrescos : integer;
  attribute num_refrescos of fsm : entity is 4;
  attribute width_word : integer;
  attribute width_word of fsm : entity is 8;
end fsm;

architecture STRUCTURE of fsm is
  signal \FSM_sequential_current_state_reg[1]_i_1_n_0\ : STD_LOGIC;
  signal Inst_gestion_dinero_n_13 : STD_LOGIC;
  signal Inst_gestion_dinero_n_25 : STD_LOGIC;
  signal Inst_gestion_dinero_n_26 : STD_LOGIC;
  signal RESET_IBUF : STD_LOGIC;
  signal bdf_din_OBUF : STD_LOGIC;
  signal button_ok_IBUF : STD_LOGIC;
  signal clk_IBUF : STD_LOGIC;
  signal clk_IBUF_BUFG : STD_LOGIC;
  signal clr_dinero_OBUF : STD_LOGIC;
  signal clr_dinero_r : STD_LOGIC;
  signal \current_state[1]_i_1_n_0\ : STD_LOGIC;
  signal devolver_OBUF : STD_LOGIC;
  signal \disp[1]_OBUF\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \disp[2]_OBUF\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \disp[3]_OBUF\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \disp[4]_OBUF\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \disp[5]_OBUF\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \disp[6]_OBUF\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \disp[7]_OBUF\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \disp[8]_OBUF\ : STD_LOGIC_VECTOR ( 4 downto 1 );
  signal \disp_aux[1]_0\ : STD_LOGIC;
  signal \disp_dinero[2]\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \disp_dinero[3]\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \disp_refresco[2]\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \disp_refresco[3]\ : STD_LOGIC_VECTOR ( 6 to 6 );
  signal \disp_refresco[4]\ : STD_LOGIC_VECTOR ( 6 downto 1 );
  signal \disp_refresco[5]\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \disp_refresco[7]\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \disp_refresco[8]\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal next_state : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \next_state_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \next_state_reg[1]_i_10_n_0\ : STD_LOGIC;
  signal \next_state_reg[1]_i_1_n_0\ : STD_LOGIC;
  signal \next_state_reg[1]_i_3_n_0\ : STD_LOGIC;
  signal \next_state_reg[1]_i_4_n_0\ : STD_LOGIC;
  signal \next_state_reg[1]_i_5_n_0\ : STD_LOGIC;
  signal \next_state_reg[1]_i_6_n_0\ : STD_LOGIC;
  signal \next_state_reg[1]_i_7_n_0\ : STD_LOGIC;
  signal \next_state_reg[1]_i_8_n_0\ : STD_LOGIC;
  signal \next_state_reg[1]_i_9_n_0\ : STD_LOGIC;
  signal ref_option_IBUF : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal st_OBUF : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal st_dinero_OBUF : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal tot_OBUF : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal v_mon_OBUF : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal valor_moneda_IBUF : STD_LOGIC_VECTOR ( 30 downto 8 );
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \next_state_reg[0]\ : label is "LD";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \next_state_reg[0]_i_1\ : label is "soft_lutpair57";
  attribute XILINX_LEGACY_PRIM of \next_state_reg[1]\ : label is "LD";
  attribute SOFT_HLUTNM of \next_state_reg[1]_i_1\ : label is "soft_lutpair57";
begin
\FSM_sequential_current_state_reg[1]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => clr_dinero_OBUF,
      O => \FSM_sequential_current_state_reg[1]_i_1_n_0\
    );
Inst_gestion_dinero: entity work.gestion_dinero
     port map (
      AR(0) => \FSM_sequential_current_state_reg[1]_i_1_n_0\,
      E(0) => Inst_gestion_dinero_n_26,
      Q(1 downto 0) => st_OBUF(1 downto 0),
      bdf_din_OBUF => bdf_din_OBUF,
      button_ok_IBUF => button_ok_IBUF,
      clk_IBUF_BUFG => clk_IBUF_BUFG,
      clr_dinero_OBUF => clr_dinero_OBUF,
      clr_dinero_r => clr_dinero_r,
      \current_state_reg[0]\ => Inst_gestion_dinero_n_25,
      \current_state_reg[0]_0\ => \next_state_reg[1]_i_3_n_0\,
      \current_state_reg[0]_1\(0) => \disp_aux[1]_0\,
      devolver_OBUF => devolver_OBUF,
      \disp[2]_OBUF\(2 downto 0) => \disp[2]_OBUF\(2 downto 0),
      \disp[3]_OBUF\(1 downto 0) => \disp[3]_OBUF\(3 downto 2),
      \disp[4]_OBUF\(1 downto 0) => \disp[4]_OBUF\(2 downto 1),
      \disp[5]_OBUF\(1) => \disp[5]_OBUF\(4),
      \disp[5]_OBUF\(0) => \disp[5]_OBUF\(0),
      \disp_dinero[3]\(0) => \disp_dinero[3]\(0),
      \disp_refresco[2]\(1 downto 0) => \disp_refresco[2]\(1 downto 0),
      \disp_refresco[3]\(0) => \disp_refresco[3]\(6),
      \disp_refresco[4]\(1) => \disp_refresco[4]\(6),
      \disp_refresco[4]\(0) => \disp_refresco[4]\(1),
      \disp_refresco[5]\(0) => \disp_refresco[5]\(0),
      \disp_refresco[7]\(0) => \disp_refresco[7]\(3),
      \disp_refresco[8]\(0) => \disp_refresco[8]\(2),
      st_dinero_OBUF(1 downto 0) => st_dinero_OBUF(1 downto 0),
      \string_cent_decenas[1]5__185_carry\(0) => \disp_dinero[2]\(3),
      tot_OBUF(7 downto 0) => tot_OBUF(7 downto 0),
      \total_reg[26]_0\ => Inst_gestion_dinero_n_13,
      v_mon_OBUF(7 downto 0) => v_mon_OBUF(7 downto 0),
      valor_moneda_IBUF(22 downto 0) => valor_moneda_IBUF(30 downto 8)
    );
Inst_gestion_refresco: entity work.gestion_refresco
     port map (
      E(0) => \disp_aux[1]_0\,
      Q(1 downto 0) => st_OBUF(1 downto 0),
      \disp[1][1]\ => Inst_gestion_dinero_n_13,
      \disp[1]_OBUF\(3 downto 0) => \disp[1]_OBUF\(3 downto 0),
      \disp[2][3]\(0) => \disp_dinero[2]\(3),
      \disp[2]_OBUF\(2) => \disp[2]_OBUF\(6),
      \disp[2]_OBUF\(1 downto 0) => \disp[2]_OBUF\(4 downto 3),
      \disp[3][1]\ => Inst_gestion_dinero_n_25,
      \disp[3]_OBUF\(3) => \disp[3]_OBUF\(6),
      \disp[3]_OBUF\(2) => \disp[3]_OBUF\(4),
      \disp[3]_OBUF\(1 downto 0) => \disp[3]_OBUF\(1 downto 0),
      \disp[4]_OBUF\(2 downto 1) => \disp[4]_OBUF\(4 downto 3),
      \disp[4]_OBUF\(0) => \disp[4]_OBUF\(0),
      \disp[5]_OBUF\(3) => \disp[5]_OBUF\(6),
      \disp[5]_OBUF\(2 downto 0) => \disp[5]_OBUF\(3 downto 1),
      \disp[6]_OBUF\(3 downto 0) => \disp[6]_OBUF\(3 downto 0),
      \disp[7]_OBUF\(2 downto 0) => \disp[7]_OBUF\(2 downto 0),
      \disp[8]_OBUF\(2 downto 0) => \disp[8]_OBUF\(3 downto 1),
      \disp_aux_reg[6][3]_i_2_0\(1) => \disp_refresco[4]\(6),
      \disp_aux_reg[6][3]_i_2_0\(0) => \disp_refresco[4]\(1),
      \disp_aux_reg[6][3]_i_2_1\(0) => \disp_refresco[3]\(6),
      \disp_aux_reg[6][3]_i_2_2\(0) => \disp_refresco[8]\(2),
      \disp_aux_reg[6][3]_i_2_3\(1 downto 0) => \disp_refresco[2]\(1 downto 0),
      \disp_dinero[3]\(0) => \disp_dinero[3]\(0),
      \disp_refresco[5]\(0) => \disp_refresco[5]\(0),
      \disp_refresco[7]\(0) => \disp_refresco[7]\(3),
      ref_option_IBUF(3 downto 0) => ref_option_IBUF(3 downto 0)
    );
RESET_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => RESET,
      O => RESET_IBUF
    );
bdf_din_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => bdf_din_OBUF,
      O => bdf_din
    );
button_ok_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => button_ok,
      O => button_ok_IBUF
    );
clk_IBUF_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => clk_IBUF,
      O => clk_IBUF_BUFG
    );
clk_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => clk,
      O => clk_IBUF
    );
clr_dinero_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => clr_dinero_OBUF,
      O => clr_dinero
    );
clr_dinero_r_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \current_state[1]_i_1_n_0\,
      D => '1',
      Q => clr_dinero_r
    );
\current_state[1]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => RESET_IBUF,
      O => \current_state[1]_i_1_n_0\
    );
\current_state_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \current_state[1]_i_1_n_0\,
      D => next_state(0),
      Q => st_OBUF(0)
    );
\current_state_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \current_state[1]_i_1_n_0\,
      D => next_state(1),
      Q => st_OBUF(1)
    );
devolver_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => devolver_OBUF,
      O => devolver
    );
\disp[1][0]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[1]_OBUF\(0),
      O => \disp[1]\(0)
    );
\disp[1][1]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[1]_OBUF\(1),
      O => \disp[1]\(1)
    );
\disp[1][2]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[1]_OBUF\(2),
      O => \disp[1]\(2)
    );
\disp[1][3]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[1]_OBUF\(3),
      O => \disp[1]\(3)
    );
\disp[1][4]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[1]\(4)
    );
\disp[1][5]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[1]\(5)
    );
\disp[1][6]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[1]_OBUF\(0),
      O => \disp[1]\(6)
    );
\disp[1][7]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[1]\(7)
    );
\disp[2][0]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[2]_OBUF\(0),
      O => \disp[2]\(0)
    );
\disp[2][1]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[2]_OBUF\(1),
      O => \disp[2]\(1)
    );
\disp[2][2]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[2]_OBUF\(2),
      O => \disp[2]\(2)
    );
\disp[2][3]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[2]_OBUF\(3),
      O => \disp[2]\(3)
    );
\disp[2][4]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[2]_OBUF\(4),
      O => \disp[2]\(4)
    );
\disp[2][5]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[2]\(5)
    );
\disp[2][6]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[2]_OBUF\(6),
      O => \disp[2]\(6)
    );
\disp[2][7]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[2]\(7)
    );
\disp[3][0]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[3]_OBUF\(0),
      O => \disp[3]\(0)
    );
\disp[3][1]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[3]_OBUF\(1),
      O => \disp[3]\(1)
    );
\disp[3][2]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[3]_OBUF\(2),
      O => \disp[3]\(2)
    );
\disp[3][3]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[3]_OBUF\(3),
      O => \disp[3]\(3)
    );
\disp[3][4]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[3]_OBUF\(4),
      O => \disp[3]\(4)
    );
\disp[3][5]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[3]\(5)
    );
\disp[3][6]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[3]_OBUF\(6),
      O => \disp[3]\(6)
    );
\disp[3][7]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[3]\(7)
    );
\disp[4][0]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[4]_OBUF\(0),
      O => \disp[4]\(0)
    );
\disp[4][1]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[4]_OBUF\(1),
      O => \disp[4]\(1)
    );
\disp[4][2]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[4]_OBUF\(2),
      O => \disp[4]\(2)
    );
\disp[4][3]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[4]_OBUF\(3),
      O => \disp[4]\(3)
    );
\disp[4][4]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[4]_OBUF\(4),
      O => \disp[4]\(4)
    );
\disp[4][5]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[4]\(5)
    );
\disp[4][6]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[2]_OBUF\(6),
      O => \disp[4]\(6)
    );
\disp[4][7]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[4]\(7)
    );
\disp[5][0]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[5]_OBUF\(0),
      O => \disp[5]\(0)
    );
\disp[5][1]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[5]_OBUF\(1),
      O => \disp[5]\(1)
    );
\disp[5][2]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[5]_OBUF\(2),
      O => \disp[5]\(2)
    );
\disp[5][3]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[5]_OBUF\(3),
      O => \disp[5]\(3)
    );
\disp[5][4]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[5]_OBUF\(4),
      O => \disp[5]\(4)
    );
\disp[5][5]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[5]\(5)
    );
\disp[5][6]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[5]_OBUF\(6),
      O => \disp[5]\(6)
    );
\disp[5][7]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[5]\(7)
    );
\disp[6][0]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[6]_OBUF\(0),
      O => \disp[6]\(0)
    );
\disp[6][1]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[6]_OBUF\(1),
      O => \disp[6]\(1)
    );
\disp[6][2]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[6]_OBUF\(2),
      O => \disp[6]\(2)
    );
\disp[6][3]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[6]_OBUF\(3),
      O => \disp[6]\(3)
    );
\disp[6][4]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[6]\(4)
    );
\disp[6][5]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[6]\(5)
    );
\disp[6][6]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[5]_OBUF\(6),
      O => \disp[6]\(6)
    );
\disp[6][7]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[6]\(7)
    );
\disp[7][0]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[7]_OBUF\(0),
      O => \disp[7]\(0)
    );
\disp[7][1]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[7]_OBUF\(1),
      O => \disp[7]\(1)
    );
\disp[7][2]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[7]_OBUF\(2),
      O => \disp[7]\(2)
    );
\disp[7][3]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[7]_OBUF\(1),
      O => \disp[7]\(3)
    );
\disp[7][4]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[7]\(4)
    );
\disp[7][5]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[7]\(5)
    );
\disp[7][6]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[7]_OBUF\(0),
      O => \disp[7]\(6)
    );
\disp[7][7]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[7]\(7)
    );
\disp[8][0]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[6]_OBUF\(0),
      O => \disp[8]\(0)
    );
\disp[8][1]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[8]_OBUF\(1),
      O => \disp[8]\(1)
    );
\disp[8][2]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[8]_OBUF\(2),
      O => \disp[8]\(2)
    );
\disp[8][3]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[8]_OBUF\(3),
      O => \disp[8]\(3)
    );
\disp[8][4]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[8]_OBUF\(4),
      O => \disp[8]\(4)
    );
\disp[8][4]_INST_0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => st_OBUF(1),
      I1 => st_OBUF(0),
      O => \disp[8]_OBUF\(4)
    );
\disp[8][5]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[8]\(5)
    );
\disp[8][6]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[7]_OBUF\(0),
      O => \disp[8]\(6)
    );
\disp[8][7]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[8]\(7)
    );
\next_state_reg[0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \next_state_reg[0]_i_1_n_0\,
      G => Inst_gestion_dinero_n_26,
      GE => '1',
      Q => next_state(0)
    );
\next_state_reg[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => st_OBUF(0),
      I1 => \next_state_reg[1]_i_3_n_0\,
      I2 => st_OBUF(1),
      O => \next_state_reg[0]_i_1_n_0\
    );
\next_state_reg[1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \next_state_reg[1]_i_1_n_0\,
      G => Inst_gestion_dinero_n_26,
      GE => '1',
      Q => next_state(1)
    );
\next_state_reg[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"62"
    )
        port map (
      I0 => st_OBUF(0),
      I1 => st_OBUF(1),
      I2 => \next_state_reg[1]_i_3_n_0\,
      O => \next_state_reg[1]_i_1_n_0\
    );
\next_state_reg[1]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => valor_moneda_IBUF(13),
      I1 => v_mon_OBUF(0),
      I2 => valor_moneda_IBUF(28),
      I3 => valor_moneda_IBUF(27),
      O => \next_state_reg[1]_i_10_n_0\
    );
\next_state_reg[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFFFFFFFF"
    )
        port map (
      I0 => \next_state_reg[1]_i_4_n_0\,
      I1 => valor_moneda_IBUF(26),
      I2 => v_mon_OBUF(4),
      I3 => valor_moneda_IBUF(23),
      I4 => \next_state_reg[1]_i_5_n_0\,
      I5 => \next_state_reg[1]_i_6_n_0\,
      O => \next_state_reg[1]_i_3_n_0\
    );
\next_state_reg[1]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \next_state_reg[1]_i_7_n_0\,
      I1 => valor_moneda_IBUF(24),
      I2 => valor_moneda_IBUF(17),
      I3 => valor_moneda_IBUF(15),
      I4 => v_mon_OBUF(7),
      I5 => \next_state_reg[1]_i_8_n_0\,
      O => \next_state_reg[1]_i_4_n_0\
    );
\next_state_reg[1]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => valor_moneda_IBUF(14),
      I1 => valor_moneda_IBUF(21),
      I2 => v_mon_OBUF(2),
      I3 => valor_moneda_IBUF(18),
      O => \next_state_reg[1]_i_5_n_0\
    );
\next_state_reg[1]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => valor_moneda_IBUF(19),
      I1 => v_mon_OBUF(1),
      I2 => valor_moneda_IBUF(20),
      I3 => valor_moneda_IBUF(11),
      I4 => \next_state_reg[1]_i_9_n_0\,
      O => \next_state_reg[1]_i_6_n_0\
    );
\next_state_reg[1]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => valor_moneda_IBUF(9),
      I1 => valor_moneda_IBUF(10),
      I2 => valor_moneda_IBUF(25),
      I3 => valor_moneda_IBUF(22),
      O => \next_state_reg[1]_i_7_n_0\
    );
\next_state_reg[1]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => v_mon_OBUF(6),
      I1 => valor_moneda_IBUF(8),
      I2 => valor_moneda_IBUF(16),
      I3 => valor_moneda_IBUF(12),
      I4 => \next_state_reg[1]_i_10_n_0\,
      O => \next_state_reg[1]_i_8_n_0\
    );
\next_state_reg[1]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => v_mon_OBUF(5),
      I1 => valor_moneda_IBUF(29),
      I2 => v_mon_OBUF(3),
      I3 => valor_moneda_IBUF(30),
      O => \next_state_reg[1]_i_9_n_0\
    );
\ref_option_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ref_option(0),
      O => ref_option_IBUF(0)
    );
\ref_option_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ref_option(1),
      O => ref_option_IBUF(1)
    );
\ref_option_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ref_option(2),
      O => ref_option_IBUF(2)
    );
\ref_option_IBUF[3]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ref_option(3),
      O => ref_option_IBUF(3)
    );
refresco_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => st_dinero_OBUF(0),
      O => refresco
    );
\st_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => st_OBUF(0),
      O => st(0)
    );
\st_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => st_OBUF(1),
      O => st(1)
    );
\st_dinero_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => st_dinero_OBUF(0),
      O => st_dinero(0)
    );
\st_dinero_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => st_dinero_OBUF(1),
      O => st_dinero(1)
    );
\tot_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_OBUF(0),
      O => tot(0)
    );
\tot_OBUF[10]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => tot(10)
    );
\tot_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_OBUF(1),
      O => tot(1)
    );
\tot_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_OBUF(2),
      O => tot(2)
    );
\tot_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_OBUF(3),
      O => tot(3)
    );
\tot_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_OBUF(4),
      O => tot(4)
    );
\tot_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_OBUF(5),
      O => tot(5)
    );
\tot_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_OBUF(6),
      O => tot(6)
    );
\tot_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_OBUF(7),
      O => tot(7)
    );
\tot_OBUF[8]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => tot(8)
    );
\tot_OBUF[9]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => tot(9)
    );
\v_mon_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => v_mon_OBUF(0),
      O => v_mon(0)
    );
\v_mon_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => v_mon_OBUF(1),
      O => v_mon(1)
    );
\v_mon_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => v_mon_OBUF(2),
      O => v_mon(2)
    );
\v_mon_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => v_mon_OBUF(3),
      O => v_mon(3)
    );
\v_mon_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => v_mon_OBUF(4),
      O => v_mon(4)
    );
\v_mon_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => v_mon_OBUF(5),
      O => v_mon(5)
    );
\v_mon_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => v_mon_OBUF(6),
      O => v_mon(6)
    );
\v_mon_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => v_mon_OBUF(7),
      O => v_mon(7)
    );
\valor_moneda_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(0),
      O => v_mon_OBUF(0)
    );
\valor_moneda_IBUF[10]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(10),
      O => valor_moneda_IBUF(10)
    );
\valor_moneda_IBUF[11]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(11),
      O => valor_moneda_IBUF(11)
    );
\valor_moneda_IBUF[12]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(12),
      O => valor_moneda_IBUF(12)
    );
\valor_moneda_IBUF[13]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(13),
      O => valor_moneda_IBUF(13)
    );
\valor_moneda_IBUF[14]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(14),
      O => valor_moneda_IBUF(14)
    );
\valor_moneda_IBUF[15]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(15),
      O => valor_moneda_IBUF(15)
    );
\valor_moneda_IBUF[16]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(16),
      O => valor_moneda_IBUF(16)
    );
\valor_moneda_IBUF[17]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(17),
      O => valor_moneda_IBUF(17)
    );
\valor_moneda_IBUF[18]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(18),
      O => valor_moneda_IBUF(18)
    );
\valor_moneda_IBUF[19]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(19),
      O => valor_moneda_IBUF(19)
    );
\valor_moneda_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(1),
      O => v_mon_OBUF(1)
    );
\valor_moneda_IBUF[20]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(20),
      O => valor_moneda_IBUF(20)
    );
\valor_moneda_IBUF[21]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(21),
      O => valor_moneda_IBUF(21)
    );
\valor_moneda_IBUF[22]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(22),
      O => valor_moneda_IBUF(22)
    );
\valor_moneda_IBUF[23]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(23),
      O => valor_moneda_IBUF(23)
    );
\valor_moneda_IBUF[24]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(24),
      O => valor_moneda_IBUF(24)
    );
\valor_moneda_IBUF[25]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(25),
      O => valor_moneda_IBUF(25)
    );
\valor_moneda_IBUF[26]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(26),
      O => valor_moneda_IBUF(26)
    );
\valor_moneda_IBUF[27]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(27),
      O => valor_moneda_IBUF(27)
    );
\valor_moneda_IBUF[28]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(28),
      O => valor_moneda_IBUF(28)
    );
\valor_moneda_IBUF[29]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(29),
      O => valor_moneda_IBUF(29)
    );
\valor_moneda_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(2),
      O => v_mon_OBUF(2)
    );
\valor_moneda_IBUF[30]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(30),
      O => valor_moneda_IBUF(30)
    );
\valor_moneda_IBUF[3]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(3),
      O => v_mon_OBUF(3)
    );
\valor_moneda_IBUF[4]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(4),
      O => v_mon_OBUF(4)
    );
\valor_moneda_IBUF[5]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(5),
      O => v_mon_OBUF(5)
    );
\valor_moneda_IBUF[6]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(6),
      O => v_mon_OBUF(6)
    );
\valor_moneda_IBUF[7]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(7),
      O => v_mon_OBUF(7)
    );
\valor_moneda_IBUF[8]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(8),
      O => valor_moneda_IBUF(8)
    );
\valor_moneda_IBUF[9]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(9),
      O => valor_moneda_IBUF(9)
    );
end STRUCTURE;
