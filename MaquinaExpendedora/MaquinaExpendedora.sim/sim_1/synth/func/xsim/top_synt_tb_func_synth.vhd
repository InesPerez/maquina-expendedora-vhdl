-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Thu Feb  4 19:05:41 2021
-- Host        : LAPTOP-7BL7BHFF running 64-bit major release  (build 9200)
-- Command     : write_vhdl -mode funcsim -nolib -force -file
--               C:/Users/Inees/Desktop/Trabajo_SED/MaquinaExpendedora/MaquinaExpendedora.sim/sim_1/synth/func/xsim/top_synt_tb_func_synth.vhd
-- Design      : top
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tcsg324-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity contador is
  port (
    clk_pre_reg : out STD_LOGIC;
    clk : in STD_LOGIC;
    clk_100Mh : in STD_LOGIC
  );
end contador;

architecture STRUCTURE of contador is
  signal clk_pre_i_2_n_0 : STD_LOGIC;
  signal clk_pre_i_3_n_0 : STD_LOGIC;
  signal clk_pre_i_4_n_0 : STD_LOGIC;
  signal \reg[0]_i_2_n_0\ : STD_LOGIC;
  signal reg_reg : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal \reg_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \reg_reg[0]_i_1_n_1\ : STD_LOGIC;
  signal \reg_reg[0]_i_1_n_2\ : STD_LOGIC;
  signal \reg_reg[0]_i_1_n_3\ : STD_LOGIC;
  signal \reg_reg[0]_i_1_n_4\ : STD_LOGIC;
  signal \reg_reg[0]_i_1_n_5\ : STD_LOGIC;
  signal \reg_reg[0]_i_1_n_6\ : STD_LOGIC;
  signal \reg_reg[0]_i_1_n_7\ : STD_LOGIC;
  signal \reg_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \reg_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \reg_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \reg_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \reg_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \reg_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \reg_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \reg_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \reg_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \reg_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \reg_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \reg_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \reg_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \reg_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \reg_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \reg_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \reg_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \reg_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \reg_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \reg_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \reg_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \reg_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \reg_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \reg_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \reg_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal \NLW_reg_reg[16]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_reg_reg[16]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
begin
clk_pre_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => clk_pre_i_2_n_0,
      I1 => clk_pre_i_3_n_0,
      I2 => clk_pre_i_4_n_0,
      I3 => clk,
      O => clk_pre_reg
    );
clk_pre_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0080000000000000"
    )
        port map (
      I0 => reg_reg(13),
      I1 => reg_reg(14),
      I2 => reg_reg(11),
      I3 => reg_reg(12),
      I4 => reg_reg(16),
      I5 => reg_reg(15),
      O => clk_pre_i_2_n_0
    );
clk_pre_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000080"
    )
        port map (
      I0 => reg_reg(0),
      I1 => reg_reg(1),
      I2 => reg_reg(2),
      I3 => reg_reg(4),
      I4 => reg_reg(3),
      O => clk_pre_i_3_n_0
    );
clk_pre_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000010"
    )
        port map (
      I0 => reg_reg(7),
      I1 => reg_reg(8),
      I2 => reg_reg(6),
      I3 => reg_reg(5),
      I4 => reg_reg(10),
      I5 => reg_reg(9),
      O => clk_pre_i_4_n_0
    );
\reg[0]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => reg_reg(0),
      O => \reg[0]_i_2_n_0\
    );
\reg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh,
      CE => '1',
      D => \reg_reg[0]_i_1_n_7\,
      Q => reg_reg(0),
      R => '0'
    );
\reg_reg[0]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \reg_reg[0]_i_1_n_0\,
      CO(2) => \reg_reg[0]_i_1_n_1\,
      CO(1) => \reg_reg[0]_i_1_n_2\,
      CO(0) => \reg_reg[0]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \reg_reg[0]_i_1_n_4\,
      O(2) => \reg_reg[0]_i_1_n_5\,
      O(1) => \reg_reg[0]_i_1_n_6\,
      O(0) => \reg_reg[0]_i_1_n_7\,
      S(3 downto 1) => reg_reg(3 downto 1),
      S(0) => \reg[0]_i_2_n_0\
    );
\reg_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh,
      CE => '1',
      D => \reg_reg[8]_i_1_n_5\,
      Q => reg_reg(10),
      R => '0'
    );
\reg_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh,
      CE => '1',
      D => \reg_reg[8]_i_1_n_4\,
      Q => reg_reg(11),
      R => '0'
    );
\reg_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh,
      CE => '1',
      D => \reg_reg[12]_i_1_n_7\,
      Q => reg_reg(12),
      R => '0'
    );
\reg_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \reg_reg[8]_i_1_n_0\,
      CO(3) => \reg_reg[12]_i_1_n_0\,
      CO(2) => \reg_reg[12]_i_1_n_1\,
      CO(1) => \reg_reg[12]_i_1_n_2\,
      CO(0) => \reg_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \reg_reg[12]_i_1_n_4\,
      O(2) => \reg_reg[12]_i_1_n_5\,
      O(1) => \reg_reg[12]_i_1_n_6\,
      O(0) => \reg_reg[12]_i_1_n_7\,
      S(3 downto 0) => reg_reg(15 downto 12)
    );
\reg_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh,
      CE => '1',
      D => \reg_reg[12]_i_1_n_6\,
      Q => reg_reg(13),
      R => '0'
    );
\reg_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh,
      CE => '1',
      D => \reg_reg[12]_i_1_n_5\,
      Q => reg_reg(14),
      R => '0'
    );
\reg_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh,
      CE => '1',
      D => \reg_reg[12]_i_1_n_4\,
      Q => reg_reg(15),
      R => '0'
    );
\reg_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh,
      CE => '1',
      D => \reg_reg[16]_i_1_n_7\,
      Q => reg_reg(16),
      R => '0'
    );
\reg_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \reg_reg[12]_i_1_n_0\,
      CO(3 downto 0) => \NLW_reg_reg[16]_i_1_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_reg_reg[16]_i_1_O_UNCONNECTED\(3 downto 1),
      O(0) => \reg_reg[16]_i_1_n_7\,
      S(3 downto 1) => B"000",
      S(0) => reg_reg(16)
    );
\reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh,
      CE => '1',
      D => \reg_reg[0]_i_1_n_6\,
      Q => reg_reg(1),
      R => '0'
    );
\reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh,
      CE => '1',
      D => \reg_reg[0]_i_1_n_5\,
      Q => reg_reg(2),
      R => '0'
    );
\reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh,
      CE => '1',
      D => \reg_reg[0]_i_1_n_4\,
      Q => reg_reg(3),
      R => '0'
    );
\reg_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh,
      CE => '1',
      D => \reg_reg[4]_i_1_n_7\,
      Q => reg_reg(4),
      R => '0'
    );
\reg_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \reg_reg[0]_i_1_n_0\,
      CO(3) => \reg_reg[4]_i_1_n_0\,
      CO(2) => \reg_reg[4]_i_1_n_1\,
      CO(1) => \reg_reg[4]_i_1_n_2\,
      CO(0) => \reg_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \reg_reg[4]_i_1_n_4\,
      O(2) => \reg_reg[4]_i_1_n_5\,
      O(1) => \reg_reg[4]_i_1_n_6\,
      O(0) => \reg_reg[4]_i_1_n_7\,
      S(3 downto 0) => reg_reg(7 downto 4)
    );
\reg_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh,
      CE => '1',
      D => \reg_reg[4]_i_1_n_6\,
      Q => reg_reg(5),
      R => '0'
    );
\reg_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh,
      CE => '1',
      D => \reg_reg[4]_i_1_n_5\,
      Q => reg_reg(6),
      R => '0'
    );
\reg_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh,
      CE => '1',
      D => \reg_reg[4]_i_1_n_4\,
      Q => reg_reg(7),
      R => '0'
    );
\reg_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh,
      CE => '1',
      D => \reg_reg[8]_i_1_n_7\,
      Q => reg_reg(8),
      R => '0'
    );
\reg_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \reg_reg[4]_i_1_n_0\,
      CO(3) => \reg_reg[8]_i_1_n_0\,
      CO(2) => \reg_reg[8]_i_1_n_1\,
      CO(1) => \reg_reg[8]_i_1_n_2\,
      CO(0) => \reg_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \reg_reg[8]_i_1_n_4\,
      O(2) => \reg_reg[8]_i_1_n_5\,
      O(1) => \reg_reg[8]_i_1_n_6\,
      O(0) => \reg_reg[8]_i_1_n_7\,
      S(3 downto 0) => reg_reg(11 downto 8)
    );
\reg_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh,
      CE => '1',
      D => \reg_reg[8]_i_1_n_6\,
      Q => reg_reg(9),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \contador__parameterized1\ is
  port (
    D : out STD_LOGIC_VECTOR ( 5 downto 0 );
    \FSM_sequential_current_state_reg[1]\ : out STD_LOGIC;
    \charact_dot_reg[0]\ : out STD_LOGIC;
    \reg_reg[1]_0\ : out STD_LOGIC;
    \reg_reg[0]_0\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \charact_dot_reg[0]_0\ : out STD_LOGIC;
    \reg_reg[2]_0\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[0]\ : out STD_LOGIC;
    \reg_reg[1]_1\ : out STD_LOGIC;
    \disp_reg[1][6]\ : out STD_LOGIC;
    AR : out STD_LOGIC_VECTOR ( 0 to 0 );
    elem_OBUF : out STD_LOGIC_VECTOR ( 7 downto 0 );
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    \reg_reg[0]_1\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[0]_0\ : out STD_LOGIC;
    \reg_reg[0]_2\ : out STD_LOGIC;
    \charact_dot_reg[0]_1\ : out STD_LOGIC;
    \caracter_reg[0]\ : in STD_LOGIC;
    \caracter_reg[0]_0\ : in STD_LOGIC;
    \disp_reg[1]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    current_state : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \caracter[0]_i_6_0\ : in STD_LOGIC;
    \disp_dinero[5]_0\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \caracter_reg[1]\ : in STD_LOGIC;
    \caracter_reg[1]_0\ : in STD_LOGIC;
    \disp_dinero[3]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter_reg[2]\ : in STD_LOGIC;
    \charact_dot_reg[0]_2\ : in STD_LOGIC;
    \disp_reg[5]\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \disp_reg[4]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_dinero[2]_2\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_reg[6]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter_reg[4]\ : in STD_LOGIC;
    \caracter_reg[4]_0\ : in STD_LOGIC;
    \caracter[4]_i_2_0\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \caracter[2]_i_6_0\ : in STD_LOGIC;
    \caracter_reg[6]\ : in STD_LOGIC;
    \disp_reg[3]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \disp_reg[8]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_reg[7]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    reset_IBUF : in STD_LOGIC;
    \caracter[1]_i_8_0\ : in STD_LOGIC;
    dot_reg : in STD_LOGIC;
    \charact_dot_reg[0]_3\ : in STD_LOGIC;
    \charact_dot_reg[0]_4\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \contador__parameterized1\ : entity is "contador";
end \contador__parameterized1\;

architecture STRUCTURE of \contador__parameterized1\ is
  signal \^ar\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^co\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^fsm_sequential_current_state_reg[0]\ : STD_LOGIC;
  signal \^fsm_sequential_current_state_reg[1]\ : STD_LOGIC;
  signal \^q\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal caracter2 : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \caracter[0]_i_15_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_16_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_17_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_18_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_19_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_27_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_2_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_3_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_6_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_7_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_8_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_10_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_11_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_12_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_13_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_14_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_20_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_21_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_23_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_2_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_33_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_3_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_4_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_7_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_8_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_9_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_14_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_15_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_16_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_17_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_18_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_19_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_20_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_26_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_2_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_3_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_5_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_6_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_7_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_9_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_13_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_15_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_27_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_2_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_6_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_7_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_8_n_0\ : STD_LOGIC;
  signal \caracter[4]_i_10_n_0\ : STD_LOGIC;
  signal \caracter[4]_i_11_n_0\ : STD_LOGIC;
  signal \caracter[4]_i_12_n_0\ : STD_LOGIC;
  signal \caracter[4]_i_13_n_0\ : STD_LOGIC;
  signal \caracter[4]_i_15_n_0\ : STD_LOGIC;
  signal \caracter[4]_i_2_n_0\ : STD_LOGIC;
  signal \caracter[4]_i_3_n_0\ : STD_LOGIC;
  signal \caracter[4]_i_4_n_0\ : STD_LOGIC;
  signal \caracter[4]_i_6_n_0\ : STD_LOGIC;
  signal \caracter[4]_i_8_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_10_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_11_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_12_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_14_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_2_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_3_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_5_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_6_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_7_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_8_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_9_n_0\ : STD_LOGIC;
  signal \^charact_dot_reg[0]\ : STD_LOGIC;
  signal \^charact_dot_reg[0]_0\ : STD_LOGIC;
  signal dot_i_11_n_0 : STD_LOGIC;
  signal dot_i_12_n_0 : STD_LOGIC;
  signal dot_i_13_n_0 : STD_LOGIC;
  signal dot_i_14_n_0 : STD_LOGIC;
  signal dot_i_15_n_0 : STD_LOGIC;
  signal dot_i_16_n_0 : STD_LOGIC;
  signal dot_i_6_n_0 : STD_LOGIC;
  signal dot_i_7_n_0 : STD_LOGIC;
  signal dot_i_9_n_0 : STD_LOGIC;
  signal dot_reg_i_4_n_2 : STD_LOGIC;
  signal dot_reg_i_4_n_3 : STD_LOGIC;
  signal plusOp : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \reg[1]_i_1_n_0\ : STD_LOGIC;
  signal \reg[2]_i_1_n_0\ : STD_LOGIC;
  signal \^reg_reg[0]_0\ : STD_LOGIC;
  signal \^reg_reg[1]_0\ : STD_LOGIC;
  signal \^reg_reg[1]_1\ : STD_LOGIC;
  signal \^reg_reg[2]_0\ : STD_LOGIC;
  signal NLW_dot_reg_i_4_CO_UNCONNECTED : STD_LOGIC_VECTOR ( 2 to 2 );
  signal NLW_dot_reg_i_4_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \caracter[0]_i_13\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \caracter[0]_i_14\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \caracter[0]_i_17\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \caracter[0]_i_19\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \caracter[0]_i_27\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \caracter[1]_i_12\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \caracter[1]_i_13\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \caracter[1]_i_23\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \caracter[1]_i_33\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \caracter[1]_i_34\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \caracter[2]_i_14\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \caracter[2]_i_20\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \caracter[2]_i_26\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \caracter[3]_i_2\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \caracter[3]_i_3\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \caracter[3]_i_4\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \caracter[4]_i_10\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \caracter[4]_i_11\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \caracter[4]_i_12\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \caracter[4]_i_13\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \caracter[4]_i_14\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \caracter[4]_i_15\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \caracter[4]_i_16\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \caracter[4]_i_6\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \caracter[4]_i_7\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \caracter[6]_i_10\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \caracter[6]_i_11\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \caracter[6]_i_12\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \caracter[6]_i_13\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \caracter[6]_i_14\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \caracter[6]_i_5\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \caracter[6]_i_8\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of dot_i_10 : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of dot_i_7 : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of dot_i_8 : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \elem_OBUF[0]_inst_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \elem_OBUF[1]_inst_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \elem_OBUF[2]_inst_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \elem_OBUF[3]_inst_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \elem_OBUF[4]_inst_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \elem_OBUF[5]_inst_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \elem_OBUF[6]_inst_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \elem_OBUF[7]_inst_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \reg[0]_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \reg[1]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \reg[2]_i_1\ : label is "soft_lutpair20";
begin
  AR(0) <= \^ar\(0);
  CO(0) <= \^co\(0);
  \FSM_sequential_current_state_reg[0]\ <= \^fsm_sequential_current_state_reg[0]\;
  \FSM_sequential_current_state_reg[1]\ <= \^fsm_sequential_current_state_reg[1]\;
  Q(2 downto 0) <= \^q\(2 downto 0);
  \charact_dot_reg[0]\ <= \^charact_dot_reg[0]\;
  \charact_dot_reg[0]_0\ <= \^charact_dot_reg[0]_0\;
  \reg_reg[0]_0\ <= \^reg_reg[0]_0\;
  \reg_reg[1]_0\ <= \^reg_reg[1]_0\;
  \reg_reg[1]_1\ <= \^reg_reg[1]_1\;
  \reg_reg[2]_0\ <= \^reg_reg[2]_0\;
\FSM_sequential_current_state[1]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => reset_IBUF,
      O => \^ar\(0)
    );
\caracter[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFEEE"
    )
        port map (
      I0 => \caracter[0]_i_2_n_0\,
      I1 => \caracter[0]_i_3_n_0\,
      I2 => \caracter_reg[0]\,
      I3 => \^fsm_sequential_current_state_reg[1]\,
      I4 => \caracter_reg[0]_0\,
      I5 => \caracter[0]_i_6_n_0\,
      O => D(0)
    );
\caracter[0]_i_13\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AE3300FF"
    )
        port map (
      I0 => \disp_reg[1]\(1),
      I1 => \charact_dot_reg[0]_2\,
      I2 => \disp_reg[1]\(0),
      I3 => \^q\(1),
      I4 => \^q\(0),
      O => \disp_reg[1][6]\
    );
\caracter[0]_i_14\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000006"
    )
        port map (
      I0 => \^q\(0),
      I1 => \charact_dot_reg[0]_2\,
      I2 => \^co\(0),
      I3 => current_state(1),
      I4 => dot_i_7_n_0,
      O => \reg_reg[0]_1\
    );
\caracter[0]_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2200000050FF0000"
    )
        port map (
      I0 => \^charact_dot_reg[0]\,
      I1 => \caracter[0]_i_6_0\,
      I2 => \disp_dinero[5]_0\(0),
      I3 => current_state(0),
      I4 => \^reg_reg[1]_0\,
      I5 => \caracter[6]_i_11_n_0\,
      O => \caracter[0]_i_15_n_0\
    );
\caracter[0]_i_16\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF80000080800000"
    )
        port map (
      I0 => \caracter[6]_i_11_n_0\,
      I1 => \^reg_reg[2]_0\,
      I2 => \disp_reg[3]\(1),
      I3 => \caracter[4]_i_13_n_0\,
      I4 => \caracter[6]_i_14_n_0\,
      I5 => \disp_reg[5]\(0),
      O => \caracter[0]_i_16_n_0\
    );
\caracter[0]_i_17\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4111"
    )
        port map (
      I0 => \^co\(0),
      I1 => \^q\(2),
      I2 => \^q\(0),
      I3 => \^q\(1),
      O => \caracter[0]_i_17_n_0\
    );
\caracter[0]_i_18\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0C00808000008080"
    )
        port map (
      I0 => \caracter[0]_i_27_n_0\,
      I1 => \caracter[6]_i_14_n_0\,
      I2 => \caracter[6]_i_11_n_0\,
      I3 => \disp_reg[7]\(1),
      I4 => dot_i_7_n_0,
      I5 => \^charact_dot_reg[0]_0\,
      O => \caracter[0]_i_18_n_0\
    );
\caracter[0]_i_19\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000006"
    )
        port map (
      I0 => \^q\(0),
      I1 => \charact_dot_reg[0]_2\,
      I2 => \^co\(0),
      I3 => current_state(1),
      I4 => current_state(0),
      O => \caracter[0]_i_19_n_0\
    );
\caracter[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFEAEAEA"
    )
        port map (
      I0 => \caracter[0]_i_7_n_0\,
      I1 => \caracter[4]_i_2_0\(0),
      I2 => \caracter_reg[6]\,
      I3 => \disp_reg[3]\(1),
      I4 => \caracter[2]_i_9_n_0\,
      I5 => \caracter[0]_i_8_n_0\,
      O => \caracter[0]_i_2_n_0\
    );
\caracter[0]_i_27\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8AAAFFFF"
    )
        port map (
      I0 => \caracter[4]_i_2_0\(0),
      I1 => \charact_dot_reg[0]_2\,
      I2 => \^q\(1),
      I3 => \^q\(0),
      I4 => current_state(0),
      O => \caracter[0]_i_27_n_0\
    );
\caracter[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6066000000000000"
    )
        port map (
      I0 => \^q\(0),
      I1 => \charact_dot_reg[0]_2\,
      I2 => \disp_dinero[5]_0\(0),
      I3 => current_state(0),
      I4 => current_state(1),
      I5 => \caracter[1]_i_12_n_0\,
      O => \caracter[0]_i_3_n_0\
    );
\caracter[0]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFEFEFEEEEEEEEEE"
    )
        port map (
      I0 => \caracter[0]_i_15_n_0\,
      I1 => \caracter[0]_i_16_n_0\,
      I2 => \caracter[6]_i_12_n_0\,
      I3 => \disp_reg[1]\(1),
      I4 => current_state(1),
      I5 => \caracter[0]_i_17_n_0\,
      O => \caracter[0]_i_6_n_0\
    );
\caracter[0]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000100100000"
    )
        port map (
      I0 => \^co\(0),
      I1 => current_state(1),
      I2 => \charact_dot_reg[0]_2\,
      I3 => \^q\(2),
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \caracter[0]_i_7_n_0\
    );
\caracter[0]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEAFAAFFAEAEAAAA"
    )
        port map (
      I0 => \caracter[0]_i_18_n_0\,
      I1 => \caracter[6]_i_5_n_0\,
      I2 => \charact_dot_reg[0]_2\,
      I3 => \^q\(1),
      I4 => \^q\(0),
      I5 => \caracter[0]_i_19_n_0\,
      O => \caracter[0]_i_8_n_0\
    );
\caracter[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \caracter[1]_i_2_n_0\,
      I1 => \caracter[1]_i_3_n_0\,
      I2 => \caracter[1]_i_4_n_0\,
      I3 => \caracter_reg[1]\,
      I4 => \caracter_reg[1]_0\,
      I5 => \caracter[1]_i_7_n_0\,
      O => D(1)
    );
\caracter[1]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08088808"
    )
        port map (
      I0 => \^charact_dot_reg[0]_0\,
      I1 => \caracter[2]_i_20_n_0\,
      I2 => current_state(0),
      I3 => \disp_reg[5]\(1),
      I4 => dot_i_7_n_0,
      O => \caracter[1]_i_10_n_0\
    );
\caracter[1]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEAAAAAEAEAAAAA"
    )
        port map (
      I0 => \caracter[3]_i_27_n_0\,
      I1 => \caracter[1]_i_23_n_0\,
      I2 => \caracter[4]_i_15_n_0\,
      I3 => \caracter[4]_i_13_n_0\,
      I4 => \caracter[2]_i_20_n_0\,
      I5 => \disp_reg[4]\(0),
      O => \caracter[1]_i_11_n_0\
    );
\caracter[1]_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => \^co\(0),
      I1 => \^q\(0),
      I2 => \^q\(1),
      I3 => \charact_dot_reg[0]_2\,
      O => \caracter[1]_i_12_n_0\
    );
\caracter[1]_i_13\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AC3300FF"
    )
        port map (
      I0 => \disp_reg[1]\(1),
      I1 => \charact_dot_reg[0]_2\,
      I2 => \disp_reg[1]\(0),
      I3 => \^q\(1),
      I4 => \^q\(0),
      O => \caracter[1]_i_13_n_0\
    );
\caracter[1]_i_14\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0300840000000000"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(2),
      I3 => \caracter[6]_i_14_n_0\,
      I4 => \charact_dot_reg[0]_2\,
      I5 => \disp_reg[3]\(0),
      O => \caracter[1]_i_14_n_0\
    );
\caracter[1]_i_17\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000400000000000"
    )
        port map (
      I0 => \^co\(0),
      I1 => \disp_dinero[5]_0\(1),
      I2 => \^q\(0),
      I3 => \charact_dot_reg[0]_2\,
      I4 => dot_i_7_n_0,
      I5 => current_state(1),
      O => \reg_reg[0]_2\
    );
\caracter[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFAEAFAEAE"
    )
        port map (
      I0 => \caracter[1]_i_8_n_0\,
      I1 => \caracter[4]_i_6_n_0\,
      I2 => current_state(0),
      I3 => \caracter[6]_i_12_n_0\,
      I4 => \caracter[4]_i_11_n_0\,
      I5 => \caracter[1]_i_9_n_0\,
      O => \caracter[1]_i_2_n_0\
    );
\caracter[1]_i_20\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0F02020202020202"
    )
        port map (
      I0 => \^reg_reg[1]_0\,
      I1 => \disp_dinero[5]_0\(1),
      I2 => \caracter[6]_i_12_n_0\,
      I3 => \disp_reg[1]\(1),
      I4 => \disp_reg[1]\(0),
      I5 => \caracter[1]_i_33_n_0\,
      O => \caracter[1]_i_20_n_0\
    );
\caracter[1]_i_21\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0080800000008000"
    )
        port map (
      I0 => \caracter[4]_i_11_n_0\,
      I1 => \^reg_reg[2]_0\,
      I2 => \disp_reg[7]\(0),
      I3 => \caracter[1]_i_23_n_0\,
      I4 => plusOp(1),
      I5 => \disp_reg[7]\(1),
      O => \caracter[1]_i_21_n_0\
    );
\caracter[1]_i_23\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \charact_dot_reg[0]_2\,
      I1 => \^q\(0),
      O => \caracter[1]_i_23_n_0\
    );
\caracter[1]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFEAAA"
    )
        port map (
      I0 => \caracter[1]_i_10_n_0\,
      I1 => \disp_reg[5]\(2),
      I2 => dot_i_7_n_0,
      I3 => \caracter[6]_i_10_n_0\,
      I4 => \caracter[1]_i_11_n_0\,
      O => \caracter[1]_i_3_n_0\
    );
\caracter[1]_i_33\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"10010101"
    )
        port map (
      I0 => current_state(1),
      I1 => \^co\(0),
      I2 => \^q\(2),
      I3 => \^q\(0),
      I4 => \^q\(1),
      O => \caracter[1]_i_33_n_0\
    );
\caracter[1]_i_34\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      O => plusOp(1)
    );
\caracter[1]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF200020002000"
    )
        port map (
      I0 => \caracter[1]_i_12_n_0\,
      I1 => current_state(0),
      I2 => current_state(1),
      I3 => \caracter[6]_i_11_n_0\,
      I4 => \caracter[1]_i_13_n_0\,
      I5 => \caracter[1]_i_14_n_0\,
      O => \caracter[1]_i_4_n_0\
    );
\caracter[1]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEEEEFEE"
    )
        port map (
      I0 => \caracter[1]_i_20_n_0\,
      I1 => \caracter[1]_i_21_n_0\,
      I2 => current_state(0),
      I3 => \caracter[4]_i_11_n_0\,
      I4 => \^charact_dot_reg[0]\,
      O => \caracter[1]_i_7_n_0\
    );
\caracter[1]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFF040404"
    )
        port map (
      I0 => dot_i_7_n_0,
      I1 => \caracter[4]_i_11_n_0\,
      I2 => current_state(0),
      I3 => \caracter_reg[6]\,
      I4 => \disp_reg[5]\(1),
      I5 => \caracter[6]_i_9_n_0\,
      O => \caracter[1]_i_8_n_0\
    );
\caracter[1]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0800800000000088"
    )
        port map (
      I0 => \disp_dinero[2]_2\(0),
      I1 => \^reg_reg[1]_0\,
      I2 => \disp_dinero[5]_0\(1),
      I3 => \^q\(0),
      I4 => \^q\(1),
      I5 => \charact_dot_reg[0]_2\,
      O => \caracter[1]_i_9_n_0\
    );
\caracter[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFEEE"
    )
        port map (
      I0 => \caracter[2]_i_2_n_0\,
      I1 => \caracter[2]_i_3_n_0\,
      I2 => \caracter_reg[2]\,
      I3 => \^fsm_sequential_current_state_reg[1]\,
      I4 => \caracter[2]_i_5_n_0\,
      I5 => \caracter[2]_i_6_n_0\,
      O => D(2)
    );
\caracter[2]_i_14\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000093"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => \charact_dot_reg[0]_2\,
      I3 => current_state(1),
      I4 => \^co\(0),
      O => \caracter[2]_i_14_n_0\
    );
\caracter[2]_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0040000000000000"
    )
        port map (
      I0 => \^co\(0),
      I1 => current_state(1),
      I2 => \disp_dinero[5]_0\(1),
      I3 => \charact_dot_reg[0]_2\,
      I4 => \^q\(1),
      I5 => \^q\(0),
      O => \caracter[2]_i_15_n_0\
    );
\caracter[2]_i_16\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9324002400000000"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => \charact_dot_reg[0]_2\,
      I3 => current_state(0),
      I4 => \caracter[2]_i_6_0\,
      I5 => \caracter[0]_i_17_n_0\,
      O => \caracter[2]_i_16_n_0\
    );
\caracter[2]_i_17\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000500000000040"
    )
        port map (
      I0 => \^co\(0),
      I1 => current_state(1),
      I2 => \^charact_dot_reg[0]\,
      I3 => \caracter[6]_i_11_n_0\,
      I4 => current_state(0),
      I5 => dot_i_7_n_0,
      O => \caracter[2]_i_17_n_0\
    );
\caracter[2]_i_18\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0F80000000800000"
    )
        port map (
      I0 => \disp_dinero[5]_0\(1),
      I1 => \^reg_reg[1]_0\,
      I2 => \charact_dot_reg[0]_2\,
      I3 => \^q\(1),
      I4 => \^q\(0),
      I5 => \caracter[6]_i_5_n_0\,
      O => \caracter[2]_i_18_n_0\
    );
\caracter[2]_i_19\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F888888800000000"
    )
        port map (
      I0 => \caracter[4]_i_13_n_0\,
      I1 => \disp_reg[6]\(0),
      I2 => \caracter[2]_i_26_n_0\,
      I3 => \disp_reg[7]\(0),
      I4 => dot_i_7_n_0,
      I5 => \caracter[4]_i_11_n_0\,
      O => \caracter[2]_i_19_n_0\
    );
\caracter[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFBAAABAAABAAA"
    )
        port map (
      I0 => \caracter[2]_i_7_n_0\,
      I1 => \caracter[6]_i_12_n_0\,
      I2 => \disp_dinero[5]_0\(1),
      I3 => \^reg_reg[1]_0\,
      I4 => \disp_reg[5]\(1),
      I5 => \caracter[2]_i_9_n_0\,
      O => \caracter[2]_i_2_n_0\
    );
\caracter[2]_i_20\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1001"
    )
        port map (
      I0 => current_state(1),
      I1 => \^co\(0),
      I2 => \charact_dot_reg[0]_2\,
      I3 => \^q\(0),
      O => \caracter[2]_i_20_n_0\
    );
\caracter[2]_i_26\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"C30B"
    )
        port map (
      I0 => \disp_reg[7]\(1),
      I1 => \^q\(0),
      I2 => \^q\(1),
      I3 => \charact_dot_reg[0]_2\,
      O => \caracter[2]_i_26_n_0\
    );
\caracter[2]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => \disp_reg[1]\(0),
      I1 => \caracter[6]_i_14_n_0\,
      I2 => \^q\(2),
      I3 => \^q\(0),
      I4 => \^q\(1),
      I5 => \charact_dot_reg[0]_2\,
      O => \caracter[2]_i_3_n_0\
    );
\caracter[2]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88FFF80088008800"
    )
        port map (
      I0 => \caracter[6]_i_10_n_0\,
      I1 => \disp_reg[6]\(0),
      I2 => \disp_reg[3]\(3),
      I3 => current_state(0),
      I4 => dot_i_7_n_0,
      I5 => \caracter[2]_i_14_n_0\,
      O => \caracter[2]_i_5_n_0\
    );
\caracter[2]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFF8"
    )
        port map (
      I0 => \caracter[2]_i_15_n_0\,
      I1 => \caracter[6]_i_11_n_0\,
      I2 => \caracter[2]_i_16_n_0\,
      I3 => \caracter[2]_i_17_n_0\,
      I4 => \caracter[2]_i_18_n_0\,
      I5 => \caracter[2]_i_19_n_0\,
      O => \caracter[2]_i_6_n_0\
    );
\caracter[2]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0800"
    )
        port map (
      I0 => \caracter[2]_i_20_n_0\,
      I1 => \^charact_dot_reg[0]_0\,
      I2 => dot_i_7_n_0,
      I3 => current_state(0),
      I4 => \caracter_reg[6]\,
      O => \caracter[2]_i_7_n_0\
    );
\caracter[2]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF01000000"
    )
        port map (
      I0 => \^co\(0),
      I1 => \^charact_dot_reg[0]\,
      I2 => current_state(1),
      I3 => current_state(0),
      I4 => dot_i_7_n_0,
      I5 => \caracter[1]_i_8_0\,
      O => \caracter[2]_i_9_n_0\
    );
\caracter[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFEAAA"
    )
        port map (
      I0 => \caracter[3]_i_2_n_0\,
      I1 => \^fsm_sequential_current_state_reg[1]\,
      I2 => \^reg_reg[0]_0\,
      I3 => \disp_dinero[3]_1\(0),
      I4 => \caracter[3]_i_6_n_0\,
      I5 => \caracter[3]_i_7_n_0\,
      O => D(3)
    );
\caracter[3]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F888888888888888"
    )
        port map (
      I0 => \disp_reg[8]\(0),
      I1 => \caracter[2]_i_7_n_0\,
      I2 => \caracter[1]_i_13_n_0\,
      I3 => \disp_reg[3]\(1),
      I4 => \^fsm_sequential_current_state_reg[0]\,
      I5 => \caracter[4]_i_11_n_0\,
      O => \caracter[3]_i_13_n_0\
    );
\caracter[3]_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFEAAAAAAA"
    )
        port map (
      I0 => \caracter[1]_i_21_n_0\,
      I1 => \disp_reg[6]\(0),
      I2 => dot_i_7_n_0,
      I3 => current_state(0),
      I4 => \caracter[6]_i_10_n_0\,
      I5 => \caracter[3]_i_27_n_0\,
      O => \caracter[3]_i_15_n_0\
    );
\caracter[3]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0002"
    )
        port map (
      I0 => current_state(1),
      I1 => current_state(0),
      I2 => \^co\(0),
      I3 => \caracter[6]_i_12_n_0\,
      I4 => \caracter[3]_i_8_n_0\,
      O => \caracter[3]_i_2_n_0\
    );
\caracter[3]_i_27\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF08208020"
    )
        port map (
      I0 => \caracter[2]_i_15_n_0\,
      I1 => \charact_dot_reg[0]_2\,
      I2 => \^q\(2),
      I3 => \^q\(0),
      I4 => \^q\(1),
      I5 => \caracter[2]_i_18_n_0\,
      O => \caracter[3]_i_27_n_0\
    );
\caracter[3]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00020200"
    )
        port map (
      I0 => current_state(1),
      I1 => \^co\(0),
      I2 => dot_i_7_n_0,
      I3 => \charact_dot_reg[0]_2\,
      I4 => \^q\(0),
      O => \^fsm_sequential_current_state_reg[1]\
    );
\caracter[3]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9300"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => \charact_dot_reg[0]_2\,
      I3 => current_state(0),
      O => \^reg_reg[0]_0\
    );
\caracter[3]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000010"
    )
        port map (
      I0 => current_state(0),
      I1 => \^charact_dot_reg[0]\,
      I2 => current_state(1),
      I3 => \^co\(0),
      I4 => dot_i_7_n_0,
      O => \caracter[3]_i_6_n_0\
    );
\caracter[3]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFEAAAAAAA"
    )
        port map (
      I0 => \caracter[3]_i_13_n_0\,
      I1 => \disp_dinero[2]_2\(1),
      I2 => \caracter[4]_i_6_n_0\,
      I3 => current_state(0),
      I4 => \^charact_dot_reg[0]_0\,
      I5 => \caracter[3]_i_15_n_0\,
      O => \caracter[3]_i_7_n_0\
    );
\caracter[3]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000001000000"
    )
        port map (
      I0 => \^co\(0),
      I1 => current_state(1),
      I2 => dot_i_7_n_0,
      I3 => \disp_reg[1]\(0),
      I4 => \disp_reg[1]\(1),
      I5 => \caracter[6]_i_12_n_0\,
      O => \caracter[3]_i_8_n_0\
    );
\caracter[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFFFEFFFE"
    )
        port map (
      I0 => \caracter[4]_i_2_n_0\,
      I1 => \caracter[4]_i_3_n_0\,
      I2 => \caracter[4]_i_4_n_0\,
      I3 => \caracter_reg[4]\,
      I4 => \caracter[4]_i_6_n_0\,
      I5 => \^charact_dot_reg[0]_0\,
      O => D(4)
    );
\caracter[4]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9333"
    )
        port map (
      I0 => \charact_dot_reg[0]_2\,
      I1 => \^q\(2),
      I2 => \^q\(0),
      I3 => \^q\(1),
      O => \caracter[4]_i_10_n_0\
    );
\caracter[4]_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0110"
    )
        port map (
      I0 => current_state(1),
      I1 => \^co\(0),
      I2 => \charact_dot_reg[0]_2\,
      I3 => \^q\(0),
      O => \caracter[4]_i_11_n_0\
    );
\caracter[4]_i_12\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8C3300FF"
    )
        port map (
      I0 => \disp_reg[1]\(1),
      I1 => \charact_dot_reg[0]_2\,
      I2 => \disp_reg[1]\(0),
      I3 => \^q\(1),
      I4 => \^q\(0),
      O => \caracter[4]_i_12_n_0\
    );
\caracter[4]_i_13\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"13200000"
    )
        port map (
      I0 => \charact_dot_reg[0]_2\,
      I1 => \^q\(2),
      I2 => \^q\(0),
      I3 => \^q\(1),
      I4 => current_state(0),
      O => \caracter[4]_i_13_n_0\
    );
\caracter[4]_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D3"
    )
        port map (
      I0 => \charact_dot_reg[0]_2\,
      I1 => \^q\(1),
      I2 => \^q\(0),
      O => \^charact_dot_reg[0]\
    );
\caracter[4]_i_15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"006A"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(0),
      I2 => \^q\(1),
      I3 => current_state(0),
      O => \caracter[4]_i_15_n_0\
    );
\caracter[4]_i_16\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00870000"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(2),
      I3 => \^co\(0),
      I4 => current_state(1),
      O => \^reg_reg[1]_0\
    );
\caracter[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EAEAEAEAFFAAAAAA"
    )
        port map (
      I0 => \caracter[4]_i_8_n_0\,
      I1 => \caracter_reg[4]_0\,
      I2 => \^fsm_sequential_current_state_reg[1]\,
      I3 => \caracter[4]_i_10_n_0\,
      I4 => \caracter[4]_i_11_n_0\,
      I5 => current_state(0),
      O => \caracter[4]_i_2_n_0\
    );
\caracter[4]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF20000020200000"
    )
        port map (
      I0 => \caracter[4]_i_12_n_0\,
      I1 => dot_i_7_n_0,
      I2 => \disp_reg[3]\(2),
      I3 => \caracter[6]_i_8_n_0\,
      I4 => \caracter[4]_i_11_n_0\,
      I5 => \disp_reg[5]\(1),
      O => \caracter[4]_i_3_n_0\
    );
\caracter[4]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"80FF808000000000"
    )
        port map (
      I0 => \caracter[4]_i_13_n_0\,
      I1 => \caracter[6]_i_11_n_0\,
      I2 => \disp_reg[4]\(1),
      I3 => \^charact_dot_reg[0]\,
      I4 => \caracter[4]_i_15_n_0\,
      I5 => \caracter[6]_i_14_n_0\,
      O => \caracter[4]_i_4_n_0\
    );
\caracter[4]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000002"
    )
        port map (
      I0 => current_state(1),
      I1 => \^co\(0),
      I2 => dot_i_7_n_0,
      I3 => \charact_dot_reg[0]_2\,
      I4 => \^q\(0),
      O => \caracter[4]_i_6_n_0\
    );
\caracter[4]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"93"
    )
        port map (
      I0 => \charact_dot_reg[0]_2\,
      I1 => \^q\(1),
      I2 => \^q\(0),
      O => \^charact_dot_reg[0]_0\
    );
\caracter[4]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F000888800008888"
    )
        port map (
      I0 => \caracter[2]_i_15_n_0\,
      I1 => \^reg_reg[2]_0\,
      I2 => \^reg_reg[0]_0\,
      I3 => \caracter[4]_i_2_0\(1),
      I4 => \caracter[6]_i_11_n_0\,
      I5 => \caracter[0]_i_17_n_0\,
      O => \caracter[4]_i_8_n_0\
    );
\caracter[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \caracter[6]_i_2_n_0\,
      I1 => \caracter[6]_i_3_n_0\,
      I2 => \caracter_reg[6]\,
      I3 => \caracter[6]_i_5_n_0\,
      I4 => \caracter[6]_i_6_n_0\,
      I5 => \caracter[6]_i_7_n_0\,
      O => D(5)
    );
\caracter[6]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8100"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \charact_dot_reg[0]_2\,
      I3 => \caracter[6]_i_14_n_0\,
      O => \caracter[6]_i_10_n_0\
    );
\caracter[6]_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(0),
      I1 => \charact_dot_reg[0]_2\,
      O => \caracter[6]_i_11_n_0\
    );
\caracter[6]_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BF"
    )
        port map (
      I0 => \charact_dot_reg[0]_2\,
      I1 => \^q\(1),
      I2 => \^q\(0),
      O => \caracter[6]_i_12_n_0\
    );
\caracter[6]_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8222"
    )
        port map (
      I0 => current_state(0),
      I1 => \^q\(2),
      I2 => \^q\(0),
      I3 => \^q\(1),
      O => \^fsm_sequential_current_state_reg[0]\
    );
\caracter[6]_i_14\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^co\(0),
      I1 => current_state(1),
      O => \caracter[6]_i_14_n_0\
    );
\caracter[6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF00FF10FF00"
    )
        port map (
      I0 => \^co\(0),
      I1 => current_state(1),
      I2 => \caracter[6]_i_8_n_0\,
      I3 => \caracter[6]_i_9_n_0\,
      I4 => \disp_reg[5]\(2),
      I5 => \caracter[6]_i_10_n_0\,
      O => \caracter[6]_i_2_n_0\
    );
\caracter[6]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000050005005454"
    )
        port map (
      I0 => \^co\(0),
      I1 => \disp_reg[1]\(1),
      I2 => current_state(1),
      I3 => \caracter[6]_i_11_n_0\,
      I4 => \caracter[6]_i_12_n_0\,
      I5 => dot_i_7_n_0,
      O => \caracter[6]_i_3_n_0\
    );
\caracter[6]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000087"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(2),
      I3 => \^co\(0),
      I4 => current_state(0),
      O => \caracter[6]_i_5_n_0\
    );
\caracter[6]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0088C08800000000"
    )
        port map (
      I0 => \disp_reg[3]\(3),
      I1 => \^charact_dot_reg[0]_0\,
      I2 => \disp_reg[7]\(1),
      I3 => dot_i_7_n_0,
      I4 => \caracter[6]_i_11_n_0\,
      I5 => \caracter[6]_i_14_n_0\,
      O => \caracter[6]_i_6_n_0\
    );
\caracter[6]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0501010105010105"
    )
        port map (
      I0 => \^co\(0),
      I1 => current_state(1),
      I2 => current_state(0),
      I3 => \^q\(0),
      I4 => \^q\(1),
      I5 => \charact_dot_reg[0]_2\,
      O => \caracter[6]_i_7_n_0\
    );
\caracter[6]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"060A"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(2),
      I3 => \charact_dot_reg[0]_2\,
      O => \caracter[6]_i_8_n_0\
    );
\caracter[6]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCCCCCDC00000000"
    )
        port map (
      I0 => \^co\(0),
      I1 => \caracter[1]_i_8_0\,
      I2 => dot_i_7_n_0,
      I3 => current_state(1),
      I4 => \^charact_dot_reg[0]\,
      I5 => \disp_reg[8]\(0),
      O => \caracter[6]_i_9_n_0\
    );
\charact_dot[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFCCCC44F4CCCC"
    )
        port map (
      I0 => \^co\(0),
      I1 => \charact_dot_reg[0]_2\,
      I2 => \charact_dot_reg[0]_3\,
      I3 => \^q\(0),
      I4 => reset_IBUF,
      I5 => \charact_dot_reg[0]_4\,
      O => \charact_dot_reg[0]_1\
    );
dot_i_10: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6A00"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(0),
      I2 => \^q\(1),
      I3 => current_state(0),
      O => \^reg_reg[2]_0\
    );
dot_i_11: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(2),
      O => dot_i_11_n_0
    );
dot_i_12: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      O => dot_i_12_n_0
    );
dot_i_13: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(0),
      O => dot_i_13_n_0
    );
dot_i_14: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(2),
      O => dot_i_14_n_0
    );
dot_i_15: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      O => dot_i_15_n_0
    );
dot_i_16: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(0),
      I1 => \charact_dot_reg[0]_2\,
      O => dot_i_16_n_0
    );
dot_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFCDCDCDDD"
    )
        port map (
      I0 => \^co\(0),
      I1 => dot_i_6_n_0,
      I2 => current_state(0),
      I3 => dot_i_7_n_0,
      I4 => \^reg_reg[1]_1\,
      I5 => dot_i_9_n_0,
      O => \FSM_sequential_current_state_reg[0]_0\
    );
dot_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4555005545000055"
    )
        port map (
      I0 => \^co\(0),
      I1 => \disp_reg[1]\(1),
      I2 => \disp_reg[1]\(0),
      I3 => \^q\(1),
      I4 => \^q\(0),
      I5 => dot_reg,
      O => dot_i_6_n_0
    );
dot_i_7: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(2),
      O => dot_i_7_n_0
    );
dot_i_8: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      O => \^reg_reg[1]_1\
    );
dot_i_9: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5454555515051505"
    )
        port map (
      I0 => \^co\(0),
      I1 => \^q\(2),
      I2 => \^q\(0),
      I3 => \^q\(1),
      I4 => \disp_dinero[5]_0\(1),
      I5 => current_state(1),
      O => dot_i_9_n_0
    );
dot_reg_i_4: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \^co\(0),
      CO(2) => NLW_dot_reg_i_4_CO_UNCONNECTED(2),
      CO(1) => dot_reg_i_4_n_2,
      CO(0) => dot_reg_i_4_n_3,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => dot_i_11_n_0,
      DI(1) => dot_i_12_n_0,
      DI(0) => dot_i_13_n_0,
      O(3) => NLW_dot_reg_i_4_O_UNCONNECTED(3),
      O(2) => caracter2(2),
      O(1 downto 0) => NLW_dot_reg_i_4_O_UNCONNECTED(1 downto 0),
      S(3) => '1',
      S(2) => dot_i_14_n_0,
      S(1) => dot_i_15_n_0,
      S(0) => dot_i_16_n_0
    );
\elem_OBUF[0]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEF"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(1),
      I2 => reset_IBUF,
      I3 => \^q\(0),
      O => elem_OBUF(0)
    );
\elem_OBUF[1]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFF7"
    )
        port map (
      I0 => \^q\(0),
      I1 => reset_IBUF,
      I2 => \^q\(2),
      I3 => \^q\(1),
      O => elem_OBUF(1)
    );
\elem_OBUF[2]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFDF"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(2),
      I2 => reset_IBUF,
      I3 => \^q\(0),
      O => elem_OBUF(2)
    );
\elem_OBUF[3]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => reset_IBUF,
      I1 => \^q\(2),
      I2 => \^q\(1),
      I3 => \^q\(0),
      O => elem_OBUF(3)
    );
\elem_OBUF[4]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFDF"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(1),
      I2 => reset_IBUF,
      I3 => \^q\(0),
      O => elem_OBUF(4)
    );
\elem_OBUF[5]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF7F"
    )
        port map (
      I0 => \^q\(0),
      I1 => reset_IBUF,
      I2 => \^q\(2),
      I3 => \^q\(1),
      O => elem_OBUF(5)
    );
\elem_OBUF[6]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF7F"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(1),
      I2 => reset_IBUF,
      I3 => \^q\(0),
      O => elem_OBUF(6)
    );
\elem_OBUF[7]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(0),
      I2 => \^q\(1),
      I3 => reset_IBUF,
      O => elem_OBUF(7)
    );
\reg[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(0),
      O => \reg[0]_i_1_n_0\
    );
\reg[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      O => \reg[1]_i_1_n_0\
    );
\reg[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(0),
      I2 => \^q\(1),
      O => \reg[2]_i_1_n_0\
    );
\reg_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => \^ar\(0),
      D => \reg[0]_i_1_n_0\,
      Q => \^q\(0)
    );
\reg_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => \^ar\(0),
      D => \reg[1]_i_1_n_0\,
      Q => \^q\(1)
    );
\reg_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => \^ar\(0),
      D => \reg[2]_i_1_n_0\,
      Q => \^q\(2)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decod_monedas is
  port (
    \valor_moneda_reg[6]_0\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 6 downto 0 );
    \valor_moneda_reg[0]_0\ : out STD_LOGIC;
    current_state : in STD_LOGIC_VECTOR ( 1 downto 0 );
    D : in STD_LOGIC_VECTOR ( 6 downto 0 );
    clk_BUFG : in STD_LOGIC
  );
end decod_monedas;

architecture STRUCTURE of decod_monedas is
  signal \FSM_sequential_current_state[1]_i_5__0_n_0\ : STD_LOGIC;
  signal \^q\ : STD_LOGIC_VECTOR ( 6 downto 0 );
begin
  Q(6 downto 0) <= \^q\(6 downto 0);
\FSM_sequential_current_state[0]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \FSM_sequential_current_state[1]_i_5__0_n_0\,
      I1 => \^q\(0),
      I2 => \^q\(5),
      I3 => \^q\(6),
      O => \valor_moneda_reg[0]_0\
    );
\FSM_sequential_current_state[1]_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFE00000000"
    )
        port map (
      I0 => \^q\(6),
      I1 => \^q\(5),
      I2 => \^q\(0),
      I3 => \FSM_sequential_current_state[1]_i_5__0_n_0\,
      I4 => current_state(0),
      I5 => current_state(1),
      O => \valor_moneda_reg[6]_0\
    );
\FSM_sequential_current_state[1]_i_5__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(1),
      I2 => \^q\(4),
      I3 => \^q\(3),
      O => \FSM_sequential_current_state[1]_i_5__0_n_0\
    );
\valor_moneda_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => D(0),
      Q => \^q\(0),
      R => '0'
    );
\valor_moneda_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => D(1),
      Q => \^q\(1),
      R => '0'
    );
\valor_moneda_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => D(2),
      Q => \^q\(2),
      R => '0'
    );
\valor_moneda_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => D(3),
      Q => \^q\(3),
      R => '0'
    );
\valor_moneda_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => D(4),
      Q => \^q\(4),
      R => '0'
    );
\valor_moneda_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => D(5),
      Q => \^q\(5),
      R => '0'
    );
\valor_moneda_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => D(6),
      Q => \^q\(6),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity digito is
  port (
    digit_OBUF : out STD_LOGIC_VECTOR ( 2 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 5 downto 0 )
  );
end digito;

architecture STRUCTURE of digito is
begin
\digit_OBUF[0]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0028000390102081"
    )
        port map (
      I0 => Q(0),
      I1 => Q(1),
      I2 => Q(5),
      I3 => Q(3),
      I4 => Q(2),
      I5 => Q(4),
      O => digit_OBUF(0)
    );
\digit_OBUF[4]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E3BCAF8FE7FBBF8F"
    )
        port map (
      I0 => Q(3),
      I1 => Q(2),
      I2 => Q(4),
      I3 => Q(1),
      I4 => Q(5),
      I5 => Q(0),
      O => digit_OBUF(1)
    );
\digit_OBUF[6]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F2AFABAFFABFFB4F"
    )
        port map (
      I0 => Q(3),
      I1 => Q(0),
      I2 => Q(4),
      I3 => Q(5),
      I4 => Q(2),
      I5 => Q(1),
      O => digit_OBUF(2)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity edgedtctr is
  port (
    \sreg_reg[2]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \sreg_reg[2]_1\ : out STD_LOGIC;
    \sreg_reg[0]_0\ : out STD_LOGIC;
    \sreg_reg[0]_1\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    \valor_moneda_reg[2]\ : in STD_LOGIC;
    \valor_moneda_reg[2]_0\ : in STD_LOGIC;
    \valor_moneda_reg[2]_1\ : in STD_LOGIC;
    \valor_moneda_reg[2]_2\ : in STD_LOGIC;
    \valor_moneda_reg[0]\ : in STD_LOGIC;
    sreg : in STD_LOGIC_VECTOR ( 2 downto 0 )
  );
end edgedtctr;

architecture STRUCTURE of edgedtctr is
  signal sreg_0 : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \^sreg_reg[2]_1\ : STD_LOGIC;
begin
  \sreg_reg[2]_1\ <= \^sreg_reg[2]_1\;
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \sreg_reg[0]_1\,
      Q => sreg_0(0),
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => sreg_0(0),
      Q => sreg_0(1),
      R => '0'
    );
\sreg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => sreg_0(1),
      Q => sreg_0(2),
      R => '0'
    );
\valor_moneda[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000A8AA00000000"
    )
        port map (
      I0 => \valor_moneda_reg[2]_2\,
      I1 => sreg_0(0),
      I2 => sreg_0(1),
      I3 => sreg_0(2),
      I4 => \valor_moneda_reg[2]\,
      I5 => \valor_moneda_reg[0]\,
      O => \sreg_reg[2]_0\(0)
    );
\valor_moneda[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"68000000"
    )
        port map (
      I0 => \^sreg_reg[2]_1\,
      I1 => \valor_moneda_reg[2]\,
      I2 => \valor_moneda_reg[2]_0\,
      I3 => \valor_moneda_reg[2]_1\,
      I4 => \valor_moneda_reg[2]_2\,
      O => \sreg_reg[2]_0\(1)
    );
\valor_moneda[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFEFEF00EFEFEFEF"
    )
        port map (
      I0 => sreg_0(0),
      I1 => sreg_0(1),
      I2 => sreg_0(2),
      I3 => sreg(0),
      I4 => sreg(1),
      I5 => sreg(2),
      O => \sreg_reg[0]_0\
    );
\valor_moneda[6]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => sreg_0(2),
      I1 => sreg_0(1),
      I2 => sreg_0(0),
      O => \^sreg_reg[2]_1\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity edgedtctr_10 is
  port (
    sreg : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \sreg_reg[0]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \sreg_reg[2]_0\ : out STD_LOGIC;
    \sreg_reg[0]_1\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    \valor_moneda_reg[3]\ : in STD_LOGIC;
    \valor_moneda_reg[3]_0\ : in STD_LOGIC;
    \valor_moneda_reg[3]_1\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of edgedtctr_10 : entity is "edgedtctr";
end edgedtctr_10;

architecture STRUCTURE of edgedtctr_10 is
  signal \^sreg\ : STD_LOGIC_VECTOR ( 2 downto 0 );
begin
  sreg(2 downto 0) <= \^sreg\(2 downto 0);
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \sreg_reg[0]_1\,
      Q => \^sreg\(0),
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \^sreg\(0),
      Q => \^sreg\(1),
      R => '0'
    );
\sreg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \^sreg\(1),
      Q => \^sreg\(2),
      R => '0'
    );
\valor_moneda[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5455000000000000"
    )
        port map (
      I0 => \valor_moneda_reg[3]\,
      I1 => \^sreg\(0),
      I2 => \^sreg\(1),
      I3 => \^sreg\(2),
      I4 => \valor_moneda_reg[3]_0\,
      I5 => \valor_moneda_reg[3]_1\,
      O => \sreg_reg[0]_0\(0)
    );
\valor_moneda[4]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => \^sreg\(2),
      I1 => \^sreg\(1),
      I2 => \^sreg\(0),
      O => \sreg_reg[2]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity edgedtctr_11 is
  port (
    \sreg_reg[2]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \sreg_reg[0]_0\ : out STD_LOGIC;
    \sreg_reg[2]_1\ : out STD_LOGIC;
    \sreg_reg[0]_1\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    \valor_moneda_reg[1]\ : in STD_LOGIC;
    \valor_moneda_reg[1]_0\ : in STD_LOGIC;
    \valor_moneda_reg[1]_1\ : in STD_LOGIC;
    sreg : in STD_LOGIC_VECTOR ( 2 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of edgedtctr_11 : entity is "edgedtctr";
end edgedtctr_11;

architecture STRUCTURE of edgedtctr_11 is
  signal sreg_0 : STD_LOGIC_VECTOR ( 2 downto 0 );
begin
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \sreg_reg[0]_1\,
      Q => sreg_0(0),
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => sreg_0(0),
      Q => sreg_0(1),
      R => '0'
    );
\sreg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => sreg_0(1),
      Q => sreg_0(2),
      R => '0'
    );
\valor_moneda[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000008088888808"
    )
        port map (
      I0 => \valor_moneda_reg[1]\,
      I1 => \valor_moneda_reg[1]_0\,
      I2 => sreg_0(2),
      I3 => sreg_0(1),
      I4 => sreg_0(0),
      I5 => \valor_moneda_reg[1]_1\,
      O => \sreg_reg[2]_0\(0)
    );
\valor_moneda[3]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => sreg_0(2),
      I1 => sreg_0(1),
      I2 => sreg_0(0),
      O => \sreg_reg[2]_1\
    );
\valor_moneda[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222220288888808"
    )
        port map (
      I0 => \valor_moneda_reg[1]\,
      I1 => \valor_moneda_reg[1]_0\,
      I2 => sreg_0(2),
      I3 => sreg_0(1),
      I4 => sreg_0(0),
      I5 => \valor_moneda_reg[1]_1\,
      O => \sreg_reg[2]_0\(1)
    );
\valor_moneda[6]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFEFEF00EFEFEFEF"
    )
        port map (
      I0 => sreg_0(0),
      I1 => sreg_0(1),
      I2 => sreg_0(2),
      I3 => sreg(0),
      I4 => sreg(1),
      I5 => sreg(2),
      O => \sreg_reg[0]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity edgedtctr_12 is
  port (
    sreg : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \sreg_reg[0]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \sreg_reg[2]_0\ : out STD_LOGIC;
    \sreg_reg[0]_1\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    \valor_moneda_reg[5]\ : in STD_LOGIC;
    \valor_moneda_reg[5]_0\ : in STD_LOGIC;
    \valor_moneda_reg[5]_1\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of edgedtctr_12 : entity is "edgedtctr";
end edgedtctr_12;

architecture STRUCTURE of edgedtctr_12 is
  signal \^sreg\ : STD_LOGIC_VECTOR ( 2 downto 0 );
begin
  sreg(2 downto 0) <= \^sreg\(2 downto 0);
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \sreg_reg[0]_1\,
      Q => \^sreg\(0),
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \^sreg\(0),
      Q => \^sreg\(1),
      R => '0'
    );
\sreg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \^sreg\(1),
      Q => \^sreg\(2),
      R => '0'
    );
\valor_moneda[2]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => \^sreg\(2),
      I1 => \^sreg\(1),
      I2 => \^sreg\(0),
      O => \sreg_reg[2]_0\
    );
\valor_moneda[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222220288888808"
    )
        port map (
      I0 => \valor_moneda_reg[5]\,
      I1 => \valor_moneda_reg[5]_0\,
      I2 => \^sreg\(2),
      I3 => \^sreg\(1),
      I4 => \^sreg\(0),
      I5 => \valor_moneda_reg[5]_1\,
      O => \sreg_reg[0]_0\(0)
    );
\valor_moneda[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000A8AA00000000"
    )
        port map (
      I0 => \valor_moneda_reg[5]_1\,
      I1 => \^sreg\(0),
      I2 => \^sreg\(1),
      I3 => \^sreg\(2),
      I4 => \valor_moneda_reg[5]_0\,
      I5 => \valor_moneda_reg[5]\,
      O => \sreg_reg[0]_0\(1)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity edgedtctr_13 is
  port (
    \disp_reg[4][1]\ : out STD_LOGIC;
    \sreg_reg[2]_0\ : out STD_LOGIC;
    \sreg_reg[0]_0\ : out STD_LOGIC;
    \disp_reg[8][6]\ : out STD_LOGIC;
    \disp_reg[5][6]\ : out STD_LOGIC;
    \disp_reg[5]_0_sp_1\ : out STD_LOGIC;
    \sreg_reg[0]_1\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    \disp_reg[4]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_reg[5][0]_0\ : in STD_LOGIC;
    \disp_reg[5][0]_1\ : in STD_LOGIC;
    \disp_reg[5][0]_2\ : in STD_LOGIC;
    \FSM_sequential_current_state[1]_i_6__0\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_reg[8]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_reg[5]\ : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of edgedtctr_13 : entity is "edgedtctr";
end edgedtctr_13;

architecture STRUCTURE of edgedtctr_13 is
  signal \disp_reg[5]_0_sn_1\ : STD_LOGIC;
  signal \^sreg_reg[2]_0\ : STD_LOGIC;
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  signal \sreg_reg_n_0_[1]\ : STD_LOGIC;
  signal \sreg_reg_n_0_[2]\ : STD_LOGIC;
begin
  \disp_reg[5]_0_sp_1\ <= \disp_reg[5]_0_sn_1\;
  \sreg_reg[2]_0\ <= \^sreg_reg[2]_0\;
\FSM_sequential_current_state[1]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"101010FF10101010"
    )
        port map (
      I0 => \sreg_reg_n_0_[0]\,
      I1 => \sreg_reg_n_0_[1]\,
      I2 => \sreg_reg_n_0_[2]\,
      I3 => \FSM_sequential_current_state[1]_i_6__0\,
      I4 => Q(0),
      I5 => Q(1),
      O => \sreg_reg[0]_0\
    );
\disp[3][6]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => \sreg_reg_n_0_[2]\,
      I1 => \sreg_reg_n_0_[1]\,
      I2 => \sreg_reg_n_0_[0]\,
      O => \^sreg_reg[2]_0\
    );
\disp[4][1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B2EA2AAA"
    )
        port map (
      I0 => \disp_reg[4]\(0),
      I1 => \^sreg_reg[2]_0\,
      I2 => \disp_reg[5][0]_0\,
      I3 => \disp_reg[5][0]_1\,
      I4 => \disp_reg[5][0]_2\,
      O => \disp_reg[4][1]\
    );
\disp[5][0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B2EA2AAA"
    )
        port map (
      I0 => \disp_reg[5]\(0),
      I1 => \^sreg_reg[2]_0\,
      I2 => \disp_reg[5][0]_0\,
      I3 => \disp_reg[5][0]_1\,
      I4 => \disp_reg[5][0]_2\,
      O => \disp_reg[5]_0_sn_1\
    );
\disp[5][6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B2EAEAAA"
    )
        port map (
      I0 => \disp_reg[5]\(1),
      I1 => \^sreg_reg[2]_0\,
      I2 => \disp_reg[5][0]_0\,
      I3 => \disp_reg[5][0]_1\,
      I4 => \disp_reg[5][0]_2\,
      O => \disp_reg[5][6]\
    );
\disp[8][6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B22AEAAA"
    )
        port map (
      I0 => \disp_reg[8]\(0),
      I1 => \^sreg_reg[2]_0\,
      I2 => \disp_reg[5][0]_0\,
      I3 => \disp_reg[5][0]_1\,
      I4 => \disp_reg[5][0]_2\,
      O => \disp_reg[8][6]\
    );
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \sreg_reg[0]_1\,
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg_n_0_[1]\,
      R => '0'
    );
\sreg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \sreg_reg_n_0_[1]\,
      Q => \sreg_reg_n_0_[2]\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity edgedtctr_14 is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    \sreg_reg[2]_0\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \sreg_reg[0]_1\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of edgedtctr_14 : entity is "edgedtctr";
end edgedtctr_14;

architecture STRUCTURE of edgedtctr_14 is
  signal \^q\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^sreg_reg[0]_0\ : STD_LOGIC;
begin
  Q(1 downto 0) <= \^q\(1 downto 0);
  \sreg_reg[0]_0\ <= \^sreg_reg[0]_0\;
\disp[3][6]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^sreg_reg[0]_0\,
      O => \sreg_reg[2]_0\
    );
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \sreg_reg[0]_1\,
      Q => \^sreg_reg[0]_0\,
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \^sreg_reg[0]_0\,
      Q => \^q\(0),
      R => '0'
    );
\sreg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \^q\(0),
      Q => \^q\(1),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity edgedtctr_15 is
  port (
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_reg[7][6]\ : out STD_LOGIC;
    \sreg_reg[2]_0\ : out STD_LOGIC;
    \sreg_reg[2]_1\ : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \sreg_reg[2]_2\ : out STD_LOGIC;
    p_0_out : out STD_LOGIC_VECTOR ( 0 to 0 );
    \sreg_reg[0]_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \sreg_reg[0]_1\ : out STD_LOGIC;
    \sreg_reg[0]_2\ : out STD_LOGIC;
    \sreg_reg[0]_3\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    \disp_reg[2][0]\ : in STD_LOGIC;
    \disp_reg[2][0]_0\ : in STD_LOGIC;
    \disp_reg[2][0]_1\ : in STD_LOGIC;
    \disp_reg[7]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \FSM_sequential_current_state[1]_i_6__0\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_reg[3][3]\ : in STD_LOGIC;
    \disp_reg[3][3]_0\ : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of edgedtctr_15 : entity is "edgedtctr";
end edgedtctr_15;

architecture STRUCTURE of edgedtctr_15 is
  signal \^sreg_reg[2]_0\ : STD_LOGIC;
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  signal \sreg_reg_n_0_[1]\ : STD_LOGIC;
  signal \sreg_reg_n_0_[2]\ : STD_LOGIC;
begin
  \sreg_reg[2]_0\ <= \^sreg_reg[2]_0\;
\FSM_sequential_current_state[1]_i_8__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"101010FF10101010"
    )
        port map (
      I0 => \sreg_reg_n_0_[0]\,
      I1 => \sreg_reg_n_0_[1]\,
      I2 => \sreg_reg_n_0_[2]\,
      I3 => \FSM_sequential_current_state[1]_i_6__0\,
      I4 => Q(0),
      I5 => Q(1),
      O => \sreg_reg[0]_1\
    );
\disp[1][6]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => \sreg_reg_n_0_[2]\,
      I1 => \sreg_reg_n_0_[1]\,
      I2 => \sreg_reg_n_0_[0]\,
      O => \^sreg_reg[2]_0\
    );
\disp[2][0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF10FF10FFFFFF"
    )
        port map (
      I0 => \sreg_reg_n_0_[0]\,
      I1 => \sreg_reg_n_0_[1]\,
      I2 => \sreg_reg_n_0_[2]\,
      I3 => \disp_reg[2][0]\,
      I4 => \disp_reg[2][0]_1\,
      I5 => \disp_reg[2][0]_0\,
      O => D(0)
    );
\disp[2][4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFFF10FF10FFFFFF"
    )
        port map (
      I0 => \sreg_reg_n_0_[0]\,
      I1 => \sreg_reg_n_0_[1]\,
      I2 => \sreg_reg_n_0_[2]\,
      I3 => \disp_reg[2][0]\,
      I4 => \disp_reg[2][0]_0\,
      I5 => \disp_reg[2][0]_1\,
      O => D(1)
    );
\disp[3][1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EF10FFFFFFFFFFFF"
    )
        port map (
      I0 => \sreg_reg_n_0_[0]\,
      I1 => \sreg_reg_n_0_[1]\,
      I2 => \sreg_reg_n_0_[2]\,
      I3 => \disp_reg[2][0]_1\,
      I4 => \disp_reg[2][0]\,
      I5 => \disp_reg[2][0]_0\,
      O => \sreg_reg[0]_0\(0)
    );
\disp[3][3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFEFEF00EFEFEFEF"
    )
        port map (
      I0 => \sreg_reg_n_0_[0]\,
      I1 => \sreg_reg_n_0_[1]\,
      I2 => \sreg_reg_n_0_[2]\,
      I3 => \disp_reg[3][3]\,
      I4 => \disp_reg[3][3]_0\(0),
      I5 => \disp_reg[3][3]_0\(1),
      O => \sreg_reg[0]_2\
    );
\disp[3][4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0200000000000000"
    )
        port map (
      I0 => \sreg_reg_n_0_[2]\,
      I1 => \sreg_reg_n_0_[1]\,
      I2 => \sreg_reg_n_0_[0]\,
      I3 => \disp_reg[2][0]_0\,
      I4 => \disp_reg[2][0]\,
      I5 => \disp_reg[2][0]_1\,
      O => \sreg_reg[0]_0\(1)
    );
\disp[3][6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6668666688808888"
    )
        port map (
      I0 => \disp_reg[2][0]_0\,
      I1 => \disp_reg[2][0]\,
      I2 => \sreg_reg_n_0_[0]\,
      I3 => \sreg_reg_n_0_[1]\,
      I4 => \sreg_reg_n_0_[2]\,
      I5 => \disp_reg[2][0]_1\,
      O => E(0)
    );
\disp[3][6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF10FFFFFFFFFF"
    )
        port map (
      I0 => \sreg_reg_n_0_[0]\,
      I1 => \sreg_reg_n_0_[1]\,
      I2 => \sreg_reg_n_0_[2]\,
      I3 => \disp_reg[2][0]\,
      I4 => \disp_reg[2][0]_1\,
      I5 => \disp_reg[2][0]_0\,
      O => \sreg_reg[0]_0\(2)
    );
\disp[5][4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FD0000000000"
    )
        port map (
      I0 => \sreg_reg_n_0_[2]\,
      I1 => \sreg_reg_n_0_[1]\,
      I2 => \sreg_reg_n_0_[0]\,
      I3 => \disp_reg[2][0]_0\,
      I4 => \disp_reg[2][0]_1\,
      I5 => \disp_reg[2][0]\,
      O => \sreg_reg[2]_1\
    );
\disp[6][3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0200A8AA00000000"
    )
        port map (
      I0 => \disp_reg[2][0]\,
      I1 => \sreg_reg_n_0_[0]\,
      I2 => \sreg_reg_n_0_[1]\,
      I3 => \sreg_reg_n_0_[2]\,
      I4 => \disp_reg[2][0]_1\,
      I5 => \disp_reg[2][0]_0\,
      O => p_0_out(0)
    );
\disp[7][3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5559AAA200000000"
    )
        port map (
      I0 => \disp_reg[2][0]_1\,
      I1 => \sreg_reg_n_0_[2]\,
      I2 => \sreg_reg_n_0_[1]\,
      I3 => \sreg_reg_n_0_[0]\,
      I4 => \disp_reg[2][0]\,
      I5 => \disp_reg[2][0]_0\,
      O => \sreg_reg[2]_2\
    );
\disp[7][6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BF7F2800"
    )
        port map (
      I0 => \^sreg_reg[2]_0\,
      I1 => \disp_reg[2][0]_1\,
      I2 => \disp_reg[2][0]_0\,
      I3 => \disp_reg[2][0]\,
      I4 => \disp_reg[7]\(0),
      O => \disp_reg[7][6]\
    );
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \sreg_reg[0]_3\,
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg_n_0_[1]\,
      R => '0'
    );
\sreg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \sreg_reg_n_0_[1]\,
      Q => \sreg_reg_n_0_[2]\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity edgedtctr_16 is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    \disp_reg[4][4]\ : out STD_LOGIC;
    \sreg_reg[2]_0\ : out STD_LOGIC;
    \disp_reg[7][3]\ : out STD_LOGIC;
    \sreg_reg[2]_1\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \sreg_reg[0]_1\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    \disp_reg[4]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_reg[3][3]\ : in STD_LOGIC;
    \disp_reg[4][4]_0\ : in STD_LOGIC;
    \disp_reg[4][4]_1\ : in STD_LOGIC;
    \disp_reg[7][3]_0\ : in STD_LOGIC;
    \disp_reg[7]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_reg[3][3]_0\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of edgedtctr_16 : entity is "edgedtctr";
end edgedtctr_16;

architecture STRUCTURE of edgedtctr_16 is
  signal \^q\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^sreg_reg[0]_0\ : STD_LOGIC;
  signal \^sreg_reg[2]_0\ : STD_LOGIC;
  signal \^sreg_reg[2]_1\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \disp[3][3]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \disp[3][6]_i_4\ : label is "soft_lutpair0";
begin
  Q(1 downto 0) <= \^q\(1 downto 0);
  \sreg_reg[0]_0\ <= \^sreg_reg[0]_0\;
  \sreg_reg[2]_0\ <= \^sreg_reg[2]_0\;
  \sreg_reg[2]_1\ <= \^sreg_reg[2]_1\;
\disp[3][3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"22222202"
    )
        port map (
      I0 => \disp_reg[3][3]_0\,
      I1 => \disp_reg[3][3]\,
      I2 => \^q\(1),
      I3 => \^q\(0),
      I4 => \^sreg_reg[0]_0\,
      O => \^sreg_reg[2]_1\
    );
\disp[3][6]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^sreg_reg[0]_0\,
      O => \^sreg_reg[2]_0\
    );
\disp[4][4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"822AEAAA"
    )
        port map (
      I0 => \disp_reg[4]\(0),
      I1 => \^sreg_reg[2]_0\,
      I2 => \disp_reg[3][3]\,
      I3 => \disp_reg[4][4]_0\,
      I4 => \disp_reg[4][4]_1\,
      O => \disp_reg[4][4]\
    );
\disp[7][3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \^sreg_reg[2]_1\,
      I1 => \disp_reg[7][3]_0\,
      I2 => \disp_reg[7]\(0),
      O => \disp_reg[7][3]\
    );
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \sreg_reg[0]_1\,
      Q => \^sreg_reg[0]_0\,
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \^sreg_reg[0]_0\,
      Q => \^q\(0),
      R => '0'
    );
\sreg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \^q\(0),
      Q => \^q\(1),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity edgedtctr_17 is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    \sreg_reg[1]_0\ : out STD_LOGIC;
    \sreg_reg[0]_1\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    \FSM_sequential_current_state[1]_i_4__0\ : in STD_LOGIC;
    \FSM_sequential_current_state[1]_i_4__0_0\ : in STD_LOGIC;
    current_state : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of edgedtctr_17 : entity is "edgedtctr";
end edgedtctr_17;

architecture STRUCTURE of edgedtctr_17 is
  signal \FSM_sequential_current_state[1]_i_9__0_n_0\ : STD_LOGIC;
  signal sreg : STD_LOGIC_VECTOR ( 2 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_current_state[1]_i_5\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \FSM_sequential_current_state[1]_i_9__0\ : label is "soft_lutpair1";
begin
\FSM_sequential_current_state[1]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => sreg(1),
      I1 => sreg(2),
      I2 => sreg(0),
      O => \sreg_reg[1]_0\
    );
\FSM_sequential_current_state[1]_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000F00000EEEE"
    )
        port map (
      I0 => \FSM_sequential_current_state[1]_i_4__0\,
      I1 => \FSM_sequential_current_state[1]_i_4__0_0\,
      I2 => \FSM_sequential_current_state[1]_i_9__0_n_0\,
      I3 => sreg(0),
      I4 => current_state(1),
      I5 => current_state(0),
      O => \sreg_reg[0]_0\
    );
\FSM_sequential_current_state[1]_i_9__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => sreg(2),
      I1 => sreg(1),
      O => \FSM_sequential_current_state[1]_i_9__0_n_0\
    );
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \sreg_reg[0]_1\,
      Q => sreg(0),
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => sreg(0),
      Q => sreg(1),
      R => '0'
    );
\sreg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => sreg(1),
      Q => sreg(2),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity edgedtctr_9 is
  port (
    \sreg_reg[2]_0\ : out STD_LOGIC;
    \sreg_reg[0]_0\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of edgedtctr_9 : entity is "edgedtctr";
end edgedtctr_9;

architecture STRUCTURE of edgedtctr_9 is
  signal sreg : STD_LOGIC_VECTOR ( 2 downto 0 );
begin
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \sreg_reg[0]_0\,
      Q => sreg(0),
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => sreg(0),
      Q => sreg(1),
      R => '0'
    );
\sreg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => sreg(1),
      Q => sreg(2),
      R => '0'
    );
\valor_moneda[6]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => sreg(2),
      I1 => sreg(1),
      I2 => sreg(0),
      O => \sreg_reg[2]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gestion_refresco is
  port (
    \disp_reg[3]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \disp_reg[5]\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \disp_reg[6]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_reg[7][6]_0\ : out STD_LOGIC;
    \disp_reg[7][3]_0\ : out STD_LOGIC;
    \disp_reg[4]\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_reg[8]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_reg[2][4]_0\ : out STD_LOGIC;
    \disp_reg[1][6]_0\ : out STD_LOGIC;
    \disp_reg[2][4]_1\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \FSM_sequential_current_state_reg[1]\ : out STD_LOGIC;
    \disp_reg[1]\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_reg[7][6]_1\ : out STD_LOGIC;
    \disp_reg[7][6]_2\ : out STD_LOGIC;
    dot : out STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_reg[3][3]_0\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    \disp_reg[5][4]_0\ : in STD_LOGIC;
    p_0_out : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_reg[7][6]_3\ : in STD_LOGIC;
    \disp_reg[7][3]_1\ : in STD_LOGIC;
    \disp_reg[4][4]_0\ : in STD_LOGIC;
    \disp_reg[4][1]_0\ : in STD_LOGIC;
    \disp_reg[5][0]_0\ : in STD_LOGIC;
    \disp_reg[8][6]_0\ : in STD_LOGIC;
    \disp_reg[5][6]_0\ : in STD_LOGIC;
    \caracter_reg[4]\ : in STD_LOGIC;
    \caracter_reg[4]_0\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    dot_reg : in STD_LOGIC;
    dot_reg_0 : in STD_LOGIC;
    dot_reg_1 : in STD_LOGIC;
    \charact_dot_reg[0]\ : in STD_LOGIC;
    dot_reg_2 : in STD_LOGIC;
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \disp_reg[2][4]_2\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_reg[1][3]_0\ : in STD_LOGIC;
    \disp_reg[1][3]_1\ : in STD_LOGIC;
    \disp_reg[1][3]_2\ : in STD_LOGIC;
    \disp_reg[1][3]_3\ : in STD_LOGIC
  );
end gestion_refresco;

architecture STRUCTURE of gestion_refresco is
  signal \disp[1][3]_i_1_n_0\ : STD_LOGIC;
  signal \disp[1][6]_i_1_n_0\ : STD_LOGIC;
  signal \^disp_reg[1]\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^disp_reg[1][6]_0\ : STD_LOGIC;
  signal \^disp_reg[2][4]_1\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^disp_reg[7][3]_0\ : STD_LOGIC;
  signal \^disp_reg[7][6]_0\ : STD_LOGIC;
  signal \^disp_reg[7][6]_1\ : STD_LOGIC;
begin
  \disp_reg[1]\(1 downto 0) <= \^disp_reg[1]\(1 downto 0);
  \disp_reg[1][6]_0\ <= \^disp_reg[1][6]_0\;
  \disp_reg[2][4]_1\(1 downto 0) <= \^disp_reg[2][4]_1\(1 downto 0);
  \disp_reg[7][3]_0\ <= \^disp_reg[7][3]_0\;
  \disp_reg[7][6]_0\ <= \^disp_reg[7][6]_0\;
  \disp_reg[7][6]_1\ <= \^disp_reg[7][6]_1\;
\caracter[4]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F888FFFFF888F888"
    )
        port map (
      I0 => \^disp_reg[1][6]_0\,
      I1 => \^disp_reg[2][4]_1\(1),
      I2 => \caracter_reg[4]\,
      I3 => \caracter_reg[4]_0\,
      I4 => Q(0),
      I5 => dot_reg,
      O => \disp_reg[2][4]_0\
    );
\caracter[6]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0040000000000000"
    )
        port map (
      I0 => \^disp_reg[1]\(1),
      I1 => \^disp_reg[1]\(0),
      I2 => dot_reg_2,
      I3 => dot_reg_1,
      I4 => Q(0),
      I5 => Q(1),
      O => \^disp_reg[1][6]_0\
    );
\charact_dot[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00000200"
    )
        port map (
      I0 => dot_reg_0,
      I1 => dot_reg_1,
      I2 => \charact_dot_reg[0]\,
      I3 => \^disp_reg[7][3]_0\,
      I4 => \^disp_reg[7][6]_0\,
      I5 => \^disp_reg[1][6]_0\,
      O => \FSM_sequential_current_state_reg[1]\
    );
\disp[1][3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"822AEAAA"
    )
        port map (
      I0 => \^disp_reg[1]\(0),
      I1 => \disp_reg[1][3]_0\,
      I2 => \disp_reg[1][3]_1\,
      I3 => \disp_reg[1][3]_2\,
      I4 => \disp_reg[1][3]_3\,
      O => \disp[1][3]_i_1_n_0\
    );
\disp[1][6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BEEA2AAA"
    )
        port map (
      I0 => \^disp_reg[1]\(1),
      I1 => \disp_reg[1][3]_0\,
      I2 => \disp_reg[1][3]_1\,
      I3 => \disp_reg[1][3]_2\,
      I4 => \disp_reg[1][3]_3\,
      O => \disp[1][6]_i_1_n_0\
    );
\disp_reg[1][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \disp[1][3]_i_1_n_0\,
      Q => \^disp_reg[1]\(0),
      R => '0'
    );
\disp_reg[1][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \disp[1][6]_i_1_n_0\,
      Q => \^disp_reg[1]\(1),
      R => '0'
    );
\disp_reg[2][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => E(0),
      D => \disp_reg[2][4]_2\(0),
      Q => \^disp_reg[2][4]_1\(0),
      R => '0'
    );
\disp_reg[2][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => E(0),
      D => \disp_reg[2][4]_2\(1),
      Q => \^disp_reg[2][4]_1\(1),
      R => '0'
    );
\disp_reg[3][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => E(0),
      D => D(0),
      Q => \disp_reg[3]\(0),
      R => '0'
    );
\disp_reg[3][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => E(0),
      D => \disp_reg[3][3]_0\,
      Q => \disp_reg[3]\(1),
      R => '0'
    );
\disp_reg[3][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => E(0),
      D => D(1),
      Q => \disp_reg[3]\(2),
      R => '0'
    );
\disp_reg[3][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => E(0),
      D => D(2),
      Q => \disp_reg[3]\(3),
      R => '0'
    );
\disp_reg[4][1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \disp_reg[4][1]_0\,
      Q => \disp_reg[4]\(0),
      R => '0'
    );
\disp_reg[4][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \disp_reg[4][4]_0\,
      Q => \disp_reg[4]\(1),
      R => '0'
    );
\disp_reg[5][0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \disp_reg[5][0]_0\,
      Q => \disp_reg[5]\(0),
      R => '0'
    );
\disp_reg[5][4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => E(0),
      D => \disp_reg[5][4]_0\,
      Q => \disp_reg[5]\(1),
      R => '0'
    );
\disp_reg[5][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \disp_reg[5][6]_0\,
      Q => \disp_reg[5]\(2),
      R => '0'
    );
\disp_reg[6][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => E(0),
      D => p_0_out(0),
      Q => \disp_reg[6]\(0),
      R => '0'
    );
\disp_reg[7][3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \disp_reg[7][3]_1\,
      Q => \^disp_reg[7][3]_0\,
      R => '0'
    );
\disp_reg[7][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \disp_reg[7][6]_3\,
      Q => \^disp_reg[7][6]_0\,
      R => '0'
    );
\disp_reg[8][6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \disp_reg[8][6]_0\,
      Q => \disp_reg[8]\(0),
      R => '0'
    );
dot_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFFFEFEF"
    )
        port map (
      I0 => \^disp_reg[7][6]_1\,
      I1 => \^disp_reg[1][6]_0\,
      I2 => CO(0),
      I3 => Q(0),
      I4 => dot_reg,
      O => dot
    );
dot_i_17: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \^disp_reg[7][6]_0\,
      I1 => \^disp_reg[7][3]_0\,
      O => \disp_reg[7][6]_2\
    );
dot_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000040000000000"
    )
        port map (
      I0 => \^disp_reg[7][6]_0\,
      I1 => \^disp_reg[7][3]_0\,
      I2 => Q(1),
      I3 => Q(0),
      I4 => dot_reg_1,
      I5 => dot_reg_0,
      O => \^disp_reg[7][6]_1\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity int_to_string is
  port (
    \charact_dot_reg[0]\ : out STD_LOGIC;
    \reg_reg[0]\ : out STD_LOGIC;
    \disp_dinero[5]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \reg_reg[0]_0\ : out STD_LOGIC;
    \charact_dot_reg[0]_0\ : out STD_LOGIC;
    \total_string_reg[0]\ : out STD_LOGIC;
    \charact_dot_reg[0]_1\ : out STD_LOGIC;
    \disp_dinero[3]_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \reg_reg[0]_1\ : out STD_LOGIC;
    \caracter_reg[1]_i_15_0\ : out STD_LOGIC;
    \caracter_reg[1]_i_16_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter_reg[0]\ : in STD_LOGIC;
    \caracter_reg[0]_0\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter_reg[0]_1\ : in STD_LOGIC;
    \caracter_reg[0]_2\ : in STD_LOGIC;
    \disp_reg[3]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter_reg[1]\ : in STD_LOGIC;
    \caracter[2]_i_16\ : in STD_LOGIC;
    \caracter_reg[3]_i_126_0\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    \caracter_reg[2]\ : in STD_LOGIC;
    \caracter_reg[1]_0\ : in STD_LOGIC;
    \caracter_reg[1]_1\ : in STD_LOGIC
  );
end int_to_string;

architecture STRUCTURE of int_to_string is
  signal aD2M4dsP : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \caracter[0]_i_100_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_101_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_102_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_103_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_104_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_105_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_107_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_108_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_109_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_10_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_110_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_112_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_113_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_114_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_115_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_116_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_117_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_118_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_119_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_11_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_121_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_122_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_123_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_124_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_125_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_126_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_127_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_128_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_129_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_12_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_131_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_132_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_133_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_134_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_135_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_136_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_137_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_138_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_140_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_141_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_143_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_146_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_147_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_148_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_149_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_150_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_151_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_152_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_153_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_154_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_155_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_156_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_157_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_158_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_159_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_160_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_161_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_162_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_163_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_164_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_169_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_170_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_171_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_172_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_173_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_174_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_175_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_176_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_177_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_178_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_179_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_180_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_185_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_186_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_187_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_188_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_189_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_190_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_191_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_192_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_193_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_194_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_195_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_196_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_197_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_198_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_199_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_200_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_201_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_202_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_203_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_204_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_205_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_206_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_207_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_208_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_209_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_210_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_211_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_212_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_213_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_214_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_215_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_216_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_217_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_218_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_219_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_220_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_222_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_223_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_224_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_225_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_226_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_227_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_228_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_229_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_232_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_233_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_234_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_235_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_236_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_237_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_238_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_239_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_241_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_242_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_243_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_244_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_245_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_246_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_247_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_248_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_254_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_255_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_256_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_257_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_258_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_259_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_25_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_260_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_261_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_263_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_264_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_266_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_268_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_270_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_271_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_272_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_273_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_274_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_275_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_276_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_277_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_278_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_279_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_280_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_281_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_283_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_284_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_285_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_286_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_287_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_288_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_289_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_290_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_291_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_292_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_293_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_294_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_295_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_296_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_297_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_298_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_299_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_29_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_300_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_301_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_302_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_303_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_304_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_305_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_30_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_311_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_312_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_313_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_314_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_315_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_316_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_317_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_318_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_320_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_321_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_322_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_323_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_324_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_325_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_326_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_327_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_329_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_330_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_331_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_332_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_333_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_334_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_335_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_336_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_339_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_340_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_341_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_342_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_343_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_344_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_345_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_346_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_347_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_349_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_34_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_351_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_352_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_353_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_354_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_355_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_356_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_357_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_359_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_35_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_360_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_362_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_363_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_364_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_365_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_366_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_367_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_368_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_369_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_36_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_371_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_372_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_374_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_379_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_380_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_381_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_382_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_383_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_384_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_385_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_386_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_387_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_388_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_389_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_38_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_390_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_391_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_392_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_393_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_394_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_395_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_396_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_397_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_398_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_399_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_39_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_400_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_401_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_402_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_403_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_404_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_405_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_406_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_407_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_408_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_409_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_40_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_412_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_413_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_414_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_415_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_416_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_417_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_418_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_419_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_41_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_421_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_422_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_423_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_424_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_425_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_426_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_427_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_428_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_42_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_430_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_431_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_432_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_433_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_434_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_435_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_436_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_437_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_439_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_43_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_440_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_441_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_442_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_443_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_444_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_445_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_446_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_449_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_44_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_450_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_451_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_452_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_453_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_454_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_455_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_456_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_457_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_458_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_459_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_45_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_464_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_465_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_466_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_467_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_468_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_469_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_470_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_471_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_472_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_473_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_474_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_475_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_476_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_477_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_478_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_479_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_47_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_480_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_481_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_482_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_483_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_485_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_486_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_487_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_488_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_489_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_48_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_490_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_491_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_492_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_493_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_494_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_495_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_496_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_498_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_499_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_49_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_500_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_501_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_502_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_503_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_504_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_505_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_507_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_508_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_509_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_50_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_510_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_511_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_512_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_513_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_514_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_515_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_516_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_517_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_518_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_519_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_51_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_520_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_521_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_522_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_523_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_524_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_525_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_526_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_527_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_528_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_529_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_52_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_532_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_534_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_536_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_537_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_538_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_539_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_53_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_540_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_541_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_542_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_543_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_544_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_545_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_546_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_547_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_548_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_54_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_553_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_554_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_555_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_556_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_558_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_559_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_560_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_561_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_562_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_563_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_564_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_565_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_566_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_567_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_568_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_569_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_570_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_571_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_572_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_573_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_574_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_575_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_576_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_577_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_578_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_579_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_57_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_580_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_581_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_582_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_583_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_584_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_585_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_586_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_587_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_588_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_589_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_58_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_590_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_591_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_596_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_597_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_598_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_599_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_59_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_600_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_601_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_602_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_603_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_604_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_606_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_608_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_609_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_60_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_610_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_611_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_612_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_613_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_614_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_615_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_616_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_617_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_618_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_619_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_620_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_621_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_62_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_63_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_64_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_65_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_66_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_67_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_68_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_69_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_73_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_74_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_75_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_76_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_77_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_78_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_79_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_80_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_85_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_86_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_88_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_90_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_92_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_95_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_96_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_98_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_99_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_100_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_101_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_105_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_106_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_107_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_108_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_109_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_110_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_111_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_112_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_113_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_114_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_115_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_116_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_117_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_118_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_119_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_120_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_121_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_122_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_123_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_125_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_126_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_127_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_128_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_129_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_130_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_131_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_132_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_133_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_134_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_135_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_136_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_137_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_138_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_139_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_140_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_142_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_143_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_144_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_145_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_146_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_147_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_148_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_149_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_151_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_153_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_154_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_155_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_156_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_157_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_158_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_159_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_160_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_162_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_163_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_164_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_165_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_167_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_168_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_169_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_170_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_171_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_172_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_173_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_174_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_175_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_176_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_177_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_178_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_179_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_180_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_182_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_183_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_184_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_185_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_186_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_187_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_188_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_189_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_18_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_191_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_192_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_193_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_194_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_195_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_196_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_197_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_198_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_19_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_202_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_203_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_204_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_205_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_207_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_208_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_209_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_210_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_211_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_212_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_213_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_214_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_217_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_218_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_219_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_220_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_221_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_222_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_223_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_224_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_226_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_227_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_228_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_229_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_230_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_231_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_232_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_233_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_234_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_235_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_236_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_237_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_238_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_240_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_241_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_242_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_243_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_244_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_245_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_246_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_247_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_24_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_251_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_252_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_253_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_254_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_255_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_256_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_258_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_259_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_25_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_260_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_261_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_262_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_263_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_264_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_265_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_266_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_267_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_268_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_269_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_26_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_270_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_271_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_272_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_273_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_274_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_275_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_276_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_277_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_278_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_279_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_27_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_280_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_281_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_282_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_283_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_284_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_286_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_287_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_288_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_289_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_28_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_290_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_291_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_292_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_293_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_295_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_296_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_297_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_298_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_299_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_29_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_300_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_301_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_302_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_304_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_305_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_306_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_307_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_308_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_309_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_30_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_311_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_312_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_313_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_314_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_315_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_316_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_318_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_319_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_31_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_320_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_321_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_322_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_323_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_324_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_325_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_326_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_327_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_328_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_329_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_32_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_330_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_331_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_332_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_334_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_335_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_336_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_337_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_338_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_339_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_340_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_341_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_342_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_343_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_344_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_345_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_346_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_347_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_348_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_349_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_350_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_351_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_352_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_355_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_356_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_357_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_358_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_359_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_35_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_361_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_362_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_363_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_365_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_366_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_367_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_368_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_369_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_36_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_370_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_371_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_372_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_37_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_41_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_42_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_45_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_46_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_48_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_49_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_50_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_51_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_52_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_53_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_54_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_55_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_56_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_57_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_58_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_59_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_61_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_62_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_63_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_64_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_65_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_67_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_68_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_69_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_70_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_71_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_72_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_73_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_74_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_81_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_82_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_83_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_84_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_86_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_87_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_88_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_89_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_91_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_93_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_94_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_95_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_96_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_97_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_98_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_99_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_10_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_11_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_12_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_13_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_21_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_23_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_24_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_31_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_32_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_33_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_34_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_100_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_101_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_102_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_103_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_104_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_105_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_106_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_107_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_109_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_10_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_110_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_111_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_112_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_113_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_114_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_115_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_116_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_118_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_119_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_11_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_120_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_121_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_122_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_123_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_124_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_125_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_128_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_129_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_12_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_130_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_131_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_132_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_133_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_134_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_136_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_137_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_138_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_139_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_140_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_141_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_142_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_143_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_144_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_145_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_146_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_147_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_148_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_149_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_150_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_151_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_152_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_153_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_154_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_155_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_156_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_157_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_158_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_159_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_160_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_161_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_162_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_163_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_164_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_165_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_166_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_167_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_168_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_169_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_16_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_170_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_171_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_172_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_175_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_177_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_178_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_179_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_17_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_180_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_181_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_182_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_184_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_185_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_186_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_187_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_188_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_189_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_18_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_190_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_191_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_192_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_193_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_194_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_195_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_196_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_197_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_198_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_19_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_20_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_22_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_23_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_24_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_25_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_26_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_28_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_29_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_30_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_33_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_34_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_35_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_36_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_37_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_38_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_39_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_40_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_41_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_42_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_43_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_46_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_47_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_48_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_49_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_50_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_51_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_53_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_54_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_55_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_56_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_57_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_58_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_59_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_62_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_63_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_64_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_65_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_66_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_67_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_68_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_69_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_73_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_74_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_76_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_77_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_78_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_79_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_80_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_81_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_82_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_83_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_85_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_86_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_87_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_89_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_90_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_91_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_92_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_93_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_95_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_96_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_97_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_98_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_9_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_106_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_106_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_106_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_106_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_111_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_111_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_111_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_111_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_111_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_111_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_111_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_111_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_120_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_120_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_120_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_120_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_120_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_120_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_120_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_120_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_130_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_130_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_130_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_130_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_139_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_139_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_139_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_139_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_139_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_139_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_139_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_139_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_142_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_142_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_142_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_142_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_142_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_142_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_142_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_142_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_144_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_144_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_144_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_144_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_144_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_144_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_144_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_144_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_145_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_145_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_145_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_145_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_145_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_145_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_145_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_145_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_20_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_21_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_221_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_221_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_221_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_221_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_22_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_230_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_230_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_230_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_230_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_231_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_231_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_231_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_231_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_231_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_231_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_231_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_231_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_23_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_23_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_23_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_240_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_240_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_240_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_240_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_240_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_240_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_240_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_240_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_249_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_249_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_249_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_249_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_249_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_249_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_249_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_249_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_24_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_24_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_24_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_24_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_24_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_24_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_24_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_24_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_250_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_250_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_250_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_250_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_251_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_251_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_251_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_251_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_251_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_251_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_252_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_252_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_252_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_252_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_253_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_253_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_253_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_253_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_262_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_262_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_262_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_262_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_262_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_262_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_262_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_262_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_265_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_265_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_265_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_265_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_265_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_265_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_265_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_265_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_267_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_267_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_267_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_267_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_267_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_267_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_267_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_267_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_269_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_269_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_269_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_269_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_269_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_269_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_269_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_269_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_28_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_28_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_28_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_28_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_306_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_306_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_306_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_306_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_307_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_307_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_307_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_307_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_308_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_308_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_308_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_308_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_310_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_310_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_310_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_310_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_319_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_319_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_319_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_319_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_319_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_319_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_319_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_319_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_31_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_31_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_31_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_31_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_328_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_328_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_328_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_328_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_328_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_328_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_328_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_328_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_32_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_32_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_32_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_32_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_32_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_32_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_32_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_32_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_337_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_337_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_337_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_337_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_337_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_337_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_337_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_337_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_338_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_338_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_338_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_338_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_338_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_338_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_338_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_338_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_33_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_33_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_33_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_33_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_33_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_33_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_33_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_33_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_350_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_350_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_350_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_350_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_350_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_350_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_350_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_350_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_361_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_361_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_361_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_361_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_375_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_375_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_375_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_375_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_375_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_375_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_375_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_376_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_376_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_376_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_376_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_376_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_376_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_376_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_376_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_377_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_377_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_377_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_377_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_377_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_377_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_377_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_377_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_37_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_37_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_37_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_37_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_420_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_420_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_420_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_420_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_429_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_429_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_429_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_429_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_429_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_429_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_429_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_429_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_438_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_438_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_438_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_438_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_438_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_438_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_438_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_438_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_447_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_447_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_447_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_447_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_447_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_447_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_447_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_447_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_448_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_448_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_448_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_448_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_448_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_448_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_448_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_448_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_46_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_46_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_46_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_46_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_484_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_484_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_484_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_484_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_484_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_484_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_484_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_484_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_497_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_497_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_497_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_497_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_497_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_497_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_497_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_497_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_506_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_506_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_506_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_506_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_530_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_530_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_530_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_530_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_530_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_530_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_530_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_530_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_531_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_531_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_533_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_533_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_533_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_533_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_533_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_533_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_533_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_533_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_535_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_535_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_535_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_535_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_535_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_535_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_535_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_535_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_557_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_557_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_557_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_557_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_557_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_55_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_55_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_55_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_55_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_56_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_56_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_56_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_56_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_56_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_56_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_56_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_605_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_605_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_605_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_605_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_61_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_61_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_61_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_61_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_61_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_61_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_61_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_61_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_70_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_70_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_70_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_70_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_70_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_70_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_70_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_70_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_71_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_72_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_72_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_72_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_72_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_81_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_81_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_81_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_81_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_81_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_81_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_81_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_81_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_82_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_82_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_82_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_82_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_82_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_82_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_82_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_82_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_83_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_83_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_83_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_83_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_83_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_83_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_83_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_83_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_84_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_84_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_84_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_84_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_84_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_84_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_84_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_84_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_87_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_87_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_87_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_87_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_87_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_87_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_87_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_87_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_89_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_89_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_89_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_89_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_89_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_89_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_89_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_89_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_91_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_91_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_91_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_91_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_91_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_91_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_91_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_91_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_93_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_93_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_93_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_93_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_93_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_93_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_93_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_93_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_94_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_94_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_94_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_94_n_3\ : STD_LOGIC;
  signal \caracter_reg[0]_i_94_n_4\ : STD_LOGIC;
  signal \caracter_reg[0]_i_94_n_5\ : STD_LOGIC;
  signal \caracter_reg[0]_i_94_n_6\ : STD_LOGIC;
  signal \caracter_reg[0]_i_94_n_7\ : STD_LOGIC;
  signal \caracter_reg[0]_i_97_n_0\ : STD_LOGIC;
  signal \caracter_reg[0]_i_97_n_1\ : STD_LOGIC;
  signal \caracter_reg[0]_i_97_n_2\ : STD_LOGIC;
  signal \caracter_reg[0]_i_97_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_102_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_103_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_103_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_103_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_103_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_103_n_4\ : STD_LOGIC;
  signal \caracter_reg[1]_i_103_n_5\ : STD_LOGIC;
  signal \caracter_reg[1]_i_103_n_6\ : STD_LOGIC;
  signal \caracter_reg[1]_i_103_n_7\ : STD_LOGIC;
  signal \caracter_reg[1]_i_104_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_104_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_104_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_104_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_104_n_4\ : STD_LOGIC;
  signal \caracter_reg[1]_i_104_n_5\ : STD_LOGIC;
  signal \caracter_reg[1]_i_104_n_6\ : STD_LOGIC;
  signal \caracter_reg[1]_i_124_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_124_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_124_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_124_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_124_n_4\ : STD_LOGIC;
  signal \caracter_reg[1]_i_124_n_5\ : STD_LOGIC;
  signal \caracter_reg[1]_i_124_n_6\ : STD_LOGIC;
  signal \caracter_reg[1]_i_124_n_7\ : STD_LOGIC;
  signal \caracter_reg[1]_i_141_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_141_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_141_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_141_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_150_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_150_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_150_n_5\ : STD_LOGIC;
  signal \caracter_reg[1]_i_150_n_6\ : STD_LOGIC;
  signal \caracter_reg[1]_i_150_n_7\ : STD_LOGIC;
  signal \caracter_reg[1]_i_152_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_152_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_152_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_152_n_3\ : STD_LOGIC;
  signal \^caracter_reg[1]_i_15_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_15_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_15_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_15_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_15_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_15_n_4\ : STD_LOGIC;
  signal \caracter_reg[1]_i_15_n_5\ : STD_LOGIC;
  signal \caracter_reg[1]_i_15_n_6\ : STD_LOGIC;
  signal \caracter_reg[1]_i_161_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_161_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_161_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_161_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_161_n_4\ : STD_LOGIC;
  signal \caracter_reg[1]_i_161_n_5\ : STD_LOGIC;
  signal \caracter_reg[1]_i_161_n_6\ : STD_LOGIC;
  signal \caracter_reg[1]_i_161_n_7\ : STD_LOGIC;
  signal \caracter_reg[1]_i_166_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_166_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_166_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_166_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_166_n_4\ : STD_LOGIC;
  signal \caracter_reg[1]_i_166_n_5\ : STD_LOGIC;
  signal \caracter_reg[1]_i_166_n_6\ : STD_LOGIC;
  signal \caracter_reg[1]_i_166_n_7\ : STD_LOGIC;
  signal \caracter_reg[1]_i_16_n_7\ : STD_LOGIC;
  signal \caracter_reg[1]_i_181_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_181_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_181_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_181_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_181_n_4\ : STD_LOGIC;
  signal \caracter_reg[1]_i_181_n_5\ : STD_LOGIC;
  signal \caracter_reg[1]_i_181_n_6\ : STD_LOGIC;
  signal \caracter_reg[1]_i_181_n_7\ : STD_LOGIC;
  signal \caracter_reg[1]_i_190_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_190_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_190_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_190_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_199_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_200_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_200_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_200_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_200_n_5\ : STD_LOGIC;
  signal \caracter_reg[1]_i_200_n_6\ : STD_LOGIC;
  signal \caracter_reg[1]_i_200_n_7\ : STD_LOGIC;
  signal \caracter_reg[1]_i_201_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_201_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_201_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_201_n_5\ : STD_LOGIC;
  signal \caracter_reg[1]_i_201_n_6\ : STD_LOGIC;
  signal \caracter_reg[1]_i_201_n_7\ : STD_LOGIC;
  signal \caracter_reg[1]_i_206_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_206_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_206_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_206_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_215_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_215_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_215_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_215_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_215_n_4\ : STD_LOGIC;
  signal \caracter_reg[1]_i_215_n_5\ : STD_LOGIC;
  signal \caracter_reg[1]_i_215_n_6\ : STD_LOGIC;
  signal \caracter_reg[1]_i_216_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_216_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_216_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_216_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_216_n_7\ : STD_LOGIC;
  signal \caracter_reg[1]_i_225_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_225_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_225_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_225_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_225_n_4\ : STD_LOGIC;
  signal \caracter_reg[1]_i_225_n_5\ : STD_LOGIC;
  signal \caracter_reg[1]_i_225_n_6\ : STD_LOGIC;
  signal \caracter_reg[1]_i_239_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_239_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_239_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_239_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_248_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_248_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_248_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_248_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_248_n_4\ : STD_LOGIC;
  signal \caracter_reg[1]_i_248_n_5\ : STD_LOGIC;
  signal \caracter_reg[1]_i_248_n_6\ : STD_LOGIC;
  signal \caracter_reg[1]_i_248_n_7\ : STD_LOGIC;
  signal \caracter_reg[1]_i_249_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_249_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_249_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_249_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_249_n_4\ : STD_LOGIC;
  signal \caracter_reg[1]_i_249_n_5\ : STD_LOGIC;
  signal \caracter_reg[1]_i_249_n_6\ : STD_LOGIC;
  signal \caracter_reg[1]_i_249_n_7\ : STD_LOGIC;
  signal \caracter_reg[1]_i_250_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_250_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_250_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_250_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_250_n_4\ : STD_LOGIC;
  signal \caracter_reg[1]_i_250_n_5\ : STD_LOGIC;
  signal \caracter_reg[1]_i_250_n_6\ : STD_LOGIC;
  signal \caracter_reg[1]_i_250_n_7\ : STD_LOGIC;
  signal \caracter_reg[1]_i_257_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_257_n_6\ : STD_LOGIC;
  signal \caracter_reg[1]_i_257_n_7\ : STD_LOGIC;
  signal \caracter_reg[1]_i_285_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_285_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_285_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_285_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_294_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_294_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_294_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_294_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_294_n_4\ : STD_LOGIC;
  signal \caracter_reg[1]_i_294_n_5\ : STD_LOGIC;
  signal \caracter_reg[1]_i_294_n_6\ : STD_LOGIC;
  signal \caracter_reg[1]_i_294_n_7\ : STD_LOGIC;
  signal \caracter_reg[1]_i_303_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_303_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_303_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_303_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_303_n_4\ : STD_LOGIC;
  signal \caracter_reg[1]_i_303_n_5\ : STD_LOGIC;
  signal \caracter_reg[1]_i_303_n_6\ : STD_LOGIC;
  signal \caracter_reg[1]_i_303_n_7\ : STD_LOGIC;
  signal \caracter_reg[1]_i_310_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_310_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_310_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_310_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_310_n_4\ : STD_LOGIC;
  signal \caracter_reg[1]_i_310_n_5\ : STD_LOGIC;
  signal \caracter_reg[1]_i_310_n_6\ : STD_LOGIC;
  signal \caracter_reg[1]_i_317_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_317_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_317_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_317_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_333_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_333_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_333_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_333_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_333_n_4\ : STD_LOGIC;
  signal \caracter_reg[1]_i_333_n_5\ : STD_LOGIC;
  signal \caracter_reg[1]_i_333_n_6\ : STD_LOGIC;
  signal \caracter_reg[1]_i_333_n_7\ : STD_LOGIC;
  signal \caracter_reg[1]_i_353_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_353_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_353_n_6\ : STD_LOGIC;
  signal \caracter_reg[1]_i_353_n_7\ : STD_LOGIC;
  signal \caracter_reg[1]_i_354_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_354_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_354_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_354_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_354_n_4\ : STD_LOGIC;
  signal \caracter_reg[1]_i_354_n_5\ : STD_LOGIC;
  signal \caracter_reg[1]_i_354_n_6\ : STD_LOGIC;
  signal \caracter_reg[1]_i_354_n_7\ : STD_LOGIC;
  signal \caracter_reg[1]_i_360_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_360_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_360_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_360_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_360_n_4\ : STD_LOGIC;
  signal \caracter_reg[1]_i_364_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_364_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_364_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_364_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_38_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_38_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_38_n_5\ : STD_LOGIC;
  signal \caracter_reg[1]_i_38_n_6\ : STD_LOGIC;
  signal \caracter_reg[1]_i_38_n_7\ : STD_LOGIC;
  signal \caracter_reg[1]_i_39_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_39_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_39_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_39_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_39_n_4\ : STD_LOGIC;
  signal \caracter_reg[1]_i_40_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_40_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_40_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_40_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_40_n_4\ : STD_LOGIC;
  signal \caracter_reg[1]_i_40_n_5\ : STD_LOGIC;
  signal \caracter_reg[1]_i_44_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_44_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_44_n_5\ : STD_LOGIC;
  signal \caracter_reg[1]_i_44_n_6\ : STD_LOGIC;
  signal \caracter_reg[1]_i_44_n_7\ : STD_LOGIC;
  signal \caracter_reg[1]_i_47_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_47_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_47_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_47_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_60_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_60_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_60_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_60_n_4\ : STD_LOGIC;
  signal \caracter_reg[1]_i_60_n_5\ : STD_LOGIC;
  signal \caracter_reg[1]_i_60_n_6\ : STD_LOGIC;
  signal \caracter_reg[1]_i_60_n_7\ : STD_LOGIC;
  signal \caracter_reg[1]_i_66_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_66_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_66_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_66_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_75_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_75_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_75_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_75_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_75_n_4\ : STD_LOGIC;
  signal \caracter_reg[1]_i_75_n_5\ : STD_LOGIC;
  signal \caracter_reg[1]_i_75_n_6\ : STD_LOGIC;
  signal \caracter_reg[1]_i_75_n_7\ : STD_LOGIC;
  signal \caracter_reg[1]_i_76_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_77_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_77_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_77_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_77_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_77_n_4\ : STD_LOGIC;
  signal \caracter_reg[1]_i_77_n_5\ : STD_LOGIC;
  signal \caracter_reg[1]_i_77_n_6\ : STD_LOGIC;
  signal \caracter_reg[1]_i_77_n_7\ : STD_LOGIC;
  signal \caracter_reg[1]_i_78_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_78_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_78_n_6\ : STD_LOGIC;
  signal \caracter_reg[1]_i_78_n_7\ : STD_LOGIC;
  signal \caracter_reg[1]_i_79_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_79_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_79_n_6\ : STD_LOGIC;
  signal \caracter_reg[1]_i_79_n_7\ : STD_LOGIC;
  signal \caracter_reg[1]_i_80_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_80_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_80_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_80_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_80_n_4\ : STD_LOGIC;
  signal \caracter_reg[1]_i_80_n_5\ : STD_LOGIC;
  signal \caracter_reg[1]_i_80_n_6\ : STD_LOGIC;
  signal \caracter_reg[1]_i_80_n_7\ : STD_LOGIC;
  signal \caracter_reg[1]_i_85_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_85_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_85_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_85_n_3\ : STD_LOGIC;
  signal \caracter_reg[1]_i_85_n_4\ : STD_LOGIC;
  signal \caracter_reg[1]_i_85_n_5\ : STD_LOGIC;
  signal \caracter_reg[1]_i_85_n_6\ : STD_LOGIC;
  signal \caracter_reg[1]_i_85_n_7\ : STD_LOGIC;
  signal \caracter_reg[1]_i_90_n_7\ : STD_LOGIC;
  signal \caracter_reg[1]_i_92_n_0\ : STD_LOGIC;
  signal \caracter_reg[1]_i_92_n_1\ : STD_LOGIC;
  signal \caracter_reg[1]_i_92_n_2\ : STD_LOGIC;
  signal \caracter_reg[1]_i_92_n_3\ : STD_LOGIC;
  signal \caracter_reg[2]_i_22_n_0\ : STD_LOGIC;
  signal \caracter_reg[2]_i_22_n_1\ : STD_LOGIC;
  signal \caracter_reg[2]_i_22_n_2\ : STD_LOGIC;
  signal \caracter_reg[2]_i_22_n_3\ : STD_LOGIC;
  signal \caracter_reg[2]_i_22_n_4\ : STD_LOGIC;
  signal \caracter_reg[2]_i_22_n_5\ : STD_LOGIC;
  signal \caracter_reg[3]_i_108_n_0\ : STD_LOGIC;
  signal \caracter_reg[3]_i_108_n_1\ : STD_LOGIC;
  signal \caracter_reg[3]_i_108_n_2\ : STD_LOGIC;
  signal \caracter_reg[3]_i_108_n_3\ : STD_LOGIC;
  signal \caracter_reg[3]_i_108_n_4\ : STD_LOGIC;
  signal \caracter_reg[3]_i_108_n_5\ : STD_LOGIC;
  signal \caracter_reg[3]_i_108_n_6\ : STD_LOGIC;
  signal \caracter_reg[3]_i_108_n_7\ : STD_LOGIC;
  signal \caracter_reg[3]_i_117_n_0\ : STD_LOGIC;
  signal \caracter_reg[3]_i_117_n_1\ : STD_LOGIC;
  signal \caracter_reg[3]_i_117_n_2\ : STD_LOGIC;
  signal \caracter_reg[3]_i_117_n_3\ : STD_LOGIC;
  signal \caracter_reg[3]_i_117_n_4\ : STD_LOGIC;
  signal \caracter_reg[3]_i_117_n_5\ : STD_LOGIC;
  signal \caracter_reg[3]_i_117_n_6\ : STD_LOGIC;
  signal \caracter_reg[3]_i_126_n_0\ : STD_LOGIC;
  signal \caracter_reg[3]_i_126_n_1\ : STD_LOGIC;
  signal \caracter_reg[3]_i_126_n_2\ : STD_LOGIC;
  signal \caracter_reg[3]_i_126_n_3\ : STD_LOGIC;
  signal \caracter_reg[3]_i_126_n_4\ : STD_LOGIC;
  signal \caracter_reg[3]_i_126_n_5\ : STD_LOGIC;
  signal \caracter_reg[3]_i_126_n_6\ : STD_LOGIC;
  signal \caracter_reg[3]_i_135_n_0\ : STD_LOGIC;
  signal \caracter_reg[3]_i_135_n_1\ : STD_LOGIC;
  signal \caracter_reg[3]_i_135_n_2\ : STD_LOGIC;
  signal \caracter_reg[3]_i_135_n_3\ : STD_LOGIC;
  signal \caracter_reg[3]_i_173_n_2\ : STD_LOGIC;
  signal \caracter_reg[3]_i_173_n_7\ : STD_LOGIC;
  signal \caracter_reg[3]_i_174_n_0\ : STD_LOGIC;
  signal \caracter_reg[3]_i_174_n_1\ : STD_LOGIC;
  signal \caracter_reg[3]_i_174_n_2\ : STD_LOGIC;
  signal \caracter_reg[3]_i_174_n_3\ : STD_LOGIC;
  signal \caracter_reg[3]_i_174_n_4\ : STD_LOGIC;
  signal \caracter_reg[3]_i_174_n_5\ : STD_LOGIC;
  signal \caracter_reg[3]_i_174_n_6\ : STD_LOGIC;
  signal \caracter_reg[3]_i_174_n_7\ : STD_LOGIC;
  signal \caracter_reg[3]_i_176_n_0\ : STD_LOGIC;
  signal \caracter_reg[3]_i_176_n_1\ : STD_LOGIC;
  signal \caracter_reg[3]_i_176_n_2\ : STD_LOGIC;
  signal \caracter_reg[3]_i_176_n_3\ : STD_LOGIC;
  signal \caracter_reg[3]_i_176_n_4\ : STD_LOGIC;
  signal \caracter_reg[3]_i_183_n_0\ : STD_LOGIC;
  signal \caracter_reg[3]_i_183_n_1\ : STD_LOGIC;
  signal \caracter_reg[3]_i_183_n_2\ : STD_LOGIC;
  signal \caracter_reg[3]_i_183_n_3\ : STD_LOGIC;
  signal \caracter_reg[3]_i_21_n_1\ : STD_LOGIC;
  signal \caracter_reg[3]_i_21_n_2\ : STD_LOGIC;
  signal \caracter_reg[3]_i_21_n_3\ : STD_LOGIC;
  signal \caracter_reg[3]_i_21_n_4\ : STD_LOGIC;
  signal \caracter_reg[3]_i_21_n_5\ : STD_LOGIC;
  signal \caracter_reg[3]_i_21_n_6\ : STD_LOGIC;
  signal \caracter_reg[3]_i_21_n_7\ : STD_LOGIC;
  signal \caracter_reg[3]_i_44_n_2\ : STD_LOGIC;
  signal \caracter_reg[3]_i_44_n_3\ : STD_LOGIC;
  signal \caracter_reg[3]_i_44_n_5\ : STD_LOGIC;
  signal \caracter_reg[3]_i_44_n_6\ : STD_LOGIC;
  signal \caracter_reg[3]_i_44_n_7\ : STD_LOGIC;
  signal \caracter_reg[3]_i_45_n_0\ : STD_LOGIC;
  signal \caracter_reg[3]_i_45_n_1\ : STD_LOGIC;
  signal \caracter_reg[3]_i_45_n_2\ : STD_LOGIC;
  signal \caracter_reg[3]_i_45_n_3\ : STD_LOGIC;
  signal \caracter_reg[3]_i_45_n_4\ : STD_LOGIC;
  signal \caracter_reg[3]_i_45_n_5\ : STD_LOGIC;
  signal \caracter_reg[3]_i_45_n_6\ : STD_LOGIC;
  signal \caracter_reg[3]_i_45_n_7\ : STD_LOGIC;
  signal \caracter_reg[3]_i_52_n_0\ : STD_LOGIC;
  signal \caracter_reg[3]_i_52_n_1\ : STD_LOGIC;
  signal \caracter_reg[3]_i_52_n_2\ : STD_LOGIC;
  signal \caracter_reg[3]_i_52_n_3\ : STD_LOGIC;
  signal \caracter_reg[3]_i_60_n_3\ : STD_LOGIC;
  signal \caracter_reg[3]_i_60_n_6\ : STD_LOGIC;
  signal \caracter_reg[3]_i_60_n_7\ : STD_LOGIC;
  signal \caracter_reg[3]_i_61_n_0\ : STD_LOGIC;
  signal \caracter_reg[3]_i_61_n_1\ : STD_LOGIC;
  signal \caracter_reg[3]_i_61_n_2\ : STD_LOGIC;
  signal \caracter_reg[3]_i_61_n_3\ : STD_LOGIC;
  signal \caracter_reg[3]_i_70_n_1\ : STD_LOGIC;
  signal \caracter_reg[3]_i_70_n_3\ : STD_LOGIC;
  signal \caracter_reg[3]_i_70_n_6\ : STD_LOGIC;
  signal \caracter_reg[3]_i_70_n_7\ : STD_LOGIC;
  signal \caracter_reg[3]_i_71_n_0\ : STD_LOGIC;
  signal \caracter_reg[3]_i_71_n_2\ : STD_LOGIC;
  signal \caracter_reg[3]_i_71_n_3\ : STD_LOGIC;
  signal \caracter_reg[3]_i_71_n_5\ : STD_LOGIC;
  signal \caracter_reg[3]_i_71_n_6\ : STD_LOGIC;
  signal \caracter_reg[3]_i_71_n_7\ : STD_LOGIC;
  signal \caracter_reg[3]_i_72_n_1\ : STD_LOGIC;
  signal \caracter_reg[3]_i_72_n_3\ : STD_LOGIC;
  signal \caracter_reg[3]_i_72_n_6\ : STD_LOGIC;
  signal \caracter_reg[3]_i_72_n_7\ : STD_LOGIC;
  signal \caracter_reg[3]_i_75_n_0\ : STD_LOGIC;
  signal \caracter_reg[3]_i_75_n_1\ : STD_LOGIC;
  signal \caracter_reg[3]_i_75_n_2\ : STD_LOGIC;
  signal \caracter_reg[3]_i_75_n_3\ : STD_LOGIC;
  signal \caracter_reg[3]_i_84_n_0\ : STD_LOGIC;
  signal \caracter_reg[3]_i_84_n_1\ : STD_LOGIC;
  signal \caracter_reg[3]_i_84_n_2\ : STD_LOGIC;
  signal \caracter_reg[3]_i_84_n_3\ : STD_LOGIC;
  signal \caracter_reg[3]_i_84_n_4\ : STD_LOGIC;
  signal \caracter_reg[3]_i_84_n_5\ : STD_LOGIC;
  signal \caracter_reg[3]_i_84_n_6\ : STD_LOGIC;
  signal \caracter_reg[3]_i_84_n_7\ : STD_LOGIC;
  signal \caracter_reg[3]_i_88_n_0\ : STD_LOGIC;
  signal \caracter_reg[3]_i_88_n_1\ : STD_LOGIC;
  signal \caracter_reg[3]_i_88_n_2\ : STD_LOGIC;
  signal \caracter_reg[3]_i_88_n_3\ : STD_LOGIC;
  signal \caracter_reg[3]_i_88_n_4\ : STD_LOGIC;
  signal \caracter_reg[3]_i_88_n_5\ : STD_LOGIC;
  signal \caracter_reg[3]_i_88_n_6\ : STD_LOGIC;
  signal \caracter_reg[3]_i_88_n_7\ : STD_LOGIC;
  signal \caracter_reg[3]_i_94_n_0\ : STD_LOGIC;
  signal \caracter_reg[3]_i_94_n_1\ : STD_LOGIC;
  signal \caracter_reg[3]_i_94_n_2\ : STD_LOGIC;
  signal \caracter_reg[3]_i_94_n_3\ : STD_LOGIC;
  signal \caracter_reg[3]_i_94_n_4\ : STD_LOGIC;
  signal \caracter_reg[3]_i_94_n_5\ : STD_LOGIC;
  signal \caracter_reg[3]_i_94_n_6\ : STD_LOGIC;
  signal \caracter_reg[3]_i_94_n_7\ : STD_LOGIC;
  signal \caracter_reg[3]_i_99_n_0\ : STD_LOGIC;
  signal \caracter_reg[3]_i_99_n_1\ : STD_LOGIC;
  signal \caracter_reg[3]_i_99_n_2\ : STD_LOGIC;
  signal \caracter_reg[3]_i_99_n_3\ : STD_LOGIC;
  signal cent : STD_LOGIC_VECTOR ( 6 downto 1 );
  signal \disp_dinero[2]_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^disp_dinero[5]_0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal euros2 : STD_LOGIC_VECTOR ( 30 downto 0 );
  signal euros3 : STD_LOGIC_VECTOR ( 30 downto 1 );
  signal p_1_in : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \string_cent_decenas[1]5\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^total_string_reg[0]\ : STD_LOGIC;
  signal \NLW_caracter_reg[0]_i_130_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_20_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_caracter_reg[0]_i_20_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_21_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_caracter_reg[0]_i_21_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_caracter_reg[0]_i_22_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_caracter_reg[0]_i_22_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_221_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_23_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_caracter_reg[0]_i_23_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_caracter_reg[0]_i_250_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_caracter_reg[0]_i_250_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_caracter_reg[0]_i_251_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \NLW_caracter_reg[0]_i_251_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_caracter_reg[0]_i_252_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_caracter_reg[0]_i_252_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_caracter_reg[0]_i_253_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_28_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_310_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_361_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_37_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_375_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_caracter_reg[0]_i_420_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_46_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_506_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_531_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_531_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_caracter_reg[0]_i_557_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[0]_i_605_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_71_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_71_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_caracter_reg[0]_i_72_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[0]_i_97_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[1]_i_102_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_caracter_reg[1]_i_102_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[1]_i_104_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_caracter_reg[1]_i_141_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[1]_i_150_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_caracter_reg[1]_i_150_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_caracter_reg[1]_i_152_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[1]_i_16_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[1]_i_16_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_caracter_reg[1]_i_190_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[1]_i_199_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_caracter_reg[1]_i_199_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[1]_i_200_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \NLW_caracter_reg[1]_i_200_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_caracter_reg[1]_i_201_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \NLW_caracter_reg[1]_i_201_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_caracter_reg[1]_i_206_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[1]_i_215_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_caracter_reg[1]_i_216_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_caracter_reg[1]_i_225_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_caracter_reg[1]_i_239_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[1]_i_257_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_caracter_reg[1]_i_257_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_caracter_reg[1]_i_285_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[1]_i_310_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_caracter_reg[1]_i_317_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[1]_i_353_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_caracter_reg[1]_i_353_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_caracter_reg[1]_i_360_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[1]_i_364_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[1]_i_38_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_caracter_reg[1]_i_38_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_caracter_reg[1]_i_39_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[1]_i_44_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_caracter_reg[1]_i_44_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_caracter_reg[1]_i_47_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[1]_i_60_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_caracter_reg[1]_i_66_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[1]_i_76_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_caracter_reg[1]_i_76_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[1]_i_78_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_caracter_reg[1]_i_78_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_caracter_reg[1]_i_79_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_caracter_reg[1]_i_79_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_caracter_reg[1]_i_90_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[1]_i_90_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_caracter_reg[1]_i_92_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[2]_i_22_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_caracter_reg[3]_i_117_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_caracter_reg[3]_i_126_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_caracter_reg[3]_i_135_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[3]_i_173_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[3]_i_173_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_caracter_reg[3]_i_176_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_caracter_reg[3]_i_183_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[3]_i_21_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_caracter_reg[3]_i_44_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_caracter_reg[3]_i_44_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_caracter_reg[3]_i_52_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[3]_i_60_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_caracter_reg[3]_i_60_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_caracter_reg[3]_i_61_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[3]_i_70_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_caracter_reg[3]_i_70_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_caracter_reg[3]_i_71_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \NLW_caracter_reg[3]_i_71_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_caracter_reg[3]_i_72_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_caracter_reg[3]_i_72_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_caracter_reg[3]_i_75_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_caracter_reg[3]_i_99_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \caracter[0]_i_10\ : label is "soft_lutpair27";
  attribute HLUTNM : string;
  attribute HLUTNM of \caracter[0]_i_112\ : label is "lutpair67";
  attribute HLUTNM of \caracter[0]_i_115\ : label is "lutpair66";
  attribute HLUTNM of \caracter[0]_i_116\ : label is "lutpair68";
  attribute HLUTNM of \caracter[0]_i_117\ : label is "lutpair67";
  attribute HLUTNM of \caracter[0]_i_121\ : label is "lutpair58";
  attribute HLUTNM of \caracter[0]_i_122\ : label is "lutpair57";
  attribute HLUTNM of \caracter[0]_i_123\ : label is "lutpair56";
  attribute HLUTNM of \caracter[0]_i_124\ : label is "lutpair55";
  attribute HLUTNM of \caracter[0]_i_126\ : label is "lutpair58";
  attribute HLUTNM of \caracter[0]_i_127\ : label is "lutpair57";
  attribute HLUTNM of \caracter[0]_i_128\ : label is "lutpair56";
  attribute SOFT_HLUTNM of \caracter[0]_i_141\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \caracter[0]_i_146\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \caracter[0]_i_147\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \caracter[0]_i_148\ : label is "soft_lutpair39";
  attribute HLUTNM of \caracter[0]_i_153\ : label is "lutpair37";
  attribute HLUTNM of \caracter[0]_i_157\ : label is "lutpair29";
  attribute HLUTNM of \caracter[0]_i_158\ : label is "lutpair28";
  attribute HLUTNM of \caracter[0]_i_159\ : label is "lutpair27";
  attribute HLUTNM of \caracter[0]_i_160\ : label is "lutpair26";
  attribute HLUTNM of \caracter[0]_i_161\ : label is "lutpair30";
  attribute HLUTNM of \caracter[0]_i_162\ : label is "lutpair29";
  attribute HLUTNM of \caracter[0]_i_163\ : label is "lutpair28";
  attribute HLUTNM of \caracter[0]_i_164\ : label is "lutpair27";
  attribute HLUTNM of \caracter[0]_i_175\ : label is "lutpair18";
  attribute HLUTNM of \caracter[0]_i_176\ : label is "lutpair17";
  attribute HLUTNM of \caracter[0]_i_180\ : label is "lutpair18";
  attribute HLUTNM of \caracter[0]_i_189\ : label is "lutpair16";
  attribute HLUTNM of \caracter[0]_i_190\ : label is "lutpair15";
  attribute HLUTNM of \caracter[0]_i_191\ : label is "lutpair14";
  attribute HLUTNM of \caracter[0]_i_192\ : label is "lutpair13";
  attribute HLUTNM of \caracter[0]_i_193\ : label is "lutpair17";
  attribute HLUTNM of \caracter[0]_i_194\ : label is "lutpair16";
  attribute HLUTNM of \caracter[0]_i_195\ : label is "lutpair15";
  attribute HLUTNM of \caracter[0]_i_196\ : label is "lutpair14";
  attribute HLUTNM of \caracter[0]_i_197\ : label is "lutpair25";
  attribute HLUTNM of \caracter[0]_i_198\ : label is "lutpair24";
  attribute HLUTNM of \caracter[0]_i_199\ : label is "lutpair23";
  attribute HLUTNM of \caracter[0]_i_200\ : label is "lutpair22";
  attribute HLUTNM of \caracter[0]_i_201\ : label is "lutpair26";
  attribute HLUTNM of \caracter[0]_i_202\ : label is "lutpair25";
  attribute HLUTNM of \caracter[0]_i_203\ : label is "lutpair24";
  attribute HLUTNM of \caracter[0]_i_204\ : label is "lutpair23";
  attribute HLUTNM of \caracter[0]_i_205\ : label is "lutpair33";
  attribute HLUTNM of \caracter[0]_i_206\ : label is "lutpair32";
  attribute HLUTNM of \caracter[0]_i_207\ : label is "lutpair31";
  attribute HLUTNM of \caracter[0]_i_208\ : label is "lutpair30";
  attribute HLUTNM of \caracter[0]_i_209\ : label is "lutpair34";
  attribute HLUTNM of \caracter[0]_i_210\ : label is "lutpair33";
  attribute HLUTNM of \caracter[0]_i_211\ : label is "lutpair32";
  attribute HLUTNM of \caracter[0]_i_212\ : label is "lutpair31";
  attribute HLUTNM of \caracter[0]_i_213\ : label is "lutpair40";
  attribute HLUTNM of \caracter[0]_i_214\ : label is "lutpair39";
  attribute HLUTNM of \caracter[0]_i_215\ : label is "lutpair38";
  attribute HLUTNM of \caracter[0]_i_216\ : label is "lutpair37";
  attribute HLUTNM of \caracter[0]_i_217\ : label is "lutpair41";
  attribute HLUTNM of \caracter[0]_i_218\ : label is "lutpair40";
  attribute HLUTNM of \caracter[0]_i_219\ : label is "lutpair39";
  attribute HLUTNM of \caracter[0]_i_220\ : label is "lutpair38";
  attribute HLUTNM of \caracter[0]_i_232\ : label is "lutpair65";
  attribute HLUTNM of \caracter[0]_i_235\ : label is "lutpair64";
  attribute HLUTNM of \caracter[0]_i_236\ : label is "lutpair66";
  attribute HLUTNM of \caracter[0]_i_237\ : label is "lutpair65";
  attribute HLUTNM of \caracter[0]_i_245\ : label is "lutpair55";
  attribute SOFT_HLUTNM of \caracter[0]_i_25\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \caracter[0]_i_263\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \caracter[0]_i_264\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \caracter[0]_i_266\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \caracter[0]_i_268\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \caracter[0]_i_270\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \caracter[0]_i_271\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \caracter[0]_i_272\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \caracter[0]_i_273\ : label is "soft_lutpair54";
  attribute HLUTNM of \caracter[0]_i_290\ : label is "lutpair21";
  attribute HLUTNM of \caracter[0]_i_291\ : label is "lutpair20";
  attribute HLUTNM of \caracter[0]_i_292\ : label is "lutpair19";
  attribute HLUTNM of \caracter[0]_i_294\ : label is "lutpair22";
  attribute HLUTNM of \caracter[0]_i_295\ : label is "lutpair21";
  attribute HLUTNM of \caracter[0]_i_296\ : label is "lutpair20";
  attribute HLUTNM of \caracter[0]_i_297\ : label is "lutpair19";
  attribute HLUTNM of \caracter[0]_i_298\ : label is "lutpair12";
  attribute HLUTNM of \caracter[0]_i_299\ : label is "lutpair11";
  attribute HLUTNM of \caracter[0]_i_300\ : label is "lutpair10";
  attribute HLUTNM of \caracter[0]_i_301\ : label is "lutpair9";
  attribute HLUTNM of \caracter[0]_i_302\ : label is "lutpair13";
  attribute HLUTNM of \caracter[0]_i_303\ : label is "lutpair12";
  attribute HLUTNM of \caracter[0]_i_304\ : label is "lutpair11";
  attribute HLUTNM of \caracter[0]_i_305\ : label is "lutpair10";
  attribute SOFT_HLUTNM of \caracter[0]_i_309\ : label is "soft_lutpair32";
  attribute HLUTNM of \caracter[0]_i_320\ : label is "lutpair63";
  attribute HLUTNM of \caracter[0]_i_323\ : label is "lutpair62";
  attribute HLUTNM of \caracter[0]_i_324\ : label is "lutpair64";
  attribute HLUTNM of \caracter[0]_i_325\ : label is "lutpair63";
  attribute HLUTNM of \caracter[0]_i_364\ : label is "lutpair54";
  attribute HLUTNM of \caracter[0]_i_365\ : label is "lutpair53";
  attribute HLUTNM of \caracter[0]_i_369\ : label is "lutpair54";
  attribute SOFT_HLUTNM of \caracter[0]_i_370\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \caracter[0]_i_372\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \caracter[0]_i_373\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \caracter[0]_i_374\ : label is "soft_lutpair28";
  attribute HLUTNM of \caracter[0]_i_401\ : label is "lutpair8";
  attribute HLUTNM of \caracter[0]_i_402\ : label is "lutpair7";
  attribute HLUTNM of \caracter[0]_i_403\ : label is "lutpair6";
  attribute HLUTNM of \caracter[0]_i_404\ : label is "lutpair5";
  attribute HLUTNM of \caracter[0]_i_405\ : label is "lutpair9";
  attribute HLUTNM of \caracter[0]_i_406\ : label is "lutpair8";
  attribute HLUTNM of \caracter[0]_i_407\ : label is "lutpair7";
  attribute HLUTNM of \caracter[0]_i_408\ : label is "lutpair6";
  attribute SOFT_HLUTNM of \caracter[0]_i_409\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \caracter[0]_i_411\ : label is "soft_lutpair33";
  attribute HLUTNM of \caracter[0]_i_430\ : label is "lutpair61";
  attribute HLUTNM of \caracter[0]_i_433\ : label is "lutpair60";
  attribute HLUTNM of \caracter[0]_i_434\ : label is "lutpair62";
  attribute HLUTNM of \caracter[0]_i_435\ : label is "lutpair61";
  attribute HLUTNM of \caracter[0]_i_452\ : label is "lutpair52";
  attribute HLUTNM of \caracter[0]_i_453\ : label is "lutpair51";
  attribute HLUTNM of \caracter[0]_i_454\ : label is "lutpair50";
  attribute HLUTNM of \caracter[0]_i_455\ : label is "lutpair49";
  attribute HLUTNM of \caracter[0]_i_457\ : label is "lutpair52";
  attribute HLUTNM of \caracter[0]_i_458\ : label is "lutpair51";
  attribute HLUTNM of \caracter[0]_i_459\ : label is "lutpair50";
  attribute HLUTNM of \caracter[0]_i_469\ : label is "lutpair36";
  attribute HLUTNM of \caracter[0]_i_470\ : label is "lutpair35";
  attribute HLUTNM of \caracter[0]_i_471\ : label is "lutpair34";
  attribute HLUTNM of \caracter[0]_i_474\ : label is "lutpair36";
  attribute HLUTNM of \caracter[0]_i_475\ : label is "lutpair35";
  attribute HLUTNM of \caracter[0]_i_480\ : label is "lutpair53";
  attribute HLUTNM of \caracter[0]_i_489\ : label is "lutpair4";
  attribute HLUTNM of \caracter[0]_i_493\ : label is "lutpair5";
  attribute HLUTNM of \caracter[0]_i_494\ : label is "lutpair4";
  attribute HLUTNM of \caracter[0]_i_515\ : label is "lutpair59";
  attribute HLUTNM of \caracter[0]_i_518\ : label is "lutpair60";
  attribute HLUTNM of \caracter[0]_i_519\ : label is "lutpair59";
  attribute SOFT_HLUTNM of \caracter[0]_i_534\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \caracter[0]_i_536\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \caracter[0]_i_539\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \caracter[0]_i_540\ : label is "soft_lutpair48";
  attribute HLUTNM of \caracter[0]_i_541\ : label is "lutpair48";
  attribute HLUTNM of \caracter[0]_i_542\ : label is "lutpair47";
  attribute HLUTNM of \caracter[0]_i_543\ : label is "lutpair46";
  attribute HLUTNM of \caracter[0]_i_544\ : label is "lutpair45";
  attribute HLUTNM of \caracter[0]_i_545\ : label is "lutpair49";
  attribute HLUTNM of \caracter[0]_i_546\ : label is "lutpair48";
  attribute HLUTNM of \caracter[0]_i_547\ : label is "lutpair47";
  attribute HLUTNM of \caracter[0]_i_548\ : label is "lutpair46";
  attribute SOFT_HLUTNM of \caracter[0]_i_577\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \caracter[0]_i_578\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \caracter[0]_i_579\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \caracter[0]_i_580\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \caracter[0]_i_581\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \caracter[0]_i_582\ : label is "soft_lutpair45";
  attribute HLUTNM of \caracter[0]_i_583\ : label is "lutpair44";
  attribute HLUTNM of \caracter[0]_i_584\ : label is "lutpair43";
  attribute HLUTNM of \caracter[0]_i_585\ : label is "lutpair42";
  attribute HLUTNM of \caracter[0]_i_586\ : label is "lutpair41";
  attribute HLUTNM of \caracter[0]_i_587\ : label is "lutpair45";
  attribute HLUTNM of \caracter[0]_i_588\ : label is "lutpair44";
  attribute HLUTNM of \caracter[0]_i_589\ : label is "lutpair43";
  attribute HLUTNM of \caracter[0]_i_590\ : label is "lutpair42";
  attribute SOFT_HLUTNM of \caracter[0]_i_621\ : label is "soft_lutpair32";
  attribute HLUTNM of \caracter[0]_i_64\ : label is "lutpair69";
  attribute HLUTNM of \caracter[0]_i_65\ : label is "lutpair68";
  attribute HLUTNM of \caracter[0]_i_69\ : label is "lutpair69";
  attribute SOFT_HLUTNM of \caracter[0]_i_85\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \caracter[0]_i_86\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \caracter[0]_i_90\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \caracter[0]_i_92\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \caracter[0]_i_95\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \caracter[0]_i_96\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \caracter[1]_i_105\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \caracter[1]_i_106\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \caracter[1]_i_107\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \caracter[1]_i_108\ : label is "soft_lutpair51";
  attribute HLUTNM of \caracter[1]_i_117\ : label is "lutpair71";
  attribute HLUTNM of \caracter[1]_i_120\ : label is "lutpair72";
  attribute HLUTNM of \caracter[1]_i_121\ : label is "lutpair71";
  attribute HLUTNM of \caracter[1]_i_134\ : label is "lutpair74";
  attribute HLUTNM of \caracter[1]_i_135\ : label is "lutpair73";
  attribute HLUTNM of \caracter[1]_i_136\ : label is "lutpair72";
  attribute HLUTNM of \caracter[1]_i_139\ : label is "lutpair74";
  attribute HLUTNM of \caracter[1]_i_140\ : label is "lutpair73";
  attribute HLUTNM of \caracter[1]_i_144\ : label is "lutpair3";
  attribute HLUTNM of \caracter[1]_i_149\ : label is "lutpair3";
  attribute SOFT_HLUTNM of \caracter[1]_i_162\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \caracter[1]_i_163\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \caracter[1]_i_164\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \caracter[1]_i_165\ : label is "soft_lutpair51";
  attribute HLUTNM of \caracter[1]_i_174\ : label is "lutpair70";
  attribute HLUTNM of \caracter[1]_i_178\ : label is "lutpair70";
  attribute HLUTNM of \caracter[1]_i_207\ : label is "lutpair76";
  attribute HLUTNM of \caracter[1]_i_208\ : label is "lutpair75";
  attribute HLUTNM of \caracter[1]_i_212\ : label is "lutpair76";
  attribute HLUTNM of \caracter[1]_i_213\ : label is "lutpair75";
  attribute SOFT_HLUTNM of \caracter[1]_i_22\ : label is "soft_lutpair29";
  attribute HLUTNM of \caracter[1]_i_320\ : label is "lutpair2";
  attribute HLUTNM of \caracter[1]_i_321\ : label is "lutpair1";
  attribute HLUTNM of \caracter[1]_i_325\ : label is "lutpair2";
  attribute HLUTNM of \caracter[1]_i_334\ : label is "lutpair0";
  attribute HLUTNM of \caracter[1]_i_337\ : label is "lutpair81";
  attribute HLUTNM of \caracter[1]_i_339\ : label is "lutpair0";
  attribute HLUTNM of \caracter[1]_i_349\ : label is "lutpair1";
  attribute HLUTNM of \caracter[1]_i_357\ : label is "lutpair81";
  attribute SOFT_HLUTNM of \caracter[1]_i_81\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \caracter[1]_i_82\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \caracter[1]_i_83\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \caracter[1]_i_84\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \caracter[1]_i_91\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \caracter[2]_i_10\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \caracter[2]_i_23\ : label is "soft_lutpair27";
  attribute HLUTNM of \caracter[3]_i_120\ : label is "lutpair77";
  attribute HLUTNM of \caracter[3]_i_125\ : label is "lutpair77";
  attribute HLUTNM of \caracter[3]_i_138\ : label is "lutpair79";
  attribute HLUTNM of \caracter[3]_i_139\ : label is "lutpair78";
  attribute HLUTNM of \caracter[3]_i_143\ : label is "lutpair79";
  attribute SOFT_HLUTNM of \caracter[3]_i_144\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \caracter[3]_i_147\ : label is "soft_lutpair34";
  attribute HLUTNM of \caracter[3]_i_169\ : label is "lutpair78";
  attribute SOFT_HLUTNM of \caracter[3]_i_175\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \caracter[3]_i_22\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \caracter[3]_i_29\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \caracter[3]_i_39\ : label is "soft_lutpair31";
  attribute HLUTNM of \caracter[3]_i_54\ : label is "lutpair80";
  attribute HLUTNM of \caracter[3]_i_59\ : label is "lutpair80";
begin
  \caracter_reg[1]_i_15_0\ <= \^caracter_reg[1]_i_15_0\;
  \disp_dinero[5]_0\(1 downto 0) <= \^disp_dinero[5]_0\(1 downto 0);
  \total_string_reg[0]\ <= \^total_string_reg[0]\;
\caracter[0]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66699666"
    )
        port map (
      I0 => \string_cent_decenas[1]5\(1),
      I1 => \caracter[0]_i_25_n_0\,
      I2 => \string_cent_decenas[1]5\(0),
      I3 => \^total_string_reg[0]\,
      I4 => \caracter[3]_i_9_n_0\,
      O => \caracter[0]_i_10_n_0\
    );
\caracter[0]_i_100\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_61_n_6\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(22),
      O => \caracter[0]_i_100_n_0\
    );
\caracter[0]_i_101\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_61_n_7\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(21),
      O => \caracter[0]_i_101_n_0\
    );
\caracter[0]_i_102\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(24),
      I1 => \caracter_reg[0]_i_61_n_4\,
      I2 => euros3(25),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_33_n_7\,
      O => \caracter[0]_i_102_n_0\
    );
\caracter[0]_i_103\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(23),
      I1 => \caracter_reg[0]_i_61_n_5\,
      I2 => euros3(24),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_61_n_4\,
      O => \caracter[0]_i_103_n_0\
    );
\caracter[0]_i_104\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(22),
      I1 => \caracter_reg[0]_i_61_n_6\,
      I2 => euros3(23),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_61_n_5\,
      O => \caracter[0]_i_104_n_0\
    );
\caracter[0]_i_105\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(21),
      I1 => \caracter_reg[0]_i_61_n_7\,
      I2 => euros3(22),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_61_n_6\,
      O => \caracter[0]_i_105_n_0\
    );
\caracter[0]_i_107\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A956"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(3),
      I1 => \caracter_reg[1]_i_40_n_5\,
      I2 => \caracter[1]_i_42_n_0\,
      I3 => \caracter_reg[1]_i_40_n_4\,
      O => \caracter[0]_i_107_n_0\
    );
\caracter[0]_i_108\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(2),
      I1 => \caracter[1]_i_42_n_0\,
      I2 => \caracter_reg[1]_i_40_n_5\,
      O => \caracter[0]_i_108_n_0\
    );
\caracter[0]_i_109\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(1),
      I1 => p_1_in(1),
      O => \caracter[0]_i_109_n_0\
    );
\caracter[0]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAA56655995"
    )
        port map (
      I0 => \caracter[2]_i_24_n_0\,
      I1 => \string_cent_decenas[1]5\(1),
      I2 => \string_cent_decenas[1]5\(0),
      I3 => \^total_string_reg[0]\,
      I4 => \caracter[0]_i_25_n_0\,
      I5 => \caracter[3]_i_9_n_0\,
      O => \caracter[0]_i_11_n_0\
    );
\caracter[0]_i_110\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(0),
      I1 => p_1_in(0),
      O => \caracter[0]_i_110_n_0\
    );
\caracter[0]_i_112\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_120_n_7\,
      I1 => \caracter_reg[0]_i_120_n_5\,
      I2 => \caracter_reg[0]_i_70_n_6\,
      O => \caracter[0]_i_112_n_0\
    );
\caracter[0]_i_113\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_240_n_4\,
      I1 => \caracter_reg[0]_i_120_n_6\,
      I2 => \caracter_reg[0]_i_70_n_7\,
      O => \caracter[0]_i_113_n_0\
    );
\caracter[0]_i_114\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_240_n_5\,
      I1 => \caracter_reg[0]_i_120_n_7\,
      I2 => \caracter_reg[0]_i_120_n_4\,
      O => \caracter[0]_i_114_n_0\
    );
\caracter[0]_i_115\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_240_n_6\,
      I1 => \caracter_reg[0]_i_240_n_4\,
      I2 => \caracter_reg[0]_i_120_n_5\,
      O => \caracter[0]_i_115_n_0\
    );
\caracter[0]_i_116\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_120_n_6\,
      I1 => \caracter_reg[0]_i_120_n_4\,
      I2 => \caracter_reg[0]_i_70_n_5\,
      I3 => \caracter[0]_i_112_n_0\,
      O => \caracter[0]_i_116_n_0\
    );
\caracter[0]_i_117\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_120_n_7\,
      I1 => \caracter_reg[0]_i_120_n_5\,
      I2 => \caracter_reg[0]_i_70_n_6\,
      I3 => \caracter[0]_i_113_n_0\,
      O => \caracter[0]_i_117_n_0\
    );
\caracter[0]_i_118\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_240_n_4\,
      I1 => \caracter_reg[0]_i_120_n_6\,
      I2 => \caracter_reg[0]_i_70_n_7\,
      I3 => \caracter[0]_i_114_n_0\,
      O => \caracter[0]_i_118_n_0\
    );
\caracter[0]_i_119\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_240_n_5\,
      I1 => \caracter_reg[0]_i_120_n_7\,
      I2 => \caracter_reg[0]_i_120_n_4\,
      I3 => \caracter[0]_i_115_n_0\,
      O => \caracter[0]_i_119_n_0\
    );
\caracter[0]_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter[2]_i_13_n_0\,
      I1 => \caracter[0]_i_11_n_0\,
      I2 => \caracter[3]_i_25_n_0\,
      O => \caracter[0]_i_12_n_0\
    );
\caracter[0]_i_121\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \caracter_reg[0]_i_249_n_4\,
      I1 => \caracter_reg[0]_i_250_n_7\,
      I2 => \caracter_reg[0]_i_251_n_0\,
      I3 => \caracter_reg[0]_i_252_n_1\,
      O => \caracter[0]_i_121_n_0\
    );
\caracter[0]_i_122\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \caracter_reg[0]_i_249_n_5\,
      I1 => \caracter_reg[0]_i_249_n_4\,
      I2 => \caracter_reg[0]_i_251_n_0\,
      I3 => \caracter_reg[0]_i_252_n_1\,
      O => \caracter[0]_i_122_n_0\
    );
\caracter[0]_i_123\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \caracter_reg[0]_i_249_n_6\,
      I1 => \caracter_reg[0]_i_249_n_5\,
      I2 => \caracter_reg[0]_i_251_n_0\,
      I3 => \caracter_reg[0]_i_252_n_1\,
      O => \caracter[0]_i_123_n_0\
    );
\caracter[0]_i_124\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \caracter_reg[0]_i_249_n_7\,
      I1 => \caracter_reg[0]_i_249_n_6\,
      I2 => \caracter_reg[0]_i_251_n_0\,
      I3 => \caracter_reg[0]_i_252_n_1\,
      O => \caracter[0]_i_124_n_0\
    );
\caracter[0]_i_125\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"566AA995"
    )
        port map (
      I0 => \caracter[0]_i_121_n_0\,
      I1 => \caracter_reg[0]_i_250_n_7\,
      I2 => \caracter_reg[0]_i_251_n_0\,
      I3 => \caracter_reg[0]_i_252_n_1\,
      I4 => \caracter_reg[0]_i_250_n_6\,
      O => \caracter[0]_i_125_n_0\
    );
\caracter[0]_i_126\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"366CC993"
    )
        port map (
      I0 => \caracter_reg[0]_i_249_n_4\,
      I1 => \caracter_reg[0]_i_250_n_7\,
      I2 => \caracter_reg[0]_i_251_n_0\,
      I3 => \caracter_reg[0]_i_252_n_1\,
      I4 => \caracter[0]_i_122_n_0\,
      O => \caracter[0]_i_126_n_0\
    );
\caracter[0]_i_127\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"366CC993"
    )
        port map (
      I0 => \caracter_reg[0]_i_249_n_5\,
      I1 => \caracter_reg[0]_i_249_n_4\,
      I2 => \caracter_reg[0]_i_251_n_0\,
      I3 => \caracter_reg[0]_i_252_n_1\,
      I4 => \caracter[0]_i_123_n_0\,
      O => \caracter[0]_i_127_n_0\
    );
\caracter[0]_i_128\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"366CC993"
    )
        port map (
      I0 => \caracter_reg[0]_i_249_n_6\,
      I1 => \caracter_reg[0]_i_249_n_5\,
      I2 => \caracter_reg[0]_i_251_n_0\,
      I3 => \caracter_reg[0]_i_252_n_1\,
      I4 => \caracter[0]_i_124_n_0\,
      O => \caracter[0]_i_128_n_0\
    );
\caracter[0]_i_129\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE80017"
    )
        port map (
      I0 => \caracter_reg[0]_i_250_n_7\,
      I1 => \caracter_reg[0]_i_252_n_1\,
      I2 => \caracter_reg[0]_i_251_n_0\,
      I3 => \caracter_reg[0]_i_250_n_6\,
      I4 => \caracter_reg[0]_i_250_n_1\,
      O => \caracter[0]_i_129_n_0\
    );
\caracter[0]_i_131\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_262_n_4\,
      I1 => \caracter_reg[0]_i_144_n_5\,
      I2 => \caracter_reg[0]_i_142_n_6\,
      I3 => \caracter_reg[0]_i_145_n_5\,
      I4 => \caracter[0]_i_263_n_0\,
      O => \caracter[0]_i_131_n_0\
    );
\caracter[0]_i_132\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_262_n_5\,
      I1 => \caracter_reg[0]_i_144_n_6\,
      I2 => \caracter_reg[0]_i_142_n_7\,
      I3 => \caracter_reg[0]_i_145_n_6\,
      I4 => \caracter[0]_i_264_n_0\,
      O => \caracter[0]_i_132_n_0\
    );
\caracter[0]_i_133\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_262_n_6\,
      I1 => \caracter_reg[0]_i_144_n_7\,
      I2 => \caracter_reg[0]_i_265_n_4\,
      I3 => \caracter_reg[0]_i_145_n_7\,
      I4 => \caracter[0]_i_266_n_0\,
      O => \caracter[0]_i_133_n_0\
    );
\caracter[0]_i_134\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_262_n_7\,
      I1 => \caracter_reg[0]_i_267_n_4\,
      I2 => \caracter_reg[0]_i_265_n_5\,
      I3 => \caracter[0]_i_268_n_0\,
      I4 => \caracter_reg[0]_i_269_n_4\,
      O => \caracter[0]_i_134_n_0\
    );
\caracter[0]_i_135\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \caracter[0]_i_131_n_0\,
      I1 => \caracter[0]_i_270_n_0\,
      I2 => \caracter_reg[0]_i_142_n_4\,
      I3 => \caracter_reg[0]_i_91_n_7\,
      I4 => \caracter_reg[0]_i_139_n_6\,
      I5 => \caracter_reg[0]_i_145_n_4\,
      O => \caracter[0]_i_135_n_0\
    );
\caracter[0]_i_136\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \caracter[0]_i_132_n_0\,
      I1 => \caracter[0]_i_271_n_0\,
      I2 => \caracter_reg[0]_i_142_n_5\,
      I3 => \caracter_reg[0]_i_144_n_4\,
      I4 => \caracter_reg[0]_i_139_n_7\,
      I5 => \caracter_reg[0]_i_145_n_5\,
      O => \caracter[0]_i_136_n_0\
    );
\caracter[0]_i_137\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \caracter[0]_i_133_n_0\,
      I1 => \caracter[0]_i_272_n_0\,
      I2 => \caracter_reg[0]_i_142_n_6\,
      I3 => \caracter_reg[0]_i_144_n_5\,
      I4 => \caracter_reg[0]_i_262_n_4\,
      I5 => \caracter_reg[0]_i_145_n_6\,
      O => \caracter[0]_i_137_n_0\
    );
\caracter[0]_i_138\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \caracter[0]_i_134_n_0\,
      I1 => \caracter[0]_i_273_n_0\,
      I2 => \caracter_reg[0]_i_142_n_7\,
      I3 => \caracter_reg[0]_i_144_n_6\,
      I4 => \caracter_reg[0]_i_262_n_5\,
      I5 => \caracter_reg[0]_i_145_n_7\,
      O => \caracter[0]_i_138_n_0\
    );
\caracter[0]_i_140\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_87_n_5\,
      I1 => \caracter_reg[0]_i_91_n_4\,
      I2 => \caracter_reg[0]_i_81_n_7\,
      O => \caracter[0]_i_140_n_0\
    );
\caracter[0]_i_141\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_87_n_6\,
      I1 => \caracter_reg[0]_i_91_n_5\,
      I2 => \caracter_reg[0]_i_139_n_4\,
      O => \caracter[0]_i_141_n_0\
    );
\caracter[0]_i_143\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_87_n_7\,
      I1 => \caracter_reg[0]_i_91_n_6\,
      I2 => \caracter_reg[0]_i_139_n_5\,
      O => \caracter[0]_i_143_n_0\
    );
\caracter[0]_i_146\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_142_n_4\,
      I1 => \caracter_reg[0]_i_91_n_7\,
      I2 => \caracter_reg[0]_i_139_n_6\,
      O => \caracter[0]_i_146_n_0\
    );
\caracter[0]_i_147\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter_reg[0]_i_139_n_4\,
      I1 => \caracter_reg[0]_i_91_n_5\,
      I2 => \caracter_reg[0]_i_87_n_6\,
      O => \caracter[0]_i_147_n_0\
    );
\caracter[0]_i_148\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter_reg[0]_i_139_n_6\,
      I1 => \caracter_reg[0]_i_91_n_7\,
      I2 => \caracter_reg[0]_i_142_n_4\,
      O => \caracter[0]_i_148_n_0\
    );
\caracter[0]_i_149\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => euros3(13),
      I1 => euros3(9),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(7),
      O => \caracter[0]_i_149_n_0\
    );
\caracter[0]_i_150\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => euros3(12),
      I1 => euros3(8),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(6),
      O => \caracter[0]_i_150_n_0\
    );
\caracter[0]_i_151\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCFA00FACCA000A0"
    )
        port map (
      I0 => euros3(7),
      I1 => \caracter_reg[0]_i_32_n_4\,
      I2 => euros3(5),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_32_n_6\,
      I5 => euros3(11),
      O => \caracter[0]_i_151_n_0\
    );
\caracter[0]_i_152\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCFA00FACCA000A0"
    )
        port map (
      I0 => euros3(6),
      I1 => \caracter_reg[0]_i_32_n_5\,
      I2 => euros3(4),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_32_n_7\,
      I5 => euros3(10),
      O => \caracter[0]_i_152_n_0\
    );
\caracter[0]_i_153\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(14),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(8),
      I3 => euros3(10),
      I4 => \caracter[0]_i_149_n_0\,
      O => \caracter[0]_i_153_n_0\
    );
\caracter[0]_i_154\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5659A6A95956A9A6"
    )
        port map (
      I0 => \caracter[0]_i_150_n_0\,
      I1 => euros3(13),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(7),
      I4 => \caracter_reg[0]_i_32_n_4\,
      I5 => euros3(9),
      O => \caracter[0]_i_154_n_0\
    );
\caracter[0]_i_155\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5A665A995A995A66"
    )
        port map (
      I0 => \caracter[0]_i_151_n_0\,
      I1 => euros3(6),
      I2 => \caracter_reg[0]_i_32_n_5\,
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => euros3(8),
      I5 => euros3(12),
      O => \caracter[0]_i_155_n_0\
    );
\caracter[0]_i_156\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9969666999966696"
    )
        port map (
      I0 => \caracter[0]_i_152_n_0\,
      I1 => euros2(5),
      I2 => euros3(7),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_32_n_4\,
      I5 => euros3(11),
      O => \caracter[0]_i_156_n_0\
    );
\caracter[0]_i_157\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(22),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(18),
      I3 => euros3(20),
      O => \caracter[0]_i_157_n_0\
    );
\caracter[0]_i_158\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(21),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(17),
      I3 => euros3(19),
      O => \caracter[0]_i_158_n_0\
    );
\caracter[0]_i_159\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(20),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(16),
      I3 => euros3(18),
      O => \caracter[0]_i_159_n_0\
    );
\caracter[0]_i_160\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(19),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(15),
      I3 => euros3(17),
      O => \caracter[0]_i_160_n_0\
    );
\caracter[0]_i_161\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(23),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(19),
      I3 => euros3(21),
      I4 => \caracter[0]_i_157_n_0\,
      O => \caracter[0]_i_161_n_0\
    );
\caracter[0]_i_162\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(22),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(18),
      I3 => euros3(20),
      I4 => \caracter[0]_i_158_n_0\,
      O => \caracter[0]_i_162_n_0\
    );
\caracter[0]_i_163\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(21),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(17),
      I3 => euros3(19),
      I4 => \caracter[0]_i_159_n_0\,
      O => \caracter[0]_i_163_n_0\
    );
\caracter[0]_i_164\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(20),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(16),
      I3 => euros3(18),
      I4 => \caracter[0]_i_160_n_0\,
      O => \caracter[0]_i_164_n_0\
    );
\caracter[0]_i_165\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(16),
      I1 => \caracter_reg[0]_i_22_n_3\,
      O => euros2(16)
    );
\caracter[0]_i_166\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(15),
      I1 => \caracter_reg[0]_i_22_n_3\,
      O => euros2(15)
    );
\caracter[0]_i_167\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(14),
      I1 => \caracter_reg[0]_i_22_n_3\,
      O => euros2(14)
    );
\caracter[0]_i_168\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(13),
      I1 => \caracter_reg[0]_i_22_n_3\,
      O => euros2(13)
    );
\caracter[0]_i_169\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(16),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(19),
      O => \caracter[0]_i_169_n_0\
    );
\caracter[0]_i_170\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(15),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(18),
      O => \caracter[0]_i_170_n_0\
    );
\caracter[0]_i_171\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(14),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(17),
      O => \caracter[0]_i_171_n_0\
    );
\caracter[0]_i_172\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(13),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(16),
      O => \caracter[0]_i_172_n_0\
    );
\caracter[0]_i_173\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => euros3(29),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(24),
      O => \caracter[0]_i_173_n_0\
    );
\caracter[0]_i_174\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(28),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(30),
      I3 => euros3(23),
      O => \caracter[0]_i_174_n_0\
    );
\caracter[0]_i_175\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(27),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(29),
      I3 => euros3(22),
      O => \caracter[0]_i_175_n_0\
    );
\caracter[0]_i_176\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(26),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(28),
      I3 => euros3(21),
      O => \caracter[0]_i_176_n_0\
    );
\caracter[0]_i_177\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00870078"
    )
        port map (
      I0 => euros3(24),
      I1 => euros3(29),
      I2 => euros3(30),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => euros3(25),
      O => \caracter[0]_i_177_n_0\
    );
\caracter[0]_i_178\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000E817000017E8"
    )
        port map (
      I0 => euros3(23),
      I1 => euros3(30),
      I2 => euros3(28),
      I3 => euros3(29),
      I4 => \caracter_reg[0]_i_22_n_3\,
      I5 => euros3(24),
      O => \caracter[0]_i_178_n_0\
    );
\caracter[0]_i_179\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A6A9A9A6"
    )
        port map (
      I0 => \caracter[0]_i_175_n_0\,
      I1 => euros3(28),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(30),
      I4 => euros3(23),
      O => \caracter[0]_i_179_n_0\
    );
\caracter[0]_i_180\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(27),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(29),
      I3 => euros3(22),
      I4 => \caracter[0]_i_176_n_0\,
      O => \caracter[0]_i_180_n_0\
    );
\caracter[0]_i_181\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(12),
      I1 => \caracter_reg[0]_i_22_n_3\,
      O => euros2(12)
    );
\caracter[0]_i_182\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(11),
      I1 => \caracter_reg[0]_i_22_n_3\,
      O => euros2(11)
    );
\caracter[0]_i_183\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(10),
      I1 => \caracter_reg[0]_i_22_n_3\,
      O => euros2(10)
    );
\caracter[0]_i_184\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(9),
      I1 => \caracter_reg[0]_i_22_n_3\,
      O => euros2(9)
    );
\caracter[0]_i_185\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(12),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(15),
      O => \caracter[0]_i_185_n_0\
    );
\caracter[0]_i_186\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(11),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(14),
      O => \caracter[0]_i_186_n_0\
    );
\caracter[0]_i_187\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(10),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(13),
      O => \caracter[0]_i_187_n_0\
    );
\caracter[0]_i_188\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(9),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(12),
      O => \caracter[0]_i_188_n_0\
    );
\caracter[0]_i_189\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(25),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(27),
      I3 => euros3(20),
      O => \caracter[0]_i_189_n_0\
    );
\caracter[0]_i_190\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(24),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(26),
      I3 => euros3(19),
      O => \caracter[0]_i_190_n_0\
    );
\caracter[0]_i_191\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(23),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(25),
      I3 => euros3(18),
      O => \caracter[0]_i_191_n_0\
    );
\caracter[0]_i_192\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(22),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(24),
      I3 => euros3(17),
      O => \caracter[0]_i_192_n_0\
    );
\caracter[0]_i_193\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(26),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(28),
      I3 => euros3(21),
      I4 => \caracter[0]_i_189_n_0\,
      O => \caracter[0]_i_193_n_0\
    );
\caracter[0]_i_194\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(25),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(27),
      I3 => euros3(20),
      I4 => \caracter[0]_i_190_n_0\,
      O => \caracter[0]_i_194_n_0\
    );
\caracter[0]_i_195\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(24),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(26),
      I3 => euros3(19),
      I4 => \caracter[0]_i_191_n_0\,
      O => \caracter[0]_i_195_n_0\
    );
\caracter[0]_i_196\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(23),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(25),
      I3 => euros3(18),
      I4 => \caracter[0]_i_192_n_0\,
      O => \caracter[0]_i_196_n_0\
    );
\caracter[0]_i_197\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(18),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(14),
      I3 => euros3(16),
      O => \caracter[0]_i_197_n_0\
    );
\caracter[0]_i_198\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(17),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(13),
      I3 => euros3(15),
      O => \caracter[0]_i_198_n_0\
    );
\caracter[0]_i_199\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(16),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(12),
      I3 => euros3(14),
      O => \caracter[0]_i_199_n_0\
    );
\caracter[0]_i_200\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(15),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(11),
      I3 => euros3(13),
      O => \caracter[0]_i_200_n_0\
    );
\caracter[0]_i_201\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(19),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(15),
      I3 => euros3(17),
      I4 => \caracter[0]_i_197_n_0\,
      O => \caracter[0]_i_201_n_0\
    );
\caracter[0]_i_202\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(18),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(14),
      I3 => euros3(16),
      I4 => \caracter[0]_i_198_n_0\,
      O => \caracter[0]_i_202_n_0\
    );
\caracter[0]_i_203\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(17),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(13),
      I3 => euros3(15),
      I4 => \caracter[0]_i_199_n_0\,
      O => \caracter[0]_i_203_n_0\
    );
\caracter[0]_i_204\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(16),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(12),
      I3 => euros3(14),
      I4 => \caracter[0]_i_200_n_0\,
      O => \caracter[0]_i_204_n_0\
    );
\caracter[0]_i_205\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(26),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(22),
      I3 => euros3(24),
      O => \caracter[0]_i_205_n_0\
    );
\caracter[0]_i_206\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(25),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(21),
      I3 => euros3(23),
      O => \caracter[0]_i_206_n_0\
    );
\caracter[0]_i_207\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(24),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(20),
      I3 => euros3(22),
      O => \caracter[0]_i_207_n_0\
    );
\caracter[0]_i_208\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(23),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(19),
      I3 => euros3(21),
      O => \caracter[0]_i_208_n_0\
    );
\caracter[0]_i_209\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(27),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(23),
      I3 => euros3(25),
      I4 => \caracter[0]_i_205_n_0\,
      O => \caracter[0]_i_209_n_0\
    );
\caracter[0]_i_210\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(26),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(22),
      I3 => euros3(24),
      I4 => \caracter[0]_i_206_n_0\,
      O => \caracter[0]_i_210_n_0\
    );
\caracter[0]_i_211\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(25),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(21),
      I3 => euros3(23),
      I4 => \caracter[0]_i_207_n_0\,
      O => \caracter[0]_i_211_n_0\
    );
\caracter[0]_i_212\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(24),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(20),
      I3 => euros3(22),
      I4 => \caracter[0]_i_208_n_0\,
      O => \caracter[0]_i_212_n_0\
    );
\caracter[0]_i_213\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(17),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(13),
      I3 => euros3(11),
      O => \caracter[0]_i_213_n_0\
    );
\caracter[0]_i_214\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(16),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(12),
      I3 => euros3(10),
      O => \caracter[0]_i_214_n_0\
    );
\caracter[0]_i_215\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(15),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(11),
      I3 => euros3(9),
      O => \caracter[0]_i_215_n_0\
    );
\caracter[0]_i_216\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(14),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(8),
      I3 => euros3(10),
      O => \caracter[0]_i_216_n_0\
    );
\caracter[0]_i_217\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(18),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(14),
      I3 => euros3(12),
      I4 => \caracter[0]_i_213_n_0\,
      O => \caracter[0]_i_217_n_0\
    );
\caracter[0]_i_218\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(17),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(13),
      I3 => euros3(11),
      I4 => \caracter[0]_i_214_n_0\,
      O => \caracter[0]_i_218_n_0\
    );
\caracter[0]_i_219\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(16),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(12),
      I3 => euros3(10),
      I4 => \caracter[0]_i_215_n_0\,
      O => \caracter[0]_i_219_n_0\
    );
\caracter[0]_i_220\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(15),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(11),
      I3 => euros3(9),
      I4 => \caracter[0]_i_216_n_0\,
      O => \caracter[0]_i_220_n_0\
    );
\caracter[0]_i_222\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_111_n_4\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(20),
      O => \caracter[0]_i_222_n_0\
    );
\caracter[0]_i_223\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_111_n_5\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(19),
      O => \caracter[0]_i_223_n_0\
    );
\caracter[0]_i_224\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_111_n_6\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(18),
      O => \caracter[0]_i_224_n_0\
    );
\caracter[0]_i_225\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_111_n_7\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(17),
      O => \caracter[0]_i_225_n_0\
    );
\caracter[0]_i_226\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(20),
      I1 => \caracter_reg[0]_i_111_n_4\,
      I2 => euros3(21),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_61_n_7\,
      O => \caracter[0]_i_226_n_0\
    );
\caracter[0]_i_227\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(19),
      I1 => \caracter_reg[0]_i_111_n_5\,
      I2 => euros3(20),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_111_n_4\,
      O => \caracter[0]_i_227_n_0\
    );
\caracter[0]_i_228\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(18),
      I1 => \caracter_reg[0]_i_111_n_6\,
      I2 => euros3(19),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_111_n_5\,
      O => \caracter[0]_i_228_n_0\
    );
\caracter[0]_i_229\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(17),
      I1 => \caracter_reg[0]_i_111_n_7\,
      I2 => euros3(18),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_111_n_6\,
      O => \caracter[0]_i_229_n_0\
    );
\caracter[0]_i_232\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_240_n_7\,
      I1 => \caracter_reg[0]_i_240_n_5\,
      I2 => \caracter_reg[0]_i_120_n_6\,
      O => \caracter[0]_i_232_n_0\
    );
\caracter[0]_i_233\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_328_n_4\,
      I1 => \caracter_reg[0]_i_240_n_6\,
      I2 => \caracter_reg[0]_i_120_n_7\,
      O => \caracter[0]_i_233_n_0\
    );
\caracter[0]_i_234\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_328_n_5\,
      I1 => \caracter_reg[0]_i_240_n_7\,
      I2 => \caracter_reg[0]_i_240_n_4\,
      O => \caracter[0]_i_234_n_0\
    );
\caracter[0]_i_235\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_328_n_6\,
      I1 => \caracter_reg[0]_i_328_n_4\,
      I2 => \caracter_reg[0]_i_240_n_5\,
      O => \caracter[0]_i_235_n_0\
    );
\caracter[0]_i_236\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_240_n_6\,
      I1 => \caracter_reg[0]_i_240_n_4\,
      I2 => \caracter_reg[0]_i_120_n_5\,
      I3 => \caracter[0]_i_232_n_0\,
      O => \caracter[0]_i_236_n_0\
    );
\caracter[0]_i_237\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_240_n_7\,
      I1 => \caracter_reg[0]_i_240_n_5\,
      I2 => \caracter_reg[0]_i_120_n_6\,
      I3 => \caracter[0]_i_233_n_0\,
      O => \caracter[0]_i_237_n_0\
    );
\caracter[0]_i_238\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_328_n_4\,
      I1 => \caracter_reg[0]_i_240_n_6\,
      I2 => \caracter_reg[0]_i_120_n_7\,
      I3 => \caracter[0]_i_234_n_0\,
      O => \caracter[0]_i_238_n_0\
    );
\caracter[0]_i_239\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_328_n_5\,
      I1 => \caracter_reg[0]_i_240_n_7\,
      I2 => \caracter_reg[0]_i_240_n_4\,
      I3 => \caracter[0]_i_235_n_0\,
      O => \caracter[0]_i_239_n_0\
    );
\caracter[0]_i_241\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"800E08E0"
    )
        port map (
      I0 => \caracter_reg[0]_i_337_n_4\,
      I1 => \caracter_reg[0]_i_252_n_6\,
      I2 => \caracter_reg[0]_i_249_n_7\,
      I3 => \caracter_reg[0]_i_251_n_0\,
      I4 => \caracter_reg[0]_i_252_n_1\,
      O => \caracter[0]_i_241_n_0\
    );
\caracter[0]_i_242\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60990090"
    )
        port map (
      I0 => \caracter_reg[0]_i_252_n_6\,
      I1 => \caracter_reg[0]_i_337_n_4\,
      I2 => \caracter_reg[0]_i_337_n_5\,
      I3 => \caracter_reg[0]_i_251_n_0\,
      I4 => \caracter_reg[0]_i_252_n_7\,
      O => \caracter[0]_i_242_n_0\
    );
\caracter[0]_i_243\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60990090"
    )
        port map (
      I0 => \caracter_reg[0]_i_252_n_7\,
      I1 => \caracter_reg[0]_i_337_n_5\,
      I2 => \caracter_reg[0]_i_337_n_6\,
      I3 => \caracter_reg[0]_i_251_n_0\,
      I4 => \caracter_reg[0]_i_338_n_4\,
      O => \caracter[0]_i_243_n_0\
    );
\caracter[0]_i_244\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60990090"
    )
        port map (
      I0 => \caracter_reg[0]_i_338_n_4\,
      I1 => \caracter_reg[0]_i_337_n_6\,
      I2 => \caracter_reg[0]_i_337_n_7\,
      I3 => \caracter_reg[0]_i_251_n_0\,
      I4 => \caracter_reg[0]_i_338_n_5\,
      O => \caracter[0]_i_244_n_0\
    );
\caracter[0]_i_245\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"366CC993"
    )
        port map (
      I0 => \caracter_reg[0]_i_249_n_7\,
      I1 => \caracter_reg[0]_i_249_n_6\,
      I2 => \caracter_reg[0]_i_251_n_0\,
      I3 => \caracter_reg[0]_i_252_n_1\,
      I4 => \caracter[0]_i_241_n_0\,
      O => \caracter[0]_i_245_n_0\
    );
\caracter[0]_i_246\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669669966996996"
    )
        port map (
      I0 => \caracter[0]_i_242_n_0\,
      I1 => \caracter_reg[0]_i_252_n_1\,
      I2 => \caracter_reg[0]_i_251_n_0\,
      I3 => \caracter_reg[0]_i_249_n_7\,
      I4 => \caracter_reg[0]_i_252_n_6\,
      I5 => \caracter_reg[0]_i_337_n_4\,
      O => \caracter[0]_i_246_n_0\
    );
\caracter[0]_i_247\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AA9955695566AA9"
    )
        port map (
      I0 => \caracter[0]_i_243_n_0\,
      I1 => \caracter_reg[0]_i_252_n_7\,
      I2 => \caracter_reg[0]_i_251_n_0\,
      I3 => \caracter_reg[0]_i_337_n_5\,
      I4 => \caracter_reg[0]_i_337_n_4\,
      I5 => \caracter_reg[0]_i_252_n_6\,
      O => \caracter[0]_i_247_n_0\
    );
\caracter[0]_i_248\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AA9955695566AA9"
    )
        port map (
      I0 => \caracter[0]_i_244_n_0\,
      I1 => \caracter_reg[0]_i_338_n_4\,
      I2 => \caracter_reg[0]_i_251_n_0\,
      I3 => \caracter_reg[0]_i_337_n_6\,
      I4 => \caracter_reg[0]_i_337_n_5\,
      I5 => \caracter_reg[0]_i_252_n_7\,
      O => \caracter[0]_i_248_n_0\
    );
\caracter[0]_i_25\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CD99"
    )
        port map (
      I0 => \caracter_reg[1]_i_16_n_7\,
      I1 => \caracter_reg[1]_i_15_n_6\,
      I2 => \caracter_reg[1]_i_15_n_5\,
      I3 => \caracter_reg[1]_i_15_n_4\,
      O => \caracter[0]_i_25_n_0\
    );
\caracter[0]_i_254\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_265_n_6\,
      I1 => euros2(2),
      I2 => \caracter_reg[0]_i_267_n_5\,
      I3 => \caracter_reg[0]_i_269_n_5\,
      I4 => \caracter[0]_i_371_n_0\,
      O => \caracter[0]_i_254_n_0\
    );
\caracter[0]_i_255\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DDD4D444"
    )
        port map (
      I0 => \caracter[0]_i_372_n_0\,
      I1 => \caracter_reg[0]_i_269_n_6\,
      I2 => euros2(1),
      I3 => \caracter_reg[0]_i_265_n_7\,
      I4 => \caracter_reg[0]_i_267_n_6\,
      O => \caracter[0]_i_255_n_0\
    );
\caracter[0]_i_256\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DDD4D444"
    )
        port map (
      I0 => \caracter[0]_i_374_n_0\,
      I1 => \caracter_reg[0]_i_269_n_7\,
      I2 => \caracter_reg[0]_i_375_n_4\,
      I3 => euros2(0),
      I4 => \caracter_reg[0]_i_267_n_7\,
      O => \caracter[0]_i_256_n_0\
    );
\caracter[0]_i_257\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EBBE822882288228"
    )
        port map (
      I0 => \caracter_reg[0]_i_376_n_4\,
      I1 => \caracter_reg[0]_i_375_n_4\,
      I2 => euros2(0),
      I3 => \caracter_reg[0]_i_267_n_7\,
      I4 => \caracter_reg[0]_i_375_n_5\,
      I5 => \caracter_reg[0]_i_377_n_4\,
      O => \caracter[0]_i_257_n_0\
    );
\caracter[0]_i_258\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \caracter[0]_i_254_n_0\,
      I1 => \caracter_reg[0]_i_269_n_4\,
      I2 => \caracter[0]_i_268_n_0\,
      I3 => \caracter_reg[0]_i_262_n_7\,
      I4 => \caracter_reg[0]_i_267_n_4\,
      I5 => \caracter_reg[0]_i_265_n_5\,
      O => \caracter[0]_i_258_n_0\
    );
\caracter[0]_i_259\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"566AA995A995566A"
    )
        port map (
      I0 => \caracter[0]_i_255_n_0\,
      I1 => \caracter_reg[0]_i_265_n_6\,
      I2 => euros2(2),
      I3 => \caracter_reg[0]_i_267_n_5\,
      I4 => \caracter_reg[0]_i_269_n_5\,
      I5 => \caracter[0]_i_371_n_0\,
      O => \caracter[0]_i_259_n_0\
    );
\caracter[0]_i_26\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => p_1_in(0),
      I1 => \^disp_dinero[5]_0\(1),
      I2 => \caracter_reg[3]_i_126_0\(0),
      O => \^total_string_reg[0]\
    );
\caracter[0]_i_260\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A995566A566AA995"
    )
        port map (
      I0 => \caracter[0]_i_256_n_0\,
      I1 => euros2(1),
      I2 => \caracter_reg[0]_i_265_n_7\,
      I3 => \caracter_reg[0]_i_267_n_6\,
      I4 => \caracter_reg[0]_i_269_n_6\,
      I5 => \caracter[0]_i_372_n_0\,
      O => \caracter[0]_i_260_n_0\
    );
\caracter[0]_i_261\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9696966996696969"
    )
        port map (
      I0 => \caracter[0]_i_257_n_0\,
      I1 => \caracter[0]_i_374_n_0\,
      I2 => \caracter_reg[0]_i_269_n_7\,
      I3 => \caracter_reg[0]_i_375_n_4\,
      I4 => euros2(0),
      I5 => \caracter_reg[0]_i_267_n_7\,
      O => \caracter[0]_i_261_n_0\
    );
\caracter[0]_i_263\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_142_n_5\,
      I1 => \caracter_reg[0]_i_144_n_4\,
      I2 => \caracter_reg[0]_i_139_n_7\,
      O => \caracter[0]_i_263_n_0\
    );
\caracter[0]_i_264\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_142_n_6\,
      I1 => \caracter_reg[0]_i_144_n_5\,
      I2 => \caracter_reg[0]_i_262_n_4\,
      O => \caracter[0]_i_264_n_0\
    );
\caracter[0]_i_266\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_142_n_7\,
      I1 => \caracter_reg[0]_i_144_n_6\,
      I2 => \caracter_reg[0]_i_262_n_5\,
      O => \caracter[0]_i_266_n_0\
    );
\caracter[0]_i_268\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_265_n_4\,
      I1 => \caracter_reg[0]_i_144_n_7\,
      I2 => \caracter_reg[0]_i_262_n_6\,
      O => \caracter[0]_i_268_n_0\
    );
\caracter[0]_i_270\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter_reg[0]_i_139_n_7\,
      I1 => \caracter_reg[0]_i_144_n_4\,
      I2 => \caracter_reg[0]_i_142_n_5\,
      O => \caracter[0]_i_270_n_0\
    );
\caracter[0]_i_271\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter_reg[0]_i_262_n_4\,
      I1 => \caracter_reg[0]_i_144_n_5\,
      I2 => \caracter_reg[0]_i_142_n_6\,
      O => \caracter[0]_i_271_n_0\
    );
\caracter[0]_i_272\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter_reg[0]_i_262_n_5\,
      I1 => \caracter_reg[0]_i_144_n_6\,
      I2 => \caracter_reg[0]_i_142_n_7\,
      O => \caracter[0]_i_272_n_0\
    );
\caracter[0]_i_273\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter_reg[0]_i_262_n_6\,
      I1 => \caracter_reg[0]_i_144_n_7\,
      I2 => \caracter_reg[0]_i_265_n_4\,
      O => \caracter[0]_i_273_n_0\
    );
\caracter[0]_i_274\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCFA00FACCA000A0"
    )
        port map (
      I0 => euros3(5),
      I1 => \caracter_reg[0]_i_32_n_6\,
      I2 => euros3(3),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_56_n_4\,
      I5 => euros3(9),
      O => \caracter[0]_i_274_n_0\
    );
\caracter[0]_i_275\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCFA00FACCA000A0"
    )
        port map (
      I0 => euros3(2),
      I1 => \caracter_reg[0]_i_56_n_5\,
      I2 => euros3(4),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_32_n_7\,
      I5 => euros3(8),
      O => \caracter[0]_i_275_n_0\
    );
\caracter[0]_i_276\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DFD54F45D5D04540"
    )
        port map (
      I0 => \caracter[0]_i_409_n_0\,
      I1 => \caracter_reg[0]_i_56_n_4\,
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(3),
      I4 => \caracter_reg[0]_i_56_n_6\,
      I5 => euros3(1),
      O => \caracter[0]_i_276_n_0\
    );
\caracter[0]_i_277\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"656A959A6A659A95"
    )
        port map (
      I0 => \caracter[0]_i_409_n_0\,
      I1 => \caracter_reg[0]_i_56_n_6\,
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(1),
      I4 => \caracter_reg[0]_i_56_n_4\,
      I5 => euros3(3),
      O => \caracter[0]_i_277_n_0\
    );
\caracter[0]_i_278\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A6A956595956A9A6"
    )
        port map (
      I0 => \caracter[0]_i_274_n_0\,
      I1 => euros3(10),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(6),
      I4 => \caracter_reg[0]_i_32_n_5\,
      I5 => euros2(4),
      O => \caracter[0]_i_278_n_0\
    );
\caracter[0]_i_279\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A659A95656A959A6"
    )
        port map (
      I0 => \caracter[0]_i_275_n_0\,
      I1 => euros3(9),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros2(5),
      I4 => euros3(3),
      I5 => \caracter_reg[0]_i_56_n_4\,
      O => \caracter[0]_i_279_n_0\
    );
\caracter[0]_i_280\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A6A956595956A9A6"
    )
        port map (
      I0 => \caracter[0]_i_276_n_0\,
      I1 => euros3(8),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(4),
      I4 => \caracter_reg[0]_i_32_n_7\,
      I5 => euros2(2),
      O => \caracter[0]_i_280_n_0\
    );
\caracter[0]_i_281\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696969"
    )
        port map (
      I0 => euros2(3),
      I1 => euros2(1),
      I2 => \caracter[0]_i_409_n_0\,
      I3 => euros2(0),
      I4 => euros2(2),
      O => \caracter[0]_i_281_n_0\
    );
\caracter[0]_i_282\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(8),
      I1 => \caracter_reg[0]_i_22_n_3\,
      O => euros2(8)
    );
\caracter[0]_i_283\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E2"
    )
        port map (
      I0 => euros3(7),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => \caracter_reg[0]_i_32_n_4\,
      O => \caracter[0]_i_283_n_0\
    );
\caracter[0]_i_284\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E2"
    )
        port map (
      I0 => euros3(6),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => \caracter_reg[0]_i_32_n_5\,
      O => \caracter[0]_i_284_n_0\
    );
\caracter[0]_i_285\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_32_n_6\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(5),
      O => \caracter[0]_i_285_n_0\
    );
\caracter[0]_i_286\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(8),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(11),
      O => \caracter[0]_i_286_n_0\
    );
\caracter[0]_i_287\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3A35"
    )
        port map (
      I0 => euros3(7),
      I1 => \caracter_reg[0]_i_32_n_4\,
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(10),
      O => \caracter[0]_i_287_n_0\
    );
\caracter[0]_i_288\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3A35"
    )
        port map (
      I0 => euros3(6),
      I1 => \caracter_reg[0]_i_32_n_5\,
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(9),
      O => \caracter[0]_i_288_n_0\
    );
\caracter[0]_i_289\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3A35"
    )
        port map (
      I0 => euros3(5),
      I1 => \caracter_reg[0]_i_32_n_6\,
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(8),
      O => \caracter[0]_i_289_n_0\
    );
\caracter[0]_i_29\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_23_n_7\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(29),
      O => \caracter[0]_i_29_n_0\
    );
\caracter[0]_i_290\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(14),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(10),
      I3 => euros3(12),
      O => \caracter[0]_i_290_n_0\
    );
\caracter[0]_i_291\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(13),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(9),
      I3 => euros3(11),
      O => \caracter[0]_i_291_n_0\
    );
\caracter[0]_i_292\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(12),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(8),
      I3 => euros3(10),
      O => \caracter[0]_i_292_n_0\
    );
\caracter[0]_i_293\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFCD"
    )
        port map (
      I0 => euros3(9),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(11),
      I3 => euros3(7),
      O => \caracter[0]_i_293_n_0\
    );
\caracter[0]_i_294\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(15),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(11),
      I3 => euros3(13),
      I4 => \caracter[0]_i_290_n_0\,
      O => \caracter[0]_i_294_n_0\
    );
\caracter[0]_i_295\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(14),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(10),
      I3 => euros3(12),
      I4 => \caracter[0]_i_291_n_0\,
      O => \caracter[0]_i_295_n_0\
    );
\caracter[0]_i_296\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(13),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(9),
      I3 => euros3(11),
      I4 => \caracter[0]_i_292_n_0\,
      O => \caracter[0]_i_296_n_0\
    );
\caracter[0]_i_297\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(12),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(8),
      I3 => euros3(10),
      I4 => \caracter[0]_i_293_n_0\,
      O => \caracter[0]_i_297_n_0\
    );
\caracter[0]_i_298\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(21),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(23),
      I3 => euros3(16),
      O => \caracter[0]_i_298_n_0\
    );
\caracter[0]_i_299\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(20),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(22),
      I3 => euros3(15),
      O => \caracter[0]_i_299_n_0\
    );
\caracter[0]_i_30\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(29),
      I1 => \caracter_reg[0]_i_23_n_7\,
      I2 => euros3(30),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_23_n_6\,
      O => \caracter[0]_i_30_n_0\
    );
\caracter[0]_i_300\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(19),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(21),
      I3 => euros3(14),
      O => \caracter[0]_i_300_n_0\
    );
\caracter[0]_i_301\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(18),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(20),
      I3 => euros3(13),
      O => \caracter[0]_i_301_n_0\
    );
\caracter[0]_i_302\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(22),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(24),
      I3 => euros3(17),
      I4 => \caracter[0]_i_298_n_0\,
      O => \caracter[0]_i_302_n_0\
    );
\caracter[0]_i_303\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(21),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(23),
      I3 => euros3(16),
      I4 => \caracter[0]_i_299_n_0\,
      O => \caracter[0]_i_303_n_0\
    );
\caracter[0]_i_304\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(20),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(22),
      I3 => euros3(15),
      I4 => \caracter[0]_i_300_n_0\,
      O => \caracter[0]_i_304_n_0\
    );
\caracter[0]_i_305\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(19),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(21),
      I3 => euros3(14),
      I4 => \caracter[0]_i_301_n_0\,
      O => \caracter[0]_i_305_n_0\
    );
\caracter[0]_i_309\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_32_n_6\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(5),
      O => euros2(5)
    );
\caracter[0]_i_311\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_231_n_4\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(16),
      O => \caracter[0]_i_311_n_0\
    );
\caracter[0]_i_312\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_231_n_5\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(15),
      O => \caracter[0]_i_312_n_0\
    );
\caracter[0]_i_313\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_231_n_6\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(14),
      O => \caracter[0]_i_313_n_0\
    );
\caracter[0]_i_314\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_231_n_7\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(13),
      O => \caracter[0]_i_314_n_0\
    );
\caracter[0]_i_315\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(16),
      I1 => \caracter_reg[0]_i_231_n_4\,
      I2 => euros3(17),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_111_n_7\,
      O => \caracter[0]_i_315_n_0\
    );
\caracter[0]_i_316\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(15),
      I1 => \caracter_reg[0]_i_231_n_5\,
      I2 => euros3(16),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_231_n_4\,
      O => \caracter[0]_i_316_n_0\
    );
\caracter[0]_i_317\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(14),
      I1 => \caracter_reg[0]_i_231_n_6\,
      I2 => euros3(15),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_231_n_5\,
      O => \caracter[0]_i_317_n_0\
    );
\caracter[0]_i_318\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(13),
      I1 => \caracter_reg[0]_i_231_n_7\,
      I2 => euros3(14),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_231_n_6\,
      O => \caracter[0]_i_318_n_0\
    );
\caracter[0]_i_320\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_328_n_7\,
      I1 => \caracter_reg[0]_i_328_n_5\,
      I2 => \caracter_reg[0]_i_240_n_6\,
      O => \caracter[0]_i_320_n_0\
    );
\caracter[0]_i_321\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_438_n_4\,
      I1 => \caracter_reg[0]_i_328_n_6\,
      I2 => \caracter_reg[0]_i_240_n_7\,
      O => \caracter[0]_i_321_n_0\
    );
\caracter[0]_i_322\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_438_n_5\,
      I1 => \caracter_reg[0]_i_328_n_7\,
      I2 => \caracter_reg[0]_i_328_n_4\,
      O => \caracter[0]_i_322_n_0\
    );
\caracter[0]_i_323\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_438_n_6\,
      I1 => \caracter_reg[0]_i_438_n_4\,
      I2 => \caracter_reg[0]_i_328_n_5\,
      O => \caracter[0]_i_323_n_0\
    );
\caracter[0]_i_324\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_328_n_6\,
      I1 => \caracter_reg[0]_i_328_n_4\,
      I2 => \caracter_reg[0]_i_240_n_5\,
      I3 => \caracter[0]_i_320_n_0\,
      O => \caracter[0]_i_324_n_0\
    );
\caracter[0]_i_325\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_328_n_7\,
      I1 => \caracter_reg[0]_i_328_n_5\,
      I2 => \caracter_reg[0]_i_240_n_6\,
      I3 => \caracter[0]_i_321_n_0\,
      O => \caracter[0]_i_325_n_0\
    );
\caracter[0]_i_326\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_438_n_4\,
      I1 => \caracter_reg[0]_i_328_n_6\,
      I2 => \caracter_reg[0]_i_240_n_7\,
      I3 => \caracter[0]_i_322_n_0\,
      O => \caracter[0]_i_326_n_0\
    );
\caracter[0]_i_327\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_438_n_5\,
      I1 => \caracter_reg[0]_i_328_n_7\,
      I2 => \caracter_reg[0]_i_328_n_4\,
      I3 => \caracter[0]_i_323_n_0\,
      O => \caracter[0]_i_327_n_0\
    );
\caracter[0]_i_329\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E8E800E80000E8"
    )
        port map (
      I0 => \caracter_reg[0]_i_338_n_6\,
      I1 => \caracter_reg[0]_i_251_n_5\,
      I2 => \caracter_reg[0]_i_447_n_4\,
      I3 => \caracter_reg[0]_i_338_n_5\,
      I4 => \caracter_reg[0]_i_251_n_0\,
      I5 => \caracter_reg[0]_i_337_n_7\,
      O => \caracter[0]_i_329_n_0\
    );
\caracter[0]_i_330\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E80000E800E8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_338_n_7\,
      I1 => \caracter_reg[0]_i_251_n_6\,
      I2 => \caracter_reg[0]_i_447_n_5\,
      I3 => \caracter_reg[0]_i_251_n_5\,
      I4 => \caracter_reg[0]_i_447_n_4\,
      I5 => \caracter_reg[0]_i_338_n_6\,
      O => \caracter[0]_i_330_n_0\
    );
\caracter[0]_i_331\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E80000E800E8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_448_n_4\,
      I1 => \caracter_reg[0]_i_251_n_7\,
      I2 => \caracter_reg[0]_i_447_n_6\,
      I3 => \caracter_reg[0]_i_251_n_6\,
      I4 => \caracter_reg[0]_i_447_n_5\,
      I5 => \caracter_reg[0]_i_338_n_7\,
      O => \caracter[0]_i_331_n_0\
    );
\caracter[0]_i_332\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E80000E800E8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_448_n_5\,
      I1 => \caracter_reg[0]_i_350_n_4\,
      I2 => \caracter_reg[0]_i_447_n_7\,
      I3 => \caracter_reg[0]_i_251_n_7\,
      I4 => \caracter_reg[0]_i_447_n_6\,
      I5 => \caracter_reg[0]_i_448_n_4\,
      O => \caracter[0]_i_332_n_0\
    );
\caracter[0]_i_333\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AA9955695566AA9"
    )
        port map (
      I0 => \caracter[0]_i_329_n_0\,
      I1 => \caracter_reg[0]_i_338_n_5\,
      I2 => \caracter_reg[0]_i_251_n_0\,
      I3 => \caracter_reg[0]_i_337_n_7\,
      I4 => \caracter_reg[0]_i_337_n_6\,
      I5 => \caracter_reg[0]_i_338_n_4\,
      O => \caracter[0]_i_333_n_0\
    );
\caracter[0]_i_334\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99969666"
    )
        port map (
      I0 => \caracter[0]_i_330_n_0\,
      I1 => \caracter[0]_i_449_n_0\,
      I2 => \caracter_reg[0]_i_447_n_4\,
      I3 => \caracter_reg[0]_i_251_n_5\,
      I4 => \caracter_reg[0]_i_338_n_6\,
      O => \caracter[0]_i_334_n_0\
    );
\caracter[0]_i_335\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66696999"
    )
        port map (
      I0 => \caracter[0]_i_331_n_0\,
      I1 => \caracter[0]_i_450_n_0\,
      I2 => \caracter_reg[0]_i_447_n_5\,
      I3 => \caracter_reg[0]_i_251_n_6\,
      I4 => \caracter_reg[0]_i_338_n_7\,
      O => \caracter[0]_i_335_n_0\
    );
\caracter[0]_i_336\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66696999"
    )
        port map (
      I0 => \caracter[0]_i_332_n_0\,
      I1 => \caracter[0]_i_451_n_0\,
      I2 => \caracter_reg[0]_i_447_n_6\,
      I3 => \caracter_reg[0]_i_251_n_7\,
      I4 => \caracter_reg[0]_i_448_n_4\,
      O => \caracter[0]_i_336_n_0\
    );
\caracter[0]_i_339\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => euros3(29),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(27),
      O => \caracter[0]_i_339_n_0\
    );
\caracter[0]_i_34\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \caracter_reg[0]_i_70_n_6\,
      I1 => \caracter_reg[0]_i_70_n_4\,
      O => \caracter[0]_i_34_n_0\
    );
\caracter[0]_i_340\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => euros3(28),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(26),
      O => \caracter[0]_i_340_n_0\
    );
\caracter[0]_i_341\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => euros3(27),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(25),
      O => \caracter[0]_i_341_n_0\
    );
\caracter[0]_i_342\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => euros3(25),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(27),
      O => \caracter[0]_i_342_n_0\
    );
\caracter[0]_i_343\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00870078"
    )
        port map (
      I0 => euros3(27),
      I1 => euros3(29),
      I2 => euros3(30),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => euros3(28),
      O => \caracter[0]_i_343_n_0\
    );
\caracter[0]_i_344\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00870078"
    )
        port map (
      I0 => euros3(26),
      I1 => euros3(28),
      I2 => euros3(29),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => euros3(27),
      O => \caracter[0]_i_344_n_0\
    );
\caracter[0]_i_345\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00870078"
    )
        port map (
      I0 => euros3(25),
      I1 => euros3(27),
      I2 => euros3(28),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => euros3(26),
      O => \caracter[0]_i_345_n_0\
    );
\caracter[0]_i_346\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000E817000017E8"
    )
        port map (
      I0 => euros3(24),
      I1 => euros3(26),
      I2 => euros3(30),
      I3 => euros3(27),
      I4 => \caracter_reg[0]_i_22_n_3\,
      I5 => euros3(25),
      O => \caracter[0]_i_346_n_0\
    );
\caracter[0]_i_347\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => euros3(30),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(28),
      O => \caracter[0]_i_347_n_0\
    );
\caracter[0]_i_348\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(30),
      I1 => \caracter_reg[0]_i_22_n_3\,
      O => euros2(30)
    );
\caracter[0]_i_349\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0708"
    )
        port map (
      I0 => euros3(28),
      I1 => euros3(30),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(29),
      O => \caracter[0]_i_349_n_0\
    );
\caracter[0]_i_35\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"65"
    )
        port map (
      I0 => \caracter_reg[0]_i_70_n_4\,
      I1 => \caracter_reg[0]_i_71_n_7\,
      I2 => \caracter_reg[0]_i_70_n_5\,
      O => \caracter[0]_i_35_n_0\
    );
\caracter[0]_i_351\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(30),
      I1 => \caracter_reg[0]_i_22_n_3\,
      O => \caracter[0]_i_351_n_0\
    );
\caracter[0]_i_352\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(29),
      I1 => \caracter_reg[0]_i_22_n_3\,
      O => \caracter[0]_i_352_n_0\
    );
\caracter[0]_i_353\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EF"
    )
        port map (
      I0 => euros3(27),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(29),
      O => \caracter[0]_i_353_n_0\
    );
\caracter[0]_i_354\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \caracter_reg[0]_i_22_n_3\,
      I1 => euros3(30),
      O => \caracter[0]_i_354_n_0\
    );
\caracter[0]_i_355\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F2FD"
    )
        port map (
      I0 => euros3(30),
      I1 => euros3(28),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(29),
      O => \caracter[0]_i_355_n_0\
    );
\caracter[0]_i_356\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFD2FF2D"
    )
        port map (
      I0 => euros3(29),
      I1 => euros3(27),
      I2 => euros3(30),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => euros3(28),
      O => \caracter[0]_i_356_n_0\
    );
\caracter[0]_i_357\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(30),
      I1 => \caracter_reg[0]_i_22_n_3\,
      O => \caracter[0]_i_357_n_0\
    );
\caracter[0]_i_358\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(29),
      I1 => \caracter_reg[0]_i_22_n_3\,
      O => euros2(29)
    );
\caracter[0]_i_359\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \caracter_reg[0]_i_22_n_3\,
      I1 => euros3(30),
      O => \caracter[0]_i_359_n_0\
    );
\caracter[0]_i_36\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B44B"
    )
        port map (
      I0 => \caracter_reg[0]_i_70_n_4\,
      I1 => \caracter_reg[0]_i_70_n_6\,
      I2 => \caracter_reg[0]_i_71_n_7\,
      I3 => \caracter_reg[0]_i_70_n_5\,
      O => \caracter[0]_i_36_n_0\
    );
\caracter[0]_i_360\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \caracter_reg[0]_i_22_n_3\,
      I1 => euros3(29),
      O => \caracter[0]_i_360_n_0\
    );
\caracter[0]_i_362\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BE282828"
    )
        port map (
      I0 => \caracter_reg[0]_i_376_n_5\,
      I1 => \caracter_reg[0]_i_377_n_4\,
      I2 => \caracter_reg[0]_i_375_n_5\,
      I3 => \caracter_reg[0]_i_375_n_6\,
      I4 => \caracter_reg[0]_i_377_n_5\,
      O => \caracter[0]_i_362_n_0\
    );
\caracter[0]_i_363\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BE282828"
    )
        port map (
      I0 => \caracter_reg[0]_i_376_n_6\,
      I1 => \caracter_reg[0]_i_377_n_5\,
      I2 => \caracter_reg[0]_i_375_n_6\,
      I3 => euros2(0),
      I4 => \caracter_reg[0]_i_377_n_6\,
      O => \caracter[0]_i_363_n_0\
    );
\caracter[0]_i_364\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"28"
    )
        port map (
      I0 => \caracter_reg[0]_i_376_n_7\,
      I1 => \caracter_reg[0]_i_377_n_6\,
      I2 => euros2(0),
      O => \caracter[0]_i_364_n_0\
    );
\caracter[0]_i_365\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[0]_i_484_n_4\,
      I1 => \caracter_reg[0]_i_377_n_7\,
      O => \caracter[0]_i_365_n_0\
    );
\caracter[0]_i_366\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => \caracter[0]_i_362_n_0\,
      I1 => \caracter_reg[0]_i_376_n_4\,
      I2 => \caracter[0]_i_485_n_0\,
      I3 => \caracter_reg[0]_i_375_n_5\,
      I4 => \caracter_reg[0]_i_377_n_4\,
      O => \caracter[0]_i_366_n_0\
    );
\caracter[0]_i_367\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669966996"
    )
        port map (
      I0 => \caracter[0]_i_363_n_0\,
      I1 => \caracter_reg[0]_i_376_n_5\,
      I2 => \caracter_reg[0]_i_377_n_4\,
      I3 => \caracter_reg[0]_i_375_n_5\,
      I4 => \caracter_reg[0]_i_375_n_6\,
      I5 => \caracter_reg[0]_i_377_n_5\,
      O => \caracter[0]_i_367_n_0\
    );
\caracter[0]_i_368\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669966996"
    )
        port map (
      I0 => \caracter[0]_i_364_n_0\,
      I1 => \caracter_reg[0]_i_376_n_6\,
      I2 => \caracter_reg[0]_i_377_n_5\,
      I3 => \caracter_reg[0]_i_375_n_6\,
      I4 => euros2(0),
      I5 => \caracter_reg[0]_i_377_n_6\,
      O => \caracter[0]_i_368_n_0\
    );
\caracter[0]_i_369\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \caracter_reg[0]_i_376_n_7\,
      I1 => \caracter_reg[0]_i_377_n_6\,
      I2 => euros2(0),
      I3 => \caracter[0]_i_365_n_0\,
      O => \caracter[0]_i_369_n_0\
    );
\caracter[0]_i_370\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_56_n_5\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(2),
      O => euros2(2)
    );
\caracter[0]_i_371\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_265_n_5\,
      I1 => \caracter_reg[0]_i_267_n_4\,
      I2 => \caracter_reg[0]_i_262_n_7\,
      O => \caracter[0]_i_371_n_0\
    );
\caracter[0]_i_372\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => \caracter_reg[0]_i_267_n_5\,
      I1 => \caracter_reg[0]_i_56_n_5\,
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(2),
      I4 => \caracter_reg[0]_i_265_n_6\,
      O => \caracter[0]_i_372_n_0\
    );
\caracter[0]_i_373\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_56_n_6\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(1),
      O => euros2(1)
    );
\caracter[0]_i_374\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => \caracter_reg[0]_i_267_n_6\,
      I1 => \caracter_reg[0]_i_56_n_6\,
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(1),
      I4 => \caracter_reg[0]_i_265_n_7\,
      O => \caracter[0]_i_374_n_0\
    );
\caracter[0]_i_378\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E2"
    )
        port map (
      I0 => euros3(6),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => \caracter_reg[0]_i_32_n_5\,
      O => euros2(6)
    );
\caracter[0]_i_379\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_32_n_6\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(5),
      O => \caracter[0]_i_379_n_0\
    );
\caracter[0]_i_38\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_81_n_4\,
      I1 => \caracter_reg[0]_i_82_n_5\,
      I2 => \caracter_reg[0]_i_83_n_6\,
      I3 => \caracter_reg[0]_i_84_n_5\,
      I4 => \caracter[0]_i_85_n_0\,
      O => \caracter[0]_i_38_n_0\
    );
\caracter[0]_i_380\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_32_n_7\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(4),
      O => \caracter[0]_i_380_n_0\
    );
\caracter[0]_i_381\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9999A55A6666A55A"
    )
        port map (
      I0 => euros2(0),
      I1 => \caracter_reg[0]_i_56_n_5\,
      I2 => euros3(2),
      I3 => euros3(6),
      I4 => \caracter_reg[0]_i_22_n_3\,
      I5 => \caracter_reg[0]_i_32_n_5\,
      O => \caracter[0]_i_381_n_0\
    );
\caracter[0]_i_382\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"335ACC5A"
    )
        port map (
      I0 => euros3(5),
      I1 => \caracter_reg[0]_i_32_n_6\,
      I2 => euros3(1),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_56_n_6\,
      O => \caracter[0]_i_382_n_0\
    );
\caracter[0]_i_383\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1DE2"
    )
        port map (
      I0 => euros3(4),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => \caracter_reg[0]_i_32_n_7\,
      I3 => euros2(0),
      O => \caracter[0]_i_383_n_0\
    );
\caracter[0]_i_384\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_56_n_4\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(3),
      O => \caracter[0]_i_384_n_0\
    );
\caracter[0]_i_385\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_32_n_7\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(4),
      O => \caracter[0]_i_385_n_0\
    );
\caracter[0]_i_386\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_56_n_4\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(3),
      O => \caracter[0]_i_386_n_0\
    );
\caracter[0]_i_387\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_56_n_5\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(2),
      O => \caracter[0]_i_387_n_0\
    );
\caracter[0]_i_388\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_56_n_6\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(1),
      O => \caracter[0]_i_388_n_0\
    );
\caracter[0]_i_389\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCA533A5"
    )
        port map (
      I0 => euros3(4),
      I1 => \caracter_reg[0]_i_32_n_7\,
      I2 => euros3(7),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_32_n_4\,
      O => \caracter[0]_i_389_n_0\
    );
\caracter[0]_i_39\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \caracter_reg[0]_i_84_n_6\,
      I1 => \caracter[0]_i_86_n_0\,
      I2 => \caracter_reg[0]_i_81_n_5\,
      I3 => \caracter_reg[0]_i_82_n_6\,
      I4 => \caracter_reg[0]_i_83_n_7\,
      O => \caracter[0]_i_39_n_0\
    );
\caracter[0]_i_390\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCA533A5"
    )
        port map (
      I0 => euros3(3),
      I1 => \caracter_reg[0]_i_56_n_4\,
      I2 => euros3(6),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_32_n_5\,
      O => \caracter[0]_i_390_n_0\
    );
\caracter[0]_i_391\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCA533A5"
    )
        port map (
      I0 => euros3(2),
      I1 => \caracter_reg[0]_i_56_n_5\,
      I2 => euros3(5),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_32_n_6\,
      O => \caracter[0]_i_391_n_0\
    );
\caracter[0]_i_392\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCA533A5"
    )
        port map (
      I0 => euros3(1),
      I1 => \caracter_reg[0]_i_56_n_6\,
      I2 => euros3(4),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_32_n_7\,
      O => \caracter[0]_i_392_n_0\
    );
\caracter[0]_i_393\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F2FB"
    )
        port map (
      I0 => euros3(6),
      I1 => euros3(8),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(10),
      O => \caracter[0]_i_393_n_0\
    );
\caracter[0]_i_394\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFFF4777CCCF4447"
    )
        port map (
      I0 => \caracter_reg[0]_i_32_n_4\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(9),
      I3 => euros3(7),
      I4 => \caracter_reg[0]_i_32_n_6\,
      I5 => euros3(5),
      O => \caracter[0]_i_394_n_0\
    );
\caracter[0]_i_395\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFFF4777CCCF4447"
    )
        port map (
      I0 => \caracter_reg[0]_i_32_n_5\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(8),
      I3 => euros3(6),
      I4 => \caracter_reg[0]_i_32_n_7\,
      I5 => euros3(4),
      O => \caracter[0]_i_395_n_0\
    );
\caracter[0]_i_396\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"57F7075751F10151"
    )
        port map (
      I0 => euros2(5),
      I1 => euros3(7),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => \caracter_reg[0]_i_32_n_4\,
      I4 => \caracter_reg[0]_i_56_n_4\,
      I5 => euros3(3),
      O => \caracter[0]_i_396_n_0\
    );
\caracter[0]_i_397\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5659A6A95956A9A6"
    )
        port map (
      I0 => \caracter[0]_i_393_n_0\,
      I1 => euros3(11),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(7),
      I4 => \caracter_reg[0]_i_32_n_4\,
      I5 => euros3(9),
      O => \caracter[0]_i_397_n_0\
    );
\caracter[0]_i_398\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5A665A995A995A66"
    )
        port map (
      I0 => \caracter[0]_i_394_n_0\,
      I1 => euros3(6),
      I2 => \caracter_reg[0]_i_32_n_5\,
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => euros3(8),
      I5 => euros3(10),
      O => \caracter[0]_i_398_n_0\
    );
\caracter[0]_i_399\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9969666999966696"
    )
        port map (
      I0 => \caracter[0]_i_395_n_0\,
      I1 => euros2(5),
      I2 => euros3(7),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_32_n_4\,
      I5 => euros3(9),
      O => \caracter[0]_i_399_n_0\
    );
\caracter[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAA888A08880000"
    )
        port map (
      I0 => \caracter_reg[0]\,
      I1 => \caracter[1]_i_18_n_0\,
      I2 => \caracter[0]_i_10_n_0\,
      I3 => \caracter[0]_i_11_n_0\,
      I4 => \caracter[0]_i_12_n_0\,
      I5 => \caracter[3]_i_11_n_0\,
      O => \charact_dot_reg[0]\
    );
\caracter[0]_i_40\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_81_n_6\,
      I1 => \caracter_reg[0]_i_82_n_7\,
      I2 => \caracter_reg[0]_i_87_n_4\,
      I3 => \caracter_reg[0]_i_84_n_7\,
      I4 => \caracter[0]_i_88_n_0\,
      O => \caracter[0]_i_40_n_0\
    );
\caracter[0]_i_400\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A65659A9A95956A6"
    )
        port map (
      I0 => \caracter[0]_i_396_n_0\,
      I1 => euros3(6),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => \caracter_reg[0]_i_32_n_5\,
      I4 => euros2(4),
      I5 => euros3(8),
      O => \caracter[0]_i_400_n_0\
    );
\caracter[0]_i_401\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(17),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(19),
      I3 => euros3(12),
      O => \caracter[0]_i_401_n_0\
    );
\caracter[0]_i_402\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(16),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(18),
      I3 => euros3(11),
      O => \caracter[0]_i_402_n_0\
    );
\caracter[0]_i_403\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(15),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(17),
      I3 => euros3(10),
      O => \caracter[0]_i_403_n_0\
    );
\caracter[0]_i_404\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(14),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(16),
      I3 => euros3(9),
      O => \caracter[0]_i_404_n_0\
    );
\caracter[0]_i_405\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(18),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(20),
      I3 => euros3(13),
      I4 => \caracter[0]_i_401_n_0\,
      O => \caracter[0]_i_405_n_0\
    );
\caracter[0]_i_406\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(17),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(19),
      I3 => euros3(12),
      I4 => \caracter[0]_i_402_n_0\,
      O => \caracter[0]_i_406_n_0\
    );
\caracter[0]_i_407\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(16),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(18),
      I3 => euros3(11),
      I4 => \caracter[0]_i_403_n_0\,
      O => \caracter[0]_i_407_n_0\
    );
\caracter[0]_i_408\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(15),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(17),
      I3 => euros3(10),
      I4 => \caracter[0]_i_404_n_0\,
      O => \caracter[0]_i_408_n_0\
    );
\caracter[0]_i_409\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \caracter_reg[0]_i_32_n_4\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(7),
      O => \caracter[0]_i_409_n_0\
    );
\caracter[0]_i_41\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \caracter_reg[0]_i_89_n_4\,
      I1 => \caracter[0]_i_90_n_0\,
      I2 => \caracter_reg[0]_i_81_n_7\,
      I3 => \caracter_reg[0]_i_91_n_4\,
      I4 => \caracter_reg[0]_i_87_n_5\,
      O => \caracter[0]_i_41_n_0\
    );
\caracter[0]_i_410\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_32_n_7\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(4),
      O => euros2(4)
    );
\caracter[0]_i_411\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_56_n_4\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(3),
      O => euros2(3)
    );
\caracter[0]_i_412\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[0]_i_32_n_4\,
      O => \caracter[0]_i_412_n_0\
    );
\caracter[0]_i_413\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[0]_i_32_n_5\,
      O => \caracter[0]_i_413_n_0\
    );
\caracter[0]_i_414\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[0]_i_32_n_6\,
      O => \caracter[0]_i_414_n_0\
    );
\caracter[0]_i_415\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => euros2(0),
      O => \caracter[0]_i_415_n_0\
    );
\caracter[0]_i_416\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[0]_i_32_n_7\,
      O => \caracter[0]_i_416_n_0\
    );
\caracter[0]_i_417\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[0]_i_56_n_4\,
      O => \caracter[0]_i_417_n_0\
    );
\caracter[0]_i_418\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[0]_i_56_n_5\,
      O => \caracter[0]_i_418_n_0\
    );
\caracter[0]_i_419\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[0]_i_56_n_6\,
      O => \caracter[0]_i_419_n_0\
    );
\caracter[0]_i_42\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \caracter[0]_i_38_n_0\,
      I1 => \caracter[0]_i_92_n_0\,
      I2 => \caracter_reg[0]_i_83_n_4\,
      I3 => \caracter_reg[0]_i_93_n_7\,
      I4 => \caracter_reg[0]_i_94_n_6\,
      I5 => \caracter_reg[0]_i_84_n_4\,
      O => \caracter[0]_i_42_n_0\
    );
\caracter[0]_i_421\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_319_n_4\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(12),
      O => \caracter[0]_i_421_n_0\
    );
\caracter[0]_i_422\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_319_n_5\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(11),
      O => \caracter[0]_i_422_n_0\
    );
\caracter[0]_i_423\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_319_n_6\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(10),
      O => \caracter[0]_i_423_n_0\
    );
\caracter[0]_i_424\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_319_n_7\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(9),
      O => \caracter[0]_i_424_n_0\
    );
\caracter[0]_i_425\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(12),
      I1 => \caracter_reg[0]_i_319_n_4\,
      I2 => euros3(13),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_231_n_7\,
      O => \caracter[0]_i_425_n_0\
    );
\caracter[0]_i_426\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(11),
      I1 => \caracter_reg[0]_i_319_n_5\,
      I2 => euros3(12),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_319_n_4\,
      O => \caracter[0]_i_426_n_0\
    );
\caracter[0]_i_427\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(10),
      I1 => \caracter_reg[0]_i_319_n_6\,
      I2 => euros3(11),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_319_n_5\,
      O => \caracter[0]_i_427_n_0\
    );
\caracter[0]_i_428\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(9),
      I1 => \caracter_reg[0]_i_319_n_7\,
      I2 => euros3(10),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_319_n_6\,
      O => \caracter[0]_i_428_n_0\
    );
\caracter[0]_i_43\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \caracter[0]_i_39_n_0\,
      I1 => \caracter[0]_i_95_n_0\,
      I2 => \caracter_reg[0]_i_83_n_5\,
      I3 => \caracter_reg[0]_i_82_n_4\,
      I4 => \caracter_reg[0]_i_94_n_7\,
      I5 => \caracter_reg[0]_i_84_n_5\,
      O => \caracter[0]_i_43_n_0\
    );
\caracter[0]_i_430\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_438_n_7\,
      I1 => \caracter_reg[0]_i_438_n_5\,
      I2 => \caracter_reg[0]_i_328_n_6\,
      O => \caracter[0]_i_430_n_0\
    );
\caracter[0]_i_431\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_24_n_4\,
      I1 => \caracter_reg[0]_i_438_n_6\,
      I2 => \caracter_reg[0]_i_328_n_7\,
      O => \caracter[0]_i_431_n_0\
    );
\caracter[0]_i_432\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_24_n_5\,
      I1 => \caracter_reg[0]_i_438_n_7\,
      I2 => \caracter_reg[0]_i_438_n_4\,
      O => \caracter[0]_i_432_n_0\
    );
\caracter[0]_i_433\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_24_n_6\,
      I1 => \caracter_reg[0]_i_24_n_4\,
      I2 => \caracter_reg[0]_i_438_n_5\,
      O => \caracter[0]_i_433_n_0\
    );
\caracter[0]_i_434\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_438_n_6\,
      I1 => \caracter_reg[0]_i_438_n_4\,
      I2 => \caracter_reg[0]_i_328_n_5\,
      I3 => \caracter[0]_i_430_n_0\,
      O => \caracter[0]_i_434_n_0\
    );
\caracter[0]_i_435\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_438_n_7\,
      I1 => \caracter_reg[0]_i_438_n_5\,
      I2 => \caracter_reg[0]_i_328_n_6\,
      I3 => \caracter[0]_i_431_n_0\,
      O => \caracter[0]_i_435_n_0\
    );
\caracter[0]_i_436\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_24_n_4\,
      I1 => \caracter_reg[0]_i_438_n_6\,
      I2 => \caracter_reg[0]_i_328_n_7\,
      I3 => \caracter[0]_i_432_n_0\,
      O => \caracter[0]_i_436_n_0\
    );
\caracter[0]_i_437\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_24_n_5\,
      I1 => \caracter_reg[0]_i_438_n_7\,
      I2 => \caracter_reg[0]_i_438_n_4\,
      I3 => \caracter[0]_i_433_n_0\,
      O => \caracter[0]_i_437_n_0\
    );
\caracter[0]_i_439\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E80000E800E8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_448_n_6\,
      I1 => \caracter_reg[0]_i_350_n_5\,
      I2 => \caracter_reg[0]_i_530_n_4\,
      I3 => \caracter_reg[0]_i_350_n_4\,
      I4 => \caracter_reg[0]_i_447_n_7\,
      I5 => \caracter_reg[0]_i_448_n_5\,
      O => \caracter[0]_i_439_n_0\
    );
\caracter[0]_i_44\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \caracter[0]_i_40_n_0\,
      I1 => \caracter_reg[0]_i_84_n_6\,
      I2 => \caracter[0]_i_86_n_0\,
      I3 => \caracter_reg[0]_i_81_n_5\,
      I4 => \caracter_reg[0]_i_82_n_6\,
      I5 => \caracter_reg[0]_i_83_n_7\,
      O => \caracter[0]_i_44_n_0\
    );
\caracter[0]_i_440\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_530_n_5\,
      I1 => \caracter_reg[0]_i_350_n_6\,
      I2 => \caracter_reg[0]_i_448_n_7\,
      I3 => \caracter_reg[0]_i_531_n_2\,
      I4 => \caracter[0]_i_532_n_0\,
      O => \caracter[0]_i_440_n_0\
    );
\caracter[0]_i_441\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_530_n_6\,
      I1 => \caracter_reg[0]_i_350_n_7\,
      I2 => \caracter_reg[0]_i_533_n_4\,
      I3 => \caracter_reg[0]_i_531_n_7\,
      I4 => \caracter[0]_i_534_n_0\,
      O => \caracter[0]_i_441_n_0\
    );
\caracter[0]_i_442\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \caracter_reg[0]_i_535_n_4\,
      I1 => \caracter[0]_i_536_n_0\,
      I2 => \caracter_reg[0]_i_530_n_7\,
      I3 => \caracter_reg[0]_i_93_n_4\,
      I4 => \caracter_reg[0]_i_533_n_5\,
      O => \caracter[0]_i_442_n_0\
    );
\caracter[0]_i_443\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66696999"
    )
        port map (
      I0 => \caracter[0]_i_439_n_0\,
      I1 => \caracter[0]_i_537_n_0\,
      I2 => \caracter_reg[0]_i_447_n_7\,
      I3 => \caracter_reg[0]_i_350_n_4\,
      I4 => \caracter_reg[0]_i_448_n_5\,
      O => \caracter[0]_i_443_n_0\
    );
\caracter[0]_i_444\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66696999"
    )
        port map (
      I0 => \caracter[0]_i_440_n_0\,
      I1 => \caracter[0]_i_538_n_0\,
      I2 => \caracter_reg[0]_i_530_n_4\,
      I3 => \caracter_reg[0]_i_350_n_5\,
      I4 => \caracter_reg[0]_i_448_n_6\,
      O => \caracter[0]_i_444_n_0\
    );
\caracter[0]_i_445\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \caracter[0]_i_441_n_0\,
      I1 => \caracter[0]_i_539_n_0\,
      I2 => \caracter_reg[0]_i_448_n_6\,
      I3 => \caracter_reg[0]_i_350_n_5\,
      I4 => \caracter_reg[0]_i_530_n_4\,
      I5 => \caracter_reg[0]_i_531_n_2\,
      O => \caracter[0]_i_445_n_0\
    );
\caracter[0]_i_446\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \caracter[0]_i_442_n_0\,
      I1 => \caracter[0]_i_540_n_0\,
      I2 => \caracter_reg[0]_i_448_n_7\,
      I3 => \caracter_reg[0]_i_350_n_6\,
      I4 => \caracter_reg[0]_i_530_n_5\,
      I5 => \caracter_reg[0]_i_531_n_7\,
      O => \caracter[0]_i_446_n_0\
    );
\caracter[0]_i_449\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[0]_i_337_n_7\,
      I1 => \caracter_reg[0]_i_251_n_0\,
      I2 => \caracter_reg[0]_i_338_n_5\,
      O => \caracter[0]_i_449_n_0\
    );
\caracter[0]_i_45\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \caracter[0]_i_41_n_0\,
      I1 => \caracter[0]_i_96_n_0\,
      I2 => \caracter_reg[0]_i_83_n_7\,
      I3 => \caracter_reg[0]_i_82_n_6\,
      I4 => \caracter_reg[0]_i_81_n_5\,
      I5 => \caracter_reg[0]_i_84_n_7\,
      O => \caracter[0]_i_45_n_0\
    );
\caracter[0]_i_450\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[0]_i_338_n_6\,
      I1 => \caracter_reg[0]_i_447_n_4\,
      I2 => \caracter_reg[0]_i_251_n_5\,
      O => \caracter[0]_i_450_n_0\
    );
\caracter[0]_i_451\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[0]_i_338_n_7\,
      I1 => \caracter_reg[0]_i_447_n_5\,
      I2 => \caracter_reg[0]_i_251_n_6\,
      O => \caracter[0]_i_451_n_0\
    );
\caracter[0]_i_452\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => euros3(29),
      I1 => euros3(25),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(23),
      O => \caracter[0]_i_452_n_0\
    );
\caracter[0]_i_453\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => euros3(28),
      I1 => euros3(24),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(22),
      O => \caracter[0]_i_453_n_0\
    );
\caracter[0]_i_454\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => euros3(27),
      I1 => euros3(23),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(21),
      O => \caracter[0]_i_454_n_0\
    );
\caracter[0]_i_455\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => euros3(26),
      I1 => euros3(22),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(20),
      O => \caracter[0]_i_455_n_0\
    );
\caracter[0]_i_456\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A6A9A9A6"
    )
        port map (
      I0 => \caracter[0]_i_452_n_0\,
      I1 => euros3(30),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(24),
      I4 => euros3(26),
      O => \caracter[0]_i_456_n_0\
    );
\caracter[0]_i_457\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F6F90906"
    )
        port map (
      I0 => euros3(29),
      I1 => euros3(25),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(23),
      I4 => \caracter[0]_i_453_n_0\,
      O => \caracter[0]_i_457_n_0\
    );
\caracter[0]_i_458\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F6F90906"
    )
        port map (
      I0 => euros3(28),
      I1 => euros3(24),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(22),
      I4 => \caracter[0]_i_454_n_0\,
      O => \caracter[0]_i_458_n_0\
    );
\caracter[0]_i_459\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F6F90906"
    )
        port map (
      I0 => euros3(27),
      I1 => euros3(23),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(21),
      I4 => \caracter[0]_i_455_n_0\,
      O => \caracter[0]_i_459_n_0\
    );
\caracter[0]_i_460\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(28),
      I1 => \caracter_reg[0]_i_22_n_3\,
      O => euros2(28)
    );
\caracter[0]_i_461\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(27),
      I1 => \caracter_reg[0]_i_22_n_3\,
      O => euros2(27)
    );
\caracter[0]_i_462\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(26),
      I1 => \caracter_reg[0]_i_22_n_3\,
      O => euros2(26)
    );
\caracter[0]_i_463\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(25),
      I1 => \caracter_reg[0]_i_22_n_3\,
      O => euros2(25)
    );
\caracter[0]_i_464\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \caracter_reg[0]_i_22_n_3\,
      I1 => euros3(28),
      O => \caracter[0]_i_464_n_0\
    );
\caracter[0]_i_465\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(27),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(30),
      O => \caracter[0]_i_465_n_0\
    );
\caracter[0]_i_466\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(26),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(29),
      O => \caracter[0]_i_466_n_0\
    );
\caracter[0]_i_467\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(25),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(28),
      O => \caracter[0]_i_467_n_0\
    );
\caracter[0]_i_468\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFCD"
    )
        port map (
      I0 => euros3(30),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(28),
      I3 => euros3(26),
      O => \caracter[0]_i_468_n_0\
    );
\caracter[0]_i_469\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFCD"
    )
        port map (
      I0 => euros3(29),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(27),
      I3 => euros3(25),
      O => \caracter[0]_i_469_n_0\
    );
\caracter[0]_i_47\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_33_n_4\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(28),
      O => \caracter[0]_i_47_n_0\
    );
\caracter[0]_i_470\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(28),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(24),
      I3 => euros3(26),
      O => \caracter[0]_i_470_n_0\
    );
\caracter[0]_i_471\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => euros3(27),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(23),
      I3 => euros3(25),
      O => \caracter[0]_i_471_n_0\
    );
\caracter[0]_i_472\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF2BD4FFFFD42B"
    )
        port map (
      I0 => euros3(26),
      I1 => euros3(28),
      I2 => euros3(30),
      I3 => euros3(29),
      I4 => \caracter_reg[0]_i_22_n_3\,
      I5 => euros3(27),
      O => \caracter[0]_i_472_n_0\
    );
\caracter[0]_i_473\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A6A9A9A6"
    )
        port map (
      I0 => \caracter[0]_i_469_n_0\,
      I1 => euros3(30),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(26),
      I4 => euros3(28),
      O => \caracter[0]_i_473_n_0\
    );
\caracter[0]_i_474\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(29),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(27),
      I3 => euros3(25),
      I4 => \caracter[0]_i_470_n_0\,
      O => \caracter[0]_i_474_n_0\
    );
\caracter[0]_i_475\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(28),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(24),
      I3 => euros3(26),
      I4 => \caracter[0]_i_471_n_0\,
      O => \caracter[0]_i_475_n_0\
    );
\caracter[0]_i_476\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[0]_i_484_n_5\,
      I1 => \caracter_reg[0]_i_497_n_4\,
      O => \caracter[0]_i_476_n_0\
    );
\caracter[0]_i_477\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[0]_i_484_n_6\,
      I1 => \caracter_reg[0]_i_497_n_5\,
      O => \caracter[0]_i_477_n_0\
    );
\caracter[0]_i_478\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[0]_i_484_n_7\,
      I1 => \caracter_reg[0]_i_497_n_6\,
      O => \caracter[0]_i_478_n_0\
    );
\caracter[0]_i_479\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[0]_i_497_n_7\,
      I1 => \caracter_reg[0]_i_557_n_4\,
      O => \caracter[0]_i_479_n_0\
    );
\caracter[0]_i_48\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_33_n_5\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(27),
      O => \caracter[0]_i_48_n_0\
    );
\caracter[0]_i_480\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => \caracter_reg[0]_i_484_n_4\,
      I1 => \caracter_reg[0]_i_377_n_7\,
      I2 => \caracter_reg[0]_i_497_n_4\,
      I3 => \caracter_reg[0]_i_484_n_5\,
      O => \caracter[0]_i_480_n_0\
    );
\caracter[0]_i_481\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \caracter_reg[0]_i_497_n_5\,
      I1 => \caracter_reg[0]_i_484_n_6\,
      I2 => \caracter_reg[0]_i_497_n_4\,
      I3 => \caracter_reg[0]_i_484_n_5\,
      O => \caracter[0]_i_481_n_0\
    );
\caracter[0]_i_482\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \caracter_reg[0]_i_497_n_6\,
      I1 => \caracter_reg[0]_i_484_n_7\,
      I2 => \caracter_reg[0]_i_497_n_5\,
      I3 => \caracter_reg[0]_i_484_n_6\,
      O => \caracter[0]_i_482_n_0\
    );
\caracter[0]_i_483\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \caracter_reg[0]_i_557_n_4\,
      I1 => \caracter_reg[0]_i_497_n_7\,
      I2 => \caracter_reg[0]_i_497_n_6\,
      I3 => \caracter_reg[0]_i_484_n_7\,
      O => \caracter[0]_i_483_n_0\
    );
\caracter[0]_i_485\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_267_n_7\,
      I1 => euros2(0),
      I2 => \caracter_reg[0]_i_375_n_4\,
      O => \caracter[0]_i_485_n_0\
    );
\caracter[0]_i_486\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A959"
    )
        port map (
      I0 => euros2(0),
      I1 => euros3(3),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => \caracter_reg[0]_i_56_n_4\,
      O => \caracter[0]_i_486_n_0\
    );
\caracter[0]_i_487\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1D"
    )
        port map (
      I0 => euros3(2),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => \caracter_reg[0]_i_56_n_5\,
      O => \caracter[0]_i_487_n_0\
    );
\caracter[0]_i_488\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1D"
    )
        port map (
      I0 => euros3(1),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => \caracter_reg[0]_i_56_n_6\,
      O => \caracter[0]_i_488_n_0\
    );
\caracter[0]_i_489\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(13),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(15),
      I3 => euros3(8),
      O => \caracter[0]_i_489_n_0\
    );
\caracter[0]_i_49\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_33_n_6\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(26),
      O => \caracter[0]_i_49_n_0\
    );
\caracter[0]_i_490\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(14),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(12),
      I3 => euros3(7),
      O => \caracter[0]_i_490_n_0\
    );
\caracter[0]_i_491\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(13),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(11),
      I3 => euros3(6),
      O => \caracter[0]_i_491_n_0\
    );
\caracter[0]_i_492\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"5440"
    )
        port map (
      I0 => \caracter_reg[0]_i_22_n_3\,
      I1 => euros3(5),
      I2 => euros3(12),
      I3 => euros3(10),
      O => \caracter[0]_i_492_n_0\
    );
\caracter[0]_i_493\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(14),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(16),
      I3 => euros3(9),
      I4 => \caracter[0]_i_489_n_0\,
      O => \caracter[0]_i_493_n_0\
    );
\caracter[0]_i_494\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => euros3(13),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(15),
      I3 => euros3(8),
      I4 => \caracter[0]_i_490_n_0\,
      O => \caracter[0]_i_494_n_0\
    );
\caracter[0]_i_495\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5659A6A95956A9A6"
    )
        port map (
      I0 => \caracter[0]_i_491_n_0\,
      I1 => euros3(12),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(14),
      I4 => \caracter_reg[0]_i_32_n_4\,
      I5 => euros3(7),
      O => \caracter[0]_i_495_n_0\
    );
\caracter[0]_i_496\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5659A6A95956A9A6"
    )
        port map (
      I0 => \caracter[0]_i_492_n_0\,
      I1 => euros3(11),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(13),
      I4 => \caracter_reg[0]_i_32_n_5\,
      I5 => euros3(6),
      O => \caracter[0]_i_496_n_0\
    );
\caracter[0]_i_498\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"335FFF5F00053305"
    )
        port map (
      I0 => euros3(4),
      I1 => \caracter_reg[0]_i_32_n_7\,
      I2 => euros3(6),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_32_n_5\,
      I5 => euros2(2),
      O => \caracter[0]_i_498_n_0\
    );
\caracter[0]_i_499\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"57F7075751F10151"
    )
        port map (
      I0 => euros2(5),
      I1 => euros3(3),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => \caracter_reg[0]_i_56_n_4\,
      I4 => \caracter_reg[0]_i_56_n_6\,
      I5 => euros3(1),
      O => \caracter[0]_i_499_n_0\
    );
\caracter[0]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF08080808080808"
    )
        port map (
      I0 => \caracter_reg[0]_0\,
      I1 => \^disp_dinero[5]_0\(0),
      I2 => Q(0),
      I3 => \caracter_reg[0]_1\,
      I4 => \caracter_reg[0]_2\,
      I5 => \disp_reg[3]\(0),
      O => \reg_reg[0]\
    );
\caracter[0]_i_50\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_33_n_7\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(25),
      O => \caracter[0]_i_50_n_0\
    );
\caracter[0]_i_500\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"47007703CF44FF47"
    )
        port map (
      I0 => \caracter_reg[0]_i_32_n_7\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(4),
      I3 => euros2(0),
      I4 => euros3(2),
      I5 => \caracter_reg[0]_i_56_n_5\,
      O => \caracter[0]_i_500_n_0\
    );
\caracter[0]_i_501\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00053305"
    )
        port map (
      I0 => euros3(1),
      I1 => \caracter_reg[0]_i_56_n_6\,
      I2 => euros3(3),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_56_n_4\,
      O => \caracter[0]_i_501_n_0\
    );
\caracter[0]_i_502\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9996669666699969"
    )
        port map (
      I0 => \caracter[0]_i_498_n_0\,
      I1 => euros2(5),
      I2 => euros3(3),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_56_n_4\,
      I5 => \caracter[0]_i_409_n_0\,
      O => \caracter[0]_i_502_n_0\
    );
\caracter[0]_i_503\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"56A6A959A95956A6"
    )
        port map (
      I0 => \caracter[0]_i_499_n_0\,
      I1 => euros3(6),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => \caracter_reg[0]_i_32_n_5\,
      I4 => euros2(4),
      I5 => euros2(2),
      O => \caracter[0]_i_503_n_0\
    );
\caracter[0]_i_504\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6996696969969696"
    )
        port map (
      I0 => \caracter[0]_i_500_n_0\,
      I1 => euros2(5),
      I2 => euros2(3),
      I3 => \caracter_reg[0]_i_56_n_6\,
      I4 => \caracter_reg[0]_i_22_n_3\,
      I5 => euros3(1),
      O => \caracter[0]_i_504_n_0\
    );
\caracter[0]_i_505\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6966699996999666"
    )
        port map (
      I0 => euros2(0),
      I1 => euros2(2),
      I2 => \caracter_reg[0]_i_32_n_7\,
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => euros3(4),
      I5 => \caracter[0]_i_501_n_0\,
      O => \caracter[0]_i_505_n_0\
    );
\caracter[0]_i_507\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_429_n_4\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(8),
      O => \caracter[0]_i_507_n_0\
    );
\caracter[0]_i_508\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"02A2"
    )
        port map (
      I0 => \caracter_reg[0]_i_429_n_5\,
      I1 => euros3(7),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => \caracter_reg[0]_i_32_n_4\,
      O => \caracter[0]_i_508_n_0\
    );
\caracter[0]_i_509\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"ABFB"
    )
        port map (
      I0 => \caracter_reg[0]_i_429_n_6\,
      I1 => euros3(6),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => \caracter_reg[0]_i_32_n_5\,
      O => \caracter[0]_i_509_n_0\
    );
\caracter[0]_i_51\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(28),
      I1 => \caracter_reg[0]_i_33_n_4\,
      I2 => euros3(29),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_23_n_7\,
      O => \caracter[0]_i_51_n_0\
    );
\caracter[0]_i_510\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"ABFB"
    )
        port map (
      I0 => \caracter_reg[0]_i_429_n_7\,
      I1 => euros3(5),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => \caracter_reg[0]_i_32_n_6\,
      O => \caracter[0]_i_510_n_0\
    );
\caracter[0]_i_511\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(8),
      I1 => \caracter_reg[0]_i_429_n_4\,
      I2 => euros3(9),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_319_n_7\,
      O => \caracter[0]_i_511_n_0\
    );
\caracter[0]_i_512\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5050CF30AFAF30CF"
    )
        port map (
      I0 => \caracter_reg[0]_i_32_n_4\,
      I1 => euros3(7),
      I2 => \caracter_reg[0]_i_429_n_5\,
      I3 => euros3(8),
      I4 => \caracter_reg[0]_i_22_n_3\,
      I5 => \caracter_reg[0]_i_429_n_4\,
      O => \caracter[0]_i_512_n_0\
    );
\caracter[0]_i_513\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF4700B800B8FF47"
    )
        port map (
      I0 => \caracter_reg[0]_i_32_n_5\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(6),
      I3 => \caracter_reg[0]_i_429_n_6\,
      I4 => \caracter[0]_i_409_n_0\,
      I5 => \caracter_reg[0]_i_429_n_5\,
      O => \caracter[0]_i_513_n_0\
    );
\caracter[0]_i_514\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"D2DDD2222D222DDD"
    )
        port map (
      I0 => euros2(5),
      I1 => \caracter_reg[0]_i_429_n_7\,
      I2 => \caracter_reg[0]_i_32_n_5\,
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => euros3(6),
      I5 => \caracter_reg[0]_i_429_n_6\,
      O => \caracter[0]_i_514_n_0\
    );
\caracter[0]_i_515\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_24_n_7\,
      I1 => \caracter_reg[0]_i_24_n_5\,
      I2 => \caracter_reg[0]_i_438_n_6\,
      O => \caracter[0]_i_515_n_0\
    );
\caracter[0]_i_516\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[0]_i_438_n_6\,
      I1 => \caracter_reg[0]_i_24_n_5\,
      I2 => \caracter_reg[0]_i_24_n_7\,
      O => \caracter[0]_i_516_n_0\
    );
\caracter[0]_i_517\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \caracter_reg[0]_i_24_n_4\,
      I1 => \caracter_reg[0]_i_24_n_7\,
      O => \caracter[0]_i_517_n_0\
    );
\caracter[0]_i_518\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_24_n_6\,
      I1 => \caracter_reg[0]_i_24_n_4\,
      I2 => \caracter_reg[0]_i_438_n_5\,
      I3 => \caracter[0]_i_515_n_0\,
      O => \caracter[0]_i_518_n_0\
    );
\caracter[0]_i_519\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69966969"
    )
        port map (
      I0 => \caracter_reg[0]_i_24_n_7\,
      I1 => \caracter_reg[0]_i_24_n_5\,
      I2 => \caracter_reg[0]_i_438_n_6\,
      I3 => \caracter_reg[0]_i_24_n_6\,
      I4 => \caracter_reg[0]_i_438_n_7\,
      O => \caracter[0]_i_519_n_0\
    );
\caracter[0]_i_52\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(27),
      I1 => \caracter_reg[0]_i_33_n_5\,
      I2 => euros3(28),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_33_n_4\,
      O => \caracter[0]_i_52_n_0\
    );
\caracter[0]_i_520\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => \caracter_reg[0]_i_24_n_7\,
      I1 => \caracter_reg[0]_i_24_n_4\,
      I2 => \caracter_reg[0]_i_438_n_7\,
      I3 => \caracter_reg[0]_i_24_n_6\,
      O => \caracter[0]_i_520_n_0\
    );
\caracter[0]_i_521\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter_reg[0]_i_24_n_4\,
      I1 => \caracter_reg[0]_i_24_n_7\,
      O => \caracter[0]_i_521_n_0\
    );
\caracter[0]_i_522\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_94_n_4\,
      I1 => \caracter_reg[0]_i_93_n_5\,
      I2 => \caracter_reg[0]_i_533_n_6\,
      I3 => \caracter_reg[0]_i_535_n_5\,
      I4 => \caracter[0]_i_576_n_0\,
      O => \caracter[0]_i_522_n_0\
    );
\caracter[0]_i_523\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_94_n_5\,
      I1 => \caracter_reg[0]_i_93_n_6\,
      I2 => \caracter_reg[0]_i_533_n_7\,
      I3 => \caracter_reg[0]_i_535_n_6\,
      I4 => \caracter[0]_i_577_n_0\,
      O => \caracter[0]_i_523_n_0\
    );
\caracter[0]_i_524\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_94_n_6\,
      I1 => \caracter_reg[0]_i_93_n_7\,
      I2 => \caracter_reg[0]_i_83_n_4\,
      I3 => \caracter_reg[0]_i_535_n_7\,
      I4 => \caracter[0]_i_578_n_0\,
      O => \caracter[0]_i_524_n_0\
    );
\caracter[0]_i_525\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_94_n_7\,
      I1 => \caracter_reg[0]_i_82_n_4\,
      I2 => \caracter_reg[0]_i_83_n_5\,
      I3 => \caracter_reg[0]_i_84_n_4\,
      I4 => \caracter[0]_i_579_n_0\,
      O => \caracter[0]_i_525_n_0\
    );
\caracter[0]_i_526\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \caracter[0]_i_522_n_0\,
      I1 => \caracter_reg[0]_i_535_n_4\,
      I2 => \caracter[0]_i_536_n_0\,
      I3 => \caracter_reg[0]_i_530_n_7\,
      I4 => \caracter_reg[0]_i_93_n_4\,
      I5 => \caracter_reg[0]_i_533_n_5\,
      O => \caracter[0]_i_526_n_0\
    );
\caracter[0]_i_527\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \caracter[0]_i_523_n_0\,
      I1 => \caracter[0]_i_580_n_0\,
      I2 => \caracter_reg[0]_i_533_n_5\,
      I3 => \caracter_reg[0]_i_93_n_4\,
      I4 => \caracter_reg[0]_i_530_n_7\,
      I5 => \caracter_reg[0]_i_535_n_5\,
      O => \caracter[0]_i_527_n_0\
    );
\caracter[0]_i_528\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \caracter[0]_i_524_n_0\,
      I1 => \caracter[0]_i_581_n_0\,
      I2 => \caracter_reg[0]_i_533_n_6\,
      I3 => \caracter_reg[0]_i_93_n_5\,
      I4 => \caracter_reg[0]_i_94_n_4\,
      I5 => \caracter_reg[0]_i_535_n_6\,
      O => \caracter[0]_i_528_n_0\
    );
\caracter[0]_i_529\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \caracter[0]_i_525_n_0\,
      I1 => \caracter[0]_i_582_n_0\,
      I2 => \caracter_reg[0]_i_533_n_7\,
      I3 => \caracter_reg[0]_i_93_n_6\,
      I4 => \caracter_reg[0]_i_94_n_5\,
      I5 => \caracter_reg[0]_i_535_n_7\,
      O => \caracter[0]_i_529_n_0\
    );
\caracter[0]_i_53\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(26),
      I1 => \caracter_reg[0]_i_33_n_6\,
      I2 => euros3(27),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_33_n_5\,
      O => \caracter[0]_i_53_n_0\
    );
\caracter[0]_i_532\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_448_n_6\,
      I1 => \caracter_reg[0]_i_350_n_5\,
      I2 => \caracter_reg[0]_i_530_n_4\,
      O => \caracter[0]_i_532_n_0\
    );
\caracter[0]_i_534\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_448_n_7\,
      I1 => \caracter_reg[0]_i_350_n_6\,
      I2 => \caracter_reg[0]_i_530_n_5\,
      O => \caracter[0]_i_534_n_0\
    );
\caracter[0]_i_536\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_533_n_4\,
      I1 => \caracter_reg[0]_i_350_n_7\,
      I2 => \caracter_reg[0]_i_530_n_6\,
      O => \caracter[0]_i_536_n_0\
    );
\caracter[0]_i_537\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[0]_i_448_n_4\,
      I1 => \caracter_reg[0]_i_447_n_6\,
      I2 => \caracter_reg[0]_i_251_n_7\,
      O => \caracter[0]_i_537_n_0\
    );
\caracter[0]_i_538\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[0]_i_448_n_5\,
      I1 => \caracter_reg[0]_i_447_n_7\,
      I2 => \caracter_reg[0]_i_350_n_4\,
      O => \caracter[0]_i_538_n_0\
    );
\caracter[0]_i_539\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter_reg[0]_i_530_n_5\,
      I1 => \caracter_reg[0]_i_350_n_6\,
      I2 => \caracter_reg[0]_i_448_n_7\,
      O => \caracter[0]_i_539_n_0\
    );
\caracter[0]_i_54\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => euros3(25),
      I1 => \caracter_reg[0]_i_33_n_7\,
      I2 => euros3(26),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_33_n_6\,
      O => \caracter[0]_i_54_n_0\
    );
\caracter[0]_i_540\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter_reg[0]_i_530_n_6\,
      I1 => \caracter_reg[0]_i_350_n_7\,
      I2 => \caracter_reg[0]_i_533_n_4\,
      O => \caracter[0]_i_540_n_0\
    );
\caracter[0]_i_541\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => euros3(25),
      I1 => euros3(21),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(19),
      O => \caracter[0]_i_541_n_0\
    );
\caracter[0]_i_542\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => euros3(24),
      I1 => euros3(20),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(18),
      O => \caracter[0]_i_542_n_0\
    );
\caracter[0]_i_543\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => euros3(23),
      I1 => euros3(19),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(17),
      O => \caracter[0]_i_543_n_0\
    );
\caracter[0]_i_544\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => euros3(22),
      I1 => euros3(18),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(16),
      O => \caracter[0]_i_544_n_0\
    );
\caracter[0]_i_545\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F6F90906"
    )
        port map (
      I0 => euros3(26),
      I1 => euros3(22),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(20),
      I4 => \caracter[0]_i_541_n_0\,
      O => \caracter[0]_i_545_n_0\
    );
\caracter[0]_i_546\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F6F90906"
    )
        port map (
      I0 => euros3(25),
      I1 => euros3(21),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(19),
      I4 => \caracter[0]_i_542_n_0\,
      O => \caracter[0]_i_546_n_0\
    );
\caracter[0]_i_547\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F6F90906"
    )
        port map (
      I0 => euros3(24),
      I1 => euros3(20),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(18),
      I4 => \caracter[0]_i_543_n_0\,
      O => \caracter[0]_i_547_n_0\
    );
\caracter[0]_i_548\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F6F90906"
    )
        port map (
      I0 => euros3(23),
      I1 => euros3(19),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(17),
      I4 => \caracter[0]_i_544_n_0\,
      O => \caracter[0]_i_548_n_0\
    );
\caracter[0]_i_549\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(24),
      I1 => \caracter_reg[0]_i_22_n_3\,
      O => euros2(24)
    );
\caracter[0]_i_550\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(23),
      I1 => \caracter_reg[0]_i_22_n_3\,
      O => euros2(23)
    );
\caracter[0]_i_551\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(22),
      I1 => \caracter_reg[0]_i_22_n_3\,
      O => euros2(22)
    );
\caracter[0]_i_552\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(21),
      I1 => \caracter_reg[0]_i_22_n_3\,
      O => euros2(21)
    );
\caracter[0]_i_553\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(24),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(27),
      O => \caracter[0]_i_553_n_0\
    );
\caracter[0]_i_554\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(23),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(26),
      O => \caracter[0]_i_554_n_0\
    );
\caracter[0]_i_555\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(22),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(25),
      O => \caracter[0]_i_555_n_0\
    );
\caracter[0]_i_556\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(21),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(24),
      O => \caracter[0]_i_556_n_0\
    );
\caracter[0]_i_558\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"5440"
    )
        port map (
      I0 => \caracter_reg[0]_i_22_n_3\,
      I1 => euros3(4),
      I2 => euros3(11),
      I3 => euros3(9),
      O => \caracter[0]_i_558_n_0\
    );
\caracter[0]_i_559\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(10),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(8),
      I3 => euros3(3),
      O => \caracter[0]_i_559_n_0\
    );
\caracter[0]_i_560\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCA0F0ACAC00A00"
    )
        port map (
      I0 => euros3(7),
      I1 => \caracter_reg[0]_i_32_n_4\,
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(9),
      I4 => \caracter_reg[0]_i_56_n_5\,
      I5 => euros3(2),
      O => \caracter[0]_i_560_n_0\
    );
\caracter[0]_i_561\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAFC00FCAAC000C0"
    )
        port map (
      I0 => \caracter_reg[0]_i_56_n_6\,
      I1 => euros3(1),
      I2 => euros3(8),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_32_n_5\,
      I5 => euros3(6),
      O => \caracter[0]_i_561_n_0\
    );
\caracter[0]_i_562\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5659A6A95956A9A6"
    )
        port map (
      I0 => \caracter[0]_i_558_n_0\,
      I1 => euros3(10),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(12),
      I4 => \caracter_reg[0]_i_32_n_6\,
      I5 => euros3(5),
      O => \caracter[0]_i_562_n_0\
    );
\caracter[0]_i_563\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5659A6A95956A9A6"
    )
        port map (
      I0 => \caracter[0]_i_559_n_0\,
      I1 => euros3(9),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(11),
      I4 => \caracter_reg[0]_i_32_n_7\,
      I5 => euros3(4),
      O => \caracter[0]_i_563_n_0\
    );
\caracter[0]_i_564\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5659A6A95956A9A6"
    )
        port map (
      I0 => \caracter[0]_i_560_n_0\,
      I1 => euros3(8),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(10),
      I4 => \caracter_reg[0]_i_56_n_4\,
      I5 => euros3(3),
      O => \caracter[0]_i_564_n_0\
    );
\caracter[0]_i_565\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9969666999966696"
    )
        port map (
      I0 => \caracter[0]_i_561_n_0\,
      I1 => euros2(2),
      I2 => euros3(7),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_32_n_4\,
      I5 => euros3(9),
      O => \caracter[0]_i_565_n_0\
    );
\caracter[0]_i_566\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"656A959A6A659A95"
    )
        port map (
      I0 => euros2(0),
      I1 => \caracter_reg[0]_i_56_n_6\,
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(1),
      I4 => \caracter_reg[0]_i_56_n_4\,
      I5 => euros3(3),
      O => \caracter[0]_i_566_n_0\
    );
\caracter[0]_i_567\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9A95"
    )
        port map (
      I0 => euros2(0),
      I1 => \caracter_reg[0]_i_56_n_5\,
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(2),
      O => \caracter[0]_i_567_n_0\
    );
\caracter[0]_i_568\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1D"
    )
        port map (
      I0 => euros3(1),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => \caracter_reg[0]_i_56_n_6\,
      O => \caracter[0]_i_568_n_0\
    );
\caracter[0]_i_569\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"02A2"
    )
        port map (
      I0 => \caracter_reg[0]_i_24_n_5\,
      I1 => euros3(4),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => \caracter_reg[0]_i_32_n_7\,
      O => \caracter[0]_i_569_n_0\
    );
\caracter[0]_i_57\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(7),
      O => \caracter[0]_i_57_n_0\
    );
\caracter[0]_i_570\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"02A2"
    )
        port map (
      I0 => \caracter_reg[0]_i_24_n_6\,
      I1 => euros3(3),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => \caracter_reg[0]_i_56_n_4\,
      O => \caracter[0]_i_570_n_0\
    );
\caracter[0]_i_571\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"ABFB"
    )
        port map (
      I0 => \caracter_reg[0]_i_24_n_7\,
      I1 => euros3(2),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => \caracter_reg[0]_i_56_n_5\,
      O => \caracter[0]_i_571_n_0\
    );
\caracter[0]_i_572\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4700B8FFB8FF4700"
    )
        port map (
      I0 => \caracter_reg[0]_i_32_n_7\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(4),
      I3 => \caracter_reg[0]_i_24_n_5\,
      I4 => euros2(5),
      I5 => \caracter_reg[0]_i_429_n_7\,
      O => \caracter[0]_i_572_n_0\
    );
\caracter[0]_i_573\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8FF47004700B8FF"
    )
        port map (
      I0 => \caracter_reg[0]_i_56_n_4\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(3),
      I3 => \caracter_reg[0]_i_24_n_6\,
      I4 => euros2(4),
      I5 => \caracter_reg[0]_i_24_n_5\,
      O => \caracter[0]_i_573_n_0\
    );
\caracter[0]_i_574\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2D222DDDD2DDD222"
    )
        port map (
      I0 => euros2(2),
      I1 => \caracter_reg[0]_i_24_n_7\,
      I2 => \caracter_reg[0]_i_56_n_4\,
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => euros3(3),
      I5 => \caracter_reg[0]_i_24_n_6\,
      O => \caracter[0]_i_574_n_0\
    );
\caracter[0]_i_575\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1DE2"
    )
        port map (
      I0 => euros3(2),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => \caracter_reg[0]_i_56_n_5\,
      I3 => \caracter_reg[0]_i_24_n_7\,
      O => \caracter[0]_i_575_n_0\
    );
\caracter[0]_i_576\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_533_n_5\,
      I1 => \caracter_reg[0]_i_93_n_4\,
      I2 => \caracter_reg[0]_i_530_n_7\,
      O => \caracter[0]_i_576_n_0\
    );
\caracter[0]_i_577\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_533_n_6\,
      I1 => \caracter_reg[0]_i_93_n_5\,
      I2 => \caracter_reg[0]_i_94_n_4\,
      O => \caracter[0]_i_577_n_0\
    );
\caracter[0]_i_578\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_533_n_7\,
      I1 => \caracter_reg[0]_i_93_n_6\,
      I2 => \caracter_reg[0]_i_94_n_5\,
      O => \caracter[0]_i_578_n_0\
    );
\caracter[0]_i_579\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_83_n_4\,
      I1 => \caracter_reg[0]_i_93_n_7\,
      I2 => \caracter_reg[0]_i_94_n_6\,
      O => \caracter[0]_i_579_n_0\
    );
\caracter[0]_i_58\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(6),
      I1 => \caracter[3]_i_46_n_0\,
      O => \caracter[0]_i_58_n_0\
    );
\caracter[0]_i_580\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter_reg[0]_i_94_n_4\,
      I1 => \caracter_reg[0]_i_93_n_5\,
      I2 => \caracter_reg[0]_i_533_n_6\,
      O => \caracter[0]_i_580_n_0\
    );
\caracter[0]_i_581\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter_reg[0]_i_94_n_5\,
      I1 => \caracter_reg[0]_i_93_n_6\,
      I2 => \caracter_reg[0]_i_533_n_7\,
      O => \caracter[0]_i_581_n_0\
    );
\caracter[0]_i_582\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter_reg[0]_i_94_n_6\,
      I1 => \caracter_reg[0]_i_93_n_7\,
      I2 => \caracter_reg[0]_i_83_n_4\,
      O => \caracter[0]_i_582_n_0\
    );
\caracter[0]_i_583\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => euros3(21),
      I1 => euros3(17),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(15),
      O => \caracter[0]_i_583_n_0\
    );
\caracter[0]_i_584\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => euros3(20),
      I1 => euros3(16),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(14),
      O => \caracter[0]_i_584_n_0\
    );
\caracter[0]_i_585\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => euros3(19),
      I1 => euros3(15),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(13),
      O => \caracter[0]_i_585_n_0\
    );
\caracter[0]_i_586\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => euros3(18),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(14),
      I3 => euros3(12),
      O => \caracter[0]_i_586_n_0\
    );
\caracter[0]_i_587\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F6F90906"
    )
        port map (
      I0 => euros3(22),
      I1 => euros3(18),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(16),
      I4 => \caracter[0]_i_583_n_0\,
      O => \caracter[0]_i_587_n_0\
    );
\caracter[0]_i_588\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F6F90906"
    )
        port map (
      I0 => euros3(21),
      I1 => euros3(17),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(15),
      I4 => \caracter[0]_i_584_n_0\,
      O => \caracter[0]_i_588_n_0\
    );
\caracter[0]_i_589\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F6F90906"
    )
        port map (
      I0 => euros3(20),
      I1 => euros3(16),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(14),
      I4 => \caracter[0]_i_585_n_0\,
      O => \caracter[0]_i_589_n_0\
    );
\caracter[0]_i_59\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(5),
      I1 => \caracter[3]_i_47_n_0\,
      O => \caracter[0]_i_59_n_0\
    );
\caracter[0]_i_590\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F6F90906"
    )
        port map (
      I0 => euros3(19),
      I1 => euros3(15),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(13),
      I4 => \caracter[0]_i_586_n_0\,
      O => \caracter[0]_i_590_n_0\
    );
\caracter[0]_i_591\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(30),
      I1 => \caracter_reg[0]_i_22_n_3\,
      O => \caracter[0]_i_591_n_0\
    );
\caracter[0]_i_592\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(20),
      I1 => \caracter_reg[0]_i_22_n_3\,
      O => euros2(20)
    );
\caracter[0]_i_593\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(19),
      I1 => \caracter_reg[0]_i_22_n_3\,
      O => euros2(19)
    );
\caracter[0]_i_594\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(18),
      I1 => \caracter_reg[0]_i_22_n_3\,
      O => euros2(18)
    );
\caracter[0]_i_595\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(17),
      I1 => \caracter_reg[0]_i_22_n_3\,
      O => euros2(17)
    );
\caracter[0]_i_596\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(20),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(23),
      O => \caracter[0]_i_596_n_0\
    );
\caracter[0]_i_597\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(19),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(22),
      O => \caracter[0]_i_597_n_0\
    );
\caracter[0]_i_598\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(18),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(21),
      O => \caracter[0]_i_598_n_0\
    );
\caracter[0]_i_599\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => euros3(17),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(20),
      O => \caracter[0]_i_599_n_0\
    );
\caracter[0]_i_60\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AA55A956"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(4),
      I1 => \caracter_reg[1]_i_40_n_4\,
      I2 => \caracter_reg[1]_i_40_n_5\,
      I3 => \caracter_reg[1]_i_60_n_7\,
      I4 => \caracter[1]_i_42_n_0\,
      O => \caracter[0]_i_60_n_0\
    );
\caracter[0]_i_600\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(26),
      I1 => \caracter_reg[0]_i_22_n_3\,
      O => \caracter[0]_i_600_n_0\
    );
\caracter[0]_i_601\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(29),
      I1 => \caracter_reg[0]_i_22_n_3\,
      O => \caracter[0]_i_601_n_0\
    );
\caracter[0]_i_602\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(28),
      I1 => \caracter_reg[0]_i_22_n_3\,
      O => \caracter[0]_i_602_n_0\
    );
\caracter[0]_i_603\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => euros3(27),
      I1 => \caracter_reg[0]_i_22_n_3\,
      O => \caracter[0]_i_603_n_0\
    );
\caracter[0]_i_604\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0708"
    )
        port map (
      I0 => euros3(25),
      I1 => euros3(30),
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(26),
      O => \caracter[0]_i_604_n_0\
    );
\caracter[0]_i_606\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A35CAC5353AC5CA"
    )
        port map (
      I0 => euros3(8),
      I1 => \caracter_reg[0]_i_56_n_6\,
      I2 => \caracter_reg[0]_i_22_n_3\,
      I3 => euros3(1),
      I4 => \caracter_reg[0]_i_32_n_5\,
      I5 => euros3(6),
      O => \caracter[0]_i_606_n_0\
    );
\caracter[0]_i_607\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E2"
    )
        port map (
      I0 => euros3(7),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => \caracter_reg[0]_i_32_n_4\,
      O => euros2(7)
    );
\caracter[0]_i_608\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E2"
    )
        port map (
      I0 => euros3(6),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => \caracter_reg[0]_i_32_n_5\,
      O => \caracter[0]_i_608_n_0\
    );
\caracter[0]_i_609\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_32_n_6\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(5),
      O => \caracter[0]_i_609_n_0\
    );
\caracter[0]_i_610\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6696996999699969"
    )
        port map (
      I0 => \caracter[0]_i_621_n_0\,
      I1 => euros2(1),
      I2 => euros3(8),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => euros2(0),
      I5 => euros2(5),
      O => \caracter[0]_i_610_n_0\
    );
\caracter[0]_i_611\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9999A55A6666A55A"
    )
        port map (
      I0 => euros2(0),
      I1 => \caracter_reg[0]_i_32_n_6\,
      I2 => euros3(5),
      I3 => euros3(7),
      I4 => \caracter_reg[0]_i_22_n_3\,
      I5 => \caracter_reg[0]_i_32_n_4\,
      O => \caracter[0]_i_611_n_0\
    );
\caracter[0]_i_612\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"335ACC5A"
    )
        port map (
      I0 => euros3(6),
      I1 => \caracter_reg[0]_i_32_n_5\,
      I2 => euros3(4),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_32_n_7\,
      O => \caracter[0]_i_612_n_0\
    );
\caracter[0]_i_613\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"335ACC5A"
    )
        port map (
      I0 => euros3(5),
      I1 => \caracter_reg[0]_i_32_n_6\,
      I2 => euros3(3),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_56_n_4\,
      O => \caracter[0]_i_613_n_0\
    );
\caracter[0]_i_614\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_32_n_7\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(4),
      O => \caracter[0]_i_614_n_0\
    );
\caracter[0]_i_615\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_56_n_4\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(3),
      O => \caracter[0]_i_615_n_0\
    );
\caracter[0]_i_616\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_56_n_5\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(2),
      O => \caracter[0]_i_616_n_0\
    );
\caracter[0]_i_617\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"335ACC5A"
    )
        port map (
      I0 => euros3(4),
      I1 => \caracter_reg[0]_i_32_n_7\,
      I2 => euros3(2),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_56_n_5\,
      O => \caracter[0]_i_617_n_0\
    );
\caracter[0]_i_618\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"335ACC5A"
    )
        port map (
      I0 => euros3(3),
      I1 => \caracter_reg[0]_i_56_n_4\,
      I2 => euros3(1),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_56_n_6\,
      O => \caracter[0]_i_618_n_0\
    );
\caracter[0]_i_619\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1DE2"
    )
        port map (
      I0 => euros3(2),
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => \caracter_reg[0]_i_56_n_5\,
      I3 => euros2(0),
      O => \caracter[0]_i_619_n_0\
    );
\caracter[0]_i_62\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \caracter_reg[0]_i_70_n_7\,
      I1 => \caracter_reg[0]_i_70_n_5\,
      O => \caracter[0]_i_62_n_0\
    );
\caracter[0]_i_620\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \caracter_reg[0]_i_56_n_6\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(1),
      O => \caracter[0]_i_620_n_0\
    );
\caracter[0]_i_621\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \caracter_reg[0]_i_32_n_5\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(6),
      O => \caracter[0]_i_621_n_0\
    );
\caracter[0]_i_63\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_120_n_4\,
      I1 => \caracter_reg[0]_i_70_n_6\,
      I2 => \caracter_reg[0]_i_71_n_7\,
      O => \caracter[0]_i_63_n_0\
    );
\caracter[0]_i_64\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_120_n_5\,
      I1 => \caracter_reg[0]_i_70_n_7\,
      I2 => \caracter_reg[0]_i_70_n_4\,
      O => \caracter[0]_i_64_n_0\
    );
\caracter[0]_i_65\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[0]_i_120_n_6\,
      I1 => \caracter_reg[0]_i_120_n_4\,
      I2 => \caracter_reg[0]_i_70_n_5\,
      O => \caracter[0]_i_65_n_0\
    );
\caracter[0]_i_66\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B44B"
    )
        port map (
      I0 => \caracter_reg[0]_i_70_n_5\,
      I1 => \caracter_reg[0]_i_70_n_7\,
      I2 => \caracter_reg[0]_i_70_n_4\,
      I3 => \caracter_reg[0]_i_70_n_6\,
      O => \caracter[0]_i_66_n_0\
    );
\caracter[0]_i_67\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4DB2B24D"
    )
        port map (
      I0 => \caracter_reg[0]_i_71_n_7\,
      I1 => \caracter_reg[0]_i_70_n_6\,
      I2 => \caracter_reg[0]_i_120_n_4\,
      I3 => \caracter_reg[0]_i_70_n_5\,
      I4 => \caracter_reg[0]_i_70_n_7\,
      O => \caracter[0]_i_67_n_0\
    );
\caracter[0]_i_68\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter[0]_i_64_n_0\,
      I1 => \caracter_reg[0]_i_120_n_4\,
      I2 => \caracter_reg[0]_i_70_n_6\,
      I3 => \caracter_reg[0]_i_71_n_7\,
      O => \caracter[0]_i_68_n_0\
    );
\caracter[0]_i_69\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[0]_i_120_n_5\,
      I1 => \caracter_reg[0]_i_70_n_7\,
      I2 => \caracter_reg[0]_i_70_n_4\,
      I3 => \caracter[0]_i_65_n_0\,
      O => \caracter[0]_i_69_n_0\
    );
\caracter[0]_i_73\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_139_n_4\,
      I1 => \caracter_reg[0]_i_91_n_5\,
      I2 => \caracter_reg[0]_i_87_n_6\,
      I3 => \caracter_reg[0]_i_89_n_5\,
      I4 => \caracter[0]_i_140_n_0\,
      O => \caracter[0]_i_73_n_0\
    );
\caracter[0]_i_74\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_139_n_5\,
      I1 => \caracter_reg[0]_i_91_n_6\,
      I2 => \caracter_reg[0]_i_87_n_7\,
      I3 => \caracter[0]_i_141_n_0\,
      I4 => \caracter_reg[0]_i_89_n_6\,
      O => \caracter[0]_i_74_n_0\
    );
\caracter[0]_i_75\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_139_n_6\,
      I1 => \caracter_reg[0]_i_91_n_7\,
      I2 => \caracter_reg[0]_i_142_n_4\,
      I3 => \caracter_reg[0]_i_89_n_7\,
      I4 => \caracter[0]_i_143_n_0\,
      O => \caracter[0]_i_75_n_0\
    );
\caracter[0]_i_76\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \caracter_reg[0]_i_139_n_7\,
      I1 => \caracter_reg[0]_i_144_n_4\,
      I2 => \caracter_reg[0]_i_142_n_5\,
      I3 => \caracter_reg[0]_i_145_n_4\,
      I4 => \caracter[0]_i_146_n_0\,
      O => \caracter[0]_i_76_n_0\
    );
\caracter[0]_i_77\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \caracter[0]_i_73_n_0\,
      I1 => \caracter_reg[0]_i_89_n_4\,
      I2 => \caracter[0]_i_90_n_0\,
      I3 => \caracter_reg[0]_i_81_n_7\,
      I4 => \caracter_reg[0]_i_91_n_4\,
      I5 => \caracter_reg[0]_i_87_n_5\,
      O => \caracter[0]_i_77_n_0\
    );
\caracter[0]_i_78\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \caracter[0]_i_74_n_0\,
      I1 => \caracter[0]_i_147_n_0\,
      I2 => \caracter_reg[0]_i_87_n_5\,
      I3 => \caracter_reg[0]_i_91_n_4\,
      I4 => \caracter_reg[0]_i_81_n_7\,
      I5 => \caracter_reg[0]_i_89_n_5\,
      O => \caracter[0]_i_78_n_0\
    );
\caracter[0]_i_79\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \caracter[0]_i_75_n_0\,
      I1 => \caracter_reg[0]_i_89_n_6\,
      I2 => \caracter[0]_i_141_n_0\,
      I3 => \caracter_reg[0]_i_139_n_5\,
      I4 => \caracter_reg[0]_i_91_n_6\,
      I5 => \caracter_reg[0]_i_87_n_7\,
      O => \caracter[0]_i_79_n_0\
    );
\caracter[0]_i_80\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \caracter[0]_i_76_n_0\,
      I1 => \caracter[0]_i_148_n_0\,
      I2 => \caracter_reg[0]_i_87_n_7\,
      I3 => \caracter_reg[0]_i_91_n_6\,
      I4 => \caracter_reg[0]_i_139_n_5\,
      I5 => \caracter_reg[0]_i_89_n_7\,
      O => \caracter[0]_i_80_n_0\
    );
\caracter[0]_i_85\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_83_n_5\,
      I1 => \caracter_reg[0]_i_82_n_4\,
      I2 => \caracter_reg[0]_i_94_n_7\,
      O => \caracter[0]_i_85_n_0\
    );
\caracter[0]_i_86\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_83_n_6\,
      I1 => \caracter_reg[0]_i_82_n_5\,
      I2 => \caracter_reg[0]_i_81_n_4\,
      O => \caracter[0]_i_86_n_0\
    );
\caracter[0]_i_88\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_83_n_7\,
      I1 => \caracter_reg[0]_i_82_n_6\,
      I2 => \caracter_reg[0]_i_81_n_5\,
      O => \caracter[0]_i_88_n_0\
    );
\caracter[0]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AA8A888800202222"
    )
        port map (
      I0 => \^disp_dinero[5]_0\(1),
      I1 => \caracter_reg[0]_i_20_n_3\,
      I2 => euros3(30),
      I3 => \caracter_reg[0]_i_22_n_3\,
      I4 => \caracter_reg[0]_i_23_n_6\,
      I5 => \caracter_reg[0]_i_24_n_7\,
      O => \^disp_dinero[5]_0\(0)
    );
\caracter[0]_i_90\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[0]_i_87_n_4\,
      I1 => \caracter_reg[0]_i_82_n_7\,
      I2 => \caracter_reg[0]_i_81_n_6\,
      O => \caracter[0]_i_90_n_0\
    );
\caracter[0]_i_92\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter_reg[0]_i_94_n_7\,
      I1 => \caracter_reg[0]_i_82_n_4\,
      I2 => \caracter_reg[0]_i_83_n_5\,
      O => \caracter[0]_i_92_n_0\
    );
\caracter[0]_i_95\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter_reg[0]_i_81_n_4\,
      I1 => \caracter_reg[0]_i_82_n_5\,
      I2 => \caracter_reg[0]_i_83_n_6\,
      O => \caracter[0]_i_95_n_0\
    );
\caracter[0]_i_96\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter_reg[0]_i_81_n_6\,
      I1 => \caracter_reg[0]_i_82_n_7\,
      I2 => \caracter_reg[0]_i_87_n_4\,
      O => \caracter[0]_i_96_n_0\
    );
\caracter[0]_i_98\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_61_n_4\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(24),
      O => \caracter[0]_i_98_n_0\
    );
\caracter[0]_i_99\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_i_61_n_5\,
      I1 => \caracter_reg[0]_i_22_n_3\,
      I2 => euros3(23),
      O => \caracter[0]_i_99_n_0\
    );
\caracter[1]_i_100\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"95A96A566A5695A9"
    )
        port map (
      I0 => \caracter[1]_i_96_n_0\,
      I1 => \caracter_reg[1]_i_102_n_3\,
      I2 => \^total_string_reg[0]\,
      I3 => \caracter_reg[1]_i_161_n_6\,
      I4 => \caracter_reg[1]_i_78_n_1\,
      I5 => \caracter[1]_i_165_n_0\,
      O => \caracter[1]_i_100_n_0\
    );
\caracter[1]_i_101\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[1]_i_75_n_7\,
      I1 => \caracter_reg[1]_i_76_n_3\,
      I2 => \caracter_reg[1]_i_77_n_7\,
      O => \caracter[1]_i_101_n_0\
    );
\caracter[1]_i_105\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[1]_i_35_n_0\,
      I1 => \^total_string_reg[0]\,
      O => \caracter[1]_i_105_n_0\
    );
\caracter[1]_i_106\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[1]_i_77_n_6\,
      I1 => \caracter_reg[1]_i_76_n_3\,
      I2 => \caracter_reg[1]_i_75_n_6\,
      O => \caracter[1]_i_106_n_0\
    );
\caracter[1]_i_107\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[1]_i_104_n_4\,
      I1 => \caracter_reg[1]_i_103_n_4\,
      I2 => \caracter_reg[1]_i_102_n_3\,
      O => \caracter[1]_i_107_n_0\
    );
\caracter[1]_i_108\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[1]_i_104_n_5\,
      I1 => \caracter_reg[1]_i_103_n_5\,
      I2 => \caracter_reg[1]_i_102_n_3\,
      O => \caracter[1]_i_108_n_0\
    );
\caracter[1]_i_109\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \caracter[3]_i_42_n_0\,
      I1 => \caracter[1]_i_41_n_0\,
      O => \caracter[1]_i_109_n_0\
    );
\caracter[1]_i_110\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \caracter[3]_i_43_n_0\,
      I1 => \caracter[1]_i_35_n_0\,
      O => \caracter[1]_i_110_n_0\
    );
\caracter[1]_i_111\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \caracter[1]_i_41_n_0\,
      I1 => \caracter[1]_i_36_n_0\,
      O => \caracter[1]_i_111_n_0\
    );
\caracter[1]_i_112\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter[1]_i_41_n_0\,
      I1 => \caracter[1]_i_36_n_0\,
      O => \caracter[1]_i_112_n_0\
    );
\caracter[1]_i_113\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B4"
    )
        port map (
      I0 => \caracter[1]_i_41_n_0\,
      I1 => \caracter[3]_i_42_n_0\,
      I2 => \caracter[3]_i_43_n_0\,
      O => \caracter[1]_i_113_n_0\
    );
\caracter[1]_i_114\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B44B"
    )
        port map (
      I0 => \caracter[1]_i_35_n_0\,
      I1 => \caracter[3]_i_43_n_0\,
      I2 => \caracter[3]_i_42_n_0\,
      I3 => \caracter[1]_i_41_n_0\,
      O => \caracter[1]_i_114_n_0\
    );
\caracter[1]_i_115\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B44B"
    )
        port map (
      I0 => \caracter[1]_i_36_n_0\,
      I1 => \caracter[1]_i_41_n_0\,
      I2 => \caracter[3]_i_43_n_0\,
      I3 => \caracter[1]_i_35_n_0\,
      O => \caracter[1]_i_115_n_0\
    );
\caracter[1]_i_116\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"D42B2BD4"
    )
        port map (
      I0 => \caracter[1]_i_35_n_0\,
      I1 => \caracter[3]_i_42_n_0\,
      I2 => \caracter[1]_i_37_n_0\,
      I3 => \caracter[1]_i_36_n_0\,
      I4 => \caracter[1]_i_41_n_0\,
      O => \caracter[1]_i_116_n_0\
    );
\caracter[1]_i_117\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => \caracter_reg[1]_i_102_n_3\,
      I1 => \^total_string_reg[0]\,
      I2 => \caracter[1]_i_36_n_0\,
      O => \caracter[1]_i_117_n_0\
    );
\caracter[1]_i_118\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[1]_i_102_n_3\,
      I1 => \caracter[1]_i_36_n_0\,
      I2 => \^total_string_reg[0]\,
      O => \caracter[1]_i_118_n_0\
    );
\caracter[1]_i_119\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \^total_string_reg[0]\,
      I1 => \caracter_reg[1]_i_102_n_3\,
      O => \caracter[1]_i_119_n_0\
    );
\caracter[1]_i_120\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \caracter[1]_i_37_n_0\,
      I1 => \caracter_reg[1]_i_102_n_3\,
      I2 => \caracter[1]_i_35_n_0\,
      I3 => \caracter[1]_i_117_n_0\,
      O => \caracter[1]_i_120_n_0\
    );
\caracter[1]_i_121\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"C396"
    )
        port map (
      I0 => \caracter_reg[1]_i_102_n_3\,
      I1 => \^total_string_reg[0]\,
      I2 => \caracter[1]_i_36_n_0\,
      I3 => \caracter[1]_i_37_n_0\,
      O => \caracter[1]_i_121_n_0\
    );
\caracter[1]_i_122\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5A335ACC0F000FFF"
    )
        port map (
      I0 => p_1_in(0),
      I1 => \caracter_reg[3]_i_126_0\(0),
      I2 => p_1_in(1),
      I3 => \^disp_dinero[5]_0\(1),
      I4 => \caracter_reg[3]_i_126_0\(1),
      I5 => \caracter_reg[1]_i_102_n_3\,
      O => \caracter[1]_i_122_n_0\
    );
\caracter[1]_i_123\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^total_string_reg[0]\,
      I1 => \caracter_reg[1]_i_102_n_3\,
      O => \caracter[1]_i_123_n_0\
    );
\caracter[1]_i_125\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[3]_i_42_n_0\,
      O => \caracter[1]_i_125_n_0\
    );
\caracter[1]_i_126\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \caracter[3]_i_46_n_0\,
      I1 => \^disp_dinero[5]_0\(1),
      I2 => \caracter_reg[3]_i_126_0\(6),
      O => \caracter[1]_i_126_n_0\
    );
\caracter[1]_i_127\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \caracter[3]_i_46_n_0\,
      I1 => \^disp_dinero[5]_0\(1),
      I2 => \caracter_reg[3]_i_126_0\(6),
      O => \caracter[1]_i_127_n_0\
    );
\caracter[1]_i_128\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter[3]_i_43_n_0\,
      I1 => \caracter[3]_i_42_n_0\,
      O => \caracter[1]_i_128_n_0\
    );
\caracter[1]_i_129\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[3]_i_42_n_0\,
      O => \caracter[1]_i_129_n_0\
    );
\caracter[1]_i_130\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \caracter[3]_i_46_n_0\,
      I1 => \^disp_dinero[5]_0\(1),
      I2 => \caracter_reg[3]_i_126_0\(6),
      O => \caracter[1]_i_130_n_0\
    );
\caracter[1]_i_131\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \caracter[3]_i_46_n_0\,
      I1 => \^disp_dinero[5]_0\(1),
      I2 => \caracter_reg[3]_i_126_0\(6),
      O => \caracter[1]_i_131_n_0\
    );
\caracter[1]_i_132\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter[3]_i_43_n_0\,
      I1 => \caracter[3]_i_42_n_0\,
      O => \caracter[1]_i_132_n_0\
    );
\caracter[1]_i_133\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[1]_i_102_n_3\,
      I1 => \caracter[3]_i_43_n_0\,
      O => \caracter[1]_i_133_n_0\
    );
\caracter[1]_i_134\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4D"
    )
        port map (
      I0 => \caracter[1]_i_35_n_0\,
      I1 => \caracter[3]_i_43_n_0\,
      I2 => \caracter_reg[1]_i_102_n_3\,
      O => \caracter[1]_i_134_n_0\
    );
\caracter[1]_i_135\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => \caracter_reg[1]_i_102_n_3\,
      I1 => \caracter[1]_i_36_n_0\,
      I2 => \caracter[1]_i_41_n_0\,
      O => \caracter[1]_i_135_n_0\
    );
\caracter[1]_i_136\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => \caracter[1]_i_37_n_0\,
      I1 => \caracter_reg[1]_i_102_n_3\,
      I2 => \caracter[1]_i_35_n_0\,
      O => \caracter[1]_i_136_n_0\
    );
\caracter[1]_i_137\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B4D2"
    )
        port map (
      I0 => \caracter[1]_i_41_n_0\,
      I1 => \caracter[3]_i_42_n_0\,
      I2 => \caracter[3]_i_43_n_0\,
      I3 => \caracter_reg[1]_i_102_n_3\,
      O => \caracter[1]_i_137_n_0\
    );
\caracter[1]_i_138\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \caracter[1]_i_134_n_0\,
      I1 => \caracter[1]_i_41_n_0\,
      I2 => \caracter[3]_i_42_n_0\,
      I3 => \caracter_reg[1]_i_102_n_3\,
      O => \caracter[1]_i_138_n_0\
    );
\caracter[1]_i_139\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \caracter[1]_i_35_n_0\,
      I1 => \caracter[3]_i_43_n_0\,
      I2 => \caracter_reg[1]_i_102_n_3\,
      I3 => \caracter[1]_i_135_n_0\,
      O => \caracter[1]_i_139_n_0\
    );
\caracter[1]_i_140\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \caracter_reg[1]_i_102_n_3\,
      I1 => \caracter[1]_i_36_n_0\,
      I2 => \caracter[1]_i_41_n_0\,
      I3 => \caracter[1]_i_136_n_0\,
      O => \caracter[1]_i_140_n_0\
    );
\caracter[1]_i_142\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \caracter_reg[1]_i_199_n_3\,
      I1 => \caracter_reg[1]_i_200_n_0\,
      O => \caracter[1]_i_142_n_0\
    );
\caracter[1]_i_143\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"044A"
    )
        port map (
      I0 => \caracter_reg[1]_i_201_n_0\,
      I1 => \caracter_reg[1]_i_201_n_5\,
      I2 => \caracter_reg[1]_i_199_n_3\,
      I3 => \caracter_reg[1]_i_200_n_0\,
      O => \caracter[1]_i_143_n_0\
    );
\caracter[1]_i_144\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \caracter_reg[1]_i_201_n_6\,
      I1 => \caracter_reg[1]_i_201_n_5\,
      I2 => \caracter_reg[1]_i_200_n_0\,
      I3 => \caracter_reg[1]_i_199_n_3\,
      O => \caracter[1]_i_144_n_0\
    );
\caracter[1]_i_145\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \caracter_reg[1]_i_201_n_7\,
      I1 => \caracter_reg[1]_i_201_n_6\,
      I2 => \caracter_reg[1]_i_200_n_0\,
      I3 => \caracter_reg[1]_i_199_n_3\,
      O => \caracter[1]_i_145_n_0\
    );
\caracter[1]_i_146\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter_reg[1]_i_201_n_0\,
      I1 => \caracter_reg[1]_i_199_n_3\,
      I2 => \caracter_reg[1]_i_200_n_0\,
      O => \caracter[1]_i_146_n_0\
    );
\caracter[1]_i_147\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0113"
    )
        port map (
      I0 => \caracter_reg[1]_i_201_n_5\,
      I1 => \caracter_reg[1]_i_201_n_0\,
      I2 => \caracter_reg[1]_i_199_n_3\,
      I3 => \caracter_reg[1]_i_200_n_0\,
      O => \caracter[1]_i_147_n_0\
    );
\caracter[1]_i_148\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"566AA995"
    )
        port map (
      I0 => \caracter[1]_i_144_n_0\,
      I1 => \caracter_reg[1]_i_200_n_0\,
      I2 => \caracter_reg[1]_i_199_n_3\,
      I3 => \caracter_reg[1]_i_201_n_5\,
      I4 => \caracter_reg[1]_i_201_n_0\,
      O => \caracter[1]_i_148_n_0\
    );
\caracter[1]_i_149\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCC9C999"
    )
        port map (
      I0 => \caracter_reg[1]_i_201_n_6\,
      I1 => \caracter_reg[1]_i_201_n_5\,
      I2 => \caracter_reg[1]_i_200_n_0\,
      I3 => \caracter_reg[1]_i_199_n_3\,
      I4 => \caracter_reg[1]_i_201_n_7\,
      O => \caracter[1]_i_149_n_0\
    );
\caracter[1]_i_151\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"9A"
    )
        port map (
      I0 => \caracter[3]_i_42_n_0\,
      I1 => \caracter[3]_i_43_n_0\,
      I2 => \caracter_reg[1]_i_102_n_3\,
      O => \caracter[1]_i_151_n_0\
    );
\caracter[1]_i_153\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B190"
    )
        port map (
      I0 => \caracter_reg[1]_i_161_n_7\,
      I1 => \caracter_reg[1]_i_102_n_3\,
      I2 => \caracter_reg[1]_i_78_n_7\,
      I3 => \caracter_reg[1]_i_215_n_4\,
      O => \caracter[1]_i_153_n_0\
    );
\caracter[1]_i_154\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B190"
    )
        port map (
      I0 => \caracter_reg[1]_i_215_n_4\,
      I1 => \caracter_reg[1]_i_102_n_3\,
      I2 => \caracter_reg[1]_i_124_n_4\,
      I3 => \caracter_reg[1]_i_215_n_5\,
      O => \caracter[1]_i_154_n_0\
    );
\caracter[1]_i_155\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B190"
    )
        port map (
      I0 => \caracter_reg[1]_i_215_n_5\,
      I1 => \caracter_reg[1]_i_102_n_3\,
      I2 => \caracter_reg[1]_i_124_n_5\,
      I3 => \caracter_reg[1]_i_215_n_6\,
      O => \caracter[1]_i_155_n_0\
    );
\caracter[1]_i_156\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A832"
    )
        port map (
      I0 => \caracter_reg[1]_i_124_n_6\,
      I1 => \caracter_reg[1]_i_102_n_3\,
      I2 => \caracter_reg[1]_i_216_n_7\,
      I3 => \caracter_reg[1]_i_215_n_6\,
      O => \caracter[1]_i_156_n_0\
    );
\caracter[1]_i_157\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669966996696996"
    )
        port map (
      I0 => \caracter[1]_i_153_n_0\,
      I1 => \caracter_reg[1]_i_78_n_6\,
      I2 => \caracter_reg[1]_i_161_n_6\,
      I3 => \^total_string_reg[0]\,
      I4 => \caracter_reg[1]_i_102_n_3\,
      I5 => \caracter_reg[1]_i_161_n_7\,
      O => \caracter[1]_i_157_n_0\
    );
\caracter[1]_i_158\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C3873C780F1EF0E1"
    )
        port map (
      I0 => \caracter_reg[1]_i_215_n_5\,
      I1 => \caracter_reg[1]_i_124_n_4\,
      I2 => \caracter_reg[1]_i_78_n_7\,
      I3 => \caracter_reg[1]_i_102_n_3\,
      I4 => \caracter_reg[1]_i_161_n_7\,
      I5 => \caracter_reg[1]_i_215_n_4\,
      O => \caracter[1]_i_158_n_0\
    );
\caracter[1]_i_159\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C3873C780F1EF0E1"
    )
        port map (
      I0 => \caracter_reg[1]_i_215_n_6\,
      I1 => \caracter_reg[1]_i_124_n_5\,
      I2 => \caracter_reg[1]_i_124_n_4\,
      I3 => \caracter_reg[1]_i_102_n_3\,
      I4 => \caracter_reg[1]_i_215_n_4\,
      I5 => \caracter_reg[1]_i_215_n_5\,
      O => \caracter[1]_i_159_n_0\
    );
\caracter[1]_i_160\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C3873C780F1EF0E1"
    )
        port map (
      I0 => \caracter_reg[1]_i_216_n_7\,
      I1 => \caracter_reg[1]_i_124_n_6\,
      I2 => \caracter_reg[1]_i_124_n_5\,
      I3 => \caracter_reg[1]_i_102_n_3\,
      I4 => \caracter_reg[1]_i_215_n_5\,
      I5 => \caracter_reg[1]_i_215_n_6\,
      O => \caracter[1]_i_160_n_0\
    );
\caracter[1]_i_162\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B22B"
    )
        port map (
      I0 => \caracter_reg[1]_i_102_n_3\,
      I1 => \caracter_reg[1]_i_103_n_7\,
      I2 => \caracter[1]_i_35_n_0\,
      I3 => \^total_string_reg[0]\,
      O => \caracter[1]_i_162_n_0\
    );
\caracter[1]_i_163\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \^total_string_reg[0]\,
      I1 => \caracter[1]_i_35_n_0\,
      I2 => \caracter_reg[1]_i_103_n_7\,
      I3 => \caracter_reg[1]_i_102_n_3\,
      O => \caracter[1]_i_163_n_0\
    );
\caracter[1]_i_164\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => \caracter_reg[1]_i_102_n_3\,
      I1 => \caracter[1]_i_37_n_0\,
      I2 => \caracter_reg[1]_i_161_n_5\,
      O => \caracter[1]_i_164_n_0\
    );
\caracter[1]_i_165\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[1]_i_161_n_5\,
      I1 => \caracter[1]_i_37_n_0\,
      I2 => \caracter_reg[1]_i_102_n_3\,
      O => \caracter[1]_i_165_n_0\
    );
\caracter[1]_i_167\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[3]_i_42_n_0\,
      O => \caracter[1]_i_167_n_0\
    );
\caracter[1]_i_168\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \caracter[3]_i_46_n_0\,
      I1 => \^disp_dinero[5]_0\(1),
      I2 => \caracter_reg[3]_i_126_0\(6),
      O => \caracter[1]_i_168_n_0\
    );
\caracter[1]_i_169\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \caracter[3]_i_43_n_0\,
      I1 => \caracter[1]_i_35_n_0\,
      O => \caracter[1]_i_169_n_0\
    );
\caracter[1]_i_170\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \caracter[3]_i_46_n_0\,
      I1 => \^disp_dinero[5]_0\(1),
      I2 => \caracter_reg[3]_i_126_0\(6),
      O => \caracter[1]_i_170_n_0\
    );
\caracter[1]_i_171\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter[3]_i_43_n_0\,
      I1 => \caracter[3]_i_42_n_0\,
      O => \caracter[1]_i_171_n_0\
    );
\caracter[1]_i_172\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"87"
    )
        port map (
      I0 => \caracter[1]_i_41_n_0\,
      I1 => \caracter[3]_i_42_n_0\,
      I2 => \caracter[3]_i_43_n_0\,
      O => \caracter[1]_i_172_n_0\
    );
\caracter[1]_i_173\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \caracter[3]_i_43_n_0\,
      I1 => \caracter[1]_i_35_n_0\,
      I2 => \caracter[1]_i_41_n_0\,
      I3 => \caracter[3]_i_42_n_0\,
      O => \caracter[1]_i_173_n_0\
    );
\caracter[1]_i_174\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4D"
    )
        port map (
      I0 => \^total_string_reg[0]\,
      I1 => \caracter[1]_i_36_n_0\,
      I2 => \caracter[3]_i_43_n_0\,
      O => \caracter[1]_i_174_n_0\
    );
\caracter[1]_i_175\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter[3]_i_43_n_0\,
      I1 => \caracter[1]_i_36_n_0\,
      I2 => \^total_string_reg[0]\,
      O => \caracter[1]_i_175_n_0\
    );
\caracter[1]_i_176\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \^total_string_reg[0]\,
      I1 => \caracter[1]_i_35_n_0\,
      O => \caracter[1]_i_176_n_0\
    );
\caracter[1]_i_177\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \caracter[1]_i_174_n_0\,
      I1 => \caracter[1]_i_35_n_0\,
      I2 => \caracter[1]_i_37_n_0\,
      I3 => \caracter[3]_i_42_n_0\,
      O => \caracter[1]_i_177_n_0\
    );
\caracter[1]_i_178\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96699696"
    )
        port map (
      I0 => \^total_string_reg[0]\,
      I1 => \caracter[1]_i_36_n_0\,
      I2 => \caracter[3]_i_43_n_0\,
      I3 => \caracter[1]_i_41_n_0\,
      I4 => \caracter[1]_i_37_n_0\,
      O => \caracter[1]_i_178_n_0\
    );
\caracter[1]_i_179\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => \caracter[1]_i_35_n_0\,
      I1 => \^total_string_reg[0]\,
      I2 => \caracter[1]_i_37_n_0\,
      I3 => \caracter[1]_i_41_n_0\,
      O => \caracter[1]_i_179_n_0\
    );
\caracter[1]_i_18\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"308EEF30EF3008E0"
    )
        port map (
      I0 => \caracter[0]_i_11_n_0\,
      I1 => \caracter[2]_i_13_n_0\,
      I2 => \caracter[3]_i_16_n_0\,
      I3 => \caracter[3]_i_20_n_0\,
      I4 => \caracter[3]_i_23_n_0\,
      I5 => \caracter[3]_i_17_n_0\,
      O => \caracter[1]_i_18_n_0\
    );
\caracter[1]_i_180\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[1]_i_35_n_0\,
      I1 => \^total_string_reg[0]\,
      O => \caracter[1]_i_180_n_0\
    );
\caracter[1]_i_182\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \caracter[3]_i_42_n_0\,
      I1 => \caracter[1]_i_41_n_0\,
      O => \caracter[1]_i_182_n_0\
    );
\caracter[1]_i_183\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \caracter[3]_i_43_n_0\,
      I1 => \caracter[1]_i_35_n_0\,
      O => \caracter[1]_i_183_n_0\
    );
\caracter[1]_i_184\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \caracter[1]_i_41_n_0\,
      I1 => \caracter[1]_i_36_n_0\,
      O => \caracter[1]_i_184_n_0\
    );
\caracter[1]_i_185\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter[1]_i_41_n_0\,
      I1 => \caracter[1]_i_36_n_0\,
      O => \caracter[1]_i_185_n_0\
    );
\caracter[1]_i_186\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B4"
    )
        port map (
      I0 => \caracter[1]_i_41_n_0\,
      I1 => \caracter[3]_i_42_n_0\,
      I2 => \caracter[3]_i_43_n_0\,
      O => \caracter[1]_i_186_n_0\
    );
\caracter[1]_i_187\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B44B"
    )
        port map (
      I0 => \caracter[1]_i_35_n_0\,
      I1 => \caracter[3]_i_43_n_0\,
      I2 => \caracter[3]_i_42_n_0\,
      I3 => \caracter[1]_i_41_n_0\,
      O => \caracter[1]_i_187_n_0\
    );
\caracter[1]_i_188\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B44B"
    )
        port map (
      I0 => \caracter[1]_i_36_n_0\,
      I1 => \caracter[1]_i_41_n_0\,
      I2 => \caracter[3]_i_43_n_0\,
      I3 => \caracter[1]_i_35_n_0\,
      O => \caracter[1]_i_188_n_0\
    );
\caracter[1]_i_189\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"D42B2BD4"
    )
        port map (
      I0 => \caracter[1]_i_35_n_0\,
      I1 => \caracter[3]_i_42_n_0\,
      I2 => \caracter[1]_i_37_n_0\,
      I3 => \caracter[1]_i_36_n_0\,
      I4 => \caracter[1]_i_41_n_0\,
      O => \caracter[1]_i_189_n_0\
    );
\caracter[1]_i_19\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"26646565A6A62664"
    )
        port map (
      I0 => \caracter[3]_i_26_n_0\,
      I1 => \caracter[3]_i_25_n_0\,
      I2 => \caracter[2]_i_13_n_0\,
      I3 => \caracter[3]_i_24_n_0\,
      I4 => \caracter[0]_i_10_n_0\,
      I5 => \caracter[0]_i_11_n_0\,
      O => \caracter[1]_i_19_n_0\
    );
\caracter[1]_i_191\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \caracter_reg[1]_i_248_n_4\,
      I1 => \caracter_reg[1]_i_201_n_7\,
      I2 => \caracter_reg[1]_i_200_n_0\,
      I3 => \caracter_reg[1]_i_199_n_3\,
      O => \caracter[1]_i_191_n_0\
    );
\caracter[1]_i_192\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \caracter_reg[1]_i_248_n_5\,
      I1 => \caracter_reg[1]_i_248_n_4\,
      I2 => \caracter_reg[1]_i_200_n_0\,
      I3 => \caracter_reg[1]_i_199_n_3\,
      O => \caracter[1]_i_192_n_0\
    );
\caracter[1]_i_193\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \caracter_reg[1]_i_248_n_6\,
      I1 => \caracter_reg[1]_i_248_n_5\,
      I2 => \caracter_reg[1]_i_200_n_0\,
      I3 => \caracter_reg[1]_i_199_n_3\,
      O => \caracter[1]_i_193_n_0\
    );
\caracter[1]_i_194\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"90660060"
    )
        port map (
      I0 => \caracter_reg[1]_i_248_n_6\,
      I1 => \caracter_reg[1]_i_200_n_0\,
      I2 => \caracter_reg[1]_i_248_n_7\,
      I3 => \caracter_reg[1]_i_199_n_3\,
      I4 => \caracter_reg[1]_i_200_n_5\,
      O => \caracter[1]_i_194_n_0\
    );
\caracter[1]_i_195\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0E1E1C3"
    )
        port map (
      I0 => \caracter_reg[1]_i_248_n_4\,
      I1 => \caracter_reg[1]_i_201_n_7\,
      I2 => \caracter_reg[1]_i_201_n_6\,
      I3 => \caracter_reg[1]_i_200_n_0\,
      I4 => \caracter_reg[1]_i_199_n_3\,
      O => \caracter[1]_i_195_n_0\
    );
\caracter[1]_i_196\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0E1E1C3"
    )
        port map (
      I0 => \caracter_reg[1]_i_248_n_5\,
      I1 => \caracter_reg[1]_i_248_n_4\,
      I2 => \caracter_reg[1]_i_201_n_7\,
      I3 => \caracter_reg[1]_i_200_n_0\,
      I4 => \caracter_reg[1]_i_199_n_3\,
      O => \caracter[1]_i_196_n_0\
    );
\caracter[1]_i_197\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0E1E1C3"
    )
        port map (
      I0 => \caracter_reg[1]_i_248_n_6\,
      I1 => \caracter_reg[1]_i_248_n_5\,
      I2 => \caracter_reg[1]_i_248_n_4\,
      I3 => \caracter_reg[1]_i_200_n_0\,
      I4 => \caracter_reg[1]_i_199_n_3\,
      O => \caracter[1]_i_197_n_0\
    );
\caracter[1]_i_198\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7F80F807FE01E01F"
    )
        port map (
      I0 => \caracter_reg[1]_i_200_n_5\,
      I1 => \caracter_reg[1]_i_248_n_7\,
      I2 => \caracter_reg[1]_i_248_n_6\,
      I3 => \caracter_reg[1]_i_248_n_5\,
      I4 => \caracter_reg[1]_i_200_n_0\,
      I5 => \caracter_reg[1]_i_199_n_3\,
      O => \caracter[1]_i_198_n_0\
    );
\caracter[1]_i_202\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \caracter_reg[1]_i_85_n_4\,
      I1 => \caracter_reg[1]_i_85_n_7\,
      O => \caracter[1]_i_202_n_0\
    );
\caracter[1]_i_203\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66969969"
    )
        port map (
      I0 => \caracter_reg[1]_i_257_n_6\,
      I1 => \caracter_reg[1]_i_85_n_5\,
      I2 => \caracter_reg[1]_i_257_n_7\,
      I3 => \caracter_reg[1]_i_85_n_6\,
      I4 => \caracter_reg[1]_i_85_n_7\,
      O => \caracter[1]_i_203_n_0\
    );
\caracter[1]_i_204\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => \caracter_reg[1]_i_85_n_7\,
      I1 => \caracter_reg[1]_i_85_n_4\,
      I2 => \caracter_reg[1]_i_257_n_7\,
      I3 => \caracter_reg[1]_i_85_n_6\,
      O => \caracter[1]_i_204_n_0\
    );
\caracter[1]_i_205\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter_reg[1]_i_85_n_4\,
      I1 => \caracter_reg[1]_i_85_n_7\,
      O => \caracter[1]_i_205_n_0\
    );
\caracter[1]_i_207\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"82"
    )
        port map (
      I0 => \caracter_reg[1]_i_124_n_7\,
      I1 => \caracter_reg[1]_i_216_n_7\,
      I2 => \caracter_reg[1]_i_102_n_3\,
      O => \caracter[1]_i_207_n_0\
    );
\caracter[1]_i_208\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[1]_i_181_n_4\,
      I1 => \caracter_reg[1]_i_166_n_4\,
      O => \caracter[1]_i_208_n_0\
    );
\caracter[1]_i_209\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[1]_i_181_n_5\,
      I1 => \caracter_reg[1]_i_166_n_5\,
      O => \caracter[1]_i_209_n_0\
    );
\caracter[1]_i_210\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[1]_i_181_n_6\,
      I1 => \caracter_reg[1]_i_166_n_6\,
      O => \caracter[1]_i_210_n_0\
    );
\caracter[1]_i_211\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99966669"
    )
        port map (
      I0 => \caracter[1]_i_207_n_0\,
      I1 => \caracter_reg[1]_i_124_n_6\,
      I2 => \caracter_reg[1]_i_102_n_3\,
      I3 => \caracter_reg[1]_i_216_n_7\,
      I4 => \caracter_reg[1]_i_215_n_6\,
      O => \caracter[1]_i_211_n_0\
    );
\caracter[1]_i_212\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[1]_i_124_n_7\,
      I1 => \caracter_reg[1]_i_216_n_7\,
      I2 => \caracter_reg[1]_i_102_n_3\,
      I3 => \caracter[1]_i_208_n_0\,
      O => \caracter[1]_i_212_n_0\
    );
\caracter[1]_i_213\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => \caracter_reg[1]_i_181_n_4\,
      I1 => \caracter_reg[1]_i_166_n_4\,
      I2 => \caracter_reg[1]_i_166_n_5\,
      I3 => \caracter_reg[1]_i_181_n_5\,
      O => \caracter[1]_i_213_n_0\
    );
\caracter[1]_i_214\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \caracter_reg[1]_i_166_n_6\,
      I1 => \caracter_reg[1]_i_181_n_6\,
      I2 => \caracter_reg[1]_i_166_n_5\,
      I3 => \caracter_reg[1]_i_181_n_5\,
      O => \caracter[1]_i_214_n_0\
    );
\caracter[1]_i_217\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter[1]_i_35_n_0\,
      I1 => \caracter[3]_i_43_n_0\,
      O => \caracter[1]_i_217_n_0\
    );
\caracter[1]_i_218\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter[3]_i_42_n_0\,
      I1 => \caracter[1]_i_36_n_0\,
      I2 => \caracter[1]_i_41_n_0\,
      O => \caracter[1]_i_218_n_0\
    );
\caracter[1]_i_219\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter[1]_i_37_n_0\,
      I1 => \caracter[3]_i_43_n_0\,
      I2 => \caracter[1]_i_35_n_0\,
      O => \caracter[1]_i_219_n_0\
    );
\caracter[1]_i_22\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0958"
    )
        port map (
      I0 => \caracter_reg[1]_i_15_n_4\,
      I1 => \caracter_reg[1]_i_15_n_5\,
      I2 => \caracter_reg[1]_i_15_n_6\,
      I3 => \caracter_reg[1]_i_16_n_7\,
      O => \caracter_reg[1]_i_16_0\(0)
    );
\caracter[1]_i_220\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[1]_i_36_n_0\,
      O => \caracter[1]_i_220_n_0\
    );
\caracter[1]_i_221\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"D42B2BD4"
    )
        port map (
      I0 => \caracter[3]_i_42_n_0\,
      I1 => \caracter[1]_i_41_n_0\,
      I2 => \caracter[1]_i_36_n_0\,
      I3 => \caracter[3]_i_43_n_0\,
      I4 => \caracter[1]_i_35_n_0\,
      O => \caracter[1]_i_221_n_0\
    );
\caracter[1]_i_222\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B24D4DB24DB2B24D"
    )
        port map (
      I0 => \caracter[1]_i_37_n_0\,
      I1 => \caracter[3]_i_43_n_0\,
      I2 => \caracter[1]_i_35_n_0\,
      I3 => \caracter[1]_i_41_n_0\,
      I4 => \caracter[1]_i_36_n_0\,
      I5 => \caracter[3]_i_42_n_0\,
      O => \caracter[1]_i_222_n_0\
    );
\caracter[1]_i_223\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96699696"
    )
        port map (
      I0 => \caracter[1]_i_35_n_0\,
      I1 => \caracter[3]_i_43_n_0\,
      I2 => \caracter[1]_i_37_n_0\,
      I3 => \^total_string_reg[0]\,
      I4 => \caracter[1]_i_41_n_0\,
      O => \caracter[1]_i_223_n_0\
    );
\caracter[1]_i_224\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \^total_string_reg[0]\,
      I1 => \caracter[1]_i_41_n_0\,
      I2 => \caracter[1]_i_36_n_0\,
      O => \caracter[1]_i_224_n_0\
    );
\caracter[1]_i_226\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[3]_i_42_n_0\,
      O => \caracter[1]_i_226_n_0\
    );
\caracter[1]_i_227\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \caracter[3]_i_46_n_0\,
      I1 => \^disp_dinero[5]_0\(1),
      I2 => \caracter_reg[3]_i_126_0\(6),
      O => \caracter[1]_i_227_n_0\
    );
\caracter[1]_i_228\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \caracter[3]_i_43_n_0\,
      I1 => \caracter[1]_i_35_n_0\,
      O => \caracter[1]_i_228_n_0\
    );
\caracter[1]_i_229\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \caracter[3]_i_46_n_0\,
      I1 => \^disp_dinero[5]_0\(1),
      I2 => \caracter_reg[3]_i_126_0\(6),
      O => \caracter[1]_i_229_n_0\
    );
\caracter[1]_i_230\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter[3]_i_43_n_0\,
      I1 => \caracter[3]_i_42_n_0\,
      O => \caracter[1]_i_230_n_0\
    );
\caracter[1]_i_231\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"87"
    )
        port map (
      I0 => \caracter[1]_i_41_n_0\,
      I1 => \caracter[3]_i_42_n_0\,
      I2 => \caracter[3]_i_43_n_0\,
      O => \caracter[1]_i_231_n_0\
    );
\caracter[1]_i_232\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \caracter[3]_i_43_n_0\,
      I1 => \caracter[1]_i_35_n_0\,
      I2 => \caracter[1]_i_41_n_0\,
      I3 => \caracter[3]_i_42_n_0\,
      O => \caracter[1]_i_232_n_0\
    );
\caracter[1]_i_233\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter[3]_i_43_n_0\,
      I1 => \caracter[1]_i_36_n_0\,
      I2 => \^total_string_reg[0]\,
      O => \caracter[1]_i_233_n_0\
    );
\caracter[1]_i_234\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \^total_string_reg[0]\,
      I1 => \caracter[1]_i_35_n_0\,
      O => \caracter[1]_i_234_n_0\
    );
\caracter[1]_i_235\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \caracter[1]_i_174_n_0\,
      I1 => \caracter[1]_i_35_n_0\,
      I2 => \caracter[1]_i_37_n_0\,
      I3 => \caracter[3]_i_42_n_0\,
      O => \caracter[1]_i_235_n_0\
    );
\caracter[1]_i_236\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96699696"
    )
        port map (
      I0 => \^total_string_reg[0]\,
      I1 => \caracter[1]_i_36_n_0\,
      I2 => \caracter[3]_i_43_n_0\,
      I3 => \caracter[1]_i_41_n_0\,
      I4 => \caracter[1]_i_37_n_0\,
      O => \caracter[1]_i_236_n_0\
    );
\caracter[1]_i_237\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => \caracter[1]_i_35_n_0\,
      I1 => \^total_string_reg[0]\,
      I2 => \caracter[1]_i_37_n_0\,
      I3 => \caracter[1]_i_41_n_0\,
      O => \caracter[1]_i_237_n_0\
    );
\caracter[1]_i_238\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[1]_i_35_n_0\,
      I1 => \^total_string_reg[0]\,
      O => \caracter[1]_i_238_n_0\
    );
\caracter[1]_i_24\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[1]_i_35_n_0\,
      O => \caracter[1]_i_24_n_0\
    );
\caracter[1]_i_240\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60990090"
    )
        port map (
      I0 => \caracter_reg[1]_i_200_n_5\,
      I1 => \caracter_reg[1]_i_248_n_7\,
      I2 => \caracter_reg[1]_i_294_n_4\,
      I3 => \caracter_reg[1]_i_199_n_3\,
      I4 => \caracter_reg[1]_i_200_n_6\,
      O => \caracter[1]_i_240_n_0\
    );
\caracter[1]_i_241\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60990090"
    )
        port map (
      I0 => \caracter_reg[1]_i_200_n_6\,
      I1 => \caracter_reg[1]_i_294_n_4\,
      I2 => \caracter_reg[1]_i_294_n_5\,
      I3 => \caracter_reg[1]_i_199_n_3\,
      I4 => \caracter_reg[1]_i_200_n_7\,
      O => \caracter[1]_i_241_n_0\
    );
\caracter[1]_i_242\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60990090"
    )
        port map (
      I0 => \caracter_reg[1]_i_200_n_7\,
      I1 => \caracter_reg[1]_i_294_n_5\,
      I2 => \caracter_reg[1]_i_294_n_6\,
      I3 => \caracter_reg[1]_i_199_n_3\,
      I4 => \caracter_reg[1]_i_250_n_4\,
      O => \caracter[1]_i_242_n_0\
    );
\caracter[1]_i_243\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E8E800E80000E8"
    )
        port map (
      I0 => \caracter_reg[1]_i_250_n_5\,
      I1 => \caracter_reg[1]_i_249_n_4\,
      I2 => \caracter_reg[1]_i_294_n_7\,
      I3 => \caracter_reg[1]_i_250_n_4\,
      I4 => \caracter_reg[1]_i_199_n_3\,
      I5 => \caracter_reg[1]_i_294_n_6\,
      O => \caracter[1]_i_243_n_0\
    );
\caracter[1]_i_244\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"95566AA96AA99556"
    )
        port map (
      I0 => \caracter[1]_i_240_n_0\,
      I1 => \caracter_reg[1]_i_200_n_5\,
      I2 => \caracter_reg[1]_i_199_n_3\,
      I3 => \caracter_reg[1]_i_248_n_7\,
      I4 => \caracter_reg[1]_i_200_n_0\,
      I5 => \caracter_reg[1]_i_248_n_6\,
      O => \caracter[1]_i_244_n_0\
    );
\caracter[1]_i_245\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AA9955695566AA9"
    )
        port map (
      I0 => \caracter[1]_i_241_n_0\,
      I1 => \caracter_reg[1]_i_200_n_6\,
      I2 => \caracter_reg[1]_i_199_n_3\,
      I3 => \caracter_reg[1]_i_294_n_4\,
      I4 => \caracter_reg[1]_i_248_n_7\,
      I5 => \caracter_reg[1]_i_200_n_5\,
      O => \caracter[1]_i_245_n_0\
    );
\caracter[1]_i_246\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AA9955695566AA9"
    )
        port map (
      I0 => \caracter[1]_i_242_n_0\,
      I1 => \caracter_reg[1]_i_200_n_7\,
      I2 => \caracter_reg[1]_i_199_n_3\,
      I3 => \caracter_reg[1]_i_294_n_5\,
      I4 => \caracter_reg[1]_i_294_n_4\,
      I5 => \caracter_reg[1]_i_200_n_6\,
      O => \caracter[1]_i_246_n_0\
    );
\caracter[1]_i_247\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AA9955695566AA9"
    )
        port map (
      I0 => \caracter[1]_i_243_n_0\,
      I1 => \caracter_reg[1]_i_250_n_4\,
      I2 => \caracter_reg[1]_i_199_n_3\,
      I3 => \caracter_reg[1]_i_294_n_6\,
      I4 => \caracter_reg[1]_i_294_n_5\,
      I5 => \caracter_reg[1]_i_200_n_7\,
      O => \caracter[1]_i_247_n_0\
    );
\caracter[1]_i_25\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[1]_i_36_n_0\,
      O => \caracter[1]_i_25_n_0\
    );
\caracter[1]_i_251\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(7),
      O => \caracter[1]_i_251_n_0\
    );
\caracter[1]_i_252\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(6),
      O => \caracter[1]_i_252_n_0\
    );
\caracter[1]_i_253\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(5),
      O => \caracter[1]_i_253_n_0\
    );
\caracter[1]_i_254\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(6),
      I1 => \caracter_reg[3]_i_126_0\(4),
      O => \caracter[1]_i_254_n_0\
    );
\caracter[1]_i_255\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(7),
      I1 => \caracter_reg[3]_i_126_0\(5),
      I2 => \caracter_reg[3]_i_126_0\(6),
      O => \caracter[1]_i_255_n_0\
    );
\caracter[1]_i_256\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(4),
      I1 => \caracter_reg[3]_i_126_0\(6),
      I2 => \caracter_reg[3]_i_126_0\(7),
      I3 => \caracter_reg[3]_i_126_0\(5),
      O => \caracter[1]_i_256_n_0\
    );
\caracter[1]_i_258\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[1]_i_181_n_7\,
      I1 => \caracter_reg[1]_i_166_n_7\,
      O => \caracter[1]_i_258_n_0\
    );
\caracter[1]_i_259\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \caracter_reg[1]_i_225_n_4\,
      I1 => \caracter[1]_i_36_n_0\,
      O => \caracter[1]_i_259_n_0\
    );
\caracter[1]_i_26\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[1]_i_37_n_0\,
      O => \caracter[1]_i_26_n_0\
    );
\caracter[1]_i_260\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \caracter_reg[1]_i_225_n_5\,
      I1 => \caracter[1]_i_37_n_0\,
      O => \caracter[1]_i_260_n_0\
    );
\caracter[1]_i_261\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \caracter_reg[1]_i_225_n_6\,
      I1 => \^total_string_reg[0]\,
      O => \caracter[1]_i_261_n_0\
    );
\caracter[1]_i_262\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \caracter_reg[1]_i_166_n_7\,
      I1 => \caracter_reg[1]_i_181_n_7\,
      I2 => \caracter_reg[1]_i_166_n_6\,
      I3 => \caracter_reg[1]_i_181_n_6\,
      O => \caracter[1]_i_262_n_0\
    );
\caracter[1]_i_263\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4BB4"
    )
        port map (
      I0 => \caracter[1]_i_36_n_0\,
      I1 => \caracter_reg[1]_i_225_n_4\,
      I2 => \caracter_reg[1]_i_166_n_7\,
      I3 => \caracter_reg[1]_i_181_n_7\,
      O => \caracter[1]_i_263_n_0\
    );
\caracter[1]_i_264\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B44B"
    )
        port map (
      I0 => \caracter[1]_i_37_n_0\,
      I1 => \caracter_reg[1]_i_225_n_5\,
      I2 => \caracter[1]_i_36_n_0\,
      I3 => \caracter_reg[1]_i_225_n_4\,
      O => \caracter[1]_i_264_n_0\
    );
\caracter[1]_i_265\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B44B"
    )
        port map (
      I0 => \^total_string_reg[0]\,
      I1 => \caracter_reg[1]_i_225_n_6\,
      I2 => \caracter[1]_i_37_n_0\,
      I3 => \caracter_reg[1]_i_225_n_5\,
      O => \caracter[1]_i_265_n_0\
    );
\caracter[1]_i_266\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[1]_i_37_n_0\,
      O => \caracter[1]_i_266_n_0\
    );
\caracter[1]_i_267\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^total_string_reg[0]\,
      O => \caracter[1]_i_267_n_0\
    );
\caracter[1]_i_268\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter[1]_i_37_n_0\,
      I1 => \caracter[1]_i_35_n_0\,
      O => \caracter[1]_i_268_n_0\
    );
\caracter[1]_i_269\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^total_string_reg[0]\,
      I1 => \caracter[1]_i_36_n_0\,
      O => \caracter[1]_i_269_n_0\
    );
\caracter[1]_i_27\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^total_string_reg[0]\,
      O => \caracter[1]_i_27_n_0\
    );
\caracter[1]_i_270\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => p_1_in(1),
      I1 => \^disp_dinero[5]_0\(1),
      I2 => \caracter_reg[3]_i_126_0\(1),
      O => \caracter[1]_i_270_n_0\
    );
\caracter[1]_i_271\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^total_string_reg[0]\,
      O => \caracter[1]_i_271_n_0\
    );
\caracter[1]_i_272\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[1]_i_37_n_0\,
      O => \caracter[1]_i_272_n_0\
    );
\caracter[1]_i_273\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^total_string_reg[0]\,
      O => \caracter[1]_i_273_n_0\
    );
\caracter[1]_i_274\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter[1]_i_37_n_0\,
      I1 => \caracter[1]_i_35_n_0\,
      O => \caracter[1]_i_274_n_0\
    );
\caracter[1]_i_275\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^total_string_reg[0]\,
      I1 => \caracter[1]_i_36_n_0\,
      O => \caracter[1]_i_275_n_0\
    );
\caracter[1]_i_276\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => p_1_in(1),
      I1 => \^disp_dinero[5]_0\(1),
      I2 => \caracter_reg[3]_i_126_0\(1),
      O => \caracter[1]_i_276_n_0\
    );
\caracter[1]_i_277\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^total_string_reg[0]\,
      O => \caracter[1]_i_277_n_0\
    );
\caracter[1]_i_278\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter[1]_i_35_n_0\,
      I1 => \caracter[3]_i_43_n_0\,
      O => \caracter[1]_i_278_n_0\
    );
\caracter[1]_i_279\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter[1]_i_37_n_0\,
      I1 => \caracter[3]_i_43_n_0\,
      I2 => \caracter[1]_i_35_n_0\,
      O => \caracter[1]_i_279_n_0\
    );
\caracter[1]_i_28\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[1]_i_35_n_0\,
      I1 => \caracter_reg[1]_i_38_n_6\,
      O => \caracter[1]_i_28_n_0\
    );
\caracter[1]_i_280\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[1]_i_36_n_0\,
      O => \caracter[1]_i_280_n_0\
    );
\caracter[1]_i_281\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"D42B2BD4"
    )
        port map (
      I0 => \caracter[3]_i_42_n_0\,
      I1 => \caracter[1]_i_41_n_0\,
      I2 => \caracter[1]_i_36_n_0\,
      I3 => \caracter[3]_i_43_n_0\,
      I4 => \caracter[1]_i_35_n_0\,
      O => \caracter[1]_i_281_n_0\
    );
\caracter[1]_i_282\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B24D4DB24DB2B24D"
    )
        port map (
      I0 => \caracter[1]_i_37_n_0\,
      I1 => \caracter[3]_i_43_n_0\,
      I2 => \caracter[1]_i_35_n_0\,
      I3 => \caracter[1]_i_41_n_0\,
      I4 => \caracter[1]_i_36_n_0\,
      I5 => \caracter[3]_i_42_n_0\,
      O => \caracter[1]_i_282_n_0\
    );
\caracter[1]_i_283\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96699696"
    )
        port map (
      I0 => \caracter[1]_i_35_n_0\,
      I1 => \caracter[3]_i_43_n_0\,
      I2 => \caracter[1]_i_37_n_0\,
      I3 => \^total_string_reg[0]\,
      I4 => \caracter[1]_i_41_n_0\,
      O => \caracter[1]_i_283_n_0\
    );
\caracter[1]_i_284\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \^total_string_reg[0]\,
      I1 => \caracter[1]_i_41_n_0\,
      I2 => \caracter[1]_i_36_n_0\,
      O => \caracter[1]_i_284_n_0\
    );
\caracter[1]_i_286\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E80000E800E8E800"
    )
        port map (
      I0 => \caracter_reg[1]_i_250_n_6\,
      I1 => \caracter_reg[1]_i_249_n_5\,
      I2 => \caracter_reg[3]_i_126_0\(2),
      I3 => \caracter_reg[1]_i_249_n_4\,
      I4 => \caracter_reg[1]_i_294_n_7\,
      I5 => \caracter_reg[1]_i_250_n_5\,
      O => \caracter[1]_i_286_n_0\
    );
\caracter[1]_i_287\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E80000E800E8E800"
    )
        port map (
      I0 => \caracter_reg[1]_i_250_n_7\,
      I1 => \caracter_reg[1]_i_249_n_6\,
      I2 => \caracter_reg[3]_i_126_0\(1),
      I3 => \caracter_reg[3]_i_126_0\(2),
      I4 => \caracter_reg[1]_i_249_n_5\,
      I5 => \caracter_reg[1]_i_250_n_6\,
      O => \caracter[1]_i_287_n_0\
    );
\caracter[1]_i_288\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E80000E800E8E800"
    )
        port map (
      I0 => \caracter_reg[1]_i_310_n_4\,
      I1 => \caracter_reg[1]_i_249_n_7\,
      I2 => \caracter_reg[3]_i_126_0\(0),
      I3 => \caracter_reg[3]_i_126_0\(1),
      I4 => \caracter_reg[1]_i_249_n_6\,
      I5 => \caracter_reg[1]_i_250_n_7\,
      O => \caracter[1]_i_288_n_0\
    );
\caracter[1]_i_289\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80080880"
    )
        port map (
      I0 => \caracter_reg[1]_i_303_n_4\,
      I1 => \caracter_reg[1]_i_310_n_5\,
      I2 => \caracter_reg[3]_i_126_0\(0),
      I3 => \caracter_reg[1]_i_249_n_7\,
      I4 => \caracter_reg[1]_i_310_n_4\,
      O => \caracter[1]_i_289_n_0\
    );
\caracter[1]_i_29\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[1]_i_36_n_0\,
      I1 => \caracter_reg[1]_i_38_n_7\,
      O => \caracter[1]_i_29_n_0\
    );
\caracter[1]_i_290\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99969666"
    )
        port map (
      I0 => \caracter[1]_i_286_n_0\,
      I1 => \caracter[1]_i_326_n_0\,
      I2 => \caracter_reg[1]_i_294_n_7\,
      I3 => \caracter_reg[1]_i_249_n_4\,
      I4 => \caracter_reg[1]_i_250_n_5\,
      O => \caracter[1]_i_290_n_0\
    );
\caracter[1]_i_291\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66696999"
    )
        port map (
      I0 => \caracter[1]_i_287_n_0\,
      I1 => \caracter[1]_i_327_n_0\,
      I2 => \caracter_reg[3]_i_126_0\(2),
      I3 => \caracter_reg[1]_i_249_n_5\,
      I4 => \caracter_reg[1]_i_250_n_6\,
      O => \caracter[1]_i_291_n_0\
    );
\caracter[1]_i_292\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66696999"
    )
        port map (
      I0 => \caracter[1]_i_288_n_0\,
      I1 => \caracter[1]_i_328_n_0\,
      I2 => \caracter_reg[3]_i_126_0\(1),
      I3 => \caracter_reg[1]_i_249_n_6\,
      I4 => \caracter_reg[1]_i_250_n_7\,
      O => \caracter[1]_i_292_n_0\
    );
\caracter[1]_i_293\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66696999"
    )
        port map (
      I0 => \caracter[1]_i_289_n_0\,
      I1 => \caracter[1]_i_329_n_0\,
      I2 => \caracter_reg[3]_i_126_0\(0),
      I3 => \caracter_reg[1]_i_249_n_7\,
      I4 => \caracter_reg[1]_i_310_n_4\,
      O => \caracter[1]_i_293_n_0\
    );
\caracter[1]_i_295\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(5),
      I1 => \caracter_reg[3]_i_126_0\(3),
      O => \caracter[1]_i_295_n_0\
    );
\caracter[1]_i_296\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(2),
      I1 => \caracter_reg[3]_i_126_0\(4),
      O => \caracter[1]_i_296_n_0\
    );
\caracter[1]_i_297\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(1),
      I1 => \caracter_reg[3]_i_126_0\(3),
      I2 => \caracter_reg[3]_i_126_0\(7),
      O => \caracter[1]_i_297_n_0\
    );
\caracter[1]_i_298\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(7),
      I1 => \caracter_reg[3]_i_126_0\(1),
      I2 => \caracter_reg[3]_i_126_0\(3),
      O => \caracter[1]_i_298_n_0\
    );
\caracter[1]_i_299\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(3),
      I1 => \caracter_reg[3]_i_126_0\(5),
      I2 => \caracter_reg[3]_i_126_0\(4),
      I3 => \caracter_reg[3]_i_126_0\(6),
      O => \caracter[1]_i_299_n_0\
    );
\caracter[1]_i_30\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[1]_i_37_n_0\,
      I1 => \caracter_reg[1]_i_39_n_4\,
      O => \caracter[1]_i_30_n_0\
    );
\caracter[1]_i_300\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(4),
      I1 => \caracter_reg[3]_i_126_0\(2),
      I2 => \caracter_reg[3]_i_126_0\(3),
      I3 => \caracter_reg[3]_i_126_0\(5),
      O => \caracter[1]_i_300_n_0\
    );
\caracter[1]_i_301\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E81717E8"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(7),
      I1 => \caracter_reg[3]_i_126_0\(3),
      I2 => \caracter_reg[3]_i_126_0\(1),
      I3 => \caracter_reg[3]_i_126_0\(2),
      I4 => \caracter_reg[3]_i_126_0\(4),
      O => \caracter[1]_i_301_n_0\
    );
\caracter[1]_i_302\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(3),
      I1 => \caracter_reg[3]_i_126_0\(1),
      I2 => \caracter_reg[3]_i_126_0\(7),
      I3 => \caracter_reg[3]_i_126_0\(2),
      I4 => \caracter_reg[3]_i_126_0\(0),
      O => \caracter[1]_i_302_n_0\
    );
\caracter[1]_i_304\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(4),
      I1 => \caracter_reg[3]_i_126_0\(6),
      O => \caracter[1]_i_304_n_0\
    );
\caracter[1]_i_305\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(6),
      I1 => \caracter_reg[3]_i_126_0\(4),
      O => \caracter[1]_i_305_n_0\
    );
\caracter[1]_i_306\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(7),
      O => \caracter[1]_i_306_n_0\
    );
\caracter[1]_i_307\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2D"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(7),
      I1 => \caracter_reg[3]_i_126_0\(5),
      I2 => \caracter_reg[3]_i_126_0\(6),
      O => \caracter[1]_i_307_n_0\
    );
\caracter[1]_i_308\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"D22D"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(6),
      I1 => \caracter_reg[3]_i_126_0\(4),
      I2 => \caracter_reg[3]_i_126_0\(7),
      I3 => \caracter_reg[3]_i_126_0\(5),
      O => \caracter[1]_i_308_n_0\
    );
\caracter[1]_i_309\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4DB2B24D"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(7),
      I1 => \caracter_reg[3]_i_126_0\(3),
      I2 => \caracter_reg[3]_i_126_0\(5),
      I3 => \caracter_reg[3]_i_126_0\(4),
      I4 => \caracter_reg[3]_i_126_0\(6),
      O => \caracter[1]_i_309_n_0\
    );
\caracter[1]_i_31\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => p_1_in(0),
      I1 => \^disp_dinero[5]_0\(1),
      I2 => \caracter_reg[3]_i_126_0\(0),
      O => \caracter[1]_i_31_n_0\
    );
\caracter[1]_i_311\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(4),
      I1 => \caracter_reg[3]_i_126_0\(7),
      O => \caracter[1]_i_311_n_0\
    );
\caracter[1]_i_312\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(3),
      I1 => \caracter_reg[3]_i_126_0\(6),
      O => \caracter[1]_i_312_n_0\
    );
\caracter[1]_i_313\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(2),
      I1 => \caracter_reg[3]_i_126_0\(5),
      O => \caracter[1]_i_313_n_0\
    );
\caracter[1]_i_314\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(1),
      I1 => \caracter_reg[3]_i_126_0\(4),
      O => \caracter[1]_i_314_n_0\
    );
\caracter[1]_i_315\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \caracter_reg[1]_i_199_n_3\,
      I1 => \caracter_reg[1]_i_200_n_0\,
      O => \caracter[1]_i_315_n_0\
    );
\caracter[1]_i_316\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \caracter_reg[1]_i_199_n_3\,
      I1 => \caracter_reg[1]_i_200_n_0\,
      O => \caracter[1]_i_316_n_0\
    );
\caracter[1]_i_318\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BE282828"
    )
        port map (
      I0 => \caracter_reg[1]_i_353_n_1\,
      I1 => \caracter_reg[1]_i_303_n_4\,
      I2 => \caracter_reg[1]_i_310_n_5\,
      I3 => \caracter_reg[1]_i_310_n_6\,
      I4 => \caracter_reg[1]_i_303_n_5\,
      O => \caracter[1]_i_318_n_0\
    );
\caracter[1]_i_319\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BE282828"
    )
        port map (
      I0 => \caracter_reg[1]_i_353_n_6\,
      I1 => \caracter_reg[1]_i_303_n_5\,
      I2 => \caracter_reg[1]_i_310_n_6\,
      I3 => \caracter_reg[3]_i_126_0\(0),
      I4 => \caracter_reg[1]_i_303_n_6\,
      O => \caracter[1]_i_319_n_0\
    );
\caracter[1]_i_32\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter_reg[1]_i_38_n_5\,
      I1 => \caracter[1]_i_41_n_0\,
      O => \caracter[1]_i_32_n_0\
    );
\caracter[1]_i_320\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"28"
    )
        port map (
      I0 => \caracter_reg[1]_i_353_n_7\,
      I1 => \caracter_reg[1]_i_303_n_6\,
      I2 => \caracter_reg[3]_i_126_0\(0),
      O => \caracter[1]_i_320_n_0\
    );
\caracter[1]_i_321\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[1]_i_354_n_4\,
      I1 => \caracter_reg[1]_i_303_n_7\,
      O => \caracter[1]_i_321_n_0\
    );
\caracter[1]_i_322\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7F80F807F807807F"
    )
        port map (
      I0 => \caracter_reg[1]_i_303_n_5\,
      I1 => \caracter_reg[1]_i_310_n_6\,
      I2 => \caracter_reg[1]_i_353_n_1\,
      I3 => \caracter[1]_i_355_n_0\,
      I4 => \caracter_reg[1]_i_303_n_4\,
      I5 => \caracter_reg[1]_i_310_n_5\,
      O => \caracter[1]_i_322_n_0\
    );
\caracter[1]_i_323\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669966996"
    )
        port map (
      I0 => \caracter[1]_i_319_n_0\,
      I1 => \caracter_reg[1]_i_353_n_1\,
      I2 => \caracter_reg[1]_i_303_n_4\,
      I3 => \caracter_reg[1]_i_310_n_5\,
      I4 => \caracter_reg[1]_i_310_n_6\,
      I5 => \caracter_reg[1]_i_303_n_5\,
      O => \caracter[1]_i_323_n_0\
    );
\caracter[1]_i_324\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669966996"
    )
        port map (
      I0 => \caracter[1]_i_320_n_0\,
      I1 => \caracter_reg[1]_i_353_n_6\,
      I2 => \caracter_reg[1]_i_303_n_5\,
      I3 => \caracter_reg[1]_i_310_n_6\,
      I4 => \caracter_reg[3]_i_126_0\(0),
      I5 => \caracter_reg[1]_i_303_n_6\,
      O => \caracter[1]_i_324_n_0\
    );
\caracter[1]_i_325\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \caracter_reg[1]_i_353_n_7\,
      I1 => \caracter_reg[1]_i_303_n_6\,
      I2 => \caracter_reg[3]_i_126_0\(0),
      I3 => \caracter[1]_i_321_n_0\,
      O => \caracter[1]_i_325_n_0\
    );
\caracter[1]_i_326\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[1]_i_294_n_6\,
      I1 => \caracter_reg[1]_i_199_n_3\,
      I2 => \caracter_reg[1]_i_250_n_4\,
      O => \caracter[1]_i_326_n_0\
    );
\caracter[1]_i_327\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[1]_i_250_n_5\,
      I1 => \caracter_reg[1]_i_294_n_7\,
      I2 => \caracter_reg[1]_i_249_n_4\,
      O => \caracter[1]_i_327_n_0\
    );
\caracter[1]_i_328\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[1]_i_250_n_6\,
      I1 => \caracter_reg[1]_i_249_n_5\,
      I2 => \caracter_reg[3]_i_126_0\(2),
      O => \caracter[1]_i_328_n_0\
    );
\caracter[1]_i_329\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[1]_i_250_n_7\,
      I1 => \caracter_reg[1]_i_249_n_6\,
      I2 => \caracter_reg[3]_i_126_0\(1),
      O => \caracter[1]_i_329_n_0\
    );
\caracter[1]_i_330\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(0),
      I1 => \caracter_reg[3]_i_126_0\(2),
      I2 => \caracter_reg[3]_i_126_0\(6),
      O => \caracter[1]_i_330_n_0\
    );
\caracter[1]_i_331\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(5),
      I1 => \caracter_reg[3]_i_126_0\(1),
      O => \caracter[1]_i_331_n_0\
    );
\caracter[1]_i_332\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(4),
      I1 => \caracter_reg[3]_i_126_0\(0),
      O => \caracter[1]_i_332_n_0\
    );
\caracter[1]_i_334\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(2),
      I1 => \caracter_reg[3]_i_126_0\(4),
      I2 => \caracter_reg[3]_i_126_0\(6),
      O => \caracter[1]_i_334_n_0\
    );
\caracter[1]_i_335\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(1),
      I1 => \caracter_reg[3]_i_126_0\(3),
      I2 => \caracter_reg[3]_i_126_0\(5),
      O => \caracter[1]_i_335_n_0\
    );
\caracter[1]_i_336\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4D"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(2),
      I1 => \caracter_reg[3]_i_126_0\(0),
      I2 => \caracter_reg[3]_i_126_0\(4),
      O => \caracter[1]_i_336_n_0\
    );
\caracter[1]_i_337\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(1),
      I1 => \caracter_reg[3]_i_126_0\(3),
      O => \caracter[1]_i_337_n_0\
    );
\caracter[1]_i_338\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \caracter[1]_i_334_n_0\,
      I1 => \caracter_reg[3]_i_126_0\(5),
      I2 => \caracter_reg[3]_i_126_0\(3),
      I3 => \caracter_reg[3]_i_126_0\(7),
      O => \caracter[1]_i_338_n_0\
    );
\caracter[1]_i_339\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(2),
      I1 => \caracter_reg[3]_i_126_0\(4),
      I2 => \caracter_reg[3]_i_126_0\(6),
      I3 => \caracter[1]_i_335_n_0\,
      O => \caracter[1]_i_339_n_0\
    );
\caracter[1]_i_340\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(1),
      I1 => \caracter_reg[3]_i_126_0\(3),
      I2 => \caracter_reg[3]_i_126_0\(5),
      I3 => \caracter[1]_i_336_n_0\,
      O => \caracter[1]_i_340_n_0\
    );
\caracter[1]_i_341\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(2),
      I1 => \caracter_reg[3]_i_126_0\(0),
      I2 => \caracter_reg[3]_i_126_0\(4),
      I3 => \caracter[1]_i_337_n_0\,
      O => \caracter[1]_i_341_n_0\
    );
\caracter[1]_i_342\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(0),
      I1 => \caracter_reg[3]_i_126_0\(3),
      O => \caracter[1]_i_342_n_0\
    );
\caracter[1]_i_343\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(2),
      O => \caracter[1]_i_343_n_0\
    );
\caracter[1]_i_344\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(1),
      O => \caracter[1]_i_344_n_0\
    );
\caracter[1]_i_345\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[1]_i_354_n_5\,
      I1 => \caracter_reg[1]_i_333_n_4\,
      O => \caracter[1]_i_345_n_0\
    );
\caracter[1]_i_346\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[1]_i_354_n_6\,
      I1 => \caracter_reg[1]_i_333_n_5\,
      O => \caracter[1]_i_346_n_0\
    );
\caracter[1]_i_347\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[1]_i_354_n_7\,
      I1 => \caracter_reg[1]_i_333_n_6\,
      O => \caracter[1]_i_347_n_0\
    );
\caracter[1]_i_348\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[1]_i_333_n_7\,
      I1 => \caracter_reg[1]_i_360_n_4\,
      O => \caracter[1]_i_348_n_0\
    );
\caracter[1]_i_349\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => \caracter_reg[1]_i_354_n_4\,
      I1 => \caracter_reg[1]_i_303_n_7\,
      I2 => \caracter_reg[1]_i_333_n_4\,
      I3 => \caracter_reg[1]_i_354_n_5\,
      O => \caracter[1]_i_349_n_0\
    );
\caracter[1]_i_35\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"1E001EFF"
    )
        port map (
      I0 => \caracter_reg[1]_i_40_n_5\,
      I1 => \caracter[1]_i_42_n_0\,
      I2 => \caracter_reg[1]_i_40_n_4\,
      I3 => \^disp_dinero[5]_0\(1),
      I4 => \caracter_reg[3]_i_126_0\(3),
      O => \caracter[1]_i_35_n_0\
    );
\caracter[1]_i_350\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \caracter_reg[1]_i_333_n_5\,
      I1 => \caracter_reg[1]_i_354_n_6\,
      I2 => \caracter_reg[1]_i_333_n_4\,
      I3 => \caracter_reg[1]_i_354_n_5\,
      O => \caracter[1]_i_350_n_0\
    );
\caracter[1]_i_351\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \caracter_reg[1]_i_333_n_6\,
      I1 => \caracter_reg[1]_i_354_n_7\,
      I2 => \caracter_reg[1]_i_333_n_5\,
      I3 => \caracter_reg[1]_i_354_n_6\,
      O => \caracter[1]_i_351_n_0\
    );
\caracter[1]_i_352\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \caracter_reg[1]_i_360_n_4\,
      I1 => \caracter_reg[1]_i_333_n_7\,
      I2 => \caracter_reg[1]_i_333_n_6\,
      I3 => \caracter_reg[1]_i_354_n_7\,
      O => \caracter[1]_i_352_n_0\
    );
\caracter[1]_i_355\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[1]_i_310_n_4\,
      I1 => \caracter_reg[1]_i_249_n_7\,
      I2 => \caracter_reg[3]_i_126_0\(0),
      O => \caracter[1]_i_355_n_0\
    );
\caracter[1]_i_356\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(0),
      O => \caracter[1]_i_356_n_0\
    );
\caracter[1]_i_357\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(1),
      I1 => \caracter_reg[3]_i_126_0\(3),
      I2 => \caracter_reg[3]_i_126_0\(0),
      O => \caracter[1]_i_357_n_0\
    );
\caracter[1]_i_358\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(0),
      I1 => \caracter_reg[3]_i_126_0\(2),
      O => \caracter[1]_i_358_n_0\
    );
\caracter[1]_i_359\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(1),
      O => \caracter[1]_i_359_n_0\
    );
\caracter[1]_i_36\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBBBB0BBB0BBB0B"
    )
        port map (
      I0 => aD2M4dsP(2),
      I1 => \^disp_dinero[5]_0\(1),
      I2 => \caracter_reg[3]_i_126_0\(2),
      I3 => \caracter_reg[3]_i_126_0\(7),
      I4 => \caracter_reg[3]_i_126_0\(6),
      I5 => \caracter_reg[3]_i_126_0\(5),
      O => \caracter[1]_i_36_n_0\
    );
\caracter[1]_i_361\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(6),
      I1 => \caracter_reg[3]_i_126_0\(1),
      O => \caracter[1]_i_361_n_0\
    );
\caracter[1]_i_362\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(7),
      I1 => \caracter_reg[3]_i_126_0\(2),
      I2 => \caracter_reg[3]_i_126_0\(3),
      O => \caracter[1]_i_362_n_0\
    );
\caracter[1]_i_363\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(1),
      I1 => \caracter_reg[3]_i_126_0\(6),
      I2 => \caracter_reg[3]_i_126_0\(7),
      I3 => \caracter_reg[3]_i_126_0\(2),
      O => \caracter[1]_i_363_n_0\
    );
\caracter[1]_i_365\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(6),
      I1 => \caracter_reg[3]_i_126_0\(1),
      O => \caracter[1]_i_365_n_0\
    );
\caracter[1]_i_366\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(1),
      I1 => \caracter_reg[3]_i_126_0\(6),
      I2 => \caracter_reg[3]_i_126_0\(0),
      I3 => \caracter_reg[3]_i_126_0\(5),
      O => \caracter[1]_i_366_n_0\
    );
\caracter[1]_i_367\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(0),
      I1 => \caracter_reg[3]_i_126_0\(5),
      I2 => \caracter_reg[3]_i_126_0\(7),
      O => \caracter[1]_i_367_n_0\
    );
\caracter[1]_i_368\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(6),
      I1 => \caracter_reg[3]_i_126_0\(4),
      O => \caracter[1]_i_368_n_0\
    );
\caracter[1]_i_369\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(5),
      I1 => \caracter_reg[3]_i_126_0\(3),
      O => \caracter[1]_i_369_n_0\
    );
\caracter[1]_i_37\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => p_1_in(1),
      I1 => \^disp_dinero[5]_0\(1),
      I2 => \caracter_reg[3]_i_126_0\(1),
      O => \caracter[1]_i_37_n_0\
    );
\caracter[1]_i_370\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(4),
      I1 => \caracter_reg[3]_i_126_0\(2),
      O => \caracter[1]_i_370_n_0\
    );
\caracter[1]_i_371\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(3),
      I1 => \caracter_reg[3]_i_126_0\(1),
      O => \caracter[1]_i_371_n_0\
    );
\caracter[1]_i_372\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(2),
      I1 => \caracter_reg[3]_i_126_0\(0),
      O => \caracter[1]_i_372_n_0\
    );
\caracter[1]_i_41\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0F1E00000F1EFFFF"
    )
        port map (
      I0 => \caracter_reg[1]_i_40_n_4\,
      I1 => \caracter_reg[1]_i_40_n_5\,
      I2 => \caracter_reg[1]_i_60_n_7\,
      I3 => \caracter[1]_i_42_n_0\,
      I4 => \^disp_dinero[5]_0\(1),
      I5 => \caracter_reg[3]_i_126_0\(4),
      O => \caracter[1]_i_41_n_0\
    );
\caracter[1]_i_42\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000005557FFFF"
    )
        port map (
      I0 => \caracter_reg[1]_i_60_n_6\,
      I1 => \caracter_reg[1]_i_60_n_7\,
      I2 => \caracter_reg[1]_i_40_n_4\,
      I3 => \caracter_reg[1]_i_40_n_5\,
      I4 => \caracter_reg[1]_i_60_n_5\,
      I5 => \caracter_reg[1]_i_60_n_4\,
      O => \caracter[1]_i_42_n_0\
    );
\caracter[1]_i_43\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E1E1E1E5A5A5A5A5"
    )
        port map (
      I0 => \caracter_reg[1]_i_60_n_4\,
      I1 => \caracter_reg[1]_i_60_n_5\,
      I2 => \caracter_reg[1]_i_40_n_5\,
      I3 => \caracter_reg[1]_i_40_n_4\,
      I4 => \caracter_reg[1]_i_60_n_7\,
      I5 => \caracter_reg[1]_i_60_n_6\,
      O => aD2M4dsP(2)
    );
\caracter[1]_i_45\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter_reg[1]_i_44_n_7\,
      I1 => \caracter_reg[1]_i_44_n_5\,
      O => \caracter[1]_i_45_n_0\
    );
\caracter[1]_i_46\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter_reg[1]_i_44_n_6\,
      I1 => \caracter_reg[1]_i_39_n_4\,
      O => \caracter[1]_i_46_n_0\
    );
\caracter[1]_i_48\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"32B380FE80FE32B3"
    )
        port map (
      I0 => \caracter_reg[1]_i_75_n_4\,
      I1 => \caracter_reg[1]_i_76_n_3\,
      I2 => \caracter_reg[1]_i_77_n_4\,
      I3 => \caracter_reg[1]_i_78_n_1\,
      I4 => \caracter_reg[1]_i_79_n_7\,
      I5 => \caracter_reg[1]_i_80_n_7\,
      O => \caracter[1]_i_48_n_0\
    );
\caracter[1]_i_49\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"32B380FE80FE32B3"
    )
        port map (
      I0 => \caracter_reg[1]_i_75_n_5\,
      I1 => \caracter_reg[1]_i_76_n_3\,
      I2 => \caracter_reg[1]_i_77_n_5\,
      I3 => \caracter_reg[1]_i_78_n_1\,
      I4 => \caracter_reg[1]_i_75_n_4\,
      I5 => \caracter_reg[1]_i_77_n_4\,
      O => \caracter[1]_i_49_n_0\
    );
\caracter[1]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0082228000000000"
    )
        port map (
      I0 => Q(0),
      I1 => \caracter_reg[1]_i_15_n_4\,
      I2 => \caracter_reg[1]_i_15_n_5\,
      I3 => \caracter_reg[1]_i_15_n_6\,
      I4 => \caracter_reg[1]_i_16_n_7\,
      I5 => \caracter_reg[1]\,
      O => \reg_reg[0]_0\
    );
\caracter[1]_i_50\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"32B380FE80FE32B3"
    )
        port map (
      I0 => \caracter_reg[1]_i_75_n_6\,
      I1 => \caracter_reg[1]_i_76_n_3\,
      I2 => \caracter_reg[1]_i_77_n_6\,
      I3 => \caracter_reg[1]_i_78_n_1\,
      I4 => \caracter_reg[1]_i_75_n_5\,
      I5 => \caracter_reg[1]_i_77_n_5\,
      O => \caracter[1]_i_50_n_0\
    );
\caracter[1]_i_51\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"69FF217B217B0069"
    )
        port map (
      I0 => \caracter_reg[1]_i_75_n_6\,
      I1 => \caracter_reg[1]_i_76_n_3\,
      I2 => \caracter_reg[1]_i_77_n_6\,
      I3 => \caracter_reg[1]_i_78_n_1\,
      I4 => \caracter_reg[1]_i_77_n_7\,
      I5 => \caracter_reg[1]_i_75_n_7\,
      O => \caracter[1]_i_51_n_0\
    );
\caracter[1]_i_52\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9A5965A665A69A59"
    )
        port map (
      I0 => \caracter[1]_i_48_n_0\,
      I1 => \caracter_reg[1]_i_79_n_7\,
      I2 => \caracter_reg[1]_i_76_n_3\,
      I3 => \caracter_reg[1]_i_80_n_7\,
      I4 => \caracter_reg[1]_i_78_n_1\,
      I5 => \caracter[1]_i_81_n_0\,
      O => \caracter[1]_i_52_n_0\
    );
\caracter[1]_i_53\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9A5965A665A69A59"
    )
        port map (
      I0 => \caracter[1]_i_49_n_0\,
      I1 => \caracter_reg[1]_i_75_n_4\,
      I2 => \caracter_reg[1]_i_76_n_3\,
      I3 => \caracter_reg[1]_i_77_n_4\,
      I4 => \caracter_reg[1]_i_78_n_1\,
      I5 => \caracter[1]_i_82_n_0\,
      O => \caracter[1]_i_53_n_0\
    );
\caracter[1]_i_54\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9A5965A665A69A59"
    )
        port map (
      I0 => \caracter[1]_i_50_n_0\,
      I1 => \caracter_reg[1]_i_75_n_5\,
      I2 => \caracter_reg[1]_i_76_n_3\,
      I3 => \caracter_reg[1]_i_77_n_5\,
      I4 => \caracter_reg[1]_i_78_n_1\,
      I5 => \caracter[1]_i_83_n_0\,
      O => \caracter[1]_i_54_n_0\
    );
\caracter[1]_i_55\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9A5965A665A69A59"
    )
        port map (
      I0 => \caracter[1]_i_51_n_0\,
      I1 => \caracter_reg[1]_i_75_n_6\,
      I2 => \caracter_reg[1]_i_76_n_3\,
      I3 => \caracter_reg[1]_i_77_n_6\,
      I4 => \caracter_reg[1]_i_78_n_1\,
      I5 => \caracter[1]_i_84_n_0\,
      O => \caracter[1]_i_55_n_0\
    );
\caracter[1]_i_56\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(3),
      I1 => \caracter_reg[1]_i_85_n_6\,
      O => \caracter[1]_i_56_n_0\
    );
\caracter[1]_i_57\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(2),
      I1 => \caracter_reg[1]_i_85_n_7\,
      O => \caracter[1]_i_57_n_0\
    );
\caracter[1]_i_58\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(1),
      O => \caracter[1]_i_58_n_0\
    );
\caracter[1]_i_59\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(0),
      O => \caracter[1]_i_59_n_0\
    );
\caracter[1]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00C8000000000000"
    )
        port map (
      I0 => \caracter[3]_i_9_n_0\,
      I1 => \caracter[1]_i_18_n_0\,
      I2 => \caracter[1]_i_19_n_0\,
      I3 => \caracter[3]_i_10_n_0\,
      I4 => \caracter_reg[1]_0\,
      I5 => \caracter_reg[1]_1\,
      O => \reg_reg[0]_1\
    );
\caracter[1]_i_61\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"80FE32B332B380FE"
    )
        port map (
      I0 => \caracter_reg[1]_i_79_n_6\,
      I1 => \caracter_reg[1]_i_76_n_3\,
      I2 => \caracter_reg[1]_i_80_n_6\,
      I3 => \caracter_reg[1]_i_78_n_1\,
      I4 => \caracter_reg[1]_i_79_n_1\,
      I5 => \caracter_reg[1]_i_80_n_5\,
      O => \caracter[1]_i_61_n_0\
    );
\caracter[1]_i_62\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"32B380FE80FE32B3"
    )
        port map (
      I0 => \caracter_reg[1]_i_79_n_7\,
      I1 => \caracter_reg[1]_i_76_n_3\,
      I2 => \caracter_reg[1]_i_80_n_7\,
      I3 => \caracter_reg[1]_i_78_n_1\,
      I4 => \caracter_reg[1]_i_79_n_6\,
      I5 => \caracter_reg[1]_i_80_n_6\,
      O => \caracter[1]_i_62_n_0\
    );
\caracter[1]_i_63\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAA995A9955555"
    )
        port map (
      I0 => \caracter_reg[1]_i_90_n_7\,
      I1 => \caracter_reg[1]_i_80_n_5\,
      I2 => \caracter_reg[1]_i_76_n_3\,
      I3 => \caracter_reg[1]_i_79_n_1\,
      I4 => \caracter_reg[1]_i_80_n_4\,
      I5 => \caracter_reg[1]_i_78_n_1\,
      O => \caracter[1]_i_63_n_0\
    );
\caracter[1]_i_64\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"566AA995A995566A"
    )
        port map (
      I0 => \caracter[1]_i_61_n_0\,
      I1 => \caracter_reg[1]_i_79_n_1\,
      I2 => \caracter_reg[1]_i_76_n_3\,
      I3 => \caracter_reg[1]_i_80_n_5\,
      I4 => \caracter_reg[1]_i_78_n_1\,
      I5 => \caracter_reg[1]_i_80_n_4\,
      O => \caracter[1]_i_64_n_0\
    );
\caracter[1]_i_65\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9A5965A665A69A59"
    )
        port map (
      I0 => \caracter[1]_i_62_n_0\,
      I1 => \caracter_reg[1]_i_79_n_6\,
      I2 => \caracter_reg[1]_i_76_n_3\,
      I3 => \caracter_reg[1]_i_80_n_6\,
      I4 => \caracter_reg[1]_i_78_n_1\,
      I5 => \caracter[1]_i_91_n_0\,
      O => \caracter[1]_i_65_n_0\
    );
\caracter[1]_i_67\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BB2B2B22"
    )
        port map (
      I0 => \caracter[1]_i_101_n_0\,
      I1 => \caracter_reg[1]_i_78_n_1\,
      I2 => \caracter_reg[1]_i_102_n_3\,
      I3 => \caracter_reg[1]_i_103_n_4\,
      I4 => \caracter_reg[1]_i_104_n_4\,
      O => \caracter[1]_i_67_n_0\
    );
\caracter[1]_i_68\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7DD7355335531441"
    )
        port map (
      I0 => \caracter_reg[1]_i_78_n_1\,
      I1 => \caracter_reg[1]_i_102_n_3\,
      I2 => \caracter_reg[1]_i_103_n_4\,
      I3 => \caracter_reg[1]_i_104_n_4\,
      I4 => \caracter_reg[1]_i_103_n_5\,
      I5 => \caracter_reg[1]_i_104_n_5\,
      O => \caracter[1]_i_68_n_0\
    );
\caracter[1]_i_69\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7DD7355335531441"
    )
        port map (
      I0 => \caracter_reg[1]_i_78_n_1\,
      I1 => \caracter_reg[1]_i_102_n_3\,
      I2 => \caracter_reg[1]_i_103_n_5\,
      I3 => \caracter_reg[1]_i_104_n_5\,
      I4 => \caracter_reg[1]_i_103_n_6\,
      I5 => \caracter_reg[1]_i_104_n_6\,
      O => \caracter[1]_i_69_n_0\
    );
\caracter[1]_i_70\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7DD7355335531441"
    )
        port map (
      I0 => \caracter_reg[1]_i_78_n_1\,
      I1 => \caracter_reg[1]_i_102_n_3\,
      I2 => \caracter_reg[1]_i_103_n_6\,
      I3 => \caracter_reg[1]_i_104_n_6\,
      I4 => \caracter_reg[1]_i_103_n_7\,
      I5 => \caracter[1]_i_105_n_0\,
      O => \caracter[1]_i_70_n_0\
    );
\caracter[1]_i_71\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A665599A599AA665"
    )
        port map (
      I0 => \caracter[1]_i_67_n_0\,
      I1 => \caracter_reg[1]_i_76_n_3\,
      I2 => \caracter_reg[1]_i_77_n_7\,
      I3 => \caracter_reg[1]_i_75_n_7\,
      I4 => \caracter_reg[1]_i_78_n_1\,
      I5 => \caracter[1]_i_106_n_0\,
      O => \caracter[1]_i_71_n_0\
    );
\caracter[1]_i_72\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A665599A599AA665"
    )
        port map (
      I0 => \caracter[1]_i_68_n_0\,
      I1 => \caracter_reg[1]_i_102_n_3\,
      I2 => \caracter_reg[1]_i_103_n_4\,
      I3 => \caracter_reg[1]_i_104_n_4\,
      I4 => \caracter_reg[1]_i_78_n_1\,
      I5 => \caracter[1]_i_101_n_0\,
      O => \caracter[1]_i_72_n_0\
    );
\caracter[1]_i_73\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"599AA665A665599A"
    )
        port map (
      I0 => \caracter[1]_i_69_n_0\,
      I1 => \caracter_reg[1]_i_102_n_3\,
      I2 => \caracter_reg[1]_i_103_n_5\,
      I3 => \caracter_reg[1]_i_104_n_5\,
      I4 => \caracter_reg[1]_i_78_n_1\,
      I5 => \caracter[1]_i_107_n_0\,
      O => \caracter[1]_i_73_n_0\
    );
\caracter[1]_i_74\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"599AA665A665599A"
    )
        port map (
      I0 => \caracter[1]_i_70_n_0\,
      I1 => \caracter_reg[1]_i_102_n_3\,
      I2 => \caracter_reg[1]_i_103_n_6\,
      I3 => \caracter_reg[1]_i_104_n_6\,
      I4 => \caracter_reg[1]_i_78_n_1\,
      I5 => \caracter[1]_i_108_n_0\,
      O => \caracter[1]_i_74_n_0\
    );
\caracter[1]_i_81\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[1]_i_80_n_6\,
      I1 => \caracter_reg[1]_i_76_n_3\,
      I2 => \caracter_reg[1]_i_79_n_6\,
      O => \caracter[1]_i_81_n_0\
    );
\caracter[1]_i_82\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[1]_i_80_n_7\,
      I1 => \caracter_reg[1]_i_76_n_3\,
      I2 => \caracter_reg[1]_i_79_n_7\,
      O => \caracter[1]_i_82_n_0\
    );
\caracter[1]_i_83\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[1]_i_77_n_4\,
      I1 => \caracter_reg[1]_i_76_n_3\,
      I2 => \caracter_reg[1]_i_75_n_4\,
      O => \caracter[1]_i_83_n_0\
    );
\caracter[1]_i_84\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[1]_i_77_n_5\,
      I1 => \caracter_reg[1]_i_76_n_3\,
      I2 => \caracter_reg[1]_i_75_n_5\,
      O => \caracter[1]_i_84_n_0\
    );
\caracter[1]_i_86\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(7),
      I1 => \caracter_reg[1]_i_150_n_5\,
      O => \caracter[1]_i_86_n_0\
    );
\caracter[1]_i_87\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(6),
      I1 => \caracter_reg[1]_i_150_n_6\,
      O => \caracter[1]_i_87_n_0\
    );
\caracter[1]_i_88\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(5),
      I1 => \caracter_reg[1]_i_150_n_7\,
      O => \caracter[1]_i_88_n_0\
    );
\caracter[1]_i_89\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(4),
      I1 => \caracter_reg[1]_i_85_n_5\,
      O => \caracter[1]_i_89_n_0\
    );
\caracter[1]_i_91\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[1]_i_80_n_5\,
      I1 => \caracter_reg[1]_i_76_n_3\,
      I2 => \caracter_reg[1]_i_79_n_1\,
      O => \caracter[1]_i_91_n_0\
    );
\caracter[1]_i_93\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"517520FB20FB5175"
    )
        port map (
      I0 => \caracter_reg[1]_i_102_n_3\,
      I1 => \caracter[1]_i_36_n_0\,
      I2 => \caracter_reg[1]_i_161_n_4\,
      I3 => \caracter_reg[1]_i_78_n_1\,
      I4 => \caracter_reg[1]_i_103_n_7\,
      I5 => \caracter[1]_i_105_n_0\,
      O => \caracter[1]_i_93_n_0\
    );
\caracter[1]_i_94\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"20FB0B2F0B2F20FB"
    )
        port map (
      I0 => \caracter_reg[1]_i_161_n_5\,
      I1 => \caracter[1]_i_37_n_0\,
      I2 => \caracter_reg[1]_i_102_n_3\,
      I3 => \caracter_reg[1]_i_78_n_1\,
      I4 => \caracter_reg[1]_i_161_n_4\,
      I5 => \caracter[1]_i_36_n_0\,
      O => \caracter[1]_i_94_n_0\
    );
\caracter[1]_i_95\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"20FB5175517520FB"
    )
        port map (
      I0 => \caracter_reg[1]_i_102_n_3\,
      I1 => \^total_string_reg[0]\,
      I2 => \caracter_reg[1]_i_161_n_6\,
      I3 => \caracter_reg[1]_i_78_n_1\,
      I4 => \caracter[1]_i_37_n_0\,
      I5 => \caracter_reg[1]_i_161_n_5\,
      O => \caracter[1]_i_95_n_0\
    );
\caracter[1]_i_96\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"82BE8228"
    )
        port map (
      I0 => \caracter_reg[1]_i_78_n_6\,
      I1 => \caracter_reg[1]_i_161_n_6\,
      I2 => \^total_string_reg[0]\,
      I3 => \caracter_reg[1]_i_102_n_3\,
      I4 => \caracter_reg[1]_i_161_n_7\,
      O => \caracter[1]_i_96_n_0\
    );
\caracter[1]_i_97\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \caracter[1]_i_93_n_0\,
      I1 => \caracter[1]_i_162_n_0\,
      I2 => \caracter_reg[1]_i_78_n_1\,
      I3 => \caracter_reg[1]_i_102_n_3\,
      I4 => \caracter_reg[1]_i_103_n_6\,
      I5 => \caracter_reg[1]_i_104_n_6\,
      O => \caracter[1]_i_97_n_0\
    );
\caracter[1]_i_98\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"95A96A566A5695A9"
    )
        port map (
      I0 => \caracter[1]_i_94_n_0\,
      I1 => \caracter_reg[1]_i_102_n_3\,
      I2 => \caracter[1]_i_36_n_0\,
      I3 => \caracter_reg[1]_i_161_n_4\,
      I4 => \caracter_reg[1]_i_78_n_1\,
      I5 => \caracter[1]_i_163_n_0\,
      O => \caracter[1]_i_98_n_0\
    );
\caracter[1]_i_99\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \caracter[1]_i_95_n_0\,
      I1 => \caracter_reg[1]_i_161_n_4\,
      I2 => \caracter[1]_i_36_n_0\,
      I3 => \caracter_reg[1]_i_102_n_3\,
      I4 => \caracter_reg[1]_i_78_n_1\,
      I5 => \caracter[1]_i_164_n_0\,
      O => \caracter[1]_i_99_n_0\
    );
\caracter[2]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"5559AAA6"
    )
        port map (
      I0 => \caracter_reg[3]_i_21_n_7\,
      I1 => \caracter[2]_i_21_n_0\,
      I2 => \caracter_reg[2]_i_22_n_4\,
      I3 => \caracter_reg[2]_i_22_n_5\,
      I4 => \caracter[3]_i_23_n_0\,
      O => \caracter[2]_i_10_n_0\
    );
\caracter[2]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"111FE0E0107A8580"
    )
        port map (
      I0 => \caracter_reg[3]_i_21_n_7\,
      I1 => \caracter[3]_i_22_n_0\,
      I2 => \caracter_reg[3]_i_21_n_4\,
      I3 => \caracter_reg[3]_i_21_n_5\,
      I4 => \caracter_reg[3]_i_21_n_6\,
      I5 => \caracter[3]_i_23_n_0\,
      O => \caracter[2]_i_11_n_0\
    );
\caracter[2]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"03FF03FAE8000000"
    )
        port map (
      I0 => \caracter[3]_i_23_n_0\,
      I1 => \caracter[3]_i_22_n_0\,
      I2 => \caracter_reg[3]_i_21_n_7\,
      I3 => \caracter_reg[3]_i_21_n_6\,
      I4 => \caracter_reg[3]_i_21_n_4\,
      I5 => \caracter_reg[3]_i_21_n_5\,
      O => \caracter[2]_i_12_n_0\
    );
\caracter[2]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9696969669699669"
    )
        port map (
      I0 => \^caracter_reg[1]_i_15_0\,
      I1 => \caracter[3]_i_18_n_0\,
      I2 => \caracter[3]_i_19_n_0\,
      I3 => \caracter[2]_i_23_n_0\,
      I4 => \caracter[2]_i_24_n_0\,
      I5 => \caracter[3]_i_9_n_0\,
      O => \caracter[2]_i_13_n_0\
    );
\caracter[2]_i_21\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAAAAAAAAA"
    )
        port map (
      I0 => \caracter_reg[3]_i_21_n_4\,
      I1 => \caracter_reg[3]_i_21_n_5\,
      I2 => \caracter_reg[2]_i_22_n_4\,
      I3 => \caracter_reg[2]_i_22_n_5\,
      I4 => \caracter_reg[3]_i_21_n_7\,
      I5 => \caracter_reg[3]_i_21_n_6\,
      O => \caracter[2]_i_21_n_0\
    );
\caracter[2]_i_23\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1428"
    )
        port map (
      I0 => \string_cent_decenas[1]5\(1),
      I1 => \string_cent_decenas[1]5\(0),
      I2 => \^total_string_reg[0]\,
      I3 => \caracter[0]_i_25_n_0\,
      O => \caracter[2]_i_23_n_0\
    );
\caracter[2]_i_24\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A6656559599A9AA6"
    )
        port map (
      I0 => \caracter[3]_i_30_n_0\,
      I1 => \string_cent_decenas[1]5\(1),
      I2 => \caracter[3]_i_28_n_0\,
      I3 => \caracter[3]_i_29_n_0\,
      I4 => \caracter_reg[1]_i_15_n_6\,
      I5 => \caracter_reg[1]_i_15_n_5\,
      O => \caracter[2]_i_24_n_0\
    );
\caracter[2]_i_25\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9000000990009090"
    )
        port map (
      I0 => \caracter[2]_i_16\,
      I1 => Q(0),
      I2 => \caracter_reg[1]_i_15_n_5\,
      I3 => \caracter_reg[1]_i_15_n_6\,
      I4 => \caracter_reg[1]_i_15_n_4\,
      I5 => \caracter_reg[1]_i_16_n_7\,
      O => \charact_dot_reg[0]_0\
    );
\caracter[2]_i_27\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[1]_i_35_n_0\,
      O => cent(3)
    );
\caracter[2]_i_28\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[1]_i_36_n_0\,
      O => cent(2)
    );
\caracter[2]_i_29\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[1]_i_37_n_0\,
      O => cent(1)
    );
\caracter[2]_i_30\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^total_string_reg[0]\,
      O => \disp_dinero[2]_0\(0)
    );
\caracter[2]_i_31\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[1]_i_35_n_0\,
      I1 => \caracter_reg[3]_i_45_n_6\,
      O => \caracter[2]_i_31_n_0\
    );
\caracter[2]_i_32\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[1]_i_36_n_0\,
      I1 => \caracter_reg[3]_i_45_n_7\,
      O => \caracter[2]_i_32_n_0\
    );
\caracter[2]_i_33\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => p_1_in(1),
      I1 => \^disp_dinero[5]_0\(1),
      I2 => \caracter_reg[3]_i_126_0\(1),
      O => \caracter[2]_i_33_n_0\
    );
\caracter[2]_i_34\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => p_1_in(0),
      I1 => \^disp_dinero[5]_0\(1),
      I2 => \caracter_reg[3]_i_126_0\(0),
      O => \caracter[2]_i_34_n_0\
    );
\caracter[2]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00C4CC0000C00800"
    )
        port map (
      I0 => \caracter[3]_i_9_n_0\,
      I1 => \caracter_reg[2]\,
      I2 => \caracter[2]_i_10_n_0\,
      I3 => \caracter[2]_i_11_n_0\,
      I4 => \caracter[2]_i_12_n_0\,
      I5 => \caracter[2]_i_13_n_0\,
      O => \charact_dot_reg[0]_1\
    );
\caracter[2]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF88888880"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(5),
      I1 => \caracter_reg[3]_i_126_0\(6),
      I2 => \caracter_reg[3]_i_126_0\(3),
      I3 => \caracter_reg[3]_i_126_0\(4),
      I4 => \caracter_reg[3]_i_126_0\(2),
      I5 => \caracter_reg[3]_i_126_0\(7),
      O => \^disp_dinero[5]_0\(1)
    );
\caracter[3]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1180FA001100E000"
    )
        port map (
      I0 => \caracter_reg[3]_i_21_n_7\,
      I1 => \caracter[3]_i_22_n_0\,
      I2 => \caracter_reg[3]_i_21_n_4\,
      I3 => \caracter_reg[3]_i_21_n_5\,
      I4 => \caracter_reg[3]_i_21_n_6\,
      I5 => \caracter[3]_i_23_n_0\,
      O => \caracter[3]_i_10_n_0\
    );
\caracter[3]_i_100\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00B2B200B20000B2"
    )
        port map (
      I0 => \caracter_reg[3]_i_71_n_5\,
      I1 => \caracter[1]_i_36_n_0\,
      I2 => \caracter_reg[3]_i_94_n_6\,
      I3 => \caracter_reg[3]_i_71_n_0\,
      I4 => \caracter_reg[3]_i_108_n_7\,
      I5 => \caracter_reg[3]_i_94_n_5\,
      O => \caracter[3]_i_100_n_0\
    );
\caracter[3]_i_101\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6900696900006900"
    )
        port map (
      I0 => \caracter_reg[3]_i_94_n_6\,
      I1 => \caracter[1]_i_36_n_0\,
      I2 => \caracter_reg[3]_i_71_n_5\,
      I3 => \caracter_reg[3]_i_71_n_6\,
      I4 => \caracter[1]_i_37_n_0\,
      I5 => \caracter_reg[3]_i_94_n_7\,
      O => \caracter[3]_i_101_n_0\
    );
\caracter[3]_i_102\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00B2B200B20000B2"
    )
        port map (
      I0 => \caracter_reg[3]_i_71_n_7\,
      I1 => \^total_string_reg[0]\,
      I2 => \caracter_reg[3]_i_126_n_4\,
      I3 => \caracter_reg[3]_i_94_n_7\,
      I4 => \caracter[1]_i_37_n_0\,
      I5 => \caracter_reg[3]_i_71_n_6\,
      O => \caracter[3]_i_102_n_0\
    );
\caracter[3]_i_103\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08808008"
    )
        port map (
      I0 => \caracter_reg[3]_i_88_n_4\,
      I1 => \caracter_reg[3]_i_126_n_5\,
      I2 => \caracter_reg[3]_i_126_n_4\,
      I3 => \^total_string_reg[0]\,
      I4 => \caracter_reg[3]_i_71_n_7\,
      O => \caracter[3]_i_103_n_0\
    );
\caracter[3]_i_104\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6996996699669669"
    )
        port map (
      I0 => \caracter[3]_i_100_n_0\,
      I1 => \caracter_reg[3]_i_108_n_6\,
      I2 => \caracter_reg[3]_i_71_n_0\,
      I3 => \caracter_reg[3]_i_94_n_4\,
      I4 => \caracter_reg[3]_i_108_n_7\,
      I5 => \caracter_reg[3]_i_94_n_5\,
      O => \caracter[3]_i_104_n_0\
    );
\caracter[3]_i_105\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E78E1871"
    )
        port map (
      I0 => \caracter[3]_i_144_n_0\,
      I1 => \caracter_reg[3]_i_71_n_5\,
      I2 => \caracter[1]_i_36_n_0\,
      I3 => \caracter_reg[3]_i_94_n_6\,
      I4 => \caracter[3]_i_145_n_0\,
      O => \caracter[3]_i_105_n_0\
    );
\caracter[3]_i_106\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A5965A6"
    )
        port map (
      I0 => \caracter[3]_i_102_n_0\,
      I1 => \caracter_reg[3]_i_94_n_7\,
      I2 => \caracter[1]_i_37_n_0\,
      I3 => \caracter_reg[3]_i_71_n_6\,
      I4 => \caracter[3]_i_146_n_0\,
      O => \caracter[3]_i_106_n_0\
    );
\caracter[3]_i_107\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F87F80F807807F07"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_n_5\,
      I1 => \caracter_reg[3]_i_88_n_4\,
      I2 => \caracter_reg[3]_i_71_n_7\,
      I3 => \^total_string_reg[0]\,
      I4 => \caracter_reg[3]_i_126_n_4\,
      I5 => \caracter[3]_i_147_n_0\,
      O => \caracter[3]_i_107_n_0\
    );
\caracter[3]_i_109\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[1]_i_35_n_0\,
      I1 => \caracter[3]_i_43_n_0\,
      O => \caracter[3]_i_109_n_0\
    );
\caracter[3]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B42B2BD3AD4A4AB6"
    )
        port map (
      I0 => \caracter[0]_i_11_n_0\,
      I1 => \caracter[3]_i_16_n_0\,
      I2 => \caracter[3]_i_20_n_0\,
      I3 => \caracter[3]_i_23_n_0\,
      I4 => \caracter[3]_i_17_n_0\,
      I5 => \caracter[2]_i_13_n_0\,
      O => \caracter[3]_i_11_n_0\
    );
\caracter[3]_i_110\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[1]_i_36_n_0\,
      I1 => \caracter[1]_i_41_n_0\,
      O => \caracter[3]_i_110_n_0\
    );
\caracter[3]_i_111\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[1]_i_35_n_0\,
      I1 => \caracter[1]_i_37_n_0\,
      O => \caracter[3]_i_111_n_0\
    );
\caracter[3]_i_112\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[1]_i_35_n_0\,
      I1 => \caracter[1]_i_37_n_0\,
      O => \caracter[3]_i_112_n_0\
    );
\caracter[3]_i_113\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1EE1"
    )
        port map (
      I0 => \caracter[3]_i_43_n_0\,
      I1 => \caracter[1]_i_35_n_0\,
      I2 => \caracter[1]_i_41_n_0\,
      I3 => \caracter[3]_i_42_n_0\,
      O => \caracter[3]_i_113_n_0\
    );
\caracter[3]_i_114\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1EE1"
    )
        port map (
      I0 => \caracter[1]_i_41_n_0\,
      I1 => \caracter[1]_i_36_n_0\,
      I2 => \caracter[1]_i_35_n_0\,
      I3 => \caracter[3]_i_43_n_0\,
      O => \caracter[3]_i_114_n_0\
    );
\caracter[3]_i_115\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1EE1"
    )
        port map (
      I0 => \caracter[1]_i_37_n_0\,
      I1 => \caracter[1]_i_35_n_0\,
      I2 => \caracter[1]_i_41_n_0\,
      I3 => \caracter[1]_i_36_n_0\,
      O => \caracter[3]_i_115_n_0\
    );
\caracter[3]_i_116\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6669"
    )
        port map (
      I0 => \caracter[1]_i_37_n_0\,
      I1 => \caracter[1]_i_35_n_0\,
      I2 => \caracter[1]_i_36_n_0\,
      I3 => \^total_string_reg[0]\,
      O => \caracter[3]_i_116_n_0\
    );
\caracter[3]_i_118\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8E"
    )
        port map (
      I0 => \caracter[3]_i_42_n_0\,
      I1 => \caracter[1]_i_41_n_0\,
      I2 => \caracter[1]_i_36_n_0\,
      O => \caracter[3]_i_118_n_0\
    );
\caracter[3]_i_119\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8E"
    )
        port map (
      I0 => \caracter[1]_i_35_n_0\,
      I1 => \caracter[3]_i_43_n_0\,
      I2 => \caracter[1]_i_37_n_0\,
      O => \caracter[3]_i_119_n_0\
    );
\caracter[3]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"44AAAAD4D4AAAADD"
    )
        port map (
      I0 => \caracter[0]_i_11_n_0\,
      I1 => \caracter[0]_i_10_n_0\,
      I2 => \caracter[3]_i_24_n_0\,
      I3 => \caracter[2]_i_13_n_0\,
      I4 => \caracter[3]_i_25_n_0\,
      I5 => \caracter[3]_i_26_n_0\,
      O => \caracter[3]_i_12_n_0\
    );
\caracter[3]_i_120\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8E"
    )
        port map (
      I0 => \caracter[1]_i_41_n_0\,
      I1 => \caracter[1]_i_36_n_0\,
      I2 => \^total_string_reg[0]\,
      O => \caracter[3]_i_120_n_0\
    );
\caracter[3]_i_121\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter[1]_i_41_n_0\,
      I1 => \caracter[1]_i_36_n_0\,
      I2 => \^total_string_reg[0]\,
      O => \caracter[3]_i_121_n_0\
    );
\caracter[3]_i_122\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"D42B2BD4"
    )
        port map (
      I0 => \caracter[1]_i_36_n_0\,
      I1 => \caracter[1]_i_41_n_0\,
      I2 => \caracter[3]_i_42_n_0\,
      I3 => \caracter[3]_i_43_n_0\,
      I4 => \caracter[1]_i_35_n_0\,
      O => \caracter[3]_i_122_n_0\
    );
\caracter[3]_i_123\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"D42B2BD42BD4D42B"
    )
        port map (
      I0 => \caracter[1]_i_37_n_0\,
      I1 => \caracter[3]_i_43_n_0\,
      I2 => \caracter[1]_i_35_n_0\,
      I3 => \caracter[1]_i_41_n_0\,
      I4 => \caracter[1]_i_36_n_0\,
      I5 => \caracter[3]_i_42_n_0\,
      O => \caracter[3]_i_123_n_0\
    );
\caracter[3]_i_124\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter[3]_i_120_n_0\,
      I1 => \caracter[1]_i_35_n_0\,
      I2 => \caracter[1]_i_37_n_0\,
      I3 => \caracter[3]_i_43_n_0\,
      O => \caracter[3]_i_124_n_0\
    );
\caracter[3]_i_125\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696969"
    )
        port map (
      I0 => \caracter[1]_i_41_n_0\,
      I1 => \caracter[1]_i_36_n_0\,
      I2 => \^total_string_reg[0]\,
      I3 => \caracter[1]_i_37_n_0\,
      I4 => \caracter[1]_i_35_n_0\,
      O => \caracter[3]_i_125_n_0\
    );
\caracter[3]_i_127\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[1]_i_41_n_0\,
      O => cent(4)
    );
\caracter[3]_i_128\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[1]_i_35_n_0\,
      O => \caracter[3]_i_128_n_0\
    );
\caracter[3]_i_129\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[1]_i_36_n_0\,
      O => \caracter[3]_i_129_n_0\
    );
\caracter[3]_i_130\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[1]_i_37_n_0\,
      O => \caracter[3]_i_130_n_0\
    );
\caracter[3]_i_131\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0F1E00000F1EFFFF"
    )
        port map (
      I0 => \caracter_reg[1]_i_40_n_4\,
      I1 => \caracter_reg[1]_i_40_n_5\,
      I2 => \caracter_reg[1]_i_60_n_7\,
      I3 => \caracter[1]_i_42_n_0\,
      I4 => \^disp_dinero[5]_0\(1),
      I5 => \caracter_reg[3]_i_126_0\(4),
      O => \caracter[3]_i_131_n_0\
    );
\caracter[3]_i_132\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter[1]_i_35_n_0\,
      I1 => \caracter[3]_i_42_n_0\,
      O => \caracter[3]_i_132_n_0\
    );
\caracter[3]_i_133\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter[1]_i_36_n_0\,
      I1 => \caracter[3]_i_43_n_0\,
      O => \caracter[3]_i_133_n_0\
    );
\caracter[3]_i_134\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \caracter[1]_i_37_n_0\,
      I1 => \caracter[1]_i_41_n_0\,
      O => \caracter[3]_i_134_n_0\
    );
\caracter[3]_i_136\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0880"
    )
        port map (
      I0 => \caracter_reg[3]_i_88_n_5\,
      I1 => \caracter_reg[3]_i_126_n_6\,
      I2 => \caracter_reg[3]_i_88_n_4\,
      I3 => \caracter_reg[3]_i_126_n_5\,
      O => \caracter[3]_i_136_n_0\
    );
\caracter[3]_i_137\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"2828BE28"
    )
        port map (
      I0 => \caracter_reg[3]_i_173_n_2\,
      I1 => \caracter_reg[3]_i_88_n_5\,
      I2 => \caracter_reg[3]_i_126_n_6\,
      I3 => \caracter_reg[3]_i_88_n_6\,
      I4 => \^total_string_reg[0]\,
      O => \caracter[3]_i_137_n_0\
    );
\caracter[3]_i_138\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"82"
    )
        port map (
      I0 => \caracter_reg[3]_i_173_n_7\,
      I1 => \caracter_reg[3]_i_88_n_6\,
      I2 => \^total_string_reg[0]\,
      O => \caracter[3]_i_138_n_0\
    );
\caracter[3]_i_139\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[3]_i_174_n_4\,
      I1 => \caracter_reg[3]_i_88_n_7\,
      O => \caracter[3]_i_139_n_0\
    );
\caracter[3]_i_14\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"444A"
    )
        port map (
      I0 => \caracter_reg[1]_i_15_n_4\,
      I1 => \caracter_reg[1]_i_16_n_7\,
      I2 => \caracter_reg[1]_i_15_n_6\,
      I3 => \caracter_reg[1]_i_15_n_5\,
      O => \^caracter_reg[1]_i_15_0\
    );
\caracter[3]_i_140\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F087870F"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_n_6\,
      I1 => \caracter_reg[3]_i_88_n_5\,
      I2 => \caracter[3]_i_175_n_0\,
      I3 => \caracter_reg[3]_i_88_n_4\,
      I4 => \caracter_reg[3]_i_126_n_5\,
      O => \caracter[3]_i_140_n_0\
    );
\caracter[3]_i_141\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => \caracter[3]_i_137_n_0\,
      I1 => \caracter_reg[3]_i_126_n_5\,
      I2 => \caracter_reg[3]_i_88_n_4\,
      I3 => \caracter_reg[3]_i_126_n_6\,
      I4 => \caracter_reg[3]_i_88_n_5\,
      O => \caracter[3]_i_141_n_0\
    );
\caracter[3]_i_142\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6996699696696996"
    )
        port map (
      I0 => \caracter[3]_i_138_n_0\,
      I1 => \caracter_reg[3]_i_173_n_2\,
      I2 => \caracter_reg[3]_i_88_n_5\,
      I3 => \caracter_reg[3]_i_126_n_6\,
      I4 => \caracter_reg[3]_i_88_n_6\,
      I5 => \^total_string_reg[0]\,
      O => \caracter[3]_i_142_n_0\
    );
\caracter[3]_i_143\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter_reg[3]_i_173_n_7\,
      I1 => \caracter_reg[3]_i_88_n_6\,
      I2 => \^total_string_reg[0]\,
      I3 => \caracter[3]_i_139_n_0\,
      O => \caracter[3]_i_143_n_0\
    );
\caracter[3]_i_144\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \caracter_reg[3]_i_94_n_7\,
      I1 => \caracter[1]_i_37_n_0\,
      I2 => \caracter_reg[3]_i_71_n_6\,
      O => \caracter[3]_i_144_n_0\
    );
\caracter[3]_i_145\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[3]_i_94_n_5\,
      I1 => \caracter_reg[3]_i_108_n_7\,
      I2 => \caracter_reg[3]_i_71_n_0\,
      O => \caracter[3]_i_145_n_0\
    );
\caracter[3]_i_146\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \caracter_reg[3]_i_71_n_5\,
      I1 => \caracter[1]_i_36_n_0\,
      I2 => \caracter_reg[3]_i_94_n_6\,
      O => \caracter[3]_i_146_n_0\
    );
\caracter[3]_i_147\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[3]_i_71_n_6\,
      I1 => \caracter[1]_i_37_n_0\,
      I2 => \caracter_reg[3]_i_94_n_7\,
      O => \caracter[3]_i_147_n_0\
    );
\caracter[3]_i_148\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[3]_i_42_n_0\,
      O => \caracter[3]_i_148_n_0\
    );
\caracter[3]_i_149\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[3]_i_43_n_0\,
      O => \caracter[3]_i_149_n_0\
    );
\caracter[3]_i_150\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[1]_i_41_n_0\,
      O => \caracter[3]_i_150_n_0\
    );
\caracter[3]_i_151\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^total_string_reg[0]\,
      I1 => \caracter[1]_i_36_n_0\,
      I2 => \caracter[3]_i_42_n_0\,
      O => \caracter[3]_i_151_n_0\
    );
\caracter[3]_i_152\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[3]_i_43_n_0\,
      I1 => \caracter[1]_i_37_n_0\,
      O => \caracter[3]_i_152_n_0\
    );
\caracter[3]_i_153\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[1]_i_41_n_0\,
      I1 => \^total_string_reg[0]\,
      O => \caracter[3]_i_153_n_0\
    );
\caracter[3]_i_154\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[1]_i_35_n_0\,
      O => \caracter[3]_i_154_n_0\
    );
\caracter[3]_i_155\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^total_string_reg[0]\,
      O => \caracter[3]_i_155_n_0\
    );
\caracter[3]_i_156\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C3AAC3553C553CAA"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(0),
      I1 => p_1_in(0),
      I2 => p_1_in(1),
      I3 => \^disp_dinero[5]_0\(1),
      I4 => \caracter_reg[3]_i_126_0\(1),
      I5 => \caracter[1]_i_35_n_0\,
      O => \caracter[3]_i_156_n_0\
    );
\caracter[3]_i_157\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^total_string_reg[0]\,
      I1 => \caracter[1]_i_36_n_0\,
      O => \caracter[3]_i_157_n_0\
    );
\caracter[3]_i_158\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => p_1_in(1),
      I1 => \^disp_dinero[5]_0\(1),
      I2 => \caracter_reg[3]_i_126_0\(1),
      O => \caracter[3]_i_158_n_0\
    );
\caracter[3]_i_159\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^total_string_reg[0]\,
      O => \caracter[3]_i_159_n_0\
    );
\caracter[3]_i_16\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"010101FFFE00FE00"
    )
        port map (
      I0 => \caracter_reg[3]_i_21_n_7\,
      I1 => \caracter_reg[2]_i_22_n_5\,
      I2 => \caracter_reg[2]_i_22_n_4\,
      I3 => \caracter_reg[3]_i_21_n_4\,
      I4 => \caracter_reg[3]_i_21_n_5\,
      I5 => \caracter_reg[3]_i_21_n_6\,
      O => \caracter[3]_i_16_n_0\
    );
\caracter[3]_i_160\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^total_string_reg[0]\,
      O => \caracter[3]_i_160_n_0\
    );
\caracter[3]_i_161\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^total_string_reg[0]\,
      I1 => \caracter[1]_i_35_n_0\,
      O => \caracter[3]_i_161_n_0\
    );
\caracter[3]_i_162\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBBBB0BBB0BBB0B"
    )
        port map (
      I0 => aD2M4dsP(2),
      I1 => \^disp_dinero[5]_0\(1),
      I2 => \caracter_reg[3]_i_126_0\(2),
      I3 => \caracter_reg[3]_i_126_0\(7),
      I4 => \caracter_reg[3]_i_126_0\(6),
      I5 => \caracter_reg[3]_i_126_0\(5),
      O => \caracter[3]_i_162_n_0\
    );
\caracter[3]_i_163\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => p_1_in(1),
      I1 => \^disp_dinero[5]_0\(1),
      I2 => \caracter_reg[3]_i_126_0\(1),
      O => \caracter[3]_i_163_n_0\
    );
\caracter[3]_i_164\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^total_string_reg[0]\,
      O => \caracter[3]_i_164_n_0\
    );
\caracter[3]_i_165\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[3]_i_174_n_5\,
      I1 => \caracter_reg[3]_i_117_n_4\,
      O => \caracter[3]_i_165_n_0\
    );
\caracter[3]_i_166\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[3]_i_174_n_6\,
      I1 => \caracter_reg[3]_i_117_n_5\,
      O => \caracter[3]_i_166_n_0\
    );
\caracter[3]_i_167\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \caracter_reg[3]_i_174_n_7\,
      I1 => \caracter_reg[3]_i_117_n_6\,
      O => \caracter[3]_i_167_n_0\
    );
\caracter[3]_i_168\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \caracter_reg[3]_i_176_n_4\,
      I1 => \^total_string_reg[0]\,
      O => \caracter[3]_i_168_n_0\
    );
\caracter[3]_i_169\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => \caracter_reg[3]_i_174_n_4\,
      I1 => \caracter_reg[3]_i_88_n_7\,
      I2 => \caracter_reg[3]_i_117_n_4\,
      I3 => \caracter_reg[3]_i_174_n_5\,
      O => \caracter[3]_i_169_n_0\
    );
\caracter[3]_i_17\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFF1000AFFF5000A"
    )
        port map (
      I0 => \caracter_reg[3]_i_21_n_4\,
      I1 => \caracter_reg[3]_i_21_n_5\,
      I2 => \caracter_reg[2]_i_22_n_4\,
      I3 => \caracter_reg[2]_i_22_n_5\,
      I4 => \caracter_reg[3]_i_21_n_7\,
      I5 => \caracter_reg[3]_i_21_n_6\,
      O => \caracter[3]_i_17_n_0\
    );
\caracter[3]_i_170\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \caracter_reg[3]_i_117_n_5\,
      I1 => \caracter_reg[3]_i_174_n_6\,
      I2 => \caracter_reg[3]_i_117_n_4\,
      I3 => \caracter_reg[3]_i_174_n_5\,
      O => \caracter[3]_i_170_n_0\
    );
\caracter[3]_i_171\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \caracter_reg[3]_i_117_n_6\,
      I1 => \caracter_reg[3]_i_174_n_7\,
      I2 => \caracter_reg[3]_i_117_n_5\,
      I3 => \caracter_reg[3]_i_174_n_6\,
      O => \caracter[3]_i_171_n_0\
    );
\caracter[3]_i_172\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4BB4"
    )
        port map (
      I0 => \^total_string_reg[0]\,
      I1 => \caracter_reg[3]_i_176_n_4\,
      I2 => \caracter_reg[3]_i_117_n_6\,
      I3 => \caracter_reg[3]_i_174_n_7\,
      O => \caracter[3]_i_172_n_0\
    );
\caracter[3]_i_175\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \caracter_reg[3]_i_71_n_7\,
      I1 => \^total_string_reg[0]\,
      I2 => \caracter_reg[3]_i_126_n_4\,
      O => \caracter[3]_i_175_n_0\
    );
\caracter[3]_i_177\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[3]_i_42_n_0\,
      O => \caracter[3]_i_177_n_0\
    );
\caracter[3]_i_178\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[1]_i_37_n_0\,
      I1 => \caracter[3]_i_42_n_0\,
      O => \caracter[3]_i_178_n_0\
    );
\caracter[3]_i_179\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[3]_i_43_n_0\,
      O => \caracter[3]_i_179_n_0\
    );
\caracter[3]_i_18\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0FE10FE50FA50FA5"
    )
        port map (
      I0 => \caracter_reg[3]_i_21_n_4\,
      I1 => \caracter_reg[3]_i_21_n_5\,
      I2 => \caracter_reg[2]_i_22_n_4\,
      I3 => \caracter_reg[2]_i_22_n_5\,
      I4 => \caracter_reg[3]_i_21_n_7\,
      I5 => \caracter_reg[3]_i_21_n_6\,
      O => \caracter[3]_i_18_n_0\
    );
\caracter[3]_i_180\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[1]_i_41_n_0\,
      O => \caracter[3]_i_180_n_0\
    );
\caracter[3]_i_181\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[1]_i_35_n_0\,
      O => \caracter[3]_i_181_n_0\
    );
\caracter[3]_i_182\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1E"
    )
        port map (
      I0 => \caracter[3]_i_42_n_0\,
      I1 => \caracter[1]_i_37_n_0\,
      I2 => \caracter[1]_i_36_n_0\,
      O => \caracter[3]_i_182_n_0\
    );
\caracter[3]_i_184\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[3]_i_42_n_0\,
      I1 => \caracter[1]_i_37_n_0\,
      O => \caracter[3]_i_184_n_0\
    );
\caracter[3]_i_185\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[3]_i_43_n_0\,
      I1 => \^total_string_reg[0]\,
      O => \caracter[3]_i_185_n_0\
    );
\caracter[3]_i_186\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[3]_i_42_n_0\,
      O => \caracter[3]_i_186_n_0\
    );
\caracter[3]_i_187\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[3]_i_43_n_0\,
      O => \caracter[3]_i_187_n_0\
    );
\caracter[3]_i_188\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6669"
    )
        port map (
      I0 => \caracter[1]_i_37_n_0\,
      I1 => \caracter[3]_i_42_n_0\,
      I2 => \caracter[3]_i_43_n_0\,
      I3 => \^total_string_reg[0]\,
      O => \caracter[3]_i_188_n_0\
    );
\caracter[3]_i_189\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[3]_i_43_n_0\,
      I1 => \^total_string_reg[0]\,
      O => \caracter[3]_i_189_n_0\
    );
\caracter[3]_i_19\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2BBFFFF200022BB0"
    )
        port map (
      I0 => \string_cent_decenas[1]5\(1),
      I1 => \caracter[3]_i_28_n_0\,
      I2 => \caracter[3]_i_29_n_0\,
      I3 => \caracter_reg[1]_i_15_n_6\,
      I4 => \caracter_reg[1]_i_15_n_5\,
      I5 => \caracter[3]_i_30_n_0\,
      O => \caracter[3]_i_19_n_0\
    );
\caracter[3]_i_190\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[3]_i_42_n_0\,
      I1 => \caracter[1]_i_41_n_0\,
      O => \caracter[3]_i_190_n_0\
    );
\caracter[3]_i_191\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[3]_i_43_n_0\,
      I1 => \caracter[1]_i_35_n_0\,
      O => \caracter[3]_i_191_n_0\
    );
\caracter[3]_i_192\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[1]_i_41_n_0\,
      O => \caracter[3]_i_192_n_0\
    );
\caracter[3]_i_193\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[1]_i_35_n_0\,
      O => \caracter[3]_i_193_n_0\
    );
\caracter[3]_i_194\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[1]_i_36_n_0\,
      O => \caracter[3]_i_194_n_0\
    );
\caracter[3]_i_195\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[1]_i_36_n_0\,
      I1 => \caracter[1]_i_41_n_0\,
      O => \caracter[3]_i_195_n_0\
    );
\caracter[3]_i_196\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[1]_i_35_n_0\,
      I1 => \caracter[1]_i_37_n_0\,
      O => \caracter[3]_i_196_n_0\
    );
\caracter[3]_i_197\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[1]_i_36_n_0\,
      I1 => \^total_string_reg[0]\,
      O => \caracter[3]_i_197_n_0\
    );
\caracter[3]_i_198\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[1]_i_37_n_0\,
      O => \caracter[3]_i_198_n_0\
    );
\caracter[3]_i_20\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4A4A4A4A4A4A4AAA"
    )
        port map (
      I0 => \caracter_reg[3]_i_21_n_5\,
      I1 => \caracter_reg[3]_i_21_n_4\,
      I2 => \caracter_reg[3]_i_21_n_6\,
      I3 => \caracter_reg[3]_i_21_n_7\,
      I4 => \caracter_reg[2]_i_22_n_5\,
      I5 => \caracter_reg[2]_i_22_n_4\,
      O => \caracter[3]_i_20_n_0\
    );
\caracter[3]_i_22\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \caracter_reg[2]_i_22_n_4\,
      I1 => \caracter_reg[2]_i_22_n_5\,
      O => \caracter[3]_i_22_n_0\
    );
\caracter[3]_i_23\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"40D00D40F4FDDFF4"
    )
        port map (
      I0 => \caracter[3]_i_38_n_0\,
      I1 => \caracter[3]_i_39_n_0\,
      I2 => \caracter_reg[2]_i_22_n_4\,
      I3 => \caracter[2]_i_21_n_0\,
      I4 => \caracter_reg[2]_i_22_n_5\,
      I5 => \^caracter_reg[1]_i_15_0\,
      O => \caracter[3]_i_23_n_0\
    );
\caracter[3]_i_24\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7E577E56A86AA86B"
    )
        port map (
      I0 => \caracter[3]_i_16_n_0\,
      I1 => \caracter[3]_i_17_n_0\,
      I2 => \caracter[3]_i_23_n_0\,
      I3 => \caracter[3]_i_20_n_0\,
      I4 => \caracter[3]_i_40_n_0\,
      I5 => \caracter[3]_i_41_n_0\,
      O => \caracter[3]_i_24_n_0\
    );
\caracter[3]_i_25\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4DDFF333"
    )
        port map (
      I0 => \caracter[3]_i_41_n_0\,
      I1 => \caracter[3]_i_20_n_0\,
      I2 => \caracter[3]_i_23_n_0\,
      I3 => \caracter[3]_i_17_n_0\,
      I4 => \caracter[3]_i_16_n_0\,
      O => \caracter[3]_i_25_n_0\
    );
\caracter[3]_i_26\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A55A5AA5A5AAAA59"
    )
        port map (
      I0 => \caracter[3]_i_41_n_0\,
      I1 => \caracter[3]_i_40_n_0\,
      I2 => \caracter[3]_i_20_n_0\,
      I3 => \caracter[3]_i_23_n_0\,
      I4 => \caracter[3]_i_17_n_0\,
      I5 => \caracter[3]_i_16_n_0\,
      O => \caracter[3]_i_26_n_0\
    );
\caracter[3]_i_28\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00E2"
    )
        port map (
      I0 => \caracter_reg[3]_i_126_0\(0),
      I1 => \^disp_dinero[5]_0\(1),
      I2 => p_1_in(0),
      I3 => \string_cent_decenas[1]5\(0),
      O => \caracter[3]_i_28_n_0\
    );
\caracter[3]_i_29\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0057"
    )
        port map (
      I0 => \caracter_reg[1]_i_15_n_4\,
      I1 => \caracter_reg[1]_i_15_n_5\,
      I2 => \caracter_reg[1]_i_15_n_6\,
      I3 => \caracter_reg[1]_i_16_n_7\,
      O => \caracter[3]_i_29_n_0\
    );
\caracter[3]_i_30\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0F0F0F0F5A58F0F0"
    )
        port map (
      I0 => \caracter_reg[3]_i_21_n_6\,
      I1 => \caracter_reg[3]_i_21_n_7\,
      I2 => \caracter_reg[2]_i_22_n_5\,
      I3 => \caracter_reg[2]_i_22_n_4\,
      I4 => \caracter_reg[3]_i_21_n_5\,
      I5 => \caracter_reg[3]_i_21_n_4\,
      O => \caracter[3]_i_30_n_0\
    );
\caracter[3]_i_31\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[3]_i_42_n_0\,
      O => cent(6)
    );
\caracter[3]_i_32\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[3]_i_43_n_0\,
      O => cent(5)
    );
\caracter[3]_i_33\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[1]_i_41_n_0\,
      O => \caracter[3]_i_33_n_0\
    );
\caracter[3]_i_34\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter_reg[3]_i_44_n_5\,
      O => \caracter[3]_i_34_n_0\
    );
\caracter[3]_i_35\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[3]_i_42_n_0\,
      I1 => \caracter_reg[3]_i_44_n_6\,
      O => \caracter[3]_i_35_n_0\
    );
\caracter[3]_i_36\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[3]_i_43_n_0\,
      I1 => \caracter_reg[3]_i_44_n_7\,
      O => \caracter[3]_i_36_n_0\
    );
\caracter[3]_i_37\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter[1]_i_41_n_0\,
      I1 => \caracter_reg[3]_i_45_n_5\,
      O => \caracter[3]_i_37_n_0\
    );
\caracter[3]_i_38\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"32660000FFFF3266"
    )
        port map (
      I0 => \caracter_reg[1]_i_16_n_7\,
      I1 => \caracter_reg[1]_i_15_n_6\,
      I2 => \caracter_reg[1]_i_15_n_5\,
      I3 => \caracter_reg[1]_i_15_n_4\,
      I4 => \caracter[3]_i_28_n_0\,
      I5 => \string_cent_decenas[1]5\(1),
      O => \caracter[3]_i_38_n_0\
    );
\caracter[3]_i_39\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6675"
    )
        port map (
      I0 => \caracter_reg[1]_i_15_n_5\,
      I1 => \caracter_reg[1]_i_15_n_6\,
      I2 => \caracter_reg[1]_i_15_n_4\,
      I3 => \caracter_reg[1]_i_16_n_7\,
      O => \caracter[3]_i_39_n_0\
    );
\caracter[3]_i_40\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0110044002200880"
    )
        port map (
      I0 => \caracter[3]_i_39_n_0\,
      I1 => \caracter[0]_i_25_n_0\,
      I2 => \^total_string_reg[0]\,
      I3 => \string_cent_decenas[1]5\(0),
      I4 => \string_cent_decenas[1]5\(1),
      I5 => \caracter[3]_i_30_n_0\,
      O => \caracter[3]_i_40_n_0\
    );
\caracter[3]_i_41\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"D24BB4D22DB44B2D"
    )
        port map (
      I0 => \caracter[3]_i_39_n_0\,
      I1 => \caracter[3]_i_38_n_0\,
      I2 => \caracter_reg[2]_i_22_n_4\,
      I3 => \caracter[2]_i_21_n_0\,
      I4 => \caracter_reg[2]_i_22_n_5\,
      I5 => \^caracter_reg[1]_i_15_0\,
      O => \caracter[3]_i_41_n_0\
    );
\caracter[3]_i_42\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \caracter[3]_i_46_n_0\,
      I1 => \^disp_dinero[5]_0\(1),
      I2 => \caracter_reg[3]_i_126_0\(6),
      O => \caracter[3]_i_42_n_0\
    );
\caracter[3]_i_43\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \caracter[3]_i_47_n_0\,
      I1 => \^disp_dinero[5]_0\(1),
      I2 => \caracter_reg[3]_i_126_0\(5),
      O => \caracter[3]_i_43_n_0\
    );
\caracter[3]_i_46\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222CCCCCCCCC"
    )
        port map (
      I0 => \caracter_reg[1]_i_60_n_4\,
      I1 => \caracter_reg[1]_i_60_n_5\,
      I2 => \caracter_reg[1]_i_40_n_5\,
      I3 => \caracter_reg[1]_i_40_n_4\,
      I4 => \caracter_reg[1]_i_60_n_7\,
      I5 => \caracter_reg[1]_i_60_n_6\,
      O => \caracter[3]_i_46_n_0\
    );
\caracter[3]_i_47\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"555655560002AAAA"
    )
        port map (
      I0 => \caracter_reg[1]_i_60_n_6\,
      I1 => \caracter_reg[1]_i_60_n_7\,
      I2 => \caracter_reg[1]_i_40_n_4\,
      I3 => \caracter_reg[1]_i_40_n_5\,
      I4 => \caracter_reg[1]_i_60_n_5\,
      I5 => \caracter_reg[1]_i_60_n_4\,
      O => \caracter[3]_i_47_n_0\
    );
\caracter[3]_i_48\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \caracter_reg[3]_i_45_n_4\,
      I1 => \caracter_reg[3]_i_45_n_7\,
      O => \caracter[3]_i_48_n_0\
    );
\caracter[3]_i_49\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66969969"
    )
        port map (
      I0 => \caracter_reg[3]_i_60_n_6\,
      I1 => \caracter_reg[3]_i_45_n_5\,
      I2 => \caracter_reg[3]_i_60_n_7\,
      I3 => \caracter_reg[3]_i_45_n_6\,
      I4 => \caracter_reg[3]_i_45_n_7\,
      O => \caracter[3]_i_49_n_0\
    );
\caracter[3]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CDDC"
    )
        port map (
      I0 => \caracter[3]_i_9_n_0\,
      I1 => \caracter[3]_i_10_n_0\,
      I2 => \caracter[3]_i_11_n_0\,
      I3 => \caracter[3]_i_12_n_0\,
      O => \disp_dinero[3]_1\(0)
    );
\caracter[3]_i_50\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => \caracter_reg[3]_i_45_n_7\,
      I1 => \caracter_reg[3]_i_45_n_4\,
      I2 => \caracter_reg[3]_i_60_n_7\,
      I3 => \caracter_reg[3]_i_45_n_6\,
      O => \caracter[3]_i_50_n_0\
    );
\caracter[3]_i_51\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \caracter_reg[3]_i_45_n_4\,
      I1 => \caracter_reg[3]_i_45_n_7\,
      O => \caracter[3]_i_51_n_0\
    );
\caracter[3]_i_53\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"28"
    )
        port map (
      I0 => \caracter_reg[3]_i_70_n_1\,
      I1 => \caracter_reg[3]_i_71_n_0\,
      I2 => \caracter_reg[3]_i_72_n_1\,
      O => \caracter[3]_i_53_n_0\
    );
\caracter[3]_i_54\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \caracter_reg[3]_i_70_n_6\,
      I1 => \caracter_reg[3]_i_70_n_1\,
      I2 => \caracter_reg[3]_i_72_n_1\,
      I3 => \caracter_reg[3]_i_71_n_0\,
      O => \caracter[3]_i_54_n_0\
    );
\caracter[3]_i_55\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \caracter_reg[3]_i_70_n_7\,
      I1 => \caracter_reg[3]_i_70_n_6\,
      I2 => \caracter_reg[3]_i_72_n_1\,
      I3 => \caracter_reg[3]_i_71_n_0\,
      O => \caracter[3]_i_55_n_0\
    );
\caracter[3]_i_56\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \caracter_reg[3]_i_71_n_0\,
      I1 => \caracter_reg[3]_i_72_n_1\,
      O => \caracter[3]_i_56_n_0\
    );
\caracter[3]_i_57\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \caracter_reg[3]_i_70_n_1\,
      I1 => \caracter_reg[3]_i_72_n_1\,
      I2 => \caracter_reg[3]_i_71_n_0\,
      O => \caracter[3]_i_57_n_0\
    );
\caracter[3]_i_58\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A995"
    )
        port map (
      I0 => \caracter[3]_i_54_n_0\,
      I1 => \caracter_reg[3]_i_72_n_1\,
      I2 => \caracter_reg[3]_i_71_n_0\,
      I3 => \caracter_reg[3]_i_70_n_1\,
      O => \caracter[3]_i_58_n_0\
    );
\caracter[3]_i_59\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCC9C999"
    )
        port map (
      I0 => \caracter_reg[3]_i_70_n_6\,
      I1 => \caracter_reg[3]_i_70_n_1\,
      I2 => \caracter_reg[3]_i_72_n_1\,
      I3 => \caracter_reg[3]_i_71_n_0\,
      I4 => \caracter_reg[3]_i_70_n_7\,
      O => \caracter[3]_i_59_n_0\
    );
\caracter[3]_i_62\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \caracter_reg[3]_i_84_n_4\,
      I1 => \caracter_reg[3]_i_70_n_7\,
      I2 => \caracter_reg[3]_i_72_n_1\,
      I3 => \caracter_reg[3]_i_71_n_0\,
      O => \caracter[3]_i_62_n_0\
    );
\caracter[3]_i_63\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \caracter_reg[3]_i_84_n_5\,
      I1 => \caracter_reg[3]_i_84_n_4\,
      I2 => \caracter_reg[3]_i_72_n_1\,
      I3 => \caracter_reg[3]_i_71_n_0\,
      O => \caracter[3]_i_63_n_0\
    );
\caracter[3]_i_64\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \caracter_reg[3]_i_84_n_6\,
      I1 => \caracter_reg[3]_i_84_n_5\,
      I2 => \caracter_reg[3]_i_72_n_1\,
      I3 => \caracter_reg[3]_i_71_n_0\,
      O => \caracter[3]_i_64_n_0\
    );
\caracter[3]_i_65\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"044A"
    )
        port map (
      I0 => \caracter_reg[3]_i_84_n_6\,
      I1 => \caracter_reg[3]_i_84_n_7\,
      I2 => \caracter_reg[3]_i_71_n_0\,
      I3 => \caracter_reg[3]_i_72_n_1\,
      O => \caracter[3]_i_65_n_0\
    );
\caracter[3]_i_66\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0E1E1C3"
    )
        port map (
      I0 => \caracter_reg[3]_i_84_n_4\,
      I1 => \caracter_reg[3]_i_70_n_7\,
      I2 => \caracter_reg[3]_i_70_n_6\,
      I3 => \caracter_reg[3]_i_72_n_1\,
      I4 => \caracter_reg[3]_i_71_n_0\,
      O => \caracter[3]_i_66_n_0\
    );
\caracter[3]_i_67\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0E1E1C3"
    )
        port map (
      I0 => \caracter_reg[3]_i_84_n_5\,
      I1 => \caracter_reg[3]_i_84_n_4\,
      I2 => \caracter_reg[3]_i_70_n_7\,
      I3 => \caracter_reg[3]_i_72_n_1\,
      I4 => \caracter_reg[3]_i_71_n_0\,
      O => \caracter[3]_i_67_n_0\
    );
\caracter[3]_i_68\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0E1E1C3"
    )
        port map (
      I0 => \caracter_reg[3]_i_84_n_6\,
      I1 => \caracter_reg[3]_i_84_n_5\,
      I2 => \caracter_reg[3]_i_84_n_4\,
      I3 => \caracter_reg[3]_i_72_n_1\,
      I4 => \caracter_reg[3]_i_71_n_0\,
      O => \caracter[3]_i_68_n_0\
    );
\caracter[3]_i_69\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0E1E1C3"
    )
        port map (
      I0 => \caracter_reg[3]_i_84_n_7\,
      I1 => \caracter_reg[3]_i_84_n_6\,
      I2 => \caracter_reg[3]_i_84_n_5\,
      I3 => \caracter_reg[3]_i_72_n_1\,
      I4 => \caracter_reg[3]_i_71_n_0\,
      O => \caracter[3]_i_69_n_0\
    );
\caracter[3]_i_73\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \caracter_reg[3]_i_71_n_0\,
      I1 => \caracter_reg[3]_i_72_n_1\,
      O => \caracter[3]_i_73_n_0\
    );
\caracter[3]_i_74\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \caracter_reg[3]_i_71_n_0\,
      I1 => \caracter_reg[3]_i_72_n_1\,
      O => \caracter[3]_i_74_n_0\
    );
\caracter[3]_i_76\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"90660060"
    )
        port map (
      I0 => \caracter_reg[3]_i_72_n_1\,
      I1 => \caracter_reg[3]_i_84_n_7\,
      I2 => \caracter_reg[3]_i_108_n_4\,
      I3 => \caracter_reg[3]_i_71_n_0\,
      I4 => \caracter_reg[3]_i_72_n_6\,
      O => \caracter[3]_i_76_n_0\
    );
\caracter[3]_i_77\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60990090"
    )
        port map (
      I0 => \caracter_reg[3]_i_72_n_6\,
      I1 => \caracter_reg[3]_i_108_n_4\,
      I2 => \caracter_reg[3]_i_108_n_5\,
      I3 => \caracter_reg[3]_i_71_n_0\,
      I4 => \caracter_reg[3]_i_72_n_7\,
      O => \caracter[3]_i_77_n_0\
    );
\caracter[3]_i_78\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60990090"
    )
        port map (
      I0 => \caracter_reg[3]_i_72_n_7\,
      I1 => \caracter_reg[3]_i_108_n_5\,
      I2 => \caracter_reg[3]_i_108_n_6\,
      I3 => \caracter_reg[3]_i_71_n_0\,
      I4 => \caracter_reg[3]_i_94_n_4\,
      O => \caracter[3]_i_78_n_0\
    );
\caracter[3]_i_79\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08E0800E"
    )
        port map (
      I0 => \caracter_reg[3]_i_94_n_5\,
      I1 => \caracter_reg[3]_i_108_n_7\,
      I2 => \caracter_reg[3]_i_94_n_4\,
      I3 => \caracter_reg[3]_i_71_n_0\,
      I4 => \caracter_reg[3]_i_108_n_6\,
      O => \caracter[3]_i_79_n_0\
    );
\caracter[3]_i_80\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"566AA995"
    )
        port map (
      I0 => \caracter[3]_i_76_n_0\,
      I1 => \caracter_reg[3]_i_72_n_1\,
      I2 => \caracter_reg[3]_i_71_n_0\,
      I3 => \caracter_reg[3]_i_84_n_7\,
      I4 => \caracter_reg[3]_i_84_n_6\,
      O => \caracter[3]_i_80_n_0\
    );
\caracter[3]_i_81\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"95566AA96AA99556"
    )
        port map (
      I0 => \caracter[3]_i_77_n_0\,
      I1 => \caracter_reg[3]_i_72_n_6\,
      I2 => \caracter_reg[3]_i_71_n_0\,
      I3 => \caracter_reg[3]_i_108_n_4\,
      I4 => \caracter_reg[3]_i_84_n_7\,
      I5 => \caracter_reg[3]_i_72_n_1\,
      O => \caracter[3]_i_81_n_0\
    );
\caracter[3]_i_82\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AA9955695566AA9"
    )
        port map (
      I0 => \caracter[3]_i_78_n_0\,
      I1 => \caracter_reg[3]_i_72_n_7\,
      I2 => \caracter_reg[3]_i_71_n_0\,
      I3 => \caracter_reg[3]_i_108_n_5\,
      I4 => \caracter_reg[3]_i_108_n_4\,
      I5 => \caracter_reg[3]_i_72_n_6\,
      O => \caracter[3]_i_82_n_0\
    );
\caracter[3]_i_83\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AA9955695566AA9"
    )
        port map (
      I0 => \caracter[3]_i_79_n_0\,
      I1 => \caracter_reg[3]_i_94_n_4\,
      I2 => \caracter_reg[3]_i_71_n_0\,
      I3 => \caracter_reg[3]_i_108_n_6\,
      I4 => \caracter_reg[3]_i_108_n_5\,
      I5 => \caracter_reg[3]_i_72_n_7\,
      O => \caracter[3]_i_83_n_0\
    );
\caracter[3]_i_85\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[1]_i_41_n_0\,
      I1 => \caracter[3]_i_42_n_0\,
      O => \caracter[3]_i_85_n_0\
    );
\caracter[3]_i_86\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[3]_i_42_n_0\,
      O => \caracter[3]_i_86_n_0\
    );
\caracter[3]_i_87\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1E"
    )
        port map (
      I0 => \caracter[3]_i_42_n_0\,
      I1 => \caracter[1]_i_41_n_0\,
      I2 => \caracter[3]_i_43_n_0\,
      O => \caracter[3]_i_87_n_0\
    );
\caracter[3]_i_89\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \caracter[3]_i_42_n_0\,
      I1 => \caracter[1]_i_41_n_0\,
      O => \caracter[3]_i_89_n_0\
    );
\caracter[3]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFEFFFEEEF"
    )
        port map (
      I0 => \caracter[3]_i_16_n_0\,
      I1 => \caracter[3]_i_17_n_0\,
      I2 => \^caracter_reg[1]_i_15_0\,
      I3 => \caracter[3]_i_18_n_0\,
      I4 => \caracter[3]_i_19_n_0\,
      I5 => \caracter[3]_i_20_n_0\,
      O => \caracter[3]_i_9_n_0\
    );
\caracter[3]_i_90\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \caracter[3]_i_43_n_0\,
      I1 => \caracter[1]_i_35_n_0\,
      O => \caracter[3]_i_90_n_0\
    );
\caracter[3]_i_91\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \caracter[3]_i_46_n_0\,
      I1 => \^disp_dinero[5]_0\(1),
      I2 => \caracter_reg[3]_i_126_0\(6),
      O => \caracter[3]_i_91_n_0\
    );
\caracter[3]_i_92\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => \caracter[1]_i_41_n_0\,
      I1 => \caracter[3]_i_42_n_0\,
      I2 => \caracter[3]_i_43_n_0\,
      O => \caracter[3]_i_92_n_0\
    );
\caracter[3]_i_93\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B44B"
    )
        port map (
      I0 => \caracter[3]_i_43_n_0\,
      I1 => \caracter[1]_i_35_n_0\,
      I2 => \caracter[3]_i_42_n_0\,
      I3 => \caracter[1]_i_41_n_0\,
      O => \caracter[3]_i_93_n_0\
    );
\caracter[3]_i_95\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[3]_i_42_n_0\,
      O => \caracter[3]_i_95_n_0\
    );
\caracter[3]_i_96\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \caracter[3]_i_43_n_0\,
      O => \caracter[3]_i_96_n_0\
    );
\caracter[3]_i_97\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \caracter[3]_i_46_n_0\,
      I1 => \^disp_dinero[5]_0\(1),
      I2 => \caracter_reg[3]_i_126_0\(6),
      O => \caracter[3]_i_97_n_0\
    );
\caracter[3]_i_98\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \caracter[3]_i_47_n_0\,
      I1 => \^disp_dinero[5]_0\(1),
      I2 => \caracter_reg[3]_i_126_0\(5),
      O => \caracter[3]_i_98_n_0\
    );
\caracter_reg[0]_i_106\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_230_n_0\,
      CO(3) => \caracter_reg[0]_i_106_n_0\,
      CO(2) => \caracter_reg[0]_i_106_n_1\,
      CO(1) => \caracter_reg[0]_i_106_n_2\,
      CO(0) => \caracter_reg[0]_i_106_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => euros3(20 downto 17),
      S(3) => \caracter_reg[0]_i_22_n_3\,
      S(2) => \caracter_reg[0]_i_22_n_3\,
      S(1) => \caracter_reg[0]_i_22_n_3\,
      S(0) => \caracter_reg[0]_i_22_n_3\
    );
\caracter_reg[0]_i_111\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_231_n_0\,
      CO(3) => \caracter_reg[0]_i_111_n_0\,
      CO(2) => \caracter_reg[0]_i_111_n_1\,
      CO(1) => \caracter_reg[0]_i_111_n_2\,
      CO(0) => \caracter_reg[0]_i_111_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_232_n_0\,
      DI(2) => \caracter[0]_i_233_n_0\,
      DI(1) => \caracter[0]_i_234_n_0\,
      DI(0) => \caracter[0]_i_235_n_0\,
      O(3) => \caracter_reg[0]_i_111_n_4\,
      O(2) => \caracter_reg[0]_i_111_n_5\,
      O(1) => \caracter_reg[0]_i_111_n_6\,
      O(0) => \caracter_reg[0]_i_111_n_7\,
      S(3) => \caracter[0]_i_236_n_0\,
      S(2) => \caracter[0]_i_237_n_0\,
      S(1) => \caracter[0]_i_238_n_0\,
      S(0) => \caracter[0]_i_239_n_0\
    );
\caracter_reg[0]_i_120\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_240_n_0\,
      CO(3) => \caracter_reg[0]_i_120_n_0\,
      CO(2) => \caracter_reg[0]_i_120_n_1\,
      CO(1) => \caracter_reg[0]_i_120_n_2\,
      CO(0) => \caracter_reg[0]_i_120_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_241_n_0\,
      DI(2) => \caracter[0]_i_242_n_0\,
      DI(1) => \caracter[0]_i_243_n_0\,
      DI(0) => \caracter[0]_i_244_n_0\,
      O(3) => \caracter_reg[0]_i_120_n_4\,
      O(2) => \caracter_reg[0]_i_120_n_5\,
      O(1) => \caracter_reg[0]_i_120_n_6\,
      O(0) => \caracter_reg[0]_i_120_n_7\,
      S(3) => \caracter[0]_i_245_n_0\,
      S(2) => \caracter[0]_i_246_n_0\,
      S(1) => \caracter[0]_i_247_n_0\,
      S(0) => \caracter[0]_i_248_n_0\
    );
\caracter_reg[0]_i_130\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_253_n_0\,
      CO(3) => \caracter_reg[0]_i_130_n_0\,
      CO(2) => \caracter_reg[0]_i_130_n_1\,
      CO(1) => \caracter_reg[0]_i_130_n_2\,
      CO(0) => \caracter_reg[0]_i_130_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_254_n_0\,
      DI(2) => \caracter[0]_i_255_n_0\,
      DI(1) => \caracter[0]_i_256_n_0\,
      DI(0) => \caracter[0]_i_257_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[0]_i_130_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[0]_i_258_n_0\,
      S(2) => \caracter[0]_i_259_n_0\,
      S(1) => \caracter[0]_i_260_n_0\,
      S(0) => \caracter[0]_i_261_n_0\
    );
\caracter_reg[0]_i_139\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_262_n_0\,
      CO(3) => \caracter_reg[0]_i_139_n_0\,
      CO(2) => \caracter_reg[0]_i_139_n_1\,
      CO(1) => \caracter_reg[0]_i_139_n_2\,
      CO(0) => \caracter_reg[0]_i_139_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_274_n_0\,
      DI(2) => \caracter[0]_i_275_n_0\,
      DI(1) => \caracter[0]_i_276_n_0\,
      DI(0) => \caracter[0]_i_277_n_0\,
      O(3) => \caracter_reg[0]_i_139_n_4\,
      O(2) => \caracter_reg[0]_i_139_n_5\,
      O(1) => \caracter_reg[0]_i_139_n_6\,
      O(0) => \caracter_reg[0]_i_139_n_7\,
      S(3) => \caracter[0]_i_278_n_0\,
      S(2) => \caracter[0]_i_279_n_0\,
      S(1) => \caracter[0]_i_280_n_0\,
      S(0) => \caracter[0]_i_281_n_0\
    );
\caracter_reg[0]_i_142\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_265_n_0\,
      CO(3) => \caracter_reg[0]_i_142_n_0\,
      CO(2) => \caracter_reg[0]_i_142_n_1\,
      CO(1) => \caracter_reg[0]_i_142_n_2\,
      CO(0) => \caracter_reg[0]_i_142_n_3\,
      CYINIT => '0',
      DI(3) => euros2(8),
      DI(2) => \caracter[0]_i_283_n_0\,
      DI(1) => \caracter[0]_i_284_n_0\,
      DI(0) => \caracter[0]_i_285_n_0\,
      O(3) => \caracter_reg[0]_i_142_n_4\,
      O(2) => \caracter_reg[0]_i_142_n_5\,
      O(1) => \caracter_reg[0]_i_142_n_6\,
      O(0) => \caracter_reg[0]_i_142_n_7\,
      S(3) => \caracter[0]_i_286_n_0\,
      S(2) => \caracter[0]_i_287_n_0\,
      S(1) => \caracter[0]_i_288_n_0\,
      S(0) => \caracter[0]_i_289_n_0\
    );
\caracter_reg[0]_i_144\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_267_n_0\,
      CO(3) => \caracter_reg[0]_i_144_n_0\,
      CO(2) => \caracter_reg[0]_i_144_n_1\,
      CO(1) => \caracter_reg[0]_i_144_n_2\,
      CO(0) => \caracter_reg[0]_i_144_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_290_n_0\,
      DI(2) => \caracter[0]_i_291_n_0\,
      DI(1) => \caracter[0]_i_292_n_0\,
      DI(0) => \caracter[0]_i_293_n_0\,
      O(3) => \caracter_reg[0]_i_144_n_4\,
      O(2) => \caracter_reg[0]_i_144_n_5\,
      O(1) => \caracter_reg[0]_i_144_n_6\,
      O(0) => \caracter_reg[0]_i_144_n_7\,
      S(3) => \caracter[0]_i_294_n_0\,
      S(2) => \caracter[0]_i_295_n_0\,
      S(1) => \caracter[0]_i_296_n_0\,
      S(0) => \caracter[0]_i_297_n_0\
    );
\caracter_reg[0]_i_145\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_269_n_0\,
      CO(3) => \caracter_reg[0]_i_145_n_0\,
      CO(2) => \caracter_reg[0]_i_145_n_1\,
      CO(1) => \caracter_reg[0]_i_145_n_2\,
      CO(0) => \caracter_reg[0]_i_145_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_298_n_0\,
      DI(2) => \caracter[0]_i_299_n_0\,
      DI(1) => \caracter[0]_i_300_n_0\,
      DI(0) => \caracter[0]_i_301_n_0\,
      O(3) => \caracter_reg[0]_i_145_n_4\,
      O(2) => \caracter_reg[0]_i_145_n_5\,
      O(1) => \caracter_reg[0]_i_145_n_6\,
      O(0) => \caracter_reg[0]_i_145_n_7\,
      S(3) => \caracter[0]_i_302_n_0\,
      S(2) => \caracter[0]_i_303_n_0\,
      S(1) => \caracter[0]_i_304_n_0\,
      S(0) => \caracter[0]_i_305_n_0\
    );
\caracter_reg[0]_i_20\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_28_n_0\,
      CO(3 downto 1) => \NLW_caracter_reg[0]_i_20_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \caracter_reg[0]_i_20_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \caracter[0]_i_29_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[0]_i_20_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => B"000",
      S(0) => \caracter[0]_i_30_n_0\
    );
\caracter_reg[0]_i_21\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_31_n_0\,
      CO(3 downto 1) => \NLW_caracter_reg[0]_i_21_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \caracter_reg[0]_i_21_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 2) => \NLW_caracter_reg[0]_i_21_O_UNCONNECTED\(3 downto 2),
      O(1 downto 0) => euros3(30 downto 29),
      S(3 downto 2) => B"00",
      S(1) => \caracter_reg[0]_i_22_n_3\,
      S(0) => \caracter_reg[0]_i_22_n_3\
    );
\caracter_reg[0]_i_22\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_32_n_0\,
      CO(3 downto 1) => \NLW_caracter_reg[0]_i_22_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \caracter_reg[0]_i_22_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_caracter_reg[0]_i_22_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
\caracter_reg[0]_i_221\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_310_n_0\,
      CO(3) => \caracter_reg[0]_i_221_n_0\,
      CO(2) => \caracter_reg[0]_i_221_n_1\,
      CO(1) => \caracter_reg[0]_i_221_n_2\,
      CO(0) => \caracter_reg[0]_i_221_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_311_n_0\,
      DI(2) => \caracter[0]_i_312_n_0\,
      DI(1) => \caracter[0]_i_313_n_0\,
      DI(0) => \caracter[0]_i_314_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[0]_i_221_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[0]_i_315_n_0\,
      S(2) => \caracter[0]_i_316_n_0\,
      S(1) => \caracter[0]_i_317_n_0\,
      S(0) => \caracter[0]_i_318_n_0\
    );
\caracter_reg[0]_i_23\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_33_n_0\,
      CO(3 downto 1) => \NLW_caracter_reg[0]_i_23_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \caracter_reg[0]_i_23_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \caracter[0]_i_34_n_0\,
      O(3 downto 2) => \NLW_caracter_reg[0]_i_23_O_UNCONNECTED\(3 downto 2),
      O(1) => \caracter_reg[0]_i_23_n_6\,
      O(0) => \caracter_reg[0]_i_23_n_7\,
      S(3 downto 2) => B"00",
      S(1) => \caracter[0]_i_35_n_0\,
      S(0) => \caracter[0]_i_36_n_0\
    );
\caracter_reg[0]_i_230\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_306_n_0\,
      CO(3) => \caracter_reg[0]_i_230_n_0\,
      CO(2) => \caracter_reg[0]_i_230_n_1\,
      CO(1) => \caracter_reg[0]_i_230_n_2\,
      CO(0) => \caracter_reg[0]_i_230_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => euros3(16 downto 13),
      S(3) => \caracter_reg[0]_i_22_n_3\,
      S(2) => \caracter_reg[0]_i_22_n_3\,
      S(1) => \caracter_reg[0]_i_22_n_3\,
      S(0) => \caracter_reg[0]_i_22_n_3\
    );
\caracter_reg[0]_i_231\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_319_n_0\,
      CO(3) => \caracter_reg[0]_i_231_n_0\,
      CO(2) => \caracter_reg[0]_i_231_n_1\,
      CO(1) => \caracter_reg[0]_i_231_n_2\,
      CO(0) => \caracter_reg[0]_i_231_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_320_n_0\,
      DI(2) => \caracter[0]_i_321_n_0\,
      DI(1) => \caracter[0]_i_322_n_0\,
      DI(0) => \caracter[0]_i_323_n_0\,
      O(3) => \caracter_reg[0]_i_231_n_4\,
      O(2) => \caracter_reg[0]_i_231_n_5\,
      O(1) => \caracter_reg[0]_i_231_n_6\,
      O(0) => \caracter_reg[0]_i_231_n_7\,
      S(3) => \caracter[0]_i_324_n_0\,
      S(2) => \caracter[0]_i_325_n_0\,
      S(1) => \caracter[0]_i_326_n_0\,
      S(0) => \caracter[0]_i_327_n_0\
    );
\caracter_reg[0]_i_24\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_37_n_0\,
      CO(3) => \caracter_reg[0]_i_24_n_0\,
      CO(2) => \caracter_reg[0]_i_24_n_1\,
      CO(1) => \caracter_reg[0]_i_24_n_2\,
      CO(0) => \caracter_reg[0]_i_24_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_38_n_0\,
      DI(2) => \caracter[0]_i_39_n_0\,
      DI(1) => \caracter[0]_i_40_n_0\,
      DI(0) => \caracter[0]_i_41_n_0\,
      O(3) => \caracter_reg[0]_i_24_n_4\,
      O(2) => \caracter_reg[0]_i_24_n_5\,
      O(1) => \caracter_reg[0]_i_24_n_6\,
      O(0) => \caracter_reg[0]_i_24_n_7\,
      S(3) => \caracter[0]_i_42_n_0\,
      S(2) => \caracter[0]_i_43_n_0\,
      S(1) => \caracter[0]_i_44_n_0\,
      S(0) => \caracter[0]_i_45_n_0\
    );
\caracter_reg[0]_i_240\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_328_n_0\,
      CO(3) => \caracter_reg[0]_i_240_n_0\,
      CO(2) => \caracter_reg[0]_i_240_n_1\,
      CO(1) => \caracter_reg[0]_i_240_n_2\,
      CO(0) => \caracter_reg[0]_i_240_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_329_n_0\,
      DI(2) => \caracter[0]_i_330_n_0\,
      DI(1) => \caracter[0]_i_331_n_0\,
      DI(0) => \caracter[0]_i_332_n_0\,
      O(3) => \caracter_reg[0]_i_240_n_4\,
      O(2) => \caracter_reg[0]_i_240_n_5\,
      O(1) => \caracter_reg[0]_i_240_n_6\,
      O(0) => \caracter_reg[0]_i_240_n_7\,
      S(3) => \caracter[0]_i_333_n_0\,
      S(2) => \caracter[0]_i_334_n_0\,
      S(1) => \caracter[0]_i_335_n_0\,
      S(0) => \caracter[0]_i_336_n_0\
    );
\caracter_reg[0]_i_249\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_337_n_0\,
      CO(3) => \caracter_reg[0]_i_249_n_0\,
      CO(2) => \caracter_reg[0]_i_249_n_1\,
      CO(1) => \caracter_reg[0]_i_249_n_2\,
      CO(0) => \caracter_reg[0]_i_249_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_339_n_0\,
      DI(2) => \caracter[0]_i_340_n_0\,
      DI(1) => \caracter[0]_i_341_n_0\,
      DI(0) => \caracter[0]_i_342_n_0\,
      O(3) => \caracter_reg[0]_i_249_n_4\,
      O(2) => \caracter_reg[0]_i_249_n_5\,
      O(1) => \caracter_reg[0]_i_249_n_6\,
      O(0) => \caracter_reg[0]_i_249_n_7\,
      S(3) => \caracter[0]_i_343_n_0\,
      S(2) => \caracter[0]_i_344_n_0\,
      S(1) => \caracter[0]_i_345_n_0\,
      S(0) => \caracter[0]_i_346_n_0\
    );
\caracter_reg[0]_i_250\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_249_n_0\,
      CO(3) => \NLW_caracter_reg[0]_i_250_CO_UNCONNECTED\(3),
      CO(2) => \caracter_reg[0]_i_250_n_1\,
      CO(1) => \NLW_caracter_reg[0]_i_250_CO_UNCONNECTED\(1),
      CO(0) => \caracter_reg[0]_i_250_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \caracter[0]_i_347_n_0\,
      O(3 downto 2) => \NLW_caracter_reg[0]_i_250_O_UNCONNECTED\(3 downto 2),
      O(1) => \caracter_reg[0]_i_250_n_6\,
      O(0) => \caracter_reg[0]_i_250_n_7\,
      S(3 downto 2) => B"01",
      S(1) => euros2(30),
      S(0) => \caracter[0]_i_349_n_0\
    );
\caracter_reg[0]_i_251\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_350_n_0\,
      CO(3) => \caracter_reg[0]_i_251_n_0\,
      CO(2) => \NLW_caracter_reg[0]_i_251_CO_UNCONNECTED\(2),
      CO(1) => \caracter_reg[0]_i_251_n_2\,
      CO(0) => \caracter_reg[0]_i_251_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \caracter[0]_i_351_n_0\,
      DI(1) => \caracter[0]_i_352_n_0\,
      DI(0) => \caracter[0]_i_353_n_0\,
      O(3) => \NLW_caracter_reg[0]_i_251_O_UNCONNECTED\(3),
      O(2) => \caracter_reg[0]_i_251_n_5\,
      O(1) => \caracter_reg[0]_i_251_n_6\,
      O(0) => \caracter_reg[0]_i_251_n_7\,
      S(3) => '1',
      S(2) => \caracter[0]_i_354_n_0\,
      S(1) => \caracter[0]_i_355_n_0\,
      S(0) => \caracter[0]_i_356_n_0\
    );
\caracter_reg[0]_i_252\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_338_n_0\,
      CO(3) => \NLW_caracter_reg[0]_i_252_CO_UNCONNECTED\(3),
      CO(2) => \caracter_reg[0]_i_252_n_1\,
      CO(1) => \NLW_caracter_reg[0]_i_252_CO_UNCONNECTED\(1),
      CO(0) => \caracter_reg[0]_i_252_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \caracter[0]_i_357_n_0\,
      DI(0) => euros2(29),
      O(3 downto 2) => \NLW_caracter_reg[0]_i_252_O_UNCONNECTED\(3 downto 2),
      O(1) => \caracter_reg[0]_i_252_n_6\,
      O(0) => \caracter_reg[0]_i_252_n_7\,
      S(3 downto 2) => B"01",
      S(1) => \caracter[0]_i_359_n_0\,
      S(0) => \caracter[0]_i_360_n_0\
    );
\caracter_reg[0]_i_253\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_361_n_0\,
      CO(3) => \caracter_reg[0]_i_253_n_0\,
      CO(2) => \caracter_reg[0]_i_253_n_1\,
      CO(1) => \caracter_reg[0]_i_253_n_2\,
      CO(0) => \caracter_reg[0]_i_253_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_362_n_0\,
      DI(2) => \caracter[0]_i_363_n_0\,
      DI(1) => \caracter[0]_i_364_n_0\,
      DI(0) => \caracter[0]_i_365_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[0]_i_253_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[0]_i_366_n_0\,
      S(2) => \caracter[0]_i_367_n_0\,
      S(1) => \caracter[0]_i_368_n_0\,
      S(0) => \caracter[0]_i_369_n_0\
    );
\caracter_reg[0]_i_262\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[0]_i_262_n_0\,
      CO(2) => \caracter_reg[0]_i_262_n_1\,
      CO(1) => \caracter_reg[0]_i_262_n_2\,
      CO(0) => \caracter_reg[0]_i_262_n_3\,
      CYINIT => '0',
      DI(3) => euros2(6),
      DI(2) => \caracter[0]_i_379_n_0\,
      DI(1) => \caracter[0]_i_380_n_0\,
      DI(0) => '0',
      O(3) => \caracter_reg[0]_i_262_n_4\,
      O(2) => \caracter_reg[0]_i_262_n_5\,
      O(1) => \caracter_reg[0]_i_262_n_6\,
      O(0) => \caracter_reg[0]_i_262_n_7\,
      S(3) => \caracter[0]_i_381_n_0\,
      S(2) => \caracter[0]_i_382_n_0\,
      S(1) => \caracter[0]_i_383_n_0\,
      S(0) => \caracter[0]_i_384_n_0\
    );
\caracter_reg[0]_i_265\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_375_n_0\,
      CO(3) => \caracter_reg[0]_i_265_n_0\,
      CO(2) => \caracter_reg[0]_i_265_n_1\,
      CO(1) => \caracter_reg[0]_i_265_n_2\,
      CO(0) => \caracter_reg[0]_i_265_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_385_n_0\,
      DI(2) => \caracter[0]_i_386_n_0\,
      DI(1) => \caracter[0]_i_387_n_0\,
      DI(0) => \caracter[0]_i_388_n_0\,
      O(3) => \caracter_reg[0]_i_265_n_4\,
      O(2) => \caracter_reg[0]_i_265_n_5\,
      O(1) => \caracter_reg[0]_i_265_n_6\,
      O(0) => \caracter_reg[0]_i_265_n_7\,
      S(3) => \caracter[0]_i_389_n_0\,
      S(2) => \caracter[0]_i_390_n_0\,
      S(1) => \caracter[0]_i_391_n_0\,
      S(0) => \caracter[0]_i_392_n_0\
    );
\caracter_reg[0]_i_267\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_377_n_0\,
      CO(3) => \caracter_reg[0]_i_267_n_0\,
      CO(2) => \caracter_reg[0]_i_267_n_1\,
      CO(1) => \caracter_reg[0]_i_267_n_2\,
      CO(0) => \caracter_reg[0]_i_267_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_393_n_0\,
      DI(2) => \caracter[0]_i_394_n_0\,
      DI(1) => \caracter[0]_i_395_n_0\,
      DI(0) => \caracter[0]_i_396_n_0\,
      O(3) => \caracter_reg[0]_i_267_n_4\,
      O(2) => \caracter_reg[0]_i_267_n_5\,
      O(1) => \caracter_reg[0]_i_267_n_6\,
      O(0) => \caracter_reg[0]_i_267_n_7\,
      S(3) => \caracter[0]_i_397_n_0\,
      S(2) => \caracter[0]_i_398_n_0\,
      S(1) => \caracter[0]_i_399_n_0\,
      S(0) => \caracter[0]_i_400_n_0\
    );
\caracter_reg[0]_i_269\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_376_n_0\,
      CO(3) => \caracter_reg[0]_i_269_n_0\,
      CO(2) => \caracter_reg[0]_i_269_n_1\,
      CO(1) => \caracter_reg[0]_i_269_n_2\,
      CO(0) => \caracter_reg[0]_i_269_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_401_n_0\,
      DI(2) => \caracter[0]_i_402_n_0\,
      DI(1) => \caracter[0]_i_403_n_0\,
      DI(0) => \caracter[0]_i_404_n_0\,
      O(3) => \caracter_reg[0]_i_269_n_4\,
      O(2) => \caracter_reg[0]_i_269_n_5\,
      O(1) => \caracter_reg[0]_i_269_n_6\,
      O(0) => \caracter_reg[0]_i_269_n_7\,
      S(3) => \caracter[0]_i_405_n_0\,
      S(2) => \caracter[0]_i_406_n_0\,
      S(1) => \caracter[0]_i_407_n_0\,
      S(0) => \caracter[0]_i_408_n_0\
    );
\caracter_reg[0]_i_28\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_46_n_0\,
      CO(3) => \caracter_reg[0]_i_28_n_0\,
      CO(2) => \caracter_reg[0]_i_28_n_1\,
      CO(1) => \caracter_reg[0]_i_28_n_2\,
      CO(0) => \caracter_reg[0]_i_28_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_47_n_0\,
      DI(2) => \caracter[0]_i_48_n_0\,
      DI(1) => \caracter[0]_i_49_n_0\,
      DI(0) => \caracter[0]_i_50_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[0]_i_28_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[0]_i_51_n_0\,
      S(2) => \caracter[0]_i_52_n_0\,
      S(1) => \caracter[0]_i_53_n_0\,
      S(0) => \caracter[0]_i_54_n_0\
    );
\caracter_reg[0]_i_306\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_307_n_0\,
      CO(3) => \caracter_reg[0]_i_306_n_0\,
      CO(2) => \caracter_reg[0]_i_306_n_1\,
      CO(1) => \caracter_reg[0]_i_306_n_2\,
      CO(0) => \caracter_reg[0]_i_306_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => euros3(12 downto 9),
      S(3) => \caracter_reg[0]_i_22_n_3\,
      S(2) => \caracter_reg[0]_i_22_n_3\,
      S(1) => \caracter_reg[0]_i_22_n_3\,
      S(0) => \caracter_reg[0]_i_22_n_3\
    );
\caracter_reg[0]_i_307\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_308_n_0\,
      CO(3) => \caracter_reg[0]_i_307_n_0\,
      CO(2) => \caracter_reg[0]_i_307_n_1\,
      CO(1) => \caracter_reg[0]_i_307_n_2\,
      CO(0) => \caracter_reg[0]_i_307_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => euros3(8 downto 5),
      S(3) => \caracter_reg[0]_i_22_n_3\,
      S(2) => \caracter[0]_i_412_n_0\,
      S(1) => \caracter[0]_i_413_n_0\,
      S(0) => \caracter[0]_i_414_n_0\
    );
\caracter_reg[0]_i_308\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[0]_i_308_n_0\,
      CO(2) => \caracter_reg[0]_i_308_n_1\,
      CO(1) => \caracter_reg[0]_i_308_n_2\,
      CO(0) => \caracter_reg[0]_i_308_n_3\,
      CYINIT => \caracter[0]_i_415_n_0\,
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => euros3(4 downto 1),
      S(3) => \caracter[0]_i_416_n_0\,
      S(2) => \caracter[0]_i_417_n_0\,
      S(1) => \caracter[0]_i_418_n_0\,
      S(0) => \caracter[0]_i_419_n_0\
    );
\caracter_reg[0]_i_31\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_55_n_0\,
      CO(3) => \caracter_reg[0]_i_31_n_0\,
      CO(2) => \caracter_reg[0]_i_31_n_1\,
      CO(1) => \caracter_reg[0]_i_31_n_2\,
      CO(0) => \caracter_reg[0]_i_31_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => euros3(28 downto 25),
      S(3) => \caracter_reg[0]_i_22_n_3\,
      S(2) => \caracter_reg[0]_i_22_n_3\,
      S(1) => \caracter_reg[0]_i_22_n_3\,
      S(0) => \caracter_reg[0]_i_22_n_3\
    );
\caracter_reg[0]_i_310\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_420_n_0\,
      CO(3) => \caracter_reg[0]_i_310_n_0\,
      CO(2) => \caracter_reg[0]_i_310_n_1\,
      CO(1) => \caracter_reg[0]_i_310_n_2\,
      CO(0) => \caracter_reg[0]_i_310_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_421_n_0\,
      DI(2) => \caracter[0]_i_422_n_0\,
      DI(1) => \caracter[0]_i_423_n_0\,
      DI(0) => \caracter[0]_i_424_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[0]_i_310_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[0]_i_425_n_0\,
      S(2) => \caracter[0]_i_426_n_0\,
      S(1) => \caracter[0]_i_427_n_0\,
      S(0) => \caracter[0]_i_428_n_0\
    );
\caracter_reg[0]_i_319\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_429_n_0\,
      CO(3) => \caracter_reg[0]_i_319_n_0\,
      CO(2) => \caracter_reg[0]_i_319_n_1\,
      CO(1) => \caracter_reg[0]_i_319_n_2\,
      CO(0) => \caracter_reg[0]_i_319_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_430_n_0\,
      DI(2) => \caracter[0]_i_431_n_0\,
      DI(1) => \caracter[0]_i_432_n_0\,
      DI(0) => \caracter[0]_i_433_n_0\,
      O(3) => \caracter_reg[0]_i_319_n_4\,
      O(2) => \caracter_reg[0]_i_319_n_5\,
      O(1) => \caracter_reg[0]_i_319_n_6\,
      O(0) => \caracter_reg[0]_i_319_n_7\,
      S(3) => \caracter[0]_i_434_n_0\,
      S(2) => \caracter[0]_i_435_n_0\,
      S(1) => \caracter[0]_i_436_n_0\,
      S(0) => \caracter[0]_i_437_n_0\
    );
\caracter_reg[0]_i_32\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_56_n_0\,
      CO(3) => \caracter_reg[0]_i_32_n_0\,
      CO(2) => \caracter_reg[0]_i_32_n_1\,
      CO(1) => \caracter_reg[0]_i_32_n_2\,
      CO(0) => \caracter_reg[0]_i_32_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \caracter_reg[3]_i_126_0\(7 downto 4),
      O(3) => \caracter_reg[0]_i_32_n_4\,
      O(2) => \caracter_reg[0]_i_32_n_5\,
      O(1) => \caracter_reg[0]_i_32_n_6\,
      O(0) => \caracter_reg[0]_i_32_n_7\,
      S(3) => \caracter[0]_i_57_n_0\,
      S(2) => \caracter[0]_i_58_n_0\,
      S(1) => \caracter[0]_i_59_n_0\,
      S(0) => \caracter[0]_i_60_n_0\
    );
\caracter_reg[0]_i_328\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_438_n_0\,
      CO(3) => \caracter_reg[0]_i_328_n_0\,
      CO(2) => \caracter_reg[0]_i_328_n_1\,
      CO(1) => \caracter_reg[0]_i_328_n_2\,
      CO(0) => \caracter_reg[0]_i_328_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_439_n_0\,
      DI(2) => \caracter[0]_i_440_n_0\,
      DI(1) => \caracter[0]_i_441_n_0\,
      DI(0) => \caracter[0]_i_442_n_0\,
      O(3) => \caracter_reg[0]_i_328_n_4\,
      O(2) => \caracter_reg[0]_i_328_n_5\,
      O(1) => \caracter_reg[0]_i_328_n_6\,
      O(0) => \caracter_reg[0]_i_328_n_7\,
      S(3) => \caracter[0]_i_443_n_0\,
      S(2) => \caracter[0]_i_444_n_0\,
      S(1) => \caracter[0]_i_445_n_0\,
      S(0) => \caracter[0]_i_446_n_0\
    );
\caracter_reg[0]_i_33\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_61_n_0\,
      CO(3) => \caracter_reg[0]_i_33_n_0\,
      CO(2) => \caracter_reg[0]_i_33_n_1\,
      CO(1) => \caracter_reg[0]_i_33_n_2\,
      CO(0) => \caracter_reg[0]_i_33_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_62_n_0\,
      DI(2) => \caracter[0]_i_63_n_0\,
      DI(1) => \caracter[0]_i_64_n_0\,
      DI(0) => \caracter[0]_i_65_n_0\,
      O(3) => \caracter_reg[0]_i_33_n_4\,
      O(2) => \caracter_reg[0]_i_33_n_5\,
      O(1) => \caracter_reg[0]_i_33_n_6\,
      O(0) => \caracter_reg[0]_i_33_n_7\,
      S(3) => \caracter[0]_i_66_n_0\,
      S(2) => \caracter[0]_i_67_n_0\,
      S(1) => \caracter[0]_i_68_n_0\,
      S(0) => \caracter[0]_i_69_n_0\
    );
\caracter_reg[0]_i_337\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_447_n_0\,
      CO(3) => \caracter_reg[0]_i_337_n_0\,
      CO(2) => \caracter_reg[0]_i_337_n_1\,
      CO(1) => \caracter_reg[0]_i_337_n_2\,
      CO(0) => \caracter_reg[0]_i_337_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_452_n_0\,
      DI(2) => \caracter[0]_i_453_n_0\,
      DI(1) => \caracter[0]_i_454_n_0\,
      DI(0) => \caracter[0]_i_455_n_0\,
      O(3) => \caracter_reg[0]_i_337_n_4\,
      O(2) => \caracter_reg[0]_i_337_n_5\,
      O(1) => \caracter_reg[0]_i_337_n_6\,
      O(0) => \caracter_reg[0]_i_337_n_7\,
      S(3) => \caracter[0]_i_456_n_0\,
      S(2) => \caracter[0]_i_457_n_0\,
      S(1) => \caracter[0]_i_458_n_0\,
      S(0) => \caracter[0]_i_459_n_0\
    );
\caracter_reg[0]_i_338\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_448_n_0\,
      CO(3) => \caracter_reg[0]_i_338_n_0\,
      CO(2) => \caracter_reg[0]_i_338_n_1\,
      CO(1) => \caracter_reg[0]_i_338_n_2\,
      CO(0) => \caracter_reg[0]_i_338_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => euros2(28 downto 25),
      O(3) => \caracter_reg[0]_i_338_n_4\,
      O(2) => \caracter_reg[0]_i_338_n_5\,
      O(1) => \caracter_reg[0]_i_338_n_6\,
      O(0) => \caracter_reg[0]_i_338_n_7\,
      S(3) => \caracter[0]_i_464_n_0\,
      S(2) => \caracter[0]_i_465_n_0\,
      S(1) => \caracter[0]_i_466_n_0\,
      S(0) => \caracter[0]_i_467_n_0\
    );
\caracter_reg[0]_i_350\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_93_n_0\,
      CO(3) => \caracter_reg[0]_i_350_n_0\,
      CO(2) => \caracter_reg[0]_i_350_n_1\,
      CO(1) => \caracter_reg[0]_i_350_n_2\,
      CO(0) => \caracter_reg[0]_i_350_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_468_n_0\,
      DI(2) => \caracter[0]_i_469_n_0\,
      DI(1) => \caracter[0]_i_470_n_0\,
      DI(0) => \caracter[0]_i_471_n_0\,
      O(3) => \caracter_reg[0]_i_350_n_4\,
      O(2) => \caracter_reg[0]_i_350_n_5\,
      O(1) => \caracter_reg[0]_i_350_n_6\,
      O(0) => \caracter_reg[0]_i_350_n_7\,
      S(3) => \caracter[0]_i_472_n_0\,
      S(2) => \caracter[0]_i_473_n_0\,
      S(1) => \caracter[0]_i_474_n_0\,
      S(0) => \caracter[0]_i_475_n_0\
    );
\caracter_reg[0]_i_361\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[0]_i_361_n_0\,
      CO(2) => \caracter_reg[0]_i_361_n_1\,
      CO(1) => \caracter_reg[0]_i_361_n_2\,
      CO(0) => \caracter_reg[0]_i_361_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_476_n_0\,
      DI(2) => \caracter[0]_i_477_n_0\,
      DI(1) => \caracter[0]_i_478_n_0\,
      DI(0) => \caracter[0]_i_479_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[0]_i_361_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[0]_i_480_n_0\,
      S(2) => \caracter[0]_i_481_n_0\,
      S(1) => \caracter[0]_i_482_n_0\,
      S(0) => \caracter[0]_i_483_n_0\
    );
\caracter_reg[0]_i_37\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_72_n_0\,
      CO(3) => \caracter_reg[0]_i_37_n_0\,
      CO(2) => \caracter_reg[0]_i_37_n_1\,
      CO(1) => \caracter_reg[0]_i_37_n_2\,
      CO(0) => \caracter_reg[0]_i_37_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_73_n_0\,
      DI(2) => \caracter[0]_i_74_n_0\,
      DI(1) => \caracter[0]_i_75_n_0\,
      DI(0) => \caracter[0]_i_76_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[0]_i_37_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[0]_i_77_n_0\,
      S(2) => \caracter[0]_i_78_n_0\,
      S(1) => \caracter[0]_i_79_n_0\,
      S(0) => \caracter[0]_i_80_n_0\
    );
\caracter_reg[0]_i_375\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[0]_i_375_n_0\,
      CO(2) => \caracter_reg[0]_i_375_n_1\,
      CO(1) => \caracter_reg[0]_i_375_n_2\,
      CO(0) => \caracter_reg[0]_i_375_n_3\,
      CYINIT => '0',
      DI(3) => euros2(0),
      DI(2 downto 0) => B"001",
      O(3) => \caracter_reg[0]_i_375_n_4\,
      O(2) => \caracter_reg[0]_i_375_n_5\,
      O(1) => \caracter_reg[0]_i_375_n_6\,
      O(0) => \NLW_caracter_reg[0]_i_375_O_UNCONNECTED\(0),
      S(3) => \caracter[0]_i_486_n_0\,
      S(2) => \caracter[0]_i_487_n_0\,
      S(1) => \caracter[0]_i_488_n_0\,
      S(0) => euros2(0)
    );
\caracter_reg[0]_i_376\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_484_n_0\,
      CO(3) => \caracter_reg[0]_i_376_n_0\,
      CO(2) => \caracter_reg[0]_i_376_n_1\,
      CO(1) => \caracter_reg[0]_i_376_n_2\,
      CO(0) => \caracter_reg[0]_i_376_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_489_n_0\,
      DI(2) => \caracter[0]_i_490_n_0\,
      DI(1) => \caracter[0]_i_491_n_0\,
      DI(0) => \caracter[0]_i_492_n_0\,
      O(3) => \caracter_reg[0]_i_376_n_4\,
      O(2) => \caracter_reg[0]_i_376_n_5\,
      O(1) => \caracter_reg[0]_i_376_n_6\,
      O(0) => \caracter_reg[0]_i_376_n_7\,
      S(3) => \caracter[0]_i_493_n_0\,
      S(2) => \caracter[0]_i_494_n_0\,
      S(1) => \caracter[0]_i_495_n_0\,
      S(0) => \caracter[0]_i_496_n_0\
    );
\caracter_reg[0]_i_377\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_497_n_0\,
      CO(3) => \caracter_reg[0]_i_377_n_0\,
      CO(2) => \caracter_reg[0]_i_377_n_1\,
      CO(1) => \caracter_reg[0]_i_377_n_2\,
      CO(0) => \caracter_reg[0]_i_377_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_498_n_0\,
      DI(2) => \caracter[0]_i_499_n_0\,
      DI(1) => \caracter[0]_i_500_n_0\,
      DI(0) => \caracter[0]_i_501_n_0\,
      O(3) => \caracter_reg[0]_i_377_n_4\,
      O(2) => \caracter_reg[0]_i_377_n_5\,
      O(1) => \caracter_reg[0]_i_377_n_6\,
      O(0) => \caracter_reg[0]_i_377_n_7\,
      S(3) => \caracter[0]_i_502_n_0\,
      S(2) => \caracter[0]_i_503_n_0\,
      S(1) => \caracter[0]_i_504_n_0\,
      S(0) => \caracter[0]_i_505_n_0\
    );
\caracter_reg[0]_i_420\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_506_n_0\,
      CO(3) => \caracter_reg[0]_i_420_n_0\,
      CO(2) => \caracter_reg[0]_i_420_n_1\,
      CO(1) => \caracter_reg[0]_i_420_n_2\,
      CO(0) => \caracter_reg[0]_i_420_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_507_n_0\,
      DI(2) => \caracter[0]_i_508_n_0\,
      DI(1) => \caracter[0]_i_509_n_0\,
      DI(0) => \caracter[0]_i_510_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[0]_i_420_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[0]_i_511_n_0\,
      S(2) => \caracter[0]_i_512_n_0\,
      S(1) => \caracter[0]_i_513_n_0\,
      S(0) => \caracter[0]_i_514_n_0\
    );
\caracter_reg[0]_i_429\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[0]_i_429_n_0\,
      CO(2) => \caracter_reg[0]_i_429_n_1\,
      CO(1) => \caracter_reg[0]_i_429_n_2\,
      CO(0) => \caracter_reg[0]_i_429_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_515_n_0\,
      DI(2) => \caracter[0]_i_516_n_0\,
      DI(1) => \caracter[0]_i_517_n_0\,
      DI(0) => '0',
      O(3) => \caracter_reg[0]_i_429_n_4\,
      O(2) => \caracter_reg[0]_i_429_n_5\,
      O(1) => \caracter_reg[0]_i_429_n_6\,
      O(0) => \caracter_reg[0]_i_429_n_7\,
      S(3) => \caracter[0]_i_518_n_0\,
      S(2) => \caracter[0]_i_519_n_0\,
      S(1) => \caracter[0]_i_520_n_0\,
      S(0) => \caracter[0]_i_521_n_0\
    );
\caracter_reg[0]_i_438\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_24_n_0\,
      CO(3) => \caracter_reg[0]_i_438_n_0\,
      CO(2) => \caracter_reg[0]_i_438_n_1\,
      CO(1) => \caracter_reg[0]_i_438_n_2\,
      CO(0) => \caracter_reg[0]_i_438_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_522_n_0\,
      DI(2) => \caracter[0]_i_523_n_0\,
      DI(1) => \caracter[0]_i_524_n_0\,
      DI(0) => \caracter[0]_i_525_n_0\,
      O(3) => \caracter_reg[0]_i_438_n_4\,
      O(2) => \caracter_reg[0]_i_438_n_5\,
      O(1) => \caracter_reg[0]_i_438_n_6\,
      O(0) => \caracter_reg[0]_i_438_n_7\,
      S(3) => \caracter[0]_i_526_n_0\,
      S(2) => \caracter[0]_i_527_n_0\,
      S(1) => \caracter[0]_i_528_n_0\,
      S(0) => \caracter[0]_i_529_n_0\
    );
\caracter_reg[0]_i_447\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_530_n_0\,
      CO(3) => \caracter_reg[0]_i_447_n_0\,
      CO(2) => \caracter_reg[0]_i_447_n_1\,
      CO(1) => \caracter_reg[0]_i_447_n_2\,
      CO(0) => \caracter_reg[0]_i_447_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_541_n_0\,
      DI(2) => \caracter[0]_i_542_n_0\,
      DI(1) => \caracter[0]_i_543_n_0\,
      DI(0) => \caracter[0]_i_544_n_0\,
      O(3) => \caracter_reg[0]_i_447_n_4\,
      O(2) => \caracter_reg[0]_i_447_n_5\,
      O(1) => \caracter_reg[0]_i_447_n_6\,
      O(0) => \caracter_reg[0]_i_447_n_7\,
      S(3) => \caracter[0]_i_545_n_0\,
      S(2) => \caracter[0]_i_546_n_0\,
      S(1) => \caracter[0]_i_547_n_0\,
      S(0) => \caracter[0]_i_548_n_0\
    );
\caracter_reg[0]_i_448\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_533_n_0\,
      CO(3) => \caracter_reg[0]_i_448_n_0\,
      CO(2) => \caracter_reg[0]_i_448_n_1\,
      CO(1) => \caracter_reg[0]_i_448_n_2\,
      CO(0) => \caracter_reg[0]_i_448_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => euros2(24 downto 21),
      O(3) => \caracter_reg[0]_i_448_n_4\,
      O(2) => \caracter_reg[0]_i_448_n_5\,
      O(1) => \caracter_reg[0]_i_448_n_6\,
      O(0) => \caracter_reg[0]_i_448_n_7\,
      S(3) => \caracter[0]_i_553_n_0\,
      S(2) => \caracter[0]_i_554_n_0\,
      S(1) => \caracter[0]_i_555_n_0\,
      S(0) => \caracter[0]_i_556_n_0\
    );
\caracter_reg[0]_i_46\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_97_n_0\,
      CO(3) => \caracter_reg[0]_i_46_n_0\,
      CO(2) => \caracter_reg[0]_i_46_n_1\,
      CO(1) => \caracter_reg[0]_i_46_n_2\,
      CO(0) => \caracter_reg[0]_i_46_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_98_n_0\,
      DI(2) => \caracter[0]_i_99_n_0\,
      DI(1) => \caracter[0]_i_100_n_0\,
      DI(0) => \caracter[0]_i_101_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[0]_i_46_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[0]_i_102_n_0\,
      S(2) => \caracter[0]_i_103_n_0\,
      S(1) => \caracter[0]_i_104_n_0\,
      S(0) => \caracter[0]_i_105_n_0\
    );
\caracter_reg[0]_i_484\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_557_n_0\,
      CO(3) => \caracter_reg[0]_i_484_n_0\,
      CO(2) => \caracter_reg[0]_i_484_n_1\,
      CO(1) => \caracter_reg[0]_i_484_n_2\,
      CO(0) => \caracter_reg[0]_i_484_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_558_n_0\,
      DI(2) => \caracter[0]_i_559_n_0\,
      DI(1) => \caracter[0]_i_560_n_0\,
      DI(0) => \caracter[0]_i_561_n_0\,
      O(3) => \caracter_reg[0]_i_484_n_4\,
      O(2) => \caracter_reg[0]_i_484_n_5\,
      O(1) => \caracter_reg[0]_i_484_n_6\,
      O(0) => \caracter_reg[0]_i_484_n_7\,
      S(3) => \caracter[0]_i_562_n_0\,
      S(2) => \caracter[0]_i_563_n_0\,
      S(1) => \caracter[0]_i_564_n_0\,
      S(0) => \caracter[0]_i_565_n_0\
    );
\caracter_reg[0]_i_497\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[0]_i_497_n_0\,
      CO(2) => \caracter_reg[0]_i_497_n_1\,
      CO(1) => \caracter_reg[0]_i_497_n_2\,
      CO(0) => \caracter_reg[0]_i_497_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_415_n_0\,
      DI(2) => euros2(0),
      DI(1 downto 0) => B"01",
      O(3) => \caracter_reg[0]_i_497_n_4\,
      O(2) => \caracter_reg[0]_i_497_n_5\,
      O(1) => \caracter_reg[0]_i_497_n_6\,
      O(0) => \caracter_reg[0]_i_497_n_7\,
      S(3) => \caracter[0]_i_566_n_0\,
      S(2) => \caracter[0]_i_567_n_0\,
      S(1) => \caracter[0]_i_568_n_0\,
      S(0) => euros2(0)
    );
\caracter_reg[0]_i_506\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[0]_i_506_n_0\,
      CO(2) => \caracter_reg[0]_i_506_n_1\,
      CO(1) => \caracter_reg[0]_i_506_n_2\,
      CO(0) => \caracter_reg[0]_i_506_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_569_n_0\,
      DI(2) => \caracter[0]_i_570_n_0\,
      DI(1) => \caracter[0]_i_571_n_0\,
      DI(0) => '0',
      O(3 downto 0) => \NLW_caracter_reg[0]_i_506_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[0]_i_572_n_0\,
      S(2) => \caracter[0]_i_573_n_0\,
      S(1) => \caracter[0]_i_574_n_0\,
      S(0) => \caracter[0]_i_575_n_0\
    );
\caracter_reg[0]_i_530\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_94_n_0\,
      CO(3) => \caracter_reg[0]_i_530_n_0\,
      CO(2) => \caracter_reg[0]_i_530_n_1\,
      CO(1) => \caracter_reg[0]_i_530_n_2\,
      CO(0) => \caracter_reg[0]_i_530_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_583_n_0\,
      DI(2) => \caracter[0]_i_584_n_0\,
      DI(1) => \caracter[0]_i_585_n_0\,
      DI(0) => \caracter[0]_i_586_n_0\,
      O(3) => \caracter_reg[0]_i_530_n_4\,
      O(2) => \caracter_reg[0]_i_530_n_5\,
      O(1) => \caracter_reg[0]_i_530_n_6\,
      O(0) => \caracter_reg[0]_i_530_n_7\,
      S(3) => \caracter[0]_i_587_n_0\,
      S(2) => \caracter[0]_i_588_n_0\,
      S(1) => \caracter[0]_i_589_n_0\,
      S(0) => \caracter[0]_i_590_n_0\
    );
\caracter_reg[0]_i_531\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_535_n_0\,
      CO(3 downto 2) => \NLW_caracter_reg[0]_i_531_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \caracter_reg[0]_i_531_n_2\,
      CO(0) => \NLW_caracter_reg[0]_i_531_CO_UNCONNECTED\(0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_caracter_reg[0]_i_531_O_UNCONNECTED\(3 downto 1),
      O(0) => \caracter_reg[0]_i_531_n_7\,
      S(3 downto 1) => B"001",
      S(0) => \caracter[0]_i_591_n_0\
    );
\caracter_reg[0]_i_533\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_83_n_0\,
      CO(3) => \caracter_reg[0]_i_533_n_0\,
      CO(2) => \caracter_reg[0]_i_533_n_1\,
      CO(1) => \caracter_reg[0]_i_533_n_2\,
      CO(0) => \caracter_reg[0]_i_533_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => euros2(20 downto 17),
      O(3) => \caracter_reg[0]_i_533_n_4\,
      O(2) => \caracter_reg[0]_i_533_n_5\,
      O(1) => \caracter_reg[0]_i_533_n_6\,
      O(0) => \caracter_reg[0]_i_533_n_7\,
      S(3) => \caracter[0]_i_596_n_0\,
      S(2) => \caracter[0]_i_597_n_0\,
      S(1) => \caracter[0]_i_598_n_0\,
      S(0) => \caracter[0]_i_599_n_0\
    );
\caracter_reg[0]_i_535\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_84_n_0\,
      CO(3) => \caracter_reg[0]_i_535_n_0\,
      CO(2) => \caracter_reg[0]_i_535_n_1\,
      CO(1) => \caracter_reg[0]_i_535_n_2\,
      CO(0) => \caracter_reg[0]_i_535_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \caracter[0]_i_600_n_0\,
      O(3) => \caracter_reg[0]_i_535_n_4\,
      O(2) => \caracter_reg[0]_i_535_n_5\,
      O(1) => \caracter_reg[0]_i_535_n_6\,
      O(0) => \caracter_reg[0]_i_535_n_7\,
      S(3) => \caracter[0]_i_601_n_0\,
      S(2) => \caracter[0]_i_602_n_0\,
      S(1) => \caracter[0]_i_603_n_0\,
      S(0) => \caracter[0]_i_604_n_0\
    );
\caracter_reg[0]_i_55\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_106_n_0\,
      CO(3) => \caracter_reg[0]_i_55_n_0\,
      CO(2) => \caracter_reg[0]_i_55_n_1\,
      CO(1) => \caracter_reg[0]_i_55_n_2\,
      CO(0) => \caracter_reg[0]_i_55_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => euros3(24 downto 21),
      S(3) => \caracter_reg[0]_i_22_n_3\,
      S(2) => \caracter_reg[0]_i_22_n_3\,
      S(1) => \caracter_reg[0]_i_22_n_3\,
      S(0) => \caracter_reg[0]_i_22_n_3\
    );
\caracter_reg[0]_i_557\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_605_n_0\,
      CO(3) => \caracter_reg[0]_i_557_n_0\,
      CO(2) => \caracter_reg[0]_i_557_n_1\,
      CO(1) => \caracter_reg[0]_i_557_n_2\,
      CO(0) => \caracter_reg[0]_i_557_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_606_n_0\,
      DI(2) => euros2(7),
      DI(1) => \caracter[0]_i_608_n_0\,
      DI(0) => \caracter[0]_i_609_n_0\,
      O(3) => \caracter_reg[0]_i_557_n_4\,
      O(2 downto 0) => \NLW_caracter_reg[0]_i_557_O_UNCONNECTED\(2 downto 0),
      S(3) => \caracter[0]_i_610_n_0\,
      S(2) => \caracter[0]_i_611_n_0\,
      S(1) => \caracter[0]_i_612_n_0\,
      S(0) => \caracter[0]_i_613_n_0\
    );
\caracter_reg[0]_i_56\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[0]_i_56_n_0\,
      CO(2) => \caracter_reg[0]_i_56_n_1\,
      CO(1) => \caracter_reg[0]_i_56_n_2\,
      CO(0) => \caracter_reg[0]_i_56_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => \caracter_reg[3]_i_126_0\(3 downto 0),
      O(3) => \caracter_reg[0]_i_56_n_4\,
      O(2) => \caracter_reg[0]_i_56_n_5\,
      O(1) => \caracter_reg[0]_i_56_n_6\,
      O(0) => euros2(0),
      S(3) => \caracter[0]_i_107_n_0\,
      S(2) => \caracter[0]_i_108_n_0\,
      S(1) => \caracter[0]_i_109_n_0\,
      S(0) => \caracter[0]_i_110_n_0\
    );
\caracter_reg[0]_i_605\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[0]_i_605_n_0\,
      CO(2) => \caracter_reg[0]_i_605_n_1\,
      CO(1) => \caracter_reg[0]_i_605_n_2\,
      CO(0) => \caracter_reg[0]_i_605_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_614_n_0\,
      DI(2) => \caracter[0]_i_615_n_0\,
      DI(1) => \caracter[0]_i_616_n_0\,
      DI(0) => '0',
      O(3 downto 0) => \NLW_caracter_reg[0]_i_605_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[0]_i_617_n_0\,
      S(2) => \caracter[0]_i_618_n_0\,
      S(1) => \caracter[0]_i_619_n_0\,
      S(0) => \caracter[0]_i_620_n_0\
    );
\caracter_reg[0]_i_61\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_111_n_0\,
      CO(3) => \caracter_reg[0]_i_61_n_0\,
      CO(2) => \caracter_reg[0]_i_61_n_1\,
      CO(1) => \caracter_reg[0]_i_61_n_2\,
      CO(0) => \caracter_reg[0]_i_61_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_112_n_0\,
      DI(2) => \caracter[0]_i_113_n_0\,
      DI(1) => \caracter[0]_i_114_n_0\,
      DI(0) => \caracter[0]_i_115_n_0\,
      O(3) => \caracter_reg[0]_i_61_n_4\,
      O(2) => \caracter_reg[0]_i_61_n_5\,
      O(1) => \caracter_reg[0]_i_61_n_6\,
      O(0) => \caracter_reg[0]_i_61_n_7\,
      S(3) => \caracter[0]_i_116_n_0\,
      S(2) => \caracter[0]_i_117_n_0\,
      S(1) => \caracter[0]_i_118_n_0\,
      S(0) => \caracter[0]_i_119_n_0\
    );
\caracter_reg[0]_i_70\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_120_n_0\,
      CO(3) => \caracter_reg[0]_i_70_n_0\,
      CO(2) => \caracter_reg[0]_i_70_n_1\,
      CO(1) => \caracter_reg[0]_i_70_n_2\,
      CO(0) => \caracter_reg[0]_i_70_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_121_n_0\,
      DI(2) => \caracter[0]_i_122_n_0\,
      DI(1) => \caracter[0]_i_123_n_0\,
      DI(0) => \caracter[0]_i_124_n_0\,
      O(3) => \caracter_reg[0]_i_70_n_4\,
      O(2) => \caracter_reg[0]_i_70_n_5\,
      O(1) => \caracter_reg[0]_i_70_n_6\,
      O(0) => \caracter_reg[0]_i_70_n_7\,
      S(3) => \caracter[0]_i_125_n_0\,
      S(2) => \caracter[0]_i_126_n_0\,
      S(1) => \caracter[0]_i_127_n_0\,
      S(0) => \caracter[0]_i_128_n_0\
    );
\caracter_reg[0]_i_71\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_70_n_0\,
      CO(3 downto 0) => \NLW_caracter_reg[0]_i_71_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_caracter_reg[0]_i_71_O_UNCONNECTED\(3 downto 1),
      O(0) => \caracter_reg[0]_i_71_n_7\,
      S(3 downto 1) => B"000",
      S(0) => \caracter[0]_i_129_n_0\
    );
\caracter_reg[0]_i_72\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_130_n_0\,
      CO(3) => \caracter_reg[0]_i_72_n_0\,
      CO(2) => \caracter_reg[0]_i_72_n_1\,
      CO(1) => \caracter_reg[0]_i_72_n_2\,
      CO(0) => \caracter_reg[0]_i_72_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_131_n_0\,
      DI(2) => \caracter[0]_i_132_n_0\,
      DI(1) => \caracter[0]_i_133_n_0\,
      DI(0) => \caracter[0]_i_134_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[0]_i_72_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[0]_i_135_n_0\,
      S(2) => \caracter[0]_i_136_n_0\,
      S(1) => \caracter[0]_i_137_n_0\,
      S(0) => \caracter[0]_i_138_n_0\
    );
\caracter_reg[0]_i_81\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_139_n_0\,
      CO(3) => \caracter_reg[0]_i_81_n_0\,
      CO(2) => \caracter_reg[0]_i_81_n_1\,
      CO(1) => \caracter_reg[0]_i_81_n_2\,
      CO(0) => \caracter_reg[0]_i_81_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_149_n_0\,
      DI(2) => \caracter[0]_i_150_n_0\,
      DI(1) => \caracter[0]_i_151_n_0\,
      DI(0) => \caracter[0]_i_152_n_0\,
      O(3) => \caracter_reg[0]_i_81_n_4\,
      O(2) => \caracter_reg[0]_i_81_n_5\,
      O(1) => \caracter_reg[0]_i_81_n_6\,
      O(0) => \caracter_reg[0]_i_81_n_7\,
      S(3) => \caracter[0]_i_153_n_0\,
      S(2) => \caracter[0]_i_154_n_0\,
      S(1) => \caracter[0]_i_155_n_0\,
      S(0) => \caracter[0]_i_156_n_0\
    );
\caracter_reg[0]_i_82\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_91_n_0\,
      CO(3) => \caracter_reg[0]_i_82_n_0\,
      CO(2) => \caracter_reg[0]_i_82_n_1\,
      CO(1) => \caracter_reg[0]_i_82_n_2\,
      CO(0) => \caracter_reg[0]_i_82_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_157_n_0\,
      DI(2) => \caracter[0]_i_158_n_0\,
      DI(1) => \caracter[0]_i_159_n_0\,
      DI(0) => \caracter[0]_i_160_n_0\,
      O(3) => \caracter_reg[0]_i_82_n_4\,
      O(2) => \caracter_reg[0]_i_82_n_5\,
      O(1) => \caracter_reg[0]_i_82_n_6\,
      O(0) => \caracter_reg[0]_i_82_n_7\,
      S(3) => \caracter[0]_i_161_n_0\,
      S(2) => \caracter[0]_i_162_n_0\,
      S(1) => \caracter[0]_i_163_n_0\,
      S(0) => \caracter[0]_i_164_n_0\
    );
\caracter_reg[0]_i_83\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_87_n_0\,
      CO(3) => \caracter_reg[0]_i_83_n_0\,
      CO(2) => \caracter_reg[0]_i_83_n_1\,
      CO(1) => \caracter_reg[0]_i_83_n_2\,
      CO(0) => \caracter_reg[0]_i_83_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => euros2(16 downto 13),
      O(3) => \caracter_reg[0]_i_83_n_4\,
      O(2) => \caracter_reg[0]_i_83_n_5\,
      O(1) => \caracter_reg[0]_i_83_n_6\,
      O(0) => \caracter_reg[0]_i_83_n_7\,
      S(3) => \caracter[0]_i_169_n_0\,
      S(2) => \caracter[0]_i_170_n_0\,
      S(1) => \caracter[0]_i_171_n_0\,
      S(0) => \caracter[0]_i_172_n_0\
    );
\caracter_reg[0]_i_84\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_89_n_0\,
      CO(3) => \caracter_reg[0]_i_84_n_0\,
      CO(2) => \caracter_reg[0]_i_84_n_1\,
      CO(1) => \caracter_reg[0]_i_84_n_2\,
      CO(0) => \caracter_reg[0]_i_84_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_173_n_0\,
      DI(2) => \caracter[0]_i_174_n_0\,
      DI(1) => \caracter[0]_i_175_n_0\,
      DI(0) => \caracter[0]_i_176_n_0\,
      O(3) => \caracter_reg[0]_i_84_n_4\,
      O(2) => \caracter_reg[0]_i_84_n_5\,
      O(1) => \caracter_reg[0]_i_84_n_6\,
      O(0) => \caracter_reg[0]_i_84_n_7\,
      S(3) => \caracter[0]_i_177_n_0\,
      S(2) => \caracter[0]_i_178_n_0\,
      S(1) => \caracter[0]_i_179_n_0\,
      S(0) => \caracter[0]_i_180_n_0\
    );
\caracter_reg[0]_i_87\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_142_n_0\,
      CO(3) => \caracter_reg[0]_i_87_n_0\,
      CO(2) => \caracter_reg[0]_i_87_n_1\,
      CO(1) => \caracter_reg[0]_i_87_n_2\,
      CO(0) => \caracter_reg[0]_i_87_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => euros2(12 downto 9),
      O(3) => \caracter_reg[0]_i_87_n_4\,
      O(2) => \caracter_reg[0]_i_87_n_5\,
      O(1) => \caracter_reg[0]_i_87_n_6\,
      O(0) => \caracter_reg[0]_i_87_n_7\,
      S(3) => \caracter[0]_i_185_n_0\,
      S(2) => \caracter[0]_i_186_n_0\,
      S(1) => \caracter[0]_i_187_n_0\,
      S(0) => \caracter[0]_i_188_n_0\
    );
\caracter_reg[0]_i_89\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_145_n_0\,
      CO(3) => \caracter_reg[0]_i_89_n_0\,
      CO(2) => \caracter_reg[0]_i_89_n_1\,
      CO(1) => \caracter_reg[0]_i_89_n_2\,
      CO(0) => \caracter_reg[0]_i_89_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_189_n_0\,
      DI(2) => \caracter[0]_i_190_n_0\,
      DI(1) => \caracter[0]_i_191_n_0\,
      DI(0) => \caracter[0]_i_192_n_0\,
      O(3) => \caracter_reg[0]_i_89_n_4\,
      O(2) => \caracter_reg[0]_i_89_n_5\,
      O(1) => \caracter_reg[0]_i_89_n_6\,
      O(0) => \caracter_reg[0]_i_89_n_7\,
      S(3) => \caracter[0]_i_193_n_0\,
      S(2) => \caracter[0]_i_194_n_0\,
      S(1) => \caracter[0]_i_195_n_0\,
      S(0) => \caracter[0]_i_196_n_0\
    );
\caracter_reg[0]_i_91\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_144_n_0\,
      CO(3) => \caracter_reg[0]_i_91_n_0\,
      CO(2) => \caracter_reg[0]_i_91_n_1\,
      CO(1) => \caracter_reg[0]_i_91_n_2\,
      CO(0) => \caracter_reg[0]_i_91_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_197_n_0\,
      DI(2) => \caracter[0]_i_198_n_0\,
      DI(1) => \caracter[0]_i_199_n_0\,
      DI(0) => \caracter[0]_i_200_n_0\,
      O(3) => \caracter_reg[0]_i_91_n_4\,
      O(2) => \caracter_reg[0]_i_91_n_5\,
      O(1) => \caracter_reg[0]_i_91_n_6\,
      O(0) => \caracter_reg[0]_i_91_n_7\,
      S(3) => \caracter[0]_i_201_n_0\,
      S(2) => \caracter[0]_i_202_n_0\,
      S(1) => \caracter[0]_i_203_n_0\,
      S(0) => \caracter[0]_i_204_n_0\
    );
\caracter_reg[0]_i_93\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_82_n_0\,
      CO(3) => \caracter_reg[0]_i_93_n_0\,
      CO(2) => \caracter_reg[0]_i_93_n_1\,
      CO(1) => \caracter_reg[0]_i_93_n_2\,
      CO(0) => \caracter_reg[0]_i_93_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_205_n_0\,
      DI(2) => \caracter[0]_i_206_n_0\,
      DI(1) => \caracter[0]_i_207_n_0\,
      DI(0) => \caracter[0]_i_208_n_0\,
      O(3) => \caracter_reg[0]_i_93_n_4\,
      O(2) => \caracter_reg[0]_i_93_n_5\,
      O(1) => \caracter_reg[0]_i_93_n_6\,
      O(0) => \caracter_reg[0]_i_93_n_7\,
      S(3) => \caracter[0]_i_209_n_0\,
      S(2) => \caracter[0]_i_210_n_0\,
      S(1) => \caracter[0]_i_211_n_0\,
      S(0) => \caracter[0]_i_212_n_0\
    );
\caracter_reg[0]_i_94\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_81_n_0\,
      CO(3) => \caracter_reg[0]_i_94_n_0\,
      CO(2) => \caracter_reg[0]_i_94_n_1\,
      CO(1) => \caracter_reg[0]_i_94_n_2\,
      CO(0) => \caracter_reg[0]_i_94_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_213_n_0\,
      DI(2) => \caracter[0]_i_214_n_0\,
      DI(1) => \caracter[0]_i_215_n_0\,
      DI(0) => \caracter[0]_i_216_n_0\,
      O(3) => \caracter_reg[0]_i_94_n_4\,
      O(2) => \caracter_reg[0]_i_94_n_5\,
      O(1) => \caracter_reg[0]_i_94_n_6\,
      O(0) => \caracter_reg[0]_i_94_n_7\,
      S(3) => \caracter[0]_i_217_n_0\,
      S(2) => \caracter[0]_i_218_n_0\,
      S(1) => \caracter[0]_i_219_n_0\,
      S(0) => \caracter[0]_i_220_n_0\
    );
\caracter_reg[0]_i_97\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[0]_i_221_n_0\,
      CO(3) => \caracter_reg[0]_i_97_n_0\,
      CO(2) => \caracter_reg[0]_i_97_n_1\,
      CO(1) => \caracter_reg[0]_i_97_n_2\,
      CO(0) => \caracter_reg[0]_i_97_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[0]_i_222_n_0\,
      DI(2) => \caracter[0]_i_223_n_0\,
      DI(1) => \caracter[0]_i_224_n_0\,
      DI(0) => \caracter[0]_i_225_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[0]_i_97_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[0]_i_226_n_0\,
      S(2) => \caracter[0]_i_227_n_0\,
      S(1) => \caracter[0]_i_228_n_0\,
      S(0) => \caracter[0]_i_229_n_0\
    );
\caracter_reg[1]_i_102\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_166_n_0\,
      CO(3 downto 1) => \NLW_caracter_reg[1]_i_102_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \caracter_reg[1]_i_102_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_caracter_reg[1]_i_102_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
\caracter_reg[1]_i_103\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_161_n_0\,
      CO(3) => \caracter_reg[1]_i_103_n_0\,
      CO(2) => \caracter_reg[1]_i_103_n_1\,
      CO(1) => \caracter_reg[1]_i_103_n_2\,
      CO(0) => \caracter_reg[1]_i_103_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[1]_i_167_n_0\,
      DI(2) => \caracter[1]_i_168_n_0\,
      DI(1) => \caracter[3]_i_43_n_0\,
      DI(0) => \caracter[1]_i_169_n_0\,
      O(3) => \caracter_reg[1]_i_103_n_4\,
      O(2) => \caracter_reg[1]_i_103_n_5\,
      O(1) => \caracter_reg[1]_i_103_n_6\,
      O(0) => \caracter_reg[1]_i_103_n_7\,
      S(3) => \caracter[1]_i_170_n_0\,
      S(2) => \caracter[1]_i_171_n_0\,
      S(1) => \caracter[1]_i_172_n_0\,
      S(0) => \caracter[1]_i_173_n_0\
    );
\caracter_reg[1]_i_104\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[1]_i_104_n_0\,
      CO(2) => \caracter_reg[1]_i_104_n_1\,
      CO(1) => \caracter_reg[1]_i_104_n_2\,
      CO(0) => \caracter_reg[1]_i_104_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[1]_i_174_n_0\,
      DI(2) => \caracter[1]_i_175_n_0\,
      DI(1) => \caracter[1]_i_176_n_0\,
      DI(0) => '0',
      O(3) => \caracter_reg[1]_i_104_n_4\,
      O(2) => \caracter_reg[1]_i_104_n_5\,
      O(1) => \caracter_reg[1]_i_104_n_6\,
      O(0) => \NLW_caracter_reg[1]_i_104_O_UNCONNECTED\(0),
      S(3) => \caracter[1]_i_177_n_0\,
      S(2) => \caracter[1]_i_178_n_0\,
      S(1) => \caracter[1]_i_179_n_0\,
      S(0) => \caracter[1]_i_180_n_0\
    );
\caracter_reg[1]_i_124\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_181_n_0\,
      CO(3) => \caracter_reg[1]_i_124_n_0\,
      CO(2) => \caracter_reg[1]_i_124_n_1\,
      CO(1) => \caracter_reg[1]_i_124_n_2\,
      CO(0) => \caracter_reg[1]_i_124_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[1]_i_182_n_0\,
      DI(2) => \caracter[1]_i_183_n_0\,
      DI(1) => \caracter[1]_i_184_n_0\,
      DI(0) => \caracter[1]_i_185_n_0\,
      O(3) => \caracter_reg[1]_i_124_n_4\,
      O(2) => \caracter_reg[1]_i_124_n_5\,
      O(1) => \caracter_reg[1]_i_124_n_6\,
      O(0) => \caracter_reg[1]_i_124_n_7\,
      S(3) => \caracter[1]_i_186_n_0\,
      S(2) => \caracter[1]_i_187_n_0\,
      S(1) => \caracter[1]_i_188_n_0\,
      S(0) => \caracter[1]_i_189_n_0\
    );
\caracter_reg[1]_i_141\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_190_n_0\,
      CO(3) => \caracter_reg[1]_i_141_n_0\,
      CO(2) => \caracter_reg[1]_i_141_n_1\,
      CO(1) => \caracter_reg[1]_i_141_n_2\,
      CO(0) => \caracter_reg[1]_i_141_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[1]_i_191_n_0\,
      DI(2) => \caracter[1]_i_192_n_0\,
      DI(1) => \caracter[1]_i_193_n_0\,
      DI(0) => \caracter[1]_i_194_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[1]_i_141_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[1]_i_195_n_0\,
      S(2) => \caracter[1]_i_196_n_0\,
      S(1) => \caracter[1]_i_197_n_0\,
      S(0) => \caracter[1]_i_198_n_0\
    );
\caracter_reg[1]_i_15\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[1]_i_15_n_0\,
      CO(2) => \caracter_reg[1]_i_15_n_1\,
      CO(1) => \caracter_reg[1]_i_15_n_2\,
      CO(0) => \caracter_reg[1]_i_15_n_3\,
      CYINIT => '1',
      DI(3) => \caracter[1]_i_24_n_0\,
      DI(2) => \caracter[1]_i_25_n_0\,
      DI(1) => \caracter[1]_i_26_n_0\,
      DI(0) => \caracter[1]_i_27_n_0\,
      O(3) => \caracter_reg[1]_i_15_n_4\,
      O(2) => \caracter_reg[1]_i_15_n_5\,
      O(1) => \caracter_reg[1]_i_15_n_6\,
      O(0) => \string_cent_decenas[1]5\(0),
      S(3) => \caracter[1]_i_28_n_0\,
      S(2) => \caracter[1]_i_29_n_0\,
      S(1) => \caracter[1]_i_30_n_0\,
      S(0) => \caracter[1]_i_31_n_0\
    );
\caracter_reg[1]_i_150\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3 downto 2) => \NLW_caracter_reg[1]_i_150_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \caracter_reg[1]_i_150_n_2\,
      CO(0) => \caracter_reg[1]_i_150_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \caracter[1]_i_202_n_0\,
      DI(0) => '0',
      O(3) => \NLW_caracter_reg[1]_i_150_O_UNCONNECTED\(3),
      O(2) => \caracter_reg[1]_i_150_n_5\,
      O(1) => \caracter_reg[1]_i_150_n_6\,
      O(0) => \caracter_reg[1]_i_150_n_7\,
      S(3) => '0',
      S(2) => \caracter[1]_i_203_n_0\,
      S(1) => \caracter[1]_i_204_n_0\,
      S(0) => \caracter[1]_i_205_n_0\
    );
\caracter_reg[1]_i_152\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_206_n_0\,
      CO(3) => \caracter_reg[1]_i_152_n_0\,
      CO(2) => \caracter_reg[1]_i_152_n_1\,
      CO(1) => \caracter_reg[1]_i_152_n_2\,
      CO(0) => \caracter_reg[1]_i_152_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[1]_i_207_n_0\,
      DI(2) => \caracter[1]_i_208_n_0\,
      DI(1) => \caracter[1]_i_209_n_0\,
      DI(0) => \caracter[1]_i_210_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[1]_i_152_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[1]_i_211_n_0\,
      S(2) => \caracter[1]_i_212_n_0\,
      S(1) => \caracter[1]_i_213_n_0\,
      S(0) => \caracter[1]_i_214_n_0\
    );
\caracter_reg[1]_i_16\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_15_n_0\,
      CO(3 downto 0) => \NLW_caracter_reg[1]_i_16_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_caracter_reg[1]_i_16_O_UNCONNECTED\(3 downto 1),
      O(0) => \caracter_reg[1]_i_16_n_7\,
      S(3 downto 1) => B"000",
      S(0) => \caracter[1]_i_32_n_0\
    );
\caracter_reg[1]_i_161\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_215_n_0\,
      CO(3) => \caracter_reg[1]_i_161_n_0\,
      CO(2) => \caracter_reg[1]_i_161_n_1\,
      CO(1) => \caracter_reg[1]_i_161_n_2\,
      CO(0) => \caracter_reg[1]_i_161_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[1]_i_217_n_0\,
      DI(2) => \caracter[1]_i_218_n_0\,
      DI(1) => \caracter[1]_i_219_n_0\,
      DI(0) => \caracter[1]_i_220_n_0\,
      O(3) => \caracter_reg[1]_i_161_n_4\,
      O(2) => \caracter_reg[1]_i_161_n_5\,
      O(1) => \caracter_reg[1]_i_161_n_6\,
      O(0) => \caracter_reg[1]_i_161_n_7\,
      S(3) => \caracter[1]_i_221_n_0\,
      S(2) => \caracter[1]_i_222_n_0\,
      S(1) => \caracter[1]_i_223_n_0\,
      S(0) => \caracter[1]_i_224_n_0\
    );
\caracter_reg[1]_i_166\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_225_n_0\,
      CO(3) => \caracter_reg[1]_i_166_n_0\,
      CO(2) => \caracter_reg[1]_i_166_n_1\,
      CO(1) => \caracter_reg[1]_i_166_n_2\,
      CO(0) => \caracter_reg[1]_i_166_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[1]_i_226_n_0\,
      DI(2) => \caracter[1]_i_227_n_0\,
      DI(1) => \caracter[3]_i_43_n_0\,
      DI(0) => \caracter[1]_i_228_n_0\,
      O(3) => \caracter_reg[1]_i_166_n_4\,
      O(2) => \caracter_reg[1]_i_166_n_5\,
      O(1) => \caracter_reg[1]_i_166_n_6\,
      O(0) => \caracter_reg[1]_i_166_n_7\,
      S(3) => \caracter[1]_i_229_n_0\,
      S(2) => \caracter[1]_i_230_n_0\,
      S(1) => \caracter[1]_i_231_n_0\,
      S(0) => \caracter[1]_i_232_n_0\
    );
\caracter_reg[1]_i_181\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[1]_i_181_n_0\,
      CO(2) => \caracter_reg[1]_i_181_n_1\,
      CO(1) => \caracter_reg[1]_i_181_n_2\,
      CO(0) => \caracter_reg[1]_i_181_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[1]_i_174_n_0\,
      DI(2) => \caracter[1]_i_233_n_0\,
      DI(1) => \caracter[1]_i_234_n_0\,
      DI(0) => '0',
      O(3) => \caracter_reg[1]_i_181_n_4\,
      O(2) => \caracter_reg[1]_i_181_n_5\,
      O(1) => \caracter_reg[1]_i_181_n_6\,
      O(0) => \caracter_reg[1]_i_181_n_7\,
      S(3) => \caracter[1]_i_235_n_0\,
      S(2) => \caracter[1]_i_236_n_0\,
      S(1) => \caracter[1]_i_237_n_0\,
      S(0) => \caracter[1]_i_238_n_0\
    );
\caracter_reg[1]_i_190\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_239_n_0\,
      CO(3) => \caracter_reg[1]_i_190_n_0\,
      CO(2) => \caracter_reg[1]_i_190_n_1\,
      CO(1) => \caracter_reg[1]_i_190_n_2\,
      CO(0) => \caracter_reg[1]_i_190_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[1]_i_240_n_0\,
      DI(2) => \caracter[1]_i_241_n_0\,
      DI(1) => \caracter[1]_i_242_n_0\,
      DI(0) => \caracter[1]_i_243_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[1]_i_190_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[1]_i_244_n_0\,
      S(2) => \caracter[1]_i_245_n_0\,
      S(1) => \caracter[1]_i_246_n_0\,
      S(0) => \caracter[1]_i_247_n_0\
    );
\caracter_reg[1]_i_199\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_249_n_0\,
      CO(3 downto 1) => \NLW_caracter_reg[1]_i_199_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \caracter_reg[1]_i_199_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_caracter_reg[1]_i_199_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
\caracter_reg[1]_i_200\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_250_n_0\,
      CO(3) => \caracter_reg[1]_i_200_n_0\,
      CO(2) => \NLW_caracter_reg[1]_i_200_CO_UNCONNECTED\(2),
      CO(1) => \caracter_reg[1]_i_200_n_2\,
      CO(0) => \caracter_reg[1]_i_200_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => \caracter_reg[3]_i_126_0\(7 downto 5),
      O(3) => \NLW_caracter_reg[1]_i_200_O_UNCONNECTED\(3),
      O(2) => \caracter_reg[1]_i_200_n_5\,
      O(1) => \caracter_reg[1]_i_200_n_6\,
      O(0) => \caracter_reg[1]_i_200_n_7\,
      S(3) => '1',
      S(2) => \caracter[1]_i_251_n_0\,
      S(1) => \caracter[1]_i_252_n_0\,
      S(0) => \caracter[1]_i_253_n_0\
    );
\caracter_reg[1]_i_201\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_248_n_0\,
      CO(3) => \caracter_reg[1]_i_201_n_0\,
      CO(2) => \NLW_caracter_reg[1]_i_201_CO_UNCONNECTED\(2),
      CO(1) => \caracter_reg[1]_i_201_n_2\,
      CO(0) => \caracter_reg[1]_i_201_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \caracter_reg[3]_i_126_0\(6),
      DI(0) => \caracter[1]_i_254_n_0\,
      O(3) => \NLW_caracter_reg[1]_i_201_O_UNCONNECTED\(3),
      O(2) => \caracter_reg[1]_i_201_n_5\,
      O(1) => \caracter_reg[1]_i_201_n_6\,
      O(0) => \caracter_reg[1]_i_201_n_7\,
      S(3) => '1',
      S(2) => \caracter_reg[3]_i_126_0\(7),
      S(1) => \caracter[1]_i_255_n_0\,
      S(0) => \caracter[1]_i_256_n_0\
    );
\caracter_reg[1]_i_206\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[1]_i_206_n_0\,
      CO(2) => \caracter_reg[1]_i_206_n_1\,
      CO(1) => \caracter_reg[1]_i_206_n_2\,
      CO(0) => \caracter_reg[1]_i_206_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[1]_i_258_n_0\,
      DI(2) => \caracter[1]_i_259_n_0\,
      DI(1) => \caracter[1]_i_260_n_0\,
      DI(0) => \caracter[1]_i_261_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[1]_i_206_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[1]_i_262_n_0\,
      S(2) => \caracter[1]_i_263_n_0\,
      S(1) => \caracter[1]_i_264_n_0\,
      S(0) => \caracter[1]_i_265_n_0\
    );
\caracter_reg[1]_i_215\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[1]_i_215_n_0\,
      CO(2) => \caracter_reg[1]_i_215_n_1\,
      CO(1) => \caracter_reg[1]_i_215_n_2\,
      CO(0) => \caracter_reg[1]_i_215_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[1]_i_266_n_0\,
      DI(2) => \caracter[1]_i_267_n_0\,
      DI(1 downto 0) => B"01",
      O(3) => \caracter_reg[1]_i_215_n_4\,
      O(2) => \caracter_reg[1]_i_215_n_5\,
      O(1) => \caracter_reg[1]_i_215_n_6\,
      O(0) => \NLW_caracter_reg[1]_i_215_O_UNCONNECTED\(0),
      S(3) => \caracter[1]_i_268_n_0\,
      S(2) => \caracter[1]_i_269_n_0\,
      S(1) => \caracter[1]_i_270_n_0\,
      S(0) => \caracter[1]_i_271_n_0\
    );
\caracter_reg[1]_i_216\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[1]_i_216_n_0\,
      CO(2) => \caracter_reg[1]_i_216_n_1\,
      CO(1) => \caracter_reg[1]_i_216_n_2\,
      CO(0) => \caracter_reg[1]_i_216_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[1]_i_272_n_0\,
      DI(2) => \caracter[1]_i_273_n_0\,
      DI(1 downto 0) => B"01",
      O(3 downto 1) => \NLW_caracter_reg[1]_i_216_O_UNCONNECTED\(3 downto 1),
      O(0) => \caracter_reg[1]_i_216_n_7\,
      S(3) => \caracter[1]_i_274_n_0\,
      S(2) => \caracter[1]_i_275_n_0\,
      S(1) => \caracter[1]_i_276_n_0\,
      S(0) => \caracter[1]_i_277_n_0\
    );
\caracter_reg[1]_i_225\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_216_n_0\,
      CO(3) => \caracter_reg[1]_i_225_n_0\,
      CO(2) => \caracter_reg[1]_i_225_n_1\,
      CO(1) => \caracter_reg[1]_i_225_n_2\,
      CO(0) => \caracter_reg[1]_i_225_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[1]_i_278_n_0\,
      DI(2) => \caracter[1]_i_218_n_0\,
      DI(1) => \caracter[1]_i_279_n_0\,
      DI(0) => \caracter[1]_i_280_n_0\,
      O(3) => \caracter_reg[1]_i_225_n_4\,
      O(2) => \caracter_reg[1]_i_225_n_5\,
      O(1) => \caracter_reg[1]_i_225_n_6\,
      O(0) => \NLW_caracter_reg[1]_i_225_O_UNCONNECTED\(0),
      S(3) => \caracter[1]_i_281_n_0\,
      S(2) => \caracter[1]_i_282_n_0\,
      S(1) => \caracter[1]_i_283_n_0\,
      S(0) => \caracter[1]_i_284_n_0\
    );
\caracter_reg[1]_i_239\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_285_n_0\,
      CO(3) => \caracter_reg[1]_i_239_n_0\,
      CO(2) => \caracter_reg[1]_i_239_n_1\,
      CO(1) => \caracter_reg[1]_i_239_n_2\,
      CO(0) => \caracter_reg[1]_i_239_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[1]_i_286_n_0\,
      DI(2) => \caracter[1]_i_287_n_0\,
      DI(1) => \caracter[1]_i_288_n_0\,
      DI(0) => \caracter[1]_i_289_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[1]_i_239_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[1]_i_290_n_0\,
      S(2) => \caracter[1]_i_291_n_0\,
      S(1) => \caracter[1]_i_292_n_0\,
      S(0) => \caracter[1]_i_293_n_0\
    );
\caracter_reg[1]_i_248\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_294_n_0\,
      CO(3) => \caracter_reg[1]_i_248_n_0\,
      CO(2) => \caracter_reg[1]_i_248_n_1\,
      CO(1) => \caracter_reg[1]_i_248_n_2\,
      CO(0) => \caracter_reg[1]_i_248_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[1]_i_295_n_0\,
      DI(2) => \caracter[1]_i_296_n_0\,
      DI(1) => \caracter[1]_i_297_n_0\,
      DI(0) => \caracter[1]_i_298_n_0\,
      O(3) => \caracter_reg[1]_i_248_n_4\,
      O(2) => \caracter_reg[1]_i_248_n_5\,
      O(1) => \caracter_reg[1]_i_248_n_6\,
      O(0) => \caracter_reg[1]_i_248_n_7\,
      S(3) => \caracter[1]_i_299_n_0\,
      S(2) => \caracter[1]_i_300_n_0\,
      S(1) => \caracter[1]_i_301_n_0\,
      S(0) => \caracter[1]_i_302_n_0\
    );
\caracter_reg[1]_i_249\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_303_n_0\,
      CO(3) => \caracter_reg[1]_i_249_n_0\,
      CO(2) => \caracter_reg[1]_i_249_n_1\,
      CO(1) => \caracter_reg[1]_i_249_n_2\,
      CO(0) => \caracter_reg[1]_i_249_n_3\,
      CYINIT => '0',
      DI(3) => '1',
      DI(2) => \caracter_reg[3]_i_126_0\(6),
      DI(1) => \caracter[1]_i_304_n_0\,
      DI(0) => \caracter[1]_i_305_n_0\,
      O(3) => \caracter_reg[1]_i_249_n_4\,
      O(2) => \caracter_reg[1]_i_249_n_5\,
      O(1) => \caracter_reg[1]_i_249_n_6\,
      O(0) => \caracter_reg[1]_i_249_n_7\,
      S(3) => \caracter[1]_i_306_n_0\,
      S(2) => \caracter[1]_i_307_n_0\,
      S(1) => \caracter[1]_i_308_n_0\,
      S(0) => \caracter[1]_i_309_n_0\
    );
\caracter_reg[1]_i_250\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_310_n_0\,
      CO(3) => \caracter_reg[1]_i_250_n_0\,
      CO(2) => \caracter_reg[1]_i_250_n_1\,
      CO(1) => \caracter_reg[1]_i_250_n_2\,
      CO(0) => \caracter_reg[1]_i_250_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \caracter_reg[3]_i_126_0\(4 downto 1),
      O(3) => \caracter_reg[1]_i_250_n_4\,
      O(2) => \caracter_reg[1]_i_250_n_5\,
      O(1) => \caracter_reg[1]_i_250_n_6\,
      O(0) => \caracter_reg[1]_i_250_n_7\,
      S(3) => \caracter[1]_i_311_n_0\,
      S(2) => \caracter[1]_i_312_n_0\,
      S(1) => \caracter[1]_i_313_n_0\,
      S(0) => \caracter[1]_i_314_n_0\
    );
\caracter_reg[1]_i_257\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_85_n_0\,
      CO(3 downto 1) => \NLW_caracter_reg[1]_i_257_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \caracter_reg[1]_i_257_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 2) => \NLW_caracter_reg[1]_i_257_O_UNCONNECTED\(3 downto 2),
      O(1) => \caracter_reg[1]_i_257_n_6\,
      O(0) => \caracter_reg[1]_i_257_n_7\,
      S(3 downto 2) => B"00",
      S(1) => \caracter[1]_i_315_n_0\,
      S(0) => \caracter[1]_i_316_n_0\
    );
\caracter_reg[1]_i_285\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_317_n_0\,
      CO(3) => \caracter_reg[1]_i_285_n_0\,
      CO(2) => \caracter_reg[1]_i_285_n_1\,
      CO(1) => \caracter_reg[1]_i_285_n_2\,
      CO(0) => \caracter_reg[1]_i_285_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[1]_i_318_n_0\,
      DI(2) => \caracter[1]_i_319_n_0\,
      DI(1) => \caracter[1]_i_320_n_0\,
      DI(0) => \caracter[1]_i_321_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[1]_i_285_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[1]_i_322_n_0\,
      S(2) => \caracter[1]_i_323_n_0\,
      S(1) => \caracter[1]_i_324_n_0\,
      S(0) => \caracter[1]_i_325_n_0\
    );
\caracter_reg[1]_i_294\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[1]_i_294_n_0\,
      CO(2) => \caracter_reg[1]_i_294_n_1\,
      CO(1) => \caracter_reg[1]_i_294_n_2\,
      CO(0) => \caracter_reg[1]_i_294_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => \caracter_reg[3]_i_126_0\(6 downto 4),
      DI(0) => '0',
      O(3) => \caracter_reg[1]_i_294_n_4\,
      O(2) => \caracter_reg[1]_i_294_n_5\,
      O(1) => \caracter_reg[1]_i_294_n_6\,
      O(0) => \caracter_reg[1]_i_294_n_7\,
      S(3) => \caracter[1]_i_330_n_0\,
      S(2) => \caracter[1]_i_331_n_0\,
      S(1) => \caracter[1]_i_332_n_0\,
      S(0) => \caracter_reg[3]_i_126_0\(3)
    );
\caracter_reg[1]_i_303\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_333_n_0\,
      CO(3) => \caracter_reg[1]_i_303_n_0\,
      CO(2) => \caracter_reg[1]_i_303_n_1\,
      CO(1) => \caracter_reg[1]_i_303_n_2\,
      CO(0) => \caracter_reg[1]_i_303_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[1]_i_334_n_0\,
      DI(2) => \caracter[1]_i_335_n_0\,
      DI(1) => \caracter[1]_i_336_n_0\,
      DI(0) => \caracter[1]_i_337_n_0\,
      O(3) => \caracter_reg[1]_i_303_n_4\,
      O(2) => \caracter_reg[1]_i_303_n_5\,
      O(1) => \caracter_reg[1]_i_303_n_6\,
      O(0) => \caracter_reg[1]_i_303_n_7\,
      S(3) => \caracter[1]_i_338_n_0\,
      S(2) => \caracter[1]_i_339_n_0\,
      S(1) => \caracter[1]_i_340_n_0\,
      S(0) => \caracter[1]_i_341_n_0\
    );
\caracter_reg[1]_i_310\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[1]_i_310_n_0\,
      CO(2) => \caracter_reg[1]_i_310_n_1\,
      CO(1) => \caracter_reg[1]_i_310_n_2\,
      CO(0) => \caracter_reg[1]_i_310_n_3\,
      CYINIT => '0',
      DI(3) => \caracter_reg[3]_i_126_0\(0),
      DI(2 downto 0) => B"001",
      O(3) => \caracter_reg[1]_i_310_n_4\,
      O(2) => \caracter_reg[1]_i_310_n_5\,
      O(1) => \caracter_reg[1]_i_310_n_6\,
      O(0) => \NLW_caracter_reg[1]_i_310_O_UNCONNECTED\(0),
      S(3) => \caracter[1]_i_342_n_0\,
      S(2) => \caracter[1]_i_343_n_0\,
      S(1) => \caracter[1]_i_344_n_0\,
      S(0) => \caracter_reg[3]_i_126_0\(0)
    );
\caracter_reg[1]_i_317\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[1]_i_317_n_0\,
      CO(2) => \caracter_reg[1]_i_317_n_1\,
      CO(1) => \caracter_reg[1]_i_317_n_2\,
      CO(0) => \caracter_reg[1]_i_317_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[1]_i_345_n_0\,
      DI(2) => \caracter[1]_i_346_n_0\,
      DI(1) => \caracter[1]_i_347_n_0\,
      DI(0) => \caracter[1]_i_348_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[1]_i_317_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[1]_i_349_n_0\,
      S(2) => \caracter[1]_i_350_n_0\,
      S(1) => \caracter[1]_i_351_n_0\,
      S(0) => \caracter[1]_i_352_n_0\
    );
\caracter_reg[1]_i_333\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[1]_i_333_n_0\,
      CO(2) => \caracter_reg[1]_i_333_n_1\,
      CO(1) => \caracter_reg[1]_i_333_n_2\,
      CO(0) => \caracter_reg[1]_i_333_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[1]_i_356_n_0\,
      DI(2) => \caracter_reg[3]_i_126_0\(0),
      DI(1 downto 0) => B"01",
      O(3) => \caracter_reg[1]_i_333_n_4\,
      O(2) => \caracter_reg[1]_i_333_n_5\,
      O(1) => \caracter_reg[1]_i_333_n_6\,
      O(0) => \caracter_reg[1]_i_333_n_7\,
      S(3) => \caracter[1]_i_357_n_0\,
      S(2) => \caracter[1]_i_358_n_0\,
      S(1) => \caracter[1]_i_359_n_0\,
      S(0) => \caracter_reg[3]_i_126_0\(0)
    );
\caracter_reg[1]_i_353\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_354_n_0\,
      CO(3) => \NLW_caracter_reg[1]_i_353_CO_UNCONNECTED\(3),
      CO(2) => \caracter_reg[1]_i_353_n_1\,
      CO(1) => \NLW_caracter_reg[1]_i_353_CO_UNCONNECTED\(1),
      CO(0) => \caracter_reg[1]_i_353_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 2) => \NLW_caracter_reg[1]_i_353_O_UNCONNECTED\(3 downto 2),
      O(1) => \caracter_reg[1]_i_353_n_6\,
      O(0) => \caracter_reg[1]_i_353_n_7\,
      S(3 downto 2) => B"01",
      S(1 downto 0) => \caracter_reg[3]_i_126_0\(7 downto 6)
    );
\caracter_reg[1]_i_354\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_360_n_0\,
      CO(3) => \caracter_reg[1]_i_354_n_0\,
      CO(2) => \caracter_reg[1]_i_354_n_1\,
      CO(1) => \caracter_reg[1]_i_354_n_2\,
      CO(0) => \caracter_reg[1]_i_354_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \caracter_reg[3]_i_126_0\(3),
      DI(0) => \caracter[1]_i_361_n_0\,
      O(3) => \caracter_reg[1]_i_354_n_4\,
      O(2) => \caracter_reg[1]_i_354_n_5\,
      O(1) => \caracter_reg[1]_i_354_n_6\,
      O(0) => \caracter_reg[1]_i_354_n_7\,
      S(3 downto 2) => \caracter_reg[3]_i_126_0\(5 downto 4),
      S(1) => \caracter[1]_i_362_n_0\,
      S(0) => \caracter[1]_i_363_n_0\
    );
\caracter_reg[1]_i_360\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_364_n_0\,
      CO(3) => \caracter_reg[1]_i_360_n_0\,
      CO(2) => \caracter_reg[1]_i_360_n_1\,
      CO(1) => \caracter_reg[1]_i_360_n_2\,
      CO(0) => \caracter_reg[1]_i_360_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[1]_i_365_n_0\,
      DI(2 downto 0) => \caracter_reg[3]_i_126_0\(7 downto 5),
      O(3) => \caracter_reg[1]_i_360_n_4\,
      O(2 downto 0) => \NLW_caracter_reg[1]_i_360_O_UNCONNECTED\(2 downto 0),
      S(3) => \caracter[1]_i_366_n_0\,
      S(2) => \caracter[1]_i_367_n_0\,
      S(1) => \caracter[1]_i_368_n_0\,
      S(0) => \caracter[1]_i_369_n_0\
    );
\caracter_reg[1]_i_364\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[1]_i_364_n_0\,
      CO(2) => \caracter_reg[1]_i_364_n_1\,
      CO(1) => \caracter_reg[1]_i_364_n_2\,
      CO(0) => \caracter_reg[1]_i_364_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => \caracter_reg[3]_i_126_0\(4 downto 2),
      DI(0) => '0',
      O(3 downto 0) => \NLW_caracter_reg[1]_i_364_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[1]_i_370_n_0\,
      S(2) => \caracter[1]_i_371_n_0\,
      S(1) => \caracter[1]_i_372_n_0\,
      S(0) => \caracter_reg[3]_i_126_0\(1)
    );
\caracter_reg[1]_i_38\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3 downto 2) => \NLW_caracter_reg[1]_i_38_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \caracter_reg[1]_i_38_n_2\,
      CO(0) => \caracter_reg[1]_i_38_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \caracter_reg[1]_i_44_n_6\,
      DI(0) => '0',
      O(3) => \NLW_caracter_reg[1]_i_38_O_UNCONNECTED\(3),
      O(2) => \caracter_reg[1]_i_38_n_5\,
      O(1) => \caracter_reg[1]_i_38_n_6\,
      O(0) => \caracter_reg[1]_i_38_n_7\,
      S(3) => '0',
      S(2) => \caracter[1]_i_45_n_0\,
      S(1) => \caracter[1]_i_46_n_0\,
      S(0) => \caracter_reg[1]_i_44_n_7\
    );
\caracter_reg[1]_i_39\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_47_n_0\,
      CO(3) => \caracter_reg[1]_i_39_n_0\,
      CO(2) => \caracter_reg[1]_i_39_n_1\,
      CO(1) => \caracter_reg[1]_i_39_n_2\,
      CO(0) => \caracter_reg[1]_i_39_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[1]_i_48_n_0\,
      DI(2) => \caracter[1]_i_49_n_0\,
      DI(1) => \caracter[1]_i_50_n_0\,
      DI(0) => \caracter[1]_i_51_n_0\,
      O(3) => \caracter_reg[1]_i_39_n_4\,
      O(2 downto 0) => \NLW_caracter_reg[1]_i_39_O_UNCONNECTED\(2 downto 0),
      S(3) => \caracter[1]_i_52_n_0\,
      S(2) => \caracter[1]_i_53_n_0\,
      S(1) => \caracter[1]_i_54_n_0\,
      S(0) => \caracter[1]_i_55_n_0\
    );
\caracter_reg[1]_i_40\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[1]_i_40_n_0\,
      CO(2) => \caracter_reg[1]_i_40_n_1\,
      CO(1) => \caracter_reg[1]_i_40_n_2\,
      CO(0) => \caracter_reg[1]_i_40_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => \caracter_reg[3]_i_126_0\(3 downto 0),
      O(3) => \caracter_reg[1]_i_40_n_4\,
      O(2) => \caracter_reg[1]_i_40_n_5\,
      O(1 downto 0) => p_1_in(1 downto 0),
      S(3) => \caracter[1]_i_56_n_0\,
      S(2) => \caracter[1]_i_57_n_0\,
      S(1) => \caracter[1]_i_58_n_0\,
      S(0) => \caracter[1]_i_59_n_0\
    );
\caracter_reg[1]_i_44\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_39_n_0\,
      CO(3 downto 2) => \NLW_caracter_reg[1]_i_44_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \caracter_reg[1]_i_44_n_2\,
      CO(0) => \caracter_reg[1]_i_44_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \caracter[1]_i_61_n_0\,
      DI(0) => \caracter[1]_i_62_n_0\,
      O(3) => \NLW_caracter_reg[1]_i_44_O_UNCONNECTED\(3),
      O(2) => \caracter_reg[1]_i_44_n_5\,
      O(1) => \caracter_reg[1]_i_44_n_6\,
      O(0) => \caracter_reg[1]_i_44_n_7\,
      S(3) => '0',
      S(2) => \caracter[1]_i_63_n_0\,
      S(1) => \caracter[1]_i_64_n_0\,
      S(0) => \caracter[1]_i_65_n_0\
    );
\caracter_reg[1]_i_47\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_66_n_0\,
      CO(3) => \caracter_reg[1]_i_47_n_0\,
      CO(2) => \caracter_reg[1]_i_47_n_1\,
      CO(1) => \caracter_reg[1]_i_47_n_2\,
      CO(0) => \caracter_reg[1]_i_47_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[1]_i_67_n_0\,
      DI(2) => \caracter[1]_i_68_n_0\,
      DI(1) => \caracter[1]_i_69_n_0\,
      DI(0) => \caracter[1]_i_70_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[1]_i_47_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[1]_i_71_n_0\,
      S(2) => \caracter[1]_i_72_n_0\,
      S(1) => \caracter[1]_i_73_n_0\,
      S(0) => \caracter[1]_i_74_n_0\
    );
\caracter_reg[1]_i_60\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_40_n_0\,
      CO(3) => \NLW_caracter_reg[1]_i_60_CO_UNCONNECTED\(3),
      CO(2) => \caracter_reg[1]_i_60_n_1\,
      CO(1) => \caracter_reg[1]_i_60_n_2\,
      CO(0) => \caracter_reg[1]_i_60_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => \caracter_reg[3]_i_126_0\(6 downto 4),
      O(3) => \caracter_reg[1]_i_60_n_4\,
      O(2) => \caracter_reg[1]_i_60_n_5\,
      O(1) => \caracter_reg[1]_i_60_n_6\,
      O(0) => \caracter_reg[1]_i_60_n_7\,
      S(3) => \caracter[1]_i_86_n_0\,
      S(2) => \caracter[1]_i_87_n_0\,
      S(1) => \caracter[1]_i_88_n_0\,
      S(0) => \caracter[1]_i_89_n_0\
    );
\caracter_reg[1]_i_66\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_92_n_0\,
      CO(3) => \caracter_reg[1]_i_66_n_0\,
      CO(2) => \caracter_reg[1]_i_66_n_1\,
      CO(1) => \caracter_reg[1]_i_66_n_2\,
      CO(0) => \caracter_reg[1]_i_66_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[1]_i_93_n_0\,
      DI(2) => \caracter[1]_i_94_n_0\,
      DI(1) => \caracter[1]_i_95_n_0\,
      DI(0) => \caracter[1]_i_96_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[1]_i_66_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[1]_i_97_n_0\,
      S(2) => \caracter[1]_i_98_n_0\,
      S(1) => \caracter[1]_i_99_n_0\,
      S(0) => \caracter[1]_i_100_n_0\
    );
\caracter_reg[1]_i_75\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_104_n_0\,
      CO(3) => \caracter_reg[1]_i_75_n_0\,
      CO(2) => \caracter_reg[1]_i_75_n_1\,
      CO(1) => \caracter_reg[1]_i_75_n_2\,
      CO(0) => \caracter_reg[1]_i_75_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[1]_i_109_n_0\,
      DI(2) => \caracter[1]_i_110_n_0\,
      DI(1) => \caracter[1]_i_111_n_0\,
      DI(0) => \caracter[1]_i_112_n_0\,
      O(3) => \caracter_reg[1]_i_75_n_4\,
      O(2) => \caracter_reg[1]_i_75_n_5\,
      O(1) => \caracter_reg[1]_i_75_n_6\,
      O(0) => \caracter_reg[1]_i_75_n_7\,
      S(3) => \caracter[1]_i_113_n_0\,
      S(2) => \caracter[1]_i_114_n_0\,
      S(1) => \caracter[1]_i_115_n_0\,
      S(0) => \caracter[1]_i_116_n_0\
    );
\caracter_reg[1]_i_76\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_103_n_0\,
      CO(3 downto 1) => \NLW_caracter_reg[1]_i_76_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \caracter_reg[1]_i_76_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_caracter_reg[1]_i_76_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
\caracter_reg[1]_i_77\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[1]_i_77_n_0\,
      CO(2) => \caracter_reg[1]_i_77_n_1\,
      CO(1) => \caracter_reg[1]_i_77_n_2\,
      CO(0) => \caracter_reg[1]_i_77_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[1]_i_117_n_0\,
      DI(2) => \caracter[1]_i_118_n_0\,
      DI(1) => \caracter[1]_i_119_n_0\,
      DI(0) => '0',
      O(3) => \caracter_reg[1]_i_77_n_4\,
      O(2) => \caracter_reg[1]_i_77_n_5\,
      O(1) => \caracter_reg[1]_i_77_n_6\,
      O(0) => \caracter_reg[1]_i_77_n_7\,
      S(3) => \caracter[1]_i_120_n_0\,
      S(2) => \caracter[1]_i_121_n_0\,
      S(1) => \caracter[1]_i_122_n_0\,
      S(0) => \caracter[1]_i_123_n_0\
    );
\caracter_reg[1]_i_78\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_124_n_0\,
      CO(3) => \NLW_caracter_reg[1]_i_78_CO_UNCONNECTED\(3),
      CO(2) => \caracter_reg[1]_i_78_n_1\,
      CO(1) => \NLW_caracter_reg[1]_i_78_CO_UNCONNECTED\(1),
      CO(0) => \caracter_reg[1]_i_78_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \caracter[1]_i_125_n_0\,
      DI(0) => \caracter[1]_i_126_n_0\,
      O(3 downto 2) => \NLW_caracter_reg[1]_i_78_O_UNCONNECTED\(3 downto 2),
      O(1) => \caracter_reg[1]_i_78_n_6\,
      O(0) => \caracter_reg[1]_i_78_n_7\,
      S(3 downto 2) => B"01",
      S(1) => \caracter[1]_i_127_n_0\,
      S(0) => \caracter[1]_i_128_n_0\
    );
\caracter_reg[1]_i_79\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_75_n_0\,
      CO(3) => \NLW_caracter_reg[1]_i_79_CO_UNCONNECTED\(3),
      CO(2) => \caracter_reg[1]_i_79_n_1\,
      CO(1) => \NLW_caracter_reg[1]_i_79_CO_UNCONNECTED\(1),
      CO(0) => \caracter_reg[1]_i_79_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \caracter[1]_i_129_n_0\,
      DI(0) => \caracter[1]_i_130_n_0\,
      O(3 downto 2) => \NLW_caracter_reg[1]_i_79_O_UNCONNECTED\(3 downto 2),
      O(1) => \caracter_reg[1]_i_79_n_6\,
      O(0) => \caracter_reg[1]_i_79_n_7\,
      S(3 downto 2) => B"01",
      S(1) => \caracter[1]_i_131_n_0\,
      S(0) => \caracter[1]_i_132_n_0\
    );
\caracter_reg[1]_i_80\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_77_n_0\,
      CO(3) => \caracter_reg[1]_i_80_n_0\,
      CO(2) => \caracter_reg[1]_i_80_n_1\,
      CO(1) => \caracter_reg[1]_i_80_n_2\,
      CO(0) => \caracter_reg[1]_i_80_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[1]_i_133_n_0\,
      DI(2) => \caracter[1]_i_134_n_0\,
      DI(1) => \caracter[1]_i_135_n_0\,
      DI(0) => \caracter[1]_i_136_n_0\,
      O(3) => \caracter_reg[1]_i_80_n_4\,
      O(2) => \caracter_reg[1]_i_80_n_5\,
      O(1) => \caracter_reg[1]_i_80_n_6\,
      O(0) => \caracter_reg[1]_i_80_n_7\,
      S(3) => \caracter[1]_i_137_n_0\,
      S(2) => \caracter[1]_i_138_n_0\,
      S(1) => \caracter[1]_i_139_n_0\,
      S(0) => \caracter[1]_i_140_n_0\
    );
\caracter_reg[1]_i_85\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_141_n_0\,
      CO(3) => \caracter_reg[1]_i_85_n_0\,
      CO(2) => \caracter_reg[1]_i_85_n_1\,
      CO(1) => \caracter_reg[1]_i_85_n_2\,
      CO(0) => \caracter_reg[1]_i_85_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[1]_i_142_n_0\,
      DI(2) => \caracter[1]_i_143_n_0\,
      DI(1) => \caracter[1]_i_144_n_0\,
      DI(0) => \caracter[1]_i_145_n_0\,
      O(3) => \caracter_reg[1]_i_85_n_4\,
      O(2) => \caracter_reg[1]_i_85_n_5\,
      O(1) => \caracter_reg[1]_i_85_n_6\,
      O(0) => \caracter_reg[1]_i_85_n_7\,
      S(3) => \caracter[1]_i_146_n_0\,
      S(2) => \caracter[1]_i_147_n_0\,
      S(1) => \caracter[1]_i_148_n_0\,
      S(0) => \caracter[1]_i_149_n_0\
    );
\caracter_reg[1]_i_90\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_80_n_0\,
      CO(3 downto 0) => \NLW_caracter_reg[1]_i_90_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_caracter_reg[1]_i_90_O_UNCONNECTED\(3 downto 1),
      O(0) => \caracter_reg[1]_i_90_n_7\,
      S(3 downto 1) => B"000",
      S(0) => \caracter[1]_i_151_n_0\
    );
\caracter_reg[1]_i_92\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[1]_i_152_n_0\,
      CO(3) => \caracter_reg[1]_i_92_n_0\,
      CO(2) => \caracter_reg[1]_i_92_n_1\,
      CO(1) => \caracter_reg[1]_i_92_n_2\,
      CO(0) => \caracter_reg[1]_i_92_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[1]_i_153_n_0\,
      DI(2) => \caracter[1]_i_154_n_0\,
      DI(1) => \caracter[1]_i_155_n_0\,
      DI(0) => \caracter[1]_i_156_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[1]_i_92_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[1]_i_157_n_0\,
      S(2) => \caracter[1]_i_158_n_0\,
      S(1) => \caracter[1]_i_159_n_0\,
      S(0) => \caracter[1]_i_160_n_0\
    );
\caracter_reg[2]_i_22\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[2]_i_22_n_0\,
      CO(2) => \caracter_reg[2]_i_22_n_1\,
      CO(1) => \caracter_reg[2]_i_22_n_2\,
      CO(0) => \caracter_reg[2]_i_22_n_3\,
      CYINIT => '1',
      DI(3 downto 1) => cent(3 downto 1),
      DI(0) => \disp_dinero[2]_0\(0),
      O(3) => \caracter_reg[2]_i_22_n_4\,
      O(2) => \caracter_reg[2]_i_22_n_5\,
      O(1) => \string_cent_decenas[1]5\(1),
      O(0) => \NLW_caracter_reg[2]_i_22_O_UNCONNECTED\(0),
      S(3) => \caracter[2]_i_31_n_0\,
      S(2) => \caracter[2]_i_32_n_0\,
      S(1) => \caracter[2]_i_33_n_0\,
      S(0) => \caracter[2]_i_34_n_0\
    );
\caracter_reg[3]_i_108\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[3]_i_108_n_0\,
      CO(2) => \caracter_reg[3]_i_108_n_1\,
      CO(1) => \caracter_reg[3]_i_108_n_2\,
      CO(0) => \caracter_reg[3]_i_108_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[3]_i_148_n_0\,
      DI(2) => \caracter[3]_i_149_n_0\,
      DI(1) => \caracter[3]_i_150_n_0\,
      DI(0) => '0',
      O(3) => \caracter_reg[3]_i_108_n_4\,
      O(2) => \caracter_reg[3]_i_108_n_5\,
      O(1) => \caracter_reg[3]_i_108_n_6\,
      O(0) => \caracter_reg[3]_i_108_n_7\,
      S(3) => \caracter[3]_i_151_n_0\,
      S(2) => \caracter[3]_i_152_n_0\,
      S(1) => \caracter[3]_i_153_n_0\,
      S(0) => \caracter[3]_i_154_n_0\
    );
\caracter_reg[3]_i_117\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[3]_i_117_n_0\,
      CO(2) => \caracter_reg[3]_i_117_n_1\,
      CO(1) => \caracter_reg[3]_i_117_n_2\,
      CO(0) => \caracter_reg[3]_i_117_n_3\,
      CYINIT => '0',
      DI(3) => \^total_string_reg[0]\,
      DI(2) => \caracter[3]_i_155_n_0\,
      DI(1 downto 0) => B"01",
      O(3) => \caracter_reg[3]_i_117_n_4\,
      O(2) => \caracter_reg[3]_i_117_n_5\,
      O(1) => \caracter_reg[3]_i_117_n_6\,
      O(0) => \NLW_caracter_reg[3]_i_117_O_UNCONNECTED\(0),
      S(3) => \caracter[3]_i_156_n_0\,
      S(2) => \caracter[3]_i_157_n_0\,
      S(1) => \caracter[3]_i_158_n_0\,
      S(0) => \caracter[3]_i_159_n_0\
    );
\caracter_reg[3]_i_126\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[3]_i_126_n_0\,
      CO(2) => \caracter_reg[3]_i_126_n_1\,
      CO(1) => \caracter_reg[3]_i_126_n_2\,
      CO(0) => \caracter_reg[3]_i_126_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[3]_i_160_n_0\,
      DI(2 downto 0) => B"001",
      O(3) => \caracter_reg[3]_i_126_n_4\,
      O(2) => \caracter_reg[3]_i_126_n_5\,
      O(1) => \caracter_reg[3]_i_126_n_6\,
      O(0) => \NLW_caracter_reg[3]_i_126_O_UNCONNECTED\(0),
      S(3) => \caracter[3]_i_161_n_0\,
      S(2) => \caracter[3]_i_162_n_0\,
      S(1) => \caracter[3]_i_163_n_0\,
      S(0) => \caracter[3]_i_164_n_0\
    );
\caracter_reg[3]_i_135\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[3]_i_135_n_0\,
      CO(2) => \caracter_reg[3]_i_135_n_1\,
      CO(1) => \caracter_reg[3]_i_135_n_2\,
      CO(0) => \caracter_reg[3]_i_135_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[3]_i_165_n_0\,
      DI(2) => \caracter[3]_i_166_n_0\,
      DI(1) => \caracter[3]_i_167_n_0\,
      DI(0) => \caracter[3]_i_168_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[3]_i_135_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[3]_i_169_n_0\,
      S(2) => \caracter[3]_i_170_n_0\,
      S(1) => \caracter[3]_i_171_n_0\,
      S(0) => \caracter[3]_i_172_n_0\
    );
\caracter_reg[3]_i_173\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[3]_i_174_n_0\,
      CO(3 downto 2) => \NLW_caracter_reg[3]_i_173_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \caracter_reg[3]_i_173_n_2\,
      CO(0) => \NLW_caracter_reg[3]_i_173_CO_UNCONNECTED\(0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_caracter_reg[3]_i_173_O_UNCONNECTED\(3 downto 1),
      O(0) => \caracter_reg[3]_i_173_n_7\,
      S(3 downto 1) => B"001",
      S(0) => \caracter[3]_i_177_n_0\
    );
\caracter_reg[3]_i_174\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[3]_i_176_n_0\,
      CO(3) => \caracter_reg[3]_i_174_n_0\,
      CO(2) => \caracter_reg[3]_i_174_n_1\,
      CO(1) => \caracter_reg[3]_i_174_n_2\,
      CO(0) => \caracter_reg[3]_i_174_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \caracter[3]_i_178_n_0\,
      O(3) => \caracter_reg[3]_i_174_n_4\,
      O(2) => \caracter_reg[3]_i_174_n_5\,
      O(1) => \caracter_reg[3]_i_174_n_6\,
      O(0) => \caracter_reg[3]_i_174_n_7\,
      S(3) => \caracter[3]_i_179_n_0\,
      S(2) => \caracter[3]_i_180_n_0\,
      S(1) => \caracter[3]_i_181_n_0\,
      S(0) => \caracter[3]_i_182_n_0\
    );
\caracter_reg[3]_i_176\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[3]_i_183_n_0\,
      CO(3) => \caracter_reg[3]_i_176_n_0\,
      CO(2) => \caracter_reg[3]_i_176_n_1\,
      CO(1) => \caracter_reg[3]_i_176_n_2\,
      CO(0) => \caracter_reg[3]_i_176_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[3]_i_184_n_0\,
      DI(2) => \caracter[3]_i_185_n_0\,
      DI(1) => \caracter[3]_i_186_n_0\,
      DI(0) => \caracter[3]_i_187_n_0\,
      O(3) => \caracter_reg[3]_i_176_n_4\,
      O(2 downto 0) => \NLW_caracter_reg[3]_i_176_O_UNCONNECTED\(2 downto 0),
      S(3) => \caracter[3]_i_188_n_0\,
      S(2) => \caracter[3]_i_189_n_0\,
      S(1) => \caracter[3]_i_190_n_0\,
      S(0) => \caracter[3]_i_191_n_0\
    );
\caracter_reg[3]_i_183\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \caracter_reg[3]_i_183_n_0\,
      CO(2) => \caracter_reg[3]_i_183_n_1\,
      CO(1) => \caracter_reg[3]_i_183_n_2\,
      CO(0) => \caracter_reg[3]_i_183_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[3]_i_192_n_0\,
      DI(2) => \caracter[3]_i_193_n_0\,
      DI(1) => \caracter[3]_i_194_n_0\,
      DI(0) => '0',
      O(3 downto 0) => \NLW_caracter_reg[3]_i_183_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[3]_i_195_n_0\,
      S(2) => \caracter[3]_i_196_n_0\,
      S(1) => \caracter[3]_i_197_n_0\,
      S(0) => \caracter[3]_i_198_n_0\
    );
\caracter_reg[3]_i_21\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[2]_i_22_n_0\,
      CO(3) => \NLW_caracter_reg[3]_i_21_CO_UNCONNECTED\(3),
      CO(2) => \caracter_reg[3]_i_21_n_1\,
      CO(1) => \caracter_reg[3]_i_21_n_2\,
      CO(0) => \caracter_reg[3]_i_21_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 1) => cent(6 downto 5),
      DI(0) => \caracter[3]_i_33_n_0\,
      O(3) => \caracter_reg[3]_i_21_n_4\,
      O(2) => \caracter_reg[3]_i_21_n_5\,
      O(1) => \caracter_reg[3]_i_21_n_6\,
      O(0) => \caracter_reg[3]_i_21_n_7\,
      S(3) => \caracter[3]_i_34_n_0\,
      S(2) => \caracter[3]_i_35_n_0\,
      S(1) => \caracter[3]_i_36_n_0\,
      S(0) => \caracter[3]_i_37_n_0\
    );
\caracter_reg[3]_i_44\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3 downto 2) => \NLW_caracter_reg[3]_i_44_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \caracter_reg[3]_i_44_n_2\,
      CO(0) => \caracter_reg[3]_i_44_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \caracter[3]_i_48_n_0\,
      DI(0) => '0',
      O(3) => \NLW_caracter_reg[3]_i_44_O_UNCONNECTED\(3),
      O(2) => \caracter_reg[3]_i_44_n_5\,
      O(1) => \caracter_reg[3]_i_44_n_6\,
      O(0) => \caracter_reg[3]_i_44_n_7\,
      S(3) => '0',
      S(2) => \caracter[3]_i_49_n_0\,
      S(1) => \caracter[3]_i_50_n_0\,
      S(0) => \caracter[3]_i_51_n_0\
    );
\caracter_reg[3]_i_45\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[3]_i_52_n_0\,
      CO(3) => \caracter_reg[3]_i_45_n_0\,
      CO(2) => \caracter_reg[3]_i_45_n_1\,
      CO(1) => \caracter_reg[3]_i_45_n_2\,
      CO(0) => \caracter_reg[3]_i_45_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \caracter[3]_i_53_n_0\,
      DI(1) => \caracter[3]_i_54_n_0\,
      DI(0) => \caracter[3]_i_55_n_0\,
      O(3) => \caracter_reg[3]_i_45_n_4\,
      O(2) => \caracter_reg[3]_i_45_n_5\,
      O(1) => \caracter_reg[3]_i_45_n_6\,
      O(0) => \caracter_reg[3]_i_45_n_7\,
      S(3) => \caracter[3]_i_56_n_0\,
      S(2) => \caracter[3]_i_57_n_0\,
      S(1) => \caracter[3]_i_58_n_0\,
      S(0) => \caracter[3]_i_59_n_0\
    );
\caracter_reg[3]_i_52\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[3]_i_61_n_0\,
      CO(3) => \caracter_reg[3]_i_52_n_0\,
      CO(2) => \caracter_reg[3]_i_52_n_1\,
      CO(1) => \caracter_reg[3]_i_52_n_2\,
      CO(0) => \caracter_reg[3]_i_52_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[3]_i_62_n_0\,
      DI(2) => \caracter[3]_i_63_n_0\,
      DI(1) => \caracter[3]_i_64_n_0\,
      DI(0) => \caracter[3]_i_65_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[3]_i_52_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[3]_i_66_n_0\,
      S(2) => \caracter[3]_i_67_n_0\,
      S(1) => \caracter[3]_i_68_n_0\,
      S(0) => \caracter[3]_i_69_n_0\
    );
\caracter_reg[3]_i_60\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[3]_i_45_n_0\,
      CO(3 downto 1) => \NLW_caracter_reg[3]_i_60_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \caracter_reg[3]_i_60_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 2) => \NLW_caracter_reg[3]_i_60_O_UNCONNECTED\(3 downto 2),
      O(1) => \caracter_reg[3]_i_60_n_6\,
      O(0) => \caracter_reg[3]_i_60_n_7\,
      S(3 downto 2) => B"00",
      S(1) => \caracter[3]_i_73_n_0\,
      S(0) => \caracter[3]_i_74_n_0\
    );
\caracter_reg[3]_i_61\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[3]_i_75_n_0\,
      CO(3) => \caracter_reg[3]_i_61_n_0\,
      CO(2) => \caracter_reg[3]_i_61_n_1\,
      CO(1) => \caracter_reg[3]_i_61_n_2\,
      CO(0) => \caracter_reg[3]_i_61_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[3]_i_76_n_0\,
      DI(2) => \caracter[3]_i_77_n_0\,
      DI(1) => \caracter[3]_i_78_n_0\,
      DI(0) => \caracter[3]_i_79_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[3]_i_61_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[3]_i_80_n_0\,
      S(2) => \caracter[3]_i_81_n_0\,
      S(1) => \caracter[3]_i_82_n_0\,
      S(0) => \caracter[3]_i_83_n_0\
    );
\caracter_reg[3]_i_70\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[3]_i_84_n_0\,
      CO(3) => \NLW_caracter_reg[3]_i_70_CO_UNCONNECTED\(3),
      CO(2) => \caracter_reg[3]_i_70_n_1\,
      CO(1) => \NLW_caracter_reg[3]_i_70_CO_UNCONNECTED\(1),
      CO(0) => \caracter_reg[3]_i_70_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \caracter[3]_i_85_n_0\,
      O(3 downto 2) => \NLW_caracter_reg[3]_i_70_O_UNCONNECTED\(3 downto 2),
      O(1) => \caracter_reg[3]_i_70_n_6\,
      O(0) => \caracter_reg[3]_i_70_n_7\,
      S(3 downto 2) => B"01",
      S(1) => \caracter[3]_i_86_n_0\,
      S(0) => \caracter[3]_i_87_n_0\
    );
\caracter_reg[3]_i_71\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[3]_i_88_n_0\,
      CO(3) => \caracter_reg[3]_i_71_n_0\,
      CO(2) => \NLW_caracter_reg[3]_i_71_CO_UNCONNECTED\(2),
      CO(1) => \caracter_reg[3]_i_71_n_2\,
      CO(0) => \caracter_reg[3]_i_71_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"01",
      DI(1) => \caracter[3]_i_89_n_0\,
      DI(0) => \caracter[3]_i_90_n_0\,
      O(3) => \NLW_caracter_reg[3]_i_71_O_UNCONNECTED\(3),
      O(2) => \caracter_reg[3]_i_71_n_5\,
      O(1) => \caracter_reg[3]_i_71_n_6\,
      O(0) => \caracter_reg[3]_i_71_n_7\,
      S(3) => '1',
      S(2) => \caracter[3]_i_91_n_0\,
      S(1) => \caracter[3]_i_92_n_0\,
      S(0) => \caracter[3]_i_93_n_0\
    );
\caracter_reg[3]_i_72\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[3]_i_94_n_0\,
      CO(3) => \NLW_caracter_reg[3]_i_72_CO_UNCONNECTED\(3),
      CO(2) => \caracter_reg[3]_i_72_n_1\,
      CO(1) => \NLW_caracter_reg[3]_i_72_CO_UNCONNECTED\(1),
      CO(0) => \caracter_reg[3]_i_72_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \caracter[3]_i_95_n_0\,
      DI(0) => \caracter[3]_i_96_n_0\,
      O(3 downto 2) => \NLW_caracter_reg[3]_i_72_O_UNCONNECTED\(3 downto 2),
      O(1) => \caracter_reg[3]_i_72_n_6\,
      O(0) => \caracter_reg[3]_i_72_n_7\,
      S(3 downto 2) => B"01",
      S(1) => \caracter[3]_i_97_n_0\,
      S(0) => \caracter[3]_i_98_n_0\
    );
\caracter_reg[3]_i_75\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[3]_i_99_n_0\,
      CO(3) => \caracter_reg[3]_i_75_n_0\,
      CO(2) => \caracter_reg[3]_i_75_n_1\,
      CO(1) => \caracter_reg[3]_i_75_n_2\,
      CO(0) => \caracter_reg[3]_i_75_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[3]_i_100_n_0\,
      DI(2) => \caracter[3]_i_101_n_0\,
      DI(1) => \caracter[3]_i_102_n_0\,
      DI(0) => \caracter[3]_i_103_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[3]_i_75_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[3]_i_104_n_0\,
      S(2) => \caracter[3]_i_105_n_0\,
      S(1) => \caracter[3]_i_106_n_0\,
      S(0) => \caracter[3]_i_107_n_0\
    );
\caracter_reg[3]_i_84\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[3]_i_108_n_0\,
      CO(3) => \caracter_reg[3]_i_84_n_0\,
      CO(2) => \caracter_reg[3]_i_84_n_1\,
      CO(1) => \caracter_reg[3]_i_84_n_2\,
      CO(0) => \caracter_reg[3]_i_84_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[3]_i_109_n_0\,
      DI(2) => \caracter[3]_i_110_n_0\,
      DI(1) => \caracter[3]_i_111_n_0\,
      DI(0) => \caracter[3]_i_112_n_0\,
      O(3) => \caracter_reg[3]_i_84_n_4\,
      O(2) => \caracter_reg[3]_i_84_n_5\,
      O(1) => \caracter_reg[3]_i_84_n_6\,
      O(0) => \caracter_reg[3]_i_84_n_7\,
      S(3) => \caracter[3]_i_113_n_0\,
      S(2) => \caracter[3]_i_114_n_0\,
      S(1) => \caracter[3]_i_115_n_0\,
      S(0) => \caracter[3]_i_116_n_0\
    );
\caracter_reg[3]_i_88\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[3]_i_117_n_0\,
      CO(3) => \caracter_reg[3]_i_88_n_0\,
      CO(2) => \caracter_reg[3]_i_88_n_1\,
      CO(1) => \caracter_reg[3]_i_88_n_2\,
      CO(0) => \caracter_reg[3]_i_88_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[3]_i_118_n_0\,
      DI(2) => \caracter[3]_i_119_n_0\,
      DI(1) => \caracter[3]_i_120_n_0\,
      DI(0) => \caracter[3]_i_121_n_0\,
      O(3) => \caracter_reg[3]_i_88_n_4\,
      O(2) => \caracter_reg[3]_i_88_n_5\,
      O(1) => \caracter_reg[3]_i_88_n_6\,
      O(0) => \caracter_reg[3]_i_88_n_7\,
      S(3) => \caracter[3]_i_122_n_0\,
      S(2) => \caracter[3]_i_123_n_0\,
      S(1) => \caracter[3]_i_124_n_0\,
      S(0) => \caracter[3]_i_125_n_0\
    );
\caracter_reg[3]_i_94\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[3]_i_126_n_0\,
      CO(3) => \caracter_reg[3]_i_94_n_0\,
      CO(2) => \caracter_reg[3]_i_94_n_1\,
      CO(1) => \caracter_reg[3]_i_94_n_2\,
      CO(0) => \caracter_reg[3]_i_94_n_3\,
      CYINIT => '0',
      DI(3) => cent(4),
      DI(2) => \caracter[3]_i_128_n_0\,
      DI(1) => \caracter[3]_i_129_n_0\,
      DI(0) => \caracter[3]_i_130_n_0\,
      O(3) => \caracter_reg[3]_i_94_n_4\,
      O(2) => \caracter_reg[3]_i_94_n_5\,
      O(1) => \caracter_reg[3]_i_94_n_6\,
      O(0) => \caracter_reg[3]_i_94_n_7\,
      S(3) => \caracter[3]_i_131_n_0\,
      S(2) => \caracter[3]_i_132_n_0\,
      S(1) => \caracter[3]_i_133_n_0\,
      S(0) => \caracter[3]_i_134_n_0\
    );
\caracter_reg[3]_i_99\: unisim.vcomponents.CARRY4
     port map (
      CI => \caracter_reg[3]_i_135_n_0\,
      CO(3) => \caracter_reg[3]_i_99_n_0\,
      CO(2) => \caracter_reg[3]_i_99_n_1\,
      CO(1) => \caracter_reg[3]_i_99_n_2\,
      CO(0) => \caracter_reg[3]_i_99_n_3\,
      CYINIT => '0',
      DI(3) => \caracter[3]_i_136_n_0\,
      DI(2) => \caracter[3]_i_137_n_0\,
      DI(1) => \caracter[3]_i_138_n_0\,
      DI(0) => \caracter[3]_i_139_n_0\,
      O(3 downto 0) => \NLW_caracter_reg[3]_i_99_O_UNCONNECTED\(3 downto 0),
      S(3) => \caracter[3]_i_140_n_0\,
      S(2) => \caracter[3]_i_141_n_0\,
      S(1) => \caracter[3]_i_142_n_0\,
      S(0) => \caracter[3]_i_143_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity synchrnzr is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    monedas_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end synchrnzr;

architecture STRUCTURE of synchrnzr is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  attribute srl_name : string;
  attribute srl_name of sync_out_reg_srl2 : label is "\inst_sincronizador/inst_synchrnzr0/sync_out_reg_srl2 ";
begin
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => monedas_IBUF(0),
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
sync_out_reg_srl2: unisim.vcomponents.SRL16E
    generic map(
      INIT => X"0000"
    )
        port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => clk_BUFG,
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg[0]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity synchrnzr_0 is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    monedas_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of synchrnzr_0 : entity is "synchrnzr";
end synchrnzr_0;

architecture STRUCTURE of synchrnzr_0 is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  attribute srl_name : string;
  attribute srl_name of sync_out_reg_srl2 : label is "\inst_sincronizador/inst_synchrnzr1/sync_out_reg_srl2 ";
begin
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => monedas_IBUF(0),
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
sync_out_reg_srl2: unisim.vcomponents.SRL16E
    generic map(
      INIT => X"0000"
    )
        port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => clk_BUFG,
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg[0]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity synchrnzr_1 is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    monedas_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of synchrnzr_1 : entity is "synchrnzr";
end synchrnzr_1;

architecture STRUCTURE of synchrnzr_1 is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  attribute srl_name : string;
  attribute srl_name of sync_out_reg_srl2 : label is "\inst_sincronizador/inst_synchrnzr2/sync_out_reg_srl2 ";
begin
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => monedas_IBUF(0),
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
sync_out_reg_srl2: unisim.vcomponents.SRL16E
    generic map(
      INIT => X"0000"
    )
        port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => clk_BUFG,
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg[0]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity synchrnzr_2 is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    monedas_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of synchrnzr_2 : entity is "synchrnzr";
end synchrnzr_2;

architecture STRUCTURE of synchrnzr_2 is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  attribute srl_name : string;
  attribute srl_name of sync_out_reg_srl2 : label is "\inst_sincronizador/inst_synchrnzr3/sync_out_reg_srl2 ";
begin
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => monedas_IBUF(0),
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
sync_out_reg_srl2: unisim.vcomponents.SRL16E
    generic map(
      INIT => X"0000"
    )
        port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => clk_BUFG,
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg[0]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity synchrnzr_3 is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    monedas_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of synchrnzr_3 : entity is "synchrnzr";
end synchrnzr_3;

architecture STRUCTURE of synchrnzr_3 is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  attribute srl_name : string;
  attribute srl_name of sync_out_reg_srl2 : label is "\inst_sincronizador/inst_synchrnzr4/sync_out_reg_srl2 ";
begin
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => monedas_IBUF(0),
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
sync_out_reg_srl2: unisim.vcomponents.SRL16E
    generic map(
      INIT => X"0000"
    )
        port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => clk_BUFG,
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg[0]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity synchrnzr_4 is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    ref_option_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of synchrnzr_4 : entity is "synchrnzr";
end synchrnzr_4;

architecture STRUCTURE of synchrnzr_4 is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  attribute srl_name : string;
  attribute srl_name of sync_out_reg_srl2 : label is "\inst_sincronizador/inst_synchrnzr5/sync_out_reg_srl2 ";
begin
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => ref_option_IBUF(0),
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
sync_out_reg_srl2: unisim.vcomponents.SRL16E
    generic map(
      INIT => X"0000"
    )
        port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => clk_BUFG,
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg[0]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity synchrnzr_5 is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    ref_option_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of synchrnzr_5 : entity is "synchrnzr";
end synchrnzr_5;

architecture STRUCTURE of synchrnzr_5 is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  attribute srl_name : string;
  attribute srl_name of sync_out_reg_srl2 : label is "\inst_sincronizador/inst_synchrnzr6/sync_out_reg_srl2 ";
begin
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => ref_option_IBUF(0),
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
sync_out_reg_srl2: unisim.vcomponents.SRL16E
    generic map(
      INIT => X"0000"
    )
        port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => clk_BUFG,
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg[0]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity synchrnzr_6 is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    ref_option_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of synchrnzr_6 : entity is "synchrnzr";
end synchrnzr_6;

architecture STRUCTURE of synchrnzr_6 is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  attribute srl_name : string;
  attribute srl_name of sync_out_reg_srl2 : label is "\inst_sincronizador/inst_synchrnzr7/sync_out_reg_srl2 ";
begin
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => ref_option_IBUF(0),
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
sync_out_reg_srl2: unisim.vcomponents.SRL16E
    generic map(
      INIT => X"0000"
    )
        port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => clk_BUFG,
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg[0]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity synchrnzr_7 is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    ref_option_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of synchrnzr_7 : entity is "synchrnzr";
end synchrnzr_7;

architecture STRUCTURE of synchrnzr_7 is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  attribute srl_name : string;
  attribute srl_name of sync_out_reg_srl2 : label is "\inst_sincronizador/inst_synchrnzr8/sync_out_reg_srl2 ";
begin
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => ref_option_IBUF(0),
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
sync_out_reg_srl2: unisim.vcomponents.SRL16E
    generic map(
      INIT => X"0000"
    )
        port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => clk_BUFG,
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg[0]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity synchrnzr_8 is
  port (
    \sreg_reg[0]_0\ : out STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    button_ok_IBUF : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of synchrnzr_8 : entity is "synchrnzr";
end synchrnzr_8;

architecture STRUCTURE of synchrnzr_8 is
  signal \sreg_reg_n_0_[0]\ : STD_LOGIC;
  attribute srl_name : string;
  attribute srl_name of sync_out_reg_srl2 : label is "\inst_sincronizador/inst_synchrnzr9/sync_out_reg_srl2 ";
begin
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => button_ok_IBUF,
      Q => \sreg_reg_n_0_[0]\,
      R => '0'
    );
sync_out_reg_srl2: unisim.vcomponents.SRL16E
    generic map(
      INIT => X"0000"
    )
        port map (
      A0 => '1',
      A1 => '0',
      A2 => '0',
      A3 => '0',
      CE => '1',
      CLK => clk_BUFG,
      D => \sreg_reg_n_0_[0]\,
      Q => \sreg_reg[0]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity detectorflancos is
  port (
    \disp_reg[4][4]\ : out STD_LOGIC;
    \sreg_reg[2]\ : out STD_LOGIC;
    \sreg_reg[2]_0\ : out STD_LOGIC;
    \sreg_reg[2]_1\ : out STD_LOGIC;
    \sreg_reg[2]_2\ : out STD_LOGIC;
    \disp_reg[4]_1_sp_1\ : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_reg[7][6]\ : out STD_LOGIC;
    \sreg_reg[2]_3\ : out STD_LOGIC;
    \disp_reg[7][3]\ : out STD_LOGIC;
    \sreg_reg[2]_4\ : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    p_0_out : out STD_LOGIC_VECTOR ( 0 to 0 );
    \sreg_reg[0]\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \sreg_reg[0]_0\ : out STD_LOGIC;
    \sreg_reg[1]\ : out STD_LOGIC;
    \disp_reg[8][6]\ : out STD_LOGIC;
    \disp_reg[5][6]\ : out STD_LOGIC;
    \disp_reg[5]_0_sp_1\ : out STD_LOGIC;
    \sreg_reg[0]_1\ : out STD_LOGIC_VECTOR ( 6 downto 0 );
    \sreg_reg[0]_2\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    \sreg_reg[0]_3\ : in STD_LOGIC;
    \sreg_reg[0]_4\ : in STD_LOGIC;
    \sreg_reg[0]_5\ : in STD_LOGIC;
    \sreg_reg[0]_6\ : in STD_LOGIC;
    \sreg_reg[0]_7\ : in STD_LOGIC;
    \sreg_reg[0]_8\ : in STD_LOGIC;
    \sreg_reg[0]_9\ : in STD_LOGIC;
    \sreg_reg[0]_10\ : in STD_LOGIC;
    \sreg_reg[0]_11\ : in STD_LOGIC;
    \disp_reg[4]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_reg[7]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    current_state : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_reg[8]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_reg[5]\ : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
end detectorflancos;

architecture STRUCTURE of detectorflancos is
  signal \disp_reg[4]_1_sn_1\ : STD_LOGIC;
  signal \disp_reg[5]_0_sn_1\ : STD_LOGIC;
  signal inst_edgedtctr0_n_2 : STD_LOGIC;
  signal inst_edgedtctr0_n_3 : STD_LOGIC;
  signal inst_edgedtctr1_n_0 : STD_LOGIC;
  signal inst_edgedtctr2_n_4 : STD_LOGIC;
  signal inst_edgedtctr3_n_2 : STD_LOGIC;
  signal inst_edgedtctr3_n_3 : STD_LOGIC;
  signal inst_edgedtctr4_n_5 : STD_LOGIC;
  signal inst_edgedtctr5_n_2 : STD_LOGIC;
  signal inst_edgedtctr6_n_0 : STD_LOGIC;
  signal inst_edgedtctr6_n_2 : STD_LOGIC;
  signal inst_edgedtctr6_n_3 : STD_LOGIC;
  signal inst_edgedtctr7_n_11 : STD_LOGIC;
  signal inst_edgedtctr7_n_12 : STD_LOGIC;
  signal inst_edgedtctr7_n_6 : STD_LOGIC;
  signal inst_edgedtctr8_n_0 : STD_LOGIC;
  signal inst_edgedtctr8_n_5 : STD_LOGIC;
  signal inst_edgedtctr8_n_6 : STD_LOGIC;
  signal sreg : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal sreg_0 : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \^sreg_reg[2]\ : STD_LOGIC;
  signal \^sreg_reg[2]_0\ : STD_LOGIC;
  signal \^sreg_reg[2]_1\ : STD_LOGIC;
  signal \^sreg_reg[2]_2\ : STD_LOGIC;
begin
  \disp_reg[4]_1_sp_1\ <= \disp_reg[4]_1_sn_1\;
  \disp_reg[5]_0_sp_1\ <= \disp_reg[5]_0_sn_1\;
  \sreg_reg[2]\ <= \^sreg_reg[2]\;
  \sreg_reg[2]_0\ <= \^sreg_reg[2]_0\;
  \sreg_reg[2]_1\ <= \^sreg_reg[2]_1\;
  \sreg_reg[2]_2\ <= \^sreg_reg[2]_2\;
inst_edgedtctr0: entity work.edgedtctr
     port map (
      clk_BUFG => clk_BUFG,
      sreg(2 downto 0) => sreg_0(2 downto 0),
      \sreg_reg[0]_0\ => inst_edgedtctr0_n_3,
      \sreg_reg[0]_1\ => \sreg_reg[0]_2\,
      \sreg_reg[2]_0\(1) => \sreg_reg[0]_1\(2),
      \sreg_reg[2]_0\(0) => \sreg_reg[0]_1\(0),
      \sreg_reg[2]_1\ => inst_edgedtctr0_n_2,
      \valor_moneda_reg[0]\ => inst_edgedtctr3_n_2,
      \valor_moneda_reg[2]\ => inst_edgedtctr4_n_5,
      \valor_moneda_reg[2]_0\ => inst_edgedtctr2_n_4,
      \valor_moneda_reg[2]_1\ => inst_edgedtctr3_n_3,
      \valor_moneda_reg[2]_2\ => inst_edgedtctr1_n_0
    );
inst_edgedtctr1: entity work.edgedtctr_9
     port map (
      clk_BUFG => clk_BUFG,
      \sreg_reg[0]_0\ => \sreg_reg[0]_3\,
      \sreg_reg[2]_0\ => inst_edgedtctr1_n_0
    );
inst_edgedtctr2: entity work.edgedtctr_10
     port map (
      clk_BUFG => clk_BUFG,
      sreg(2 downto 0) => sreg(2 downto 0),
      \sreg_reg[0]_0\(0) => \sreg_reg[0]_1\(3),
      \sreg_reg[0]_1\ => \sreg_reg[0]_4\,
      \sreg_reg[2]_0\ => inst_edgedtctr2_n_4,
      \valor_moneda_reg[3]\ => inst_edgedtctr3_n_3,
      \valor_moneda_reg[3]_0\ => inst_edgedtctr1_n_0,
      \valor_moneda_reg[3]_1\ => inst_edgedtctr0_n_3
    );
inst_edgedtctr3: entity work.edgedtctr_11
     port map (
      clk_BUFG => clk_BUFG,
      sreg(2 downto 0) => sreg(2 downto 0),
      \sreg_reg[0]_0\ => inst_edgedtctr3_n_2,
      \sreg_reg[0]_1\ => \sreg_reg[0]_5\,
      \sreg_reg[2]_0\(1) => \sreg_reg[0]_1\(4),
      \sreg_reg[2]_0\(0) => \sreg_reg[0]_1\(1),
      \sreg_reg[2]_1\ => inst_edgedtctr3_n_3,
      \valor_moneda_reg[1]\ => inst_edgedtctr0_n_3,
      \valor_moneda_reg[1]_0\ => inst_edgedtctr2_n_4,
      \valor_moneda_reg[1]_1\ => inst_edgedtctr1_n_0
    );
inst_edgedtctr4: entity work.edgedtctr_12
     port map (
      clk_BUFG => clk_BUFG,
      sreg(2 downto 0) => sreg_0(2 downto 0),
      \sreg_reg[0]_0\(1 downto 0) => \sreg_reg[0]_1\(6 downto 5),
      \sreg_reg[0]_1\ => \sreg_reg[0]_6\,
      \sreg_reg[2]_0\ => inst_edgedtctr4_n_5,
      \valor_moneda_reg[5]\ => inst_edgedtctr3_n_2,
      \valor_moneda_reg[5]_0\ => inst_edgedtctr0_n_2,
      \valor_moneda_reg[5]_1\ => inst_edgedtctr1_n_0
    );
inst_edgedtctr5: entity work.edgedtctr_13
     port map (
      \FSM_sequential_current_state[1]_i_6__0\ => inst_edgedtctr6_n_0,
      Q(1) => inst_edgedtctr6_n_2,
      Q(0) => inst_edgedtctr6_n_3,
      clk_BUFG => clk_BUFG,
      \disp_reg[4]\(0) => \disp_reg[4]\(0),
      \disp_reg[4][1]\ => \disp_reg[4]_1_sn_1\,
      \disp_reg[5]\(1 downto 0) => \disp_reg[5]\(1 downto 0),
      \disp_reg[5][0]_0\ => \^sreg_reg[2]\,
      \disp_reg[5][0]_1\ => \^sreg_reg[2]_2\,
      \disp_reg[5][0]_2\ => \^sreg_reg[2]_1\,
      \disp_reg[5][6]\ => \disp_reg[5][6]\,
      \disp_reg[5]_0_sp_1\ => \disp_reg[5]_0_sn_1\,
      \disp_reg[8]\(0) => \disp_reg[8]\(0),
      \disp_reg[8][6]\ => \disp_reg[8][6]\,
      \sreg_reg[0]_0\ => inst_edgedtctr5_n_2,
      \sreg_reg[0]_1\ => \sreg_reg[0]_11\,
      \sreg_reg[2]_0\ => \^sreg_reg[2]_0\
    );
inst_edgedtctr6: entity work.edgedtctr_14
     port map (
      Q(1) => inst_edgedtctr6_n_2,
      Q(0) => inst_edgedtctr6_n_3,
      clk_BUFG => clk_BUFG,
      \sreg_reg[0]_0\ => inst_edgedtctr6_n_0,
      \sreg_reg[0]_1\ => \sreg_reg[0]_10\,
      \sreg_reg[2]_0\ => \^sreg_reg[2]_1\
    );
inst_edgedtctr7: entity work.edgedtctr_15
     port map (
      D(1 downto 0) => D(1 downto 0),
      E(0) => E(0),
      \FSM_sequential_current_state[1]_i_6__0\ => inst_edgedtctr8_n_0,
      Q(1) => inst_edgedtctr8_n_5,
      Q(0) => inst_edgedtctr8_n_6,
      clk_BUFG => clk_BUFG,
      \disp_reg[2][0]\ => \^sreg_reg[2]\,
      \disp_reg[2][0]_0\ => \^sreg_reg[2]_0\,
      \disp_reg[2][0]_1\ => \^sreg_reg[2]_1\,
      \disp_reg[3][3]\ => inst_edgedtctr6_n_0,
      \disp_reg[3][3]_0\(1) => inst_edgedtctr6_n_2,
      \disp_reg[3][3]_0\(0) => inst_edgedtctr6_n_3,
      \disp_reg[7]\(0) => \disp_reg[7]\(1),
      \disp_reg[7][6]\ => \disp_reg[7][6]\,
      p_0_out(0) => p_0_out(0),
      \sreg_reg[0]_0\(2 downto 0) => \sreg_reg[0]\(2 downto 0),
      \sreg_reg[0]_1\ => inst_edgedtctr7_n_11,
      \sreg_reg[0]_2\ => inst_edgedtctr7_n_12,
      \sreg_reg[0]_3\ => \sreg_reg[0]_9\,
      \sreg_reg[2]_0\ => \^sreg_reg[2]_2\,
      \sreg_reg[2]_1\ => \sreg_reg[2]_3\,
      \sreg_reg[2]_2\ => inst_edgedtctr7_n_6
    );
inst_edgedtctr8: entity work.edgedtctr_16
     port map (
      Q(1) => inst_edgedtctr8_n_5,
      Q(0) => inst_edgedtctr8_n_6,
      clk_BUFG => clk_BUFG,
      \disp_reg[3][3]\ => \^sreg_reg[2]_0\,
      \disp_reg[3][3]_0\ => inst_edgedtctr7_n_12,
      \disp_reg[4]\(0) => \disp_reg[4]\(1),
      \disp_reg[4][4]\ => \disp_reg[4][4]\,
      \disp_reg[4][4]_0\ => \^sreg_reg[2]_1\,
      \disp_reg[4][4]_1\ => \^sreg_reg[2]_2\,
      \disp_reg[7]\(0) => \disp_reg[7]\(0),
      \disp_reg[7][3]\ => \disp_reg[7][3]\,
      \disp_reg[7][3]_0\ => inst_edgedtctr7_n_6,
      \sreg_reg[0]_0\ => inst_edgedtctr8_n_0,
      \sreg_reg[0]_1\ => \sreg_reg[0]_8\,
      \sreg_reg[2]_0\ => \^sreg_reg[2]\,
      \sreg_reg[2]_1\ => \sreg_reg[2]_4\
    );
inst_edgedtctr9: entity work.edgedtctr_17
     port map (
      \FSM_sequential_current_state[1]_i_4__0\ => inst_edgedtctr5_n_2,
      \FSM_sequential_current_state[1]_i_4__0_0\ => inst_edgedtctr7_n_11,
      clk_BUFG => clk_BUFG,
      current_state(1 downto 0) => current_state(1 downto 0),
      \sreg_reg[0]_0\ => \sreg_reg[0]_0\,
      \sreg_reg[0]_1\ => \sreg_reg[0]_7\,
      \sreg_reg[1]_0\ => \sreg_reg[1]\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity display is
  port (
    dot_OBUF : out STD_LOGIC;
    output0 : out STD_LOGIC;
    \charact_dot_reg[0]_0\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[1]\ : out STD_LOGIC;
    \charact_dot_reg[0]_1\ : out STD_LOGIC;
    \reg_reg[1]\ : out STD_LOGIC;
    \reg_reg[0]\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \charact_dot_reg[0]_2\ : out STD_LOGIC;
    \reg_reg[2]\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[0]\ : out STD_LOGIC;
    \reg_reg[1]_0\ : out STD_LOGIC;
    \disp_reg[1][6]\ : out STD_LOGIC;
    elem_OBUF : out STD_LOGIC_VECTOR ( 7 downto 0 );
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    \reg_reg[0]_0\ : out STD_LOGIC;
    digit_OBUF : out STD_LOGIC_VECTOR ( 6 downto 0 );
    \reg_reg[0]_1\ : out STD_LOGIC;
    dot : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    \caracter_reg[0]_0\ : in STD_LOGIC;
    \caracter_reg[0]_1\ : in STD_LOGIC;
    \disp_reg[1]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    current_state : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \caracter[0]_i_6\ : in STD_LOGIC;
    \disp_dinero[5]_0\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \caracter_reg[1]_0\ : in STD_LOGIC;
    \caracter_reg[1]_1\ : in STD_LOGIC;
    \disp_dinero[3]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter_reg[2]_0\ : in STD_LOGIC;
    \disp_reg[5]\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \disp_reg[4]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_dinero[2]_2\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_reg[6]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter_reg[4]_0\ : in STD_LOGIC;
    \caracter[4]_i_2\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \caracter[2]_i_6\ : in STD_LOGIC;
    \caracter_reg[6]_0\ : in STD_LOGIC;
    \disp_reg[3]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \disp_reg[8]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_reg[7]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    reset_IBUF : in STD_LOGIC;
    \caracter[1]_i_8\ : in STD_LOGIC;
    dot_reg_0 : in STD_LOGIC;
    \charact_dot_reg[0]_3\ : in STD_LOGIC;
    \charact_dot_reg[0]_4\ : in STD_LOGIC
  );
end display;

architecture STRUCTURE of display is
  signal Inst_contador_n_29 : STD_LOGIC;
  signal Inst_contador_n_31 : STD_LOGIC;
  signal caracter : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \caracter[4]_i_9_n_0\ : STD_LOGIC;
  signal caracter_0 : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \^charact_dot_reg[0]_0\ : STD_LOGIC;
  signal \^output0\ : STD_LOGIC;
begin
  \charact_dot_reg[0]_0\ <= \^charact_dot_reg[0]_0\;
  output0 <= \^output0\;
Inst_contador: entity work.\contador__parameterized1\
     port map (
      AR(0) => \^output0\,
      CO(0) => CO(0),
      D(5) => caracter_0(6),
      D(4 downto 0) => caracter_0(4 downto 0),
      \FSM_sequential_current_state_reg[0]\ => \FSM_sequential_current_state_reg[0]\,
      \FSM_sequential_current_state_reg[0]_0\ => Inst_contador_n_29,
      \FSM_sequential_current_state_reg[1]\ => \FSM_sequential_current_state_reg[1]\,
      Q(2 downto 0) => Q(2 downto 0),
      \caracter[0]_i_6_0\ => \caracter[0]_i_6\,
      \caracter[1]_i_8_0\ => \caracter[1]_i_8\,
      \caracter[2]_i_6_0\ => \caracter[2]_i_6\,
      \caracter[4]_i_2_0\(1 downto 0) => \caracter[4]_i_2\(1 downto 0),
      \caracter_reg[0]\ => \caracter_reg[0]_0\,
      \caracter_reg[0]_0\ => \caracter_reg[0]_1\,
      \caracter_reg[1]\ => \caracter_reg[1]_0\,
      \caracter_reg[1]_0\ => \caracter_reg[1]_1\,
      \caracter_reg[2]\ => \caracter_reg[2]_0\,
      \caracter_reg[4]\ => \caracter_reg[4]_0\,
      \caracter_reg[4]_0\ => \caracter[4]_i_9_n_0\,
      \caracter_reg[6]\ => \caracter_reg[6]_0\,
      \charact_dot_reg[0]\ => \charact_dot_reg[0]_1\,
      \charact_dot_reg[0]_0\ => \charact_dot_reg[0]_2\,
      \charact_dot_reg[0]_1\ => Inst_contador_n_31,
      \charact_dot_reg[0]_2\ => \^charact_dot_reg[0]_0\,
      \charact_dot_reg[0]_3\ => \charact_dot_reg[0]_3\,
      \charact_dot_reg[0]_4\ => \charact_dot_reg[0]_4\,
      clk_BUFG => clk_BUFG,
      current_state(1 downto 0) => current_state(1 downto 0),
      \disp_dinero[2]_2\(1 downto 0) => \disp_dinero[2]_2\(1 downto 0),
      \disp_dinero[3]_1\(0) => \disp_dinero[3]_1\(0),
      \disp_dinero[5]_0\(1 downto 0) => \disp_dinero[5]_0\(1 downto 0),
      \disp_reg[1]\(1 downto 0) => \disp_reg[1]\(1 downto 0),
      \disp_reg[1][6]\ => \disp_reg[1][6]\,
      \disp_reg[3]\(3 downto 0) => \disp_reg[3]\(3 downto 0),
      \disp_reg[4]\(1 downto 0) => \disp_reg[4]\(1 downto 0),
      \disp_reg[5]\(2 downto 0) => \disp_reg[5]\(2 downto 0),
      \disp_reg[6]\(0) => \disp_reg[6]\(0),
      \disp_reg[7]\(1 downto 0) => \disp_reg[7]\(1 downto 0),
      \disp_reg[8]\(0) => \disp_reg[8]\(0),
      dot_reg => dot_reg_0,
      elem_OBUF(7 downto 0) => elem_OBUF(7 downto 0),
      \reg_reg[0]_0\ => \reg_reg[0]\,
      \reg_reg[0]_1\ => \reg_reg[0]_0\,
      \reg_reg[0]_2\ => \reg_reg[0]_1\,
      \reg_reg[1]_0\ => \reg_reg[1]\,
      \reg_reg[1]_1\ => \reg_reg[1]_0\,
      \reg_reg[2]_0\ => \reg_reg[2]\,
      reset_IBUF => reset_IBUF
    );
Inst_digito: entity work.digito
     port map (
      Q(5) => caracter(6),
      Q(4 downto 0) => caracter(4 downto 0),
      digit_OBUF(2) => digit_OBUF(6),
      digit_OBUF(1) => digit_OBUF(4),
      digit_OBUF(0) => digit_OBUF(0)
    );
\caracter[4]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^charact_dot_reg[0]_0\,
      I1 => \disp_dinero[5]_0\(1),
      O => \caracter[4]_i_9_n_0\
    );
\caracter_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => \^output0\,
      D => caracter_0(0),
      Q => caracter(0)
    );
\caracter_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => \^output0\,
      D => caracter_0(1),
      Q => caracter(1)
    );
\caracter_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => \^output0\,
      D => caracter_0(2),
      Q => caracter(2)
    );
\caracter_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => \^output0\,
      D => caracter_0(3),
      Q => caracter(3)
    );
\caracter_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => \^output0\,
      D => caracter_0(4),
      Q => caracter(4)
    );
\caracter_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => \^output0\,
      D => caracter_0(6),
      Q => caracter(6)
    );
\charact_dot_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => Inst_contador_n_31,
      Q => \^charact_dot_reg[0]_0\,
      R => '0'
    );
\digit_OBUF[1]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F2FFFFCFC0358FEF"
    )
        port map (
      I0 => caracter(0),
      I1 => caracter(1),
      I2 => caracter(4),
      I3 => caracter(2),
      I4 => caracter(6),
      I5 => caracter(3),
      O => digit_OBUF(1)
    );
\digit_OBUF[2]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CEAECB8FFFFFBBF3"
    )
        port map (
      I0 => caracter(3),
      I1 => caracter(4),
      I2 => caracter(2),
      I3 => caracter(1),
      I4 => caracter(0),
      I5 => caracter(6),
      O => digit_OBUF(2)
    );
\digit_OBUF[3]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F5EFDDCFC8DFFF2F"
    )
        port map (
      I0 => caracter(0),
      I1 => caracter(3),
      I2 => caracter(4),
      I3 => caracter(6),
      I4 => caracter(2),
      I5 => caracter(1),
      O => digit_OBUF(3)
    );
\digit_OBUF[5]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFFDF6BDFFF37B33"
    )
        port map (
      I0 => caracter(0),
      I1 => caracter(4),
      I2 => caracter(1),
      I3 => caracter(2),
      I4 => caracter(3),
      I5 => caracter(6),
      O => digit_OBUF(5)
    );
dot_reg: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_BUFG,
      CE => dot,
      D => Inst_contador_n_29,
      PRE => \^output0\,
      Q => dot_OBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gestion_dinero is
  port (
    \charact_dot_reg[0]\ : out STD_LOGIC;
    \reg_reg[0]\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[0]_0\ : out STD_LOGIC;
    \disp_dinero[5]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \reg_reg[0]_0\ : out STD_LOGIC;
    \charact_dot_reg[0]_0\ : out STD_LOGIC;
    refresco_OBUF : out STD_LOGIC;
    devolver_OBUF : out STD_LOGIC;
    DI : out STD_LOGIC_VECTOR ( 0 to 0 );
    \charact_dot_reg[0]_1\ : out STD_LOGIC;
    \disp_dinero[3]_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \reg_reg[0]_1\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[0]_1\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[1]_0\ : out STD_LOGIC;
    \caracter_reg[1]_i_15\ : out STD_LOGIC;
    \caracter_reg[1]_i_16\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter_reg[0]\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \caracter_reg[0]_0\ : in STD_LOGIC;
    \caracter_reg[0]_1\ : in STD_LOGIC;
    \disp_reg[3]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \FSM_sequential_current_state_reg[0]_2\ : in STD_LOGIC;
    \FSM_sequential_current_state_reg[1]_1\ : in STD_LOGIC;
    \caracter_reg[1]\ : in STD_LOGIC;
    \caracter[2]_i_16\ : in STD_LOGIC;
    clr_dinero_r : in STD_LOGIC;
    \FSM_sequential_current_state_reg[1]_2\ : in STD_LOGIC;
    \FSM_sequential_current_state_reg[1]_3\ : in STD_LOGIC;
    \FSM_sequential_current_state_reg[0]_3\ : in STD_LOGIC;
    \total_string_reg[7]_0\ : in STD_LOGIC_VECTOR ( 6 downto 0 );
    \caracter_reg[2]\ : in STD_LOGIC;
    \caracter_reg[1]_0\ : in STD_LOGIC;
    \caracter_reg[1]_1\ : in STD_LOGIC;
    \FSM_sequential_current_state_reg[0]_4\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC
  );
end gestion_dinero;

architecture STRUCTURE of gestion_dinero is
  signal \FSM_sequential_current_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_current_state[1]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_current_state[1]_i_2__0_n_0\ : STD_LOGIC;
  signal \FSM_sequential_current_state[1]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_current_state[1]_i_4__0_n_0\ : STD_LOGIC;
  signal \FSM_sequential_current_state[1]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_sequential_current_state[1]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_sequential_current_state[1]_i_7__0_n_0\ : STD_LOGIC;
  signal \FSM_sequential_current_state[1]_i_9_n_0\ : STD_LOGIC;
  signal \^fsm_sequential_current_state_reg[0]_0\ : STD_LOGIC;
  signal clr_dinero : STD_LOGIC;
  signal current_state : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^disp_dinero[5]_0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal stop : STD_LOGIC;
  signal \total_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \total_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \total_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \total_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \total_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \total_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \total_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \total_carry__0_n_1\ : STD_LOGIC;
  signal \total_carry__0_n_2\ : STD_LOGIC;
  signal \total_carry__0_n_3\ : STD_LOGIC;
  signal \total_carry__0_n_4\ : STD_LOGIC;
  signal \total_carry__0_n_5\ : STD_LOGIC;
  signal \total_carry__0_n_6\ : STD_LOGIC;
  signal \total_carry__0_n_7\ : STD_LOGIC;
  signal total_carry_i_1_n_0 : STD_LOGIC;
  signal total_carry_i_2_n_0 : STD_LOGIC;
  signal total_carry_i_3_n_0 : STD_LOGIC;
  signal total_carry_i_4_n_0 : STD_LOGIC;
  signal total_carry_i_5_n_0 : STD_LOGIC;
  signal total_carry_i_6_n_0 : STD_LOGIC;
  signal total_carry_i_7_n_0 : STD_LOGIC;
  signal total_carry_i_8_n_0 : STD_LOGIC;
  signal total_carry_n_0 : STD_LOGIC;
  signal total_carry_n_1 : STD_LOGIC;
  signal total_carry_n_2 : STD_LOGIC;
  signal total_carry_n_3 : STD_LOGIC;
  signal total_carry_n_4 : STD_LOGIC;
  signal total_carry_n_5 : STD_LOGIC;
  signal total_carry_n_6 : STD_LOGIC;
  signal total_carry_n_7 : STD_LOGIC;
  signal total_out : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal total_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \total_string[3]_i_2_n_0\ : STD_LOGIC;
  signal \total_string[3]_i_3_n_0\ : STD_LOGIC;
  signal \total_string[3]_i_4_n_0\ : STD_LOGIC;
  signal \total_string[3]_i_5_n_0\ : STD_LOGIC;
  signal \total_string[3]_i_6_n_0\ : STD_LOGIC;
  signal \total_string[3]_i_7_n_0\ : STD_LOGIC;
  signal \total_string[3]_i_8_n_0\ : STD_LOGIC;
  signal \total_string[3]_i_9_n_0\ : STD_LOGIC;
  signal \total_string[7]_i_3_n_0\ : STD_LOGIC;
  signal \total_string[7]_i_4_n_0\ : STD_LOGIC;
  signal \total_string[7]_i_5_n_0\ : STD_LOGIC;
  signal \total_string[7]_i_6_n_0\ : STD_LOGIC;
  signal \total_string[7]_i_7_n_0\ : STD_LOGIC;
  signal \total_string[7]_i_8_n_0\ : STD_LOGIC;
  signal \total_string[7]_i_9_n_0\ : STD_LOGIC;
  signal \total_string_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \total_string_reg[3]_i_1_n_1\ : STD_LOGIC;
  signal \total_string_reg[3]_i_1_n_2\ : STD_LOGIC;
  signal \total_string_reg[3]_i_1_n_3\ : STD_LOGIC;
  signal \total_string_reg[3]_i_1_n_4\ : STD_LOGIC;
  signal \total_string_reg[3]_i_1_n_5\ : STD_LOGIC;
  signal \total_string_reg[3]_i_1_n_6\ : STD_LOGIC;
  signal \total_string_reg[3]_i_1_n_7\ : STD_LOGIC;
  signal \total_string_reg[7]_i_2_n_1\ : STD_LOGIC;
  signal \total_string_reg[7]_i_2_n_2\ : STD_LOGIC;
  signal \total_string_reg[7]_i_2_n_3\ : STD_LOGIC;
  signal \total_string_reg[7]_i_2_n_4\ : STD_LOGIC;
  signal \total_string_reg[7]_i_2_n_5\ : STD_LOGIC;
  signal \total_string_reg[7]_i_2_n_6\ : STD_LOGIC;
  signal \total_string_reg[7]_i_2_n_7\ : STD_LOGIC;
  signal \NLW_total_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_total_string_reg[7]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_current_state[0]_i_1\ : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of \FSM_sequential_current_state[0]_i_1__0\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \FSM_sequential_current_state[1]_i_1__0\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \FSM_sequential_current_state[1]_i_8\ : label is "soft_lutpair57";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_current_state_reg[0]\ : label is "s2:11,s1:01,s0:00,s3:10";
  attribute FSM_ENCODED_STATES of \FSM_sequential_current_state_reg[1]\ : label is "s2:11,s1:01,s0:00,s3:10";
  attribute SOFT_HLUTNM of devolver_OBUF_inst_i_1 : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of refresco_OBUF_inst_i_1 : label is "soft_lutpair56";
begin
  \FSM_sequential_current_state_reg[0]_0\ <= \^fsm_sequential_current_state_reg[0]_0\;
  \disp_dinero[5]_0\(1 downto 0) <= \^disp_dinero[5]_0\(1 downto 0);
\FSM_sequential_current_state[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"34"
    )
        port map (
      I0 => current_state(1),
      I1 => \FSM_sequential_current_state[1]_i_6_n_0\,
      I2 => current_state(0),
      O => \FSM_sequential_current_state[0]_i_1_n_0\
    );
\FSM_sequential_current_state[0]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0FB0"
    )
        port map (
      I0 => \FSM_sequential_current_state_reg[0]_4\,
      I1 => \FSM_sequential_current_state_reg[1]_1\,
      I2 => \FSM_sequential_current_state[1]_i_4__0_n_0\,
      I3 => \FSM_sequential_current_state_reg[0]_2\,
      O => \FSM_sequential_current_state_reg[1]_0\
    );
\FSM_sequential_current_state[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEFFFFFEEE0000"
    )
        port map (
      I0 => \FSM_sequential_current_state[1]_i_3_n_0\,
      I1 => \FSM_sequential_current_state[1]_i_4_n_0\,
      I2 => current_state(0),
      I3 => \FSM_sequential_current_state_reg[1]_2\,
      I4 => \FSM_sequential_current_state[1]_i_6_n_0\,
      I5 => current_state(1),
      O => \FSM_sequential_current_state[1]_i_1_n_0\
    );
\FSM_sequential_current_state[1]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AFE0"
    )
        port map (
      I0 => \FSM_sequential_current_state_reg[1]_3\,
      I1 => \FSM_sequential_current_state_reg[0]_2\,
      I2 => \FSM_sequential_current_state[1]_i_4__0_n_0\,
      I3 => \FSM_sequential_current_state_reg[1]_1\,
      O => \FSM_sequential_current_state_reg[0]_1\
    );
\FSM_sequential_current_state[1]_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \FSM_sequential_current_state_reg[1]_1\,
      I1 => clr_dinero_r,
      O => \FSM_sequential_current_state[1]_i_2__0_n_0\
    );
\FSM_sequential_current_state[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0203030303030303"
    )
        port map (
      I0 => total_reg(1),
      I1 => current_state(0),
      I2 => current_state(1),
      I3 => total_reg(6),
      I4 => total_reg(5),
      I5 => total_reg(2),
      O => \FSM_sequential_current_state[1]_i_3_n_0\
    );
\FSM_sequential_current_state[1]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000FF000000FE"
    )
        port map (
      I0 => total_reg(7),
      I1 => total_reg(4),
      I2 => total_reg(3),
      I3 => current_state(0),
      I4 => current_state(1),
      I5 => total_reg(0),
      O => \FSM_sequential_current_state[1]_i_4_n_0\
    );
\FSM_sequential_current_state[1]_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFF0800"
    )
        port map (
      I0 => \FSM_sequential_current_state_reg[1]_1\,
      I1 => \FSM_sequential_current_state_reg[0]_2\,
      I2 => current_state(0),
      I3 => current_state(1),
      I4 => \FSM_sequential_current_state_reg[1]_3\,
      I5 => \FSM_sequential_current_state_reg[0]_3\,
      O => \FSM_sequential_current_state[1]_i_4__0_n_0\
    );
\FSM_sequential_current_state[1]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCF0CCFFCCF0CCFA"
    )
        port map (
      I0 => \FSM_sequential_current_state[1]_i_7__0_n_0\,
      I1 => \FSM_sequential_current_state_reg[1]_2\,
      I2 => total_reg(7),
      I3 => stop,
      I4 => \FSM_sequential_current_state[1]_i_9_n_0\,
      I5 => total_reg(2),
      O => \FSM_sequential_current_state[1]_i_6_n_0\
    );
\FSM_sequential_current_state[1]_i_7__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => total_reg(3),
      I1 => total_reg(4),
      O => \FSM_sequential_current_state[1]_i_7__0_n_0\
    );
\FSM_sequential_current_state[1]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => current_state(0),
      I1 => current_state(1),
      O => stop
    );
\FSM_sequential_current_state[1]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => total_reg(5),
      I1 => total_reg(6),
      O => \FSM_sequential_current_state[1]_i_9_n_0\
    );
\FSM_sequential_current_state_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => \FSM_sequential_current_state[1]_i_2__0_n_0\,
      D => \FSM_sequential_current_state[0]_i_1_n_0\,
      Q => current_state(0)
    );
\FSM_sequential_current_state_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => \FSM_sequential_current_state[1]_i_2__0_n_0\,
      D => \FSM_sequential_current_state[1]_i_1_n_0\,
      Q => current_state(1)
    );
devolver_OBUF_inst_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => current_state(0),
      I1 => current_state(1),
      O => devolver_OBUF
    );
dot_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000200020000000"
    )
        port map (
      I0 => \FSM_sequential_current_state_reg[0]_2\,
      I1 => Q(2),
      I2 => \^disp_dinero[5]_0\(1),
      I3 => \FSM_sequential_current_state_reg[1]_1\,
      I4 => Q(1),
      I5 => Q(0),
      O => \^fsm_sequential_current_state_reg[0]_0\
    );
inst_int_to_string: entity work.int_to_string
     port map (
      Q(0) => Q(0),
      \caracter[2]_i_16\ => \caracter[2]_i_16\,
      \caracter_reg[0]\ => \caracter_reg[0]\,
      \caracter_reg[0]_0\ => \^fsm_sequential_current_state_reg[0]_0\,
      \caracter_reg[0]_1\ => \caracter_reg[0]_0\,
      \caracter_reg[0]_2\ => \caracter_reg[0]_1\,
      \caracter_reg[1]\ => \caracter_reg[1]\,
      \caracter_reg[1]_0\ => \caracter_reg[1]_0\,
      \caracter_reg[1]_1\ => \caracter_reg[1]_1\,
      \caracter_reg[1]_i_15_0\ => \caracter_reg[1]_i_15\,
      \caracter_reg[1]_i_16_0\(0) => \caracter_reg[1]_i_16\(0),
      \caracter_reg[2]\ => \caracter_reg[2]\,
      \caracter_reg[3]_i_126_0\(7 downto 0) => total_out(7 downto 0),
      \charact_dot_reg[0]\ => \charact_dot_reg[0]\,
      \charact_dot_reg[0]_0\ => \charact_dot_reg[0]_0\,
      \charact_dot_reg[0]_1\ => \charact_dot_reg[0]_1\,
      \disp_dinero[3]_1\(0) => \disp_dinero[3]_1\(0),
      \disp_dinero[5]_0\(1 downto 0) => \^disp_dinero[5]_0\(1 downto 0),
      \disp_reg[3]\(0) => \disp_reg[3]\(0),
      \reg_reg[0]\ => \reg_reg[0]\,
      \reg_reg[0]_0\ => \reg_reg[0]_0\,
      \reg_reg[0]_1\ => \reg_reg[0]_1\,
      \total_string_reg[0]\ => DI(0)
    );
refresco_OBUF_inst_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => current_state(0),
      I1 => current_state(1),
      O => refresco_OBUF
    );
total_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => total_carry_n_0,
      CO(2) => total_carry_n_1,
      CO(1) => total_carry_n_2,
      CO(0) => total_carry_n_3,
      CYINIT => '0',
      DI(3) => total_carry_i_1_n_0,
      DI(2) => total_carry_i_2_n_0,
      DI(1) => total_carry_i_3_n_0,
      DI(0) => total_carry_i_4_n_0,
      O(3) => total_carry_n_4,
      O(2) => total_carry_n_5,
      O(1) => total_carry_n_6,
      O(0) => total_carry_n_7,
      S(3) => total_carry_i_5_n_0,
      S(2) => total_carry_i_6_n_0,
      S(1) => total_carry_i_7_n_0,
      S(0) => total_carry_i_8_n_0
    );
\total_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => total_carry_n_0,
      CO(3) => \NLW_total_carry__0_CO_UNCONNECTED\(3),
      CO(2) => \total_carry__0_n_1\,
      CO(1) => \total_carry__0_n_2\,
      CO(0) => \total_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \total_carry__0_i_1_n_0\,
      DI(1) => \total_carry__0_i_2_n_0\,
      DI(0) => \total_carry__0_i_3_n_0\,
      O(3) => \total_carry__0_n_4\,
      O(2) => \total_carry__0_n_5\,
      O(1) => \total_carry__0_n_6\,
      O(0) => \total_carry__0_n_7\,
      S(3) => \total_carry__0_i_4_n_0\,
      S(2) => \total_carry__0_i_5_n_0\,
      S(1) => \total_carry__0_i_6_n_0\,
      S(0) => \total_carry__0_i_7_n_0\
    );
\total_carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \total_string_reg[7]_0\(6),
      I1 => current_state(1),
      I2 => current_state(0),
      O => \total_carry__0_i_1_n_0\
    );
\total_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \total_string_reg[7]_0\(5),
      I1 => current_state(1),
      I2 => current_state(0),
      O => \total_carry__0_i_2_n_0\
    );
\total_carry__0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \total_string_reg[7]_0\(4),
      I1 => current_state(1),
      I2 => current_state(0),
      O => \total_carry__0_i_3_n_0\
    );
\total_carry__0_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => total_reg(7),
      I1 => current_state(1),
      I2 => current_state(0),
      O => \total_carry__0_i_4_n_0\
    );
\total_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0102"
    )
        port map (
      I0 => \total_string_reg[7]_0\(6),
      I1 => current_state(0),
      I2 => current_state(1),
      I3 => total_reg(6),
      O => \total_carry__0_i_5_n_0\
    );
\total_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0102"
    )
        port map (
      I0 => \total_string_reg[7]_0\(5),
      I1 => current_state(0),
      I2 => current_state(1),
      I3 => total_reg(5),
      O => \total_carry__0_i_6_n_0\
    );
\total_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0102"
    )
        port map (
      I0 => \total_string_reg[7]_0\(4),
      I1 => current_state(0),
      I2 => current_state(1),
      I3 => total_reg(4),
      O => \total_carry__0_i_7_n_0\
    );
total_carry_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \total_string_reg[7]_0\(3),
      I1 => current_state(1),
      I2 => current_state(0),
      O => total_carry_i_1_n_0
    );
total_carry_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \total_string_reg[7]_0\(2),
      I1 => current_state(1),
      I2 => current_state(0),
      O => total_carry_i_2_n_0
    );
total_carry_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \total_string_reg[7]_0\(1),
      I1 => current_state(1),
      I2 => current_state(0),
      O => total_carry_i_3_n_0
    );
total_carry_i_4: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \total_string_reg[7]_0\(0),
      I1 => current_state(1),
      I2 => current_state(0),
      O => total_carry_i_4_n_0
    );
total_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0102"
    )
        port map (
      I0 => \total_string_reg[7]_0\(3),
      I1 => current_state(0),
      I2 => current_state(1),
      I3 => total_reg(3),
      O => total_carry_i_5_n_0
    );
total_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0102"
    )
        port map (
      I0 => \total_string_reg[7]_0\(2),
      I1 => current_state(0),
      I2 => current_state(1),
      I3 => total_reg(2),
      O => total_carry_i_6_n_0
    );
total_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0102"
    )
        port map (
      I0 => \total_string_reg[7]_0\(1),
      I1 => current_state(0),
      I2 => current_state(1),
      I3 => total_reg(1),
      O => total_carry_i_7_n_0
    );
total_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0102"
    )
        port map (
      I0 => \total_string_reg[7]_0\(0),
      I1 => current_state(0),
      I2 => current_state(1),
      I3 => total_reg(0),
      O => total_carry_i_8_n_0
    );
\total_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => \FSM_sequential_current_state[1]_i_2__0_n_0\,
      D => total_carry_n_7,
      Q => total_reg(0)
    );
\total_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => \FSM_sequential_current_state[1]_i_2__0_n_0\,
      D => total_carry_n_6,
      Q => total_reg(1)
    );
\total_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => \FSM_sequential_current_state[1]_i_2__0_n_0\,
      D => total_carry_n_5,
      Q => total_reg(2)
    );
\total_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => \FSM_sequential_current_state[1]_i_2__0_n_0\,
      D => total_carry_n_4,
      Q => total_reg(3)
    );
\total_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => \FSM_sequential_current_state[1]_i_2__0_n_0\,
      D => \total_carry__0_n_7\,
      Q => total_reg(4)
    );
\total_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => \FSM_sequential_current_state[1]_i_2__0_n_0\,
      D => \total_carry__0_n_6\,
      Q => total_reg(5)
    );
\total_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => \FSM_sequential_current_state[1]_i_2__0_n_0\,
      D => \total_carry__0_n_5\,
      Q => total_reg(6)
    );
\total_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => \FSM_sequential_current_state[1]_i_2__0_n_0\,
      D => \total_carry__0_n_4\,
      Q => total_reg(7)
    );
\total_string[3]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \total_string_reg[7]_0\(3),
      I1 => current_state(1),
      I2 => current_state(0),
      O => \total_string[3]_i_2_n_0\
    );
\total_string[3]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \total_string_reg[7]_0\(2),
      I1 => current_state(1),
      I2 => current_state(0),
      O => \total_string[3]_i_3_n_0\
    );
\total_string[3]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \total_string_reg[7]_0\(1),
      I1 => current_state(1),
      I2 => current_state(0),
      O => \total_string[3]_i_4_n_0\
    );
\total_string[3]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \total_string_reg[7]_0\(0),
      I1 => current_state(1),
      I2 => current_state(0),
      O => \total_string[3]_i_5_n_0\
    );
\total_string[3]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF60006"
    )
        port map (
      I0 => \total_string_reg[7]_0\(3),
      I1 => total_reg(3),
      I2 => current_state(1),
      I3 => current_state(0),
      I4 => total_out(3),
      O => \total_string[3]_i_6_n_0\
    );
\total_string[3]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF60006"
    )
        port map (
      I0 => \total_string_reg[7]_0\(2),
      I1 => total_reg(2),
      I2 => current_state(1),
      I3 => current_state(0),
      I4 => total_out(2),
      O => \total_string[3]_i_7_n_0\
    );
\total_string[3]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF60006"
    )
        port map (
      I0 => \total_string_reg[7]_0\(1),
      I1 => total_reg(1),
      I2 => current_state(1),
      I3 => current_state(0),
      I4 => total_out(1),
      O => \total_string[3]_i_8_n_0\
    );
\total_string[3]_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF60006"
    )
        port map (
      I0 => \total_string_reg[7]_0\(0),
      I1 => total_reg(0),
      I2 => current_state(1),
      I3 => current_state(0),
      I4 => total_out(0),
      O => \total_string[3]_i_9_n_0\
    );
\total_string[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clr_dinero_r,
      I1 => \FSM_sequential_current_state_reg[1]_1\,
      O => clr_dinero
    );
\total_string[7]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \total_string_reg[7]_0\(6),
      I1 => current_state(1),
      I2 => current_state(0),
      O => \total_string[7]_i_3_n_0\
    );
\total_string[7]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \total_string_reg[7]_0\(5),
      I1 => current_state(1),
      I2 => current_state(0),
      O => \total_string[7]_i_4_n_0\
    );
\total_string[7]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \total_string_reg[7]_0\(4),
      I1 => current_state(1),
      I2 => current_state(0),
      O => \total_string[7]_i_5_n_0\
    );
\total_string[7]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"ABA8"
    )
        port map (
      I0 => total_out(7),
      I1 => current_state(0),
      I2 => current_state(1),
      I3 => total_reg(7),
      O => \total_string[7]_i_6_n_0\
    );
\total_string[7]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF60006"
    )
        port map (
      I0 => \total_string_reg[7]_0\(6),
      I1 => total_reg(6),
      I2 => current_state(1),
      I3 => current_state(0),
      I4 => total_out(6),
      O => \total_string[7]_i_7_n_0\
    );
\total_string[7]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF60006"
    )
        port map (
      I0 => \total_string_reg[7]_0\(5),
      I1 => total_reg(5),
      I2 => current_state(1),
      I3 => current_state(0),
      I4 => total_out(5),
      O => \total_string[7]_i_8_n_0\
    );
\total_string[7]_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF60006"
    )
        port map (
      I0 => \total_string_reg[7]_0\(4),
      I1 => total_reg(4),
      I2 => current_state(1),
      I3 => current_state(0),
      I4 => total_out(4),
      O => \total_string[7]_i_9_n_0\
    );
\total_string_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => clr_dinero,
      D => \total_string_reg[3]_i_1_n_7\,
      Q => total_out(0),
      R => '0'
    );
\total_string_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => clr_dinero,
      D => \total_string_reg[3]_i_1_n_6\,
      Q => total_out(1),
      R => '0'
    );
\total_string_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => clr_dinero,
      D => \total_string_reg[3]_i_1_n_5\,
      Q => total_out(2),
      R => '0'
    );
\total_string_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => clr_dinero,
      D => \total_string_reg[3]_i_1_n_4\,
      Q => total_out(3),
      R => '0'
    );
\total_string_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \total_string_reg[3]_i_1_n_0\,
      CO(2) => \total_string_reg[3]_i_1_n_1\,
      CO(1) => \total_string_reg[3]_i_1_n_2\,
      CO(0) => \total_string_reg[3]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => \total_string[3]_i_2_n_0\,
      DI(2) => \total_string[3]_i_3_n_0\,
      DI(1) => \total_string[3]_i_4_n_0\,
      DI(0) => \total_string[3]_i_5_n_0\,
      O(3) => \total_string_reg[3]_i_1_n_4\,
      O(2) => \total_string_reg[3]_i_1_n_5\,
      O(1) => \total_string_reg[3]_i_1_n_6\,
      O(0) => \total_string_reg[3]_i_1_n_7\,
      S(3) => \total_string[3]_i_6_n_0\,
      S(2) => \total_string[3]_i_7_n_0\,
      S(1) => \total_string[3]_i_8_n_0\,
      S(0) => \total_string[3]_i_9_n_0\
    );
\total_string_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => clr_dinero,
      D => \total_string_reg[7]_i_2_n_7\,
      Q => total_out(4),
      R => '0'
    );
\total_string_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => clr_dinero,
      D => \total_string_reg[7]_i_2_n_6\,
      Q => total_out(5),
      R => '0'
    );
\total_string_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => clr_dinero,
      D => \total_string_reg[7]_i_2_n_5\,
      Q => total_out(6),
      R => '0'
    );
\total_string_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => clr_dinero,
      D => \total_string_reg[7]_i_2_n_4\,
      Q => total_out(7),
      R => '0'
    );
\total_string_reg[7]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \total_string_reg[3]_i_1_n_0\,
      CO(3) => \NLW_total_string_reg[7]_i_2_CO_UNCONNECTED\(3),
      CO(2) => \total_string_reg[7]_i_2_n_1\,
      CO(1) => \total_string_reg[7]_i_2_n_2\,
      CO(0) => \total_string_reg[7]_i_2_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \total_string[7]_i_3_n_0\,
      DI(1) => \total_string[7]_i_4_n_0\,
      DI(0) => \total_string[7]_i_5_n_0\,
      O(3) => \total_string_reg[7]_i_2_n_4\,
      O(2) => \total_string_reg[7]_i_2_n_5\,
      O(1) => \total_string_reg[7]_i_2_n_6\,
      O(0) => \total_string_reg[7]_i_2_n_7\,
      S(3) => \total_string[7]_i_6_n_0\,
      S(2) => \total_string[7]_i_7_n_0\,
      S(1) => \total_string[7]_i_8_n_0\,
      S(0) => \total_string[7]_i_9_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity prescaler is
  port (
    clk : out STD_LOGIC;
    clk_100Mh : in STD_LOGIC
  );
end prescaler;

architecture STRUCTURE of prescaler is
  signal \^clk\ : STD_LOGIC;
  signal uut_n_0 : STD_LOGIC;
begin
  clk <= \^clk\;
clk_pre_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh,
      CE => '1',
      D => uut_n_0,
      Q => \^clk\,
      R => '0'
    );
uut: entity work.contador
     port map (
      clk => \^clk\,
      clk_100Mh => clk_100Mh,
      clk_pre_reg => uut_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity sincronizador is
  port (
    \sreg_reg[0]\ : out STD_LOGIC;
    \sreg_reg[0]_0\ : out STD_LOGIC;
    \sreg_reg[0]_1\ : out STD_LOGIC;
    \sreg_reg[0]_2\ : out STD_LOGIC;
    \sreg_reg[0]_3\ : out STD_LOGIC;
    \sreg_reg[0]_4\ : out STD_LOGIC;
    \sreg_reg[0]_5\ : out STD_LOGIC;
    \sreg_reg[0]_6\ : out STD_LOGIC;
    \sreg_reg[0]_7\ : out STD_LOGIC;
    \sreg_reg[0]_8\ : out STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    monedas_IBUF : in STD_LOGIC_VECTOR ( 4 downto 0 );
    button_ok_IBUF : in STD_LOGIC;
    ref_option_IBUF : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
end sincronizador;

architecture STRUCTURE of sincronizador is
begin
inst_synchrnzr0: entity work.synchrnzr
     port map (
      clk_BUFG => clk_BUFG,
      monedas_IBUF(0) => monedas_IBUF(4),
      \sreg_reg[0]_0\ => \sreg_reg[0]\
    );
inst_synchrnzr1: entity work.synchrnzr_0
     port map (
      clk_BUFG => clk_BUFG,
      monedas_IBUF(0) => monedas_IBUF(3),
      \sreg_reg[0]_0\ => \sreg_reg[0]_0\
    );
inst_synchrnzr2: entity work.synchrnzr_1
     port map (
      clk_BUFG => clk_BUFG,
      monedas_IBUF(0) => monedas_IBUF(2),
      \sreg_reg[0]_0\ => \sreg_reg[0]_1\
    );
inst_synchrnzr3: entity work.synchrnzr_2
     port map (
      clk_BUFG => clk_BUFG,
      monedas_IBUF(0) => monedas_IBUF(1),
      \sreg_reg[0]_0\ => \sreg_reg[0]_2\
    );
inst_synchrnzr4: entity work.synchrnzr_3
     port map (
      clk_BUFG => clk_BUFG,
      monedas_IBUF(0) => monedas_IBUF(0),
      \sreg_reg[0]_0\ => \sreg_reg[0]_3\
    );
inst_synchrnzr5: entity work.synchrnzr_4
     port map (
      clk_BUFG => clk_BUFG,
      ref_option_IBUF(0) => ref_option_IBUF(3),
      \sreg_reg[0]_0\ => \sreg_reg[0]_8\
    );
inst_synchrnzr6: entity work.synchrnzr_5
     port map (
      clk_BUFG => clk_BUFG,
      ref_option_IBUF(0) => ref_option_IBUF(2),
      \sreg_reg[0]_0\ => \sreg_reg[0]_7\
    );
inst_synchrnzr7: entity work.synchrnzr_6
     port map (
      clk_BUFG => clk_BUFG,
      ref_option_IBUF(0) => ref_option_IBUF(1),
      \sreg_reg[0]_0\ => \sreg_reg[0]_6\
    );
inst_synchrnzr8: entity work.synchrnzr_7
     port map (
      clk_BUFG => clk_BUFG,
      ref_option_IBUF(0) => ref_option_IBUF(0),
      \sreg_reg[0]_0\ => \sreg_reg[0]_5\
    );
inst_synchrnzr9: entity work.synchrnzr_8
     port map (
      button_ok_IBUF => button_ok_IBUF,
      clk_BUFG => clk_BUFG,
      \sreg_reg[0]_0\ => \sreg_reg[0]_4\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity fsm is
  port (
    \disp_reg[3]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \disp_reg[5]\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \disp_reg[6]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_reg[7]\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_reg[4]\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_reg[8]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \charact_dot_reg[0]\ : out STD_LOGIC;
    \reg_reg[0]\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[0]_0\ : out STD_LOGIC;
    \disp_dinero[5]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_reg[2][4]\ : out STD_LOGIC;
    \disp_reg[1][6]\ : out STD_LOGIC;
    \disp_reg[2][4]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    current_state : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \reg_reg[0]_0\ : out STD_LOGIC;
    \charact_dot_reg[0]_0\ : out STD_LOGIC;
    \FSM_sequential_current_state_reg[1]_0\ : out STD_LOGIC;
    \disp_reg[1]\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_reg[7][6]\ : out STD_LOGIC;
    refresco_OBUF : out STD_LOGIC;
    devolver_OBUF : out STD_LOGIC;
    \disp_reg[7][6]_0\ : out STD_LOGIC;
    dot : out STD_LOGIC;
    \total_string_reg[0]\ : out STD_LOGIC;
    \charact_dot_reg[0]_1\ : out STD_LOGIC;
    \disp_dinero[3]_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \reg_reg[0]_1\ : out STD_LOGIC;
    \caracter_reg[1]_i_15\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    clk_BUFG : in STD_LOGIC;
    output0 : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_reg[3]_3_sp_1\ : in STD_LOGIC;
    \disp_reg[5][4]\ : in STD_LOGIC;
    p_0_out : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_reg[7][6]_1\ : in STD_LOGIC;
    \disp_reg[7][3]\ : in STD_LOGIC;
    \disp_reg[4][4]\ : in STD_LOGIC;
    \disp_reg[4]_1_sp_1\ : in STD_LOGIC;
    \disp_reg[5]_0_sp_1\ : in STD_LOGIC;
    \disp_reg[8][6]\ : in STD_LOGIC;
    \disp_reg[5][6]\ : in STD_LOGIC;
    \caracter_reg[0]\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \caracter_reg[0]_0\ : in STD_LOGIC;
    \caracter_reg[0]_1\ : in STD_LOGIC;
    \caracter_reg[4]\ : in STD_LOGIC;
    \caracter_reg[4]_0\ : in STD_LOGIC;
    \caracter_reg[1]\ : in STD_LOGIC;
    \caracter[2]_i_16\ : in STD_LOGIC;
    dot_reg : in STD_LOGIC;
    \charact_dot_reg[0]_2\ : in STD_LOGIC;
    dot_reg_0 : in STD_LOGIC;
    \FSM_sequential_current_state_reg[1]_1\ : in STD_LOGIC;
    \FSM_sequential_current_state_reg[1]_2\ : in STD_LOGIC;
    \FSM_sequential_current_state_reg[0]_1\ : in STD_LOGIC;
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    \total_string_reg[7]\ : in STD_LOGIC_VECTOR ( 6 downto 0 );
    \caracter_reg[2]\ : in STD_LOGIC;
    \caracter_reg[1]_0\ : in STD_LOGIC;
    \FSM_sequential_current_state_reg[0]_2\ : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \disp_reg[2][4]_1\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_reg[1][3]\ : in STD_LOGIC;
    \disp_reg[1][3]_0\ : in STD_LOGIC;
    \disp_reg[1][3]_1\ : in STD_LOGIC;
    \disp_reg[1][3]_2\ : in STD_LOGIC
  );
end fsm;

architecture STRUCTURE of fsm is
  signal \^fsm_sequential_current_state_reg[0]_0\ : STD_LOGIC;
  signal Inst_gestion_dinero_n_13 : STD_LOGIC;
  signal Inst_gestion_dinero_n_14 : STD_LOGIC;
  signal clr_dinero_r : STD_LOGIC;
  signal \^current_state\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^disp_reg[3]\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \disp_reg[3]_3_sn_1\ : STD_LOGIC;
  signal \disp_reg[4]_1_sn_1\ : STD_LOGIC;
  signal \disp_reg[5]_0_sn_1\ : STD_LOGIC;
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_current_state_reg[0]\ : label is "s1:01,s2:10,s0:00,s3:11";
  attribute FSM_ENCODED_STATES of \FSM_sequential_current_state_reg[1]\ : label is "s1:01,s2:10,s0:00,s3:11";
begin
  \FSM_sequential_current_state_reg[0]_0\ <= \^fsm_sequential_current_state_reg[0]_0\;
  current_state(1 downto 0) <= \^current_state\(1 downto 0);
  \disp_reg[3]\(3 downto 0) <= \^disp_reg[3]\(3 downto 0);
  \disp_reg[3]_3_sn_1\ <= \disp_reg[3]_3_sp_1\;
  \disp_reg[4]_1_sn_1\ <= \disp_reg[4]_1_sp_1\;
  \disp_reg[5]_0_sn_1\ <= \disp_reg[5]_0_sp_1\;
\FSM_sequential_current_state_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => Inst_gestion_dinero_n_14,
      Q => \^current_state\(0)
    );
\FSM_sequential_current_state_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => Inst_gestion_dinero_n_13,
      Q => \^current_state\(1)
    );
Inst_gestion_dinero: entity work.gestion_dinero
     port map (
      DI(0) => \total_string_reg[0]\,
      \FSM_sequential_current_state_reg[0]_0\ => \^fsm_sequential_current_state_reg[0]_0\,
      \FSM_sequential_current_state_reg[0]_1\ => Inst_gestion_dinero_n_13,
      \FSM_sequential_current_state_reg[0]_2\ => \^current_state\(0),
      \FSM_sequential_current_state_reg[0]_3\ => \FSM_sequential_current_state_reg[0]_1\,
      \FSM_sequential_current_state_reg[0]_4\ => \FSM_sequential_current_state_reg[0]_2\,
      \FSM_sequential_current_state_reg[1]_0\ => Inst_gestion_dinero_n_14,
      \FSM_sequential_current_state_reg[1]_1\ => \^current_state\(1),
      \FSM_sequential_current_state_reg[1]_2\ => \FSM_sequential_current_state_reg[1]_1\,
      \FSM_sequential_current_state_reg[1]_3\ => \FSM_sequential_current_state_reg[1]_2\,
      Q(2 downto 0) => Q(2 downto 0),
      \caracter[2]_i_16\ => \caracter[2]_i_16\,
      \caracter_reg[0]\ => \caracter_reg[0]\,
      \caracter_reg[0]_0\ => \caracter_reg[0]_0\,
      \caracter_reg[0]_1\ => \caracter_reg[0]_1\,
      \caracter_reg[1]\ => \caracter_reg[1]\,
      \caracter_reg[1]_0\ => \caracter_reg[4]\,
      \caracter_reg[1]_1\ => \caracter_reg[1]_0\,
      \caracter_reg[1]_i_15\ => \caracter_reg[1]_i_15\(1),
      \caracter_reg[1]_i_16\(0) => \caracter_reg[1]_i_15\(0),
      \caracter_reg[2]\ => \caracter_reg[2]\,
      \charact_dot_reg[0]\ => \charact_dot_reg[0]\,
      \charact_dot_reg[0]_0\ => \charact_dot_reg[0]_0\,
      \charact_dot_reg[0]_1\ => \charact_dot_reg[0]_1\,
      clk_BUFG => clk_BUFG,
      clr_dinero_r => clr_dinero_r,
      devolver_OBUF => devolver_OBUF,
      \disp_dinero[3]_1\(0) => \disp_dinero[3]_1\(0),
      \disp_dinero[5]_0\(1 downto 0) => \disp_dinero[5]_0\(1 downto 0),
      \disp_reg[3]\(0) => \^disp_reg[3]\(0),
      refresco_OBUF => refresco_OBUF,
      \reg_reg[0]\ => \reg_reg[0]\,
      \reg_reg[0]_0\ => \reg_reg[0]_0\,
      \reg_reg[0]_1\ => \reg_reg[0]_1\,
      \total_string_reg[7]_0\(6 downto 0) => \total_string_reg[7]\(6 downto 0)
    );
Inst_gestion_refresco: entity work.gestion_refresco
     port map (
      CO(0) => CO(0),
      D(2 downto 0) => D(2 downto 0),
      E(0) => E(0),
      \FSM_sequential_current_state_reg[1]\ => \FSM_sequential_current_state_reg[1]_0\,
      Q(1 downto 0) => Q(1 downto 0),
      \caracter_reg[4]\ => \caracter_reg[4]\,
      \caracter_reg[4]_0\ => \caracter_reg[4]_0\,
      \charact_dot_reg[0]\ => \charact_dot_reg[0]_2\,
      clk_BUFG => clk_BUFG,
      \disp_reg[1]\(1 downto 0) => \disp_reg[1]\(1 downto 0),
      \disp_reg[1][3]_0\ => \disp_reg[1][3]\,
      \disp_reg[1][3]_1\ => \disp_reg[1][3]_0\,
      \disp_reg[1][3]_2\ => \disp_reg[1][3]_1\,
      \disp_reg[1][3]_3\ => \disp_reg[1][3]_2\,
      \disp_reg[1][6]_0\ => \disp_reg[1][6]\,
      \disp_reg[2][4]_0\ => \disp_reg[2][4]\,
      \disp_reg[2][4]_1\(1 downto 0) => \disp_reg[2][4]_0\(1 downto 0),
      \disp_reg[2][4]_2\(1 downto 0) => \disp_reg[2][4]_1\(1 downto 0),
      \disp_reg[3]\(3 downto 0) => \^disp_reg[3]\(3 downto 0),
      \disp_reg[3][3]_0\ => \disp_reg[3]_3_sn_1\,
      \disp_reg[4]\(1 downto 0) => \disp_reg[4]\(1 downto 0),
      \disp_reg[4][1]_0\ => \disp_reg[4]_1_sn_1\,
      \disp_reg[4][4]_0\ => \disp_reg[4][4]\,
      \disp_reg[5]\(2 downto 0) => \disp_reg[5]\(2 downto 0),
      \disp_reg[5][0]_0\ => \disp_reg[5]_0_sn_1\,
      \disp_reg[5][4]_0\ => \disp_reg[5][4]\,
      \disp_reg[5][6]_0\ => \disp_reg[5][6]\,
      \disp_reg[6]\(0) => \disp_reg[6]\(0),
      \disp_reg[7][3]_0\ => \disp_reg[7]\(0),
      \disp_reg[7][3]_1\ => \disp_reg[7][3]\,
      \disp_reg[7][6]_0\ => \disp_reg[7]\(1),
      \disp_reg[7][6]_1\ => \disp_reg[7][6]\,
      \disp_reg[7][6]_2\ => \disp_reg[7][6]_0\,
      \disp_reg[7][6]_3\ => \disp_reg[7][6]_1\,
      \disp_reg[8]\(0) => \disp_reg[8]\(0),
      \disp_reg[8][6]_0\ => \disp_reg[8][6]\,
      dot => dot,
      dot_reg => \^fsm_sequential_current_state_reg[0]_0\,
      dot_reg_0 => dot_reg,
      dot_reg_1 => \^current_state\(1),
      dot_reg_2 => dot_reg_0,
      p_0_out(0) => p_0_out(0)
    );
clr_dinero_r_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => '1',
      Q => clr_dinero_r
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity top is
  port (
    clk_100Mh : in STD_LOGIC;
    monedas : in STD_LOGIC_VECTOR ( 4 downto 0 );
    ref_option : in STD_LOGIC_VECTOR ( 3 downto 0 );
    button_ok : in STD_LOGIC;
    reset : in STD_LOGIC;
    devolver : out STD_LOGIC;
    refresco : out STD_LOGIC;
    digit : out STD_LOGIC_VECTOR ( 6 downto 0 );
    dot : out STD_LOGIC;
    elem : out STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of top : entity is true;
  attribute digit_number : integer;
  attribute digit_number of top : entity is 8;
  attribute division_prescaler : integer;
  attribute division_prescaler of top : entity is 250000;
  attribute monedas_types : integer;
  attribute monedas_types of top : entity is 5;
  attribute num_refrescos : integer;
  attribute num_refrescos of top : entity is 4;
  attribute size_counter : integer;
  attribute size_counter of top : entity is 3;
  attribute size_prescaler : integer;
  attribute size_prescaler of top : entity is 17;
  attribute width_word : integer;
  attribute width_word of top : entity is 8;
end top;

architecture STRUCTURE of top is
  signal button_ok_IBUF : STD_LOGIC;
  signal caracter2 : STD_LOGIC_VECTOR ( 3 to 3 );
  signal clk : STD_LOGIC;
  signal clk_100Mh_IBUF : STD_LOGIC;
  signal clk_100Mh_IBUF_BUFG : STD_LOGIC;
  signal clk_BUFG : STD_LOGIC;
  signal current_state : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal devolver_OBUF : STD_LOGIC;
  signal digit_OBUF : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \disp[1]\ : STD_LOGIC_VECTOR ( 6 to 6 );
  signal \disp_dinero[2]_0\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \disp_dinero[3]_1\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \disp_dinero[5]_3\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \disp_reg[1]\ : STD_LOGIC_VECTOR ( 6 downto 3 );
  signal \disp_reg[2]\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \disp_reg[3]\ : STD_LOGIC_VECTOR ( 6 downto 1 );
  signal \disp_reg[4]\ : STD_LOGIC_VECTOR ( 4 downto 1 );
  signal \disp_reg[5]\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \disp_reg[6]\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \disp_reg[7]\ : STD_LOGIC_VECTOR ( 6 downto 3 );
  signal \disp_reg[8]\ : STD_LOGIC_VECTOR ( 6 to 6 );
  signal dot_2 : STD_LOGIC;
  signal dot_OBUF : STD_LOGIC;
  signal elem_OBUF : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal inst_decodmonedas_n_0 : STD_LOGIC;
  signal inst_decodmonedas_n_8 : STD_LOGIC;
  signal inst_detectorflancos_n_0 : STD_LOGIC;
  signal inst_detectorflancos_n_1 : STD_LOGIC;
  signal inst_detectorflancos_n_10 : STD_LOGIC;
  signal inst_detectorflancos_n_11 : STD_LOGIC;
  signal inst_detectorflancos_n_12 : STD_LOGIC;
  signal inst_detectorflancos_n_15 : STD_LOGIC;
  signal inst_detectorflancos_n_16 : STD_LOGIC;
  signal inst_detectorflancos_n_17 : STD_LOGIC;
  signal inst_detectorflancos_n_18 : STD_LOGIC;
  signal inst_detectorflancos_n_19 : STD_LOGIC;
  signal inst_detectorflancos_n_2 : STD_LOGIC;
  signal inst_detectorflancos_n_20 : STD_LOGIC;
  signal inst_detectorflancos_n_21 : STD_LOGIC;
  signal inst_detectorflancos_n_22 : STD_LOGIC;
  signal inst_detectorflancos_n_23 : STD_LOGIC;
  signal inst_detectorflancos_n_24 : STD_LOGIC;
  signal inst_detectorflancos_n_25 : STD_LOGIC;
  signal inst_detectorflancos_n_26 : STD_LOGIC;
  signal inst_detectorflancos_n_27 : STD_LOGIC;
  signal inst_detectorflancos_n_28 : STD_LOGIC;
  signal inst_detectorflancos_n_3 : STD_LOGIC;
  signal inst_detectorflancos_n_4 : STD_LOGIC;
  signal inst_detectorflancos_n_5 : STD_LOGIC;
  signal inst_detectorflancos_n_6 : STD_LOGIC;
  signal inst_detectorflancos_n_7 : STD_LOGIC;
  signal inst_detectorflancos_n_8 : STD_LOGIC;
  signal inst_detectorflancos_n_9 : STD_LOGIC;
  signal inst_display_n_10 : STD_LOGIC;
  signal inst_display_n_11 : STD_LOGIC;
  signal inst_display_n_12 : STD_LOGIC;
  signal inst_display_n_13 : STD_LOGIC;
  signal inst_display_n_14 : STD_LOGIC;
  signal inst_display_n_2 : STD_LOGIC;
  signal inst_display_n_24 : STD_LOGIC;
  signal inst_display_n_3 : STD_LOGIC;
  signal inst_display_n_32 : STD_LOGIC;
  signal inst_display_n_4 : STD_LOGIC;
  signal inst_display_n_5 : STD_LOGIC;
  signal inst_display_n_6 : STD_LOGIC;
  signal inst_fsm_n_13 : STD_LOGIC;
  signal inst_fsm_n_14 : STD_LOGIC;
  signal inst_fsm_n_15 : STD_LOGIC;
  signal inst_fsm_n_18 : STD_LOGIC;
  signal inst_fsm_n_19 : STD_LOGIC;
  signal inst_fsm_n_24 : STD_LOGIC;
  signal inst_fsm_n_25 : STD_LOGIC;
  signal inst_fsm_n_26 : STD_LOGIC;
  signal inst_fsm_n_29 : STD_LOGIC;
  signal inst_fsm_n_32 : STD_LOGIC;
  signal inst_fsm_n_34 : STD_LOGIC;
  signal inst_fsm_n_35 : STD_LOGIC;
  signal inst_fsm_n_37 : STD_LOGIC;
  signal inst_sincronizador_n_0 : STD_LOGIC;
  signal inst_sincronizador_n_1 : STD_LOGIC;
  signal inst_sincronizador_n_2 : STD_LOGIC;
  signal inst_sincronizador_n_3 : STD_LOGIC;
  signal inst_sincronizador_n_4 : STD_LOGIC;
  signal inst_sincronizador_n_5 : STD_LOGIC;
  signal inst_sincronizador_n_6 : STD_LOGIC;
  signal inst_sincronizador_n_7 : STD_LOGIC;
  signal inst_sincronizador_n_8 : STD_LOGIC;
  signal inst_sincronizador_n_9 : STD_LOGIC;
  signal monedas_IBUF : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal output0 : STD_LOGIC;
  signal p_0_out : STD_LOGIC_VECTOR ( 3 to 3 );
  signal ref_option_IBUF : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal refresco_OBUF : STD_LOGIC;
  signal reg : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal reset_IBUF : STD_LOGIC;
  signal valor_moneda : STD_LOGIC_VECTOR ( 6 downto 0 );
begin
button_ok_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => button_ok,
      O => button_ok_IBUF
    );
clk_100Mh_IBUF_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => clk_100Mh_IBUF,
      O => clk_100Mh_IBUF_BUFG
    );
clk_100Mh_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => clk_100Mh,
      O => clk_100Mh_IBUF
    );
clk_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => clk,
      O => clk_BUFG
    );
devolver_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => devolver_OBUF,
      O => devolver
    );
\digit_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digit_OBUF(0),
      O => digit(0)
    );
\digit_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digit_OBUF(1),
      O => digit(1)
    );
\digit_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digit_OBUF(2),
      O => digit(2)
    );
\digit_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digit_OBUF(3),
      O => digit(3)
    );
\digit_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digit_OBUF(4),
      O => digit(4)
    );
\digit_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digit_OBUF(5),
      O => digit(5)
    );
\digit_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digit_OBUF(6),
      O => digit(6)
    );
dot_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => dot_OBUF,
      O => dot
    );
\elem_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => elem_OBUF(0),
      O => elem(0)
    );
\elem_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => elem_OBUF(1),
      O => elem(1)
    );
\elem_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => elem_OBUF(2),
      O => elem(2)
    );
\elem_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => elem_OBUF(3),
      O => elem(3)
    );
\elem_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => elem_OBUF(4),
      O => elem(4)
    );
\elem_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => elem_OBUF(5),
      O => elem(5)
    );
\elem_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => elem_OBUF(6),
      O => elem(6)
    );
\elem_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => elem_OBUF(7),
      O => elem(7)
    );
inst_decodmonedas: entity work.decod_monedas
     port map (
      D(6) => inst_detectorflancos_n_22,
      D(5) => inst_detectorflancos_n_23,
      D(4) => inst_detectorflancos_n_24,
      D(3) => inst_detectorflancos_n_25,
      D(2) => inst_detectorflancos_n_26,
      D(1) => inst_detectorflancos_n_27,
      D(0) => inst_detectorflancos_n_28,
      Q(6 downto 0) => valor_moneda(6 downto 0),
      clk_BUFG => clk_BUFG,
      current_state(1 downto 0) => current_state(1 downto 0),
      \valor_moneda_reg[0]_0\ => inst_decodmonedas_n_8,
      \valor_moneda_reg[6]_0\ => inst_decodmonedas_n_0
    );
inst_detectorflancos: entity work.detectorflancos
     port map (
      D(1) => inst_detectorflancos_n_6,
      D(0) => inst_detectorflancos_n_7,
      E(0) => inst_detectorflancos_n_12,
      clk_BUFG => clk_BUFG,
      current_state(1 downto 0) => current_state(1 downto 0),
      \disp_reg[4]\(1) => \disp_reg[4]\(4),
      \disp_reg[4]\(0) => \disp_reg[4]\(1),
      \disp_reg[4][4]\ => inst_detectorflancos_n_0,
      \disp_reg[4]_1_sp_1\ => inst_detectorflancos_n_5,
      \disp_reg[5]\(1) => \disp_reg[5]\(6),
      \disp_reg[5]\(0) => \disp_reg[5]\(0),
      \disp_reg[5][6]\ => inst_detectorflancos_n_20,
      \disp_reg[5]_0_sp_1\ => inst_detectorflancos_n_21,
      \disp_reg[7]\(1) => \disp_reg[7]\(6),
      \disp_reg[7]\(0) => \disp_reg[7]\(3),
      \disp_reg[7][3]\ => inst_detectorflancos_n_10,
      \disp_reg[7][6]\ => inst_detectorflancos_n_8,
      \disp_reg[8]\(0) => \disp_reg[8]\(6),
      \disp_reg[8][6]\ => inst_detectorflancos_n_19,
      p_0_out(0) => p_0_out(3),
      \sreg_reg[0]\(2) => \disp[1]\(6),
      \sreg_reg[0]\(1) => inst_detectorflancos_n_15,
      \sreg_reg[0]\(0) => inst_detectorflancos_n_16,
      \sreg_reg[0]_0\ => inst_detectorflancos_n_17,
      \sreg_reg[0]_1\(6) => inst_detectorflancos_n_22,
      \sreg_reg[0]_1\(5) => inst_detectorflancos_n_23,
      \sreg_reg[0]_1\(4) => inst_detectorflancos_n_24,
      \sreg_reg[0]_1\(3) => inst_detectorflancos_n_25,
      \sreg_reg[0]_1\(2) => inst_detectorflancos_n_26,
      \sreg_reg[0]_1\(1) => inst_detectorflancos_n_27,
      \sreg_reg[0]_1\(0) => inst_detectorflancos_n_28,
      \sreg_reg[0]_10\ => inst_sincronizador_n_8,
      \sreg_reg[0]_11\ => inst_sincronizador_n_9,
      \sreg_reg[0]_2\ => inst_sincronizador_n_0,
      \sreg_reg[0]_3\ => inst_sincronizador_n_1,
      \sreg_reg[0]_4\ => inst_sincronizador_n_2,
      \sreg_reg[0]_5\ => inst_sincronizador_n_3,
      \sreg_reg[0]_6\ => inst_sincronizador_n_4,
      \sreg_reg[0]_7\ => inst_sincronizador_n_5,
      \sreg_reg[0]_8\ => inst_sincronizador_n_6,
      \sreg_reg[0]_9\ => inst_sincronizador_n_7,
      \sreg_reg[1]\ => inst_detectorflancos_n_18,
      \sreg_reg[2]\ => inst_detectorflancos_n_1,
      \sreg_reg[2]_0\ => inst_detectorflancos_n_2,
      \sreg_reg[2]_1\ => inst_detectorflancos_n_3,
      \sreg_reg[2]_2\ => inst_detectorflancos_n_4,
      \sreg_reg[2]_3\ => inst_detectorflancos_n_9,
      \sreg_reg[2]_4\ => inst_detectorflancos_n_11
    );
inst_display: entity work.display
     port map (
      CO(0) => caracter2(3),
      \FSM_sequential_current_state_reg[0]\ => inst_display_n_12,
      \FSM_sequential_current_state_reg[1]\ => inst_display_n_3,
      Q(2 downto 0) => reg(2 downto 0),
      \caracter[0]_i_6\ => inst_fsm_n_34,
      \caracter[1]_i_8\ => inst_fsm_n_29,
      \caracter[2]_i_6\ => inst_fsm_n_25,
      \caracter[4]_i_2\(1) => \disp_reg[2]\(4),
      \caracter[4]_i_2\(0) => \disp_reg[2]\(0),
      \caracter_reg[0]_0\ => inst_fsm_n_13,
      \caracter_reg[0]_1\ => inst_fsm_n_14,
      \caracter_reg[1]_0\ => inst_fsm_n_24,
      \caracter_reg[1]_1\ => inst_fsm_n_37,
      \caracter_reg[2]_0\ => inst_fsm_n_35,
      \caracter_reg[4]_0\ => inst_fsm_n_18,
      \caracter_reg[6]_0\ => inst_fsm_n_19,
      \charact_dot_reg[0]_0\ => inst_display_n_2,
      \charact_dot_reg[0]_1\ => inst_display_n_4,
      \charact_dot_reg[0]_2\ => inst_display_n_10,
      \charact_dot_reg[0]_3\ => inst_fsm_n_15,
      \charact_dot_reg[0]_4\ => inst_fsm_n_26,
      clk_BUFG => clk_BUFG,
      current_state(1 downto 0) => current_state(1 downto 0),
      digit_OBUF(6 downto 0) => digit_OBUF(6 downto 0),
      \disp_dinero[2]_2\(1) => \disp_dinero[2]_0\(3),
      \disp_dinero[2]_2\(0) => \disp_dinero[2]_0\(1),
      \disp_dinero[3]_1\(0) => \disp_dinero[3]_1\(3),
      \disp_dinero[5]_0\(1) => \disp_dinero[5]_3\(4),
      \disp_dinero[5]_0\(0) => \disp_dinero[5]_3\(0),
      \disp_reg[1]\(1) => \disp_reg[1]\(6),
      \disp_reg[1]\(0) => \disp_reg[1]\(3),
      \disp_reg[1][6]\ => inst_display_n_14,
      \disp_reg[3]\(3) => \disp_reg[3]\(6),
      \disp_reg[3]\(2 downto 1) => \disp_reg[3]\(4 downto 3),
      \disp_reg[3]\(0) => \disp_reg[3]\(1),
      \disp_reg[4]\(1) => \disp_reg[4]\(4),
      \disp_reg[4]\(0) => \disp_reg[4]\(1),
      \disp_reg[5]\(2) => \disp_reg[5]\(6),
      \disp_reg[5]\(1) => \disp_reg[5]\(4),
      \disp_reg[5]\(0) => \disp_reg[5]\(0),
      \disp_reg[6]\(0) => \disp_reg[6]\(3),
      \disp_reg[7]\(1) => \disp_reg[7]\(6),
      \disp_reg[7]\(0) => \disp_reg[7]\(3),
      \disp_reg[8]\(0) => \disp_reg[8]\(6),
      dot => dot_2,
      dot_OBUF => dot_OBUF,
      dot_reg_0 => inst_fsm_n_32,
      elem_OBUF(7 downto 0) => elem_OBUF(7 downto 0),
      output0 => output0,
      \reg_reg[0]\ => inst_display_n_6,
      \reg_reg[0]_0\ => inst_display_n_24,
      \reg_reg[0]_1\ => inst_display_n_32,
      \reg_reg[1]\ => inst_display_n_5,
      \reg_reg[1]_0\ => inst_display_n_13,
      \reg_reg[2]\ => inst_display_n_11,
      reset_IBUF => reset_IBUF
    );
inst_fsm: entity work.fsm
     port map (
      CO(0) => caracter2(3),
      D(2) => \disp[1]\(6),
      D(1) => inst_detectorflancos_n_15,
      D(0) => inst_detectorflancos_n_16,
      E(0) => inst_detectorflancos_n_12,
      \FSM_sequential_current_state_reg[0]_0\ => inst_fsm_n_15,
      \FSM_sequential_current_state_reg[0]_1\ => inst_detectorflancos_n_17,
      \FSM_sequential_current_state_reg[0]_2\ => inst_decodmonedas_n_8,
      \FSM_sequential_current_state_reg[1]_0\ => inst_fsm_n_26,
      \FSM_sequential_current_state_reg[1]_1\ => inst_detectorflancos_n_18,
      \FSM_sequential_current_state_reg[1]_2\ => inst_decodmonedas_n_0,
      Q(2 downto 0) => reg(2 downto 0),
      \caracter[2]_i_16\ => inst_display_n_2,
      \caracter_reg[0]\ => inst_display_n_4,
      \caracter_reg[0]_0\ => inst_display_n_14,
      \caracter_reg[0]_1\ => inst_display_n_24,
      \caracter_reg[1]\ => inst_display_n_32,
      \caracter_reg[1]_0\ => inst_display_n_3,
      \caracter_reg[1]_i_15\(1) => \disp_dinero[2]_0\(3),
      \caracter_reg[1]_i_15\(0) => \disp_dinero[2]_0\(1),
      \caracter_reg[2]\ => inst_display_n_10,
      \caracter_reg[4]\ => inst_display_n_6,
      \caracter_reg[4]_0\ => inst_display_n_5,
      \charact_dot_reg[0]\ => inst_fsm_n_13,
      \charact_dot_reg[0]_0\ => inst_fsm_n_25,
      \charact_dot_reg[0]_1\ => inst_fsm_n_35,
      \charact_dot_reg[0]_2\ => inst_display_n_13,
      clk_BUFG => clk_BUFG,
      current_state(1 downto 0) => current_state(1 downto 0),
      devolver_OBUF => devolver_OBUF,
      \disp_dinero[3]_1\(0) => \disp_dinero[3]_1\(3),
      \disp_dinero[5]_0\(1) => \disp_dinero[5]_3\(4),
      \disp_dinero[5]_0\(0) => \disp_dinero[5]_3\(0),
      \disp_reg[1]\(1) => \disp_reg[1]\(6),
      \disp_reg[1]\(0) => \disp_reg[1]\(3),
      \disp_reg[1][3]\ => inst_detectorflancos_n_1,
      \disp_reg[1][3]_0\ => inst_detectorflancos_n_2,
      \disp_reg[1][3]_1\ => inst_detectorflancos_n_4,
      \disp_reg[1][3]_2\ => inst_detectorflancos_n_3,
      \disp_reg[1][6]\ => inst_fsm_n_19,
      \disp_reg[2][4]\ => inst_fsm_n_18,
      \disp_reg[2][4]_0\(1) => \disp_reg[2]\(4),
      \disp_reg[2][4]_0\(0) => \disp_reg[2]\(0),
      \disp_reg[2][4]_1\(1) => inst_detectorflancos_n_6,
      \disp_reg[2][4]_1\(0) => inst_detectorflancos_n_7,
      \disp_reg[3]\(3) => \disp_reg[3]\(6),
      \disp_reg[3]\(2 downto 1) => \disp_reg[3]\(4 downto 3),
      \disp_reg[3]\(0) => \disp_reg[3]\(1),
      \disp_reg[3]_3_sp_1\ => inst_detectorflancos_n_11,
      \disp_reg[4]\(1) => \disp_reg[4]\(4),
      \disp_reg[4]\(0) => \disp_reg[4]\(1),
      \disp_reg[4][4]\ => inst_detectorflancos_n_0,
      \disp_reg[4]_1_sp_1\ => inst_detectorflancos_n_5,
      \disp_reg[5]\(2) => \disp_reg[5]\(6),
      \disp_reg[5]\(1) => \disp_reg[5]\(4),
      \disp_reg[5]\(0) => \disp_reg[5]\(0),
      \disp_reg[5][4]\ => inst_detectorflancos_n_9,
      \disp_reg[5][6]\ => inst_detectorflancos_n_20,
      \disp_reg[5]_0_sp_1\ => inst_detectorflancos_n_21,
      \disp_reg[6]\(0) => \disp_reg[6]\(3),
      \disp_reg[7]\(1) => \disp_reg[7]\(6),
      \disp_reg[7]\(0) => \disp_reg[7]\(3),
      \disp_reg[7][3]\ => inst_detectorflancos_n_10,
      \disp_reg[7][6]\ => inst_fsm_n_29,
      \disp_reg[7][6]_0\ => inst_fsm_n_32,
      \disp_reg[7][6]_1\ => inst_detectorflancos_n_8,
      \disp_reg[8]\(0) => \disp_reg[8]\(6),
      \disp_reg[8][6]\ => inst_detectorflancos_n_19,
      dot => dot_2,
      dot_reg => inst_display_n_11,
      dot_reg_0 => inst_display_n_12,
      output0 => output0,
      p_0_out(0) => p_0_out(3),
      refresco_OBUF => refresco_OBUF,
      \reg_reg[0]\ => inst_fsm_n_14,
      \reg_reg[0]_0\ => inst_fsm_n_24,
      \reg_reg[0]_1\ => inst_fsm_n_37,
      \total_string_reg[0]\ => inst_fsm_n_34,
      \total_string_reg[7]\(6 downto 0) => valor_moneda(6 downto 0)
    );
inst_prescaler: entity work.prescaler
     port map (
      clk => clk,
      clk_100Mh => clk_100Mh_IBUF_BUFG
    );
inst_sincronizador: entity work.sincronizador
     port map (
      button_ok_IBUF => button_ok_IBUF,
      clk_BUFG => clk_BUFG,
      monedas_IBUF(4 downto 0) => monedas_IBUF(4 downto 0),
      ref_option_IBUF(3 downto 0) => ref_option_IBUF(3 downto 0),
      \sreg_reg[0]\ => inst_sincronizador_n_0,
      \sreg_reg[0]_0\ => inst_sincronizador_n_1,
      \sreg_reg[0]_1\ => inst_sincronizador_n_2,
      \sreg_reg[0]_2\ => inst_sincronizador_n_3,
      \sreg_reg[0]_3\ => inst_sincronizador_n_4,
      \sreg_reg[0]_4\ => inst_sincronizador_n_5,
      \sreg_reg[0]_5\ => inst_sincronizador_n_6,
      \sreg_reg[0]_6\ => inst_sincronizador_n_7,
      \sreg_reg[0]_7\ => inst_sincronizador_n_8,
      \sreg_reg[0]_8\ => inst_sincronizador_n_9
    );
\monedas_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => monedas(0),
      O => monedas_IBUF(0)
    );
\monedas_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => monedas(1),
      O => monedas_IBUF(1)
    );
\monedas_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => monedas(2),
      O => monedas_IBUF(2)
    );
\monedas_IBUF[3]_inst\: unisim.vcomponents.IBUF
     port map (
      I => monedas(3),
      O => monedas_IBUF(3)
    );
\monedas_IBUF[4]_inst\: unisim.vcomponents.IBUF
     port map (
      I => monedas(4),
      O => monedas_IBUF(4)
    );
\ref_option_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ref_option(0),
      O => ref_option_IBUF(0)
    );
\ref_option_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ref_option(1),
      O => ref_option_IBUF(1)
    );
\ref_option_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ref_option(2),
      O => ref_option_IBUF(2)
    );
\ref_option_IBUF[3]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ref_option(3),
      O => ref_option_IBUF(3)
    );
refresco_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => refresco_OBUF,
      O => refresco
    );
reset_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => reset,
      O => reset_IBUF
    );
end STRUCTURE;
