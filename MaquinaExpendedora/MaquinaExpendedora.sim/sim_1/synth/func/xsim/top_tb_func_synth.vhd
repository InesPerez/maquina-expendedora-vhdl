-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Sun Jan  3 17:36:33 2021
-- Host        : LAPTOP-7BL7BHFF running 64-bit major release  (build 9200)
-- Command     : write_vhdl -mode funcsim -nolib -force -file
--               C:/Users/Inees/Desktop/Trabajo_SED/MaquinaExpendedora_aux/MaquinaExpendedora_aux.sim/sim_1/synth/func/xsim/top_tb_func_synth.vhd
-- Design      : top
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tcsg324-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity contador is
  port (
    reset : out STD_LOGIC;
    \reg_reg[10]_0\ : out STD_LOGIC;
    clk_100Mh_IBUF_BUFG : in STD_LOGIC;
    reset_IBUF : in STD_LOGIC;
    clk : in STD_LOGIC
  );
end contador;

architecture STRUCTURE of contador is
  signal clk_pre_i_2_n_0 : STD_LOGIC;
  signal clk_pre_i_3_n_0 : STD_LOGIC;
  signal clk_pre_i_4_n_0 : STD_LOGIC;
  signal \reg[0]_i_2_n_0\ : STD_LOGIC;
  signal reg_reg : STD_LOGIC_VECTOR ( 16 downto 0 );
  signal \reg_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \reg_reg[0]_i_1_n_1\ : STD_LOGIC;
  signal \reg_reg[0]_i_1_n_2\ : STD_LOGIC;
  signal \reg_reg[0]_i_1_n_3\ : STD_LOGIC;
  signal \reg_reg[0]_i_1_n_4\ : STD_LOGIC;
  signal \reg_reg[0]_i_1_n_5\ : STD_LOGIC;
  signal \reg_reg[0]_i_1_n_6\ : STD_LOGIC;
  signal \reg_reg[0]_i_1_n_7\ : STD_LOGIC;
  signal \reg_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \reg_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \reg_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \reg_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \reg_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \reg_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \reg_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \reg_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \reg_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \reg_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \reg_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \reg_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \reg_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \reg_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \reg_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \reg_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \reg_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \reg_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \reg_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \reg_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \reg_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \reg_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \reg_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \reg_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \reg_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal \^reset\ : STD_LOGIC;
  signal \NLW_reg_reg[16]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_reg_reg[16]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
begin
  reset <= \^reset\;
clk_pre_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFBFFFF00040000"
    )
        port map (
      I0 => reg_reg(10),
      I1 => reg_reg(14),
      I2 => reg_reg(3),
      I3 => clk_pre_i_2_n_0,
      I4 => clk_pre_i_3_n_0,
      I5 => clk,
      O => \reg_reg[10]_0\
    );
clk_pre_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFEFFF"
    )
        port map (
      I0 => reg_reg(5),
      I1 => reg_reg(9),
      I2 => reg_reg(16),
      I3 => reg_reg(13),
      I4 => clk_pre_i_4_n_0,
      O => clk_pre_i_2_n_0
    );
clk_pre_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000800"
    )
        port map (
      I0 => reg_reg(11),
      I1 => reg_reg(2),
      I2 => reg_reg(7),
      I3 => reg_reg(1),
      I4 => reg_reg(8),
      I5 => reg_reg(12),
      O => clk_pre_i_3_n_0
    );
clk_pre_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => reg_reg(6),
      I1 => reg_reg(4),
      I2 => reg_reg(15),
      I3 => reg_reg(0),
      O => clk_pre_i_4_n_0
    );
\current_state[1]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => reset_IBUF,
      O => \^reset\
    );
\reg[0]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => reg_reg(0),
      O => \reg[0]_i_2_n_0\
    );
\reg_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[0]_i_1_n_7\,
      Q => reg_reg(0)
    );
\reg_reg[0]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \reg_reg[0]_i_1_n_0\,
      CO(2) => \reg_reg[0]_i_1_n_1\,
      CO(1) => \reg_reg[0]_i_1_n_2\,
      CO(0) => \reg_reg[0]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \reg_reg[0]_i_1_n_4\,
      O(2) => \reg_reg[0]_i_1_n_5\,
      O(1) => \reg_reg[0]_i_1_n_6\,
      O(0) => \reg_reg[0]_i_1_n_7\,
      S(3 downto 1) => reg_reg(3 downto 1),
      S(0) => \reg[0]_i_2_n_0\
    );
\reg_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[8]_i_1_n_5\,
      Q => reg_reg(10)
    );
\reg_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[8]_i_1_n_4\,
      Q => reg_reg(11)
    );
\reg_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[12]_i_1_n_7\,
      Q => reg_reg(12)
    );
\reg_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \reg_reg[8]_i_1_n_0\,
      CO(3) => \reg_reg[12]_i_1_n_0\,
      CO(2) => \reg_reg[12]_i_1_n_1\,
      CO(1) => \reg_reg[12]_i_1_n_2\,
      CO(0) => \reg_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \reg_reg[12]_i_1_n_4\,
      O(2) => \reg_reg[12]_i_1_n_5\,
      O(1) => \reg_reg[12]_i_1_n_6\,
      O(0) => \reg_reg[12]_i_1_n_7\,
      S(3 downto 0) => reg_reg(15 downto 12)
    );
\reg_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[12]_i_1_n_6\,
      Q => reg_reg(13)
    );
\reg_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[12]_i_1_n_5\,
      Q => reg_reg(14)
    );
\reg_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[12]_i_1_n_4\,
      Q => reg_reg(15)
    );
\reg_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[16]_i_1_n_7\,
      Q => reg_reg(16)
    );
\reg_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \reg_reg[12]_i_1_n_0\,
      CO(3 downto 0) => \NLW_reg_reg[16]_i_1_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_reg_reg[16]_i_1_O_UNCONNECTED\(3 downto 1),
      O(0) => \reg_reg[16]_i_1_n_7\,
      S(3 downto 1) => B"000",
      S(0) => reg_reg(16)
    );
\reg_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[0]_i_1_n_6\,
      Q => reg_reg(1)
    );
\reg_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[0]_i_1_n_5\,
      Q => reg_reg(2)
    );
\reg_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[0]_i_1_n_4\,
      Q => reg_reg(3)
    );
\reg_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[4]_i_1_n_7\,
      Q => reg_reg(4)
    );
\reg_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \reg_reg[0]_i_1_n_0\,
      CO(3) => \reg_reg[4]_i_1_n_0\,
      CO(2) => \reg_reg[4]_i_1_n_1\,
      CO(1) => \reg_reg[4]_i_1_n_2\,
      CO(0) => \reg_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \reg_reg[4]_i_1_n_4\,
      O(2) => \reg_reg[4]_i_1_n_5\,
      O(1) => \reg_reg[4]_i_1_n_6\,
      O(0) => \reg_reg[4]_i_1_n_7\,
      S(3 downto 0) => reg_reg(7 downto 4)
    );
\reg_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[4]_i_1_n_6\,
      Q => reg_reg(5)
    );
\reg_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[4]_i_1_n_5\,
      Q => reg_reg(6)
    );
\reg_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[4]_i_1_n_4\,
      Q => reg_reg(7)
    );
\reg_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[8]_i_1_n_7\,
      Q => reg_reg(8)
    );
\reg_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \reg_reg[4]_i_1_n_0\,
      CO(3) => \reg_reg[8]_i_1_n_0\,
      CO(2) => \reg_reg[8]_i_1_n_1\,
      CO(1) => \reg_reg[8]_i_1_n_2\,
      CO(0) => \reg_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \reg_reg[8]_i_1_n_4\,
      O(2) => \reg_reg[8]_i_1_n_5\,
      O(1) => \reg_reg[8]_i_1_n_6\,
      O(0) => \reg_reg[8]_i_1_n_7\,
      S(3 downto 0) => reg_reg(11 downto 8)
    );
\reg_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      CLR => \^reset\,
      D => \reg_reg[8]_i_1_n_6\,
      Q => reg_reg(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \contador__parameterized1\ is
  port (
    \reg_reg[2]_0\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \charact_dot_reg[0]\ : out STD_LOGIC;
    \reg_reg[2]_1\ : out STD_LOGIC;
    \charact_dot_reg[0]_0\ : out STD_LOGIC;
    \reg_reg[1]_0\ : out STD_LOGIC;
    \reg_reg[0]_0\ : out STD_LOGIC;
    \charact_dot_reg[0]_1\ : out STD_LOGIC;
    \reg_reg[2]_2\ : out STD_LOGIC;
    \reg_reg[2]_3\ : out STD_LOGIC;
    \reg_reg[1]_1\ : out STD_LOGIC;
    \reg_reg[1]_2\ : out STD_LOGIC;
    \reg_reg[0]_1\ : out STD_LOGIC;
    \reg_reg[2]_4\ : out STD_LOGIC;
    elem_OBUF : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \charact_dot_reg[0]_2\ : out STD_LOGIC;
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter[6]_i_4\ : out STD_LOGIC;
    dot : out STD_LOGIC;
    plusOp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \caracter[6]_i_4_0\ : in STD_LOGIC;
    \charact_dot_reg[0]_3\ : in STD_LOGIC;
    \disp_refresco[1]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter[0]_i_5\ : in STD_LOGIC;
    \caracter_reg[4]\ : in STD_LOGIC;
    \caracter_reg[4]_0\ : in STD_LOGIC;
    \caracter_reg[4]_1\ : in STD_LOGIC;
    \caracter_reg[6]\ : in STD_LOGIC;
    \caracter_reg[6]_0\ : in STD_LOGIC;
    \caracter_reg[6]_1\ : in STD_LOGIC;
    \caracter[2]_i_6\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_refresco[5]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    reset_IBUF : in STD_LOGIC;
    \caracter_reg[4]_2\ : in STD_LOGIC;
    \caracter_reg[6]_2\ : in STD_LOGIC;
    \caracter_reg[4]_3\ : in STD_LOGIC;
    \caracter_reg[4]_4\ : in STD_LOGIC;
    clk_BUFG : in STD_LOGIC;
    output0 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \contador__parameterized1\ : entity is "contador";
end \contador__parameterized1\;

architecture STRUCTURE of \contador__parameterized1\ is
  signal \^co\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^q\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal caracter2 : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \caracter[4]_i_4_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_10_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_11_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_12_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_13_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_3_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_8_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_9_n_0\ : STD_LOGIC;
  signal \caracter_reg[6]_i_2_n_2\ : STD_LOGIC;
  signal \caracter_reg[6]_i_2_n_3\ : STD_LOGIC;
  signal \^charact_dot_reg[0]_0\ : STD_LOGIC;
  signal \reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \reg[1]_i_1_n_0\ : STD_LOGIC;
  signal \reg[2]_i_1_n_0\ : STD_LOGIC;
  signal \^reg_reg[0]_0\ : STD_LOGIC;
  signal \^reg_reg[1]_0\ : STD_LOGIC;
  signal \^reg_reg[2]_1\ : STD_LOGIC;
  signal \NLW_caracter_reg[6]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \NLW_caracter_reg[6]_i_2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \caracter[0]_i_11\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \caracter[3]_i_7\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \caracter[3]_i_8\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \caracter[4]_i_8\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \caracter[6]_i_16\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \caracter[6]_i_24\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \caracter[6]_i_26\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \caracter[6]_i_38\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \caracter[6]_i_6\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \charact_dot[0]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of dot_i_2 : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \elem_OBUF[0]_inst_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \elem_OBUF[1]_inst_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \elem_OBUF[2]_inst_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \elem_OBUF[3]_inst_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \elem_OBUF[4]_inst_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \elem_OBUF[5]_inst_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \elem_OBUF[6]_inst_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \elem_OBUF[7]_inst_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \reg[0]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \reg[1]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \reg[2]_i_1\ : label is "soft_lutpair10";
begin
  CO(0) <= \^co\(0);
  Q(2 downto 0) <= \^q\(2 downto 0);
  \charact_dot_reg[0]_0\ <= \^charact_dot_reg[0]_0\;
  \reg_reg[0]_0\ <= \^reg_reg[0]_0\;
  \reg_reg[1]_0\ <= \^reg_reg[1]_0\;
  \reg_reg[2]_1\ <= \^reg_reg[2]_1\;
\caracter[0]_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      O => \reg_reg[1]_1\
    );
\caracter[0]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EC012C00EC01EC01"
    )
        port map (
      I0 => \charact_dot_reg[0]_3\,
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \^q\(2),
      I4 => \disp_refresco[1]_0\(0),
      I5 => \caracter[0]_i_5\,
      O => \charact_dot_reg[0]\
    );
\caracter[0]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(2),
      O => plusOp(1)
    );
\caracter[2]_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5555AAAAAEAA0000"
    )
        port map (
      I0 => \^q\(2),
      I1 => \caracter[2]_i_6\(0),
      I2 => \caracter[2]_i_6\(1),
      I3 => \disp_refresco[5]_1\(0),
      I4 => \^q\(1),
      I5 => \^q\(0),
      O => \reg_reg[2]_2\
    );
\caracter[3]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"42"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(1),
      I2 => \^q\(0),
      O => \reg_reg[2]_4\
    );
\caracter[3]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"68"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(0),
      I2 => \^q\(1),
      O => \reg_reg[2]_3\
    );
\caracter[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C0C0C5C0C5C5C5C5"
    )
        port map (
      I0 => \^co\(0),
      I1 => \caracter_reg[4]_3\,
      I2 => \caracter_reg[4]_2\,
      I3 => \^reg_reg[2]_1\,
      I4 => \caracter_reg[4]_4\,
      I5 => \caracter[4]_i_4_n_0\,
      O => D(0)
    );
\caracter[4]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000DDDFFFDF"
    )
        port map (
      I0 => \^charact_dot_reg[0]_0\,
      I1 => \^reg_reg[1]_0\,
      I2 => \caracter_reg[4]\,
      I3 => \^reg_reg[0]_0\,
      I4 => \caracter_reg[4]_0\,
      I5 => \caracter_reg[4]_1\,
      O => \caracter[4]_i_4_n_0\
    );
\caracter[4]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"95"
    )
        port map (
      I0 => \^q\(1),
      I1 => \charact_dot_reg[0]_3\,
      I2 => \^q\(0),
      O => \^reg_reg[1]_0\
    );
\caracter[4]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(0),
      I1 => \charact_dot_reg[0]_3\,
      O => \^reg_reg[0]_0\
    );
\caracter[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C0C0C0C5C5C5C0C5"
    )
        port map (
      I0 => \^co\(0),
      I1 => \caracter[6]_i_3_n_0\,
      I2 => \caracter_reg[4]_2\,
      I3 => \caracter_reg[6]_2\,
      I4 => \^reg_reg[2]_1\,
      I5 => \caracter_reg[6]_1\,
      O => D(1)
    );
\caracter[6]_i_10\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(0),
      O => \caracter[6]_i_10_n_0\
    );
\caracter[6]_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(2),
      O => \caracter[6]_i_11_n_0\
    );
\caracter[6]_i_12\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      O => \caracter[6]_i_12_n_0\
    );
\caracter[6]_i_13\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(0),
      I1 => \charact_dot_reg[0]_3\,
      O => \caracter[6]_i_13_n_0\
    );
\caracter[6]_i_16\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"006A"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(0),
      I2 => \^q\(1),
      I3 => \caracter[6]_i_4_0\,
      O => \reg_reg[2]_0\
    );
\caracter[6]_i_18\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      O => plusOp(0)
    );
\caracter[6]_i_24\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8013"
    )
        port map (
      I0 => \charact_dot_reg[0]_3\,
      I1 => \^q\(2),
      I2 => \^q\(0),
      I3 => \^q\(1),
      O => \charact_dot_reg[0]_1\
    );
\caracter[6]_i_26\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"13FE"
    )
        port map (
      I0 => \charact_dot_reg[0]_3\,
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \^q\(2),
      O => \^charact_dot_reg[0]_0\
    );
\caracter[6]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"008880F577FF80F5"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \caracter_reg[6]\,
      I3 => \caracter_reg[6]_0\,
      I4 => \^q\(2),
      I5 => \caracter_reg[6]_1\,
      O => \caracter[6]_i_3_n_0\
    );
\caracter[6]_i_38\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      O => \reg_reg[1]_2\
    );
\caracter[6]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2820"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(0),
      I2 => \^q\(1),
      I3 => \charact_dot_reg[0]_3\,
      O => \^reg_reg[2]_1\
    );
\caracter[6]_i_65\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000240004040404"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(2),
      I2 => \^q\(1),
      I3 => \disp_refresco[5]_1\(0),
      I4 => \caracter[2]_i_6\(1),
      I5 => \caracter[2]_i_6\(0),
      O => \reg_reg[0]_1\
    );
\caracter[6]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(2),
      O => \caracter[6]_i_8_n_0\
    );
\caracter[6]_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      O => \caracter[6]_i_9_n_0\
    );
\caracter_reg[6]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \^co\(0),
      CO(2) => \NLW_caracter_reg[6]_i_2_CO_UNCONNECTED\(2),
      CO(1) => \caracter_reg[6]_i_2_n_2\,
      CO(0) => \caracter_reg[6]_i_2_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \caracter[6]_i_8_n_0\,
      DI(1) => \caracter[6]_i_9_n_0\,
      DI(0) => \caracter[6]_i_10_n_0\,
      O(3) => \NLW_caracter_reg[6]_i_2_O_UNCONNECTED\(3),
      O(2) => caracter2(2),
      O(1 downto 0) => \NLW_caracter_reg[6]_i_2_O_UNCONNECTED\(1 downto 0),
      S(3) => '1',
      S(2) => \caracter[6]_i_11_n_0\,
      S(1) => \caracter[6]_i_12_n_0\,
      S(0) => \caracter[6]_i_13_n_0\
    );
\charact_dot[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FC4C"
    )
        port map (
      I0 => \^co\(0),
      I1 => \charact_dot_reg[0]_3\,
      I2 => reset_IBUF,
      I3 => \caracter_reg[4]_2\,
      O => \charact_dot_reg[0]_2\
    );
dot_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"D"
    )
        port map (
      I0 => \^co\(0),
      I1 => \caracter_reg[4]_2\,
      O => dot
    );
dot_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^co\(0),
      I1 => \caracter_reg[4]_2\,
      O => \caracter[6]_i_4\
    );
\elem_OBUF[0]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEF"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(0),
      I2 => reset_IBUF,
      I3 => \^q\(1),
      O => elem_OBUF(0)
    );
\elem_OBUF[1]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FDFF"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => \^q\(2),
      I3 => reset_IBUF,
      O => elem_OBUF(1)
    );
\elem_OBUF[2]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FBFF"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => reset_IBUF,
      O => elem_OBUF(2)
    );
\elem_OBUF[3]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F7FF"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(2),
      I3 => reset_IBUF,
      O => elem_OBUF(3)
    );
\elem_OBUF[4]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FBFF"
    )
        port map (
      I0 => \^q\(0),
      I1 => reset_IBUF,
      I2 => \^q\(1),
      I3 => \^q\(2),
      O => elem_OBUF(4)
    );
\elem_OBUF[5]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => reset_IBUF,
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \^q\(2),
      O => elem_OBUF(5)
    );
\elem_OBUF[6]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => reset_IBUF,
      I3 => \^q\(2),
      O => elem_OBUF(6)
    );
\elem_OBUF[7]_inst_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => reset_IBUF,
      I1 => \^q\(2),
      I2 => \^q\(0),
      I3 => \^q\(1),
      O => elem_OBUF(7)
    );
\reg[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(0),
      O => \reg[0]_i_1_n_0\
    );
\reg[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      O => \reg[1]_i_1_n_0\
    );
\reg[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(0),
      I2 => \^q\(1),
      O => \reg[2]_i_1_n_0\
    );
\reg_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => \reg[0]_i_1_n_0\,
      Q => \^q\(0)
    );
\reg_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => \reg[1]_i_1_n_0\,
      Q => \^q\(1)
    );
\reg_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => \reg[2]_i_1_n_0\,
      Q => \^q\(2)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decod_monedas is
  port (
    D : out STD_LOGIC_VECTOR ( 1 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 6 downto 0 );
    \current_state_reg[0]\ : out STD_LOGIC;
    \current_state_reg[1]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    monedas_IBUF : in STD_LOGIC_VECTOR ( 4 downto 0 );
    clk_BUFG : in STD_LOGIC
  );
end decod_monedas;

architecture STRUCTURE of decod_monedas is
  signal \^q\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \next_state_reg[1]_i_3_n_0\ : STD_LOGIC;
  signal \valor[0]_i_1_n_0\ : STD_LOGIC;
  signal \valor[1]_i_1_n_0\ : STD_LOGIC;
  signal \valor[2]_i_1_n_0\ : STD_LOGIC;
  signal \valor[3]_i_1_n_0\ : STD_LOGIC;
  signal \valor[4]_i_1_n_0\ : STD_LOGIC;
  signal \valor[5]_i_1_n_0\ : STD_LOGIC;
  signal \valor[6]_i_1_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \valor[0]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \valor[1]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \valor[2]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \valor[3]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \valor[4]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \valor[5]_i_1\ : label is "soft_lutpair2";
begin
  Q(6 downto 0) <= \^q\(6 downto 0);
\next_state_reg[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5555555555555551"
    )
        port map (
      I0 => \current_state_reg[1]\(0),
      I1 => \current_state_reg[1]\(1),
      I2 => \^q\(4),
      I3 => \^q\(3),
      I4 => \^q\(5),
      I5 => \next_state_reg[1]_i_3_n_0\,
      O => D(0)
    );
\next_state_reg[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFFFFFE0000"
    )
        port map (
      I0 => \^q\(4),
      I1 => \^q\(3),
      I2 => \^q\(5),
      I3 => \next_state_reg[1]_i_3_n_0\,
      I4 => \current_state_reg[1]\(1),
      I5 => \current_state_reg[1]\(0),
      O => D(1)
    );
\next_state_reg[1]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(1),
      I2 => \^q\(6),
      I3 => \^q\(0),
      O => \next_state_reg[1]_i_3_n_0\
    );
\next_state_reg[1]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4444444444444440"
    )
        port map (
      I0 => \current_state_reg[1]\(0),
      I1 => \current_state_reg[1]\(1),
      I2 => \next_state_reg[1]_i_3_n_0\,
      I3 => \^q\(5),
      I4 => \^q\(3),
      I5 => \^q\(4),
      O => \current_state_reg[0]\
    );
\valor[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000002"
    )
        port map (
      I0 => monedas_IBUF(0),
      I1 => monedas_IBUF(2),
      I2 => monedas_IBUF(1),
      I3 => monedas_IBUF(3),
      I4 => monedas_IBUF(4),
      O => \valor[0]_i_1_n_0\
    );
\valor[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000110"
    )
        port map (
      I0 => monedas_IBUF(4),
      I1 => monedas_IBUF(2),
      I2 => monedas_IBUF(1),
      I3 => monedas_IBUF(3),
      I4 => monedas_IBUF(0),
      O => \valor[1]_i_1_n_0\
    );
\valor[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00010110"
    )
        port map (
      I0 => monedas_IBUF(3),
      I1 => monedas_IBUF(1),
      I2 => monedas_IBUF(4),
      I3 => monedas_IBUF(2),
      I4 => monedas_IBUF(0),
      O => \valor[2]_i_1_n_0\
    );
\valor[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000002"
    )
        port map (
      I0 => monedas_IBUF(1),
      I1 => monedas_IBUF(2),
      I2 => monedas_IBUF(0),
      I3 => monedas_IBUF(3),
      I4 => monedas_IBUF(4),
      O => \valor[3]_i_1_n_0\
    );
\valor[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000110"
    )
        port map (
      I0 => monedas_IBUF(4),
      I1 => monedas_IBUF(0),
      I2 => monedas_IBUF(2),
      I3 => monedas_IBUF(3),
      I4 => monedas_IBUF(1),
      O => \valor[4]_i_1_n_0\
    );
\valor[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000110"
    )
        port map (
      I0 => monedas_IBUF(0),
      I1 => monedas_IBUF(2),
      I2 => monedas_IBUF(3),
      I3 => monedas_IBUF(4),
      I4 => monedas_IBUF(1),
      O => \valor[5]_i_1_n_0\
    );
\valor[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000002"
    )
        port map (
      I0 => monedas_IBUF(4),
      I1 => monedas_IBUF(2),
      I2 => monedas_IBUF(1),
      I3 => monedas_IBUF(3),
      I4 => monedas_IBUF(0),
      O => \valor[6]_i_1_n_0\
    );
\valor_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \valor[0]_i_1_n_0\,
      Q => \^q\(0),
      R => '0'
    );
\valor_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \valor[1]_i_1_n_0\,
      Q => \^q\(1),
      R => '0'
    );
\valor_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \valor[2]_i_1_n_0\,
      Q => \^q\(2),
      R => '0'
    );
\valor_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \valor[3]_i_1_n_0\,
      Q => \^q\(3),
      R => '0'
    );
\valor_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \valor[4]_i_1_n_0\,
      Q => \^q\(4),
      R => '0'
    );
\valor_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \valor[5]_i_1_n_0\,
      Q => \^q\(5),
      R => '0'
    );
\valor_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \valor[6]_i_1_n_0\,
      Q => \^q\(6),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity digito is
  port (
    digit_OBUF : out STD_LOGIC_VECTOR ( 3 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 5 downto 0 )
  );
end digito;

architecture STRUCTURE of digito is
begin
\digit_OBUF[0]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0008000390102081"
    )
        port map (
      I0 => Q(0),
      I1 => Q(1),
      I2 => Q(5),
      I3 => Q(3),
      I4 => Q(2),
      I5 => Q(4),
      O => digit_OBUF(0)
    );
\digit_OBUF[1]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEEABDBC7B33BBFF"
    )
        port map (
      I0 => Q(3),
      I1 => Q(5),
      I2 => Q(1),
      I3 => Q(0),
      I4 => Q(2),
      I5 => Q(4),
      O => digit_OBUF(1)
    );
\digit_OBUF[2]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DFFFECFFCFFB8773"
    )
        port map (
      I0 => Q(1),
      I1 => Q(4),
      I2 => Q(2),
      I3 => Q(5),
      I4 => Q(3),
      I5 => Q(0),
      O => digit_OBUF(2)
    );
\digit_OBUF[4]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFC3FDFC1F0FF9F"
    )
        port map (
      I0 => Q(0),
      I1 => Q(2),
      I2 => Q(5),
      I3 => Q(1),
      I4 => Q(4),
      I5 => Q(3),
      O => digit_OBUF(3)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gestion_refresco is
  port (
    \disp_aux_reg[6][3]_i_2_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_aux[1]_7\ : out STD_LOGIC;
    \disp_refresco[4]_1\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_aux_reg[6][3]_i_2_1\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_aux_reg[6][3]_i_2_2\ : out STD_LOGIC;
    \disp_aux_reg[6][3]_i_2_3\ : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \reg_reg[2]\ : out STD_LOGIC;
    \reg_reg[1]\ : out STD_LOGIC;
    \reg_reg[0]\ : out STD_LOGIC;
    \current_state_reg[1]\ : out STD_LOGIC;
    \reg_reg[0]_0\ : out STD_LOGIC;
    \current_state_reg[1]_0\ : out STD_LOGIC;
    \current_state_reg[1]_1\ : out STD_LOGIC;
    \current_state_reg[1]_2\ : out STD_LOGIC;
    \current_state_reg[0]\ : out STD_LOGIC;
    \current_state_reg[1]_3\ : out STD_LOGIC;
    \current_state_reg[1]_4\ : out STD_LOGIC;
    \current_state_reg[1]_5\ : out STD_LOGIC;
    \current_state_reg[1]_6\ : out STD_LOGIC;
    \reg_reg[0]_1\ : out STD_LOGIC;
    \current_state_reg[0]_0\ : out STD_LOGIC;
    \current_state_reg[1]_7\ : out STD_LOGIC;
    \reg_reg[1]_0\ : out STD_LOGIC;
    \current_state_reg[1]_8\ : out STD_LOGIC;
    plusOp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \caracter_reg[2]\ : in STD_LOGIC;
    \caracter_reg[2]_0\ : in STD_LOGIC;
    \caracter_reg[2]_1\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \caracter_reg[0]\ : in STD_LOGIC;
    \caracter[6]_i_28_0\ : in STD_LOGIC;
    \caracter[6]_i_28_1\ : in STD_LOGIC;
    \caracter_reg[0]_0\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \caracter_reg[3]\ : in STD_LOGIC;
    \caracter_reg[0]_1\ : in STD_LOGIC;
    \caracter_reg[3]_0\ : in STD_LOGIC;
    \caracter_reg[2]_2\ : in STD_LOGIC;
    \caracter_reg[1]\ : in STD_LOGIC;
    \caracter[1]_i_4_0\ : in STD_LOGIC;
    \caracter[2]_i_3\ : in STD_LOGIC;
    \caracter[2]_i_3_0\ : in STD_LOGIC;
    \caracter_reg[3]_1\ : in STD_LOGIC;
    \caracter_reg[3]_2\ : in STD_LOGIC;
    \caracter[6]_i_5_0\ : in STD_LOGIC;
    \caracter_reg[0]_2\ : in STD_LOGIC;
    \caracter_reg[1]_0\ : in STD_LOGIC;
    \caracter_reg[1]_1\ : in STD_LOGIC;
    \caracter[3]_i_2_0\ : in STD_LOGIC;
    \caracter_reg[2]_3\ : in STD_LOGIC;
    \caracter_reg[2]_4\ : in STD_LOGIC;
    \caracter_reg[0]_3\ : in STD_LOGIC;
    \caracter[6]_i_20_0\ : in STD_LOGIC;
    \caracter[0]_i_5_0\ : in STD_LOGIC;
    \caracter[3]_i_3_0\ : in STD_LOGIC;
    \caracter[1]_i_2_0\ : in STD_LOGIC;
    \caracter[1]_i_2_1\ : in STD_LOGIC;
    \caracter[6]_i_57\ : in STD_LOGIC;
    ref_option_IBUF : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter_reg[4]\ : in STD_LOGIC;
    \caracter[1]_i_8\ : in STD_LOGIC;
    \caracter_reg[2]_5\ : in STD_LOGIC
  );
end gestion_refresco;

architecture STRUCTURE of gestion_refresco is
  signal \caracter[0]_i_10_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_12_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_14_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_2_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_4_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_5_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_6_n_0\ : STD_LOGIC;
  signal \caracter[0]_i_7_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_10_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_11_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_12_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_2_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_3_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_4_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_5_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_6_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_7_n_0\ : STD_LOGIC;
  signal \caracter[1]_i_9_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_17_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_18_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_2_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_4_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_7_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_10_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_13_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_14_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_2_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_3_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_4_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_5_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_6_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_9_n_0\ : STD_LOGIC;
  signal \caracter[4]_i_5_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_17_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_20_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_22_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_23_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_25_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_27_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_35_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_42_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_43_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_44_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_45_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_46_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_58_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_62_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_66_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_77_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_78_n_0\ : STD_LOGIC;
  signal \caracter_reg[6]_i_41_n_0\ : STD_LOGIC;
  signal \caracter_reg[6]_i_64_n_0\ : STD_LOGIC;
  signal \^current_state_reg[0]\ : STD_LOGIC;
  signal \^current_state_reg[1]_0\ : STD_LOGIC;
  signal \^current_state_reg[1]_1\ : STD_LOGIC;
  signal \^current_state_reg[1]_3\ : STD_LOGIC;
  signal \^current_state_reg[1]_8\ : STD_LOGIC;
  signal \^disp_aux[1]_7\ : STD_LOGIC;
  signal \disp_aux_reg[1][3]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[1][6]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[2][0]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[2][3]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[2][4]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[2][6]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[3][0]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[3][1]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[3][3]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[3][4]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[4][1]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[4][3]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[5][0]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[5][2]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[5][4]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[5][6]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[6][0]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[6][1]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[6][3]_i_1_n_0\ : STD_LOGIC;
  signal \^disp_aux_reg[6][3]_i_2_0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^disp_aux_reg[6][3]_i_2_1\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^disp_aux_reg[6][3]_i_2_2\ : STD_LOGIC;
  signal \^disp_aux_reg[6][3]_i_2_3\ : STD_LOGIC;
  signal \disp_aux_reg[7][6]_i_1_n_0\ : STD_LOGIC;
  signal \disp_refresco[1]_3\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \disp_refresco[2]_2\ : STD_LOGIC_VECTOR ( 6 downto 4 );
  signal \disp_refresco[3]_5\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \^disp_refresco[4]_1\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \disp_refresco[5]_0\ : STD_LOGIC_VECTOR ( 6 to 6 );
  signal \disp_refresco[6]_4\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \disp_refresco[7]_6\ : STD_LOGIC_VECTOR ( 6 to 6 );
  signal \^reg_reg[0]\ : STD_LOGIC;
  signal \^reg_reg[2]\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \caracter[0]_i_10\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \caracter[0]_i_14\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \caracter[0]_i_6\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \caracter[0]_i_7\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \caracter[1]_i_10\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \caracter[1]_i_7\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \caracter[2]_i_17\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \caracter[2]_i_18\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \caracter[2]_i_4\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \caracter[2]_i_7\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \caracter[2]_i_9\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \caracter[3]_i_10\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \caracter[3]_i_13\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \caracter[3]_i_14\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \caracter[3]_i_4\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \caracter[3]_i_5\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \caracter[3]_i_6\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \caracter[4]_i_6\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \caracter[4]_i_9\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \caracter[6]_i_22\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \caracter[6]_i_23\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \caracter[6]_i_27\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \caracter[6]_i_29\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \caracter[6]_i_33\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \caracter[6]_i_35\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \caracter[6]_i_40\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \caracter[6]_i_56\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \caracter[6]_i_58\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \caracter[6]_i_66\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \caracter[6]_i_7\ : label is "soft_lutpair39";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[1][3]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[1][3]_i_1\ : label is "soft_lutpair23";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[1][6]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[1][6]_i_1\ : label is "soft_lutpair26";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[2][0]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[2][0]_i_1\ : label is "soft_lutpair25";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[2][3]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[2][3]_i_1\ : label is "soft_lutpair24";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[2][4]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[2][6]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[2][6]_i_1\ : label is "soft_lutpair26";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[3][0]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[3][0]_i_1\ : label is "soft_lutpair33";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[3][1]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[3][1]_i_1\ : label is "soft_lutpair24";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[3][3]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[3][3]_i_1\ : label is "soft_lutpair31";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[3][4]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[3][4]_i_1\ : label is "soft_lutpair40";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[4][1]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[4][1]_i_1\ : label is "soft_lutpair30";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[4][3]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[4][3]_i_1\ : label is "soft_lutpair30";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[5][0]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[5][0]_i_1\ : label is "soft_lutpair32";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[5][2]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[5][2]_i_1\ : label is "soft_lutpair23";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[5][4]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[5][4]_i_1\ : label is "soft_lutpair38";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[5][6]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[5][6]_i_1\ : label is "soft_lutpair25";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[6][0]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[6][0]_i_1\ : label is "soft_lutpair40";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[6][1]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[6][1]_i_1\ : label is "soft_lutpair38";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[6][3]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[6][3]_i_1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \disp_aux_reg[6][3]_i_2\ : label is "soft_lutpair32";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[7][6]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[7][6]_i_1\ : label is "soft_lutpair31";
begin
  \current_state_reg[0]\ <= \^current_state_reg[0]\;
  \current_state_reg[1]_0\ <= \^current_state_reg[1]_0\;
  \current_state_reg[1]_1\ <= \^current_state_reg[1]_1\;
  \current_state_reg[1]_3\ <= \^current_state_reg[1]_3\;
  \current_state_reg[1]_8\ <= \^current_state_reg[1]_8\;
  \disp_aux[1]_7\ <= \^disp_aux[1]_7\;
  \disp_aux_reg[6][3]_i_2_0\(1 downto 0) <= \^disp_aux_reg[6][3]_i_2_0\(1 downto 0);
  \disp_aux_reg[6][3]_i_2_1\(1 downto 0) <= \^disp_aux_reg[6][3]_i_2_1\(1 downto 0);
  \disp_aux_reg[6][3]_i_2_2\ <= \^disp_aux_reg[6][3]_i_2_2\;
  \disp_aux_reg[6][3]_i_2_3\ <= \^disp_aux_reg[6][3]_i_2_3\;
  \disp_refresco[4]_1\(1 downto 0) <= \^disp_refresco[4]_1\(1 downto 0);
  \reg_reg[0]\ <= \^reg_reg[0]\;
  \reg_reg[2]\ <= \^reg_reg[2]\;
\caracter[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F100F100F1FFF100"
    )
        port map (
      I0 => \caracter[0]_i_2_n_0\,
      I1 => plusOp(1),
      I2 => \caracter[0]_i_4_n_0\,
      I3 => \^reg_reg[2]\,
      I4 => \caracter[0]_i_5_n_0\,
      I5 => \caracter[0]_i_6_n_0\,
      O => D(0)
    );
\caracter[0]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"10D0"
    )
        port map (
      I0 => \disp_refresco[3]_5\(0),
      I1 => \caracter_reg[0]_0\(1),
      I2 => \caracter_reg[0]_0\(0),
      I3 => \caracter[0]_i_5_0\,
      O => \caracter[0]_i_10_n_0\
    );
\caracter[0]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5F50CFCF5F50C0C0"
    )
        port map (
      I0 => \caracter[0]_i_14_n_0\,
      I1 => \^current_state_reg[1]_0\,
      I2 => \caracter[1]_i_4_0\,
      I3 => \caracter[0]_i_7_n_0\,
      I4 => \caracter[2]_i_3_0\,
      I5 => \caracter_reg[0]_2\,
      O => \caracter[0]_i_12_n_0\
    );
\caracter[0]_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \disp_refresco[6]_4\(0),
      I1 => \caracter_reg[0]_0\(1),
      I2 => \caracter_reg[0]_0\(0),
      O => \caracter[0]_i_14_n_0\
    );
\caracter[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FCAF0CAFFCA00CA0"
    )
        port map (
      I0 => \caracter[0]_i_7_n_0\,
      I1 => \caracter_reg[0]_2\,
      I2 => Q(0),
      I3 => Q(1),
      I4 => \caracter_reg[0]\,
      I5 => \caracter[0]_i_10_n_0\,
      O => \caracter[0]_i_2_n_0\
    );
\caracter[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0A0F00000C000000"
    )
        port map (
      I0 => \disp_refresco[7]_6\(6),
      I1 => \disp_refresco[6]_4\(0),
      I2 => \caracter_reg[0]_0\(1),
      I3 => \caracter_reg[0]_0\(0),
      I4 => plusOp(1),
      I5 => \caracter_reg[0]_3\,
      O => \caracter[0]_i_4_n_0\
    );
\caracter[0]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"53535353FFFF00F0"
    )
        port map (
      I0 => \caracter[0]_i_10_n_0\,
      I1 => \caracter_reg[0]\,
      I2 => \caracter_reg[3]\,
      I3 => \caracter[0]_i_12_n_0\,
      I4 => \caracter_reg[0]_1\,
      I5 => \caracter_reg[3]_0\,
      O => \caracter[0]_i_5_n_0\
    );
\caracter[0]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFDF00"
    )
        port map (
      I0 => \disp_refresco[6]_4\(0),
      I1 => \caracter_reg[0]_0\(1),
      I2 => \caracter_reg[0]_0\(0),
      I3 => \caracter_reg[2]_2\,
      I4 => CO(0),
      O => \caracter[0]_i_6_n_0\
    );
\caracter[0]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"CE"
    )
        port map (
      I0 => \caracter_reg[0]_0\(0),
      I1 => \caracter_reg[0]_0\(1),
      I2 => \disp_refresco[2]_2\(6),
      O => \caracter[0]_i_7_n_0\
    );
\caracter[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E0E0E0EF"
    )
        port map (
      I0 => \caracter[1]_i_2_n_0\,
      I1 => \caracter[1]_i_3_n_0\,
      I2 => \^reg_reg[2]\,
      I3 => CO(0),
      I4 => \caracter[1]_i_4_n_0\,
      O => D(1)
    );
\caracter[1]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"02A2"
    )
        port map (
      I0 => \caracter_reg[0]_0\(0),
      I1 => \disp_refresco[1]_3\(3),
      I2 => \caracter_reg[0]_0\(1),
      I3 => \caracter[6]_i_20_0\,
      O => \caracter[1]_i_10_n_0\
    );
\caracter[1]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF413CFFFF7D3C"
    )
        port map (
      I0 => \disp_refresco[6]_4\(1),
      I1 => Q(0),
      I2 => \caracter[6]_i_5_0\,
      I3 => \caracter_reg[0]_0\(0),
      I4 => \caracter_reg[0]_0\(1),
      I5 => \disp_refresco[3]_5\(3),
      O => \caracter[1]_i_11_n_0\
    );
\caracter[1]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AF0F30F0AF0F3FF0"
    )
        port map (
      I0 => \caracter[6]_i_20_0\,
      I1 => \^disp_refresco[4]_1\(0),
      I2 => \caracter[2]_i_3_0\,
      I3 => \caracter_reg[0]_0\(0),
      I4 => \caracter_reg[0]_0\(1),
      I5 => \^disp_refresco[4]_1\(1),
      O => \caracter[1]_i_12_n_0\
    );
\caracter[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"500003355FF00335"
    )
        port map (
      I0 => \caracter[1]_i_5_n_0\,
      I1 => \caracter[1]_i_6_n_0\,
      I2 => Q(1),
      I3 => Q(0),
      I4 => Q(2),
      I5 => \caracter[1]_i_7_n_0\,
      O => \caracter[1]_i_2_n_0\
    );
\caracter[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0A0A0800000A0800"
    )
        port map (
      I0 => \caracter_reg[1]_0\,
      I1 => \disp_refresco[3]_5\(3),
      I2 => \caracter_reg[0]_0\(1),
      I3 => \caracter_reg[0]_0\(0),
      I4 => \caracter_reg[1]_1\,
      I5 => \disp_refresco[6]_4\(1),
      O => \caracter[1]_i_3_n_0\
    );
\caracter[1]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \caracter[1]_i_7_n_0\,
      I1 => \caracter_reg[2]_2\,
      I2 => \caracter_reg[1]\,
      I3 => \caracter_reg[3]_0\,
      I4 => \caracter[1]_i_9_n_0\,
      O => \caracter[1]_i_4_n_0\
    );
\caracter[1]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88888888BB8BBBBB"
    )
        port map (
      I0 => \^current_state_reg[0]\,
      I1 => Q(0),
      I2 => \caracter[3]_i_3_0\,
      I3 => \caracter[1]_i_2_0\,
      I4 => \caracter[1]_i_2_1\,
      I5 => \^current_state_reg[1]_3\,
      O => \caracter[1]_i_5_n_0\
    );
\caracter[1]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AF0F30F0AF0F3FF0"
    )
        port map (
      I0 => \caracter[6]_i_20_0\,
      I1 => \^disp_refresco[4]_1\(0),
      I2 => Q(0),
      I3 => \caracter_reg[0]_0\(0),
      I4 => \caracter_reg[0]_0\(1),
      I5 => \^disp_refresco[4]_1\(1),
      O => \caracter[1]_i_6_n_0\
    );
\caracter[1]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"CE"
    )
        port map (
      I0 => \caracter_reg[0]_0\(0),
      I1 => \caracter_reg[0]_0\(1),
      I2 => \^disp_aux_reg[6][3]_i_2_1\(1),
      O => \caracter[1]_i_7_n_0\
    );
\caracter[1]_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CFAAC0AA"
    )
        port map (
      I0 => \caracter[1]_i_10_n_0\,
      I1 => \caracter[1]_i_11_n_0\,
      I2 => \caracter[1]_i_4_0\,
      I3 => \caracter_reg[3]\,
      I4 => \caracter[1]_i_12_n_0\,
      O => \caracter[1]_i_9_n_0\
    );
\caracter[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C0C0C0C5C5C5C0C5"
    )
        port map (
      I0 => CO(0),
      I1 => \caracter[2]_i_2_n_0\,
      I2 => \^reg_reg[2]\,
      I3 => \caracter_reg[2]_5\,
      I4 => \caracter_reg[2]_2\,
      I5 => \caracter[2]_i_4_n_0\,
      O => D(2)
    );
\caracter[2]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"03F35555FFFFFFFF"
    )
        port map (
      I0 => \caracter[2]_i_3\,
      I1 => \caracter[2]_i_17_n_0\,
      I2 => \caracter[2]_i_3_0\,
      I3 => \caracter[2]_i_18_n_0\,
      I4 => \caracter[1]_i_4_0\,
      I5 => \caracter_reg[3]\,
      O => \reg_reg[0]_0\
    );
\caracter[2]_i_17\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"CE"
    )
        port map (
      I0 => \caracter_reg[0]_0\(0),
      I1 => \caracter_reg[0]_0\(1),
      I2 => \disp_refresco[6]_4\(0),
      O => \caracter[2]_i_17_n_0\
    );
\caracter[2]_i_18\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_0\(0),
      I1 => \caracter_reg[0]_0\(1),
      I2 => \^disp_aux_reg[6][3]_i_2_2\,
      O => \caracter[2]_i_18_n_0\
    );
\caracter[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8080F0F000FFF0F0"
    )
        port map (
      I0 => \^disp_aux_reg[6][3]_i_2_0\(1),
      I1 => \caracter_reg[2]_3\,
      I2 => \caracter_reg[2]_4\,
      I3 => \caracter[2]_i_7_n_0\,
      I4 => plusOp(1),
      I5 => plusOp(0),
      O => \caracter[2]_i_2_n_0\
    );
\caracter[2]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"DF"
    )
        port map (
      I0 => \^disp_aux_reg[6][3]_i_2_0\(1),
      I1 => \caracter_reg[0]_0\(1),
      I2 => \caracter_reg[0]_0\(0),
      O => \caracter[2]_i_4_n_0\
    );
\caracter[2]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F340F370"
    )
        port map (
      I0 => \^disp_aux_reg[6][3]_i_2_2\,
      I1 => \caracter_reg[1]_1\,
      I2 => \caracter_reg[0]_0\(0),
      I3 => \caracter_reg[0]_0\(1),
      I4 => \disp_refresco[6]_4\(0),
      O => \caracter[2]_i_7_n_0\
    );
\caracter[2]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1FDF"
    )
        port map (
      I0 => \disp_refresco[2]_2\(6),
      I1 => \caracter_reg[0]_0\(1),
      I2 => \caracter_reg[0]_0\(0),
      I3 => \caracter[6]_i_57\,
      O => \current_state_reg[1]_4\
    );
\caracter[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C5C5C0C5C0C0C0C5"
    )
        port map (
      I0 => CO(0),
      I1 => \caracter[3]_i_2_n_0\,
      I2 => \^reg_reg[2]\,
      I3 => \caracter[3]_i_3_n_0\,
      I4 => \caracter_reg[2]_2\,
      I5 => \caracter[3]_i_4_n_0\,
      O => D(3)
    );
\caracter[3]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"C5FF"
    )
        port map (
      I0 => \disp_refresco[3]_5\(3),
      I1 => \caracter[3]_i_3_0\,
      I2 => \caracter_reg[0]_0\(1),
      I3 => \caracter_reg[0]_0\(0),
      O => \caracter[3]_i_10_n_0\
    );
\caracter[3]_i_13\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => \caracter_reg[0]_0\(1),
      I1 => \disp_refresco[1]_3\(3),
      I2 => \caracter_reg[0]_0\(0),
      O => \caracter[3]_i_13_n_0\
    );
\caracter[3]_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"9B"
    )
        port map (
      I0 => \caracter_reg[0]_0\(0),
      I1 => \caracter_reg[0]_0\(1),
      I2 => \^disp_refresco[4]_1\(1),
      O => \caracter[3]_i_14_n_0\
    );
\caracter[3]_i_18\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000BE8200000000"
    )
        port map (
      I0 => \disp_refresco[6]_4\(3),
      I1 => Q(0),
      I2 => \caracter[6]_i_5_0\,
      I3 => \disp_refresco[3]_5\(3),
      I4 => \caracter_reg[0]_0\(1),
      I5 => \caracter_reg[0]_0\(0),
      O => \reg_reg[0]_1\
    );
\caracter[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \caracter[3]_i_5_n_0\,
      I1 => \caracter[3]_i_6_n_0\,
      I2 => \caracter_reg[1]_0\,
      I3 => \caracter[3]_i_4_n_0\,
      I4 => \caracter_reg[1]_1\,
      I5 => \caracter[3]_i_9_n_0\,
      O => \caracter[3]_i_2_n_0\
    );
\caracter[3]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0AFC0C0A0AFCFCF"
    )
        port map (
      I0 => \caracter[3]_i_10_n_0\,
      I1 => \caracter_reg[3]_1\,
      I2 => \caracter_reg[3]_0\,
      I3 => \caracter_reg[3]_2\,
      I4 => \caracter_reg[3]\,
      I5 => \caracter[3]_i_13_n_0\,
      O => \caracter[3]_i_3_n_0\
    );
\caracter[3]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \^disp_refresco[4]_1\(1),
      I1 => \caracter_reg[0]_0\(1),
      I2 => \caracter_reg[0]_0\(0),
      O => \caracter[3]_i_4_n_0\
    );
\caracter[3]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \disp_refresco[6]_4\(3),
      I1 => \caracter_reg[0]_0\(1),
      I2 => \caracter_reg[0]_0\(0),
      O => \caracter[3]_i_5_n_0\
    );
\caracter[3]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \disp_refresco[3]_5\(3),
      I1 => \caracter_reg[0]_0\(1),
      I2 => \caracter_reg[0]_0\(0),
      O => \caracter[3]_i_6_n_0\
    );
\caracter[3]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0F550F55DD00DDFF"
    )
        port map (
      I0 => \caracter[3]_i_14_n_0\,
      I1 => \caracter[3]_i_2_0\,
      I2 => \caracter_reg[3]_1\,
      I3 => Q(0),
      I4 => \caracter[3]_i_10_n_0\,
      I5 => Q(1),
      O => \caracter[3]_i_9_n_0\
    );
\caracter[4]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FD5AF85A00000000"
    )
        port map (
      I0 => \caracter_reg[3]\,
      I1 => \disp_refresco[3]_5\(4),
      I2 => \caracter_reg[0]_0\(1),
      I3 => \caracter_reg[0]_0\(0),
      I4 => \disp_refresco[2]_2\(4),
      I5 => \caracter_reg[3]_0\,
      O => \current_state_reg[1]_7\
    );
\caracter[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0000AEAAFF00AEA"
    )
        port map (
      I0 => \caracter[4]_i_5_n_0\,
      I1 => \^current_state_reg[1]_8\,
      I2 => Q(1),
      I3 => Q(0),
      I4 => Q(2),
      I5 => \caracter_reg[4]\,
      O => \reg_reg[1]_0\
    );
\caracter[4]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F8000AAFF80000A5"
    )
        port map (
      I0 => \caracter_reg[0]_0\(0),
      I1 => \disp_refresco[2]_2\(4),
      I2 => \caracter_reg[0]_0\(1),
      I3 => Q(0),
      I4 => Q(1),
      I5 => \disp_refresco[3]_5\(4),
      O => \caracter[4]_i_5_n_0\
    );
\caracter[4]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"23E3"
    )
        port map (
      I0 => \^disp_aux_reg[6][3]_i_2_0\(1),
      I1 => \caracter_reg[0]_0\(1),
      I2 => \caracter_reg[0]_0\(0),
      I3 => \caracter[6]_i_20_0\,
      O => \^current_state_reg[1]_8\
    );
\caracter[4]_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \disp_refresco[3]_5\(4),
      I1 => \caracter_reg[0]_0\(1),
      I2 => \caracter_reg[0]_0\(0),
      O => \current_state_reg[1]_6\
    );
\caracter[6]_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => \caracter_reg[0]_0\(1),
      I1 => \disp_refresco[5]_0\(6),
      I2 => \caracter_reg[0]_0\(0),
      O => \^current_state_reg[1]_1\
    );
\caracter[6]_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DD00DD000000CF00"
    )
        port map (
      I0 => \disp_refresco[2]_2\(6),
      I1 => \caracter_reg[0]_0\(1),
      I2 => \^disp_aux_reg[6][3]_i_2_3\,
      I3 => \caracter_reg[0]_0\(0),
      I4 => Q(1),
      I5 => Q(0),
      O => \current_state_reg[1]_2\
    );
\caracter[6]_i_17\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"02A2FFFF02A20000"
    )
        port map (
      I0 => \caracter_reg[0]_0\(0),
      I1 => \disp_refresco[1]_3\(3),
      I2 => \caracter_reg[0]_0\(1),
      I3 => \caracter[6]_i_20_0\,
      I4 => Q(0),
      I5 => \^current_state_reg[0]\,
      O => \caracter[6]_i_17_n_0\
    );
\caracter[6]_i_20\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0CAFFCAF0CA0FCA0"
    )
        port map (
      I0 => \caracter[3]_i_10_n_0\,
      I1 => \caracter[6]_i_35_n_0\,
      I2 => Q(0),
      I3 => Q(1),
      I4 => \caracter[3]_i_13_n_0\,
      I5 => \caracter_reg[3]_1\,
      O => \caracter[6]_i_20_n_0\
    );
\caracter[6]_i_22\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => \caracter_reg[0]_0\(1),
      I1 => \^disp_aux_reg[6][3]_i_2_3\,
      I2 => \caracter_reg[0]_0\(0),
      O => \caracter[6]_i_22_n_0\
    );
\caracter[6]_i_23\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \caracter_reg[0]_0\(0),
      I1 => \caracter_reg[0]_0\(1),
      I2 => \disp_refresco[2]_2\(6),
      O => \caracter[6]_i_23_n_0\
    );
\caracter[6]_i_25\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"322C0220FEEFCEE3"
    )
        port map (
      I0 => \^current_state_reg[1]_0\,
      I1 => Q(1),
      I2 => \caracter[6]_i_5_0\,
      I3 => Q(0),
      I4 => \caracter[6]_i_23_n_0\,
      I5 => \^current_state_reg[1]_1\,
      O => \caracter[6]_i_25_n_0\
    );
\caracter[6]_i_27\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EF"
    )
        port map (
      I0 => \^disp_aux_reg[6][3]_i_2_3\,
      I1 => \caracter_reg[0]_0\(1),
      I2 => \caracter_reg[0]_0\(0),
      O => \caracter[6]_i_27_n_0\
    );
\caracter[6]_i_28\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFBAAAAAAAA"
    )
        port map (
      I0 => \caracter_reg[6]_i_41_n_0\,
      I1 => \caracter[6]_i_42_n_0\,
      I2 => \caracter[6]_i_43_n_0\,
      I3 => \caracter[6]_i_44_n_0\,
      I4 => \caracter[6]_i_45_n_0\,
      I5 => \caracter[6]_i_46_n_0\,
      O => \^reg_reg[0]\
    );
\caracter[6]_i_29\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A022"
    )
        port map (
      I0 => \caracter_reg[0]_0\(0),
      I1 => \disp_refresco[1]_3\(3),
      I2 => \caracter[1]_i_8\,
      I3 => \caracter_reg[0]_0\(1),
      O => \^current_state_reg[0]\
    );
\caracter[6]_i_33\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"45"
    )
        port map (
      I0 => \caracter_reg[0]_0\(1),
      I1 => \disp_refresco[3]_5\(1),
      I2 => \caracter_reg[0]_0\(0),
      O => \^current_state_reg[1]_3\
    );
\caracter[6]_i_35\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"C077"
    )
        port map (
      I0 => \^disp_refresco[4]_1\(1),
      I1 => \caracter_reg[0]_0\(0),
      I2 => \caracter[6]_i_20_0\,
      I3 => \caracter_reg[0]_0\(1),
      O => \caracter[6]_i_35_n_0\
    );
\caracter[6]_i_37\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEFEEEEEEEFFF"
    )
        port map (
      I0 => \^reg_reg[0]\,
      I1 => \caracter[6]_i_58_n_0\,
      I2 => \caracter[0]_i_7_n_0\,
      I3 => Q(1),
      I4 => Q(0),
      I5 => \caracter_reg[0]\,
      O => \reg_reg[1]\
    );
\caracter[6]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAABFBAAAAAAAA"
    )
        port map (
      I0 => \caracter_reg[2]\,
      I1 => \caracter[6]_i_17_n_0\,
      I2 => plusOp(0),
      I3 => \caracter_reg[2]_0\,
      I4 => \caracter[6]_i_20_n_0\,
      I5 => \caracter_reg[2]_1\,
      O => \^reg_reg[2]\
    );
\caracter[6]_i_40\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \caracter_reg[0]_0\(1),
      I1 => \caracter_reg[0]_0\(0),
      I2 => \disp_refresco[3]_5\(0),
      O => \current_state_reg[1]_5\
    );
\caracter[6]_i_42\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8BBB8BBBCFFF0333"
    )
        port map (
      I0 => \caracter_reg[0]_2\,
      I1 => Q(0),
      I2 => \caracter_reg[2]_3\,
      I3 => \disp_refresco[6]_4\(0),
      I4 => \^current_state_reg[1]_0\,
      I5 => Q(1),
      O => \caracter[6]_i_42_n_0\
    );
\caracter[6]_i_43\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00F3FFFF33BBFFFF"
    )
        port map (
      I0 => \disp_refresco[5]_0\(6),
      I1 => \caracter_reg[0]_0\(0),
      I2 => \disp_refresco[7]_6\(6),
      I3 => \caracter_reg[0]_0\(1),
      I4 => plusOp(1),
      I5 => plusOp(0),
      O => \caracter[6]_i_43_n_0\
    );
\caracter[6]_i_44\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBBABBBBBBBAAABB"
    )
        port map (
      I0 => \caracter_reg[6]_i_64_n_0\,
      I1 => \caracter[6]_i_28_0\,
      I2 => \caracter[2]_i_17_n_0\,
      I3 => Q(1),
      I4 => Q(0),
      I5 => \caracter[2]_i_4_n_0\,
      O => \caracter[6]_i_44_n_0\
    );
\caracter[6]_i_45\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF3F77FF003F7700"
    )
        port map (
      I0 => \disp_refresco[3]_5\(3),
      I1 => \caracter_reg[2]_3\,
      I2 => \^disp_refresco[4]_1\(1),
      I3 => Q(0),
      I4 => Q(1),
      I5 => \caracter[6]_i_66_n_0\,
      O => \caracter[6]_i_45_n_0\
    );
\caracter[6]_i_46\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0FFFFFFFFFAF3F3"
    )
        port map (
      I0 => \caracter[6]_i_22_n_0\,
      I1 => \caracter[6]_i_23_n_0\,
      I2 => \caracter[6]_i_58_n_0\,
      I3 => Q(1),
      I4 => Q(0),
      I5 => Q(2),
      O => \caracter[6]_i_46_n_0\
    );
\caracter[6]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5F50C0C05F50CFCF"
    )
        port map (
      I0 => \caracter[6]_i_22_n_0\,
      I1 => \caracter[6]_i_23_n_0\,
      I2 => \caracter_reg[3]_0\,
      I3 => \caracter[6]_i_25_n_0\,
      I4 => \caracter_reg[3]\,
      I5 => \caracter[6]_i_27_n_0\,
      O => \current_state_reg[1]\
    );
\caracter[6]_i_56\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"58"
    )
        port map (
      I0 => \caracter_reg[0]_0\(0),
      I1 => \^disp_aux_reg[6][3]_i_2_3\,
      I2 => \caracter_reg[0]_0\(1),
      O => \current_state_reg[0]_0\
    );
\caracter[6]_i_58\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FD000000"
    )
        port map (
      I0 => \caracter_reg[0]_0\(0),
      I1 => \caracter_reg[0]_0\(1),
      I2 => \^disp_aux_reg[6][3]_i_2_3\,
      I3 => Q(0),
      I4 => Q(1),
      O => \caracter[6]_i_58_n_0\
    );
\caracter[6]_i_62\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0F030F032B0C280C"
    )
        port map (
      I0 => \disp_refresco[3]_5\(4),
      I1 => Q(0),
      I2 => Q(1),
      I3 => \caracter_reg[0]_0\(0),
      I4 => \disp_refresco[2]_2\(4),
      I5 => \caracter_reg[0]_0\(1),
      O => \caracter[6]_i_62_n_0\
    );
\caracter[6]_i_66\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF4733FF"
    )
        port map (
      I0 => \^disp_refresco[4]_1\(1),
      I1 => Q(0),
      I2 => \disp_refresco[6]_4\(3),
      I3 => \caracter_reg[0]_0\(1),
      I4 => \caracter_reg[0]_0\(0),
      O => \caracter[6]_i_66_n_0\
    );
\caracter[6]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => \caracter_reg[0]_0\(1),
      I1 => \disp_refresco[7]_6\(6),
      I2 => \caracter_reg[0]_0\(0),
      O => \^current_state_reg[1]_0\
    );
\caracter[6]_i_77\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F4F7F0F0"
    )
        port map (
      I0 => \^disp_refresco[4]_1\(1),
      I1 => Q(0),
      I2 => \caracter_reg[0]_0\(1),
      I3 => \disp_refresco[6]_4\(1),
      I4 => \caracter_reg[0]_0\(0),
      O => \caracter[6]_i_77_n_0\
    );
\caracter[6]_i_78\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF4CFF7C"
    )
        port map (
      I0 => \disp_refresco[3]_5\(3),
      I1 => Q(0),
      I2 => \caracter_reg[0]_0\(0),
      I3 => \caracter_reg[0]_0\(1),
      I4 => \^disp_aux_reg[6][3]_i_2_1\(1),
      O => \caracter[6]_i_78_n_0\
    );
\caracter_reg[6]_i_41\: unisim.vcomponents.MUXF7
     port map (
      I0 => \caracter[6]_i_62_n_0\,
      I1 => \caracter[6]_i_28_1\,
      O => \caracter_reg[6]_i_41_n_0\,
      S => plusOp(1)
    );
\caracter_reg[6]_i_64\: unisim.vcomponents.MUXF7
     port map (
      I0 => \caracter[6]_i_77_n_0\,
      I1 => \caracter[6]_i_78_n_0\,
      O => \caracter_reg[6]_i_64_n_0\,
      S => plusOp(0)
    );
\disp_aux_reg[1][3]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[1][3]_i_1_n_0\,
      G => \^disp_aux[1]_7\,
      GE => '1',
      Q => \disp_refresco[1]_3\(3)
    );
\disp_aux_reg[1][3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEEC"
    )
        port map (
      I0 => ref_option_IBUF(3),
      I1 => ref_option_IBUF(1),
      I2 => ref_option_IBUF(2),
      I3 => ref_option_IBUF(0),
      O => \disp_aux_reg[1][3]_i_1_n_0\
    );
\disp_aux_reg[1][6]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[1][6]_i_1_n_0\,
      G => \^disp_aux[1]_7\,
      GE => '1',
      Q => \^disp_aux_reg[6][3]_i_2_3\
    );
\disp_aux_reg[1][6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0107"
    )
        port map (
      I0 => ref_option_IBUF(0),
      I1 => ref_option_IBUF(2),
      I2 => ref_option_IBUF(1),
      I3 => ref_option_IBUF(3),
      O => \disp_aux_reg[1][6]_i_1_n_0\
    );
\disp_aux_reg[2][0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[2][0]_i_1_n_0\,
      G => \^disp_aux[1]_7\,
      GE => '1',
      Q => \^disp_aux_reg[6][3]_i_2_1\(0)
    );
\disp_aux_reg[2][0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0111"
    )
        port map (
      I0 => ref_option_IBUF(1),
      I1 => ref_option_IBUF(3),
      I2 => ref_option_IBUF(0),
      I3 => ref_option_IBUF(2),
      O => \disp_aux_reg[2][0]_i_1_n_0\
    );
\disp_aux_reg[2][3]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[2][3]_i_1_n_0\,
      G => \^disp_aux[1]_7\,
      GE => '1',
      Q => \^disp_aux_reg[6][3]_i_2_1\(1)
    );
\disp_aux_reg[2][3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFF8"
    )
        port map (
      I0 => ref_option_IBUF(2),
      I1 => ref_option_IBUF(0),
      I2 => ref_option_IBUF(3),
      I3 => ref_option_IBUF(1),
      O => \disp_aux_reg[2][3]_i_1_n_0\
    );
\disp_aux_reg[2][4]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[2][4]_i_1_n_0\,
      G => \^disp_aux[1]_7\,
      GE => '1',
      Q => \disp_refresco[2]_2\(4)
    );
\disp_aux_reg[2][4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => ref_option_IBUF(3),
      I1 => ref_option_IBUF(2),
      I2 => ref_option_IBUF(1),
      O => \disp_aux_reg[2][4]_i_1_n_0\
    );
\disp_aux_reg[2][6]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[2][6]_i_1_n_0\,
      G => \^disp_aux[1]_7\,
      GE => '1',
      Q => \disp_refresco[2]_2\(6)
    );
\disp_aux_reg[2][6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0117"
    )
        port map (
      I0 => ref_option_IBUF(1),
      I1 => ref_option_IBUF(0),
      I2 => ref_option_IBUF(3),
      I3 => ref_option_IBUF(2),
      O => \disp_aux_reg[2][6]_i_1_n_0\
    );
\disp_aux_reg[3][0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[3][0]_i_1_n_0\,
      G => \^disp_aux[1]_7\,
      GE => '1',
      Q => \disp_refresco[3]_5\(0)
    );
\disp_aux_reg[3][0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0103"
    )
        port map (
      I0 => ref_option_IBUF(3),
      I1 => ref_option_IBUF(2),
      I2 => ref_option_IBUF(1),
      I3 => ref_option_IBUF(0),
      O => \disp_aux_reg[3][0]_i_1_n_0\
    );
\disp_aux_reg[3][1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[3][1]_i_1_n_0\,
      G => \^disp_aux[1]_7\,
      GE => '1',
      Q => \disp_refresco[3]_5\(1)
    );
\disp_aux_reg[3][1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEEE"
    )
        port map (
      I0 => ref_option_IBUF(3),
      I1 => ref_option_IBUF(0),
      I2 => ref_option_IBUF(2),
      I3 => ref_option_IBUF(1),
      O => \disp_aux_reg[3][1]_i_1_n_0\
    );
\disp_aux_reg[3][3]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[3][3]_i_1_n_0\,
      G => \^disp_aux[1]_7\,
      GE => '1',
      Q => \disp_refresco[3]_5\(3)
    );
\disp_aux_reg[3][3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEF8"
    )
        port map (
      I0 => ref_option_IBUF(1),
      I1 => ref_option_IBUF(0),
      I2 => ref_option_IBUF(3),
      I3 => ref_option_IBUF(2),
      O => \disp_aux_reg[3][3]_i_1_n_0\
    );
\disp_aux_reg[3][4]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[3][4]_i_1_n_0\,
      G => \^disp_aux[1]_7\,
      GE => '1',
      Q => \disp_refresco[3]_5\(4)
    );
\disp_aux_reg[3][4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => ref_option_IBUF(1),
      I1 => ref_option_IBUF(3),
      I2 => ref_option_IBUF(0),
      O => \disp_aux_reg[3][4]_i_1_n_0\
    );
\disp_aux_reg[4][1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[4][1]_i_1_n_0\,
      G => \^disp_aux[1]_7\,
      GE => '1',
      Q => \^disp_refresco[4]_1\(0)
    );
\disp_aux_reg[4][1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFF8"
    )
        port map (
      I0 => ref_option_IBUF(1),
      I1 => ref_option_IBUF(0),
      I2 => ref_option_IBUF(3),
      I3 => ref_option_IBUF(2),
      O => \disp_aux_reg[4][1]_i_1_n_0\
    );
\disp_aux_reg[4][3]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[4][3]_i_1_n_0\,
      G => \^disp_aux[1]_7\,
      GE => '1',
      Q => \^disp_refresco[4]_1\(1)
    );
\disp_aux_reg[4][3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEE8"
    )
        port map (
      I0 => ref_option_IBUF(2),
      I1 => ref_option_IBUF(3),
      I2 => ref_option_IBUF(0),
      I3 => ref_option_IBUF(1),
      O => \disp_aux_reg[4][3]_i_1_n_0\
    );
\disp_aux_reg[5][0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[5][0]_i_1_n_0\,
      G => \^disp_aux[1]_7\,
      GE => '1',
      Q => \^disp_aux_reg[6][3]_i_2_0\(0)
    );
\disp_aux_reg[5][0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0013"
    )
        port map (
      I0 => ref_option_IBUF(2),
      I1 => ref_option_IBUF(0),
      I2 => ref_option_IBUF(3),
      I3 => ref_option_IBUF(1),
      O => \disp_aux_reg[5][0]_i_1_n_0\
    );
\disp_aux_reg[5][2]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[5][2]_i_1_n_0\,
      G => \^disp_aux[1]_7\,
      GE => '1',
      Q => \^disp_aux_reg[6][3]_i_2_2\
    );
\disp_aux_reg[5][2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0007"
    )
        port map (
      I0 => ref_option_IBUF(1),
      I1 => ref_option_IBUF(2),
      I2 => ref_option_IBUF(0),
      I3 => ref_option_IBUF(3),
      O => \disp_aux_reg[5][2]_i_1_n_0\
    );
\disp_aux_reg[5][4]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[5][4]_i_1_n_0\,
      G => \^disp_aux[1]_7\,
      GE => '1',
      Q => \^disp_aux_reg[6][3]_i_2_0\(1)
    );
\disp_aux_reg[5][4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => ref_option_IBUF(2),
      I1 => ref_option_IBUF(3),
      I2 => ref_option_IBUF(0),
      O => \disp_aux_reg[5][4]_i_1_n_0\
    );
\disp_aux_reg[5][6]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[5][6]_i_1_n_0\,
      G => \^disp_aux[1]_7\,
      GE => '1',
      Q => \disp_refresco[5]_0\(6)
    );
\disp_aux_reg[5][6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0107"
    )
        port map (
      I0 => ref_option_IBUF(1),
      I1 => ref_option_IBUF(3),
      I2 => ref_option_IBUF(0),
      I3 => ref_option_IBUF(2),
      O => \disp_aux_reg[5][6]_i_1_n_0\
    );
\disp_aux_reg[6][0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[6][0]_i_1_n_0\,
      G => \^disp_aux[1]_7\,
      GE => '1',
      Q => \disp_refresco[6]_4\(0)
    );
\disp_aux_reg[6][0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => ref_option_IBUF(0),
      I1 => ref_option_IBUF(2),
      I2 => ref_option_IBUF(1),
      O => \disp_aux_reg[6][0]_i_1_n_0\
    );
\disp_aux_reg[6][1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[6][1]_i_1_n_0\,
      G => \^disp_aux[1]_7\,
      GE => '1',
      Q => \disp_refresco[6]_4\(1)
    );
\disp_aux_reg[6][1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => ref_option_IBUF(1),
      I1 => ref_option_IBUF(2),
      I2 => ref_option_IBUF(3),
      O => \disp_aux_reg[6][1]_i_1_n_0\
    );
\disp_aux_reg[6][3]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[6][3]_i_1_n_0\,
      G => \^disp_aux[1]_7\,
      GE => '1',
      Q => \disp_refresco[6]_4\(3)
    );
\disp_aux_reg[6][3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEFC"
    )
        port map (
      I0 => ref_option_IBUF(0),
      I1 => ref_option_IBUF(1),
      I2 => ref_option_IBUF(2),
      I3 => ref_option_IBUF(3),
      O => \disp_aux_reg[6][3]_i_1_n_0\
    );
\disp_aux_reg[6][3]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => ref_option_IBUF(2),
      I1 => ref_option_IBUF(1),
      I2 => ref_option_IBUF(3),
      I3 => ref_option_IBUF(0),
      O => \^disp_aux[1]_7\
    );
\disp_aux_reg[7][6]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[7][6]_i_1_n_0\,
      G => \^disp_aux[1]_7\,
      GE => '1',
      Q => \disp_refresco[7]_6\(6)
    );
\disp_aux_reg[7][6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0013"
    )
        port map (
      I0 => ref_option_IBUF(1),
      I1 => ref_option_IBUF(0),
      I2 => ref_option_IBUF(3),
      I3 => ref_option_IBUF(2),
      O => \disp_aux_reg[7][6]_i_1_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity int_to_string is
  port (
    \current_state_reg[1]\ : out STD_LOGIC;
    \caracter[6]_i_61_0\ : out STD_LOGIC;
    \current_state_reg[1]_0\ : out STD_LOGIC;
    \current_state_reg[1]_1\ : out STD_LOGIC;
    \caracter[6]_i_52_0\ : out STD_LOGIC;
    \caracter[6]_i_55_0\ : out STD_LOGIC;
    \reg_reg[0]\ : out STD_LOGIC;
    \reg_reg[0]_0\ : out STD_LOGIC;
    \total_string_reg[2]\ : out STD_LOGIC;
    \total_string_reg[1]\ : out STD_LOGIC;
    \current_state_reg[0]\ : out STD_LOGIC;
    \total_string_reg[0]\ : out STD_LOGIC;
    \total_string_reg[0]_0\ : out STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 7 downto 0 );
    \caracter[6]_i_4\ : in STD_LOGIC;
    \caracter[6]_i_21_0\ : in STD_LOGIC;
    \caracter[6]_i_4_0\ : in STD_LOGIC;
    \caracter[6]_i_4_1\ : in STD_LOGIC;
    \caracter_reg[2]\ : in STD_LOGIC;
    \caracter_reg[2]_0\ : in STD_LOGIC;
    \caracter_reg[2]_1\ : in STD_LOGIC;
    \caracter_reg[2]_2\ : in STD_LOGIC;
    \caracter_reg[2]_3\ : in STD_LOGIC;
    \caracter[1]_i_4\ : in STD_LOGIC;
    \caracter[1]_i_4_0\ : in STD_LOGIC;
    \caracter[2]_i_2\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \caracter[2]_i_2_0\ : in STD_LOGIC;
    \caracter[2]_i_2_1\ : in STD_LOGIC;
    \caracter[0]_i_2\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \caracter[6]_i_21_1\ : in STD_LOGIC;
    \caracter[6]_i_21_2\ : in STD_LOGIC;
    \caracter[6]_i_4_2\ : in STD_LOGIC;
    \caracter[2]_i_3_0\ : in STD_LOGIC;
    \disp_refresco[2]_2\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \caracter[6]_i_54_0\ : in STD_LOGIC;
    \disp_refresco[5]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end int_to_string;

architecture STRUCTURE of int_to_string is
  signal \caracter[2]_i_14_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_19_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_8_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_16_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_36_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_48_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_49_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_50_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_51_n_0\ : STD_LOGIC;
  signal \^caracter[6]_i_52_0\ : STD_LOGIC;
  signal \caracter[6]_i_52_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_53_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_54_n_0\ : STD_LOGIC;
  signal \^caracter[6]_i_55_0\ : STD_LOGIC;
  signal \caracter[6]_i_55_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_59_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_60_n_0\ : STD_LOGIC;
  signal \^caracter[6]_i_61_0\ : STD_LOGIC;
  signal \caracter[6]_i_61_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_68_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_69_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_70_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_71_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_72_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_73_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_74_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_75_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_76_n_0\ : STD_LOGIC;
  signal cent : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \euros1_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \euros1_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \euros1_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \euros1_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \euros1_carry__0_n_1\ : STD_LOGIC;
  signal \euros1_carry__0_n_2\ : STD_LOGIC;
  signal \euros1_carry__0_n_3\ : STD_LOGIC;
  signal \euros1_carry__0_n_4\ : STD_LOGIC;
  signal \euros1_carry__0_n_5\ : STD_LOGIC;
  signal \euros1_carry__0_n_6\ : STD_LOGIC;
  signal \euros1_carry__0_n_7\ : STD_LOGIC;
  signal euros1_carry_i_1_n_0 : STD_LOGIC;
  signal euros1_carry_i_2_n_0 : STD_LOGIC;
  signal euros1_carry_i_3_n_0 : STD_LOGIC;
  signal euros1_carry_i_4_n_0 : STD_LOGIC;
  signal euros1_carry_n_0 : STD_LOGIC;
  signal euros1_carry_n_1 : STD_LOGIC;
  signal euros1_carry_n_2 : STD_LOGIC;
  signal euros1_carry_n_3 : STD_LOGIC;
  signal euros1_carry_n_4 : STD_LOGIC;
  signal euros1_carry_n_5 : STD_LOGIC;
  signal \i__carry__0_i_4_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_5_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_6_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_7_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_8_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_9_n_0\ : STD_LOGIC;
  signal \i__carry_i_10_n_0\ : STD_LOGIC;
  signal \i__carry_i_11_n_0\ : STD_LOGIC;
  signal \i__carry_i_3_n_0\ : STD_LOGIC;
  signal \i__carry_i_4_n_0\ : STD_LOGIC;
  signal \i__carry_i_5_n_0\ : STD_LOGIC;
  signal \i__carry_i_6_n_0\ : STD_LOGIC;
  signal \i__carry_i_7_n_0\ : STD_LOGIC;
  signal \i__carry_i_8_n_0\ : STD_LOGIC;
  signal p_1_in : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \string_cent_decenas[1]5\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \string_cent_decenas[1]5_inferred__0/i__carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i__carry__0_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i__carry__0_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i__carry__0_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i__carry__0_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i__carry__0_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i__carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i__carry_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i__carry_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i__carry_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i__carry_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i__carry_n_5\ : STD_LOGIC;
  signal \^total_string_reg[1]\ : STD_LOGIC;
  signal \^total_string_reg[2]\ : STD_LOGIC;
  signal \NLW_euros1_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i__carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \caracter[3]_i_19\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \caracter[6]_i_67\ : label is "soft_lutpair14";
begin
  \caracter[6]_i_52_0\ <= \^caracter[6]_i_52_0\;
  \caracter[6]_i_55_0\ <= \^caracter[6]_i_55_0\;
  \caracter[6]_i_61_0\ <= \^caracter[6]_i_61_0\;
  \total_string_reg[1]\ <= \^total_string_reg[1]\;
  \total_string_reg[2]\ <= \^total_string_reg[2]\;
\caracter[0]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F9F900FF0000FFFF"
    )
        port map (
      I0 => Q(0),
      I1 => p_1_in(0),
      I2 => \caracter[6]_i_54_0\,
      I3 => \disp_refresco[5]_0\(0),
      I4 => \caracter[0]_i_2\(1),
      I5 => \caracter[0]_i_2\(0),
      O => \total_string_reg[0]_0\
    );
\caracter[0]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"47FF47FF0000FF00"
    )
        port map (
      I0 => Q(0),
      I1 => \caracter[6]_i_54_0\,
      I2 => p_1_in(0),
      I3 => \caracter[0]_i_2\(0),
      I4 => \disp_refresco[2]_2\(0),
      I5 => \caracter[0]_i_2\(1),
      O => \total_string_reg[0]\
    );
\caracter[1]_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00DFFFFF00DF0000"
    )
        port map (
      I0 => \^caracter[6]_i_52_0\,
      I1 => \^caracter[6]_i_55_0\,
      I2 => \caracter[6]_i_4_0\,
      I3 => \caracter[1]_i_4\,
      I4 => \caracter_reg[2]_1\,
      I5 => \caracter[1]_i_4_0\,
      O => \current_state_reg[1]_1\
    );
\caracter[2]_i_14\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F4BFB42F"
    )
        port map (
      I0 => \caracter[6]_i_53_n_0\,
      I1 => \caracter[6]_i_50_n_0\,
      I2 => \caracter[6]_i_49_n_0\,
      I3 => \caracter[6]_i_48_n_0\,
      I4 => \caracter[2]_i_19_n_0\,
      O => \caracter[2]_i_14_n_0\
    );
\caracter[2]_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9166166889169166"
    )
        port map (
      I0 => \i__carry_i_11_n_0\,
      I1 => \i__carry__0_i_7_n_0\,
      I2 => \i__carry__0_i_8_n_0\,
      I3 => cent(4),
      I4 => cent(3),
      I5 => cent(1),
      O => \^total_string_reg[2]\
    );
\caracter[2]_i_19\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B2244DDB4DDBB224"
    )
        port map (
      I0 => \caracter[6]_i_70_n_0\,
      I1 => \^total_string_reg[2]\,
      I2 => \string_cent_decenas[1]5_inferred__0/i__carry_n_5\,
      I3 => \caracter[6]_i_69_n_0\,
      I4 => \string_cent_decenas[1]5_inferred__0/i__carry_n_4\,
      I5 => \caracter[3]_i_16_n_0\,
      O => \caracter[2]_i_19_n_0\
    );
\caracter[2]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0CFA0C0AFCFAFCF"
    )
        port map (
      I0 => \caracter[2]_i_8_n_0\,
      I1 => \caracter_reg[2]\,
      I2 => \caracter_reg[2]_0\,
      I3 => \caracter_reg[2]_1\,
      I4 => \caracter_reg[2]_2\,
      I5 => \caracter_reg[2]_3\,
      O => \current_state_reg[1]_0\
    );
\caracter[2]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF01C10DCD"
    )
        port map (
      I0 => \caracter[2]_i_8_n_0\,
      I1 => \caracter[2]_i_2\(0),
      I2 => \caracter[2]_i_2\(1),
      I3 => \caracter_reg[2]\,
      I4 => \caracter[2]_i_2_0\,
      I5 => \caracter[2]_i_2_1\,
      O => \reg_reg[0]\
    );
\caracter[2]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"D0DF0F0F"
    )
        port map (
      I0 => \^caracter[6]_i_52_0\,
      I1 => \caracter[2]_i_14_n_0\,
      I2 => \caracter[0]_i_2\(1),
      I3 => \caracter[2]_i_3_0\,
      I4 => \caracter[0]_i_2\(0),
      O => \caracter[2]_i_8_n_0\
    );
\caracter[3]_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"737F"
    )
        port map (
      I0 => \caracter[3]_i_16_n_0\,
      I1 => \caracter[0]_i_2\(0),
      I2 => \caracter[0]_i_2\(1),
      I3 => \disp_refresco[2]_2\(1),
      O => \current_state_reg[0]\
    );
\caracter[3]_i_16\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0810810660810810"
    )
        port map (
      I0 => \i__carry_i_11_n_0\,
      I1 => \i__carry__0_i_7_n_0\,
      I2 => \i__carry__0_i_8_n_0\,
      I3 => cent(4),
      I4 => cent(3),
      I5 => cent(1),
      O => \caracter[3]_i_16_n_0\
    );
\caracter[3]_i_19\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(1),
      I1 => \caracter[6]_i_54_0\,
      I2 => p_1_in(1),
      O => cent(1)
    );
\caracter[6]_i_19\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00DFFFFF00DF0000"
    )
        port map (
      I0 => \^caracter[6]_i_52_0\,
      I1 => \^caracter[6]_i_55_0\,
      I2 => \caracter[6]_i_4_0\,
      I3 => \caracter[1]_i_4\,
      I4 => \caracter[2]_i_2\(0),
      I5 => \caracter[6]_i_4_2\,
      O => \reg_reg[0]_0\
    );
\caracter[6]_i_21\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222202220202"
    )
        port map (
      I0 => \caracter[6]_i_36_n_0\,
      I1 => \caracter[6]_i_4\,
      I2 => \caracter[6]_i_21_0\,
      I3 => \^caracter[6]_i_61_0\,
      I4 => \caracter[6]_i_4_0\,
      I5 => \caracter[6]_i_4_1\,
      O => \current_state_reg[1]\
    );
\caracter[6]_i_30\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7777577757775757"
    )
        port map (
      I0 => \caracter[6]_i_48_n_0\,
      I1 => \caracter[6]_i_49_n_0\,
      I2 => \caracter[6]_i_50_n_0\,
      I3 => \caracter[6]_i_51_n_0\,
      I4 => \caracter[3]_i_16_n_0\,
      I5 => \caracter[6]_i_52_n_0\,
      O => \^caracter[6]_i_52_0\
    );
\caracter[6]_i_31\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"97F93ED316D13C93"
    )
        port map (
      I0 => \caracter[6]_i_49_n_0\,
      I1 => \caracter[6]_i_48_n_0\,
      I2 => \caracter[6]_i_53_n_0\,
      I3 => \caracter[6]_i_50_n_0\,
      I4 => \caracter[6]_i_54_n_0\,
      I5 => \caracter[6]_i_55_n_0\,
      O => \^caracter[6]_i_55_0\
    );
\caracter[6]_i_36\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFF200000"
    )
        port map (
      I0 => \^caracter[6]_i_52_0\,
      I1 => \caracter[2]_i_14_n_0\,
      I2 => \caracter[0]_i_2\(1),
      I3 => \caracter[6]_i_21_1\,
      I4 => \caracter[6]_i_21_0\,
      I5 => \caracter[6]_i_21_2\,
      O => \caracter[6]_i_36_n_0\
    );
\caracter[6]_i_39\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CD340130F37FD34C"
    )
        port map (
      I0 => \caracter[6]_i_59_n_0\,
      I1 => \caracter[2]_i_14_n_0\,
      I2 => \caracter[6]_i_55_n_0\,
      I3 => \caracter[6]_i_54_n_0\,
      I4 => \caracter[6]_i_60_n_0\,
      I5 => \caracter[6]_i_61_n_0\,
      O => \^caracter[6]_i_61_0\
    );
\caracter[6]_i_47\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6DB6D966D9669B6D"
    )
        port map (
      I0 => cent(1),
      I1 => cent(3),
      I2 => cent(4),
      I3 => \i__carry__0_i_8_n_0\,
      I4 => \i__carry__0_i_7_n_0\,
      I5 => \i__carry_i_11_n_0\,
      O => \^total_string_reg[1]\
    );
\caracter[6]_i_48\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1111111CCCCCCCCC"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_0\,
      I1 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_5\,
      I2 => \string_cent_decenas[1]5_inferred__0/i__carry_n_5\,
      I3 => \string_cent_decenas[1]5_inferred__0/i__carry_n_4\,
      I4 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_7\,
      I5 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_6\,
      O => \caracter[6]_i_48_n_0\
    );
\caracter[6]_i_49\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0002AAAA55565556"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_7\,
      I2 => \string_cent_decenas[1]5_inferred__0/i__carry_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i__carry_n_5\,
      I4 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_5\,
      I5 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_0\,
      O => \caracter[6]_i_49_n_0\
    );
\caracter[6]_i_50\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCC4CCCCCCC3CCC3"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_7\,
      I2 => \string_cent_decenas[1]5_inferred__0/i__carry_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i__carry_n_5\,
      I4 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_5\,
      I5 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_0\,
      O => \caracter[6]_i_50_n_0\
    );
\caracter[6]_i_51\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"55454404FFDFDD5D"
    )
        port map (
      I0 => \^total_string_reg[2]\,
      I1 => \string_cent_decenas[1]5\(1),
      I2 => cent(0),
      I3 => \string_cent_decenas[1]5\(0),
      I4 => \^total_string_reg[1]\,
      I5 => \caracter[6]_i_68_n_0\,
      O => \caracter[6]_i_51_n_0\
    );
\caracter[6]_i_52\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0FA70F0F0FF00FF0"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_7\,
      I2 => \string_cent_decenas[1]5_inferred__0/i__carry_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i__carry_n_5\,
      I4 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_5\,
      I5 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_0\,
      O => \caracter[6]_i_52_n_0\
    );
\caracter[6]_i_53\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7E061E00FF1E7E06"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i__carry_n_5\,
      I1 => \caracter[6]_i_69_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i__carry_n_4\,
      I3 => \caracter[3]_i_16_n_0\,
      I4 => \^total_string_reg[2]\,
      I5 => \caracter[6]_i_70_n_0\,
      O => \caracter[6]_i_53_n_0\
    );
\caracter[6]_i_54\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"566A9556A555AAA5"
    )
        port map (
      I0 => \caracter[6]_i_71_n_0\,
      I1 => \caracter[6]_i_72_n_0\,
      I2 => \caracter[6]_i_68_n_0\,
      I3 => \^total_string_reg[2]\,
      I4 => \caracter[6]_i_70_n_0\,
      I5 => \caracter[6]_i_73_n_0\,
      O => \caracter[6]_i_54_n_0\
    );
\caracter[6]_i_55\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"9A"
    )
        port map (
      I0 => \caracter[6]_i_74_n_0\,
      I1 => \caracter[6]_i_72_n_0\,
      I2 => \caracter[6]_i_73_n_0\,
      O => \caracter[6]_i_55_n_0\
    );
\caracter[6]_i_59\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66699666"
    )
        port map (
      I0 => \string_cent_decenas[1]5\(1),
      I1 => \^total_string_reg[1]\,
      I2 => \caracter[6]_i_73_n_0\,
      I3 => \string_cent_decenas[1]5\(0),
      I4 => cent(0),
      O => \caracter[6]_i_59_n_0\
    );
\caracter[6]_i_60\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F5AFAF0A505556F5"
    )
        port map (
      I0 => \caracter[2]_i_19_n_0\,
      I1 => \caracter[6]_i_75_n_0\,
      I2 => \caracter[6]_i_50_n_0\,
      I3 => \caracter[6]_i_53_n_0\,
      I4 => \caracter[6]_i_48_n_0\,
      I5 => \caracter[6]_i_49_n_0\,
      O => \caracter[6]_i_60_n_0\
    );
\caracter[6]_i_61\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9666999969996666"
    )
        port map (
      I0 => \caracter[6]_i_76_n_0\,
      I1 => \caracter[2]_i_19_n_0\,
      I2 => \caracter[6]_i_72_n_0\,
      I3 => \caracter[6]_i_74_n_0\,
      I4 => \caracter[6]_i_73_n_0\,
      I5 => \^caracter[6]_i_52_0\,
      O => \caracter[6]_i_61_n_0\
    );
\caracter[6]_i_67\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(0),
      I1 => \caracter[6]_i_54_0\,
      I2 => p_1_in(0),
      O => cent(0)
    );
\caracter[6]_i_68\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"D2D2D2DA5A5A5A5A"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_0\,
      I1 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_5\,
      I2 => \string_cent_decenas[1]5_inferred__0/i__carry_n_5\,
      I3 => \string_cent_decenas[1]5_inferred__0/i__carry_n_4\,
      I4 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_7\,
      I5 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_6\,
      O => \caracter[6]_i_68_n_0\
    );
\caracter[6]_i_69\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5557FFFF00000000"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_7\,
      I2 => \string_cent_decenas[1]5_inferred__0/i__carry_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i__carry_n_5\,
      I4 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_5\,
      I5 => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_0\,
      O => \caracter[6]_i_69_n_0\
    );
\caracter[6]_i_70\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFEEEFFF8A888AAA"
    )
        port map (
      I0 => \^total_string_reg[1]\,
      I1 => \string_cent_decenas[1]5\(0),
      I2 => Q(0),
      I3 => \caracter[6]_i_54_0\,
      I4 => p_1_in(0),
      I5 => \string_cent_decenas[1]5\(1),
      O => \caracter[6]_i_70_n_0\
    );
\caracter[6]_i_71\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6669"
    )
        port map (
      I0 => \caracter[3]_i_16_n_0\,
      I1 => \string_cent_decenas[1]5_inferred__0/i__carry_n_4\,
      I2 => \caracter[6]_i_69_n_0\,
      I3 => \string_cent_decenas[1]5_inferred__0/i__carry_n_5\,
      O => \caracter[6]_i_71_n_0\
    );
\caracter[6]_i_72\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4144411182888222"
    )
        port map (
      I0 => \string_cent_decenas[1]5\(1),
      I1 => \string_cent_decenas[1]5\(0),
      I2 => Q(0),
      I3 => \caracter[6]_i_54_0\,
      I4 => p_1_in(0),
      I5 => \^total_string_reg[1]\,
      O => \caracter[6]_i_72_n_0\
    );
\caracter[6]_i_73\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0101000100010000"
    )
        port map (
      I0 => \caracter[6]_i_48_n_0\,
      I1 => \caracter[6]_i_49_n_0\,
      I2 => \caracter[6]_i_50_n_0\,
      I3 => \caracter[6]_i_51_n_0\,
      I4 => \caracter[3]_i_16_n_0\,
      I5 => \caracter[6]_i_52_n_0\,
      O => \caracter[6]_i_73_n_0\
    );
\caracter[6]_i_74\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \caracter[6]_i_70_n_0\,
      I1 => \^total_string_reg[2]\,
      I2 => \string_cent_decenas[1]5_inferred__0/i__carry_n_5\,
      I3 => \caracter[6]_i_69_n_0\,
      O => \caracter[6]_i_74_n_0\
    );
\caracter[6]_i_75\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0600000660000060"
    )
        port map (
      I0 => \caracter[6]_i_68_n_0\,
      I1 => \^total_string_reg[2]\,
      I2 => \^total_string_reg[1]\,
      I3 => \string_cent_decenas[1]5\(0),
      I4 => cent(0),
      I5 => \string_cent_decenas[1]5\(1),
      O => \caracter[6]_i_75_n_0\
    );
\caracter[6]_i_76\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F000FFF01FFF111F"
    )
        port map (
      I0 => \caracter[6]_i_49_n_0\,
      I1 => \caracter[6]_i_48_n_0\,
      I2 => \caracter[6]_i_52_n_0\,
      I3 => \caracter[3]_i_16_n_0\,
      I4 => \caracter[6]_i_51_n_0\,
      I5 => \caracter[6]_i_50_n_0\,
      O => \caracter[6]_i_76_n_0\
    );
euros1_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => euros1_carry_n_0,
      CO(2) => euros1_carry_n_1,
      CO(1) => euros1_carry_n_2,
      CO(0) => euros1_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => Q(3 downto 0),
      O(3) => euros1_carry_n_4,
      O(2) => euros1_carry_n_5,
      O(1 downto 0) => p_1_in(1 downto 0),
      S(3) => euros1_carry_i_1_n_0,
      S(2) => euros1_carry_i_2_n_0,
      S(1) => euros1_carry_i_3_n_0,
      S(0) => euros1_carry_i_4_n_0
    );
\euros1_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => euros1_carry_n_0,
      CO(3) => \NLW_euros1_carry__0_CO_UNCONNECTED\(3),
      CO(2) => \euros1_carry__0_n_1\,
      CO(1) => \euros1_carry__0_n_2\,
      CO(0) => \euros1_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => Q(6 downto 4),
      O(3) => \euros1_carry__0_n_4\,
      O(2) => \euros1_carry__0_n_5\,
      O(1) => \euros1_carry__0_n_6\,
      O(0) => \euros1_carry__0_n_7\,
      S(3) => \euros1_carry__0_i_1_n_0\,
      S(2) => \euros1_carry__0_i_2_n_0\,
      S(1) => \euros1_carry__0_i_3_n_0\,
      S(0) => \euros1_carry__0_i_4_n_0\
    );
\euros1_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => Q(7),
      O => \euros1_carry__0_i_1_n_0\
    );
\euros1_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(6),
      I1 => Q(7),
      O => \euros1_carry__0_i_2_n_0\
    );
\euros1_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(5),
      I1 => Q(7),
      O => \euros1_carry__0_i_3_n_0\
    );
\euros1_carry__0_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => Q(4),
      O => \euros1_carry__0_i_4_n_0\
    );
euros1_carry_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => Q(3),
      O => euros1_carry_i_1_n_0
    );
euros1_carry_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(2),
      I1 => Q(7),
      O => euros1_carry_i_2_n_0
    );
euros1_carry_i_3: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => Q(1),
      O => euros1_carry_i_3_n_0
    );
euros1_carry_i_4: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => Q(0),
      O => euros1_carry_i_4_n_0
    );
\i__carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \i__carry__0_i_7_n_0\,
      O => cent(6)
    );
\i__carry__0_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \i__carry__0_i_8_n_0\,
      O => cent(5)
    );
\i__carry__0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BB88BB88BB88B88B"
    )
        port map (
      I0 => Q(4),
      I1 => \caracter[6]_i_54_0\,
      I2 => \i__carry_i_10_n_0\,
      I3 => \euros1_carry__0_n_7\,
      I4 => euros1_carry_n_5,
      I5 => euros1_carry_n_4,
      O => cent(4)
    );
\i__carry__0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4747747747474747"
    )
        port map (
      I0 => Q(6),
      I1 => \caracter[6]_i_54_0\,
      I2 => \euros1_carry__0_n_5\,
      I3 => \euros1_carry__0_n_4\,
      I4 => \i__carry__0_i_9_n_0\,
      I5 => \euros1_carry__0_n_6\,
      O => \i__carry__0_i_4_n_0\
    );
\i__carry__0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4477774744747747"
    )
        port map (
      I0 => Q(5),
      I1 => \caracter[6]_i_54_0\,
      I2 => \euros1_carry__0_n_4\,
      I3 => \i__carry__0_i_9_n_0\,
      I4 => \euros1_carry__0_n_6\,
      I5 => \euros1_carry__0_n_5\,
      O => \i__carry__0_i_5_n_0\
    );
\i__carry__0_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(4),
      O => \i__carry__0_i_6_n_0\
    );
\i__carry__0_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4747747747474747"
    )
        port map (
      I0 => Q(6),
      I1 => \caracter[6]_i_54_0\,
      I2 => \euros1_carry__0_n_5\,
      I3 => \euros1_carry__0_n_4\,
      I4 => \i__carry__0_i_9_n_0\,
      I5 => \euros1_carry__0_n_6\,
      O => \i__carry__0_i_7_n_0\
    );
\i__carry__0_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4477774744747747"
    )
        port map (
      I0 => Q(5),
      I1 => \caracter[6]_i_54_0\,
      I2 => \euros1_carry__0_n_4\,
      I3 => \i__carry__0_i_9_n_0\,
      I4 => \euros1_carry__0_n_6\,
      I5 => \euros1_carry__0_n_5\,
      O => \i__carry__0_i_8_n_0\
    );
\i__carry__0_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \euros1_carry__0_n_7\,
      I1 => euros1_carry_n_5,
      I2 => euros1_carry_n_4,
      O => \i__carry__0_i_9_n_0\
    );
\i__carry_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8B8B88B"
    )
        port map (
      I0 => Q(3),
      I1 => \caracter[6]_i_54_0\,
      I2 => euros1_carry_n_4,
      I3 => \i__carry_i_10_n_0\,
      I4 => euros1_carry_n_5,
      O => cent(3)
    );
\i__carry_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001555555555555"
    )
        port map (
      I0 => \euros1_carry__0_n_4\,
      I1 => \euros1_carry__0_n_7\,
      I2 => euros1_carry_n_5,
      I3 => euros1_carry_n_4,
      I4 => \euros1_carry__0_n_6\,
      I5 => \euros1_carry__0_n_5\,
      O => \i__carry_i_10_n_0\
    );
\i__carry_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"553C"
    )
        port map (
      I0 => Q(2),
      I1 => euros1_carry_n_5,
      I2 => \i__carry_i_10_n_0\,
      I3 => \caracter[6]_i_54_0\,
      O => \i__carry_i_11_n_0\
    );
\i__carry_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \i__carry_i_11_n_0\,
      O => cent(2)
    );
\i__carry_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(1),
      I1 => \caracter[6]_i_54_0\,
      I2 => p_1_in(1),
      O => \i__carry_i_3_n_0\
    );
\i__carry_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => Q(0),
      I1 => \caracter[6]_i_54_0\,
      I2 => p_1_in(0),
      O => \i__carry_i_4_n_0\
    );
\i__carry_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(3),
      O => \i__carry_i_5_n_0\
    );
\i__carry_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"553C"
    )
        port map (
      I0 => Q(2),
      I1 => euros1_carry_n_5,
      I2 => \i__carry_i_10_n_0\,
      I3 => \caracter[6]_i_54_0\,
      O => \i__carry_i_6_n_0\
    );
\i__carry_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1D"
    )
        port map (
      I0 => p_1_in(1),
      I1 => \caracter[6]_i_54_0\,
      I2 => Q(1),
      O => \i__carry_i_7_n_0\
    );
\i__carry_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1D"
    )
        port map (
      I0 => p_1_in(0),
      I1 => \caracter[6]_i_54_0\,
      I2 => Q(0),
      O => \i__carry_i_8_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5_inferred__0/i__carry_n_0\,
      CO(2) => \string_cent_decenas[1]5_inferred__0/i__carry_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i__carry_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i__carry_n_3\,
      CYINIT => '1',
      DI(3 downto 2) => cent(3 downto 2),
      DI(1) => \i__carry_i_3_n_0\,
      DI(0) => \i__carry_i_4_n_0\,
      O(3) => \string_cent_decenas[1]5_inferred__0/i__carry_n_4\,
      O(2) => \string_cent_decenas[1]5_inferred__0/i__carry_n_5\,
      O(1 downto 0) => \string_cent_decenas[1]5\(1 downto 0),
      S(3) => \i__carry_i_5_n_0\,
      S(2) => \i__carry_i_6_n_0\,
      S(1) => \i__carry_i_7_n_0\,
      S(0) => \i__carry_i_8_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i__carry_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_0\,
      CO(2) => \NLW_string_cent_decenas[1]5_inferred__0/i__carry__0_CO_UNCONNECTED\(2),
      CO(1) => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => cent(6 downto 4),
      O(3) => \NLW_string_cent_decenas[1]5_inferred__0/i__carry__0_O_UNCONNECTED\(3),
      O(2) => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i__carry__0_n_7\,
      S(3) => '1',
      S(2) => \i__carry__0_i_4_n_0\,
      S(1) => \i__carry__0_i_5_n_0\,
      S(0) => \i__carry__0_i_6_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity display is
  port (
    dot_OBUF : out STD_LOGIC;
    \charact_dot_reg[0]_0\ : out STD_LOGIC;
    \reg_reg[2]\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \charact_dot_reg[0]_1\ : out STD_LOGIC;
    \reg_reg[2]_0\ : out STD_LOGIC;
    \charact_dot_reg[0]_2\ : out STD_LOGIC;
    \reg_reg[1]\ : out STD_LOGIC;
    \reg_reg[0]\ : out STD_LOGIC;
    \charact_dot_reg[0]_3\ : out STD_LOGIC;
    \reg_reg[2]_1\ : out STD_LOGIC;
    \reg_reg[2]_2\ : out STD_LOGIC;
    \reg_reg[1]_0\ : out STD_LOGIC;
    \reg_reg[1]_1\ : out STD_LOGIC;
    \reg_reg[0]_0\ : out STD_LOGIC;
    \reg_reg[2]_3\ : out STD_LOGIC;
    elem_OBUF : out STD_LOGIC_VECTOR ( 7 downto 0 );
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    plusOp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    digit_OBUF : out STD_LOGIC_VECTOR ( 6 downto 0 );
    \caracter_reg[6]_0\ : out STD_LOGIC_VECTOR ( 5 downto 0 );
    clk_BUFG : in STD_LOGIC;
    output0 : in STD_LOGIC;
    \caracter[6]_i_4\ : in STD_LOGIC;
    \disp_refresco[1]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \caracter[0]_i_5\ : in STD_LOGIC;
    \caracter_reg[4]_0\ : in STD_LOGIC;
    \caracter_reg[4]_1\ : in STD_LOGIC;
    \caracter_reg[4]_2\ : in STD_LOGIC;
    \caracter_reg[6]_1\ : in STD_LOGIC;
    \caracter_reg[6]_2\ : in STD_LOGIC;
    \caracter_reg[6]_3\ : in STD_LOGIC;
    \caracter[2]_i_6\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_refresco[5]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    reset_IBUF : in STD_LOGIC;
    \caracter_reg[4]_3\ : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \caracter_reg[6]_4\ : in STD_LOGIC;
    \caracter_reg[4]_4\ : in STD_LOGIC;
    \caracter_reg[4]_5\ : in STD_LOGIC
  );
end display;

architecture STRUCTURE of display is
  signal Inst_contador_n_24 : STD_LOGIC;
  signal Inst_contador_n_26 : STD_LOGIC;
  signal caracter : STD_LOGIC_VECTOR ( 6 downto 4 );
  signal \^caracter_reg[6]_0\ : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal \^charact_dot_reg[0]_0\ : STD_LOGIC;
  signal dot : STD_LOGIC;
begin
  \caracter_reg[6]_0\(5 downto 0) <= \^caracter_reg[6]_0\(5 downto 0);
  \charact_dot_reg[0]_0\ <= \^charact_dot_reg[0]_0\;
Inst_contador: entity work.\contador__parameterized1\
     port map (
      CO(0) => CO(0),
      D(1) => caracter(6),
      D(0) => caracter(4),
      Q(2 downto 0) => Q(2 downto 0),
      \caracter[0]_i_5\ => \caracter[0]_i_5\,
      \caracter[2]_i_6\(1 downto 0) => \caracter[2]_i_6\(1 downto 0),
      \caracter[6]_i_4\ => Inst_contador_n_26,
      \caracter[6]_i_4_0\ => \caracter[6]_i_4\,
      \caracter_reg[4]\ => \caracter_reg[4]_0\,
      \caracter_reg[4]_0\ => \caracter_reg[4]_1\,
      \caracter_reg[4]_1\ => \caracter_reg[4]_2\,
      \caracter_reg[4]_2\ => \caracter_reg[4]_3\,
      \caracter_reg[4]_3\ => \caracter_reg[4]_4\,
      \caracter_reg[4]_4\ => \caracter_reg[4]_5\,
      \caracter_reg[6]\ => \caracter_reg[6]_1\,
      \caracter_reg[6]_0\ => \caracter_reg[6]_2\,
      \caracter_reg[6]_1\ => \caracter_reg[6]_3\,
      \caracter_reg[6]_2\ => \caracter_reg[6]_4\,
      \charact_dot_reg[0]\ => \charact_dot_reg[0]_1\,
      \charact_dot_reg[0]_0\ => \charact_dot_reg[0]_2\,
      \charact_dot_reg[0]_1\ => \charact_dot_reg[0]_3\,
      \charact_dot_reg[0]_2\ => Inst_contador_n_24,
      \charact_dot_reg[0]_3\ => \^charact_dot_reg[0]_0\,
      clk_BUFG => clk_BUFG,
      \disp_refresco[1]_0\(0) => \disp_refresco[1]_0\(0),
      \disp_refresco[5]_1\(0) => \disp_refresco[5]_1\(0),
      dot => dot,
      elem_OBUF(7 downto 0) => elem_OBUF(7 downto 0),
      output0 => output0,
      plusOp(1 downto 0) => plusOp(1 downto 0),
      \reg_reg[0]_0\ => \reg_reg[0]\,
      \reg_reg[0]_1\ => \reg_reg[0]_0\,
      \reg_reg[1]_0\ => \reg_reg[1]\,
      \reg_reg[1]_1\ => \reg_reg[1]_0\,
      \reg_reg[1]_2\ => \reg_reg[1]_1\,
      \reg_reg[2]_0\ => \reg_reg[2]\,
      \reg_reg[2]_1\ => \reg_reg[2]_0\,
      \reg_reg[2]_2\ => \reg_reg[2]_1\,
      \reg_reg[2]_3\ => \reg_reg[2]_2\,
      \reg_reg[2]_4\ => \reg_reg[2]_3\,
      reset_IBUF => reset_IBUF
    );
Inst_digito: entity work.digito
     port map (
      Q(5 downto 0) => \^caracter_reg[6]_0\(5 downto 0),
      digit_OBUF(3) => digit_OBUF(4),
      digit_OBUF(2 downto 0) => digit_OBUF(2 downto 0)
    );
\caracter_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => D(0),
      Q => \^caracter_reg[6]_0\(0)
    );
\caracter_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => D(1),
      Q => \^caracter_reg[6]_0\(1)
    );
\caracter_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => D(2),
      Q => \^caracter_reg[6]_0\(2)
    );
\caracter_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => D(3),
      Q => \^caracter_reg[6]_0\(3)
    );
\caracter_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => caracter(4),
      Q => \^caracter_reg[6]_0\(4)
    );
\caracter_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => caracter(6),
      Q => \^caracter_reg[6]_0\(5)
    );
\charact_dot_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => Inst_contador_n_24,
      Q => \^charact_dot_reg[0]_0\,
      R => '0'
    );
\digit_OBUF[3]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFFB6FB3EA9CFFFF"
    )
        port map (
      I0 => \^caracter_reg[6]_0\(3),
      I1 => \^caracter_reg[6]_0\(2),
      I2 => \^caracter_reg[6]_0\(0),
      I3 => \^caracter_reg[6]_0\(1),
      I4 => \^caracter_reg[6]_0\(4),
      I5 => \^caracter_reg[6]_0\(5),
      O => digit_OBUF(3)
    );
\digit_OBUF[5]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFFFF7EBFFD3B3D3"
    )
        port map (
      I0 => \^caracter_reg[6]_0\(0),
      I1 => \^caracter_reg[6]_0\(4),
      I2 => \^caracter_reg[6]_0\(5),
      I3 => \^caracter_reg[6]_0\(1),
      I4 => \^caracter_reg[6]_0\(3),
      I5 => \^caracter_reg[6]_0\(2),
      O => digit_OBUF(5)
    );
\digit_OBUF[6]_inst_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F7FFC90FFFCFF16F"
    )
        port map (
      I0 => \^caracter_reg[6]_0\(0),
      I1 => \^caracter_reg[6]_0\(2),
      I2 => \^caracter_reg[6]_0\(4),
      I3 => \^caracter_reg[6]_0\(5),
      I4 => \^caracter_reg[6]_0\(3),
      I5 => \^caracter_reg[6]_0\(1),
      O => digit_OBUF(6)
    );
dot_reg: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_BUFG,
      CE => dot,
      D => Inst_contador_n_26,
      PRE => output0,
      Q => dot_OBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gestion_dinero is
  port (
    \current_state_reg[1]\ : out STD_LOGIC;
    \caracter[6]_i_61\ : out STD_LOGIC;
    \reg_reg[0]\ : out STD_LOGIC;
    \total_string_reg[7]_0\ : out STD_LOGIC;
    \current_state_reg[1]_0\ : out STD_LOGIC;
    \current_state_reg[1]_1\ : out STD_LOGIC;
    \caracter[6]_i_52\ : out STD_LOGIC;
    \caracter[6]_i_55\ : out STD_LOGIC;
    \reg_reg[1]\ : out STD_LOGIC;
    \reg_reg[0]_0\ : out STD_LOGIC;
    \FSM_onehot_current_state_reg[3]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    st_dinero_OBUF : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \reg_reg[0]_1\ : out STD_LOGIC;
    \total_string_reg[2]_0\ : out STD_LOGIC;
    \total_string_reg[1]_0\ : out STD_LOGIC;
    \current_state_reg[0]\ : out STD_LOGIC;
    \total_string_reg[0]_0\ : out STD_LOGIC;
    \reg_reg[0]_2\ : out STD_LOGIC;
    \total_string_reg[0]_1\ : out STD_LOGIC;
    \current_state_reg[1]_2\ : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \tot_reg[7]_0\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \caracter[6]_i_4\ : in STD_LOGIC;
    \caracter[6]_i_21\ : in STD_LOGIC;
    \caracter[6]_i_4_0\ : in STD_LOGIC;
    \caracter[6]_i_4_1\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \tot_reg[0]_0\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_refresco[5]_0\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \caracter_reg[2]\ : in STD_LOGIC;
    \caracter_reg[2]_0\ : in STD_LOGIC;
    \caracter_reg[2]_1\ : in STD_LOGIC;
    \caracter_reg[2]_2\ : in STD_LOGIC;
    \caracter[1]_i_4\ : in STD_LOGIC;
    \caracter[1]_i_4_0\ : in STD_LOGIC;
    \caracter[3]_i_3\ : in STD_LOGIC;
    \caracter[3]_i_3_0\ : in STD_LOGIC;
    \caracter[2]_i_2\ : in STD_LOGIC;
    \caracter[6]_i_21_0\ : in STD_LOGIC;
    \caracter[6]_i_36\ : in STD_LOGIC;
    clr_dinero_r : in STD_LOGIC;
    button_ok_IBUF : in STD_LOGIC;
    \caracter[2]_i_3\ : in STD_LOGIC;
    \disp_refresco[2]_2\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \caracter[2]_i_11\ : in STD_LOGIC;
    \caracter[2]_i_11_0\ : in STD_LOGIC;
    \disp_refresco[4]_1\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \current_state_reg[0]_0\ : in STD_LOGIC;
    \disp_aux[1]_7\ : in STD_LOGIC;
    \total_reg[7]_0\ : in STD_LOGIC_VECTOR ( 6 downto 0 );
    clk_BUFG : in STD_LOGIC
  );
end gestion_dinero;

architecture STRUCTURE of gestion_dinero is
  signal \FSM_onehot_current_state[3]_i_1_n_0\ : STD_LOGIC;
  signal \^fsm_onehot_current_state_reg[3]_0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \FSM_onehot_current_state_reg_n_0_[1]\ : STD_LOGIC;
  signal \FSM_onehot_next_state_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_next_state_reg[1]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_next_state_reg[2]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_next_state_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_next_state_reg[3]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_onehot_next_state_reg[3]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_onehot_next_state_reg_n_0_[0]\ : STD_LOGIC;
  signal \FSM_onehot_next_state_reg_n_0_[1]\ : STD_LOGIC;
  signal \FSM_onehot_next_state_reg_n_0_[2]\ : STD_LOGIC;
  signal \FSM_onehot_next_state_reg_n_0_[3]\ : STD_LOGIC;
  signal aux : STD_LOGIC;
  signal \caracter[2]_i_10_n_0\ : STD_LOGIC;
  signal \caracter[2]_i_12_n_0\ : STD_LOGIC;
  signal \caracter[3]_i_17_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_34_n_0\ : STD_LOGIC;
  signal \caracter[6]_i_57_n_0\ : STD_LOGIC;
  signal \next_state__0\ : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \total0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \total0_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \total0_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \total0_carry__0_n_1\ : STD_LOGIC;
  signal \total0_carry__0_n_2\ : STD_LOGIC;
  signal \total0_carry__0_n_3\ : STD_LOGIC;
  signal total0_carry_i_1_n_0 : STD_LOGIC;
  signal total0_carry_i_2_n_0 : STD_LOGIC;
  signal total0_carry_i_3_n_0 : STD_LOGIC;
  signal total0_carry_i_4_n_0 : STD_LOGIC;
  signal total0_carry_n_0 : STD_LOGIC;
  signal total0_carry_n_1 : STD_LOGIC;
  signal total0_carry_n_2 : STD_LOGIC;
  signal total0_carry_n_3 : STD_LOGIC;
  signal total0_carry_n_7 : STD_LOGIC;
  signal \total[7]_i_1_n_0\ : STD_LOGIC;
  signal \total[7]_i_2_n_0\ : STD_LOGIC;
  signal total_out : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal total_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \total_string[0]_i_1_n_0\ : STD_LOGIC;
  signal \total_string[1]_i_1_n_0\ : STD_LOGIC;
  signal \total_string[2]_i_1_n_0\ : STD_LOGIC;
  signal \total_string[3]_i_1_n_0\ : STD_LOGIC;
  signal \total_string[3]_i_3_n_0\ : STD_LOGIC;
  signal \total_string[3]_i_4_n_0\ : STD_LOGIC;
  signal \total_string[3]_i_5_n_0\ : STD_LOGIC;
  signal \total_string[3]_i_6_n_0\ : STD_LOGIC;
  signal \total_string[4]_i_1_n_0\ : STD_LOGIC;
  signal \total_string[5]_i_1_n_0\ : STD_LOGIC;
  signal \total_string[6]_i_1_n_0\ : STD_LOGIC;
  signal \total_string[7]_i_1_n_0\ : STD_LOGIC;
  signal \total_string[7]_i_2_n_0\ : STD_LOGIC;
  signal \total_string[7]_i_4_n_0\ : STD_LOGIC;
  signal \total_string[7]_i_5_n_0\ : STD_LOGIC;
  signal \total_string[7]_i_6_n_0\ : STD_LOGIC;
  signal \total_string_reg[3]_i_2_n_0\ : STD_LOGIC;
  signal \total_string_reg[3]_i_2_n_1\ : STD_LOGIC;
  signal \total_string_reg[3]_i_2_n_2\ : STD_LOGIC;
  signal \total_string_reg[3]_i_2_n_3\ : STD_LOGIC;
  signal \total_string_reg[3]_i_2_n_4\ : STD_LOGIC;
  signal \total_string_reg[3]_i_2_n_5\ : STD_LOGIC;
  signal \total_string_reg[3]_i_2_n_6\ : STD_LOGIC;
  signal \^total_string_reg[7]_0\ : STD_LOGIC;
  signal \total_string_reg[7]_i_3_n_1\ : STD_LOGIC;
  signal \total_string_reg[7]_i_3_n_2\ : STD_LOGIC;
  signal \total_string_reg[7]_i_3_n_3\ : STD_LOGIC;
  signal \total_string_reg[7]_i_3_n_4\ : STD_LOGIC;
  signal \total_string_reg[7]_i_3_n_5\ : STD_LOGIC;
  signal \total_string_reg[7]_i_3_n_6\ : STD_LOGIC;
  signal \total_string_reg[7]_i_3_n_7\ : STD_LOGIC;
  signal \NLW_total0_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_total_string_reg[3]_i_2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_total_string_reg[7]_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[0]\ : label is "s2:1000,s1:0010,s0:0001,s3:0100";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[1]\ : label is "s2:1000,s1:0010,s0:0001,s3:0100";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[2]\ : label is "s2:1000,s1:0010,s0:0001,s3:0100";
  attribute FSM_ENCODED_STATES of \FSM_onehot_current_state_reg[3]\ : label is "s2:1000,s1:0010,s0:0001,s3:0100";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \FSM_onehot_next_state_reg[0]\ : label is "LD";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_onehot_next_state_reg[0]_i_1\ : label is "soft_lutpair15";
  attribute XILINX_LEGACY_PRIM of \FSM_onehot_next_state_reg[1]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \FSM_onehot_next_state_reg[2]\ : label is "LD";
  attribute SOFT_HLUTNM of \FSM_onehot_next_state_reg[2]_i_1\ : label is "soft_lutpair15";
  attribute XILINX_LEGACY_PRIM of \FSM_onehot_next_state_reg[3]\ : label is "LD";
  attribute SOFT_HLUTNM of \caracter[2]_i_10\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \caracter[2]_i_12\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \caracter[3]_i_15\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \caracter[6]_i_34\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of refresco_OBUF_inst_i_1 : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \st_dinero_OBUF[1]_inst_i_1\ : label is "soft_lutpair18";
begin
  \FSM_onehot_current_state_reg[3]_0\(1 downto 0) <= \^fsm_onehot_current_state_reg[3]_0\(1 downto 0);
  \total_string_reg[7]_0\ <= \^total_string_reg[7]_0\;
\FSM_onehot_current_state[3]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \tot_reg[0]_0\(1),
      I1 => clr_dinero_r,
      O => \FSM_onehot_current_state[3]_i_1_n_0\
    );
\FSM_onehot_current_state_reg[0]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      D => \FSM_onehot_next_state_reg_n_0_[0]\,
      PRE => \FSM_onehot_current_state[3]_i_1_n_0\,
      Q => aux
    );
\FSM_onehot_current_state_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => \FSM_onehot_current_state[3]_i_1_n_0\,
      D => \FSM_onehot_next_state_reg_n_0_[1]\,
      Q => \FSM_onehot_current_state_reg_n_0_[1]\
    );
\FSM_onehot_current_state_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => \FSM_onehot_current_state[3]_i_1_n_0\,
      D => \FSM_onehot_next_state_reg_n_0_[2]\,
      Q => \^fsm_onehot_current_state_reg[3]_0\(0)
    );
\FSM_onehot_current_state_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => \FSM_onehot_current_state[3]_i_1_n_0\,
      D => \FSM_onehot_next_state_reg_n_0_[3]\,
      Q => \^fsm_onehot_current_state_reg[3]_0\(1)
    );
\FSM_onehot_next_state_reg[0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '1'
    )
        port map (
      CLR => '0',
      D => \FSM_onehot_next_state_reg[0]_i_1_n_0\,
      G => \next_state__0\,
      GE => '1',
      Q => \FSM_onehot_next_state_reg_n_0_[0]\
    );
\FSM_onehot_next_state_reg[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"11331130"
    )
        port map (
      I0 => button_ok_IBUF,
      I1 => aux,
      I2 => \^fsm_onehot_current_state_reg[3]_0\(0),
      I3 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I4 => \^fsm_onehot_current_state_reg[3]_0\(1),
      O => \FSM_onehot_next_state_reg[0]_i_1_n_0\
    );
\FSM_onehot_next_state_reg[1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \FSM_onehot_next_state_reg[1]_i_1_n_0\,
      G => \next_state__0\,
      GE => '1',
      Q => \FSM_onehot_next_state_reg_n_0_[1]\
    );
\FSM_onehot_next_state_reg[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000010000000000"
    )
        port map (
      I0 => \FSM_onehot_next_state_reg[3]_i_3_n_0\,
      I1 => total_reg(7),
      I2 => total_reg(1),
      I3 => total_reg(2),
      I4 => total_reg(0),
      I5 => aux,
      O => \FSM_onehot_next_state_reg[1]_i_1_n_0\
    );
\FSM_onehot_next_state_reg[2]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \FSM_onehot_next_state_reg[2]_i_1_n_0\,
      G => \next_state__0\,
      GE => '1',
      Q => \FSM_onehot_next_state_reg_n_0_[2]\
    );
\FSM_onehot_next_state_reg[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => button_ok_IBUF,
      I1 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I2 => aux,
      O => \FSM_onehot_next_state_reg[2]_i_1_n_0\
    );
\FSM_onehot_next_state_reg[3]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \FSM_onehot_next_state_reg[3]_i_1_n_0\,
      G => \next_state__0\,
      GE => '1',
      Q => \FSM_onehot_next_state_reg_n_0_[3]\
    );
\FSM_onehot_next_state_reg[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAA8AAAA"
    )
        port map (
      I0 => aux,
      I1 => \FSM_onehot_next_state_reg[3]_i_3_n_0\,
      I2 => total_reg(7),
      I3 => total_reg(1),
      I4 => total_reg(2),
      I5 => total_reg(0),
      O => \FSM_onehot_next_state_reg[3]_i_1_n_0\
    );
\FSM_onehot_next_state_reg[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFF444444444"
    )
        port map (
      I0 => \FSM_onehot_next_state_reg[3]_i_4_n_0\,
      I1 => aux,
      I2 => \^fsm_onehot_current_state_reg[3]_0\(1),
      I3 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I4 => \^fsm_onehot_current_state_reg[3]_0\(0),
      I5 => button_ok_IBUF,
      O => \next_state__0\
    );
\FSM_onehot_next_state_reg[3]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFFF"
    )
        port map (
      I0 => total_reg(3),
      I1 => total_reg(4),
      I2 => total_reg(6),
      I3 => total_reg(5),
      O => \FSM_onehot_next_state_reg[3]_i_3_n_0\
    );
\FSM_onehot_next_state_reg[3]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000001FFFFFF"
    )
        port map (
      I0 => total_reg(2),
      I1 => total_reg(3),
      I2 => total_reg(4),
      I3 => total_reg(5),
      I4 => total_reg(6),
      I5 => total_reg(7),
      O => \FSM_onehot_next_state_reg[3]_i_4_n_0\
    );
\caracter[2]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"808C"
    )
        port map (
      I0 => \^total_string_reg[7]_0\,
      I1 => \tot_reg[0]_0\(0),
      I2 => \tot_reg[0]_0\(1),
      I3 => \disp_refresco[5]_0\(1),
      O => \caracter[2]_i_10_n_0\
    );
\caracter[2]_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \tot_reg[0]_0\(0),
      I1 => \^total_string_reg[7]_0\,
      I2 => \tot_reg[0]_0\(1),
      O => \caracter[2]_i_12_n_0\
    );
\caracter[2]_i_16\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BE3CC33CBE3CFF3C"
    )
        port map (
      I0 => \^total_string_reg[7]_0\,
      I1 => Q(0),
      I2 => \caracter[2]_i_11\,
      I3 => \tot_reg[0]_0\(0),
      I4 => \tot_reg[0]_0\(1),
      I5 => \caracter[2]_i_11_0\,
      O => \reg_reg[0]_2\
    );
\caracter[3]_i_15\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \tot_reg[0]_0\(1),
      I1 => \^total_string_reg[7]_0\,
      O => \current_state_reg[1]_2\
    );
\caracter[3]_i_17\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0F9FF0F00090F0F0"
    )
        port map (
      I0 => Q(0),
      I1 => \caracter[2]_i_11\,
      I2 => \tot_reg[0]_0\(1),
      I3 => \^total_string_reg[7]_0\,
      I4 => \tot_reg[0]_0\(0),
      I5 => \disp_refresco[4]_1\(1),
      O => \caracter[3]_i_17_n_0\
    );
\caracter[6]_i_34\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A30F"
    )
        port map (
      I0 => \^total_string_reg[7]_0\,
      I1 => \disp_refresco[4]_1\(0),
      I2 => \tot_reg[0]_0\(1),
      I3 => \tot_reg[0]_0\(0),
      O => \caracter[6]_i_34_n_0\
    );
\caracter[6]_i_57\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCCCCFDDFFCCCFDD"
    )
        port map (
      I0 => \caracter_reg[2]\,
      I1 => \caracter[6]_i_36\,
      I2 => \caracter[2]_i_12_n_0\,
      I3 => Q(1),
      I4 => Q(0),
      I5 => \caracter[2]_i_10_n_0\,
      O => \caracter[6]_i_57_n_0\
    );
\caracter[6]_i_63\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"080088CC080000CC"
    )
        port map (
      I0 => Q(0),
      I1 => Q(1),
      I2 => \^total_string_reg[7]_0\,
      I3 => \tot_reg[0]_0\(0),
      I4 => \tot_reg[0]_0\(1),
      I5 => \disp_refresco[5]_0\(1),
      O => \reg_reg[0]\
    );
\caracter_reg[3]_i_12\: unisim.vcomponents.MUXF7
     port map (
      I0 => \caracter[3]_i_17_n_0\,
      I1 => \caracter[3]_i_3_0\,
      O => \reg_reg[1]\,
      S => \caracter[3]_i_3\
    );
\i__carry_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1515151515151555"
    )
        port map (
      I0 => total_out(7),
      I1 => total_out(5),
      I2 => total_out(6),
      I3 => total_out(4),
      I4 => total_out(2),
      I5 => total_out(3),
      O => \^total_string_reg[7]_0\
    );
inst_int_to_string: entity work.int_to_string
     port map (
      Q(7 downto 0) => total_out(7 downto 0),
      \caracter[0]_i_2\(1 downto 0) => \tot_reg[0]_0\(1 downto 0),
      \caracter[1]_i_4\ => \caracter[1]_i_4\,
      \caracter[1]_i_4_0\ => \caracter[1]_i_4_0\,
      \caracter[2]_i_2\(1 downto 0) => Q(1 downto 0),
      \caracter[2]_i_2_0\ => \caracter[2]_i_12_n_0\,
      \caracter[2]_i_2_1\ => \caracter[2]_i_2\,
      \caracter[2]_i_3_0\ => \caracter[2]_i_3\,
      \caracter[6]_i_21_0\ => \caracter[6]_i_21\,
      \caracter[6]_i_21_1\ => \caracter[6]_i_21_0\,
      \caracter[6]_i_21_2\ => \caracter[6]_i_57_n_0\,
      \caracter[6]_i_4\ => \caracter[6]_i_4\,
      \caracter[6]_i_4_0\ => \caracter[6]_i_4_0\,
      \caracter[6]_i_4_1\ => \caracter[6]_i_4_1\,
      \caracter[6]_i_4_2\ => \caracter[6]_i_34_n_0\,
      \caracter[6]_i_52_0\ => \caracter[6]_i_52\,
      \caracter[6]_i_54_0\ => \^total_string_reg[7]_0\,
      \caracter[6]_i_55_0\ => \caracter[6]_i_55\,
      \caracter[6]_i_61_0\ => \caracter[6]_i_61\,
      \caracter_reg[2]\ => \caracter_reg[2]\,
      \caracter_reg[2]_0\ => \caracter_reg[2]_0\,
      \caracter_reg[2]_1\ => \caracter_reg[2]_1\,
      \caracter_reg[2]_2\ => \caracter[2]_i_10_n_0\,
      \caracter_reg[2]_3\ => \caracter_reg[2]_2\,
      \current_state_reg[0]\ => \current_state_reg[0]\,
      \current_state_reg[1]\ => \current_state_reg[1]\,
      \current_state_reg[1]_0\ => \current_state_reg[1]_0\,
      \current_state_reg[1]_1\ => \current_state_reg[1]_1\,
      \disp_refresco[2]_2\(1 downto 0) => \disp_refresco[2]_2\(1 downto 0),
      \disp_refresco[5]_0\(0) => \disp_refresco[5]_0\(0),
      \reg_reg[0]\ => \reg_reg[0]_0\,
      \reg_reg[0]_0\ => \reg_reg[0]_1\,
      \total_string_reg[0]\ => \total_string_reg[0]_0\,
      \total_string_reg[0]_0\ => \total_string_reg[0]_1\,
      \total_string_reg[1]\ => \total_string_reg[1]_0\,
      \total_string_reg[2]\ => \total_string_reg[2]_0\
    );
\next_state_reg[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEAFAEAFFEAAAEAA"
    )
        port map (
      I0 => \current_state_reg[0]_0\,
      I1 => button_ok_IBUF,
      I2 => \tot_reg[0]_0\(1),
      I3 => \tot_reg[0]_0\(0),
      I4 => \^fsm_onehot_current_state_reg[3]_0\(0),
      I5 => \disp_aux[1]_7\,
      O => E(0)
    );
refresco_OBUF_inst_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^fsm_onehot_current_state_reg[3]_0\(0),
      I1 => \FSM_onehot_current_state_reg_n_0_[1]\,
      O => st_dinero_OBUF(0)
    );
\st_dinero_OBUF[1]_inst_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^fsm_onehot_current_state_reg[3]_0\(0),
      I1 => \^fsm_onehot_current_state_reg[3]_0\(1),
      O => st_dinero_OBUF(1)
    );
\tot_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \total_string[7]_i_1_n_0\,
      D => total_out(0),
      Q => \tot_reg[7]_0\(0),
      R => '0'
    );
\tot_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \total_string[7]_i_1_n_0\,
      D => total_out(1),
      Q => \tot_reg[7]_0\(1),
      R => '0'
    );
\tot_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \total_string[7]_i_1_n_0\,
      D => total_out(2),
      Q => \tot_reg[7]_0\(2),
      R => '0'
    );
\tot_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \total_string[7]_i_1_n_0\,
      D => total_out(3),
      Q => \tot_reg[7]_0\(3),
      R => '0'
    );
\tot_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \total_string[7]_i_1_n_0\,
      D => total_out(4),
      Q => \tot_reg[7]_0\(4),
      R => '0'
    );
\tot_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \total_string[7]_i_1_n_0\,
      D => total_out(5),
      Q => \tot_reg[7]_0\(5),
      R => '0'
    );
\tot_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \total_string[7]_i_1_n_0\,
      D => total_out(6),
      Q => \tot_reg[7]_0\(6),
      R => '0'
    );
\tot_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \total_string[7]_i_1_n_0\,
      D => total_out(7),
      Q => \tot_reg[7]_0\(7),
      R => '0'
    );
total0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => total0_carry_n_0,
      CO(2) => total0_carry_n_1,
      CO(1) => total0_carry_n_2,
      CO(0) => total0_carry_n_3,
      CYINIT => '0',
      DI(3 downto 0) => total_reg(3 downto 0),
      O(3 downto 1) => p_0_in(3 downto 1),
      O(0) => total0_carry_n_7,
      S(3) => total0_carry_i_1_n_0,
      S(2) => total0_carry_i_2_n_0,
      S(1) => total0_carry_i_3_n_0,
      S(0) => total0_carry_i_4_n_0
    );
\total0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => total0_carry_n_0,
      CO(3) => \NLW_total0_carry__0_CO_UNCONNECTED\(3),
      CO(2) => \total0_carry__0_n_1\,
      CO(1) => \total0_carry__0_n_2\,
      CO(0) => \total0_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => total_reg(6 downto 4),
      O(3 downto 0) => p_0_in(7 downto 4),
      S(3) => total_reg(7),
      S(2) => \total0_carry__0_i_1_n_0\,
      S(1) => \total0_carry__0_i_2_n_0\,
      S(0) => \total0_carry__0_i_3_n_0\
    );
\total0_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(6),
      I1 => \total_reg[7]_0\(6),
      O => \total0_carry__0_i_1_n_0\
    );
\total0_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(5),
      I1 => \total_reg[7]_0\(5),
      O => \total0_carry__0_i_2_n_0\
    );
\total0_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(4),
      I1 => \total_reg[7]_0\(4),
      O => \total0_carry__0_i_3_n_0\
    );
total0_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(3),
      I1 => \total_reg[7]_0\(3),
      O => total0_carry_i_1_n_0
    );
total0_carry_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(2),
      I1 => \total_reg[7]_0\(2),
      O => total0_carry_i_2_n_0
    );
total0_carry_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(1),
      I1 => \total_reg[7]_0\(1),
      O => total0_carry_i_3_n_0
    );
total0_carry_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(0),
      I1 => \total_reg[7]_0\(0),
      O => total0_carry_i_4_n_0
    );
\total[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(0),
      I1 => \total_reg[7]_0\(0),
      O => p_0_in(0)
    );
\total[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \tot_reg[0]_0\(1),
      I1 => clr_dinero_r,
      I2 => aux,
      O => \total[7]_i_1_n_0\
    );
\total[7]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"01000000"
    )
        port map (
      I0 => \^fsm_onehot_current_state_reg[3]_0\(0),
      I1 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I2 => \^fsm_onehot_current_state_reg[3]_0\(1),
      I3 => \tot_reg[0]_0\(1),
      I4 => clr_dinero_r,
      O => \total[7]_i_2_n_0\
    );
\total_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \total[7]_i_2_n_0\,
      D => p_0_in(0),
      Q => total_reg(0),
      R => \total[7]_i_1_n_0\
    );
\total_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \total[7]_i_2_n_0\,
      D => p_0_in(1),
      Q => total_reg(1),
      R => \total[7]_i_1_n_0\
    );
\total_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \total[7]_i_2_n_0\,
      D => p_0_in(2),
      Q => total_reg(2),
      R => \total[7]_i_1_n_0\
    );
\total_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \total[7]_i_2_n_0\,
      D => p_0_in(3),
      Q => total_reg(3),
      R => \total[7]_i_1_n_0\
    );
\total_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \total[7]_i_2_n_0\,
      D => p_0_in(4),
      Q => total_reg(4),
      R => \total[7]_i_1_n_0\
    );
\total_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \total[7]_i_2_n_0\,
      D => p_0_in(5),
      Q => total_reg(5),
      R => \total[7]_i_1_n_0\
    );
\total_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \total[7]_i_2_n_0\,
      D => p_0_in(6),
      Q => total_reg(6),
      R => \total[7]_i_1_n_0\
    );
\total_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \total[7]_i_2_n_0\,
      D => p_0_in(7),
      Q => total_reg(7),
      R => \total[7]_i_1_n_0\
    );
\total_string[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00020000"
    )
        port map (
      I0 => total0_carry_n_7,
      I1 => \^fsm_onehot_current_state_reg[3]_0\(0),
      I2 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I3 => \^fsm_onehot_current_state_reg[3]_0\(1),
      I4 => aux,
      I5 => total_out(0),
      O => \total_string[0]_i_1_n_0\
    );
\total_string[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00020000"
    )
        port map (
      I0 => \total_string_reg[3]_i_2_n_6\,
      I1 => \^fsm_onehot_current_state_reg[3]_0\(0),
      I2 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I3 => \^fsm_onehot_current_state_reg[3]_0\(1),
      I4 => aux,
      I5 => total_out(1),
      O => \total_string[1]_i_1_n_0\
    );
\total_string[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00020000"
    )
        port map (
      I0 => \total_string_reg[3]_i_2_n_5\,
      I1 => \^fsm_onehot_current_state_reg[3]_0\(0),
      I2 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I3 => \^fsm_onehot_current_state_reg[3]_0\(1),
      I4 => aux,
      I5 => total_out(2),
      O => \total_string[2]_i_1_n_0\
    );
\total_string[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00020000"
    )
        port map (
      I0 => \total_string_reg[3]_i_2_n_4\,
      I1 => \^fsm_onehot_current_state_reg[3]_0\(0),
      I2 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I3 => \^fsm_onehot_current_state_reg[3]_0\(1),
      I4 => aux,
      I5 => total_out(3),
      O => \total_string[3]_i_1_n_0\
    );
\total_string[3]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(3),
      I1 => \total_reg[7]_0\(3),
      O => \total_string[3]_i_3_n_0\
    );
\total_string[3]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(2),
      I1 => \total_reg[7]_0\(2),
      O => \total_string[3]_i_4_n_0\
    );
\total_string[3]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(1),
      I1 => \total_reg[7]_0\(1),
      O => \total_string[3]_i_5_n_0\
    );
\total_string[3]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(0),
      I1 => \total_reg[7]_0\(0),
      O => \total_string[3]_i_6_n_0\
    );
\total_string[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00020000"
    )
        port map (
      I0 => \total_string_reg[7]_i_3_n_7\,
      I1 => \^fsm_onehot_current_state_reg[3]_0\(0),
      I2 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I3 => \^fsm_onehot_current_state_reg[3]_0\(1),
      I4 => aux,
      I5 => total_out(4),
      O => \total_string[4]_i_1_n_0\
    );
\total_string[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00020000"
    )
        port map (
      I0 => \total_string_reg[7]_i_3_n_6\,
      I1 => \^fsm_onehot_current_state_reg[3]_0\(0),
      I2 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I3 => \^fsm_onehot_current_state_reg[3]_0\(1),
      I4 => aux,
      I5 => total_out(5),
      O => \total_string[5]_i_1_n_0\
    );
\total_string[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00020000"
    )
        port map (
      I0 => \total_string_reg[7]_i_3_n_5\,
      I1 => \^fsm_onehot_current_state_reg[3]_0\(0),
      I2 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I3 => \^fsm_onehot_current_state_reg[3]_0\(1),
      I4 => aux,
      I5 => total_out(6),
      O => \total_string[6]_i_1_n_0\
    );
\total_string[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clr_dinero_r,
      I1 => \tot_reg[0]_0\(1),
      O => \total_string[7]_i_1_n_0\
    );
\total_string[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00020000"
    )
        port map (
      I0 => \total_string_reg[7]_i_3_n_4\,
      I1 => \^fsm_onehot_current_state_reg[3]_0\(0),
      I2 => \FSM_onehot_current_state_reg_n_0_[1]\,
      I3 => \^fsm_onehot_current_state_reg[3]_0\(1),
      I4 => aux,
      I5 => total_out(7),
      O => \total_string[7]_i_2_n_0\
    );
\total_string[7]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(6),
      I1 => \total_reg[7]_0\(6),
      O => \total_string[7]_i_4_n_0\
    );
\total_string[7]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(5),
      I1 => \total_reg[7]_0\(5),
      O => \total_string[7]_i_5_n_0\
    );
\total_string[7]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_reg(4),
      I1 => \total_reg[7]_0\(4),
      O => \total_string[7]_i_6_n_0\
    );
\total_string_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \total_string[7]_i_1_n_0\,
      D => \total_string[0]_i_1_n_0\,
      Q => total_out(0),
      R => '0'
    );
\total_string_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \total_string[7]_i_1_n_0\,
      D => \total_string[1]_i_1_n_0\,
      Q => total_out(1),
      R => '0'
    );
\total_string_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \total_string[7]_i_1_n_0\,
      D => \total_string[2]_i_1_n_0\,
      Q => total_out(2),
      R => '0'
    );
\total_string_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \total_string[7]_i_1_n_0\,
      D => \total_string[3]_i_1_n_0\,
      Q => total_out(3),
      R => '0'
    );
\total_string_reg[3]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \total_string_reg[3]_i_2_n_0\,
      CO(2) => \total_string_reg[3]_i_2_n_1\,
      CO(1) => \total_string_reg[3]_i_2_n_2\,
      CO(0) => \total_string_reg[3]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => total_reg(3 downto 0),
      O(3) => \total_string_reg[3]_i_2_n_4\,
      O(2) => \total_string_reg[3]_i_2_n_5\,
      O(1) => \total_string_reg[3]_i_2_n_6\,
      O(0) => \NLW_total_string_reg[3]_i_2_O_UNCONNECTED\(0),
      S(3) => \total_string[3]_i_3_n_0\,
      S(2) => \total_string[3]_i_4_n_0\,
      S(1) => \total_string[3]_i_5_n_0\,
      S(0) => \total_string[3]_i_6_n_0\
    );
\total_string_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \total_string[7]_i_1_n_0\,
      D => \total_string[4]_i_1_n_0\,
      Q => total_out(4),
      R => '0'
    );
\total_string_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \total_string[7]_i_1_n_0\,
      D => \total_string[5]_i_1_n_0\,
      Q => total_out(5),
      R => '0'
    );
\total_string_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \total_string[7]_i_1_n_0\,
      D => \total_string[6]_i_1_n_0\,
      Q => total_out(6),
      R => '0'
    );
\total_string_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => \total_string[7]_i_1_n_0\,
      D => \total_string[7]_i_2_n_0\,
      Q => total_out(7),
      R => '0'
    );
\total_string_reg[7]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \total_string_reg[3]_i_2_n_0\,
      CO(3) => \NLW_total_string_reg[7]_i_3_CO_UNCONNECTED\(3),
      CO(2) => \total_string_reg[7]_i_3_n_1\,
      CO(1) => \total_string_reg[7]_i_3_n_2\,
      CO(0) => \total_string_reg[7]_i_3_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => total_reg(6 downto 4),
      O(3) => \total_string_reg[7]_i_3_n_4\,
      O(2) => \total_string_reg[7]_i_3_n_5\,
      O(1) => \total_string_reg[7]_i_3_n_6\,
      O(0) => \total_string_reg[7]_i_3_n_7\,
      S(3) => total_reg(7),
      S(2) => \total_string[7]_i_4_n_0\,
      S(1) => \total_string[7]_i_5_n_0\,
      S(0) => \total_string[7]_i_6_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity prescaler is
  port (
    output0 : out STD_LOGIC;
    clk : out STD_LOGIC;
    clk_100Mh_IBUF_BUFG : in STD_LOGIC;
    reset_IBUF : in STD_LOGIC
  );
end prescaler;

architecture STRUCTURE of prescaler is
  signal \^clk\ : STD_LOGIC;
  signal uut_n_1 : STD_LOGIC;
begin
  clk <= \^clk\;
clk_pre_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_100Mh_IBUF_BUFG,
      CE => '1',
      D => uut_n_1,
      Q => \^clk\,
      R => '0'
    );
uut: entity work.contador
     port map (
      clk => \^clk\,
      clk_100Mh_IBUF_BUFG => clk_100Mh_IBUF_BUFG,
      \reg_reg[10]_0\ => uut_n_1,
      reset => output0,
      reset_IBUF => reset_IBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity fsm is
  port (
    \disp_aux_reg[6][3]_i_2\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_aux_reg[6][3]_i_2_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    D : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \reg_reg[2]\ : out STD_LOGIC;
    \reg_reg[0]\ : out STD_LOGIC;
    \current_state_reg[1]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \current_state_reg[1]_1\ : out STD_LOGIC;
    \current_state_reg[1]_2\ : out STD_LOGIC;
    \current_state_reg[1]_3\ : out STD_LOGIC;
    \current_state_reg[0]_0\ : out STD_LOGIC;
    \current_state_reg[1]_4\ : out STD_LOGIC;
    \FSM_onehot_current_state_reg[3]\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    st_dinero_OBUF : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \current_state_reg[1]_5\ : out STD_LOGIC;
    \current_state_reg[1]_6\ : out STD_LOGIC;
    \current_state_reg[1]_7\ : out STD_LOGIC;
    \reg_reg[1]\ : out STD_LOGIC;
    \current_state_reg[1]_8\ : out STD_LOGIC;
    \tot_reg[7]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    clk_BUFG : in STD_LOGIC;
    output0 : in STD_LOGIC;
    plusOp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \caracter_reg[2]\ : in STD_LOGIC;
    \caracter[6]_i_21\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \caracter[6]_i_36\ : in STD_LOGIC;
    \caracter_reg[3]\ : in STD_LOGIC;
    \caracter_reg[0]\ : in STD_LOGIC;
    \caracter_reg[3]_0\ : in STD_LOGIC;
    \caracter_reg[2]_0\ : in STD_LOGIC;
    \caracter[3]_i_3\ : in STD_LOGIC;
    \caracter[2]_i_3\ : in STD_LOGIC;
    \caracter[2]_i_11\ : in STD_LOGIC;
    \caracter_reg[1]\ : in STD_LOGIC;
    \caracter_reg[1]_0\ : in STD_LOGIC;
    \caracter[2]_i_2\ : in STD_LOGIC;
    \caracter_reg[0]_0\ : in STD_LOGIC;
    button_ok_IBUF : in STD_LOGIC;
    \current_state_reg[0]_1\ : in STD_LOGIC;
    ref_option_IBUF : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    \total_reg[7]\ : in STD_LOGIC_VECTOR ( 6 downto 0 );
    \current_state_reg[1]_9\ : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
end fsm;

architecture STRUCTURE of fsm is
  signal Inst_gestion_dinero_n_0 : STD_LOGIC;
  signal Inst_gestion_dinero_n_1 : STD_LOGIC;
  signal Inst_gestion_dinero_n_14 : STD_LOGIC;
  signal Inst_gestion_dinero_n_15 : STD_LOGIC;
  signal Inst_gestion_dinero_n_16 : STD_LOGIC;
  signal Inst_gestion_dinero_n_17 : STD_LOGIC;
  signal Inst_gestion_dinero_n_18 : STD_LOGIC;
  signal Inst_gestion_dinero_n_19 : STD_LOGIC;
  signal Inst_gestion_dinero_n_2 : STD_LOGIC;
  signal Inst_gestion_dinero_n_20 : STD_LOGIC;
  signal Inst_gestion_dinero_n_21 : STD_LOGIC;
  signal Inst_gestion_dinero_n_22 : STD_LOGIC;
  signal Inst_gestion_dinero_n_3 : STD_LOGIC;
  signal Inst_gestion_dinero_n_4 : STD_LOGIC;
  signal Inst_gestion_dinero_n_5 : STD_LOGIC;
  signal Inst_gestion_dinero_n_6 : STD_LOGIC;
  signal Inst_gestion_dinero_n_7 : STD_LOGIC;
  signal Inst_gestion_dinero_n_8 : STD_LOGIC;
  signal Inst_gestion_dinero_n_9 : STD_LOGIC;
  signal Inst_gestion_refresco_n_14 : STD_LOGIC;
  signal Inst_gestion_refresco_n_17 : STD_LOGIC;
  signal Inst_gestion_refresco_n_21 : STD_LOGIC;
  signal Inst_gestion_refresco_n_22 : STD_LOGIC;
  signal Inst_gestion_refresco_n_23 : STD_LOGIC;
  signal Inst_gestion_refresco_n_24 : STD_LOGIC;
  signal Inst_gestion_refresco_n_26 : STD_LOGIC;
  signal Inst_gestion_refresco_n_27 : STD_LOGIC;
  signal \caracter[6]_i_32_n_0\ : STD_LOGIC;
  signal clr_dinero_r : STD_LOGIC;
  signal \^current_state_reg[0]_0\ : STD_LOGIC;
  signal \^current_state_reg[1]_0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^current_state_reg[1]_6\ : STD_LOGIC;
  signal \disp_aux[1]_7\ : STD_LOGIC;
  signal \^disp_aux_reg[6][3]_i_2\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^disp_aux_reg[6][3]_i_2_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \disp_refresco[2]_2\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \disp_refresco[4]_1\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \disp_refresco[5]_0\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal next_state : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \caracter[2]_i_5\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \caracter[6]_i_32\ : label is "soft_lutpair44";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \next_state_reg[0]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \next_state_reg[1]\ : label is "LD";
begin
  \current_state_reg[0]_0\ <= \^current_state_reg[0]_0\;
  \current_state_reg[1]_0\(1 downto 0) <= \^current_state_reg[1]_0\(1 downto 0);
  \current_state_reg[1]_6\ <= \^current_state_reg[1]_6\;
  \disp_aux_reg[6][3]_i_2\(0) <= \^disp_aux_reg[6][3]_i_2\(0);
  \disp_aux_reg[6][3]_i_2_0\(0) <= \^disp_aux_reg[6][3]_i_2_0\(0);
Inst_gestion_dinero: entity work.gestion_dinero
     port map (
      E(0) => Inst_gestion_dinero_n_22,
      \FSM_onehot_current_state_reg[3]_0\(1 downto 0) => \FSM_onehot_current_state_reg[3]\(1 downto 0),
      Q(1 downto 0) => Q(1 downto 0),
      button_ok_IBUF => button_ok_IBUF,
      \caracter[1]_i_4\ => Inst_gestion_refresco_n_22,
      \caracter[1]_i_4_0\ => Inst_gestion_refresco_n_21,
      \caracter[2]_i_11\ => \caracter[2]_i_11\,
      \caracter[2]_i_11_0\ => \^disp_aux_reg[6][3]_i_2\(0),
      \caracter[2]_i_2\ => \caracter[2]_i_2\,
      \caracter[2]_i_3\ => \^disp_aux_reg[6][3]_i_2_0\(0),
      \caracter[3]_i_3\ => \caracter[3]_i_3\,
      \caracter[3]_i_3_0\ => Inst_gestion_refresco_n_26,
      \caracter[6]_i_21\ => \caracter[6]_i_21\,
      \caracter[6]_i_21_0\ => Inst_gestion_refresco_n_27,
      \caracter[6]_i_36\ => \caracter[6]_i_36\,
      \caracter[6]_i_4\ => Inst_gestion_refresco_n_14,
      \caracter[6]_i_4_0\ => \caracter[6]_i_32_n_0\,
      \caracter[6]_i_4_1\ => Inst_gestion_refresco_n_24,
      \caracter[6]_i_52\ => Inst_gestion_dinero_n_6,
      \caracter[6]_i_55\ => Inst_gestion_dinero_n_7,
      \caracter[6]_i_61\ => Inst_gestion_dinero_n_1,
      \caracter_reg[2]\ => Inst_gestion_refresco_n_23,
      \caracter_reg[2]_0\ => \caracter_reg[3]_0\,
      \caracter_reg[2]_1\ => \caracter_reg[3]\,
      \caracter_reg[2]_2\ => Inst_gestion_refresco_n_17,
      clk_BUFG => clk_BUFG,
      clr_dinero_r => clr_dinero_r,
      \current_state_reg[0]\ => Inst_gestion_dinero_n_17,
      \current_state_reg[0]_0\ => \current_state_reg[0]_1\,
      \current_state_reg[1]\ => Inst_gestion_dinero_n_0,
      \current_state_reg[1]_0\ => Inst_gestion_dinero_n_4,
      \current_state_reg[1]_1\ => Inst_gestion_dinero_n_5,
      \current_state_reg[1]_2\ => Inst_gestion_dinero_n_21,
      \disp_aux[1]_7\ => \disp_aux[1]_7\,
      \disp_refresco[2]_2\(1) => \disp_refresco[2]_2\(3),
      \disp_refresco[2]_2\(0) => \disp_refresco[2]_2\(0),
      \disp_refresco[4]_1\(1) => \disp_refresco[4]_1\(3),
      \disp_refresco[4]_1\(0) => \disp_refresco[4]_1\(1),
      \disp_refresco[5]_0\(1) => \disp_refresco[5]_0\(4),
      \disp_refresco[5]_0\(0) => \disp_refresco[5]_0\(0),
      \reg_reg[0]\ => Inst_gestion_dinero_n_2,
      \reg_reg[0]_0\ => Inst_gestion_dinero_n_9,
      \reg_reg[0]_1\ => Inst_gestion_dinero_n_14,
      \reg_reg[0]_2\ => Inst_gestion_dinero_n_19,
      \reg_reg[1]\ => Inst_gestion_dinero_n_8,
      st_dinero_OBUF(1 downto 0) => st_dinero_OBUF(1 downto 0),
      \tot_reg[0]_0\(1 downto 0) => \^current_state_reg[1]_0\(1 downto 0),
      \tot_reg[7]_0\(7 downto 0) => \tot_reg[7]\(7 downto 0),
      \total_reg[7]_0\(6 downto 0) => \total_reg[7]\(6 downto 0),
      \total_string_reg[0]_0\ => Inst_gestion_dinero_n_18,
      \total_string_reg[0]_1\ => Inst_gestion_dinero_n_20,
      \total_string_reg[1]_0\ => Inst_gestion_dinero_n_16,
      \total_string_reg[2]_0\ => Inst_gestion_dinero_n_15,
      \total_string_reg[7]_0\ => Inst_gestion_dinero_n_3
    );
Inst_gestion_refresco: entity work.gestion_refresco
     port map (
      CO(0) => CO(0),
      D(3 downto 0) => D(3 downto 0),
      Q(2 downto 0) => Q(2 downto 0),
      \caracter[0]_i_5_0\ => Inst_gestion_dinero_n_1,
      \caracter[1]_i_2_0\ => Inst_gestion_dinero_n_7,
      \caracter[1]_i_2_1\ => \caracter[6]_i_32_n_0\,
      \caracter[1]_i_4_0\ => \caracter[3]_i_3\,
      \caracter[1]_i_8\ => Inst_gestion_dinero_n_16,
      \caracter[2]_i_3\ => Inst_gestion_dinero_n_19,
      \caracter[2]_i_3_0\ => \caracter[2]_i_3\,
      \caracter[3]_i_2_0\ => Inst_gestion_dinero_n_21,
      \caracter[3]_i_3_0\ => Inst_gestion_dinero_n_6,
      \caracter[6]_i_20_0\ => Inst_gestion_dinero_n_3,
      \caracter[6]_i_28_0\ => \caracter[6]_i_36\,
      \caracter[6]_i_28_1\ => Inst_gestion_dinero_n_2,
      \caracter[6]_i_57\ => Inst_gestion_dinero_n_15,
      \caracter[6]_i_5_0\ => \caracter[2]_i_11\,
      \caracter_reg[0]\ => Inst_gestion_dinero_n_18,
      \caracter_reg[0]_0\(1 downto 0) => \^current_state_reg[1]_0\(1 downto 0),
      \caracter_reg[0]_1\ => \caracter_reg[0]\,
      \caracter_reg[0]_2\ => Inst_gestion_dinero_n_20,
      \caracter_reg[0]_3\ => \caracter_reg[0]_0\,
      \caracter_reg[1]\ => Inst_gestion_dinero_n_5,
      \caracter_reg[1]_0\ => \caracter_reg[1]\,
      \caracter_reg[1]_1\ => \caracter_reg[1]_0\,
      \caracter_reg[2]\ => \caracter_reg[2]\,
      \caracter_reg[2]_0\ => Inst_gestion_dinero_n_14,
      \caracter_reg[2]_1\ => Inst_gestion_dinero_n_0,
      \caracter_reg[2]_2\ => \caracter_reg[2]_0\,
      \caracter_reg[2]_3\ => \^current_state_reg[0]_0\,
      \caracter_reg[2]_4\ => Inst_gestion_dinero_n_9,
      \caracter_reg[2]_5\ => Inst_gestion_dinero_n_4,
      \caracter_reg[3]\ => \caracter_reg[3]\,
      \caracter_reg[3]_0\ => \caracter_reg[3]_0\,
      \caracter_reg[3]_1\ => Inst_gestion_dinero_n_17,
      \caracter_reg[3]_2\ => Inst_gestion_dinero_n_8,
      \caracter_reg[4]\ => \^current_state_reg[1]_6\,
      \current_state_reg[0]\ => Inst_gestion_refresco_n_21,
      \current_state_reg[0]_0\ => Inst_gestion_refresco_n_27,
      \current_state_reg[1]\ => \current_state_reg[1]_1\,
      \current_state_reg[1]_0\ => \current_state_reg[1]_2\,
      \current_state_reg[1]_1\ => \current_state_reg[1]_3\,
      \current_state_reg[1]_2\ => \current_state_reg[1]_4\,
      \current_state_reg[1]_3\ => Inst_gestion_refresco_n_22,
      \current_state_reg[1]_4\ => Inst_gestion_refresco_n_23,
      \current_state_reg[1]_5\ => Inst_gestion_refresco_n_24,
      \current_state_reg[1]_6\ => \current_state_reg[1]_5\,
      \current_state_reg[1]_7\ => \current_state_reg[1]_7\,
      \current_state_reg[1]_8\ => \current_state_reg[1]_8\,
      \disp_aux[1]_7\ => \disp_aux[1]_7\,
      \disp_aux_reg[6][3]_i_2_0\(1) => \disp_refresco[5]_0\(4),
      \disp_aux_reg[6][3]_i_2_0\(0) => \disp_refresco[5]_0\(0),
      \disp_aux_reg[6][3]_i_2_1\(1) => \disp_refresco[2]_2\(3),
      \disp_aux_reg[6][3]_i_2_1\(0) => \disp_refresco[2]_2\(0),
      \disp_aux_reg[6][3]_i_2_2\ => \^disp_aux_reg[6][3]_i_2\(0),
      \disp_aux_reg[6][3]_i_2_3\ => \^disp_aux_reg[6][3]_i_2_0\(0),
      \disp_refresco[4]_1\(1) => \disp_refresco[4]_1\(3),
      \disp_refresco[4]_1\(0) => \disp_refresco[4]_1\(1),
      plusOp(1 downto 0) => plusOp(1 downto 0),
      ref_option_IBUF(3 downto 0) => ref_option_IBUF(3 downto 0),
      \reg_reg[0]\ => \reg_reg[0]\,
      \reg_reg[0]_0\ => Inst_gestion_refresco_n_17,
      \reg_reg[0]_1\ => Inst_gestion_refresco_n_26,
      \reg_reg[1]\ => Inst_gestion_refresco_n_14,
      \reg_reg[1]_0\ => \reg_reg[1]\,
      \reg_reg[2]\ => \reg_reg[2]\
    );
\caracter[2]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^current_state_reg[1]_0\(0),
      I1 => \^current_state_reg[1]_0\(1),
      O => \^current_state_reg[0]_0\
    );
\caracter[4]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^current_state_reg[1]_0\(1),
      I1 => \^current_state_reg[1]_0\(0),
      O => \^current_state_reg[1]_6\
    );
\caracter[6]_i_32\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^current_state_reg[1]_0\(1),
      I1 => \^current_state_reg[1]_0\(0),
      O => \caracter[6]_i_32_n_0\
    );
clr_dinero_r_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => '1',
      Q => clr_dinero_r
    );
\current_state_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => next_state(0),
      Q => \^current_state_reg[1]_0\(0)
    );
\current_state_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_BUFG,
      CE => '1',
      CLR => output0,
      D => next_state(1),
      Q => \^current_state_reg[1]_0\(1)
    );
\next_state_reg[0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \current_state_reg[1]_9\(0),
      G => Inst_gestion_dinero_n_22,
      GE => '1',
      Q => next_state(0)
    );
\next_state_reg[1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \current_state_reg[1]_9\(1),
      G => Inst_gestion_dinero_n_22,
      GE => '1',
      Q => next_state(1)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity top is
  port (
    clk_100Mh : in STD_LOGIC;
    monedas : in STD_LOGIC_VECTOR ( 4 downto 0 );
    ref_option : in STD_LOGIC_VECTOR ( 3 downto 0 );
    button_ok : in STD_LOGIC;
    reset : in STD_LOGIC;
    devolver : out STD_LOGIC;
    refresco : out STD_LOGIC;
    digit : out STD_LOGIC_VECTOR ( 6 downto 0 );
    dot : out STD_LOGIC;
    elem : out STD_LOGIC_VECTOR ( 7 downto 0 );
    letra : out STD_LOGIC_VECTOR ( 7 downto 0 );
    st : out STD_LOGIC_VECTOR ( 1 downto 0 );
    st_dinero : out STD_LOGIC_VECTOR ( 1 downto 0 );
    v_mon : out STD_LOGIC_VECTOR ( 7 downto 0 );
    edge_1_out : out STD_LOGIC_VECTOR ( 4 downto 0 );
    edge_2_out : out STD_LOGIC_VECTOR ( 3 downto 0 );
    edge_3_out : out STD_LOGIC;
    bdf_din : out STD_LOGIC;
    tot : out STD_LOGIC_VECTOR ( 10 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of top : entity is true;
  attribute digit_number : integer;
  attribute digit_number of top : entity is 8;
  attribute division_prescaler : integer;
  attribute division_prescaler of top : entity is 250000;
  attribute monedas_types : integer;
  attribute monedas_types of top : entity is 5;
  attribute num_refrescos : integer;
  attribute num_refrescos of top : entity is 4;
  attribute size_counter : integer;
  attribute size_counter of top : entity is 3;
  attribute size_prescaler : integer;
  attribute size_prescaler of top : entity is 17;
  attribute width_word : integer;
  attribute width_word of top : entity is 8;
end top;

architecture STRUCTURE of top is
  signal bdf_din_OBUF : STD_LOGIC;
  signal button_ok_IBUF : STD_LOGIC;
  signal caracter : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal caracter2 : STD_LOGIC_VECTOR ( 3 to 3 );
  signal clk : STD_LOGIC;
  signal clk_100Mh_IBUF : STD_LOGIC;
  signal clk_100Mh_IBUF_BUFG : STD_LOGIC;
  signal clk_BUFG : STD_LOGIC;
  signal devolver_OBUF : STD_LOGIC;
  signal digit_OBUF : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \disp_refresco[1]_0\ : STD_LOGIC_VECTOR ( 6 to 6 );
  signal \disp_refresco[5]_1\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal dot_OBUF : STD_LOGIC;
  signal elem_OBUF : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal inst_decodmonedas_n_0 : STD_LOGIC;
  signal inst_decodmonedas_n_1 : STD_LOGIC;
  signal inst_decodmonedas_n_9 : STD_LOGIC;
  signal inst_display_n_1 : STD_LOGIC;
  signal inst_display_n_10 : STD_LOGIC;
  signal inst_display_n_11 : STD_LOGIC;
  signal inst_display_n_12 : STD_LOGIC;
  signal inst_display_n_13 : STD_LOGIC;
  signal inst_display_n_14 : STD_LOGIC;
  signal inst_display_n_15 : STD_LOGIC;
  signal inst_display_n_16 : STD_LOGIC;
  signal inst_display_n_17 : STD_LOGIC;
  signal inst_display_n_2 : STD_LOGIC;
  signal inst_display_n_6 : STD_LOGIC;
  signal inst_display_n_7 : STD_LOGIC;
  signal inst_display_n_8 : STD_LOGIC;
  signal inst_display_n_9 : STD_LOGIC;
  signal inst_fsm_n_10 : STD_LOGIC;
  signal inst_fsm_n_11 : STD_LOGIC;
  signal inst_fsm_n_12 : STD_LOGIC;
  signal inst_fsm_n_13 : STD_LOGIC;
  signal inst_fsm_n_14 : STD_LOGIC;
  signal inst_fsm_n_19 : STD_LOGIC;
  signal inst_fsm_n_20 : STD_LOGIC;
  signal inst_fsm_n_21 : STD_LOGIC;
  signal inst_fsm_n_22 : STD_LOGIC;
  signal inst_fsm_n_23 : STD_LOGIC;
  signal inst_fsm_n_6 : STD_LOGIC;
  signal inst_fsm_n_7 : STD_LOGIC;
  signal letra_OBUF : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal monedas_IBUF : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal output0 : STD_LOGIC;
  signal plusOp : STD_LOGIC_VECTOR ( 2 downto 1 );
  signal ref_option_IBUF : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal reg : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal reset_IBUF : STD_LOGIC;
  signal st_OBUF : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal st_dinero_OBUF : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal tot_OBUF : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal v_mon_OBUF : STD_LOGIC_VECTOR ( 6 downto 0 );
begin
bdf_din_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => bdf_din_OBUF,
      O => bdf_din
    );
button_ok_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => button_ok,
      O => button_ok_IBUF
    );
clk_100Mh_IBUF_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => clk_100Mh_IBUF,
      O => clk_100Mh_IBUF_BUFG
    );
clk_100Mh_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => clk_100Mh,
      O => clk_100Mh_IBUF
    );
clk_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => clk,
      O => clk_BUFG
    );
devolver_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => devolver_OBUF,
      O => devolver
    );
\digit_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digit_OBUF(0),
      O => digit(0)
    );
\digit_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digit_OBUF(1),
      O => digit(1)
    );
\digit_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digit_OBUF(2),
      O => digit(2)
    );
\digit_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digit_OBUF(3),
      O => digit(3)
    );
\digit_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digit_OBUF(4),
      O => digit(4)
    );
\digit_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digit_OBUF(5),
      O => digit(5)
    );
\digit_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => digit_OBUF(6),
      O => digit(6)
    );
dot_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => dot_OBUF,
      O => dot
    );
\edge_1_out_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => edge_1_out(0)
    );
\edge_1_out_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => edge_1_out(1)
    );
\edge_1_out_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => edge_1_out(2)
    );
\edge_1_out_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => edge_1_out(3)
    );
\edge_1_out_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => edge_1_out(4)
    );
\edge_2_out_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => edge_2_out(0)
    );
\edge_2_out_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => edge_2_out(1)
    );
\edge_2_out_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => edge_2_out(2)
    );
\edge_2_out_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => edge_2_out(3)
    );
edge_3_out_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => edge_3_out
    );
\elem_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => elem_OBUF(0),
      O => elem(0)
    );
\elem_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => elem_OBUF(1),
      O => elem(1)
    );
\elem_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => elem_OBUF(2),
      O => elem(2)
    );
\elem_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => elem_OBUF(3),
      O => elem(3)
    );
\elem_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => elem_OBUF(4),
      O => elem(4)
    );
\elem_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => elem_OBUF(5),
      O => elem(5)
    );
\elem_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => elem_OBUF(6),
      O => elem(6)
    );
\elem_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => elem_OBUF(7),
      O => elem(7)
    );
inst_decodmonedas: entity work.decod_monedas
     port map (
      D(1) => inst_decodmonedas_n_0,
      D(0) => inst_decodmonedas_n_1,
      Q(6 downto 0) => v_mon_OBUF(6 downto 0),
      clk_BUFG => clk_BUFG,
      \current_state_reg[0]\ => inst_decodmonedas_n_9,
      \current_state_reg[1]\(1 downto 0) => st_OBUF(1 downto 0),
      monedas_IBUF(4 downto 0) => monedas_IBUF(4 downto 0)
    );
inst_display: entity work.display
     port map (
      CO(0) => caracter2(3),
      D(3 downto 0) => caracter(3 downto 0),
      Q(2 downto 0) => reg(2 downto 0),
      \caracter[0]_i_5\ => inst_fsm_n_13,
      \caracter[2]_i_6\(1 downto 0) => st_OBUF(1 downto 0),
      \caracter[6]_i_4\ => inst_fsm_n_7,
      \caracter_reg[4]_0\ => inst_fsm_n_23,
      \caracter_reg[4]_1\ => inst_fsm_n_19,
      \caracter_reg[4]_2\ => inst_fsm_n_21,
      \caracter_reg[4]_3\ => inst_fsm_n_6,
      \caracter_reg[4]_4\ => inst_fsm_n_22,
      \caracter_reg[4]_5\ => inst_fsm_n_20,
      \caracter_reg[6]_0\(5) => letra_OBUF(6),
      \caracter_reg[6]_0\(4 downto 0) => letra_OBUF(4 downto 0),
      \caracter_reg[6]_1\ => inst_fsm_n_12,
      \caracter_reg[6]_2\ => inst_fsm_n_14,
      \caracter_reg[6]_3\ => inst_fsm_n_11,
      \caracter_reg[6]_4\ => inst_fsm_n_10,
      \charact_dot_reg[0]_0\ => inst_display_n_1,
      \charact_dot_reg[0]_1\ => inst_display_n_6,
      \charact_dot_reg[0]_2\ => inst_display_n_8,
      \charact_dot_reg[0]_3\ => inst_display_n_11,
      clk_BUFG => clk_BUFG,
      digit_OBUF(6 downto 0) => digit_OBUF(6 downto 0),
      \disp_refresco[1]_0\(0) => \disp_refresco[1]_0\(6),
      \disp_refresco[5]_1\(0) => \disp_refresco[5]_1\(2),
      dot_OBUF => dot_OBUF,
      elem_OBUF(7 downto 0) => elem_OBUF(7 downto 0),
      output0 => output0,
      plusOp(1 downto 0) => plusOp(2 downto 1),
      \reg_reg[0]\ => inst_display_n_10,
      \reg_reg[0]_0\ => inst_display_n_16,
      \reg_reg[1]\ => inst_display_n_9,
      \reg_reg[1]_0\ => inst_display_n_14,
      \reg_reg[1]_1\ => inst_display_n_15,
      \reg_reg[2]\ => inst_display_n_2,
      \reg_reg[2]_0\ => inst_display_n_7,
      \reg_reg[2]_1\ => inst_display_n_12,
      \reg_reg[2]_2\ => inst_display_n_13,
      \reg_reg[2]_3\ => inst_display_n_17,
      reset_IBUF => reset_IBUF
    );
inst_fsm: entity work.fsm
     port map (
      CO(0) => caracter2(3),
      D(3 downto 0) => caracter(3 downto 0),
      \FSM_onehot_current_state_reg[3]\(1) => devolver_OBUF,
      \FSM_onehot_current_state_reg[3]\(0) => bdf_din_OBUF,
      Q(2 downto 0) => reg(2 downto 0),
      button_ok_IBUF => button_ok_IBUF,
      \caracter[2]_i_11\ => inst_display_n_1,
      \caracter[2]_i_2\ => inst_display_n_12,
      \caracter[2]_i_3\ => inst_display_n_10,
      \caracter[3]_i_3\ => inst_display_n_9,
      \caracter[6]_i_21\ => inst_display_n_15,
      \caracter[6]_i_36\ => inst_display_n_16,
      \caracter_reg[0]\ => inst_display_n_6,
      \caracter_reg[0]_0\ => inst_display_n_14,
      \caracter_reg[1]\ => inst_display_n_17,
      \caracter_reg[1]_0\ => inst_display_n_13,
      \caracter_reg[2]\ => inst_display_n_2,
      \caracter_reg[2]_0\ => inst_display_n_7,
      \caracter_reg[3]\ => inst_display_n_8,
      \caracter_reg[3]_0\ => inst_display_n_11,
      clk_BUFG => clk_BUFG,
      \current_state_reg[0]_0\ => inst_fsm_n_13,
      \current_state_reg[0]_1\ => inst_decodmonedas_n_9,
      \current_state_reg[1]_0\(1 downto 0) => st_OBUF(1 downto 0),
      \current_state_reg[1]_1\ => inst_fsm_n_10,
      \current_state_reg[1]_2\ => inst_fsm_n_11,
      \current_state_reg[1]_3\ => inst_fsm_n_12,
      \current_state_reg[1]_4\ => inst_fsm_n_14,
      \current_state_reg[1]_5\ => inst_fsm_n_19,
      \current_state_reg[1]_6\ => inst_fsm_n_20,
      \current_state_reg[1]_7\ => inst_fsm_n_21,
      \current_state_reg[1]_8\ => inst_fsm_n_23,
      \current_state_reg[1]_9\(1) => inst_decodmonedas_n_0,
      \current_state_reg[1]_9\(0) => inst_decodmonedas_n_1,
      \disp_aux_reg[6][3]_i_2\(0) => \disp_refresco[5]_1\(2),
      \disp_aux_reg[6][3]_i_2_0\(0) => \disp_refresco[1]_0\(6),
      output0 => output0,
      plusOp(1 downto 0) => plusOp(2 downto 1),
      ref_option_IBUF(3 downto 0) => ref_option_IBUF(3 downto 0),
      \reg_reg[0]\ => inst_fsm_n_7,
      \reg_reg[1]\ => inst_fsm_n_22,
      \reg_reg[2]\ => inst_fsm_n_6,
      st_dinero_OBUF(1 downto 0) => st_dinero_OBUF(1 downto 0),
      \tot_reg[7]\(7 downto 0) => tot_OBUF(7 downto 0),
      \total_reg[7]\(6 downto 0) => v_mon_OBUF(6 downto 0)
    );
inst_prescaler: entity work.prescaler
     port map (
      clk => clk,
      clk_100Mh_IBUF_BUFG => clk_100Mh_IBUF_BUFG,
      output0 => output0,
      reset_IBUF => reset_IBUF
    );
\letra_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => letra_OBUF(0),
      O => letra(0)
    );
\letra_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => letra_OBUF(1),
      O => letra(1)
    );
\letra_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => letra_OBUF(2),
      O => letra(2)
    );
\letra_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => letra_OBUF(3),
      O => letra(3)
    );
\letra_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => letra_OBUF(4),
      O => letra(4)
    );
\letra_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => letra(5)
    );
\letra_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => letra_OBUF(6),
      O => letra(6)
    );
\letra_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => letra(7)
    );
\monedas_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => monedas(0),
      O => monedas_IBUF(0)
    );
\monedas_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => monedas(1),
      O => monedas_IBUF(1)
    );
\monedas_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => monedas(2),
      O => monedas_IBUF(2)
    );
\monedas_IBUF[3]_inst\: unisim.vcomponents.IBUF
     port map (
      I => monedas(3),
      O => monedas_IBUF(3)
    );
\monedas_IBUF[4]_inst\: unisim.vcomponents.IBUF
     port map (
      I => monedas(4),
      O => monedas_IBUF(4)
    );
\ref_option_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ref_option(0),
      O => ref_option_IBUF(0)
    );
\ref_option_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ref_option(1),
      O => ref_option_IBUF(1)
    );
\ref_option_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ref_option(2),
      O => ref_option_IBUF(2)
    );
\ref_option_IBUF[3]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ref_option(3),
      O => ref_option_IBUF(3)
    );
refresco_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => st_dinero_OBUF(0),
      O => refresco
    );
reset_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => reset,
      O => reset_IBUF
    );
\st_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => st_OBUF(0),
      O => st(0)
    );
\st_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => st_OBUF(1),
      O => st(1)
    );
\st_dinero_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => st_dinero_OBUF(0),
      O => st_dinero(0)
    );
\st_dinero_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => st_dinero_OBUF(1),
      O => st_dinero(1)
    );
\tot_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_OBUF(0),
      O => tot(0)
    );
\tot_OBUF[10]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => tot(10)
    );
\tot_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_OBUF(1),
      O => tot(1)
    );
\tot_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_OBUF(2),
      O => tot(2)
    );
\tot_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_OBUF(3),
      O => tot(3)
    );
\tot_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_OBUF(4),
      O => tot(4)
    );
\tot_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_OBUF(5),
      O => tot(5)
    );
\tot_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_OBUF(6),
      O => tot(6)
    );
\tot_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_OBUF(7),
      O => tot(7)
    );
\tot_OBUF[8]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => tot(8)
    );
\tot_OBUF[9]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => tot(9)
    );
\v_mon_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => v_mon_OBUF(0),
      O => v_mon(0)
    );
\v_mon_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => v_mon_OBUF(1),
      O => v_mon(1)
    );
\v_mon_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => v_mon_OBUF(2),
      O => v_mon(2)
    );
\v_mon_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => v_mon_OBUF(3),
      O => v_mon(3)
    );
\v_mon_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => v_mon_OBUF(4),
      O => v_mon(4)
    );
\v_mon_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => v_mon_OBUF(5),
      O => v_mon(5)
    );
\v_mon_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => v_mon_OBUF(6),
      O => v_mon(6)
    );
\v_mon_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => v_mon(7)
    );
end STRUCTURE;
