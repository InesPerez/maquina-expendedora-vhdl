-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Sat Jan  2 16:07:59 2021
-- Host        : LAPTOP-7BL7BHFF running 64-bit major release  (build 9200)
-- Command     : write_vhdl -mode funcsim -nolib -force -file
--               C:/Users/Inees/Desktop/Trabajo_SED/MaquinaExpendedora_aux/MaquinaExpendedora_aux.sim/sim_1/synth/func/xsim/gestion_dinero_tb_func_synth.vhd
-- Design      : fsm
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tcsg324-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gestion_refresco is
  port (
    \disp_refresco[5]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_aux_reg[6][3]_i_2_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_aux_reg[6][3]_i_2_1\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_aux_reg[6][3]_i_2_2\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_refresco[7]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \disp[5]_OBUF\ : out STD_LOGIC_VECTOR ( 4 downto 0 );
    \disp[1]_OBUF\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \disp[4]_OBUF\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \disp[2]_OBUF\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \disp[3]_OBUF\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \FSM_sequential_current_state_reg[1]\ : out STD_LOGIC;
    \disp[6]_OBUF\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \disp[7]_OBUF\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \disp[8]_OBUF\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \disp[5][4]\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    ref_option_IBUF : in STD_LOGIC_VECTOR ( 3 downto 0 );
    O : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end gestion_refresco;

architecture STRUCTURE of gestion_refresco is
  signal \^e\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \disp_aux_reg[2][0]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[2][1]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[2][4]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[3][0]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[3][1]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[3][6]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[4][1]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[4][4]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[4][6]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[5][0]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[6][1]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[6][2]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[6][3]_i_1_n_0\ : STD_LOGIC;
  signal \^disp_aux_reg[6][3]_i_2_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^disp_aux_reg[6][3]_i_2_1\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^disp_aux_reg[6][3]_i_2_2\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \disp_aux_reg[6][6]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[7][3]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[8][0]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[8][1]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[8][2]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[8][3]_i_1_n_0\ : STD_LOGIC;
  signal \disp_aux_reg[8][6]_i_1_n_0\ : STD_LOGIC;
  signal \disp_refresco[2]\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \disp_refresco[3]\ : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \disp_refresco[4]\ : STD_LOGIC_VECTOR ( 4 downto 1 );
  signal \disp_refresco[6]\ : STD_LOGIC_VECTOR ( 6 downto 1 );
  signal \^disp_refresco[7]\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \disp_refresco[8]\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \disp[1][1]_INST_0_i_1\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \disp[1][2]_INST_0_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \disp[1][3]_INST_0_i_1\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \disp[1][6]_INST_0_i_1\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \disp[2][0]_INST_0_i_1\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \disp[2][4]_INST_0_i_1\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \disp[3][1]_INST_0_i_2\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \disp[3][4]_INST_0_i_1\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \disp[3][6]_INST_0_i_1\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \disp[4][0]_INST_0_i_1\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \disp[4][1]_INST_0_i_1\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \disp[4][3]_INST_0_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \disp[4][4]_INST_0_i_1\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \disp[4][6]_INST_0_i_1\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \disp[5][1]_INST_0_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \disp[5][2]_INST_0_i_1\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \disp[5][3]_INST_0_i_1\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \disp[5][4]_INST_0_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \disp[6][1]_INST_0_i_1\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \disp[6][3]_INST_0_i_1\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \disp[6][6]_INST_0_i_1\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \disp[7][2]_INST_0_i_1\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \disp[7][3]_INST_0_i_1\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \disp[8][0]_INST_0_i_1\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \disp[8][1]_INST_0_i_1\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \disp[8][2]_INST_0_i_1\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \disp[8][3]_INST_0_i_1\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \disp[8][6]_INST_0_i_1\ : label is "soft_lutpair47";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[2][0]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[2][0]_i_1\ : label is "soft_lutpair40";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[2][1]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[2][1]_i_1\ : label is "soft_lutpair36";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[2][4]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[2][4]_i_1\ : label is "soft_lutpair42";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[3][0]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[3][0]_i_1\ : label is "soft_lutpair38";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[3][1]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[3][1]_i_1\ : label is "soft_lutpair39";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[3][6]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[3][6]_i_1\ : label is "soft_lutpair37";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[4][1]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[4][1]_i_1\ : label is "soft_lutpair41";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[4][4]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[4][4]_i_1\ : label is "soft_lutpair42";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[4][6]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[4][6]_i_1\ : label is "soft_lutpair35";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[5][0]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[5][0]_i_1\ : label is "soft_lutpair37";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[6][1]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[6][2]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[6][2]_i_1\ : label is "soft_lutpair33";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[6][3]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[6][3]_i_1\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \disp_aux_reg[6][3]_i_2\ : label is "soft_lutpair39";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[6][6]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[6][6]_i_1\ : label is "soft_lutpair35";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[7][3]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[7][3]_i_1\ : label is "soft_lutpair33";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[8][0]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[8][0]_i_1\ : label is "soft_lutpair43";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[8][1]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[8][1]_i_1\ : label is "soft_lutpair40";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[8][2]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[8][2]_i_1\ : label is "soft_lutpair43";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[8][3]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[8][3]_i_1\ : label is "soft_lutpair38";
  attribute XILINX_LEGACY_PRIM of \disp_aux_reg[8][6]\ : label is "LD";
  attribute SOFT_HLUTNM of \disp_aux_reg[8][6]_i_1\ : label is "soft_lutpair36";
begin
  E(0) <= \^e\(0);
  \disp_aux_reg[6][3]_i_2_0\(0) <= \^disp_aux_reg[6][3]_i_2_0\(0);
  \disp_aux_reg[6][3]_i_2_1\(1 downto 0) <= \^disp_aux_reg[6][3]_i_2_1\(1 downto 0);
  \disp_aux_reg[6][3]_i_2_2\(0) <= \^disp_aux_reg[6][3]_i_2_2\(0);
  \disp_refresco[7]\(0) <= \^disp_refresco[7]\(0);
\disp[1][1]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BBF3"
    )
        port map (
      I0 => \disp[5][4]\,
      I1 => Q(0),
      I2 => \disp_refresco[2]\(1),
      I3 => Q(1),
      O => \disp[1]_OBUF\(1)
    );
\disp[1][2]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F75"
    )
        port map (
      I0 => Q(0),
      I1 => \disp[5][4]\,
      I2 => Q(1),
      I3 => \disp_refresco[8]\(2),
      O => \disp[1]_OBUF\(2)
    );
\disp[1][3]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => Q(1),
      I1 => \disp_refresco[2]\(1),
      I2 => Q(0),
      O => \disp[1]_OBUF\(3)
    );
\disp[1][6]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FD"
    )
        port map (
      I0 => Q(0),
      I1 => Q(1),
      I2 => \^disp_aux_reg[6][3]_i_2_1\(1),
      O => \disp[1]_OBUF\(0)
    );
\disp[2][0]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"C0AF"
    )
        port map (
      I0 => \disp_refresco[2]\(0),
      I1 => O(0),
      I2 => Q(0),
      I3 => Q(1),
      O => \disp[2]_OBUF\(0)
    );
\disp[2][4]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F8"
    )
        port map (
      I0 => Q(0),
      I1 => \disp_refresco[2]\(4),
      I2 => Q(1),
      O => \disp[2]_OBUF\(1)
    );
\disp[3][1]_INST_0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"23"
    )
        port map (
      I0 => \disp_refresco[3]\(1),
      I1 => Q(1),
      I2 => Q(0),
      O => \FSM_sequential_current_state_reg[1]\
    );
\disp[3][4]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B9"
    )
        port map (
      I0 => Q(0),
      I1 => Q(1),
      I2 => \disp_refresco[4]\(4),
      O => \disp[3]_OBUF\(0)
    );
\disp[3][6]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => Q(1),
      I1 => \^disp_aux_reg[6][3]_i_2_1\(1),
      I2 => Q(0),
      O => \disp[3]_OBUF\(1)
    );
\disp[4][0]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"23"
    )
        port map (
      I0 => \^disp_aux_reg[6][3]_i_2_0\(0),
      I1 => Q(1),
      I2 => Q(0),
      O => \disp[4]_OBUF\(0)
    );
\disp[4][1]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7C4C"
    )
        port map (
      I0 => \disp[5][4]\,
      I1 => Q(1),
      I2 => Q(0),
      I3 => \disp_refresco[4]\(1),
      O => \disp[4]_OBUF\(1)
    );
\disp[4][3]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7C4C"
    )
        port map (
      I0 => \disp[5][4]\,
      I1 => Q(1),
      I2 => Q(0),
      I3 => \disp_refresco[8]\(3),
      O => \disp[4]_OBUF\(2)
    );
\disp[4][4]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \disp_refresco[4]\(4),
      I1 => Q(0),
      I2 => Q(1),
      O => \disp[4]_OBUF\(3)
    );
\disp[4][6]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => Q(1),
      I1 => \^disp_aux_reg[6][3]_i_2_0\(0),
      I2 => Q(0),
      O => \disp[2]_OBUF\(2)
    );
\disp[5][1]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"23"
    )
        port map (
      I0 => \disp_refresco[8]\(3),
      I1 => Q(1),
      I2 => Q(0),
      O => \disp[5]_OBUF\(0)
    );
\disp[5][2]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \disp_refresco[6]\(2),
      I1 => Q(0),
      I2 => Q(1),
      O => \disp[5]_OBUF\(1)
    );
\disp[5][3]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"38"
    )
        port map (
      I0 => \disp_refresco[8]\(3),
      I1 => Q(0),
      I2 => Q(1),
      O => \disp[5]_OBUF\(2)
    );
\disp[5][4]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"30AF"
    )
        port map (
      I0 => \disp_refresco[8]\(2),
      I1 => \disp[5][4]\,
      I2 => Q(0),
      I3 => Q(1),
      O => \disp[5]_OBUF\(3)
    );
\disp[6][1]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"23"
    )
        port map (
      I0 => \disp_refresco[6]\(1),
      I1 => Q(1),
      I2 => Q(0),
      O => \disp[6]_OBUF\(1)
    );
\disp[6][2]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => Q(1),
      I1 => \disp_refresco[6]\(2),
      I2 => Q(0),
      O => \disp[6]_OBUF\(2)
    );
\disp[6][3]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \disp_refresco[6]\(3),
      I1 => Q(0),
      I2 => Q(1),
      O => \disp[6]_OBUF\(3)
    );
\disp[6][6]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => Q(1),
      I1 => \disp_refresco[6]\(6),
      I2 => Q(0),
      O => \disp[5]_OBUF\(4)
    );
\disp[7][2]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"23"
    )
        port map (
      I0 => \disp_refresco[8]\(0),
      I1 => Q(1),
      I2 => Q(0),
      O => \disp[7]_OBUF\(2)
    );
\disp[7][3]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^disp_refresco[7]\(0),
      I1 => Q(0),
      I2 => Q(1),
      O => \disp[7]_OBUF\(1)
    );
\disp[8][0]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \disp_refresco[8]\(0),
      I1 => Q(0),
      I2 => Q(1),
      O => \disp[6]_OBUF\(0)
    );
\disp[8][1]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"23"
    )
        port map (
      I0 => \^disp_aux_reg[6][3]_i_2_2\(0),
      I1 => Q(1),
      I2 => Q(0),
      O => \disp[8]_OBUF\(0)
    );
\disp[8][2]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \disp_refresco[8]\(2),
      I1 => Q(0),
      I2 => Q(1),
      O => \disp[8]_OBUF\(1)
    );
\disp[8][3]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \disp_refresco[8]\(3),
      I1 => Q(0),
      I2 => Q(1),
      O => \disp[8]_OBUF\(2)
    );
\disp[8][6]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"23"
    )
        port map (
      I0 => \disp_refresco[8]\(6),
      I1 => Q(1),
      I2 => Q(0),
      O => \disp[7]_OBUF\(0)
    );
\disp_aux_reg[2][0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[2][0]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \disp_refresco[2]\(0)
    );
\disp_aux_reg[2][0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0007"
    )
        port map (
      I0 => ref_option_IBUF(0),
      I1 => ref_option_IBUF(2),
      I2 => ref_option_IBUF(1),
      I3 => ref_option_IBUF(3),
      O => \disp_aux_reg[2][0]_i_1_n_0\
    );
\disp_aux_reg[2][1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[2][1]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \disp_refresco[2]\(1)
    );
\disp_aux_reg[2][1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEEA"
    )
        port map (
      I0 => ref_option_IBUF(1),
      I1 => ref_option_IBUF(3),
      I2 => ref_option_IBUF(2),
      I3 => ref_option_IBUF(0),
      O => \disp_aux_reg[2][1]_i_1_n_0\
    );
\disp_aux_reg[2][4]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[2][4]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \disp_refresco[2]\(4)
    );
\disp_aux_reg[2][4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => ref_option_IBUF(2),
      I1 => ref_option_IBUF(1),
      I2 => ref_option_IBUF(3),
      O => \disp_aux_reg[2][4]_i_1_n_0\
    );
\disp_aux_reg[3][0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[3][0]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \^disp_aux_reg[6][3]_i_2_1\(0)
    );
\disp_aux_reg[3][0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0103"
    )
        port map (
      I0 => ref_option_IBUF(0),
      I1 => ref_option_IBUF(2),
      I2 => ref_option_IBUF(1),
      I3 => ref_option_IBUF(3),
      O => \disp_aux_reg[3][0]_i_1_n_0\
    );
\disp_aux_reg[3][1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[3][1]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \disp_refresco[3]\(1)
    );
\disp_aux_reg[3][1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEEE"
    )
        port map (
      I0 => ref_option_IBUF(3),
      I1 => ref_option_IBUF(0),
      I2 => ref_option_IBUF(2),
      I3 => ref_option_IBUF(1),
      O => \disp_aux_reg[3][1]_i_1_n_0\
    );
\disp_aux_reg[3][6]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[3][6]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \^disp_aux_reg[6][3]_i_2_1\(1)
    );
\disp_aux_reg[3][6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0107"
    )
        port map (
      I0 => ref_option_IBUF(0),
      I1 => ref_option_IBUF(2),
      I2 => ref_option_IBUF(1),
      I3 => ref_option_IBUF(3),
      O => \disp_aux_reg[3][6]_i_1_n_0\
    );
\disp_aux_reg[4][1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[4][1]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \disp_refresco[4]\(1)
    );
\disp_aux_reg[4][1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEEE"
    )
        port map (
      I0 => ref_option_IBUF(3),
      I1 => ref_option_IBUF(2),
      I2 => ref_option_IBUF(1),
      I3 => ref_option_IBUF(0),
      O => \disp_aux_reg[4][1]_i_1_n_0\
    );
\disp_aux_reg[4][4]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[4][4]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \disp_refresco[4]\(4)
    );
\disp_aux_reg[4][4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => ref_option_IBUF(3),
      I1 => ref_option_IBUF(1),
      I2 => ref_option_IBUF(0),
      O => \disp_aux_reg[4][4]_i_1_n_0\
    );
\disp_aux_reg[4][6]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[4][6]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \^disp_aux_reg[6][3]_i_2_0\(0)
    );
\disp_aux_reg[4][6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0117"
    )
        port map (
      I0 => ref_option_IBUF(0),
      I1 => ref_option_IBUF(2),
      I2 => ref_option_IBUF(1),
      I3 => ref_option_IBUF(3),
      O => \disp_aux_reg[4][6]_i_1_n_0\
    );
\disp_aux_reg[5][0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[5][0]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \disp_refresco[5]\(0)
    );
\disp_aux_reg[5][0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0103"
    )
        port map (
      I0 => ref_option_IBUF(2),
      I1 => ref_option_IBUF(0),
      I2 => ref_option_IBUF(1),
      I3 => ref_option_IBUF(3),
      O => \disp_aux_reg[5][0]_i_1_n_0\
    );
\disp_aux_reg[6][1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[6][1]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \disp_refresco[6]\(1)
    );
\disp_aux_reg[6][1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => ref_option_IBUF(3),
      I1 => ref_option_IBUF(1),
      I2 => ref_option_IBUF(2),
      O => \disp_aux_reg[6][1]_i_1_n_0\
    );
\disp_aux_reg[6][2]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[6][2]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \disp_refresco[6]\(2)
    );
\disp_aux_reg[6][2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0013"
    )
        port map (
      I0 => ref_option_IBUF(2),
      I1 => ref_option_IBUF(0),
      I2 => ref_option_IBUF(1),
      I3 => ref_option_IBUF(3),
      O => \disp_aux_reg[6][2]_i_1_n_0\
    );
\disp_aux_reg[6][3]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[6][3]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \disp_refresco[6]\(3)
    );
\disp_aux_reg[6][3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEEE"
    )
        port map (
      I0 => ref_option_IBUF(2),
      I1 => ref_option_IBUF(1),
      I2 => ref_option_IBUF(3),
      I3 => ref_option_IBUF(0),
      O => \disp_aux_reg[6][3]_i_1_n_0\
    );
\disp_aux_reg[6][3]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => ref_option_IBUF(3),
      I1 => ref_option_IBUF(1),
      I2 => ref_option_IBUF(2),
      I3 => ref_option_IBUF(0),
      O => \^e\(0)
    );
\disp_aux_reg[6][6]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[6][6]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \disp_refresco[6]\(6)
    );
\disp_aux_reg[6][6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0113"
    )
        port map (
      I0 => ref_option_IBUF(2),
      I1 => ref_option_IBUF(0),
      I2 => ref_option_IBUF(1),
      I3 => ref_option_IBUF(3),
      O => \disp_aux_reg[6][6]_i_1_n_0\
    );
\disp_aux_reg[7][3]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[7][3]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \^disp_refresco[7]\(0)
    );
\disp_aux_reg[7][3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEEA"
    )
        port map (
      I0 => ref_option_IBUF(3),
      I1 => ref_option_IBUF(1),
      I2 => ref_option_IBUF(0),
      I3 => ref_option_IBUF(2),
      O => \disp_aux_reg[7][3]_i_1_n_0\
    );
\disp_aux_reg[8][0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[8][0]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \disp_refresco[8]\(0)
    );
\disp_aux_reg[8][0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => ref_option_IBUF(2),
      I1 => ref_option_IBUF(0),
      I2 => ref_option_IBUF(1),
      O => \disp_aux_reg[8][0]_i_1_n_0\
    );
\disp_aux_reg[8][1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[8][1]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \^disp_aux_reg[6][3]_i_2_2\(0)
    );
\disp_aux_reg[8][1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEEE"
    )
        port map (
      I0 => ref_option_IBUF(3),
      I1 => ref_option_IBUF(1),
      I2 => ref_option_IBUF(2),
      I3 => ref_option_IBUF(0),
      O => \disp_aux_reg[8][1]_i_1_n_0\
    );
\disp_aux_reg[8][2]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[8][2]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \disp_refresco[8]\(2)
    );
\disp_aux_reg[8][2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => ref_option_IBUF(2),
      I1 => ref_option_IBUF(0),
      I2 => ref_option_IBUF(3),
      O => \disp_aux_reg[8][2]_i_1_n_0\
    );
\disp_aux_reg[8][3]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[8][3]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \disp_refresco[8]\(3)
    );
\disp_aux_reg[8][3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEE8"
    )
        port map (
      I0 => ref_option_IBUF(0),
      I1 => ref_option_IBUF(2),
      I2 => ref_option_IBUF(3),
      I3 => ref_option_IBUF(1),
      O => \disp_aux_reg[8][3]_i_1_n_0\
    );
\disp_aux_reg[8][6]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \disp_aux_reg[8][6]_i_1_n_0\,
      G => \^e\(0),
      GE => '1',
      Q => \disp_refresco[8]\(6)
    );
\disp_aux_reg[8][6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0007"
    )
        port map (
      I0 => ref_option_IBUF(3),
      I1 => ref_option_IBUF(1),
      I2 => ref_option_IBUF(2),
      I3 => ref_option_IBUF(0),
      O => \disp_aux_reg[8][6]_i_1_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity int_to_string is
  port (
    \disp[5]_OBUF\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    O23 : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \string_cent_decenas[1]5_carry_i_10_0\ : out STD_LOGIC;
    \disp[3]_OBUF\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \disp[2]_OBUF\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    O : out STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_refresco[5]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_refresco[3]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_refresco[7]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp[3][1]\ : in STD_LOGIC;
    \disp[3][1]_0\ : in STD_LOGIC;
    \disp[1]_OBUF\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_refresco[4]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_refresco[8]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    valor_moneda_IBUF : in STD_LOGIC_VECTOR ( 22 downto 0 );
    v_mon_OBUF : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
end int_to_string;

architecture STRUCTURE of int_to_string is
  signal \^o\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^o23\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal cent : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \disp[2][2]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \disp[3][0]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \disp[3][0]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \disp[3][1]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_10_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_11_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_12_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_13_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_14_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_15_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_16_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_17_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_18_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_19_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_20_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_21_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_22_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_23_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_24_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_7_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_8_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_9_n_0\ : STD_LOGIC;
  signal \disp_dinero[2]\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \euros1__169_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__0_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__0_n_1\ : STD_LOGIC;
  signal \euros1__169_carry__0_n_2\ : STD_LOGIC;
  signal \euros1__169_carry__0_n_3\ : STD_LOGIC;
  signal \euros1__169_carry__0_n_4\ : STD_LOGIC;
  signal \euros1__169_carry__0_n_5\ : STD_LOGIC;
  signal \euros1__169_carry__0_n_6\ : STD_LOGIC;
  signal \euros1__169_carry__0_n_7\ : STD_LOGIC;
  signal \euros1__169_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__1_i_1_n_1\ : STD_LOGIC;
  signal \euros1__169_carry__1_i_1_n_2\ : STD_LOGIC;
  signal \euros1__169_carry__1_i_1_n_3\ : STD_LOGIC;
  signal \euros1__169_carry__1_i_1_n_4\ : STD_LOGIC;
  signal \euros1__169_carry__1_i_1_n_5\ : STD_LOGIC;
  signal \euros1__169_carry__1_i_1_n_6\ : STD_LOGIC;
  signal \euros1__169_carry__1_i_1_n_7\ : STD_LOGIC;
  signal \euros1__169_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__1_i_7_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__1_i_8_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__1_i_9_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__1_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__1_n_1\ : STD_LOGIC;
  signal \euros1__169_carry__1_n_2\ : STD_LOGIC;
  signal \euros1__169_carry__1_n_3\ : STD_LOGIC;
  signal \euros1__169_carry__1_n_4\ : STD_LOGIC;
  signal \euros1__169_carry__1_n_5\ : STD_LOGIC;
  signal \euros1__169_carry__1_n_6\ : STD_LOGIC;
  signal \euros1__169_carry__1_n_7\ : STD_LOGIC;
  signal \euros1__169_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__2_i_1_n_1\ : STD_LOGIC;
  signal \euros1__169_carry__2_i_1_n_2\ : STD_LOGIC;
  signal \euros1__169_carry__2_i_1_n_3\ : STD_LOGIC;
  signal \euros1__169_carry__2_i_1_n_4\ : STD_LOGIC;
  signal \euros1__169_carry__2_i_1_n_5\ : STD_LOGIC;
  signal \euros1__169_carry__2_i_1_n_6\ : STD_LOGIC;
  signal \euros1__169_carry__2_i_1_n_7\ : STD_LOGIC;
  signal \euros1__169_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__2_i_5_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__2_i_6_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__2_i_7_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__2_i_8_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__2_i_9_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__2_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__2_n_1\ : STD_LOGIC;
  signal \euros1__169_carry__2_n_2\ : STD_LOGIC;
  signal \euros1__169_carry__2_n_3\ : STD_LOGIC;
  signal \euros1__169_carry__2_n_4\ : STD_LOGIC;
  signal \euros1__169_carry__2_n_5\ : STD_LOGIC;
  signal \euros1__169_carry__2_n_6\ : STD_LOGIC;
  signal \euros1__169_carry__2_n_7\ : STD_LOGIC;
  signal \euros1__169_carry__3_i_1_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__3_i_1_n_1\ : STD_LOGIC;
  signal \euros1__169_carry__3_i_1_n_2\ : STD_LOGIC;
  signal \euros1__169_carry__3_i_1_n_3\ : STD_LOGIC;
  signal \euros1__169_carry__3_i_1_n_4\ : STD_LOGIC;
  signal \euros1__169_carry__3_i_1_n_5\ : STD_LOGIC;
  signal \euros1__169_carry__3_i_1_n_6\ : STD_LOGIC;
  signal \euros1__169_carry__3_i_1_n_7\ : STD_LOGIC;
  signal \euros1__169_carry__3_i_2_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__3_i_3_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__3_i_4_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__3_i_5_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__3_i_6_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__3_i_7_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__3_i_8_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__3_i_9_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__3_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__3_n_1\ : STD_LOGIC;
  signal \euros1__169_carry__3_n_2\ : STD_LOGIC;
  signal \euros1__169_carry__3_n_3\ : STD_LOGIC;
  signal \euros1__169_carry__3_n_4\ : STD_LOGIC;
  signal \euros1__169_carry__3_n_5\ : STD_LOGIC;
  signal \euros1__169_carry__3_n_6\ : STD_LOGIC;
  signal \euros1__169_carry__3_n_7\ : STD_LOGIC;
  signal \euros1__169_carry__4_i_1_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__4_i_2_n_0\ : STD_LOGIC;
  signal \euros1__169_carry__4_n_3\ : STD_LOGIC;
  signal \euros1__169_carry__4_n_6\ : STD_LOGIC;
  signal \euros1__169_carry__4_n_7\ : STD_LOGIC;
  signal \euros1__169_carry_i_1_n_0\ : STD_LOGIC;
  signal \euros1__169_carry_i_2_n_0\ : STD_LOGIC;
  signal \euros1__169_carry_i_3_n_0\ : STD_LOGIC;
  signal \euros1__169_carry_n_0\ : STD_LOGIC;
  signal \euros1__169_carry_n_1\ : STD_LOGIC;
  signal \euros1__169_carry_n_2\ : STD_LOGIC;
  signal \euros1__169_carry_n_3\ : STD_LOGIC;
  signal \euros1__169_carry_n_4\ : STD_LOGIC;
  signal \euros1__169_carry_n_5\ : STD_LOGIC;
  signal \euros1__169_carry_n_6\ : STD_LOGIC;
  signal \euros1__1_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__0_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__0_n_1\ : STD_LOGIC;
  signal \euros1__1_carry__0_n_2\ : STD_LOGIC;
  signal \euros1__1_carry__0_n_3\ : STD_LOGIC;
  signal \euros1__1_carry__0_n_4\ : STD_LOGIC;
  signal \euros1__1_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__1_i_7_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__1_i_8_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__1_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__1_n_1\ : STD_LOGIC;
  signal \euros1__1_carry__1_n_2\ : STD_LOGIC;
  signal \euros1__1_carry__1_n_3\ : STD_LOGIC;
  signal \euros1__1_carry__1_n_4\ : STD_LOGIC;
  signal \euros1__1_carry__1_n_5\ : STD_LOGIC;
  signal \euros1__1_carry__1_n_6\ : STD_LOGIC;
  signal \euros1__1_carry__1_n_7\ : STD_LOGIC;
  signal \euros1__1_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__2_i_5_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__2_i_6_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__2_i_7_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__2_i_8_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__2_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__2_n_1\ : STD_LOGIC;
  signal \euros1__1_carry__2_n_2\ : STD_LOGIC;
  signal \euros1__1_carry__2_n_3\ : STD_LOGIC;
  signal \euros1__1_carry__2_n_4\ : STD_LOGIC;
  signal \euros1__1_carry__2_n_5\ : STD_LOGIC;
  signal \euros1__1_carry__2_n_6\ : STD_LOGIC;
  signal \euros1__1_carry__2_n_7\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_10_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_11_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_12_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_13_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_1_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_2_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_3_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_4_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_5_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_6_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_7_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_8_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_9_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_9_n_1\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_9_n_2\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_9_n_3\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_9_n_4\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_9_n_5\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_9_n_6\ : STD_LOGIC;
  signal \euros1__1_carry__3_i_9_n_7\ : STD_LOGIC;
  signal \euros1__1_carry__3_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__3_n_1\ : STD_LOGIC;
  signal \euros1__1_carry__3_n_2\ : STD_LOGIC;
  signal \euros1__1_carry__3_n_3\ : STD_LOGIC;
  signal \euros1__1_carry__3_n_4\ : STD_LOGIC;
  signal \euros1__1_carry__3_n_5\ : STD_LOGIC;
  signal \euros1__1_carry__3_n_6\ : STD_LOGIC;
  signal \euros1__1_carry__3_n_7\ : STD_LOGIC;
  signal \euros1__1_carry__4_i_1_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__4_i_2_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__4_i_3_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__4_i_4_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__4_i_5_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__4_i_6_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__4_i_7_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__4_i_8_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__4_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__4_n_1\ : STD_LOGIC;
  signal \euros1__1_carry__4_n_2\ : STD_LOGIC;
  signal \euros1__1_carry__4_n_3\ : STD_LOGIC;
  signal \euros1__1_carry__4_n_4\ : STD_LOGIC;
  signal \euros1__1_carry__4_n_5\ : STD_LOGIC;
  signal \euros1__1_carry__4_n_6\ : STD_LOGIC;
  signal \euros1__1_carry__4_n_7\ : STD_LOGIC;
  signal \euros1__1_carry__5_i_10_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__5_i_11_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__5_i_12_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__5_i_1_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__5_i_2_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__5_i_3_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__5_i_4_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__5_i_5_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__5_i_6_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__5_i_7_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__5_i_8_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__5_i_9_n_2\ : STD_LOGIC;
  signal \euros1__1_carry__5_i_9_n_3\ : STD_LOGIC;
  signal \euros1__1_carry__5_i_9_n_5\ : STD_LOGIC;
  signal \euros1__1_carry__5_i_9_n_6\ : STD_LOGIC;
  signal \euros1__1_carry__5_i_9_n_7\ : STD_LOGIC;
  signal \euros1__1_carry__5_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__5_n_1\ : STD_LOGIC;
  signal \euros1__1_carry__5_n_2\ : STD_LOGIC;
  signal \euros1__1_carry__5_n_3\ : STD_LOGIC;
  signal \euros1__1_carry__5_n_4\ : STD_LOGIC;
  signal \euros1__1_carry__5_n_5\ : STD_LOGIC;
  signal \euros1__1_carry__5_n_6\ : STD_LOGIC;
  signal \euros1__1_carry__5_n_7\ : STD_LOGIC;
  signal \euros1__1_carry__6_i_1_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__6_i_2_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__6_i_3_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__6_i_4_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__6_i_5_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__6_i_6_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__6_i_7_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__6_i_8_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__6_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__6_n_1\ : STD_LOGIC;
  signal \euros1__1_carry__6_n_2\ : STD_LOGIC;
  signal \euros1__1_carry__6_n_3\ : STD_LOGIC;
  signal \euros1__1_carry__6_n_4\ : STD_LOGIC;
  signal \euros1__1_carry__6_n_5\ : STD_LOGIC;
  signal \euros1__1_carry__6_n_6\ : STD_LOGIC;
  signal \euros1__1_carry__6_n_7\ : STD_LOGIC;
  signal \euros1__1_carry__7_i_1_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__7_i_1_n_1\ : STD_LOGIC;
  signal \euros1__1_carry__7_i_1_n_2\ : STD_LOGIC;
  signal \euros1__1_carry__7_i_1_n_3\ : STD_LOGIC;
  signal \euros1__1_carry__7_i_1_n_4\ : STD_LOGIC;
  signal \euros1__1_carry__7_i_1_n_5\ : STD_LOGIC;
  signal \euros1__1_carry__7_i_1_n_6\ : STD_LOGIC;
  signal \euros1__1_carry__7_i_1_n_7\ : STD_LOGIC;
  signal \euros1__1_carry__7_i_2_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__7_i_3_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__7_i_4_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__7_i_5_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__7_i_6_n_0\ : STD_LOGIC;
  signal \euros1__1_carry__7_n_3\ : STD_LOGIC;
  signal \euros1__1_carry__7_n_6\ : STD_LOGIC;
  signal \euros1__1_carry__7_n_7\ : STD_LOGIC;
  signal \euros1__1_carry_i_1_n_0\ : STD_LOGIC;
  signal \euros1__1_carry_i_2_n_0\ : STD_LOGIC;
  signal \euros1__1_carry_i_3_n_0\ : STD_LOGIC;
  signal \euros1__1_carry_n_0\ : STD_LOGIC;
  signal \euros1__1_carry_n_1\ : STD_LOGIC;
  signal \euros1__1_carry_n_2\ : STD_LOGIC;
  signal \euros1__1_carry_n_3\ : STD_LOGIC;
  signal \euros1__231_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__0_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__0_n_1\ : STD_LOGIC;
  signal \euros1__231_carry__0_n_2\ : STD_LOGIC;
  signal \euros1__231_carry__0_n_3\ : STD_LOGIC;
  signal \euros1__231_carry__0_n_4\ : STD_LOGIC;
  signal \euros1__231_carry__0_n_5\ : STD_LOGIC;
  signal \euros1__231_carry__0_n_6\ : STD_LOGIC;
  signal \euros1__231_carry__0_n_7\ : STD_LOGIC;
  signal \euros1__231_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__1_i_7_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__1_i_8_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__1_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__1_n_1\ : STD_LOGIC;
  signal \euros1__231_carry__1_n_2\ : STD_LOGIC;
  signal \euros1__231_carry__1_n_3\ : STD_LOGIC;
  signal \euros1__231_carry__1_n_4\ : STD_LOGIC;
  signal \euros1__231_carry__1_n_5\ : STD_LOGIC;
  signal \euros1__231_carry__1_n_6\ : STD_LOGIC;
  signal \euros1__231_carry__1_n_7\ : STD_LOGIC;
  signal \euros1__231_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__2_i_5_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__2_i_6_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__2_i_7_n_0\ : STD_LOGIC;
  signal \euros1__231_carry__2_n_1\ : STD_LOGIC;
  signal \euros1__231_carry__2_n_2\ : STD_LOGIC;
  signal \euros1__231_carry__2_n_3\ : STD_LOGIC;
  signal \euros1__231_carry__2_n_4\ : STD_LOGIC;
  signal \euros1__231_carry__2_n_5\ : STD_LOGIC;
  signal \euros1__231_carry__2_n_6\ : STD_LOGIC;
  signal \euros1__231_carry__2_n_7\ : STD_LOGIC;
  signal \euros1__231_carry_i_1_n_0\ : STD_LOGIC;
  signal \euros1__231_carry_i_2_n_0\ : STD_LOGIC;
  signal \euros1__231_carry_i_3_n_0\ : STD_LOGIC;
  signal \euros1__231_carry_n_0\ : STD_LOGIC;
  signal \euros1__231_carry_n_1\ : STD_LOGIC;
  signal \euros1__231_carry_n_2\ : STD_LOGIC;
  signal \euros1__231_carry_n_3\ : STD_LOGIC;
  signal \euros1__231_carry_n_4\ : STD_LOGIC;
  signal \euros1__231_carry_n_5\ : STD_LOGIC;
  signal \euros1__231_carry_n_6\ : STD_LOGIC;
  signal \euros1__231_carry_n_7\ : STD_LOGIC;
  signal \euros1__276_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__0_i_9_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__0_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__0_n_1\ : STD_LOGIC;
  signal \euros1__276_carry__0_n_2\ : STD_LOGIC;
  signal \euros1__276_carry__0_n_3\ : STD_LOGIC;
  signal \euros1__276_carry__1_i_10_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__1_i_11_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__1_i_12_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__1_i_13_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__1_i_14_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__1_i_15_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__1_i_7_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__1_i_8_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__1_i_9_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__1_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__1_n_1\ : STD_LOGIC;
  signal \euros1__276_carry__1_n_2\ : STD_LOGIC;
  signal \euros1__276_carry__1_n_3\ : STD_LOGIC;
  signal \euros1__276_carry__2_i_10_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__2_i_11_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__2_i_12_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__2_i_13_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__2_i_14_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__2_i_15_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__2_i_5_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__2_i_6_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__2_i_7_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__2_i_8_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__2_i_9_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__2_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__2_n_1\ : STD_LOGIC;
  signal \euros1__276_carry__2_n_2\ : STD_LOGIC;
  signal \euros1__276_carry__2_n_3\ : STD_LOGIC;
  signal \euros1__276_carry__3_i_10_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_i_11_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_i_12_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_i_13_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_i_14_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_i_15_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_i_16_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_i_1_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_i_2_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_i_3_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_i_4_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_i_5_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_i_6_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_i_7_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_i_8_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_i_9_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__3_n_1\ : STD_LOGIC;
  signal \euros1__276_carry__3_n_2\ : STD_LOGIC;
  signal \euros1__276_carry__3_n_3\ : STD_LOGIC;
  signal \euros1__276_carry__4_i_10_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__4_i_11_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__4_i_12_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__4_i_13_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__4_i_14_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__4_i_15_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__4_i_16_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__4_i_1_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__4_i_2_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__4_i_3_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__4_i_4_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__4_i_5_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__4_i_6_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__4_i_7_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__4_i_8_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__4_i_9_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__4_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__4_n_1\ : STD_LOGIC;
  signal \euros1__276_carry__4_n_2\ : STD_LOGIC;
  signal \euros1__276_carry__4_n_3\ : STD_LOGIC;
  signal \euros1__276_carry__4_n_4\ : STD_LOGIC;
  signal \euros1__276_carry__4_n_5\ : STD_LOGIC;
  signal \euros1__276_carry__4_n_6\ : STD_LOGIC;
  signal \euros1__276_carry__4_n_7\ : STD_LOGIC;
  signal \euros1__276_carry__5_i_1_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__5_i_2_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__5_i_3_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__5_i_4_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__5_i_5_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__5_i_6_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__5_i_7_n_0\ : STD_LOGIC;
  signal \euros1__276_carry__5_n_3\ : STD_LOGIC;
  signal \euros1__276_carry__5_n_6\ : STD_LOGIC;
  signal \euros1__276_carry__5_n_7\ : STD_LOGIC;
  signal \euros1__276_carry_i_1_n_0\ : STD_LOGIC;
  signal \euros1__276_carry_i_2_n_0\ : STD_LOGIC;
  signal \euros1__276_carry_i_3_n_0\ : STD_LOGIC;
  signal \euros1__276_carry_i_4_n_0\ : STD_LOGIC;
  signal \euros1__276_carry_i_5_n_0\ : STD_LOGIC;
  signal \euros1__276_carry_i_6_n_0\ : STD_LOGIC;
  signal \euros1__276_carry_i_7_n_0\ : STD_LOGIC;
  signal \euros1__276_carry_i_8_n_0\ : STD_LOGIC;
  signal \euros1__276_carry_n_0\ : STD_LOGIC;
  signal \euros1__276_carry_n_1\ : STD_LOGIC;
  signal \euros1__276_carry_n_2\ : STD_LOGIC;
  signal \euros1__276_carry_n_3\ : STD_LOGIC;
  signal \euros1__333_carry_i_1_n_0\ : STD_LOGIC;
  signal \euros1__333_carry_i_2_n_0\ : STD_LOGIC;
  signal \euros1__333_carry_i_3_n_0\ : STD_LOGIC;
  signal \euros1__333_carry_i_4_n_0\ : STD_LOGIC;
  signal \euros1__333_carry_n_2\ : STD_LOGIC;
  signal \euros1__333_carry_n_3\ : STD_LOGIC;
  signal \euros1__333_carry_n_5\ : STD_LOGIC;
  signal \euros1__333_carry_n_6\ : STD_LOGIC;
  signal \euros1__333_carry_n_7\ : STD_LOGIC;
  signal \euros1__339_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \euros1__339_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \euros1__339_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \euros1__339_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \euros1__339_carry__0_n_1\ : STD_LOGIC;
  signal \euros1__339_carry__0_n_2\ : STD_LOGIC;
  signal \euros1__339_carry__0_n_3\ : STD_LOGIC;
  signal \euros1__339_carry__0_n_4\ : STD_LOGIC;
  signal \euros1__339_carry__0_n_5\ : STD_LOGIC;
  signal \euros1__339_carry__0_n_6\ : STD_LOGIC;
  signal \euros1__339_carry__0_n_7\ : STD_LOGIC;
  signal \euros1__339_carry_i_1_n_0\ : STD_LOGIC;
  signal \euros1__339_carry_i_2_n_0\ : STD_LOGIC;
  signal \euros1__339_carry_i_3_n_0\ : STD_LOGIC;
  signal \euros1__339_carry_i_4_n_0\ : STD_LOGIC;
  signal \euros1__339_carry_n_0\ : STD_LOGIC;
  signal \euros1__339_carry_n_1\ : STD_LOGIC;
  signal \euros1__339_carry_n_2\ : STD_LOGIC;
  signal \euros1__339_carry_n_3\ : STD_LOGIC;
  signal \euros1__339_carry_n_4\ : STD_LOGIC;
  signal \euros1__339_carry_n_5\ : STD_LOGIC;
  signal \euros1__92_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__0_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__0_n_1\ : STD_LOGIC;
  signal \euros1__92_carry__0_n_2\ : STD_LOGIC;
  signal \euros1__92_carry__0_n_3\ : STD_LOGIC;
  signal \euros1__92_carry__0_n_4\ : STD_LOGIC;
  signal \euros1__92_carry__0_n_5\ : STD_LOGIC;
  signal \euros1__92_carry__0_n_6\ : STD_LOGIC;
  signal \euros1__92_carry__0_n_7\ : STD_LOGIC;
  signal \euros1__92_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__1_i_7_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__1_i_8_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__1_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__1_n_1\ : STD_LOGIC;
  signal \euros1__92_carry__1_n_2\ : STD_LOGIC;
  signal \euros1__92_carry__1_n_3\ : STD_LOGIC;
  signal \euros1__92_carry__1_n_4\ : STD_LOGIC;
  signal \euros1__92_carry__1_n_5\ : STD_LOGIC;
  signal \euros1__92_carry__1_n_6\ : STD_LOGIC;
  signal \euros1__92_carry__1_n_7\ : STD_LOGIC;
  signal \euros1__92_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__2_i_5_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__2_i_6_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__2_i_7_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__2_i_8_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__2_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__2_n_1\ : STD_LOGIC;
  signal \euros1__92_carry__2_n_2\ : STD_LOGIC;
  signal \euros1__92_carry__2_n_3\ : STD_LOGIC;
  signal \euros1__92_carry__2_n_4\ : STD_LOGIC;
  signal \euros1__92_carry__2_n_5\ : STD_LOGIC;
  signal \euros1__92_carry__2_n_6\ : STD_LOGIC;
  signal \euros1__92_carry__2_n_7\ : STD_LOGIC;
  signal \euros1__92_carry__3_i_1_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__3_i_2_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__3_i_3_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__3_i_4_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__3_i_5_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__3_i_6_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__3_i_7_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__3_i_8_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__3_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__3_n_1\ : STD_LOGIC;
  signal \euros1__92_carry__3_n_2\ : STD_LOGIC;
  signal \euros1__92_carry__3_n_3\ : STD_LOGIC;
  signal \euros1__92_carry__3_n_4\ : STD_LOGIC;
  signal \euros1__92_carry__3_n_5\ : STD_LOGIC;
  signal \euros1__92_carry__3_n_6\ : STD_LOGIC;
  signal \euros1__92_carry__3_n_7\ : STD_LOGIC;
  signal \euros1__92_carry__4_i_1_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__4_i_2_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__4_i_3_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__4_i_4_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__4_i_5_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__4_i_6_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__4_i_7_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__4_i_8_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__4_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__4_n_1\ : STD_LOGIC;
  signal \euros1__92_carry__4_n_2\ : STD_LOGIC;
  signal \euros1__92_carry__4_n_3\ : STD_LOGIC;
  signal \euros1__92_carry__4_n_4\ : STD_LOGIC;
  signal \euros1__92_carry__4_n_5\ : STD_LOGIC;
  signal \euros1__92_carry__4_n_6\ : STD_LOGIC;
  signal \euros1__92_carry__4_n_7\ : STD_LOGIC;
  signal \euros1__92_carry__5_i_1_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__5_i_2_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__5_i_3_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__5_i_4_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__5_i_5_n_0\ : STD_LOGIC;
  signal \euros1__92_carry__5_n_2\ : STD_LOGIC;
  signal \euros1__92_carry__5_n_3\ : STD_LOGIC;
  signal \euros1__92_carry__5_n_5\ : STD_LOGIC;
  signal \euros1__92_carry__5_n_6\ : STD_LOGIC;
  signal \euros1__92_carry__5_n_7\ : STD_LOGIC;
  signal \euros1__92_carry_i_1_n_0\ : STD_LOGIC;
  signal \euros1__92_carry_i_2_n_0\ : STD_LOGIC;
  signal \euros1__92_carry_i_3_n_0\ : STD_LOGIC;
  signal \euros1__92_carry_i_4_n_0\ : STD_LOGIC;
  signal \euros1__92_carry_n_0\ : STD_LOGIC;
  signal \euros1__92_carry_n_1\ : STD_LOGIC;
  signal \euros1__92_carry_n_2\ : STD_LOGIC;
  signal \euros1__92_carry_n_3\ : STD_LOGIC;
  signal \euros1__92_carry_n_4\ : STD_LOGIC;
  signal \euros1__92_carry_n_5\ : STD_LOGIC;
  signal \euros1__92_carry_n_6\ : STD_LOGIC;
  signal \euros1__92_carry_n_7\ : STD_LOGIC;
  signal \i___0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \i___0_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \i___0_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \i___0_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \i___0_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \i___0_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \i___0_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \i___0_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \i___0_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \i___0_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \i___0_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \i___0_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \i___0_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \i___0_carry_i_2_n_0\ : STD_LOGIC;
  signal \i___0_carry_i_3_n_0\ : STD_LOGIC;
  signal \i___0_carry_i_4_n_0\ : STD_LOGIC;
  signal \i___0_carry_i_5_n_0\ : STD_LOGIC;
  signal \i___0_carry_i_6_n_0\ : STD_LOGIC;
  signal \i___106_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \i___106_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \i___106_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \i___106_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \i___106_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \i___106_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \i___106_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \i___106_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \i___106_carry__0_i_9_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_10_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_11_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_12_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_7_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_8_n_0\ : STD_LOGIC;
  signal \i___106_carry__1_i_9_n_0\ : STD_LOGIC;
  signal \i___106_carry__2_i_10_n_0\ : STD_LOGIC;
  signal \i___106_carry__2_i_11_n_0\ : STD_LOGIC;
  signal \i___106_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \i___106_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \i___106_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \i___106_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \i___106_carry__2_i_5_n_0\ : STD_LOGIC;
  signal \i___106_carry__2_i_6_n_0\ : STD_LOGIC;
  signal \i___106_carry__2_i_7_n_0\ : STD_LOGIC;
  signal \i___106_carry__2_i_8_n_0\ : STD_LOGIC;
  signal \i___106_carry__2_i_9_n_0\ : STD_LOGIC;
  signal \i___106_carry__3_i_1_n_0\ : STD_LOGIC;
  signal \i___106_carry__3_i_2_n_0\ : STD_LOGIC;
  signal \i___106_carry__3_i_3_n_0\ : STD_LOGIC;
  signal \i___106_carry__3_i_4_n_0\ : STD_LOGIC;
  signal \i___106_carry__3_i_5_n_0\ : STD_LOGIC;
  signal \i___106_carry__3_i_6_n_0\ : STD_LOGIC;
  signal \i___106_carry__3_i_7_n_0\ : STD_LOGIC;
  signal \i___106_carry__3_i_8_n_0\ : STD_LOGIC;
  signal \i___106_carry__4_i_1_n_0\ : STD_LOGIC;
  signal \i___106_carry__4_i_2_n_0\ : STD_LOGIC;
  signal \i___106_carry__4_i_3_n_0\ : STD_LOGIC;
  signal \i___106_carry__4_i_4_n_0\ : STD_LOGIC;
  signal \i___106_carry__4_i_5_n_0\ : STD_LOGIC;
  signal \i___106_carry__4_i_6_n_0\ : STD_LOGIC;
  signal \i___106_carry__4_i_7_n_0\ : STD_LOGIC;
  signal \i___106_carry__5_i_1_n_0\ : STD_LOGIC;
  signal \i___106_carry__5_i_2_n_0\ : STD_LOGIC;
  signal \i___106_carry_i_1_n_0\ : STD_LOGIC;
  signal \i___106_carry_i_2_n_0\ : STD_LOGIC;
  signal \i___106_carry_i_3_n_0\ : STD_LOGIC;
  signal \i___106_carry_i_4_n_0\ : STD_LOGIC;
  signal \i___106_carry_i_5_n_0\ : STD_LOGIC;
  signal \i___106_carry_i_6_n_0\ : STD_LOGIC;
  signal \i___106_carry_i_7_n_0\ : STD_LOGIC;
  signal \i___106_carry_i_8_n_0\ : STD_LOGIC;
  signal \i___163_carry_i_1_n_0\ : STD_LOGIC;
  signal \i___163_carry_i_2_n_0\ : STD_LOGIC;
  signal \i___163_carry_i_3_n_0\ : STD_LOGIC;
  signal \i___163_carry_i_4_n_0\ : STD_LOGIC;
  signal \i___169_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \i___169_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \i___169_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \i___169_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \i___169_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \i___169_carry_i_2_n_0\ : STD_LOGIC;
  signal \i___169_carry_i_3_n_0\ : STD_LOGIC;
  signal \i___169_carry_i_4_n_0\ : STD_LOGIC;
  signal \i___169_carry_i_5_n_0\ : STD_LOGIC;
  signal \i___169_carry_i_6_n_0\ : STD_LOGIC;
  signal \i___27_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \i___27_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \i___27_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \i___27_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \i___27_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \i___27_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \i___27_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \i___27_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \i___27_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \i___27_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \i___27_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \i___27_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \i___27_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \i___27_carry_i_1_n_0\ : STD_LOGIC;
  signal \i___27_carry_i_2_n_0\ : STD_LOGIC;
  signal \i___27_carry_i_3_n_0\ : STD_LOGIC;
  signal \i___27_carry_i_4_n_0\ : STD_LOGIC;
  signal \i___27_carry_i_5_n_0\ : STD_LOGIC;
  signal \i___56_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \i___56_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \i___56_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \i___56_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \i___56_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \i___56_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \i___56_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \i___56_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \i___56_carry_i_1_n_0\ : STD_LOGIC;
  signal \i___56_carry_i_2_n_0\ : STD_LOGIC;
  signal \i___56_carry_i_3_n_0\ : STD_LOGIC;
  signal \i___56_carry_i_4_n_0\ : STD_LOGIC;
  signal \i___80_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \i___80_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \i___80_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \i___80_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \i___80_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \i___80_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \i___80_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \i___80_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \i___80_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \i___80_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \i___80_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \i___80_carry_i_1_n_0\ : STD_LOGIC;
  signal \i___80_carry_i_2_n_0\ : STD_LOGIC;
  signal \i___80_carry_i_3_n_0\ : STD_LOGIC;
  signal \i___80_carry_i_4_n_0\ : STD_LOGIC;
  signal \i___80_carry_i_5_n_0\ : STD_LOGIC;
  signal p_1_in : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \string_cent_decenas[1]5\ : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \string_cent_decenas[1]5__100_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry__0_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry__0_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry__0_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry__0_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry__0_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry__0_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry__0_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry__1_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry_i_8_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__100_carry_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__0_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__0_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__0_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__1_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__1_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__1_i_9_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__1_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__1_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__1_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__2_i_10_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__2_i_11_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__2_i_12_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__2_i_13_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__2_i_14_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__2_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__2_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__2_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__2_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__2_i_9_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__2_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__2_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__2_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__3_i_10_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__3_i_11_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__3_i_12_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__3_i_13_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__3_i_14_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__3_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__3_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__3_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__3_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__3_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__3_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__3_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__3_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__3_i_9_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__3_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__3_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__3_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__4_i_10_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__4_i_11_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__4_i_12_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__4_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__4_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__4_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__4_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__4_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__4_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__4_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__4_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__4_i_9_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__4_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__4_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__4_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__4_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__5_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__5_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__5_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__5_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__5_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__5_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__5_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__5_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__5_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__5_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry__5_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__124_carry_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__180_carry_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__180_carry_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__180_carry_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__180_carry_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__180_carry_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__180_carry_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__180_carry_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__186_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__186_carry__0_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__186_carry_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__186_carry_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__186_carry_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__186_carry_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__186_carry_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__186_carry_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__186_carry_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__186_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__186_carry_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__186_carry_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__186_carry_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__186_carry_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__186_carry_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__186_carry_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__0_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__1_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__1_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__1_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry__1_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__27_carry_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__0_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__0_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__0_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__0_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__0_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__0_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__0_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__1_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__1_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__1_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__1_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__1_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__1_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry__1_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__55_carry_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__0_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__0_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__0_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__0_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__0_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__0_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__0_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__1_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__1_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__1_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry__1_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5__79_carry_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__0_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry__1_n_7\ : STD_LOGIC;
  signal \^string_cent_decenas[1]5_carry_i_10_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_10_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_12_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_13_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_14_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_15_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_16_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_17_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_5_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_6_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_7_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_8_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_i_9_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_carry_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__2_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry__2_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___0_carry_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__0_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__0_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__0_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__1_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__1_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__1_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__2_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__2_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__2_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__2_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__3_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__3_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__3_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__3_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__5_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__5_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry__5_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___106_carry_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___163_carry_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___163_carry_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___163_carry_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___163_carry_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___163_carry_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___169_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___169_carry_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___169_carry_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___169_carry_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___169_carry_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___169_carry_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___27_carry_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___56_carry_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_7\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry_n_0\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry_n_1\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry_n_2\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry_n_3\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry_n_4\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry_n_5\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry_n_6\ : STD_LOGIC;
  signal \string_cent_decenas[1]5_inferred__0/i___80_carry_n_7\ : STD_LOGIC;
  signal \tot_OBUF[3]_inst_i_1_n_0\ : STD_LOGIC;
  signal \tot_OBUF[3]_inst_i_1_n_1\ : STD_LOGIC;
  signal \tot_OBUF[3]_inst_i_1_n_2\ : STD_LOGIC;
  signal \tot_OBUF[3]_inst_i_1_n_3\ : STD_LOGIC;
  signal \tot_OBUF[3]_inst_i_2_n_0\ : STD_LOGIC;
  signal \tot_OBUF[3]_inst_i_3_n_0\ : STD_LOGIC;
  signal \tot_OBUF[3]_inst_i_4_n_0\ : STD_LOGIC;
  signal \tot_OBUF[3]_inst_i_5_n_0\ : STD_LOGIC;
  signal \tot_OBUF[7]_inst_i_1_n_0\ : STD_LOGIC;
  signal \tot_OBUF[7]_inst_i_1_n_1\ : STD_LOGIC;
  signal \tot_OBUF[7]_inst_i_1_n_2\ : STD_LOGIC;
  signal \tot_OBUF[7]_inst_i_1_n_3\ : STD_LOGIC;
  signal \tot_OBUF[7]_inst_i_2_n_0\ : STD_LOGIC;
  signal \tot_OBUF[7]_inst_i_3_n_0\ : STD_LOGIC;
  signal \tot_OBUF[7]_inst_i_4_n_0\ : STD_LOGIC;
  signal \tot_OBUF[7]_inst_i_5_n_0\ : STD_LOGIC;
  signal \NLW_euros1__169_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_euros1__169_carry__4_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_euros1__169_carry__4_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_euros1__1_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_euros1__1_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_euros1__1_carry__5_i_9_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_euros1__1_carry__5_i_9_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_euros1__1_carry__7_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_euros1__1_carry__7_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_euros1__231_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_euros1__276_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_euros1__276_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_euros1__276_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_euros1__276_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_euros1__276_carry__3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_euros1__276_carry__5_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_euros1__276_carry__5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_euros1__333_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_euros1__333_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_euros1__339_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_euros1__92_carry__5_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_euros1__92_carry__5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_string_cent_decenas[1]5__100_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5__100_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_string_cent_decenas[1]5__100_carry_i_8_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_string_cent_decenas[1]5__100_carry_i_8_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5__124_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5__124_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5__124_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5__124_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5__124_carry__3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5__124_carry__3_i_11_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_string_cent_decenas[1]5__124_carry__3_i_11_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5__124_carry__4_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5__124_carry__5_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_string_cent_decenas[1]5__124_carry__5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_string_cent_decenas[1]5__180_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_string_cent_decenas[1]5__180_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_string_cent_decenas[1]5__186_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5__186_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_string_cent_decenas[1]5__27_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_string_cent_decenas[1]5__27_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_string_cent_decenas[1]5__55_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_string_cent_decenas[1]5__79_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_string_cent_decenas[1]5__79_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_string_cent_decenas[1]5__79_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_string_cent_decenas[1]5_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_string_cent_decenas[1]5_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___0_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__5_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___163_carry_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___163_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___169_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___169_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___27_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___27_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___27_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___56_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___56_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___56_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___80_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_string_cent_decenas[1]5_inferred__0/i___80_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \disp[2][2]_INST_0_i_2\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \disp[2][3]_INST_0_i_2\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \disp[3][2]_INST_0_i_4\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \disp[3][3]_INST_0_i_10\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \disp[3][3]_INST_0_i_19\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \disp[3][3]_INST_0_i_21\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \disp[3][3]_INST_0_i_23\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \disp[3][3]_INST_0_i_24\ : label is "soft_lutpair1";
  attribute HLUTNM : string;
  attribute HLUTNM of \euros1__1_carry__0_i_2\ : label is "lutpair0";
  attribute HLUTNM of \euros1__1_carry__1_i_1\ : label is "lutpair3";
  attribute HLUTNM of \euros1__1_carry__1_i_2\ : label is "lutpair2";
  attribute HLUTNM of \euros1__1_carry__1_i_3\ : label is "lutpair1";
  attribute HLUTNM of \euros1__1_carry__1_i_4\ : label is "lutpair0";
  attribute HLUTNM of \euros1__1_carry__1_i_5\ : label is "lutpair4";
  attribute HLUTNM of \euros1__1_carry__1_i_6\ : label is "lutpair3";
  attribute HLUTNM of \euros1__1_carry__1_i_7\ : label is "lutpair2";
  attribute HLUTNM of \euros1__1_carry__1_i_8\ : label is "lutpair1";
  attribute HLUTNM of \euros1__1_carry__2_i_1\ : label is "lutpair7";
  attribute HLUTNM of \euros1__1_carry__2_i_2\ : label is "lutpair6";
  attribute HLUTNM of \euros1__1_carry__2_i_3\ : label is "lutpair5";
  attribute HLUTNM of \euros1__1_carry__2_i_4\ : label is "lutpair4";
  attribute HLUTNM of \euros1__1_carry__2_i_5\ : label is "lutpair8";
  attribute HLUTNM of \euros1__1_carry__2_i_6\ : label is "lutpair7";
  attribute HLUTNM of \euros1__1_carry__2_i_7\ : label is "lutpair6";
  attribute HLUTNM of \euros1__1_carry__2_i_8\ : label is "lutpair5";
  attribute HLUTNM of \euros1__1_carry__3_i_1\ : label is "lutpair11";
  attribute HLUTNM of \euros1__1_carry__3_i_2\ : label is "lutpair10";
  attribute HLUTNM of \euros1__1_carry__3_i_3\ : label is "lutpair9";
  attribute HLUTNM of \euros1__1_carry__3_i_4\ : label is "lutpair8";
  attribute HLUTNM of \euros1__1_carry__3_i_5\ : label is "lutpair12";
  attribute HLUTNM of \euros1__1_carry__3_i_6\ : label is "lutpair11";
  attribute HLUTNM of \euros1__1_carry__3_i_7\ : label is "lutpair10";
  attribute HLUTNM of \euros1__1_carry__3_i_8\ : label is "lutpair9";
  attribute HLUTNM of \euros1__1_carry__4_i_1\ : label is "lutpair15";
  attribute HLUTNM of \euros1__1_carry__4_i_2\ : label is "lutpair14";
  attribute HLUTNM of \euros1__1_carry__4_i_3\ : label is "lutpair13";
  attribute HLUTNM of \euros1__1_carry__4_i_4\ : label is "lutpair12";
  attribute HLUTNM of \euros1__1_carry__4_i_5\ : label is "lutpair16";
  attribute HLUTNM of \euros1__1_carry__4_i_6\ : label is "lutpair15";
  attribute HLUTNM of \euros1__1_carry__4_i_7\ : label is "lutpair14";
  attribute HLUTNM of \euros1__1_carry__4_i_8\ : label is "lutpair13";
  attribute HLUTNM of \euros1__1_carry__5_i_1\ : label is "lutpair19";
  attribute HLUTNM of \euros1__1_carry__5_i_2\ : label is "lutpair18";
  attribute HLUTNM of \euros1__1_carry__5_i_3\ : label is "lutpair17";
  attribute HLUTNM of \euros1__1_carry__5_i_4\ : label is "lutpair16";
  attribute HLUTNM of \euros1__1_carry__5_i_6\ : label is "lutpair19";
  attribute HLUTNM of \euros1__1_carry__5_i_7\ : label is "lutpair18";
  attribute HLUTNM of \euros1__1_carry__5_i_8\ : label is "lutpair17";
  attribute HLUTNM of \euros1__231_carry__0_i_1\ : label is "lutpair43";
  attribute HLUTNM of \euros1__231_carry__0_i_2\ : label is "lutpair42";
  attribute HLUTNM of \euros1__231_carry__0_i_3\ : label is "lutpair41";
  attribute HLUTNM of \euros1__231_carry__0_i_5\ : label is "lutpair44";
  attribute HLUTNM of \euros1__231_carry__0_i_6\ : label is "lutpair43";
  attribute HLUTNM of \euros1__231_carry__0_i_7\ : label is "lutpair42";
  attribute HLUTNM of \euros1__231_carry__0_i_8\ : label is "lutpair41";
  attribute HLUTNM of \euros1__231_carry__1_i_1\ : label is "lutpair47";
  attribute HLUTNM of \euros1__231_carry__1_i_2\ : label is "lutpair46";
  attribute HLUTNM of \euros1__231_carry__1_i_3\ : label is "lutpair45";
  attribute HLUTNM of \euros1__231_carry__1_i_4\ : label is "lutpair44";
  attribute HLUTNM of \euros1__231_carry__1_i_5\ : label is "lutpair48";
  attribute HLUTNM of \euros1__231_carry__1_i_6\ : label is "lutpair47";
  attribute HLUTNM of \euros1__231_carry__1_i_7\ : label is "lutpair46";
  attribute HLUTNM of \euros1__231_carry__1_i_8\ : label is "lutpair45";
  attribute HLUTNM of \euros1__231_carry__2_i_1\ : label is "lutpair50";
  attribute HLUTNM of \euros1__231_carry__2_i_2\ : label is "lutpair49";
  attribute HLUTNM of \euros1__231_carry__2_i_3\ : label is "lutpair48";
  attribute HLUTNM of \euros1__231_carry__2_i_6\ : label is "lutpair50";
  attribute HLUTNM of \euros1__231_carry__2_i_7\ : label is "lutpair49";
  attribute HLUTNM of \euros1__276_carry__0_i_3\ : label is "lutpair52";
  attribute HLUTNM of \euros1__276_carry__0_i_4\ : label is "lutpair51";
  attribute HLUTNM of \euros1__276_carry__0_i_8\ : label is "lutpair52";
  attribute SOFT_HLUTNM of \euros1__276_carry__0_i_9\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \euros1__276_carry__1_i_10\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \euros1__276_carry__1_i_11\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \euros1__276_carry__1_i_12\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \euros1__276_carry__1_i_13\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \euros1__276_carry__1_i_14\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \euros1__276_carry__1_i_15\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \euros1__276_carry__2_i_10\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \euros1__276_carry__2_i_11\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \euros1__276_carry__2_i_12\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \euros1__276_carry__2_i_13\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \euros1__276_carry__2_i_14\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \euros1__276_carry__2_i_15\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \euros1__276_carry__2_i_9\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \euros1__276_carry__3_i_10\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \euros1__276_carry__3_i_11\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \euros1__276_carry__3_i_12\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \euros1__276_carry__3_i_13\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \euros1__276_carry__3_i_14\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \euros1__276_carry__3_i_15\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \euros1__276_carry__3_i_16\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \euros1__276_carry__3_i_9\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \euros1__276_carry__4_i_10\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \euros1__276_carry__4_i_11\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \euros1__276_carry__4_i_12\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \euros1__276_carry__4_i_13\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \euros1__276_carry__4_i_14\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \euros1__276_carry__4_i_15\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \euros1__276_carry__4_i_16\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \euros1__276_carry__4_i_9\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \euros1__276_carry__5_i_4\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \euros1__276_carry__5_i_5\ : label is "soft_lutpair25";
  attribute HLUTNM of \euros1__276_carry_i_5\ : label is "lutpair51";
  attribute HLUTNM of \euros1__92_carry__0_i_1\ : label is "lutpair22";
  attribute HLUTNM of \euros1__92_carry__0_i_2\ : label is "lutpair21";
  attribute HLUTNM of \euros1__92_carry__0_i_3\ : label is "lutpair20";
  attribute HLUTNM of \euros1__92_carry__0_i_4\ : label is "lutpair61";
  attribute HLUTNM of \euros1__92_carry__0_i_5\ : label is "lutpair23";
  attribute HLUTNM of \euros1__92_carry__0_i_6\ : label is "lutpair22";
  attribute HLUTNM of \euros1__92_carry__0_i_7\ : label is "lutpair21";
  attribute HLUTNM of \euros1__92_carry__0_i_8\ : label is "lutpair20";
  attribute HLUTNM of \euros1__92_carry__1_i_1\ : label is "lutpair26";
  attribute HLUTNM of \euros1__92_carry__1_i_2\ : label is "lutpair25";
  attribute HLUTNM of \euros1__92_carry__1_i_3\ : label is "lutpair24";
  attribute HLUTNM of \euros1__92_carry__1_i_4\ : label is "lutpair23";
  attribute HLUTNM of \euros1__92_carry__1_i_5\ : label is "lutpair27";
  attribute HLUTNM of \euros1__92_carry__1_i_6\ : label is "lutpair26";
  attribute HLUTNM of \euros1__92_carry__1_i_7\ : label is "lutpair25";
  attribute HLUTNM of \euros1__92_carry__1_i_8\ : label is "lutpair24";
  attribute HLUTNM of \euros1__92_carry__2_i_1\ : label is "lutpair30";
  attribute HLUTNM of \euros1__92_carry__2_i_2\ : label is "lutpair29";
  attribute HLUTNM of \euros1__92_carry__2_i_3\ : label is "lutpair28";
  attribute HLUTNM of \euros1__92_carry__2_i_4\ : label is "lutpair27";
  attribute HLUTNM of \euros1__92_carry__2_i_5\ : label is "lutpair31";
  attribute HLUTNM of \euros1__92_carry__2_i_6\ : label is "lutpair30";
  attribute HLUTNM of \euros1__92_carry__2_i_7\ : label is "lutpair29";
  attribute HLUTNM of \euros1__92_carry__2_i_8\ : label is "lutpair28";
  attribute HLUTNM of \euros1__92_carry__3_i_1\ : label is "lutpair34";
  attribute HLUTNM of \euros1__92_carry__3_i_2\ : label is "lutpair33";
  attribute HLUTNM of \euros1__92_carry__3_i_3\ : label is "lutpair32";
  attribute HLUTNM of \euros1__92_carry__3_i_4\ : label is "lutpair31";
  attribute HLUTNM of \euros1__92_carry__3_i_5\ : label is "lutpair35";
  attribute HLUTNM of \euros1__92_carry__3_i_6\ : label is "lutpair34";
  attribute HLUTNM of \euros1__92_carry__3_i_7\ : label is "lutpair33";
  attribute HLUTNM of \euros1__92_carry__3_i_8\ : label is "lutpair32";
  attribute HLUTNM of \euros1__92_carry__4_i_1\ : label is "lutpair38";
  attribute HLUTNM of \euros1__92_carry__4_i_2\ : label is "lutpair37";
  attribute HLUTNM of \euros1__92_carry__4_i_3\ : label is "lutpair36";
  attribute HLUTNM of \euros1__92_carry__4_i_4\ : label is "lutpair35";
  attribute HLUTNM of \euros1__92_carry__4_i_5\ : label is "lutpair39";
  attribute HLUTNM of \euros1__92_carry__4_i_6\ : label is "lutpair38";
  attribute HLUTNM of \euros1__92_carry__4_i_7\ : label is "lutpair37";
  attribute HLUTNM of \euros1__92_carry__4_i_8\ : label is "lutpair36";
  attribute HLUTNM of \euros1__92_carry__5_i_1\ : label is "lutpair40";
  attribute HLUTNM of \euros1__92_carry__5_i_2\ : label is "lutpair39";
  attribute HLUTNM of \euros1__92_carry__5_i_5\ : label is "lutpair40";
  attribute HLUTNM of \euros1__92_carry_i_2\ : label is "lutpair61";
  attribute HLUTNM of \i___106_carry__0_i_3\ : label is "lutpair59";
  attribute HLUTNM of \i___106_carry__0_i_4\ : label is "lutpair58";
  attribute HLUTNM of \i___106_carry__0_i_8\ : label is "lutpair59";
  attribute SOFT_HLUTNM of \i___106_carry__0_i_9\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \i___106_carry__1_i_10\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \i___106_carry__1_i_11\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \i___106_carry__1_i_12\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \i___106_carry__2_i_11\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \i___106_carry__2_i_9\ : label is "soft_lutpair5";
  attribute HLUTNM of \i___106_carry__3_i_3\ : label is "lutpair60";
  attribute HLUTNM of \i___106_carry__3_i_8\ : label is "lutpair60";
  attribute HLUTNM of \i___106_carry_i_5\ : label is "lutpair58";
  attribute HLUTNM of \i___27_carry__0_i_3\ : label is "lutpair63";
  attribute HLUTNM of \i___27_carry__0_i_8\ : label is "lutpair63";
  attribute HLUTNM of \string_cent_decenas[1]5__124_carry__0_i_1\ : label is "lutpair54";
  attribute HLUTNM of \string_cent_decenas[1]5__124_carry__0_i_2\ : label is "lutpair53";
  attribute HLUTNM of \string_cent_decenas[1]5__124_carry__0_i_5\ : label is "lutpair55";
  attribute HLUTNM of \string_cent_decenas[1]5__124_carry__0_i_6\ : label is "lutpair54";
  attribute HLUTNM of \string_cent_decenas[1]5__124_carry__0_i_7\ : label is "lutpair53";
  attribute HLUTNM of \string_cent_decenas[1]5__124_carry__1_i_2\ : label is "lutpair57";
  attribute HLUTNM of \string_cent_decenas[1]5__124_carry__1_i_3\ : label is "lutpair56";
  attribute HLUTNM of \string_cent_decenas[1]5__124_carry__1_i_4\ : label is "lutpair55";
  attribute HLUTNM of \string_cent_decenas[1]5__124_carry__1_i_7\ : label is "lutpair57";
  attribute HLUTNM of \string_cent_decenas[1]5__124_carry__1_i_8\ : label is "lutpair56";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__124_carry__1_i_9\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__124_carry__2_i_10\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__124_carry__2_i_11\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__124_carry__2_i_12\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__124_carry__2_i_13\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__124_carry__2_i_14\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__124_carry__3_i_12\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__124_carry__3_i_13\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__124_carry__3_i_14\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__124_carry__3_i_9\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__124_carry__4_i_10\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__124_carry__4_i_11\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__124_carry__4_i_12\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \string_cent_decenas[1]5__124_carry__4_i_9\ : label is "soft_lutpair29";
  attribute HLUTNM of \string_cent_decenas[1]5__27_carry_i_1\ : label is "lutpair62";
  attribute HLUTNM of \string_cent_decenas[1]5__79_carry_i_4\ : label is "lutpair62";
begin
  O(0) <= \^o\(0);
  O23(7 downto 0) <= \^o23\(7 downto 0);
  \string_cent_decenas[1]5_carry_i_10_0\ <= \^string_cent_decenas[1]5_carry_i_10_0\;
\disp[2][1]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAEAAEAEAEEAAA"
    )
        port map (
      I0 => \disp[1]_OBUF\(0),
      I1 => Q(1),
      I2 => \string_cent_decenas[1]5__186_carry_n_4\,
      I3 => \string_cent_decenas[1]5__186_carry_n_5\,
      I4 => \string_cent_decenas[1]5__186_carry_n_6\,
      I5 => \string_cent_decenas[1]5__186_carry__0_n_7\,
      O => \disp[2]_OBUF\(0)
    );
\disp[2][2]_INST_0_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"1F001000"
    )
        port map (
      I0 => \disp_dinero[2]\(3),
      I1 => \disp[2][2]_INST_0_i_2_n_0\,
      I2 => Q(1),
      I3 => Q(0),
      I4 => \disp_refresco[4]\(0),
      O => \disp[2]_OBUF\(1)
    );
\disp[2][2]_INST_0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6675"
    )
        port map (
      I0 => \string_cent_decenas[1]5__186_carry_n_5\,
      I1 => \string_cent_decenas[1]5__186_carry_n_6\,
      I2 => \string_cent_decenas[1]5__186_carry_n_4\,
      I3 => \string_cent_decenas[1]5__186_carry__0_n_7\,
      O => \disp[2][2]_INST_0_i_2_n_0\
    );
\disp[2][3]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B080"
    )
        port map (
      I0 => \disp_dinero[2]\(3),
      I1 => Q(1),
      I2 => Q(0),
      I3 => \disp_refresco[8]\(0),
      O => \disp[2]_OBUF\(2)
    );
\disp[2][3]_INST_0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"444A"
    )
        port map (
      I0 => \string_cent_decenas[1]5__186_carry_n_4\,
      I1 => \string_cent_decenas[1]5__186_carry__0_n_7\,
      I2 => \string_cent_decenas[1]5__186_carry_n_6\,
      I3 => \string_cent_decenas[1]5__186_carry_n_5\,
      O => \disp_dinero[2]\(3)
    );
\disp[3][0]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FD5DFDFDFD5DFD5D"
    )
        port map (
      I0 => Q(0),
      I1 => \disp_refresco[3]\(0),
      I2 => Q(1),
      I3 => \disp[3][0]_INST_0_i_2_n_0\,
      I4 => \disp[3][1]_INST_0_i_3_n_0\,
      I5 => \disp[3][0]_INST_0_i_3_n_0\,
      O => \disp[3]_OBUF\(0)
    );
\disp[3][0]_INST_0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C0080AF0F0AF2BC0"
    )
        port map (
      I0 => \disp[3][3]_INST_0_i_10_n_0\,
      I1 => \disp[3][3]_INST_0_i_6_n_0\,
      I2 => \disp[3][3]_INST_0_i_9_n_0\,
      I3 => \disp[3][3]_INST_0_i_8_n_0\,
      I4 => \disp[3][3]_INST_0_i_5_n_0\,
      I5 => \disp[3][3]_INST_0_i_7_n_0\,
      O => \disp[3][0]_INST_0_i_2_n_0\
    );
\disp[3][0]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"98618719E799679E"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_3_n_0\,
      I1 => \disp[3][3]_INST_0_i_13_n_0\,
      I2 => \disp[3][3]_INST_0_i_12_n_0\,
      I3 => \disp[3][3]_INST_0_i_11_n_0\,
      I4 => \disp[3][3]_INST_0_i_8_n_0\,
      I5 => \disp[3][3]_INST_0_i_9_n_0\,
      O => \disp[3][0]_INST_0_i_3_n_0\
    );
\disp[3][1]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BABABAAAAAAAAAAA"
    )
        port map (
      I0 => \disp[3][1]\,
      I1 => \disp[3][1]_INST_0_i_3_n_0\,
      I2 => \disp[3][3]_INST_0_i_4_n_0\,
      I3 => \disp[3][3]_INST_0_i_3_n_0\,
      I4 => \disp[3][3]_INST_0_i_2_n_0\,
      I5 => \disp[3][1]_0\,
      O => \disp[3]_OBUF\(1)
    );
\disp[3][1]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"D66FBCC794473CC6"
    )
        port map (
      I0 => \disp[3][3]_INST_0_i_12_n_0\,
      I1 => \disp[3][3]_INST_0_i_11_n_0\,
      I2 => \disp[3][2]_INST_0_i_3_n_0\,
      I3 => \disp[3][3]_INST_0_i_13_n_0\,
      I4 => \disp[3][3]_INST_0_i_8_n_0\,
      I5 => \disp[3][3]_INST_0_i_9_n_0\,
      O => \disp[3][1]_INST_0_i_3_n_0\
    );
\disp[3][2]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BEAE"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_2_n_0\,
      I1 => Q(1),
      I2 => Q(0),
      I3 => \disp_refresco[3]\(1),
      O => \disp[3]_OBUF\(2)
    );
\disp[3][2]_INST_0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8111899100000000"
    )
        port map (
      I0 => \disp[3][3]_INST_0_i_11_n_0\,
      I1 => \disp[3][3]_INST_0_i_12_n_0\,
      I2 => \disp[3][3]_INST_0_i_13_n_0\,
      I3 => \disp[3][2]_INST_0_i_3_n_0\,
      I4 => \disp[3][3]_INST_0_i_8_n_0\,
      I5 => Q(1),
      O => \disp[3][2]_INST_0_i_2_n_0\
    );
\disp[3][2]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"D040400DFDF4F4DF"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_4_n_0\,
      I1 => \disp[2][2]_INST_0_i_2_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_4\,
      I3 => \disp[3][2]_INST_0_i_5_n_0\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_5\,
      I5 => \disp_dinero[2]\(3),
      O => \disp[3][2]_INST_0_i_3_n_0\
    );
\disp[3][2]_INST_0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"045D"
    )
        port map (
      I0 => \disp[3][3]_INST_0_i_21_n_0\,
      I1 => \^o\(0),
      I2 => cent(0),
      I3 => \string_cent_decenas[1]5\(1),
      O => \disp[3][2]_INST_0_i_4_n_0\
    );
\disp[3][2]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000005557FFFF"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_4\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_5\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_7\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_6\,
      I5 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_4\,
      O => \disp[3][2]_INST_0_i_5_n_0\
    );
\disp[3][3]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1FFF00001F000000"
    )
        port map (
      I0 => \disp[3][3]_INST_0_i_2_n_0\,
      I1 => \disp[3][3]_INST_0_i_3_n_0\,
      I2 => \disp[3][3]_INST_0_i_4_n_0\,
      I3 => Q(1),
      I4 => Q(0),
      I5 => \disp_refresco[7]\(0),
      O => \disp[3]_OBUF\(3)
    );
\disp[3][3]_INST_0_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96999969"
    )
        port map (
      I0 => \string_cent_decenas[1]5\(1),
      I1 => \disp[3][3]_INST_0_i_21_n_0\,
      I2 => cent(0),
      I3 => \^o\(0),
      I4 => \disp[3][3]_INST_0_i_3_n_0\,
      O => \disp[3][3]_INST_0_i_10_n_0\
    );
\disp[3][3]_INST_0_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCCCCCC07777777F"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_4\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_6\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_7\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_5\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_4\,
      I5 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_5\,
      O => \disp[3][3]_INST_0_i_11_n_0\
    );
\disp[3][3]_INST_0_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0003FFFC55570000"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_4\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_5\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_7\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_6\,
      I5 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_4\,
      O => \disp[3][3]_INST_0_i_12_n_0\
    );
\disp[3][3]_INST_0_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FC03FC03FD00FF00"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_4\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_5\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_7\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_6\,
      I5 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_4\,
      O => \disp[3][3]_INST_0_i_13_n_0\
    );
\disp[3][3]_INST_0_i_14\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3C3C3C3C393B3333"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_4\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_5\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_7\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_6\,
      I5 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_4\,
      O => \disp[3][3]_INST_0_i_14_n_0\
    );
\disp[3][3]_INST_0_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFEF8AEF8A0000"
    )
        port map (
      I0 => \string_cent_decenas[1]5\(1),
      I1 => cent(0),
      I2 => \^o\(0),
      I3 => \disp[3][3]_INST_0_i_21_n_0\,
      I4 => \disp[2][2]_INST_0_i_2_n_0\,
      I5 => \disp[3][3]_INST_0_i_22_n_0\,
      O => \disp[3][3]_INST_0_i_15_n_0\
    );
\disp[3][3]_INST_0_i_16\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4BD2D2B4B42D2D4B"
    )
        port map (
      I0 => \disp[2][2]_INST_0_i_2_n_0\,
      I1 => \disp[3][2]_INST_0_i_4_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_4\,
      I3 => \disp[3][2]_INST_0_i_5_n_0\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_5\,
      I5 => \disp_dinero[2]\(3),
      O => \disp[3][3]_INST_0_i_16_n_0\
    );
\disp[3][3]_INST_0_i_17\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1001400420028008"
    )
        port map (
      I0 => \disp[2][2]_INST_0_i_2_n_0\,
      I1 => \disp[3][3]_INST_0_i_21_n_0\,
      I2 => \^o\(0),
      I3 => cent(0),
      I4 => \string_cent_decenas[1]5\(1),
      I5 => \disp[3][3]_INST_0_i_22_n_0\,
      O => \disp[3][3]_INST_0_i_17_n_0\
    );
\disp[3][3]_INST_0_i_18\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00F0F0FFBB0B0B00"
    )
        port map (
      I0 => \disp[3][3]_INST_0_i_12_n_0\,
      I1 => \disp[3][3]_INST_0_i_11_n_0\,
      I2 => \disp[3][3]_INST_0_i_15_n_0\,
      I3 => \disp[3][3]_INST_0_i_14_n_0\,
      I4 => \disp_dinero[2]\(3),
      I5 => \disp[3][3]_INST_0_i_13_n_0\,
      O => \disp[3][3]_INST_0_i_18_n_0\
    );
\disp[3][3]_INST_0_i_19\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4182"
    )
        port map (
      I0 => \string_cent_decenas[1]5\(1),
      I1 => cent(0),
      I2 => \^o\(0),
      I3 => \disp[3][3]_INST_0_i_21_n_0\,
      O => \disp[3][3]_INST_0_i_19_n_0\
    );
\disp[3][3]_INST_0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"20202220202A2220"
    )
        port map (
      I0 => \disp[3][3]_INST_0_i_5_n_0\,
      I1 => \disp[3][3]_INST_0_i_6_n_0\,
      I2 => \disp[3][3]_INST_0_i_7_n_0\,
      I3 => \disp[3][3]_INST_0_i_8_n_0\,
      I4 => \disp[3][3]_INST_0_i_9_n_0\,
      I5 => \disp[3][3]_INST_0_i_10_n_0\,
      O => \disp[3][3]_INST_0_i_2_n_0\
    );
\disp[3][3]_INST_0_i_20\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A6656559599A9AA6"
    )
        port map (
      I0 => \disp[3][3]_INST_0_i_22_n_0\,
      I1 => \string_cent_decenas[1]5\(1),
      I2 => \disp[3][3]_INST_0_i_23_n_0\,
      I3 => \disp[3][3]_INST_0_i_24_n_0\,
      I4 => \string_cent_decenas[1]5__186_carry_n_6\,
      I5 => \string_cent_decenas[1]5__186_carry_n_5\,
      O => \disp[3][3]_INST_0_i_20_n_0\
    );
\disp[3][3]_INST_0_i_21\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CD99"
    )
        port map (
      I0 => \string_cent_decenas[1]5__186_carry__0_n_7\,
      I1 => \string_cent_decenas[1]5__186_carry_n_6\,
      I2 => \string_cent_decenas[1]5__186_carry_n_5\,
      I3 => \string_cent_decenas[1]5__186_carry_n_4\,
      O => \disp[3][3]_INST_0_i_21_n_0\
    );
\disp[3][3]_INST_0_i_22\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"11EE11EA55AA55AA"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_4\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_6\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_7\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_5\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_4\,
      I5 => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_5\,
      O => \disp[3][3]_INST_0_i_22_n_0\
    );
\disp[3][3]_INST_0_i_23\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^o\(0),
      I1 => cent(0),
      O => \disp[3][3]_INST_0_i_23_n_0\
    );
\disp[3][3]_INST_0_i_24\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0057"
    )
        port map (
      I0 => \string_cent_decenas[1]5__186_carry_n_4\,
      I1 => \string_cent_decenas[1]5__186_carry_n_5\,
      I2 => \string_cent_decenas[1]5__186_carry_n_6\,
      I3 => \string_cent_decenas[1]5__186_carry__0_n_7\,
      O => \disp[3][3]_INST_0_i_24_n_0\
    );
\disp[3][3]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FDFFFFFFFDFDFDFF"
    )
        port map (
      I0 => \disp[3][3]_INST_0_i_11_n_0\,
      I1 => \disp[3][3]_INST_0_i_12_n_0\,
      I2 => \disp[3][3]_INST_0_i_13_n_0\,
      I3 => \disp_dinero[2]\(3),
      I4 => \disp[3][3]_INST_0_i_14_n_0\,
      I5 => \disp[3][3]_INST_0_i_15_n_0\,
      O => \disp[3][3]_INST_0_i_3_n_0\
    );
\disp[3][3]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBABABABBBBBBBAB"
    )
        port map (
      I0 => \disp[3][3]_INST_0_i_11_n_0\,
      I1 => \disp[3][3]_INST_0_i_12_n_0\,
      I2 => \disp[3][3]_INST_0_i_13_n_0\,
      I3 => \disp_dinero[2]\(3),
      I4 => \disp[3][3]_INST_0_i_14_n_0\,
      I5 => \disp[3][3]_INST_0_i_15_n_0\,
      O => \disp[3][3]_INST_0_i_4_n_0\
    );
\disp[3][3]_INST_0_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"1F787FF8"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_3_n_0\,
      I1 => \disp[3][3]_INST_0_i_13_n_0\,
      I2 => \disp[3][3]_INST_0_i_12_n_0\,
      I3 => \disp[3][3]_INST_0_i_11_n_0\,
      I4 => \disp[3][3]_INST_0_i_16_n_0\,
      O => \disp[3][3]_INST_0_i_5_n_0\
    );
\disp[3][3]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"033F3FFCFCC9CCC0"
    )
        port map (
      I0 => \disp[3][3]_INST_0_i_17_n_0\,
      I1 => \disp[3][3]_INST_0_i_16_n_0\,
      I2 => \disp[3][3]_INST_0_i_13_n_0\,
      I3 => \disp[3][2]_INST_0_i_3_n_0\,
      I4 => \disp[3][3]_INST_0_i_11_n_0\,
      I5 => \disp[3][3]_INST_0_i_12_n_0\,
      O => \disp[3][3]_INST_0_i_6_n_0\
    );
\disp[3][3]_INST_0_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9999669666669969"
    )
        port map (
      I0 => \disp[3][3]_INST_0_i_18_n_0\,
      I1 => \disp[3][3]_INST_0_i_16_n_0\,
      I2 => \disp[3][3]_INST_0_i_19_n_0\,
      I3 => \disp[3][3]_INST_0_i_20_n_0\,
      I4 => \disp[3][3]_INST_0_i_3_n_0\,
      I5 => \disp[3][3]_INST_0_i_4_n_0\,
      O => \disp[3][3]_INST_0_i_7_n_0\
    );
\disp[3][3]_INST_0_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969696996966996"
    )
        port map (
      I0 => \disp_dinero[2]\(3),
      I1 => \disp[3][3]_INST_0_i_14_n_0\,
      I2 => \disp[3][3]_INST_0_i_15_n_0\,
      I3 => \disp[3][3]_INST_0_i_19_n_0\,
      I4 => \disp[3][3]_INST_0_i_20_n_0\,
      I5 => \disp[3][3]_INST_0_i_3_n_0\,
      O => \disp[3][3]_INST_0_i_8_n_0\
    );
\disp[3][3]_INST_0_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"555555559AA96AA6"
    )
        port map (
      I0 => \disp[3][3]_INST_0_i_20_n_0\,
      I1 => \string_cent_decenas[1]5\(1),
      I2 => cent(0),
      I3 => \^o\(0),
      I4 => \disp[3][3]_INST_0_i_21_n_0\,
      I5 => \disp[3][3]_INST_0_i_3_n_0\,
      O => \disp[3][3]_INST_0_i_9_n_0\
    );
\disp[5][0]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"06FFFF000600FF00"
    )
        port map (
      I0 => \^o23\(0),
      I1 => p_1_in(0),
      I2 => \^string_cent_decenas[1]5_carry_i_10_0\,
      I3 => Q(1),
      I4 => Q(0),
      I5 => \disp_refresco[5]\(0),
      O => \disp[5]_OBUF\(0)
    );
\disp[5][4]_INST_0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_7_n_0\,
      I1 => \string_cent_decenas[1]5_carry_i_8_n_0\,
      I2 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I3 => \string_cent_decenas[1]5_carry_i_10_n_0\,
      O => \^string_cent_decenas[1]5_carry_i_10_0\
    );
\euros1__169_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \euros1__169_carry_n_0\,
      CO(2) => \euros1__169_carry_n_1\,
      CO(1) => \euros1__169_carry_n_2\,
      CO(0) => \euros1__169_carry_n_3\,
      CYINIT => '0',
      DI(3) => \^o23\(0),
      DI(2 downto 0) => B"001",
      O(3) => \euros1__169_carry_n_4\,
      O(2) => \euros1__169_carry_n_5\,
      O(1) => \euros1__169_carry_n_6\,
      O(0) => \NLW_euros1__169_carry_O_UNCONNECTED\(0),
      S(3) => \euros1__169_carry_i_1_n_0\,
      S(2) => \euros1__169_carry_i_2_n_0\,
      S(1) => \euros1__169_carry_i_3_n_0\,
      S(0) => \^o23\(0)
    );
\euros1__169_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__169_carry_n_0\,
      CO(3) => \euros1__169_carry__0_n_0\,
      CO(2) => \euros1__169_carry__0_n_1\,
      CO(1) => \euros1__169_carry__0_n_2\,
      CO(0) => \euros1__169_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \^o23\(4 downto 1),
      O(3) => \euros1__169_carry__0_n_4\,
      O(2) => \euros1__169_carry__0_n_5\,
      O(1) => \euros1__169_carry__0_n_6\,
      O(0) => \euros1__169_carry__0_n_7\,
      S(3) => \euros1__169_carry__0_i_1_n_0\,
      S(2) => \euros1__169_carry__0_i_2_n_0\,
      S(1) => \euros1__169_carry__0_i_3_n_0\,
      S(0) => \euros1__169_carry__0_i_4_n_0\
    );
\euros1__169_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^o23\(4),
      I1 => \^o23\(7),
      O => \euros1__169_carry__0_i_1_n_0\
    );
\euros1__169_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^o23\(3),
      I1 => \^o23\(6),
      O => \euros1__169_carry__0_i_2_n_0\
    );
\euros1__169_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^o23\(2),
      I1 => \^o23\(5),
      O => \euros1__169_carry__0_i_3_n_0\
    );
\euros1__169_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^o23\(1),
      I1 => \^o23\(4),
      O => \euros1__169_carry__0_i_4_n_0\
    );
\euros1__169_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__169_carry__0_n_0\,
      CO(3) => \euros1__169_carry__1_n_0\,
      CO(2) => \euros1__169_carry__1_n_1\,
      CO(1) => \euros1__169_carry__1_n_2\,
      CO(0) => \euros1__169_carry__1_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__169_carry__1_i_1_n_7\,
      DI(2 downto 0) => \^o23\(7 downto 5),
      O(3) => \euros1__169_carry__1_n_4\,
      O(2) => \euros1__169_carry__1_n_5\,
      O(1) => \euros1__169_carry__1_n_6\,
      O(0) => \euros1__169_carry__1_n_7\,
      S(3) => \euros1__169_carry__1_i_2_n_0\,
      S(2) => \euros1__169_carry__1_i_3_n_0\,
      S(1) => \euros1__169_carry__1_i_4_n_0\,
      S(0) => \euros1__169_carry__1_i_5_n_0\
    );
\euros1__169_carry__1_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tot_OBUF[7]_inst_i_1_n_0\,
      CO(3) => \euros1__169_carry__1_i_1_n_0\,
      CO(2) => \euros1__169_carry__1_i_1_n_1\,
      CO(1) => \euros1__169_carry__1_i_1_n_2\,
      CO(0) => \euros1__169_carry__1_i_1_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__169_carry__1_i_1_n_4\,
      DI(2) => \euros1__169_carry__1_i_1_n_5\,
      DI(1) => \euros1__169_carry__1_i_1_n_6\,
      DI(0) => \euros1__169_carry__1_i_1_n_7\,
      O(3) => \euros1__169_carry__1_i_1_n_4\,
      O(2) => \euros1__169_carry__1_i_1_n_5\,
      O(1) => \euros1__169_carry__1_i_1_n_6\,
      O(0) => \euros1__169_carry__1_i_1_n_7\,
      S(3) => \euros1__169_carry__1_i_6_n_0\,
      S(2) => \euros1__169_carry__1_i_7_n_0\,
      S(1) => \euros1__169_carry__1_i_8_n_0\,
      S(0) => \euros1__169_carry__1_i_9_n_0\
    );
\euros1__169_carry__1_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_7\,
      I1 => \euros1__169_carry__1_i_1_n_4\,
      O => \euros1__169_carry__1_i_2_n_0\
    );
\euros1__169_carry__1_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^o23\(7),
      I1 => \euros1__169_carry__1_i_1_n_5\,
      O => \euros1__169_carry__1_i_3_n_0\
    );
\euros1__169_carry__1_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^o23\(6),
      I1 => \euros1__169_carry__1_i_1_n_6\,
      O => \euros1__169_carry__1_i_4_n_0\
    );
\euros1__169_carry__1_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^o23\(5),
      I1 => \euros1__169_carry__1_i_1_n_7\,
      O => \euros1__169_carry__1_i_5_n_0\
    );
\euros1__169_carry__1_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_4\,
      I1 => valor_moneda_IBUF(3),
      O => \euros1__169_carry__1_i_6_n_0\
    );
\euros1__169_carry__1_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_5\,
      I1 => valor_moneda_IBUF(2),
      O => \euros1__169_carry__1_i_7_n_0\
    );
\euros1__169_carry__1_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_6\,
      I1 => valor_moneda_IBUF(1),
      O => \euros1__169_carry__1_i_8_n_0\
    );
\euros1__169_carry__1_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_7\,
      I1 => valor_moneda_IBUF(0),
      O => \euros1__169_carry__1_i_9_n_0\
    );
\euros1__169_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__169_carry__1_n_0\,
      CO(3) => \euros1__169_carry__2_n_0\,
      CO(2) => \euros1__169_carry__2_n_1\,
      CO(1) => \euros1__169_carry__2_n_2\,
      CO(0) => \euros1__169_carry__2_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__169_carry__2_i_1_n_7\,
      DI(2) => \euros1__169_carry__1_i_1_n_4\,
      DI(1) => \euros1__169_carry__1_i_1_n_5\,
      DI(0) => \euros1__169_carry__1_i_1_n_6\,
      O(3) => \euros1__169_carry__2_n_4\,
      O(2) => \euros1__169_carry__2_n_5\,
      O(1) => \euros1__169_carry__2_n_6\,
      O(0) => \euros1__169_carry__2_n_7\,
      S(3) => \euros1__169_carry__2_i_2_n_0\,
      S(2) => \euros1__169_carry__2_i_3_n_0\,
      S(1) => \euros1__169_carry__2_i_4_n_0\,
      S(0) => \euros1__169_carry__2_i_5_n_0\
    );
\euros1__169_carry__2_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__169_carry__1_i_1_n_0\,
      CO(3) => \euros1__169_carry__2_i_1_n_0\,
      CO(2) => \euros1__169_carry__2_i_1_n_1\,
      CO(1) => \euros1__169_carry__2_i_1_n_2\,
      CO(0) => \euros1__169_carry__2_i_1_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__169_carry__2_i_1_n_4\,
      DI(2) => \euros1__169_carry__2_i_1_n_5\,
      DI(1) => \euros1__169_carry__2_i_1_n_6\,
      DI(0) => \euros1__169_carry__2_i_1_n_7\,
      O(3) => \euros1__169_carry__2_i_1_n_4\,
      O(2) => \euros1__169_carry__2_i_1_n_5\,
      O(1) => \euros1__169_carry__2_i_1_n_6\,
      O(0) => \euros1__169_carry__2_i_1_n_7\,
      S(3) => \euros1__169_carry__2_i_6_n_0\,
      S(2) => \euros1__169_carry__2_i_7_n_0\,
      S(1) => \euros1__169_carry__2_i_8_n_0\,
      S(0) => \euros1__169_carry__2_i_9_n_0\
    );
\euros1__169_carry__2_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \euros1__169_carry__2_i_1_n_7\,
      I1 => \euros1__169_carry__2_i_1_n_4\,
      O => \euros1__169_carry__2_i_2_n_0\
    );
\euros1__169_carry__2_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_4\,
      I1 => \euros1__169_carry__2_i_1_n_5\,
      O => \euros1__169_carry__2_i_3_n_0\
    );
\euros1__169_carry__2_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_5\,
      I1 => \euros1__169_carry__2_i_1_n_6\,
      O => \euros1__169_carry__2_i_4_n_0\
    );
\euros1__169_carry__2_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_6\,
      I1 => \euros1__169_carry__2_i_1_n_7\,
      O => \euros1__169_carry__2_i_5_n_0\
    );
\euros1__169_carry__2_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \euros1__169_carry__2_i_1_n_4\,
      I1 => valor_moneda_IBUF(7),
      O => \euros1__169_carry__2_i_6_n_0\
    );
\euros1__169_carry__2_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \euros1__169_carry__2_i_1_n_5\,
      I1 => valor_moneda_IBUF(6),
      O => \euros1__169_carry__2_i_7_n_0\
    );
\euros1__169_carry__2_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \euros1__169_carry__2_i_1_n_6\,
      I1 => valor_moneda_IBUF(5),
      O => \euros1__169_carry__2_i_8_n_0\
    );
\euros1__169_carry__2_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \euros1__169_carry__2_i_1_n_7\,
      I1 => valor_moneda_IBUF(4),
      O => \euros1__169_carry__2_i_9_n_0\
    );
\euros1__169_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__169_carry__2_n_0\,
      CO(3) => \euros1__169_carry__3_n_0\,
      CO(2) => \euros1__169_carry__3_n_1\,
      CO(1) => \euros1__169_carry__3_n_2\,
      CO(0) => \euros1__169_carry__3_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__169_carry__3_i_1_n_7\,
      DI(2) => \euros1__169_carry__2_i_1_n_4\,
      DI(1) => \euros1__169_carry__2_i_1_n_5\,
      DI(0) => \euros1__169_carry__2_i_1_n_6\,
      O(3) => \euros1__169_carry__3_n_4\,
      O(2) => \euros1__169_carry__3_n_5\,
      O(1) => \euros1__169_carry__3_n_6\,
      O(0) => \euros1__169_carry__3_n_7\,
      S(3) => \euros1__169_carry__3_i_2_n_0\,
      S(2) => \euros1__169_carry__3_i_3_n_0\,
      S(1) => \euros1__169_carry__3_i_4_n_0\,
      S(0) => \euros1__169_carry__3_i_5_n_0\
    );
\euros1__169_carry__3_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__169_carry__2_i_1_n_0\,
      CO(3) => \euros1__169_carry__3_i_1_n_0\,
      CO(2) => \euros1__169_carry__3_i_1_n_1\,
      CO(1) => \euros1__169_carry__3_i_1_n_2\,
      CO(0) => \euros1__169_carry__3_i_1_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__169_carry__3_i_1_n_4\,
      DI(2) => \euros1__169_carry__3_i_1_n_5\,
      DI(1) => \euros1__169_carry__3_i_1_n_6\,
      DI(0) => \euros1__169_carry__3_i_1_n_7\,
      O(3) => \euros1__169_carry__3_i_1_n_4\,
      O(2) => \euros1__169_carry__3_i_1_n_5\,
      O(1) => \euros1__169_carry__3_i_1_n_6\,
      O(0) => \euros1__169_carry__3_i_1_n_7\,
      S(3) => \euros1__169_carry__3_i_6_n_0\,
      S(2) => \euros1__169_carry__3_i_7_n_0\,
      S(1) => \euros1__169_carry__3_i_8_n_0\,
      S(0) => \euros1__169_carry__3_i_9_n_0\
    );
\euros1__169_carry__3_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \euros1__169_carry__3_i_1_n_7\,
      I1 => \euros1__169_carry__3_i_1_n_4\,
      O => \euros1__169_carry__3_i_2_n_0\
    );
\euros1__169_carry__3_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \euros1__169_carry__2_i_1_n_4\,
      I1 => \euros1__169_carry__3_i_1_n_5\,
      O => \euros1__169_carry__3_i_3_n_0\
    );
\euros1__169_carry__3_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \euros1__169_carry__2_i_1_n_5\,
      I1 => \euros1__169_carry__3_i_1_n_6\,
      O => \euros1__169_carry__3_i_4_n_0\
    );
\euros1__169_carry__3_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \euros1__169_carry__2_i_1_n_6\,
      I1 => \euros1__169_carry__3_i_1_n_7\,
      O => \euros1__169_carry__3_i_5_n_0\
    );
\euros1__169_carry__3_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \euros1__169_carry__3_i_1_n_4\,
      I1 => valor_moneda_IBUF(11),
      O => \euros1__169_carry__3_i_6_n_0\
    );
\euros1__169_carry__3_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \euros1__169_carry__3_i_1_n_5\,
      I1 => valor_moneda_IBUF(10),
      O => \euros1__169_carry__3_i_7_n_0\
    );
\euros1__169_carry__3_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \euros1__169_carry__3_i_1_n_6\,
      I1 => valor_moneda_IBUF(9),
      O => \euros1__169_carry__3_i_8_n_0\
    );
\euros1__169_carry__3_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \euros1__169_carry__3_i_1_n_7\,
      I1 => valor_moneda_IBUF(8),
      O => \euros1__169_carry__3_i_9_n_0\
    );
\euros1__169_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__169_carry__3_n_0\,
      CO(3 downto 1) => \NLW_euros1__169_carry__4_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \euros1__169_carry__4_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \euros1__169_carry__3_i_1_n_6\,
      O(3 downto 2) => \NLW_euros1__169_carry__4_O_UNCONNECTED\(3 downto 2),
      O(1) => \euros1__169_carry__4_n_6\,
      O(0) => \euros1__169_carry__4_n_7\,
      S(3 downto 2) => B"00",
      S(1) => \euros1__169_carry__4_i_1_n_0\,
      S(0) => \euros1__169_carry__4_i_2_n_0\
    );
\euros1__169_carry__4_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \euros1__169_carry__3_i_1_n_5\,
      I1 => \euros1__1_carry__3_i_9_n_6\,
      O => \euros1__169_carry__4_i_1_n_0\
    );
\euros1__169_carry__4_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \euros1__169_carry__3_i_1_n_6\,
      I1 => \euros1__1_carry__3_i_9_n_7\,
      O => \euros1__169_carry__4_i_2_n_0\
    );
\euros1__169_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^o23\(0),
      I1 => \^o23\(3),
      O => \euros1__169_carry_i_1_n_0\
    );
\euros1__169_carry_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^o23\(2),
      O => \euros1__169_carry_i_2_n_0\
    );
\euros1__169_carry_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^o23\(1),
      O => \euros1__169_carry_i_3_n_0\
    );
\euros1__1_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \euros1__1_carry_n_0\,
      CO(2) => \euros1__1_carry_n_1\,
      CO(1) => \euros1__1_carry_n_2\,
      CO(0) => \euros1__1_carry_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => \^o23\(4 downto 2),
      DI(0) => '0',
      O(3 downto 0) => \NLW_euros1__1_carry_O_UNCONNECTED\(3 downto 0),
      S(3) => \euros1__1_carry_i_1_n_0\,
      S(2) => \euros1__1_carry_i_2_n_0\,
      S(1) => \euros1__1_carry_i_3_n_0\,
      S(0) => \^o23\(1)
    );
\euros1__1_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__1_carry_n_0\,
      CO(3) => \euros1__1_carry__0_n_0\,
      CO(2) => \euros1__1_carry__0_n_1\,
      CO(1) => \euros1__1_carry__0_n_2\,
      CO(0) => \euros1__1_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__1_carry__0_i_1_n_0\,
      DI(2 downto 0) => \^o23\(7 downto 5),
      O(3) => \euros1__1_carry__0_n_4\,
      O(2 downto 0) => \NLW_euros1__1_carry__0_O_UNCONNECTED\(2 downto 0),
      S(3) => \euros1__1_carry__0_i_2_n_0\,
      S(2) => \euros1__1_carry__0_i_3_n_0\,
      S(1) => \euros1__1_carry__0_i_4_n_0\,
      S(0) => \euros1__1_carry__0_i_5_n_0\
    );
\euros1__1_carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_7\,
      I1 => \^o23\(6),
      I2 => \^o23\(1),
      O => \euros1__1_carry__0_i_1_n_0\
    );
\euros1__1_carry__0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => \^o23\(6),
      I1 => \euros1__169_carry__1_i_1_n_7\,
      I2 => \^o23\(1),
      I3 => \^o23\(0),
      I4 => \^o23\(5),
      O => \euros1__1_carry__0_i_2_n_0\
    );
\euros1__1_carry__0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \^o23\(0),
      I1 => \^o23\(5),
      I2 => \^o23\(7),
      O => \euros1__1_carry__0_i_3_n_0\
    );
\euros1__1_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^o23\(6),
      I1 => \^o23\(4),
      O => \euros1__1_carry__0_i_4_n_0\
    );
\euros1__1_carry__0_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^o23\(5),
      I1 => \^o23\(3),
      O => \euros1__1_carry__0_i_5_n_0\
    );
\euros1__1_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__1_carry__0_n_0\,
      CO(3) => \euros1__1_carry__1_n_0\,
      CO(2) => \euros1__1_carry__1_n_1\,
      CO(1) => \euros1__1_carry__1_n_2\,
      CO(0) => \euros1__1_carry__1_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__1_carry__1_i_1_n_0\,
      DI(2) => \euros1__1_carry__1_i_2_n_0\,
      DI(1) => \euros1__1_carry__1_i_3_n_0\,
      DI(0) => \euros1__1_carry__1_i_4_n_0\,
      O(3) => \euros1__1_carry__1_n_4\,
      O(2) => \euros1__1_carry__1_n_5\,
      O(1) => \euros1__1_carry__1_n_6\,
      O(0) => \euros1__1_carry__1_n_7\,
      S(3) => \euros1__1_carry__1_i_5_n_0\,
      S(2) => \euros1__1_carry__1_i_6_n_0\,
      S(1) => \euros1__1_carry__1_i_7_n_0\,
      S(0) => \euros1__1_carry__1_i_8_n_0\
    );
\euros1__1_carry__1_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_6\,
      I1 => \euros1__169_carry__1_i_1_n_4\,
      I2 => \^o23\(4),
      O => \euros1__1_carry__1_i_1_n_0\
    );
\euros1__1_carry__1_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_7\,
      I1 => \euros1__169_carry__1_i_1_n_5\,
      I2 => \^o23\(3),
      O => \euros1__1_carry__1_i_2_n_0\
    );
\euros1__1_carry__1_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \^o23\(7),
      I1 => \euros1__169_carry__1_i_1_n_6\,
      I2 => \^o23\(2),
      O => \euros1__1_carry__1_i_3_n_0\
    );
\euros1__1_carry__1_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \^o23\(6),
      I1 => \euros1__169_carry__1_i_1_n_7\,
      I2 => \^o23\(1),
      O => \euros1__1_carry__1_i_4_n_0\
    );
\euros1__1_carry__1_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_5\,
      I1 => \euros1__169_carry__2_i_1_n_7\,
      I2 => \^o23\(5),
      I3 => \euros1__1_carry__1_i_1_n_0\,
      O => \euros1__1_carry__1_i_5_n_0\
    );
\euros1__1_carry__1_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_6\,
      I1 => \euros1__169_carry__1_i_1_n_4\,
      I2 => \^o23\(4),
      I3 => \euros1__1_carry__1_i_2_n_0\,
      O => \euros1__1_carry__1_i_6_n_0\
    );
\euros1__1_carry__1_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_7\,
      I1 => \euros1__169_carry__1_i_1_n_5\,
      I2 => \^o23\(3),
      I3 => \euros1__1_carry__1_i_3_n_0\,
      O => \euros1__1_carry__1_i_7_n_0\
    );
\euros1__1_carry__1_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \^o23\(7),
      I1 => \euros1__169_carry__1_i_1_n_6\,
      I2 => \^o23\(2),
      I3 => \euros1__1_carry__1_i_4_n_0\,
      O => \euros1__1_carry__1_i_8_n_0\
    );
\euros1__1_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__1_carry__1_n_0\,
      CO(3) => \euros1__1_carry__2_n_0\,
      CO(2) => \euros1__1_carry__2_n_1\,
      CO(1) => \euros1__1_carry__2_n_2\,
      CO(0) => \euros1__1_carry__2_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__1_carry__2_i_1_n_0\,
      DI(2) => \euros1__1_carry__2_i_2_n_0\,
      DI(1) => \euros1__1_carry__2_i_3_n_0\,
      DI(0) => \euros1__1_carry__2_i_4_n_0\,
      O(3) => \euros1__1_carry__2_n_4\,
      O(2) => \euros1__1_carry__2_n_5\,
      O(1) => \euros1__1_carry__2_n_6\,
      O(0) => \euros1__1_carry__2_n_7\,
      S(3) => \euros1__1_carry__2_i_5_n_0\,
      S(2) => \euros1__1_carry__2_i_6_n_0\,
      S(1) => \euros1__1_carry__2_i_7_n_0\,
      S(0) => \euros1__1_carry__2_i_8_n_0\
    );
\euros1__1_carry__2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \euros1__169_carry__2_i_1_n_6\,
      I1 => \euros1__169_carry__2_i_1_n_4\,
      I2 => \euros1__169_carry__1_i_1_n_7\,
      O => \euros1__1_carry__2_i_1_n_0\
    );
\euros1__1_carry__2_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \euros1__169_carry__2_i_1_n_7\,
      I1 => \euros1__169_carry__2_i_1_n_5\,
      I2 => \^o23\(7),
      O => \euros1__1_carry__2_i_2_n_0\
    );
\euros1__1_carry__2_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \^o23\(6),
      I1 => \euros1__169_carry__2_i_1_n_6\,
      I2 => \euros1__169_carry__1_i_1_n_4\,
      O => \euros1__1_carry__2_i_3_n_0\
    );
\euros1__1_carry__2_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_5\,
      I1 => \euros1__169_carry__2_i_1_n_7\,
      I2 => \^o23\(5),
      O => \euros1__1_carry__2_i_4_n_0\
    );
\euros1__1_carry__2_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_6\,
      I1 => \euros1__169_carry__3_i_1_n_7\,
      I2 => \euros1__169_carry__2_i_1_n_5\,
      I3 => \euros1__1_carry__2_i_1_n_0\,
      O => \euros1__1_carry__2_i_5_n_0\
    );
\euros1__1_carry__2_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__169_carry__2_i_1_n_6\,
      I1 => \euros1__169_carry__2_i_1_n_4\,
      I2 => \euros1__169_carry__1_i_1_n_7\,
      I3 => \euros1__1_carry__2_i_2_n_0\,
      O => \euros1__1_carry__2_i_6_n_0\
    );
\euros1__1_carry__2_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__169_carry__2_i_1_n_7\,
      I1 => \euros1__169_carry__2_i_1_n_5\,
      I2 => \^o23\(7),
      I3 => \euros1__1_carry__2_i_3_n_0\,
      O => \euros1__1_carry__2_i_7_n_0\
    );
\euros1__1_carry__2_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \^o23\(6),
      I1 => \euros1__169_carry__2_i_1_n_6\,
      I2 => \euros1__169_carry__1_i_1_n_4\,
      I3 => \euros1__1_carry__2_i_4_n_0\,
      O => \euros1__1_carry__2_i_8_n_0\
    );
\euros1__1_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__1_carry__2_n_0\,
      CO(3) => \euros1__1_carry__3_n_0\,
      CO(2) => \euros1__1_carry__3_n_1\,
      CO(1) => \euros1__1_carry__3_n_2\,
      CO(0) => \euros1__1_carry__3_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__1_carry__3_i_1_n_0\,
      DI(2) => \euros1__1_carry__3_i_2_n_0\,
      DI(1) => \euros1__1_carry__3_i_3_n_0\,
      DI(0) => \euros1__1_carry__3_i_4_n_0\,
      O(3) => \euros1__1_carry__3_n_4\,
      O(2) => \euros1__1_carry__3_n_5\,
      O(1) => \euros1__1_carry__3_n_6\,
      O(0) => \euros1__1_carry__3_n_7\,
      S(3) => \euros1__1_carry__3_i_5_n_0\,
      S(2) => \euros1__1_carry__3_i_6_n_0\,
      S(1) => \euros1__1_carry__3_i_7_n_0\,
      S(0) => \euros1__1_carry__3_i_8_n_0\
    );
\euros1__1_carry__3_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \euros1__169_carry__3_i_1_n_6\,
      I1 => \euros1__169_carry__3_i_1_n_4\,
      I2 => \euros1__169_carry__2_i_1_n_7\,
      O => \euros1__1_carry__3_i_1_n_0\
    );
\euros1__1_carry__3_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \euros1__1_carry__3_i_9_n_4\,
      I1 => valor_moneda_IBUF(15),
      O => \euros1__1_carry__3_i_10_n_0\
    );
\euros1__1_carry__3_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \euros1__1_carry__3_i_9_n_5\,
      I1 => valor_moneda_IBUF(14),
      O => \euros1__1_carry__3_i_11_n_0\
    );
\euros1__1_carry__3_i_12\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \euros1__1_carry__3_i_9_n_6\,
      I1 => valor_moneda_IBUF(13),
      O => \euros1__1_carry__3_i_12_n_0\
    );
\euros1__1_carry__3_i_13\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \euros1__1_carry__3_i_9_n_7\,
      I1 => valor_moneda_IBUF(12),
      O => \euros1__1_carry__3_i_13_n_0\
    );
\euros1__1_carry__3_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \euros1__169_carry__3_i_1_n_7\,
      I1 => \euros1__169_carry__3_i_1_n_5\,
      I2 => \euros1__169_carry__1_i_1_n_4\,
      O => \euros1__1_carry__3_i_2_n_0\
    );
\euros1__1_carry__3_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_5\,
      I1 => \euros1__169_carry__3_i_1_n_6\,
      I2 => \euros1__169_carry__2_i_1_n_4\,
      O => \euros1__1_carry__3_i_3_n_0\
    );
\euros1__1_carry__3_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_6\,
      I1 => \euros1__169_carry__3_i_1_n_7\,
      I2 => \euros1__169_carry__2_i_1_n_5\,
      O => \euros1__1_carry__3_i_4_n_0\
    );
\euros1__1_carry__3_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__169_carry__2_i_1_n_6\,
      I1 => \euros1__1_carry__3_i_9_n_7\,
      I2 => \euros1__169_carry__3_i_1_n_5\,
      I3 => \euros1__1_carry__3_i_1_n_0\,
      O => \euros1__1_carry__3_i_5_n_0\
    );
\euros1__1_carry__3_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__169_carry__3_i_1_n_6\,
      I1 => \euros1__169_carry__3_i_1_n_4\,
      I2 => \euros1__169_carry__2_i_1_n_7\,
      I3 => \euros1__1_carry__3_i_2_n_0\,
      O => \euros1__1_carry__3_i_6_n_0\
    );
\euros1__1_carry__3_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__169_carry__3_i_1_n_7\,
      I1 => \euros1__169_carry__3_i_1_n_5\,
      I2 => \euros1__169_carry__1_i_1_n_4\,
      I3 => \euros1__1_carry__3_i_3_n_0\,
      O => \euros1__1_carry__3_i_7_n_0\
    );
\euros1__1_carry__3_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_5\,
      I1 => \euros1__169_carry__3_i_1_n_6\,
      I2 => \euros1__169_carry__2_i_1_n_4\,
      I3 => \euros1__1_carry__3_i_4_n_0\,
      O => \euros1__1_carry__3_i_8_n_0\
    );
\euros1__1_carry__3_i_9\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__169_carry__3_i_1_n_0\,
      CO(3) => \euros1__1_carry__3_i_9_n_0\,
      CO(2) => \euros1__1_carry__3_i_9_n_1\,
      CO(1) => \euros1__1_carry__3_i_9_n_2\,
      CO(0) => \euros1__1_carry__3_i_9_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__1_carry__3_i_9_n_4\,
      DI(2) => \euros1__1_carry__3_i_9_n_5\,
      DI(1) => \euros1__1_carry__3_i_9_n_6\,
      DI(0) => \euros1__1_carry__3_i_9_n_7\,
      O(3) => \euros1__1_carry__3_i_9_n_4\,
      O(2) => \euros1__1_carry__3_i_9_n_5\,
      O(1) => \euros1__1_carry__3_i_9_n_6\,
      O(0) => \euros1__1_carry__3_i_9_n_7\,
      S(3) => \euros1__1_carry__3_i_10_n_0\,
      S(2) => \euros1__1_carry__3_i_11_n_0\,
      S(1) => \euros1__1_carry__3_i_12_n_0\,
      S(0) => \euros1__1_carry__3_i_13_n_0\
    );
\euros1__1_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__1_carry__3_n_0\,
      CO(3) => \euros1__1_carry__4_n_0\,
      CO(2) => \euros1__1_carry__4_n_1\,
      CO(1) => \euros1__1_carry__4_n_2\,
      CO(0) => \euros1__1_carry__4_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__1_carry__4_i_1_n_0\,
      DI(2) => \euros1__1_carry__4_i_2_n_0\,
      DI(1) => \euros1__1_carry__4_i_3_n_0\,
      DI(0) => \euros1__1_carry__4_i_4_n_0\,
      O(3) => \euros1__1_carry__4_n_4\,
      O(2) => \euros1__1_carry__4_n_5\,
      O(1) => \euros1__1_carry__4_n_6\,
      O(0) => \euros1__1_carry__4_n_7\,
      S(3) => \euros1__1_carry__4_i_5_n_0\,
      S(2) => \euros1__1_carry__4_i_6_n_0\,
      S(1) => \euros1__1_carry__4_i_7_n_0\,
      S(0) => \euros1__1_carry__4_i_8_n_0\
    );
\euros1__1_carry__4_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \euros1__1_carry__3_i_9_n_6\,
      I1 => \euros1__1_carry__3_i_9_n_4\,
      I2 => \euros1__169_carry__3_i_1_n_7\,
      O => \euros1__1_carry__4_i_1_n_0\
    );
\euros1__1_carry__4_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \euros1__1_carry__3_i_9_n_7\,
      I1 => \euros1__1_carry__3_i_9_n_5\,
      I2 => \euros1__169_carry__2_i_1_n_4\,
      O => \euros1__1_carry__4_i_2_n_0\
    );
\euros1__1_carry__4_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \euros1__169_carry__3_i_1_n_4\,
      I1 => \euros1__1_carry__3_i_9_n_6\,
      I2 => \euros1__169_carry__2_i_1_n_5\,
      O => \euros1__1_carry__4_i_3_n_0\
    );
\euros1__1_carry__4_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \euros1__169_carry__2_i_1_n_6\,
      I1 => \euros1__1_carry__3_i_9_n_7\,
      I2 => \euros1__169_carry__3_i_1_n_5\,
      O => \euros1__1_carry__4_i_4_n_0\
    );
\euros1__1_carry__4_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__1_carry__3_i_9_n_5\,
      I1 => \euros1__1_carry__7_i_1_n_7\,
      I2 => \euros1__169_carry__3_i_1_n_6\,
      I3 => \euros1__1_carry__4_i_1_n_0\,
      O => \euros1__1_carry__4_i_5_n_0\
    );
\euros1__1_carry__4_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__1_carry__3_i_9_n_6\,
      I1 => \euros1__1_carry__3_i_9_n_4\,
      I2 => \euros1__169_carry__3_i_1_n_7\,
      I3 => \euros1__1_carry__4_i_2_n_0\,
      O => \euros1__1_carry__4_i_6_n_0\
    );
\euros1__1_carry__4_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__1_carry__3_i_9_n_7\,
      I1 => \euros1__1_carry__3_i_9_n_5\,
      I2 => \euros1__169_carry__2_i_1_n_4\,
      I3 => \euros1__1_carry__4_i_3_n_0\,
      O => \euros1__1_carry__4_i_7_n_0\
    );
\euros1__1_carry__4_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__169_carry__3_i_1_n_4\,
      I1 => \euros1__1_carry__3_i_9_n_6\,
      I2 => \euros1__169_carry__2_i_1_n_5\,
      I3 => \euros1__1_carry__4_i_4_n_0\,
      O => \euros1__1_carry__4_i_8_n_0\
    );
\euros1__1_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__1_carry__4_n_0\,
      CO(3) => \euros1__1_carry__5_n_0\,
      CO(2) => \euros1__1_carry__5_n_1\,
      CO(1) => \euros1__1_carry__5_n_2\,
      CO(0) => \euros1__1_carry__5_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__1_carry__5_i_1_n_0\,
      DI(2) => \euros1__1_carry__5_i_2_n_0\,
      DI(1) => \euros1__1_carry__5_i_3_n_0\,
      DI(0) => \euros1__1_carry__5_i_4_n_0\,
      O(3) => \euros1__1_carry__5_n_4\,
      O(2) => \euros1__1_carry__5_n_5\,
      O(1) => \euros1__1_carry__5_n_6\,
      O(0) => \euros1__1_carry__5_n_7\,
      S(3) => \euros1__1_carry__5_i_5_n_0\,
      S(2) => \euros1__1_carry__5_i_6_n_0\,
      S(1) => \euros1__1_carry__5_i_7_n_0\,
      S(0) => \euros1__1_carry__5_i_8_n_0\
    );
\euros1__1_carry__5_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \euros1__1_carry__3_i_9_n_7\,
      I1 => \euros1__1_carry__7_i_1_n_4\,
      I2 => \euros1__1_carry__7_i_1_n_6\,
      O => \euros1__1_carry__5_i_1_n_0\
    );
\euros1__1_carry__5_i_10\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \euros1__1_carry__5_i_9_n_5\,
      I1 => valor_moneda_IBUF(22),
      O => \euros1__1_carry__5_i_10_n_0\
    );
\euros1__1_carry__5_i_11\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \euros1__1_carry__5_i_9_n_6\,
      I1 => valor_moneda_IBUF(21),
      O => \euros1__1_carry__5_i_11_n_0\
    );
\euros1__1_carry__5_i_12\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \euros1__1_carry__5_i_9_n_7\,
      I1 => valor_moneda_IBUF(20),
      O => \euros1__1_carry__5_i_12_n_0\
    );
\euros1__1_carry__5_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \euros1__169_carry__3_i_1_n_4\,
      I1 => \euros1__1_carry__7_i_1_n_5\,
      I2 => \euros1__1_carry__7_i_1_n_7\,
      O => \euros1__1_carry__5_i_2_n_0\
    );
\euros1__1_carry__5_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \euros1__169_carry__3_i_1_n_5\,
      I1 => \euros1__1_carry__7_i_1_n_6\,
      I2 => \euros1__1_carry__3_i_9_n_4\,
      O => \euros1__1_carry__5_i_3_n_0\
    );
\euros1__1_carry__5_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \euros1__1_carry__3_i_9_n_5\,
      I1 => \euros1__1_carry__7_i_1_n_7\,
      I2 => \euros1__169_carry__3_i_1_n_6\,
      O => \euros1__1_carry__5_i_4_n_0\
    );
\euros1__1_carry__5_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__1_carry__3_i_9_n_6\,
      I1 => \euros1__1_carry__5_i_9_n_7\,
      I2 => \euros1__1_carry__7_i_1_n_5\,
      I3 => \euros1__1_carry__5_i_1_n_0\,
      O => \euros1__1_carry__5_i_5_n_0\
    );
\euros1__1_carry__5_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__1_carry__3_i_9_n_7\,
      I1 => \euros1__1_carry__7_i_1_n_4\,
      I2 => \euros1__1_carry__7_i_1_n_6\,
      I3 => \euros1__1_carry__5_i_2_n_0\,
      O => \euros1__1_carry__5_i_6_n_0\
    );
\euros1__1_carry__5_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__169_carry__3_i_1_n_4\,
      I1 => \euros1__1_carry__7_i_1_n_5\,
      I2 => \euros1__1_carry__7_i_1_n_7\,
      I3 => \euros1__1_carry__5_i_3_n_0\,
      O => \euros1__1_carry__5_i_7_n_0\
    );
\euros1__1_carry__5_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__169_carry__3_i_1_n_5\,
      I1 => \euros1__1_carry__7_i_1_n_6\,
      I2 => \euros1__1_carry__3_i_9_n_4\,
      I3 => \euros1__1_carry__5_i_4_n_0\,
      O => \euros1__1_carry__5_i_8_n_0\
    );
\euros1__1_carry__5_i_9\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__1_carry__7_i_1_n_0\,
      CO(3 downto 2) => \NLW_euros1__1_carry__5_i_9_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \euros1__1_carry__5_i_9_n_2\,
      CO(0) => \euros1__1_carry__5_i_9_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \euros1__1_carry__5_i_9_n_6\,
      DI(0) => \euros1__1_carry__5_i_9_n_7\,
      O(3) => \NLW_euros1__1_carry__5_i_9_O_UNCONNECTED\(3),
      O(2) => \euros1__1_carry__5_i_9_n_5\,
      O(1) => \euros1__1_carry__5_i_9_n_6\,
      O(0) => \euros1__1_carry__5_i_9_n_7\,
      S(3) => '0',
      S(2) => \euros1__1_carry__5_i_10_n_0\,
      S(1) => \euros1__1_carry__5_i_11_n_0\,
      S(0) => \euros1__1_carry__5_i_12_n_0\
    );
\euros1__1_carry__6\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__1_carry__5_n_0\,
      CO(3) => \euros1__1_carry__6_n_0\,
      CO(2) => \euros1__1_carry__6_n_1\,
      CO(1) => \euros1__1_carry__6_n_2\,
      CO(0) => \euros1__1_carry__6_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__1_carry__6_i_1_n_0\,
      DI(2) => \euros1__1_carry__6_i_2_n_0\,
      DI(1) => \euros1__1_carry__6_i_3_n_0\,
      DI(0) => \euros1__1_carry__6_i_4_n_0\,
      O(3) => \euros1__1_carry__6_n_4\,
      O(2) => \euros1__1_carry__6_n_5\,
      O(1) => \euros1__1_carry__6_n_6\,
      O(0) => \euros1__1_carry__6_n_7\,
      S(3) => \euros1__1_carry__6_i_5_n_0\,
      S(2) => \euros1__1_carry__6_i_6_n_0\,
      S(1) => \euros1__1_carry__6_i_7_n_0\,
      S(0) => \euros1__1_carry__6_i_8_n_0\
    );
\euros1__1_carry__6_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \euros1__1_carry__7_i_1_n_7\,
      I1 => \euros1__1_carry__5_i_9_n_6\,
      O => \euros1__1_carry__6_i_1_n_0\
    );
\euros1__1_carry__6_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \euros1__1_carry__3_i_9_n_4\,
      I1 => \euros1__1_carry__5_i_9_n_5\,
      I2 => \euros1__1_carry__5_i_9_n_7\,
      O => \euros1__1_carry__6_i_2_n_0\
    );
\euros1__1_carry__6_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \euros1__1_carry__3_i_9_n_5\,
      I1 => \euros1__1_carry__5_i_9_n_6\,
      I2 => \euros1__1_carry__7_i_1_n_4\,
      O => \euros1__1_carry__6_i_3_n_0\
    );
\euros1__1_carry__6_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \euros1__1_carry__3_i_9_n_6\,
      I1 => \euros1__1_carry__5_i_9_n_7\,
      I2 => \euros1__1_carry__7_i_1_n_5\,
      O => \euros1__1_carry__6_i_4_n_0\
    );
\euros1__1_carry__6_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \euros1__1_carry__5_i_9_n_6\,
      I1 => \euros1__1_carry__7_i_1_n_7\,
      I2 => \euros1__1_carry__5_i_9_n_5\,
      I3 => \euros1__1_carry__7_i_1_n_6\,
      O => \euros1__1_carry__6_i_5_n_0\
    );
\euros1__1_carry__6_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E81717E8"
    )
        port map (
      I0 => \euros1__1_carry__5_i_9_n_7\,
      I1 => \euros1__1_carry__5_i_9_n_5\,
      I2 => \euros1__1_carry__3_i_9_n_4\,
      I3 => \euros1__1_carry__5_i_9_n_6\,
      I4 => \euros1__1_carry__7_i_1_n_7\,
      O => \euros1__1_carry__6_i_6_n_0\
    );
\euros1__1_carry__6_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__1_carry__6_i_3_n_0\,
      I1 => \euros1__1_carry__3_i_9_n_4\,
      I2 => \euros1__1_carry__5_i_9_n_5\,
      I3 => \euros1__1_carry__5_i_9_n_7\,
      O => \euros1__1_carry__6_i_7_n_0\
    );
\euros1__1_carry__6_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__1_carry__3_i_9_n_5\,
      I1 => \euros1__1_carry__5_i_9_n_6\,
      I2 => \euros1__1_carry__7_i_1_n_4\,
      I3 => \euros1__1_carry__6_i_4_n_0\,
      O => \euros1__1_carry__6_i_8_n_0\
    );
\euros1__1_carry__7\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__1_carry__6_n_0\,
      CO(3 downto 1) => \NLW_euros1__1_carry__7_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \euros1__1_carry__7_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \euros1__1_carry__7_i_1_n_5\,
      O(3 downto 2) => \NLW_euros1__1_carry__7_O_UNCONNECTED\(3 downto 2),
      O(1) => \euros1__1_carry__7_n_6\,
      O(0) => \euros1__1_carry__7_n_7\,
      S(3 downto 2) => B"00",
      S(1) => \euros1__1_carry__7_i_1_n_4\,
      S(0) => \euros1__1_carry__7_i_2_n_0\
    );
\euros1__1_carry__7_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__1_carry__3_i_9_n_0\,
      CO(3) => \euros1__1_carry__7_i_1_n_0\,
      CO(2) => \euros1__1_carry__7_i_1_n_1\,
      CO(1) => \euros1__1_carry__7_i_1_n_2\,
      CO(0) => \euros1__1_carry__7_i_1_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__1_carry__7_i_1_n_4\,
      DI(2) => \euros1__1_carry__7_i_1_n_5\,
      DI(1) => \euros1__1_carry__7_i_1_n_6\,
      DI(0) => \euros1__1_carry__7_i_1_n_7\,
      O(3) => \euros1__1_carry__7_i_1_n_4\,
      O(2) => \euros1__1_carry__7_i_1_n_5\,
      O(1) => \euros1__1_carry__7_i_1_n_6\,
      O(0) => \euros1__1_carry__7_i_1_n_7\,
      S(3) => \euros1__1_carry__7_i_3_n_0\,
      S(2) => \euros1__1_carry__7_i_4_n_0\,
      S(1) => \euros1__1_carry__7_i_5_n_0\,
      S(0) => \euros1__1_carry__7_i_6_n_0\
    );
\euros1__1_carry__7_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \euros1__1_carry__5_i_9_n_5\,
      I1 => \euros1__1_carry__7_i_1_n_6\,
      I2 => \euros1__1_carry__7_i_1_n_5\,
      O => \euros1__1_carry__7_i_2_n_0\
    );
\euros1__1_carry__7_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \euros1__1_carry__7_i_1_n_4\,
      I1 => valor_moneda_IBUF(19),
      O => \euros1__1_carry__7_i_3_n_0\
    );
\euros1__1_carry__7_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \euros1__1_carry__7_i_1_n_5\,
      I1 => valor_moneda_IBUF(18),
      O => \euros1__1_carry__7_i_4_n_0\
    );
\euros1__1_carry__7_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \euros1__1_carry__7_i_1_n_6\,
      I1 => valor_moneda_IBUF(17),
      O => \euros1__1_carry__7_i_5_n_0\
    );
\euros1__1_carry__7_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \euros1__1_carry__7_i_1_n_7\,
      I1 => valor_moneda_IBUF(16),
      O => \euros1__1_carry__7_i_6_n_0\
    );
\euros1__1_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^o23\(4),
      I1 => \^o23\(2),
      O => \euros1__1_carry_i_1_n_0\
    );
\euros1__1_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^o23\(3),
      I1 => \^o23\(1),
      O => \euros1__1_carry_i_2_n_0\
    );
\euros1__1_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^o23\(2),
      I1 => \^o23\(0),
      O => \euros1__1_carry_i_3_n_0\
    );
\euros1__231_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \euros1__231_carry_n_0\,
      CO(2) => \euros1__231_carry_n_1\,
      CO(1) => \euros1__231_carry_n_2\,
      CO(0) => \euros1__231_carry_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => \^o23\(6 downto 4),
      DI(0) => '0',
      O(3) => \euros1__231_carry_n_4\,
      O(2) => \euros1__231_carry_n_5\,
      O(1) => \euros1__231_carry_n_6\,
      O(0) => \euros1__231_carry_n_7\,
      S(3) => \euros1__231_carry_i_1_n_0\,
      S(2) => \euros1__231_carry_i_2_n_0\,
      S(1) => \euros1__231_carry_i_3_n_0\,
      S(0) => \^o23\(3)
    );
\euros1__231_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__231_carry_n_0\,
      CO(3) => \euros1__231_carry__0_n_0\,
      CO(2) => \euros1__231_carry__0_n_1\,
      CO(1) => \euros1__231_carry__0_n_2\,
      CO(0) => \euros1__231_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__231_carry__0_i_1_n_0\,
      DI(2) => \euros1__231_carry__0_i_2_n_0\,
      DI(1) => \euros1__231_carry__0_i_3_n_0\,
      DI(0) => \euros1__231_carry__0_i_4_n_0\,
      O(3) => \euros1__231_carry__0_n_4\,
      O(2) => \euros1__231_carry__0_n_5\,
      O(1) => \euros1__231_carry__0_n_6\,
      O(0) => \euros1__231_carry__0_n_7\,
      S(3) => \euros1__231_carry__0_i_5_n_0\,
      S(2) => \euros1__231_carry__0_i_6_n_0\,
      S(1) => \euros1__231_carry__0_i_7_n_0\,
      S(0) => \euros1__231_carry__0_i_8_n_0\
    );
\euros1__231_carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \^o23\(5),
      I1 => \^o23\(3),
      I2 => \euros1__169_carry__1_i_1_n_6\,
      O => \euros1__231_carry__0_i_1_n_0\
    );
\euros1__231_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_7\,
      I1 => \^o23\(4),
      I2 => \^o23\(2),
      O => \euros1__231_carry__0_i_2_n_0\
    );
\euros1__231_carry__0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \^o23\(1),
      I1 => \^o23\(3),
      I2 => \^o23\(7),
      O => \euros1__231_carry__0_i_3_n_0\
    );
\euros1__231_carry__0_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \^o23\(7),
      I1 => \^o23\(1),
      I2 => \^o23\(3),
      O => \euros1__231_carry__0_i_4_n_0\
    );
\euros1__231_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \^o23\(6),
      I1 => \^o23\(4),
      I2 => \euros1__169_carry__1_i_1_n_5\,
      I3 => \euros1__231_carry__0_i_1_n_0\,
      O => \euros1__231_carry__0_i_5_n_0\
    );
\euros1__231_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \^o23\(5),
      I1 => \^o23\(3),
      I2 => \euros1__169_carry__1_i_1_n_6\,
      I3 => \euros1__231_carry__0_i_2_n_0\,
      O => \euros1__231_carry__0_i_6_n_0\
    );
\euros1__231_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_7\,
      I1 => \^o23\(4),
      I2 => \^o23\(2),
      I3 => \euros1__231_carry__0_i_3_n_0\,
      O => \euros1__231_carry__0_i_7_n_0\
    );
\euros1__231_carry__0_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => \^o23\(1),
      I1 => \^o23\(3),
      I2 => \^o23\(7),
      I3 => \^o23\(2),
      I4 => \^o23\(0),
      O => \euros1__231_carry__0_i_8_n_0\
    );
\euros1__231_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__231_carry__0_n_0\,
      CO(3) => \euros1__231_carry__1_n_0\,
      CO(2) => \euros1__231_carry__1_n_1\,
      CO(1) => \euros1__231_carry__1_n_2\,
      CO(0) => \euros1__231_carry__1_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__231_carry__1_i_1_n_0\,
      DI(2) => \euros1__231_carry__1_i_2_n_0\,
      DI(1) => \euros1__231_carry__1_i_3_n_0\,
      DI(0) => \euros1__231_carry__1_i_4_n_0\,
      O(3) => \euros1__231_carry__1_n_4\,
      O(2) => \euros1__231_carry__1_n_5\,
      O(1) => \euros1__231_carry__1_n_6\,
      O(0) => \euros1__231_carry__1_n_7\,
      S(3) => \euros1__231_carry__1_i_5_n_0\,
      S(2) => \euros1__231_carry__1_i_6_n_0\,
      S(1) => \euros1__231_carry__1_i_7_n_0\,
      S(0) => \euros1__231_carry__1_i_8_n_0\
    );
\euros1__231_carry__1_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \^o23\(7),
      I1 => \euros1__169_carry__1_i_1_n_6\,
      I2 => \euros1__169_carry__2_i_1_n_6\,
      O => \euros1__231_carry__1_i_1_n_0\
    );
\euros1__231_carry__1_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \^o23\(6),
      I1 => \euros1__169_carry__1_i_1_n_7\,
      I2 => \euros1__169_carry__2_i_1_n_7\,
      O => \euros1__231_carry__1_i_2_n_0\
    );
\euros1__231_carry__1_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \^o23\(7),
      I1 => \euros1__169_carry__1_i_1_n_4\,
      I2 => \^o23\(5),
      O => \euros1__231_carry__1_i_3_n_0\
    );
\euros1__231_carry__1_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \^o23\(6),
      I1 => \^o23\(4),
      I2 => \euros1__169_carry__1_i_1_n_5\,
      O => \euros1__231_carry__1_i_4_n_0\
    );
\euros1__231_carry__1_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__169_carry__2_i_1_n_5\,
      I1 => \euros1__169_carry__1_i_1_n_5\,
      I2 => \euros1__169_carry__1_i_1_n_7\,
      I3 => \euros1__231_carry__1_i_1_n_0\,
      O => \euros1__231_carry__1_i_5_n_0\
    );
\euros1__231_carry__1_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \^o23\(7),
      I1 => \euros1__169_carry__1_i_1_n_6\,
      I2 => \euros1__169_carry__2_i_1_n_6\,
      I3 => \euros1__231_carry__1_i_2_n_0\,
      O => \euros1__231_carry__1_i_6_n_0\
    );
\euros1__231_carry__1_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \^o23\(6),
      I1 => \euros1__169_carry__1_i_1_n_7\,
      I2 => \euros1__169_carry__2_i_1_n_7\,
      I3 => \euros1__231_carry__1_i_3_n_0\,
      O => \euros1__231_carry__1_i_7_n_0\
    );
\euros1__231_carry__1_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \^o23\(7),
      I1 => \euros1__169_carry__1_i_1_n_4\,
      I2 => \^o23\(5),
      I3 => \euros1__231_carry__1_i_4_n_0\,
      O => \euros1__231_carry__1_i_8_n_0\
    );
\euros1__231_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__231_carry__1_n_0\,
      CO(3) => \NLW_euros1__231_carry__2_CO_UNCONNECTED\(3),
      CO(2) => \euros1__231_carry__2_n_1\,
      CO(1) => \euros1__231_carry__2_n_2\,
      CO(0) => \euros1__231_carry__2_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \euros1__231_carry__2_i_1_n_0\,
      DI(1) => \euros1__231_carry__2_i_2_n_0\,
      DI(0) => \euros1__231_carry__2_i_3_n_0\,
      O(3) => \euros1__231_carry__2_n_4\,
      O(2) => \euros1__231_carry__2_n_5\,
      O(1) => \euros1__231_carry__2_n_6\,
      O(0) => \euros1__231_carry__2_n_7\,
      S(3) => \euros1__231_carry__2_i_4_n_0\,
      S(2) => \euros1__231_carry__2_i_5_n_0\,
      S(1) => \euros1__231_carry__2_i_6_n_0\,
      S(0) => \euros1__231_carry__2_i_7_n_0\
    );
\euros1__231_carry__2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_5\,
      I1 => \euros1__169_carry__2_i_1_n_7\,
      I2 => \euros1__169_carry__3_i_1_n_7\,
      O => \euros1__231_carry__2_i_1_n_0\
    );
\euros1__231_carry__2_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_6\,
      I1 => \euros1__169_carry__1_i_1_n_4\,
      I2 => \euros1__169_carry__2_i_1_n_4\,
      O => \euros1__231_carry__2_i_2_n_0\
    );
\euros1__231_carry__2_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \euros1__169_carry__2_i_1_n_5\,
      I1 => \euros1__169_carry__1_i_1_n_5\,
      I2 => \euros1__169_carry__1_i_1_n_7\,
      O => \euros1__231_carry__2_i_3_n_0\
    );
\euros1__231_carry__2_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \euros1__169_carry__3_i_1_n_6\,
      I1 => \euros1__169_carry__2_i_1_n_6\,
      I2 => \euros1__169_carry__1_i_1_n_4\,
      I3 => \euros1__169_carry__2_i_1_n_7\,
      I4 => \euros1__169_carry__2_i_1_n_5\,
      I5 => \euros1__169_carry__3_i_1_n_5\,
      O => \euros1__231_carry__2_i_4_n_0\
    );
\euros1__231_carry__2_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__231_carry__2_i_1_n_0\,
      I1 => \euros1__169_carry__1_i_1_n_4\,
      I2 => \euros1__169_carry__2_i_1_n_6\,
      I3 => \euros1__169_carry__3_i_1_n_6\,
      O => \euros1__231_carry__2_i_5_n_0\
    );
\euros1__231_carry__2_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_5\,
      I1 => \euros1__169_carry__2_i_1_n_7\,
      I2 => \euros1__169_carry__3_i_1_n_7\,
      I3 => \euros1__231_carry__2_i_2_n_0\,
      O => \euros1__231_carry__2_i_6_n_0\
    );
\euros1__231_carry__2_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_6\,
      I1 => \euros1__169_carry__1_i_1_n_4\,
      I2 => \euros1__169_carry__2_i_1_n_4\,
      I3 => \euros1__231_carry__2_i_3_n_0\,
      O => \euros1__231_carry__2_i_7_n_0\
    );
\euros1__231_carry_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \^o23\(0),
      I1 => \^o23\(2),
      I2 => \^o23\(6),
      O => \euros1__231_carry_i_1_n_0\
    );
\euros1__231_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^o23\(5),
      I1 => \^o23\(1),
      O => \euros1__231_carry_i_2_n_0\
    );
\euros1__231_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^o23\(4),
      I1 => \^o23\(0),
      O => \euros1__231_carry_i_3_n_0\
    );
\euros1__276_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \euros1__276_carry_n_0\,
      CO(2) => \euros1__276_carry_n_1\,
      CO(1) => \euros1__276_carry_n_2\,
      CO(0) => \euros1__276_carry_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__276_carry_i_1_n_0\,
      DI(2) => \euros1__276_carry_i_2_n_0\,
      DI(1) => \euros1__276_carry_i_3_n_0\,
      DI(0) => \euros1__276_carry_i_4_n_0\,
      O(3 downto 0) => \NLW_euros1__276_carry_O_UNCONNECTED\(3 downto 0),
      S(3) => \euros1__276_carry_i_5_n_0\,
      S(2) => \euros1__276_carry_i_6_n_0\,
      S(1) => \euros1__276_carry_i_7_n_0\,
      S(0) => \euros1__276_carry_i_8_n_0\
    );
\euros1__276_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__276_carry_n_0\,
      CO(3) => \euros1__276_carry__0_n_0\,
      CO(2) => \euros1__276_carry__0_n_1\,
      CO(1) => \euros1__276_carry__0_n_2\,
      CO(0) => \euros1__276_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__276_carry__0_i_1_n_0\,
      DI(2) => \euros1__276_carry__0_i_2_n_0\,
      DI(1) => \euros1__276_carry__0_i_3_n_0\,
      DI(0) => \euros1__276_carry__0_i_4_n_0\,
      O(3 downto 0) => \NLW_euros1__276_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \euros1__276_carry__0_i_5_n_0\,
      S(2) => \euros1__276_carry__0_i_6_n_0\,
      S(1) => \euros1__276_carry__0_i_7_n_0\,
      S(0) => \euros1__276_carry__0_i_8_n_0\
    );
\euros1__276_carry__0_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BE282828"
    )
        port map (
      I0 => \euros1__1_carry__2_n_5\,
      I1 => \euros1__92_carry__0_n_4\,
      I2 => \euros1__169_carry_n_5\,
      I3 => \euros1__169_carry_n_6\,
      I4 => \euros1__92_carry__0_n_5\,
      O => \euros1__276_carry__0_i_1_n_0\
    );
\euros1__276_carry__0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BE282828"
    )
        port map (
      I0 => \euros1__1_carry__2_n_6\,
      I1 => \euros1__92_carry__0_n_5\,
      I2 => \euros1__169_carry_n_6\,
      I3 => \euros1__92_carry_n_7\,
      I4 => \euros1__92_carry__0_n_6\,
      O => \euros1__276_carry__0_i_2_n_0\
    );
\euros1__276_carry__0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"28"
    )
        port map (
      I0 => \euros1__1_carry__2_n_7\,
      I1 => \euros1__92_carry__0_n_6\,
      I2 => \euros1__92_carry_n_7\,
      O => \euros1__276_carry__0_i_3_n_0\
    );
\euros1__276_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \euros1__1_carry__1_n_4\,
      I1 => \euros1__92_carry__0_n_7\,
      O => \euros1__276_carry__0_i_4_n_0\
    );
\euros1__276_carry__0_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => \euros1__276_carry__0_i_1_n_0\,
      I1 => \euros1__1_carry__2_n_4\,
      I2 => \euros1__276_carry__0_i_9_n_0\,
      I3 => \euros1__169_carry_n_5\,
      I4 => \euros1__92_carry__0_n_4\,
      O => \euros1__276_carry__0_i_5_n_0\
    );
\euros1__276_carry__0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669966996"
    )
        port map (
      I0 => \euros1__276_carry__0_i_2_n_0\,
      I1 => \euros1__1_carry__2_n_5\,
      I2 => \euros1__92_carry__0_n_4\,
      I3 => \euros1__169_carry_n_5\,
      I4 => \euros1__169_carry_n_6\,
      I5 => \euros1__92_carry__0_n_5\,
      O => \euros1__276_carry__0_i_6_n_0\
    );
\euros1__276_carry__0_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669966996"
    )
        port map (
      I0 => \euros1__276_carry__0_i_3_n_0\,
      I1 => \euros1__1_carry__2_n_6\,
      I2 => \euros1__92_carry__0_n_5\,
      I3 => \euros1__169_carry_n_6\,
      I4 => \euros1__92_carry_n_7\,
      I5 => \euros1__92_carry__0_n_6\,
      O => \euros1__276_carry__0_i_7_n_0\
    );
\euros1__276_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__1_carry__2_n_7\,
      I1 => \euros1__92_carry__0_n_6\,
      I2 => \euros1__92_carry_n_7\,
      I3 => \euros1__276_carry__0_i_4_n_0\,
      O => \euros1__276_carry__0_i_8_n_0\
    );
\euros1__276_carry__0_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry_n_4\,
      I1 => \euros1__92_carry__1_n_7\,
      I2 => \^o23\(0),
      O => \euros1__276_carry__0_i_9_n_0\
    );
\euros1__276_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__276_carry__0_n_0\,
      CO(3) => \euros1__276_carry__1_n_0\,
      CO(2) => \euros1__276_carry__1_n_1\,
      CO(1) => \euros1__276_carry__1_n_2\,
      CO(0) => \euros1__276_carry__1_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__276_carry__1_i_1_n_0\,
      DI(2) => \euros1__276_carry__1_i_2_n_0\,
      DI(1) => \euros1__276_carry__1_i_3_n_0\,
      DI(0) => \euros1__276_carry__1_i_4_n_0\,
      O(3 downto 0) => \NLW_euros1__276_carry__1_O_UNCONNECTED\(3 downto 0),
      S(3) => \euros1__276_carry__1_i_5_n_0\,
      S(2) => \euros1__276_carry__1_i_6_n_0\,
      S(1) => \euros1__276_carry__1_i_7_n_0\,
      S(0) => \euros1__276_carry__1_i_8_n_0\
    );
\euros1__276_carry__1_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \^o23\(2),
      I1 => \euros1__92_carry__1_n_5\,
      I2 => \euros1__169_carry__0_n_6\,
      I3 => \euros1__1_carry__3_n_5\,
      I4 => \euros1__276_carry__1_i_9_n_0\,
      O => \euros1__276_carry__1_i_1_n_0\
    );
\euros1__276_carry__1_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__0_n_6\,
      I1 => \euros1__92_carry__1_n_5\,
      I2 => \^o23\(2),
      O => \euros1__276_carry__1_i_10_n_0\
    );
\euros1__276_carry__1_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__0_n_7\,
      I1 => \euros1__92_carry__1_n_6\,
      I2 => \^o23\(1),
      O => \euros1__276_carry__1_i_11_n_0\
    );
\euros1__276_carry__1_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__0_n_4\,
      I1 => \euros1__92_carry__2_n_7\,
      I2 => \euros1__231_carry_n_6\,
      O => \euros1__276_carry__1_i_12_n_0\
    );
\euros1__276_carry__1_i_13\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \^o23\(2),
      I1 => \euros1__92_carry__1_n_5\,
      I2 => \euros1__169_carry__0_n_6\,
      O => \euros1__276_carry__1_i_13_n_0\
    );
\euros1__276_carry__1_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \^o23\(1),
      I1 => \euros1__92_carry__1_n_6\,
      I2 => \euros1__169_carry__0_n_7\,
      O => \euros1__276_carry__1_i_14_n_0\
    );
\euros1__276_carry__1_i_15\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \^o23\(0),
      I1 => \euros1__92_carry__1_n_7\,
      I2 => \euros1__169_carry_n_4\,
      O => \euros1__276_carry__1_i_15_n_0\
    );
\euros1__276_carry__1_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \^o23\(1),
      I1 => \euros1__92_carry__1_n_6\,
      I2 => \euros1__169_carry__0_n_7\,
      I3 => \euros1__1_carry__3_n_6\,
      I4 => \euros1__276_carry__1_i_10_n_0\,
      O => \euros1__276_carry__1_i_2_n_0\
    );
\euros1__276_carry__1_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \^o23\(0),
      I1 => \euros1__92_carry__1_n_7\,
      I2 => \euros1__169_carry_n_4\,
      I3 => \euros1__1_carry__3_n_7\,
      I4 => \euros1__276_carry__1_i_11_n_0\,
      O => \euros1__276_carry__1_i_3_n_0\
    );
\euros1__276_carry__1_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EBBE822882288228"
    )
        port map (
      I0 => \euros1__1_carry__2_n_4\,
      I1 => \^o23\(0),
      I2 => \euros1__92_carry__1_n_7\,
      I3 => \euros1__169_carry_n_4\,
      I4 => \euros1__169_carry_n_5\,
      I5 => \euros1__92_carry__0_n_4\,
      O => \euros1__276_carry__1_i_4_n_0\
    );
\euros1__276_carry__1_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \euros1__276_carry__1_i_1_n_0\,
      I1 => \euros1__1_carry__3_n_4\,
      I2 => \euros1__276_carry__1_i_12_n_0\,
      I3 => \euros1__231_carry_n_7\,
      I4 => \euros1__92_carry__1_n_4\,
      I5 => \euros1__169_carry__0_n_5\,
      O => \euros1__276_carry__1_i_5_n_0\
    );
\euros1__276_carry__1_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__276_carry__1_i_2_n_0\,
      I1 => \euros1__276_carry__1_i_13_n_0\,
      I2 => \euros1__169_carry__0_n_5\,
      I3 => \euros1__92_carry__1_n_4\,
      I4 => \euros1__231_carry_n_7\,
      I5 => \euros1__1_carry__3_n_5\,
      O => \euros1__276_carry__1_i_6_n_0\
    );
\euros1__276_carry__1_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__276_carry__1_i_3_n_0\,
      I1 => \euros1__276_carry__1_i_14_n_0\,
      I2 => \euros1__169_carry__0_n_6\,
      I3 => \euros1__92_carry__1_n_5\,
      I4 => \^o23\(2),
      I5 => \euros1__1_carry__3_n_6\,
      O => \euros1__276_carry__1_i_7_n_0\
    );
\euros1__276_carry__1_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__276_carry__1_i_4_n_0\,
      I1 => \euros1__276_carry__1_i_15_n_0\,
      I2 => \euros1__169_carry__0_n_7\,
      I3 => \euros1__92_carry__1_n_6\,
      I4 => \^o23\(1),
      I5 => \euros1__1_carry__3_n_7\,
      O => \euros1__276_carry__1_i_8_n_0\
    );
\euros1__276_carry__1_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__0_n_5\,
      I1 => \euros1__92_carry__1_n_4\,
      I2 => \euros1__231_carry_n_7\,
      O => \euros1__276_carry__1_i_9_n_0\
    );
\euros1__276_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__276_carry__1_n_0\,
      CO(3) => \euros1__276_carry__2_n_0\,
      CO(2) => \euros1__276_carry__2_n_1\,
      CO(1) => \euros1__276_carry__2_n_2\,
      CO(0) => \euros1__276_carry__2_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__276_carry__2_i_1_n_0\,
      DI(2) => \euros1__276_carry__2_i_2_n_0\,
      DI(1) => \euros1__276_carry__2_i_3_n_0\,
      DI(0) => \euros1__276_carry__2_i_4_n_0\,
      O(3 downto 0) => \NLW_euros1__276_carry__2_O_UNCONNECTED\(3 downto 0),
      S(3) => \euros1__276_carry__2_i_5_n_0\,
      S(2) => \euros1__276_carry__2_i_6_n_0\,
      S(1) => \euros1__276_carry__2_i_7_n_0\,
      S(0) => \euros1__276_carry__2_i_8_n_0\
    );
\euros1__276_carry__2_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \euros1__231_carry_n_4\,
      I1 => \euros1__92_carry__2_n_5\,
      I2 => \euros1__169_carry__1_n_6\,
      I3 => \euros1__1_carry__4_n_5\,
      I4 => \euros1__276_carry__2_i_9_n_0\,
      O => \euros1__276_carry__2_i_1_n_0\
    );
\euros1__276_carry__2_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__1_n_6\,
      I1 => \euros1__92_carry__2_n_5\,
      I2 => \euros1__231_carry_n_4\,
      O => \euros1__276_carry__2_i_10_n_0\
    );
\euros1__276_carry__2_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__1_n_7\,
      I1 => \euros1__92_carry__2_n_6\,
      I2 => \euros1__231_carry_n_5\,
      O => \euros1__276_carry__2_i_11_n_0\
    );
\euros1__276_carry__2_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \euros1__231_carry__0_n_7\,
      I1 => \euros1__92_carry__2_n_4\,
      I2 => \euros1__169_carry__1_n_5\,
      O => \euros1__276_carry__2_i_12_n_0\
    );
\euros1__276_carry__2_i_13\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \euros1__231_carry_n_4\,
      I1 => \euros1__92_carry__2_n_5\,
      I2 => \euros1__169_carry__1_n_6\,
      O => \euros1__276_carry__2_i_13_n_0\
    );
\euros1__276_carry__2_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \euros1__231_carry_n_5\,
      I1 => \euros1__92_carry__2_n_6\,
      I2 => \euros1__169_carry__1_n_7\,
      O => \euros1__276_carry__2_i_14_n_0\
    );
\euros1__276_carry__2_i_15\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \euros1__231_carry_n_6\,
      I1 => \euros1__92_carry__2_n_7\,
      I2 => \euros1__169_carry__0_n_4\,
      O => \euros1__276_carry__2_i_15_n_0\
    );
\euros1__276_carry__2_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \euros1__231_carry_n_5\,
      I1 => \euros1__92_carry__2_n_6\,
      I2 => \euros1__169_carry__1_n_7\,
      I3 => \euros1__1_carry__4_n_6\,
      I4 => \euros1__276_carry__2_i_10_n_0\,
      O => \euros1__276_carry__2_i_2_n_0\
    );
\euros1__276_carry__2_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \euros1__231_carry_n_6\,
      I1 => \euros1__92_carry__2_n_7\,
      I2 => \euros1__169_carry__0_n_4\,
      I3 => \euros1__1_carry__4_n_7\,
      I4 => \euros1__276_carry__2_i_11_n_0\,
      O => \euros1__276_carry__2_i_3_n_0\
    );
\euros1__276_carry__2_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \euros1__1_carry__3_n_4\,
      I1 => \euros1__276_carry__1_i_12_n_0\,
      I2 => \euros1__231_carry_n_7\,
      I3 => \euros1__92_carry__1_n_4\,
      I4 => \euros1__169_carry__0_n_5\,
      O => \euros1__276_carry__2_i_4_n_0\
    );
\euros1__276_carry__2_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__276_carry__2_i_1_n_0\,
      I1 => \euros1__276_carry__2_i_12_n_0\,
      I2 => \euros1__169_carry__1_n_4\,
      I3 => \euros1__92_carry__3_n_7\,
      I4 => \euros1__231_carry__0_n_6\,
      I5 => \euros1__1_carry__4_n_4\,
      O => \euros1__276_carry__2_i_5_n_0\
    );
\euros1__276_carry__2_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__276_carry__2_i_2_n_0\,
      I1 => \euros1__276_carry__2_i_13_n_0\,
      I2 => \euros1__169_carry__1_n_5\,
      I3 => \euros1__92_carry__2_n_4\,
      I4 => \euros1__231_carry__0_n_7\,
      I5 => \euros1__1_carry__4_n_5\,
      O => \euros1__276_carry__2_i_6_n_0\
    );
\euros1__276_carry__2_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__276_carry__2_i_3_n_0\,
      I1 => \euros1__276_carry__2_i_14_n_0\,
      I2 => \euros1__169_carry__1_n_6\,
      I3 => \euros1__92_carry__2_n_5\,
      I4 => \euros1__231_carry_n_4\,
      I5 => \euros1__1_carry__4_n_6\,
      O => \euros1__276_carry__2_i_7_n_0\
    );
\euros1__276_carry__2_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__276_carry__2_i_4_n_0\,
      I1 => \euros1__276_carry__2_i_15_n_0\,
      I2 => \euros1__169_carry__1_n_7\,
      I3 => \euros1__92_carry__2_n_6\,
      I4 => \euros1__231_carry_n_5\,
      I5 => \euros1__1_carry__4_n_7\,
      O => \euros1__276_carry__2_i_8_n_0\
    );
\euros1__276_carry__2_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__1_n_5\,
      I1 => \euros1__92_carry__2_n_4\,
      I2 => \euros1__231_carry__0_n_7\,
      O => \euros1__276_carry__2_i_9_n_0\
    );
\euros1__276_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__276_carry__2_n_0\,
      CO(3) => \euros1__276_carry__3_n_0\,
      CO(2) => \euros1__276_carry__3_n_1\,
      CO(1) => \euros1__276_carry__3_n_2\,
      CO(0) => \euros1__276_carry__3_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__276_carry__3_i_1_n_0\,
      DI(2) => \euros1__276_carry__3_i_2_n_0\,
      DI(1) => \euros1__276_carry__3_i_3_n_0\,
      DI(0) => \euros1__276_carry__3_i_4_n_0\,
      O(3 downto 0) => \NLW_euros1__276_carry__3_O_UNCONNECTED\(3 downto 0),
      S(3) => \euros1__276_carry__3_i_5_n_0\,
      S(2) => \euros1__276_carry__3_i_6_n_0\,
      S(1) => \euros1__276_carry__3_i_7_n_0\,
      S(0) => \euros1__276_carry__3_i_8_n_0\
    );
\euros1__276_carry__3_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \euros1__231_carry__0_n_4\,
      I1 => \euros1__92_carry__3_n_5\,
      I2 => \euros1__169_carry__2_n_6\,
      I3 => \euros1__1_carry__5_n_5\,
      I4 => \euros1__276_carry__3_i_9_n_0\,
      O => \euros1__276_carry__3_i_1_n_0\
    );
\euros1__276_carry__3_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__2_n_6\,
      I1 => \euros1__92_carry__3_n_5\,
      I2 => \euros1__231_carry__0_n_4\,
      O => \euros1__276_carry__3_i_10_n_0\
    );
\euros1__276_carry__3_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__2_n_7\,
      I1 => \euros1__92_carry__3_n_6\,
      I2 => \euros1__231_carry__0_n_5\,
      O => \euros1__276_carry__3_i_11_n_0\
    );
\euros1__276_carry__3_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__1_n_4\,
      I1 => \euros1__92_carry__3_n_7\,
      I2 => \euros1__231_carry__0_n_6\,
      O => \euros1__276_carry__3_i_12_n_0\
    );
\euros1__276_carry__3_i_13\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \euros1__231_carry__1_n_7\,
      I1 => \euros1__92_carry__3_n_4\,
      I2 => \euros1__169_carry__2_n_5\,
      O => \euros1__276_carry__3_i_13_n_0\
    );
\euros1__276_carry__3_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \euros1__231_carry__0_n_4\,
      I1 => \euros1__92_carry__3_n_5\,
      I2 => \euros1__169_carry__2_n_6\,
      O => \euros1__276_carry__3_i_14_n_0\
    );
\euros1__276_carry__3_i_15\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \euros1__231_carry__0_n_5\,
      I1 => \euros1__92_carry__3_n_6\,
      I2 => \euros1__169_carry__2_n_7\,
      O => \euros1__276_carry__3_i_15_n_0\
    );
\euros1__276_carry__3_i_16\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \euros1__231_carry__0_n_6\,
      I1 => \euros1__92_carry__3_n_7\,
      I2 => \euros1__169_carry__1_n_4\,
      O => \euros1__276_carry__3_i_16_n_0\
    );
\euros1__276_carry__3_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \euros1__231_carry__0_n_5\,
      I1 => \euros1__92_carry__3_n_6\,
      I2 => \euros1__169_carry__2_n_7\,
      I3 => \euros1__1_carry__5_n_6\,
      I4 => \euros1__276_carry__3_i_10_n_0\,
      O => \euros1__276_carry__3_i_2_n_0\
    );
\euros1__276_carry__3_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \euros1__231_carry__0_n_6\,
      I1 => \euros1__92_carry__3_n_7\,
      I2 => \euros1__169_carry__1_n_4\,
      I3 => \euros1__1_carry__5_n_7\,
      I4 => \euros1__276_carry__3_i_11_n_0\,
      O => \euros1__276_carry__3_i_3_n_0\
    );
\euros1__276_carry__3_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \euros1__231_carry__0_n_7\,
      I1 => \euros1__92_carry__2_n_4\,
      I2 => \euros1__169_carry__1_n_5\,
      I3 => \euros1__1_carry__4_n_4\,
      I4 => \euros1__276_carry__3_i_12_n_0\,
      O => \euros1__276_carry__3_i_4_n_0\
    );
\euros1__276_carry__3_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__276_carry__3_i_1_n_0\,
      I1 => \euros1__276_carry__3_i_13_n_0\,
      I2 => \euros1__169_carry__2_n_4\,
      I3 => \euros1__92_carry__4_n_7\,
      I4 => \euros1__231_carry__1_n_6\,
      I5 => \euros1__1_carry__5_n_4\,
      O => \euros1__276_carry__3_i_5_n_0\
    );
\euros1__276_carry__3_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__276_carry__3_i_2_n_0\,
      I1 => \euros1__276_carry__3_i_14_n_0\,
      I2 => \euros1__169_carry__2_n_5\,
      I3 => \euros1__92_carry__3_n_4\,
      I4 => \euros1__231_carry__1_n_7\,
      I5 => \euros1__1_carry__5_n_5\,
      O => \euros1__276_carry__3_i_6_n_0\
    );
\euros1__276_carry__3_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__276_carry__3_i_3_n_0\,
      I1 => \euros1__276_carry__3_i_15_n_0\,
      I2 => \euros1__169_carry__2_n_6\,
      I3 => \euros1__92_carry__3_n_5\,
      I4 => \euros1__231_carry__0_n_4\,
      I5 => \euros1__1_carry__5_n_6\,
      O => \euros1__276_carry__3_i_7_n_0\
    );
\euros1__276_carry__3_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__276_carry__3_i_4_n_0\,
      I1 => \euros1__276_carry__3_i_16_n_0\,
      I2 => \euros1__169_carry__2_n_7\,
      I3 => \euros1__92_carry__3_n_6\,
      I4 => \euros1__231_carry__0_n_5\,
      I5 => \euros1__1_carry__5_n_7\,
      O => \euros1__276_carry__3_i_8_n_0\
    );
\euros1__276_carry__3_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__2_n_5\,
      I1 => \euros1__92_carry__3_n_4\,
      I2 => \euros1__231_carry__1_n_7\,
      O => \euros1__276_carry__3_i_9_n_0\
    );
\euros1__276_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__276_carry__3_n_0\,
      CO(3) => \euros1__276_carry__4_n_0\,
      CO(2) => \euros1__276_carry__4_n_1\,
      CO(1) => \euros1__276_carry__4_n_2\,
      CO(0) => \euros1__276_carry__4_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__276_carry__4_i_1_n_0\,
      DI(2) => \euros1__276_carry__4_i_2_n_0\,
      DI(1) => \euros1__276_carry__4_i_3_n_0\,
      DI(0) => \euros1__276_carry__4_i_4_n_0\,
      O(3) => \euros1__276_carry__4_n_4\,
      O(2) => \euros1__276_carry__4_n_5\,
      O(1) => \euros1__276_carry__4_n_6\,
      O(0) => \euros1__276_carry__4_n_7\,
      S(3) => \euros1__276_carry__4_i_5_n_0\,
      S(2) => \euros1__276_carry__4_i_6_n_0\,
      S(1) => \euros1__276_carry__4_i_7_n_0\,
      S(0) => \euros1__276_carry__4_i_8_n_0\
    );
\euros1__276_carry__4_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \euros1__231_carry__1_n_4\,
      I1 => \euros1__92_carry__4_n_5\,
      I2 => \euros1__169_carry__3_n_6\,
      I3 => \euros1__1_carry__6_n_5\,
      I4 => \euros1__276_carry__4_i_9_n_0\,
      O => \euros1__276_carry__4_i_1_n_0\
    );
\euros1__276_carry__4_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__3_n_6\,
      I1 => \euros1__92_carry__4_n_5\,
      I2 => \euros1__231_carry__1_n_4\,
      O => \euros1__276_carry__4_i_10_n_0\
    );
\euros1__276_carry__4_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__3_n_7\,
      I1 => \euros1__92_carry__4_n_6\,
      I2 => \euros1__231_carry__1_n_5\,
      O => \euros1__276_carry__4_i_11_n_0\
    );
\euros1__276_carry__4_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__2_n_4\,
      I1 => \euros1__92_carry__4_n_7\,
      I2 => \euros1__231_carry__1_n_6\,
      O => \euros1__276_carry__4_i_12_n_0\
    );
\euros1__276_carry__4_i_13\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \euros1__231_carry__2_n_7\,
      I1 => \euros1__92_carry__4_n_4\,
      I2 => \euros1__169_carry__3_n_5\,
      O => \euros1__276_carry__4_i_13_n_0\
    );
\euros1__276_carry__4_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \euros1__231_carry__1_n_4\,
      I1 => \euros1__92_carry__4_n_5\,
      I2 => \euros1__169_carry__3_n_6\,
      O => \euros1__276_carry__4_i_14_n_0\
    );
\euros1__276_carry__4_i_15\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \euros1__231_carry__1_n_5\,
      I1 => \euros1__92_carry__4_n_6\,
      I2 => \euros1__169_carry__3_n_7\,
      O => \euros1__276_carry__4_i_15_n_0\
    );
\euros1__276_carry__4_i_16\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \euros1__231_carry__1_n_6\,
      I1 => \euros1__92_carry__4_n_7\,
      I2 => \euros1__169_carry__2_n_4\,
      O => \euros1__276_carry__4_i_16_n_0\
    );
\euros1__276_carry__4_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \euros1__231_carry__1_n_5\,
      I1 => \euros1__92_carry__4_n_6\,
      I2 => \euros1__169_carry__3_n_7\,
      I3 => \euros1__1_carry__6_n_6\,
      I4 => \euros1__276_carry__4_i_10_n_0\,
      O => \euros1__276_carry__4_i_2_n_0\
    );
\euros1__276_carry__4_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \euros1__231_carry__1_n_6\,
      I1 => \euros1__92_carry__4_n_7\,
      I2 => \euros1__169_carry__2_n_4\,
      I3 => \euros1__1_carry__6_n_7\,
      I4 => \euros1__276_carry__4_i_11_n_0\,
      O => \euros1__276_carry__4_i_3_n_0\
    );
\euros1__276_carry__4_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \euros1__231_carry__1_n_7\,
      I1 => \euros1__92_carry__3_n_4\,
      I2 => \euros1__169_carry__2_n_5\,
      I3 => \euros1__1_carry__5_n_4\,
      I4 => \euros1__276_carry__4_i_12_n_0\,
      O => \euros1__276_carry__4_i_4_n_0\
    );
\euros1__276_carry__4_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__276_carry__4_i_1_n_0\,
      I1 => \euros1__276_carry__4_i_13_n_0\,
      I2 => \euros1__169_carry__3_n_4\,
      I3 => \euros1__92_carry__5_n_7\,
      I4 => \euros1__231_carry__2_n_6\,
      I5 => \euros1__1_carry__6_n_4\,
      O => \euros1__276_carry__4_i_5_n_0\
    );
\euros1__276_carry__4_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__276_carry__4_i_2_n_0\,
      I1 => \euros1__276_carry__4_i_14_n_0\,
      I2 => \euros1__169_carry__3_n_5\,
      I3 => \euros1__92_carry__4_n_4\,
      I4 => \euros1__231_carry__2_n_7\,
      I5 => \euros1__1_carry__6_n_5\,
      O => \euros1__276_carry__4_i_6_n_0\
    );
\euros1__276_carry__4_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__276_carry__4_i_3_n_0\,
      I1 => \euros1__276_carry__4_i_15_n_0\,
      I2 => \euros1__169_carry__3_n_6\,
      I3 => \euros1__92_carry__4_n_5\,
      I4 => \euros1__231_carry__1_n_4\,
      I5 => \euros1__1_carry__6_n_6\,
      O => \euros1__276_carry__4_i_7_n_0\
    );
\euros1__276_carry__4_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \euros1__276_carry__4_i_4_n_0\,
      I1 => \euros1__276_carry__4_i_16_n_0\,
      I2 => \euros1__169_carry__3_n_7\,
      I3 => \euros1__92_carry__4_n_6\,
      I4 => \euros1__231_carry__1_n_5\,
      I5 => \euros1__1_carry__6_n_7\,
      O => \euros1__276_carry__4_i_8_n_0\
    );
\euros1__276_carry__4_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__3_n_5\,
      I1 => \euros1__92_carry__4_n_4\,
      I2 => \euros1__231_carry__2_n_7\,
      O => \euros1__276_carry__4_i_9_n_0\
    );
\euros1__276_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__276_carry__4_n_0\,
      CO(3 downto 1) => \NLW_euros1__276_carry__5_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \euros1__276_carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \euros1__276_carry__5_i_1_n_0\,
      O(3 downto 2) => \NLW_euros1__276_carry__5_O_UNCONNECTED\(3 downto 2),
      O(1) => \euros1__276_carry__5_n_6\,
      O(0) => \euros1__276_carry__5_n_7\,
      S(3 downto 2) => B"00",
      S(1) => \euros1__276_carry__5_i_2_n_0\,
      S(0) => \euros1__276_carry__5_i_3_n_0\
    );
\euros1__276_carry__5_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \euros1__231_carry__2_n_7\,
      I1 => \euros1__92_carry__4_n_4\,
      I2 => \euros1__169_carry__3_n_5\,
      I3 => \euros1__1_carry__6_n_4\,
      I4 => \euros1__276_carry__5_i_4_n_0\,
      O => \euros1__276_carry__5_i_1_n_0\
    );
\euros1__276_carry__5_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E8818117177E7EE8"
    )
        port map (
      I0 => \euros1__276_carry__5_i_5_n_0\,
      I1 => \euros1__1_carry__7_n_7\,
      I2 => \euros1__231_carry__2_n_5\,
      I3 => \euros1__92_carry__5_n_6\,
      I4 => \euros1__169_carry__4_n_7\,
      I5 => \euros1__276_carry__5_i_6_n_0\,
      O => \euros1__276_carry__5_i_2_n_0\
    );
\euros1__276_carry__5_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \euros1__276_carry__5_i_1_n_0\,
      I1 => \euros1__1_carry__7_n_7\,
      I2 => \euros1__276_carry__5_i_7_n_0\,
      I3 => \euros1__231_carry__2_n_6\,
      I4 => \euros1__92_carry__5_n_7\,
      I5 => \euros1__169_carry__3_n_4\,
      O => \euros1__276_carry__5_i_3_n_0\
    );
\euros1__276_carry__5_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__3_n_4\,
      I1 => \euros1__92_carry__5_n_7\,
      I2 => \euros1__231_carry__2_n_6\,
      O => \euros1__276_carry__5_i_4_n_0\
    );
\euros1__276_carry__5_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \euros1__231_carry__2_n_6\,
      I1 => \euros1__92_carry__5_n_7\,
      I2 => \euros1__169_carry__3_n_4\,
      O => \euros1__276_carry__5_i_5_n_0\
    );
\euros1__276_carry__5_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__169_carry__4_n_6\,
      I1 => \euros1__231_carry__2_n_4\,
      I2 => \euros1__1_carry__7_n_6\,
      I3 => \euros1__92_carry__5_n_5\,
      O => \euros1__276_carry__5_i_6_n_0\
    );
\euros1__276_carry__5_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \euros1__169_carry__4_n_7\,
      I1 => \euros1__92_carry__5_n_6\,
      I2 => \euros1__231_carry__2_n_5\,
      O => \euros1__276_carry__5_i_7_n_0\
    );
\euros1__276_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \euros1__1_carry__1_n_5\,
      I1 => \euros1__92_carry_n_4\,
      O => \euros1__276_carry_i_1_n_0\
    );
\euros1__276_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \euros1__1_carry__1_n_6\,
      I1 => \euros1__92_carry_n_5\,
      O => \euros1__276_carry_i_2_n_0\
    );
\euros1__276_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \euros1__1_carry__1_n_7\,
      I1 => \euros1__92_carry_n_6\,
      O => \euros1__276_carry_i_3_n_0\
    );
\euros1__276_carry_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^o23\(0),
      I1 => \euros1__1_carry__0_n_4\,
      O => \euros1__276_carry_i_4_n_0\
    );
\euros1__276_carry_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => \euros1__1_carry__1_n_4\,
      I1 => \euros1__92_carry__0_n_7\,
      I2 => \euros1__92_carry_n_4\,
      I3 => \euros1__1_carry__1_n_5\,
      O => \euros1__276_carry_i_5_n_0\
    );
\euros1__276_carry_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \euros1__92_carry_n_5\,
      I1 => \euros1__1_carry__1_n_6\,
      I2 => \euros1__92_carry_n_4\,
      I3 => \euros1__1_carry__1_n_5\,
      O => \euros1__276_carry_i_6_n_0\
    );
\euros1__276_carry_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \euros1__92_carry_n_6\,
      I1 => \euros1__1_carry__1_n_7\,
      I2 => \euros1__92_carry_n_5\,
      I3 => \euros1__1_carry__1_n_6\,
      O => \euros1__276_carry_i_7_n_0\
    );
\euros1__276_carry_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \euros1__1_carry__0_n_4\,
      I1 => \^o23\(0),
      I2 => \euros1__92_carry_n_6\,
      I3 => \euros1__1_carry__1_n_7\,
      O => \euros1__276_carry_i_8_n_0\
    );
\euros1__333_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3 downto 2) => \NLW_euros1__333_carry_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \euros1__333_carry_n_2\,
      CO(0) => \euros1__333_carry_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \euros1__333_carry_i_1_n_0\,
      DI(0) => '0',
      O(3) => \NLW_euros1__333_carry_O_UNCONNECTED\(3),
      O(2) => \euros1__333_carry_n_5\,
      O(1) => \euros1__333_carry_n_6\,
      O(0) => \euros1__333_carry_n_7\,
      S(3) => '0',
      S(2) => \euros1__333_carry_i_2_n_0\,
      S(1) => \euros1__333_carry_i_3_n_0\,
      S(0) => \euros1__333_carry_i_4_n_0\
    );
\euros1__333_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \euros1__276_carry__4_n_4\,
      I1 => \euros1__276_carry__4_n_7\,
      O => \euros1__333_carry_i_1_n_0\
    );
\euros1__333_carry_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66969969"
    )
        port map (
      I0 => \euros1__276_carry__5_n_6\,
      I1 => \euros1__276_carry__4_n_5\,
      I2 => \euros1__276_carry__5_n_7\,
      I3 => \euros1__276_carry__4_n_6\,
      I4 => \euros1__276_carry__4_n_7\,
      O => \euros1__333_carry_i_2_n_0\
    );
\euros1__333_carry_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => \euros1__276_carry__4_n_7\,
      I1 => \euros1__276_carry__4_n_4\,
      I2 => \euros1__276_carry__5_n_7\,
      I3 => \euros1__276_carry__4_n_6\,
      O => \euros1__333_carry_i_3_n_0\
    );
\euros1__333_carry_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \euros1__276_carry__4_n_4\,
      I1 => \euros1__276_carry__4_n_7\,
      O => \euros1__333_carry_i_4_n_0\
    );
\euros1__339_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \euros1__339_carry_n_0\,
      CO(2) => \euros1__339_carry_n_1\,
      CO(1) => \euros1__339_carry_n_2\,
      CO(0) => \euros1__339_carry_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => \^o23\(3 downto 0),
      O(3) => \euros1__339_carry_n_4\,
      O(2) => \euros1__339_carry_n_5\,
      O(1 downto 0) => p_1_in(1 downto 0),
      S(3) => \euros1__339_carry_i_1_n_0\,
      S(2) => \euros1__339_carry_i_2_n_0\,
      S(1) => \euros1__339_carry_i_3_n_0\,
      S(0) => \euros1__339_carry_i_4_n_0\
    );
\euros1__339_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__339_carry_n_0\,
      CO(3) => \NLW_euros1__339_carry__0_CO_UNCONNECTED\(3),
      CO(2) => \euros1__339_carry__0_n_1\,
      CO(1) => \euros1__339_carry__0_n_2\,
      CO(0) => \euros1__339_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => \^o23\(6 downto 4),
      O(3) => \euros1__339_carry__0_n_4\,
      O(2) => \euros1__339_carry__0_n_5\,
      O(1) => \euros1__339_carry__0_n_6\,
      O(0) => \euros1__339_carry__0_n_7\,
      S(3) => \euros1__339_carry__0_i_1_n_0\,
      S(2) => \euros1__339_carry__0_i_2_n_0\,
      S(1) => \euros1__339_carry__0_i_3_n_0\,
      S(0) => \euros1__339_carry__0_i_4_n_0\
    );
\euros1__339_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^o23\(7),
      I1 => \euros1__333_carry_n_5\,
      O => \euros1__339_carry__0_i_1_n_0\
    );
\euros1__339_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^o23\(6),
      I1 => \euros1__333_carry_n_6\,
      O => \euros1__339_carry__0_i_2_n_0\
    );
\euros1__339_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^o23\(5),
      I1 => \euros1__333_carry_n_7\,
      O => \euros1__339_carry__0_i_3_n_0\
    );
\euros1__339_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^o23\(4),
      I1 => \euros1__276_carry__4_n_5\,
      O => \euros1__339_carry__0_i_4_n_0\
    );
\euros1__339_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^o23\(3),
      I1 => \euros1__276_carry__4_n_6\,
      O => \euros1__339_carry_i_1_n_0\
    );
\euros1__339_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^o23\(2),
      I1 => \euros1__276_carry__4_n_7\,
      O => \euros1__339_carry_i_2_n_0\
    );
\euros1__339_carry_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^o23\(1),
      O => \euros1__339_carry_i_3_n_0\
    );
\euros1__339_carry_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^o23\(0),
      O => \euros1__339_carry_i_4_n_0\
    );
\euros1__92_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \euros1__92_carry_n_0\,
      CO(2) => \euros1__92_carry_n_1\,
      CO(1) => \euros1__92_carry_n_2\,
      CO(0) => \euros1__92_carry_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__92_carry_i_1_n_0\,
      DI(2) => \^o23\(0),
      DI(1 downto 0) => B"01",
      O(3) => \euros1__92_carry_n_4\,
      O(2) => \euros1__92_carry_n_5\,
      O(1) => \euros1__92_carry_n_6\,
      O(0) => \euros1__92_carry_n_7\,
      S(3) => \euros1__92_carry_i_2_n_0\,
      S(2) => \euros1__92_carry_i_3_n_0\,
      S(1) => \euros1__92_carry_i_4_n_0\,
      S(0) => \^o23\(0)
    );
\euros1__92_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__92_carry_n_0\,
      CO(3) => \euros1__92_carry__0_n_0\,
      CO(2) => \euros1__92_carry__0_n_1\,
      CO(1) => \euros1__92_carry__0_n_2\,
      CO(0) => \euros1__92_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__92_carry__0_i_1_n_0\,
      DI(2) => \euros1__92_carry__0_i_2_n_0\,
      DI(1) => \euros1__92_carry__0_i_3_n_0\,
      DI(0) => \euros1__92_carry__0_i_4_n_0\,
      O(3) => \euros1__92_carry__0_n_4\,
      O(2) => \euros1__92_carry__0_n_5\,
      O(1) => \euros1__92_carry__0_n_6\,
      O(0) => \euros1__92_carry__0_n_7\,
      S(3) => \euros1__92_carry__0_i_5_n_0\,
      S(2) => \euros1__92_carry__0_i_6_n_0\,
      S(1) => \euros1__92_carry__0_i_7_n_0\,
      S(0) => \euros1__92_carry__0_i_8_n_0\
    );
\euros1__92_carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \^o23\(2),
      I1 => \^o23\(4),
      I2 => \^o23\(6),
      O => \euros1__92_carry__0_i_1_n_0\
    );
\euros1__92_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \^o23\(1),
      I1 => \^o23\(3),
      I2 => \^o23\(5),
      O => \euros1__92_carry__0_i_2_n_0\
    );
\euros1__92_carry__0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \^o23\(0),
      I1 => \^o23\(4),
      I2 => \^o23\(2),
      O => \euros1__92_carry__0_i_3_n_0\
    );
\euros1__92_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^o23\(1),
      I1 => \^o23\(3),
      O => \euros1__92_carry__0_i_4_n_0\
    );
\euros1__92_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \^o23\(5),
      I1 => \^o23\(3),
      I2 => \^o23\(7),
      I3 => \euros1__92_carry__0_i_1_n_0\,
      O => \euros1__92_carry__0_i_5_n_0\
    );
\euros1__92_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \^o23\(2),
      I1 => \^o23\(4),
      I2 => \^o23\(6),
      I3 => \euros1__92_carry__0_i_2_n_0\,
      O => \euros1__92_carry__0_i_6_n_0\
    );
\euros1__92_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \^o23\(1),
      I1 => \^o23\(3),
      I2 => \^o23\(5),
      I3 => \euros1__92_carry__0_i_3_n_0\,
      O => \euros1__92_carry__0_i_7_n_0\
    );
\euros1__92_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \^o23\(0),
      I1 => \^o23\(4),
      I2 => \^o23\(2),
      I3 => \euros1__92_carry__0_i_4_n_0\,
      O => \euros1__92_carry__0_i_8_n_0\
    );
\euros1__92_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__92_carry__0_n_0\,
      CO(3) => \euros1__92_carry__1_n_0\,
      CO(2) => \euros1__92_carry__1_n_1\,
      CO(1) => \euros1__92_carry__1_n_2\,
      CO(0) => \euros1__92_carry__1_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__92_carry__1_i_1_n_0\,
      DI(2) => \euros1__92_carry__1_i_2_n_0\,
      DI(1) => \euros1__92_carry__1_i_3_n_0\,
      DI(0) => \euros1__92_carry__1_i_4_n_0\,
      O(3) => \euros1__92_carry__1_n_4\,
      O(2) => \euros1__92_carry__1_n_5\,
      O(1) => \euros1__92_carry__1_n_6\,
      O(0) => \euros1__92_carry__1_n_7\,
      S(3) => \euros1__92_carry__1_i_5_n_0\,
      S(2) => \euros1__92_carry__1_i_6_n_0\,
      S(1) => \euros1__92_carry__1_i_7_n_0\,
      S(0) => \euros1__92_carry__1_i_8_n_0\
    );
\euros1__92_carry__1_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \^o23\(6),
      I1 => \euros1__169_carry__1_i_1_n_5\,
      I2 => \euros1__169_carry__1_i_1_n_7\,
      O => \euros1__92_carry__1_i_1_n_0\
    );
\euros1__92_carry__1_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4D"
    )
        port map (
      I0 => \^o23\(7),
      I1 => \^o23\(5),
      I2 => \euros1__169_carry__1_i_1_n_6\,
      O => \euros1__92_carry__1_i_2_n_0\
    );
\euros1__92_carry__1_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \^o23\(4),
      I1 => \euros1__169_carry__1_i_1_n_7\,
      I2 => \^o23\(6),
      O => \euros1__92_carry__1_i_3_n_0\
    );
\euros1__92_carry__1_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4D"
    )
        port map (
      I0 => \^o23\(5),
      I1 => \^o23\(3),
      I2 => \^o23\(7),
      O => \euros1__92_carry__1_i_4_n_0\
    );
\euros1__92_carry__1_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \^o23\(7),
      I1 => \euros1__169_carry__1_i_1_n_4\,
      I2 => \euros1__169_carry__1_i_1_n_6\,
      I3 => \euros1__92_carry__1_i_1_n_0\,
      O => \euros1__92_carry__1_i_5_n_0\
    );
\euros1__92_carry__1_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \^o23\(6),
      I1 => \euros1__169_carry__1_i_1_n_5\,
      I2 => \euros1__169_carry__1_i_1_n_7\,
      I3 => \euros1__92_carry__1_i_2_n_0\,
      O => \euros1__92_carry__1_i_6_n_0\
    );
\euros1__92_carry__1_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \^o23\(7),
      I1 => \^o23\(5),
      I2 => \euros1__169_carry__1_i_1_n_6\,
      I3 => \euros1__92_carry__1_i_3_n_0\,
      O => \euros1__92_carry__1_i_7_n_0\
    );
\euros1__92_carry__1_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \^o23\(4),
      I1 => \euros1__169_carry__1_i_1_n_7\,
      I2 => \^o23\(6),
      I3 => \euros1__92_carry__1_i_4_n_0\,
      O => \euros1__92_carry__1_i_8_n_0\
    );
\euros1__92_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__92_carry__1_n_0\,
      CO(3) => \euros1__92_carry__2_n_0\,
      CO(2) => \euros1__92_carry__2_n_1\,
      CO(1) => \euros1__92_carry__2_n_2\,
      CO(0) => \euros1__92_carry__2_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__92_carry__2_i_1_n_0\,
      DI(2) => \euros1__92_carry__2_i_2_n_0\,
      DI(1) => \euros1__92_carry__2_i_3_n_0\,
      DI(0) => \euros1__92_carry__2_i_4_n_0\,
      O(3) => \euros1__92_carry__2_n_4\,
      O(2) => \euros1__92_carry__2_n_5\,
      O(1) => \euros1__92_carry__2_n_6\,
      O(0) => \euros1__92_carry__2_n_7\,
      S(3) => \euros1__92_carry__2_i_5_n_0\,
      S(2) => \euros1__92_carry__2_i_6_n_0\,
      S(1) => \euros1__92_carry__2_i_7_n_0\,
      S(0) => \euros1__92_carry__2_i_8_n_0\
    );
\euros1__92_carry__2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_5\,
      I1 => \euros1__169_carry__2_i_1_n_5\,
      I2 => \euros1__169_carry__2_i_1_n_7\,
      O => \euros1__92_carry__2_i_1_n_0\
    );
\euros1__92_carry__2_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_6\,
      I1 => \euros1__169_carry__2_i_1_n_6\,
      I2 => \euros1__169_carry__1_i_1_n_4\,
      O => \euros1__92_carry__2_i_2_n_0\
    );
\euros1__92_carry__2_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_7\,
      I1 => \euros1__169_carry__2_i_1_n_7\,
      I2 => \euros1__169_carry__1_i_1_n_5\,
      O => \euros1__92_carry__2_i_3_n_0\
    );
\euros1__92_carry__2_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \^o23\(7),
      I1 => \euros1__169_carry__1_i_1_n_4\,
      I2 => \euros1__169_carry__1_i_1_n_6\,
      O => \euros1__92_carry__2_i_4_n_0\
    );
\euros1__92_carry__2_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_4\,
      I1 => \euros1__169_carry__2_i_1_n_4\,
      I2 => \euros1__169_carry__2_i_1_n_6\,
      I3 => \euros1__92_carry__2_i_1_n_0\,
      O => \euros1__92_carry__2_i_5_n_0\
    );
\euros1__92_carry__2_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_5\,
      I1 => \euros1__169_carry__2_i_1_n_5\,
      I2 => \euros1__169_carry__2_i_1_n_7\,
      I3 => \euros1__92_carry__2_i_2_n_0\,
      O => \euros1__92_carry__2_i_6_n_0\
    );
\euros1__92_carry__2_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_6\,
      I1 => \euros1__169_carry__2_i_1_n_6\,
      I2 => \euros1__169_carry__1_i_1_n_4\,
      I3 => \euros1__92_carry__2_i_3_n_0\,
      O => \euros1__92_carry__2_i_7_n_0\
    );
\euros1__92_carry__2_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_7\,
      I1 => \euros1__169_carry__2_i_1_n_7\,
      I2 => \euros1__169_carry__1_i_1_n_5\,
      I3 => \euros1__92_carry__2_i_4_n_0\,
      O => \euros1__92_carry__2_i_8_n_0\
    );
\euros1__92_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__92_carry__2_n_0\,
      CO(3) => \euros1__92_carry__3_n_0\,
      CO(2) => \euros1__92_carry__3_n_1\,
      CO(1) => \euros1__92_carry__3_n_2\,
      CO(0) => \euros1__92_carry__3_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__92_carry__3_i_1_n_0\,
      DI(2) => \euros1__92_carry__3_i_2_n_0\,
      DI(1) => \euros1__92_carry__3_i_3_n_0\,
      DI(0) => \euros1__92_carry__3_i_4_n_0\,
      O(3) => \euros1__92_carry__3_n_4\,
      O(2) => \euros1__92_carry__3_n_5\,
      O(1) => \euros1__92_carry__3_n_6\,
      O(0) => \euros1__92_carry__3_n_7\,
      S(3) => \euros1__92_carry__3_i_5_n_0\,
      S(2) => \euros1__92_carry__3_i_6_n_0\,
      S(1) => \euros1__92_carry__3_i_7_n_0\,
      S(0) => \euros1__92_carry__3_i_8_n_0\
    );
\euros1__92_carry__3_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \euros1__169_carry__2_i_1_n_5\,
      I1 => \euros1__169_carry__3_i_1_n_5\,
      I2 => \euros1__169_carry__3_i_1_n_7\,
      O => \euros1__92_carry__3_i_1_n_0\
    );
\euros1__92_carry__3_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \euros1__169_carry__2_i_1_n_6\,
      I1 => \euros1__169_carry__3_i_1_n_6\,
      I2 => \euros1__169_carry__2_i_1_n_4\,
      O => \euros1__92_carry__3_i_2_n_0\
    );
\euros1__92_carry__3_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \euros1__169_carry__2_i_1_n_7\,
      I1 => \euros1__169_carry__3_i_1_n_7\,
      I2 => \euros1__169_carry__2_i_1_n_5\,
      O => \euros1__92_carry__3_i_3_n_0\
    );
\euros1__92_carry__3_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_4\,
      I1 => \euros1__169_carry__2_i_1_n_4\,
      I2 => \euros1__169_carry__2_i_1_n_6\,
      O => \euros1__92_carry__3_i_4_n_0\
    );
\euros1__92_carry__3_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__169_carry__2_i_1_n_4\,
      I1 => \euros1__169_carry__3_i_1_n_4\,
      I2 => \euros1__169_carry__3_i_1_n_6\,
      I3 => \euros1__92_carry__3_i_1_n_0\,
      O => \euros1__92_carry__3_i_5_n_0\
    );
\euros1__92_carry__3_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__169_carry__2_i_1_n_5\,
      I1 => \euros1__169_carry__3_i_1_n_5\,
      I2 => \euros1__169_carry__3_i_1_n_7\,
      I3 => \euros1__92_carry__3_i_2_n_0\,
      O => \euros1__92_carry__3_i_6_n_0\
    );
\euros1__92_carry__3_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__169_carry__2_i_1_n_6\,
      I1 => \euros1__169_carry__3_i_1_n_6\,
      I2 => \euros1__169_carry__2_i_1_n_4\,
      I3 => \euros1__92_carry__3_i_3_n_0\,
      O => \euros1__92_carry__3_i_7_n_0\
    );
\euros1__92_carry__3_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__169_carry__2_i_1_n_7\,
      I1 => \euros1__169_carry__3_i_1_n_7\,
      I2 => \euros1__169_carry__2_i_1_n_5\,
      I3 => \euros1__92_carry__3_i_4_n_0\,
      O => \euros1__92_carry__3_i_8_n_0\
    );
\euros1__92_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__92_carry__3_n_0\,
      CO(3) => \euros1__92_carry__4_n_0\,
      CO(2) => \euros1__92_carry__4_n_1\,
      CO(1) => \euros1__92_carry__4_n_2\,
      CO(0) => \euros1__92_carry__4_n_3\,
      CYINIT => '0',
      DI(3) => \euros1__92_carry__4_i_1_n_0\,
      DI(2) => \euros1__92_carry__4_i_2_n_0\,
      DI(1) => \euros1__92_carry__4_i_3_n_0\,
      DI(0) => \euros1__92_carry__4_i_4_n_0\,
      O(3) => \euros1__92_carry__4_n_4\,
      O(2) => \euros1__92_carry__4_n_5\,
      O(1) => \euros1__92_carry__4_n_6\,
      O(0) => \euros1__92_carry__4_n_7\,
      S(3) => \euros1__92_carry__4_i_5_n_0\,
      S(2) => \euros1__92_carry__4_i_6_n_0\,
      S(1) => \euros1__92_carry__4_i_7_n_0\,
      S(0) => \euros1__92_carry__4_i_8_n_0\
    );
\euros1__92_carry__4_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \euros1__169_carry__3_i_1_n_5\,
      I1 => \euros1__1_carry__3_i_9_n_5\,
      I2 => \euros1__1_carry__3_i_9_n_7\,
      O => \euros1__92_carry__4_i_1_n_0\
    );
\euros1__92_carry__4_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \euros1__169_carry__3_i_1_n_6\,
      I1 => \euros1__1_carry__3_i_9_n_6\,
      I2 => \euros1__169_carry__3_i_1_n_4\,
      O => \euros1__92_carry__4_i_2_n_0\
    );
\euros1__92_carry__4_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \euros1__169_carry__3_i_1_n_7\,
      I1 => \euros1__1_carry__3_i_9_n_7\,
      I2 => \euros1__169_carry__3_i_1_n_5\,
      O => \euros1__92_carry__4_i_3_n_0\
    );
\euros1__92_carry__4_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \euros1__169_carry__2_i_1_n_4\,
      I1 => \euros1__169_carry__3_i_1_n_4\,
      I2 => \euros1__169_carry__3_i_1_n_6\,
      O => \euros1__92_carry__4_i_4_n_0\
    );
\euros1__92_carry__4_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__169_carry__3_i_1_n_4\,
      I1 => \euros1__1_carry__3_i_9_n_4\,
      I2 => \euros1__1_carry__3_i_9_n_6\,
      I3 => \euros1__92_carry__4_i_1_n_0\,
      O => \euros1__92_carry__4_i_5_n_0\
    );
\euros1__92_carry__4_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__169_carry__3_i_1_n_5\,
      I1 => \euros1__1_carry__3_i_9_n_5\,
      I2 => \euros1__1_carry__3_i_9_n_7\,
      I3 => \euros1__92_carry__4_i_2_n_0\,
      O => \euros1__92_carry__4_i_6_n_0\
    );
\euros1__92_carry__4_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__169_carry__3_i_1_n_6\,
      I1 => \euros1__1_carry__3_i_9_n_6\,
      I2 => \euros1__169_carry__3_i_1_n_4\,
      I3 => \euros1__92_carry__4_i_3_n_0\,
      O => \euros1__92_carry__4_i_7_n_0\
    );
\euros1__92_carry__4_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__169_carry__3_i_1_n_7\,
      I1 => \euros1__1_carry__3_i_9_n_7\,
      I2 => \euros1__169_carry__3_i_1_n_5\,
      I3 => \euros1__92_carry__4_i_4_n_0\,
      O => \euros1__92_carry__4_i_8_n_0\
    );
\euros1__92_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \euros1__92_carry__4_n_0\,
      CO(3 downto 2) => \NLW_euros1__92_carry__5_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \euros1__92_carry__5_n_2\,
      CO(0) => \euros1__92_carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \euros1__92_carry__5_i_1_n_0\,
      DI(0) => \euros1__92_carry__5_i_2_n_0\,
      O(3) => \NLW_euros1__92_carry__5_O_UNCONNECTED\(3),
      O(2) => \euros1__92_carry__5_n_5\,
      O(1) => \euros1__92_carry__5_n_6\,
      O(0) => \euros1__92_carry__5_n_7\,
      S(3) => '0',
      S(2) => \euros1__92_carry__5_i_3_n_0\,
      S(1) => \euros1__92_carry__5_i_4_n_0\,
      S(0) => \euros1__92_carry__5_i_5_n_0\
    );
\euros1__92_carry__5_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \euros1__1_carry__3_i_9_n_7\,
      I1 => \euros1__1_carry__7_i_1_n_7\,
      I2 => \euros1__1_carry__3_i_9_n_5\,
      O => \euros1__92_carry__5_i_1_n_0\
    );
\euros1__92_carry__5_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \euros1__169_carry__3_i_1_n_4\,
      I1 => \euros1__1_carry__3_i_9_n_4\,
      I2 => \euros1__1_carry__3_i_9_n_6\,
      O => \euros1__92_carry__5_i_2_n_0\
    );
\euros1__92_carry__5_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669969669699669"
    )
        port map (
      I0 => \euros1__1_carry__3_i_9_n_5\,
      I1 => \euros1__1_carry__7_i_1_n_7\,
      I2 => \euros1__1_carry__7_i_1_n_5\,
      I3 => \euros1__1_carry__3_i_9_n_4\,
      I4 => \euros1__1_carry__3_i_9_n_6\,
      I5 => \euros1__1_carry__7_i_1_n_6\,
      O => \euros1__92_carry__5_i_3_n_0\
    );
\euros1__92_carry__5_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__92_carry__5_i_1_n_0\,
      I1 => \euros1__1_carry__3_i_9_n_6\,
      I2 => \euros1__1_carry__3_i_9_n_4\,
      I3 => \euros1__1_carry__7_i_1_n_6\,
      O => \euros1__92_carry__5_i_4_n_0\
    );
\euros1__92_carry__5_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \euros1__1_carry__3_i_9_n_7\,
      I1 => \euros1__1_carry__7_i_1_n_7\,
      I2 => \euros1__1_carry__3_i_9_n_5\,
      I3 => \euros1__92_carry__5_i_2_n_0\,
      O => \euros1__92_carry__5_i_5_n_0\
    );
\euros1__92_carry_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^o23\(0),
      O => \euros1__92_carry_i_1_n_0\
    );
\euros1__92_carry_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^o23\(1),
      I1 => \^o23\(3),
      I2 => \^o23\(0),
      O => \euros1__92_carry_i_2_n_0\
    );
\euros1__92_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^o23\(0),
      I1 => \^o23\(2),
      O => \euros1__92_carry_i_3_n_0\
    );
\euros1__92_carry_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^o23\(1),
      O => \euros1__92_carry_i_4_n_0\
    );
\i___0_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(1),
      I1 => cent(6),
      O => \i___0_carry__0_i_1_n_0\
    );
\i___0_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I1 => cent(0),
      O => \i___0_carry__0_i_2_n_0\
    );
\i___0_carry__0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BB88BBBB88B88888"
    )
        port map (
      I0 => \^o23\(6),
      I1 => \^string_cent_decenas[1]5_carry_i_10_0\,
      I2 => \euros1__339_carry__0_n_4\,
      I3 => \string_cent_decenas[1]5_carry__1_i_8_n_0\,
      I4 => \euros1__339_carry__0_n_6\,
      I5 => \euros1__339_carry__0_n_5\,
      O => cent(6)
    );
\i___0_carry__0_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      O => \i___0_carry__0_i_4_n_0\
    );
\i___0_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6966"
    )
        port map (
      I0 => cent(6),
      I1 => cent(1),
      I2 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I3 => cent(0),
      O => \i___0_carry__0_i_5_n_0\
    );
\i___0_carry__0_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I1 => cent(0),
      O => \i___0_carry__0_i_6_n_0\
    );
\i___0_carry__0_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(6),
      I1 => cent(4),
      O => \i___0_carry__0_i_7_n_0\
    );
\i___0_carry__0_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I1 => cent(3),
      O => \i___0_carry__0_i_8_n_0\
    );
\i___0_carry__1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => cent(1),
      I1 => cent(6),
      O => \i___0_carry__1_i_1_n_0\
    );
\i___0_carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      O => \i___0_carry__1_i_2_n_0\
    );
\i___0_carry__1_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BB88BB88BB88B88B"
    )
        port map (
      I0 => \^o23\(4),
      I1 => \^string_cent_decenas[1]5_carry_i_10_0\,
      I2 => \string_cent_decenas[1]5__186_carry_i_8_n_0\,
      I3 => \euros1__339_carry__0_n_7\,
      I4 => \euros1__339_carry_n_5\,
      I5 => \euros1__339_carry_n_4\,
      O => \i___0_carry__1_i_3_n_0\
    );
\i___0_carry__1_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCC3"
    )
        port map (
      I0 => \^o23\(3),
      I1 => \euros1__339_carry_n_4\,
      I2 => \string_cent_decenas[1]5__186_carry_i_8_n_0\,
      I3 => \euros1__339_carry_n_5\,
      I4 => \^string_cent_decenas[1]5_carry_i_10_0\,
      O => \i___0_carry__1_i_4_n_0\
    );
\i___0_carry__1_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"87"
    )
        port map (
      I0 => cent(6),
      I1 => cent(1),
      I2 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      O => \i___0_carry__1_i_5_n_0\
    );
\i___0_carry__2_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BB88BBBB88B88888"
    )
        port map (
      I0 => \^o23\(6),
      I1 => \^string_cent_decenas[1]5_carry_i_10_0\,
      I2 => \euros1__339_carry__0_n_4\,
      I3 => \string_cent_decenas[1]5_carry__1_i_8_n_0\,
      I4 => \euros1__339_carry__0_n_6\,
      I5 => \euros1__339_carry__0_n_5\,
      O => \i___0_carry__2_i_1_n_0\
    );
\i___0_carry_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BB88BB88BB88B88B"
    )
        port map (
      I0 => \^o23\(4),
      I1 => \^string_cent_decenas[1]5_carry_i_10_0\,
      I2 => \string_cent_decenas[1]5__186_carry_i_8_n_0\,
      I3 => \euros1__339_carry__0_n_7\,
      I4 => \euros1__339_carry_n_5\,
      I5 => \euros1__339_carry_n_4\,
      O => cent(4)
    );
\i___0_carry_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      O => \i___0_carry_i_2_n_0\
    );
\i___0_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I1 => cent(4),
      O => \i___0_carry_i_3_n_0\
    );
\i___0_carry_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(1),
      I1 => cent(3),
      O => \i___0_carry_i_4_n_0\
    );
\i___0_carry_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(0),
      I1 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      O => \i___0_carry_i_5_n_0\
    );
\i___0_carry_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^o23\(1),
      I1 => \^string_cent_decenas[1]5_carry_i_10_0\,
      I2 => p_1_in(1),
      O => \i___0_carry_i_6_n_0\
    );
\i___106_carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0880"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_6\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_5\,
      O => \i___106_carry__0_i_1_n_0\
    );
\i___106_carry__0_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BE282828"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___0_carry__2_n_2\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_5\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_6\,
      I3 => cent(0),
      I4 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_6\,
      O => \i___106_carry__0_i_2_n_0\
    );
\i___106_carry__0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"28"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___0_carry__2_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_6\,
      I2 => cent(0),
      O => \i___106_carry__0_i_3_n_0\
    );
\i___106_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_4\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_7\,
      O => \i___106_carry__0_i_4_n_0\
    );
\i___106_carry__0_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F087870F"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_5\,
      I2 => \i___106_carry__0_i_9_n_0\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_5\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_4\,
      O => \i___106_carry__0_i_5_n_0\
    );
\i___106_carry__0_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => \i___106_carry__0_i_2_n_0\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_5\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_6\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_5\,
      O => \i___106_carry__0_i_6_n_0\
    );
\i___106_carry__0_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669966996"
    )
        port map (
      I0 => \i___106_carry__0_i_3_n_0\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___0_carry__2_n_2\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_5\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_6\,
      I4 => cent(0),
      I5 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_6\,
      O => \i___106_carry__0_i_7_n_0\
    );
\i___106_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___0_carry__2_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_6\,
      I2 => cent(0),
      I3 => \i___106_carry__0_i_4_n_0\,
      O => \i___106_carry__0_i_8_n_0\
    );
\i___106_carry__0_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_7\,
      I1 => cent(0),
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_4\,
      O => \i___106_carry__0_i_9_n_0\
    );
\i___106_carry__1_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6900696900006900"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_5\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_5\,
      I4 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I5 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_6\,
      O => \i___106_carry__1_i_1_n_0\
    );
\i___106_carry__1_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_4\,
      I2 => cent(0),
      O => \i___106_carry__1_i_10_n_0\
    );
\i___106_carry__1_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_5\,
      I1 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_6\,
      O => \i___106_carry__1_i_11_n_0\
    );
\i___106_carry__1_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_6\,
      I1 => cent(1),
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_7\,
      O => \i___106_carry__1_i_12_n_0\
    );
\i___106_carry__1_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E8E800E80000E8"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_7\,
      I2 => cent(1),
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_6\,
      I4 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I5 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_5\,
      O => \i___106_carry__1_i_2_n_0\
    );
\i___106_carry__1_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E80000E800E8E800"
    )
        port map (
      I0 => cent(0),
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_4\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_7\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_7\,
      I4 => cent(1),
      I5 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_6\,
      O => \i___106_carry__1_i_3_n_0\
    );
\i___106_carry__1_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80080880"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_4\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_5\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_4\,
      I3 => cent(0),
      I4 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_7\,
      O => \i___106_carry__1_i_4_n_0\
    );
\i___106_carry__1_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6996996699669669"
    )
        port map (
      I0 => \i___106_carry__1_i_1_n_0\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_4\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_6\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_7\,
      I5 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_5\,
      O => \i___106_carry__1_i_5_n_0\
    );
\i___106_carry__1_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969669"
    )
        port map (
      I0 => \i___106_carry__1_i_2_n_0\,
      I1 => \i___106_carry__1_i_9_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_5\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_7\,
      O => \i___106_carry__1_i_6_n_0\
    );
\i___106_carry__1_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7EE88117"
    )
        port map (
      I0 => \i___106_carry__1_i_10_n_0\,
      I1 => cent(1),
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_7\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_6\,
      I4 => \i___106_carry__1_i_11_n_0\,
      O => \i___106_carry__1_i_7_n_0\
    );
\i___106_carry__1_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FF8F8808007077F"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_4\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_7\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_4\,
      I4 => cent(0),
      I5 => \i___106_carry__1_i_12_n_0\,
      O => \i___106_carry__1_i_8_n_0\
    );
\i___106_carry__1_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_6\,
      I1 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_5\,
      O => \i___106_carry__1_i_9_n_0\
    );
\i___106_carry__2_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"90006660"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_6\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_4\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__2_i_1_n_0\
    );
\i___106_carry__2_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_4\,
      O => \i___106_carry__2_i_10_n_0\
    );
\i___106_carry__2_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_5\,
      O => \i___106_carry__2_i_11_n_0\
    );
\i___106_carry__2_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60009990"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_4\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_6\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_7\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_5\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__2_i_2_n_0\
    );
\i___106_carry__2_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60009990"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_7\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_6\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__2_i_3_n_0\
    );
\i___106_carry__2_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08E0800E"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_7\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_6\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_4\,
      O => \i___106_carry__2_i_4_n_0\
    );
\i___106_carry__2_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFEFE808001017F"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_4\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_6\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_7\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I5 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_6\,
      O => \i___106_carry__2_i_5_n_0\
    );
\i___106_carry__2_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0E7070F1F18F8F0E"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_7\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_4\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_6\,
      I5 => \i___106_carry__2_i_9_n_0\,
      O => \i___106_carry__2_i_6_n_0\
    );
\i___106_carry__2_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0E7070F1F18F8F0E"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_4\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_5\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_7\,
      I5 => \i___106_carry__2_i_10_n_0\,
      O => \i___106_carry__2_i_7_n_0\
    );
\i___106_carry__2_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0E7070F1F18F8F0E"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_5\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_6\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_4\,
      I5 => \i___106_carry__2_i_11_n_0\,
      O => \i___106_carry__2_i_8_n_0\
    );
\i___106_carry__2_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_7\,
      O => \i___106_carry__2_i_9_n_0\
    );
\i___106_carry__3_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1062"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__3_i_1_n_0\
    );
\i___106_carry__3_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_4\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      O => \i___106_carry__3_i_2_n_0\
    );
\i___106_carry__3_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1062"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_6\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__3_i_3_n_0\
    );
\i___106_carry__3_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1062"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_7\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__3_i_4_n_0\
    );
\i___106_carry__3_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEF80107"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_4\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_7\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_6\,
      O => \i___106_carry__3_i_5_n_0\
    );
\i___106_carry__3_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEF80107"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_7\,
      O => \i___106_carry__3_i_6_n_0\
    );
\i___106_carry__3_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"56A96A95"
    )
        port map (
      I0 => \i___106_carry__3_i_3_n_0\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_4\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_5\,
      O => \i___106_carry__3_i_7_n_0\
    );
\i___106_carry__3_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAA9A9A5"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_6\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_7\,
      O => \i___106_carry__3_i_8_n_0\
    );
\i___106_carry__4_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"48"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__4_i_1_n_0\
    );
\i___106_carry__4_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1062"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_1\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_6\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__4_i_2_n_0\
    );
\i___106_carry__4_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1062"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_7\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__4_i_3_n_0\
    );
\i___106_carry__4_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"95"
    )
        port map (
      I0 => '0',
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__4_i_4_n_0\
    );
\i___106_carry__4_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_1\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__4_i_5_n_0\
    );
\i___106_carry__4_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0107"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_1\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      O => \i___106_carry__4_i_6_n_0\
    );
\i___106_carry__4_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEF80107"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_6\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_1\,
      O => \i___106_carry__4_i_7_n_0\
    );
\i___106_carry__5_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      O => \i___106_carry__5_i_1_n_0\
    );
\i___106_carry__5_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"95"
    )
        port map (
      I0 => '0',
      I1 => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      O => \i___106_carry__5_i_2_n_0\
    );
\i___106_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_4\,
      O => \i___106_carry_i_1_n_0\
    );
\i___106_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_5\,
      O => \i___106_carry_i_2_n_0\
    );
\i___106_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_6\,
      O => \i___106_carry_i_3_n_0\
    );
\i___106_carry_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => cent(0),
      I1 => \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_4\,
      O => \i___106_carry_i_4_n_0\
    );
\i___106_carry_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_4\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_7\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_5\,
      O => \i___106_carry_i_5_n_0\
    );
\i___106_carry_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_5\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_6\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_4\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_5\,
      O => \i___106_carry_i_6_n_0\
    );
\i___106_carry_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_7\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_5\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_6\,
      O => \i___106_carry_i_7_n_0\
    );
\i___106_carry_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_4\,
      I1 => cent(0),
      I2 => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_6\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_7\,
      O => \i___106_carry_i_8_n_0\
    );
\i___163_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_4\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_7\,
      O => \i___163_carry_i_1_n_0\
    );
\i___163_carry_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66969969"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___106_carry__5_n_6\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_5\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___106_carry__5_n_7\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_6\,
      I4 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_7\,
      O => \i___163_carry_i_2_n_0\
    );
\i___163_carry_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_7\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_4\,
      I2 => \string_cent_decenas[1]5_inferred__0/i___106_carry__5_n_7\,
      I3 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_6\,
      O => \i___163_carry_i_3_n_0\
    );
\i___163_carry_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_4\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_7\,
      O => \i___163_carry_i_4_n_0\
    );
\i___169_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      O => \i___169_carry__0_i_1_n_0\
    );
\i___169_carry__0_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \string_cent_decenas[1]5_inferred__0/i___163_carry_n_5\,
      O => \i___169_carry__0_i_2_n_0\
    );
\i___169_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(6),
      I1 => \string_cent_decenas[1]5_inferred__0/i___163_carry_n_6\,
      O => \i___169_carry__0_i_3_n_0\
    );
\i___169_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___163_carry_n_7\,
      O => \i___169_carry__0_i_4_n_0\
    );
\i___169_carry__0_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(4),
      I1 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_5\,
      O => \i___169_carry__0_i_5_n_0\
    );
\i___169_carry_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      O => cent(2)
    );
\i___169_carry_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^o23\(1),
      I1 => \^string_cent_decenas[1]5_carry_i_10_0\,
      I2 => p_1_in(1),
      O => \i___169_carry_i_2_n_0\
    );
\i___169_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(3),
      I1 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_6\,
      O => \i___169_carry_i_3_n_0\
    );
\i___169_carry_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I1 => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_7\,
      O => \i___169_carry_i_4_n_0\
    );
\i___169_carry_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(1),
      O => \i___169_carry_i_5_n_0\
    );
\i___169_carry_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(0),
      O => \i___169_carry_i_6_n_0\
    );
\i___27_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(3),
      I1 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      O => \i___27_carry__0_i_1_n_0\
    );
\i___27_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => cent(6),
      I1 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I2 => cent(4),
      O => \i___27_carry__0_i_2_n_0\
    );
\i___27_carry__0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8E"
    )
        port map (
      I0 => cent(0),
      I1 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I2 => cent(4),
      O => \i___27_carry__0_i_3_n_0\
    );
\i___27_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(1),
      I1 => cent(3),
      O => \i___27_carry__0_i_4_n_0\
    );
\i___27_carry__0_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E81717E8"
    )
        port map (
      I0 => cent(4),
      I1 => cent(6),
      I2 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I3 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I4 => cent(3),
      O => \i___27_carry__0_i_5_n_0\
    );
\i___27_carry__0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8E71718E718E8E71"
    )
        port map (
      I0 => cent(1),
      I1 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I2 => cent(3),
      I3 => cent(4),
      I4 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I5 => cent(6),
      O => \i___27_carry__0_i_6_n_0\
    );
\i___27_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \i___27_carry__0_i_3_n_0\,
      I1 => cent(3),
      I2 => cent(1),
      I3 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      O => \i___27_carry__0_i_7_n_0\
    );
\i___27_carry__0_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69696996"
    )
        port map (
      I0 => cent(0),
      I1 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I2 => cent(4),
      I3 => cent(3),
      I4 => cent(1),
      O => \i___27_carry__0_i_8_n_0\
    );
\i___27_carry__1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => cent(4),
      I1 => cent(6),
      O => \i___27_carry__1_i_1_n_0\
    );
\i___27_carry__1_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => cent(3),
      I1 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      O => \i___27_carry__1_i_2_n_0\
    );
\i___27_carry__1_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(6),
      O => \i___27_carry__1_i_3_n_0\
    );
\i___27_carry__1_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B4"
    )
        port map (
      I0 => cent(4),
      I1 => cent(6),
      I2 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      O => \i___27_carry__1_i_4_n_0\
    );
\i___27_carry__1_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"E11E"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I1 => cent(3),
      I2 => cent(6),
      I3 => cent(4),
      O => \i___27_carry__1_i_5_n_0\
    );
\i___27_carry_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(0),
      O => \i___27_carry_i_1_n_0\
    );
\i___27_carry_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => cent(0),
      I1 => cent(3),
      I2 => cent(1),
      O => \i___27_carry_i_2_n_0\
    );
\i___27_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(0),
      I1 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      O => \i___27_carry_i_3_n_0\
    );
\i___27_carry_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(1),
      O => \i___27_carry_i_4_n_0\
    );
\i___27_carry_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEF00000020"
    )
        port map (
      I0 => \^o23\(0),
      I1 => \string_cent_decenas[1]5_carry_i_7_n_0\,
      I2 => \string_cent_decenas[1]5_carry_i_8_n_0\,
      I3 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I4 => \string_cent_decenas[1]5_carry_i_10_n_0\,
      I5 => p_1_in(0),
      O => \i___27_carry_i_5_n_0\
    );
\i___56_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      O => \i___56_carry__0_i_1_n_0\
    );
\i___56_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^o23\(1),
      I1 => \^string_cent_decenas[1]5_carry_i_10_0\,
      I2 => p_1_in(1),
      O => \i___56_carry__0_i_2_n_0\
    );
\i___56_carry__0_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(4),
      O => \i___56_carry__0_i_3_n_0\
    );
\i___56_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(3),
      I1 => cent(6),
      O => \i___56_carry__0_i_4_n_0\
    );
\i___56_carry__0_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I1 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      O => \i___56_carry__0_i_5_n_0\
    );
\i___56_carry__0_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(1),
      I1 => cent(4),
      O => \i___56_carry__0_i_6_n_0\
    );
\i___56_carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      O => cent(5)
    );
\i___56_carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(6),
      O => \i___56_carry__1_i_2_n_0\
    );
\i___56_carry__1_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"555555550FF30CF3"
    )
        port map (
      I0 => \^o23\(5),
      I1 => \euros1__339_carry__0_n_4\,
      I2 => \string_cent_decenas[1]5_carry__1_i_8_n_0\,
      I3 => \euros1__339_carry__0_n_6\,
      I4 => \euros1__339_carry__0_n_5\,
      I5 => \^string_cent_decenas[1]5_carry_i_10_0\,
      O => \i___56_carry__1_i_3_n_0\
    );
\i___56_carry_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__2_i_9_n_0\,
      O => \i___56_carry_i_1_n_0\
    );
\i___56_carry_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"553C"
    )
        port map (
      I0 => \^o23\(2),
      I1 => \euros1__339_carry_n_5\,
      I2 => \string_cent_decenas[1]5__186_carry_i_8_n_0\,
      I3 => \^string_cent_decenas[1]5_carry_i_10_0\,
      O => \i___56_carry_i_2_n_0\
    );
\i___56_carry_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(1),
      O => \i___56_carry_i_3_n_0\
    );
\i___56_carry_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEF00000020"
    )
        port map (
      I0 => \^o23\(0),
      I1 => \string_cent_decenas[1]5_carry_i_7_n_0\,
      I2 => \string_cent_decenas[1]5_carry_i_8_n_0\,
      I3 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I4 => \string_cent_decenas[1]5_carry_i_10_n_0\,
      I5 => p_1_in(0),
      O => \i___56_carry_i_4_n_0\
    );
\i___80_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => cent(3),
      I1 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      O => \i___80_carry__0_i_1_n_0\
    );
\i___80_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I1 => cent(3),
      O => \i___80_carry__0_i_2_n_0\
    );
\i___80_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => cent(3),
      I1 => cent(1),
      O => \i___80_carry__0_i_3_n_0\
    );
\i___80_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(1),
      I1 => cent(3),
      O => \i___80_carry__0_i_4_n_0\
    );
\i___80_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4BB4"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I1 => cent(3),
      I2 => cent(4),
      I3 => cent(6),
      O => \i___80_carry__0_i_5_n_0\
    );
\i___80_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B44B"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I1 => cent(4),
      I2 => cent(3),
      I3 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      O => \i___80_carry__0_i_6_n_0\
    );
\i___80_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7887"
    )
        port map (
      I0 => cent(1),
      I1 => cent(3),
      I2 => cent(4),
      I3 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      O => \i___80_carry__0_i_7_n_0\
    );
\i___80_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6966"
    )
        port map (
      I0 => cent(3),
      I1 => cent(1),
      I2 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I3 => cent(0),
      O => \i___80_carry__0_i_8_n_0\
    );
\i___80_carry__1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => cent(4),
      I1 => cent(6),
      O => \i___80_carry__1_i_1_n_0\
    );
\i___80_carry__1_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BB88BBBB88B88888"
    )
        port map (
      I0 => \^o23\(6),
      I1 => \^string_cent_decenas[1]5_carry_i_10_0\,
      I2 => \euros1__339_carry__0_n_4\,
      I3 => \string_cent_decenas[1]5_carry__1_i_8_n_0\,
      I4 => \euros1__339_carry__0_n_6\,
      I5 => \euros1__339_carry__0_n_5\,
      O => \i___80_carry__1_i_2_n_0\
    );
\i___80_carry__1_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"87"
    )
        port map (
      I0 => cent(6),
      I1 => cent(4),
      I2 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      O => \i___80_carry__1_i_3_n_0\
    );
\i___80_carry_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      O => \i___80_carry_i_1_n_0\
    );
\i___80_carry_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => cent(0),
      I1 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I2 => cent(6),
      O => \i___80_carry_i_2_n_0\
    );
\i___80_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I1 => cent(1),
      O => \i___80_carry_i_3_n_0\
    );
\i___80_carry_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(4),
      I1 => cent(0),
      O => \i___80_carry_i_4_n_0\
    );
\i___80_carry_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCC3"
    )
        port map (
      I0 => \^o23\(3),
      I1 => \euros1__339_carry_n_4\,
      I2 => \string_cent_decenas[1]5__186_carry_i_8_n_0\,
      I3 => \euros1__339_carry_n_5\,
      I4 => \^string_cent_decenas[1]5_carry_i_10_0\,
      O => \i___80_carry_i_5_n_0\
    );
\string_cent_decenas[1]5__100_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5__100_carry_n_0\,
      CO(2) => \string_cent_decenas[1]5__100_carry_n_1\,
      CO(1) => \string_cent_decenas[1]5__100_carry_n_2\,
      CO(0) => \string_cent_decenas[1]5__100_carry_n_3\,
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__100_carry_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__100_carry_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__100_carry_i_3_n_0\,
      DI(0) => '0',
      O(3) => \string_cent_decenas[1]5__100_carry_n_4\,
      O(2) => \string_cent_decenas[1]5__100_carry_n_5\,
      O(1) => \string_cent_decenas[1]5__100_carry_n_6\,
      O(0) => \string_cent_decenas[1]5__100_carry_n_7\,
      S(3) => \string_cent_decenas[1]5__100_carry_i_4_n_0\,
      S(2) => \string_cent_decenas[1]5__100_carry_i_5_n_0\,
      S(1) => \string_cent_decenas[1]5__100_carry_i_6_n_0\,
      S(0) => \string_cent_decenas[1]5__100_carry_i_7_n_0\
    );
\string_cent_decenas[1]5__100_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__100_carry_n_0\,
      CO(3) => \string_cent_decenas[1]5__100_carry__0_n_0\,
      CO(2) => \string_cent_decenas[1]5__100_carry__0_n_1\,
      CO(1) => \string_cent_decenas[1]5__100_carry__0_n_2\,
      CO(0) => \string_cent_decenas[1]5__100_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__100_carry__0_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__100_carry__0_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__100_carry__0_i_3_n_0\,
      DI(0) => \string_cent_decenas[1]5__100_carry__0_i_4_n_0\,
      O(3) => \string_cent_decenas[1]5__100_carry__0_n_4\,
      O(2) => \string_cent_decenas[1]5__100_carry__0_n_5\,
      O(1) => \string_cent_decenas[1]5__100_carry__0_n_6\,
      O(0) => \string_cent_decenas[1]5__100_carry__0_n_7\,
      S(3) => \string_cent_decenas[1]5__100_carry__0_i_5_n_0\,
      S(2) => \string_cent_decenas[1]5__100_carry__0_i_6_n_0\,
      S(1) => \string_cent_decenas[1]5__100_carry__0_i_7_n_0\,
      S(0) => \string_cent_decenas[1]5__100_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5__100_carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => cent(4),
      I1 => cent(6),
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__100_carry__0_i_1_n_0\
    );
\string_cent_decenas[1]5__100_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8E"
    )
        port map (
      I0 => cent(3),
      I1 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__100_carry__0_i_2_n_0\
    );
\string_cent_decenas[1]5__100_carry__0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => cent(4),
      I1 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__100_carry__0_i_3_n_0\
    );
\string_cent_decenas[1]5__100_carry__0_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4D"
    )
        port map (
      I0 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I1 => cent(1),
      I2 => cent(3),
      O => \string_cent_decenas[1]5__100_carry__0_i_4_n_0\
    );
\string_cent_decenas[1]5__100_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B4D2"
    )
        port map (
      I0 => cent(6),
      I1 => cent(4),
      I2 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I3 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__100_carry__0_i_5_n_0\
    );
\string_cent_decenas[1]5__100_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \string_cent_decenas[1]5__100_carry__0_i_2_n_0\,
      I1 => cent(4),
      I2 => cent(6),
      I3 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__100_carry__0_i_6_n_0\
    );
\string_cent_decenas[1]5__100_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => cent(3),
      I1 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I3 => \string_cent_decenas[1]5__100_carry__0_i_3_n_0\,
      O => \string_cent_decenas[1]5__100_carry__0_i_7_n_0\
    );
\string_cent_decenas[1]5__100_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => cent(4),
      I1 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I3 => \string_cent_decenas[1]5__100_carry__0_i_4_n_0\,
      O => \string_cent_decenas[1]5__100_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5__100_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__100_carry__0_n_0\,
      CO(3 downto 0) => \NLW_string_cent_decenas[1]5__100_carry__1_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_string_cent_decenas[1]5__100_carry__1_O_UNCONNECTED\(3 downto 1),
      O(0) => \string_cent_decenas[1]5__100_carry__1_n_7\,
      S(3 downto 1) => B"000",
      S(0) => \string_cent_decenas[1]5__100_carry__1_i_1_n_0\
    );
\string_cent_decenas[1]5__100_carry__1_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"63"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I1 => cent(6),
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__100_carry__1_i_1_n_0\
    );
\string_cent_decenas[1]5__100_carry_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8E"
    )
        port map (
      I0 => cent(0),
      I1 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__100_carry_i_1_n_0\
    );
\string_cent_decenas[1]5__100_carry_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => cent(0),
      I1 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__100_carry_i_2_n_0\
    );
\string_cent_decenas[1]5__100_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I1 => cent(0),
      O => \string_cent_decenas[1]5__100_carry_i_3_n_0\
    );
\string_cent_decenas[1]5__100_carry_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I1 => cent(1),
      I2 => cent(3),
      I3 => \string_cent_decenas[1]5__100_carry_i_1_n_0\,
      O => \string_cent_decenas[1]5__100_carry_i_4_n_0\
    );
\string_cent_decenas[1]5__100_carry_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6966"
    )
        port map (
      I0 => cent(0),
      I1 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I3 => cent(1),
      O => \string_cent_decenas[1]5__100_carry_i_5_n_0\
    );
\string_cent_decenas[1]5__100_carry_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"63"
    )
        port map (
      I0 => cent(0),
      I1 => cent(1),
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__100_carry_i_6_n_0\
    );
\string_cent_decenas[1]5__100_carry_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(0),
      I1 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__100_carry_i_7_n_0\
    );
\string_cent_decenas[1]5__100_carry_i_8\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_carry__1_n_0\,
      CO(3 downto 1) => \NLW_string_cent_decenas[1]5__100_carry_i_8_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_string_cent_decenas[1]5__100_carry_i_8_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
\string_cent_decenas[1]5__124_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5__124_carry_n_0\,
      CO(2) => \string_cent_decenas[1]5__124_carry_n_1\,
      CO(1) => \string_cent_decenas[1]5__124_carry_n_2\,
      CO(0) => \string_cent_decenas[1]5__124_carry_n_3\,
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__124_carry_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__124_carry_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__124_carry_i_3_n_0\,
      DI(0) => \string_cent_decenas[1]5__124_carry_i_4_n_0\,
      O(3 downto 0) => \NLW_string_cent_decenas[1]5__124_carry_O_UNCONNECTED\(3 downto 0),
      S(3) => \string_cent_decenas[1]5__124_carry_i_5_n_0\,
      S(2) => \string_cent_decenas[1]5__124_carry_i_6_n_0\,
      S(1) => \string_cent_decenas[1]5__124_carry_i_7_n_0\,
      S(0) => \string_cent_decenas[1]5__124_carry_i_8_n_0\
    );
\string_cent_decenas[1]5__124_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__124_carry_n_0\,
      CO(3) => \string_cent_decenas[1]5__124_carry__0_n_0\,
      CO(2) => \string_cent_decenas[1]5__124_carry__0_n_1\,
      CO(1) => \string_cent_decenas[1]5__124_carry__0_n_2\,
      CO(0) => \string_cent_decenas[1]5__124_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__124_carry__0_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__124_carry__0_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__124_carry__0_i_3_n_0\,
      DI(0) => \string_cent_decenas[1]5__124_carry__0_i_4_n_0\,
      O(3 downto 0) => \NLW_string_cent_decenas[1]5__124_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \string_cent_decenas[1]5__124_carry__0_i_5_n_0\,
      S(2) => \string_cent_decenas[1]5__124_carry__0_i_6_n_0\,
      S(1) => \string_cent_decenas[1]5__124_carry__0_i_7_n_0\,
      S(0) => \string_cent_decenas[1]5__124_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5__124_carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"82"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry__0_n_7\,
      I1 => \string_cent_decenas[1]5_carry_n_7\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__124_carry__0_i_1_n_0\
    );
\string_cent_decenas[1]5__124_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry_n_4\,
      I1 => \string_cent_decenas[1]5_carry__1_n_4\,
      O => \string_cent_decenas[1]5__124_carry__0_i_2_n_0\
    );
\string_cent_decenas[1]5__124_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry_n_5\,
      I1 => \string_cent_decenas[1]5_carry__1_n_5\,
      O => \string_cent_decenas[1]5__124_carry__0_i_3_n_0\
    );
\string_cent_decenas[1]5__124_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry_n_6\,
      I1 => \string_cent_decenas[1]5_carry__1_n_6\,
      O => \string_cent_decenas[1]5__124_carry__0_i_4_n_0\
    );
\string_cent_decenas[1]5__124_carry__0_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99966669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry__0_n_6\,
      I1 => \string_cent_decenas[1]5__55_carry_n_6\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I3 => \string_cent_decenas[1]5_carry_n_7\,
      I4 => \string_cent_decenas[1]5__124_carry__0_i_1_n_0\,
      O => \string_cent_decenas[1]5__124_carry__0_i_5_n_0\
    );
\string_cent_decenas[1]5__124_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry__0_n_7\,
      I1 => \string_cent_decenas[1]5_carry_n_7\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I3 => \string_cent_decenas[1]5__124_carry__0_i_2_n_0\,
      O => \string_cent_decenas[1]5__124_carry__0_i_6_n_0\
    );
\string_cent_decenas[1]5__124_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry_n_4\,
      I1 => \string_cent_decenas[1]5_carry__1_n_4\,
      I2 => \string_cent_decenas[1]5_carry__1_n_5\,
      I3 => \string_cent_decenas[1]5__27_carry_n_5\,
      O => \string_cent_decenas[1]5__124_carry__0_i_7_n_0\
    );
\string_cent_decenas[1]5__124_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__1_n_6\,
      I1 => \string_cent_decenas[1]5__27_carry_n_6\,
      I2 => \string_cent_decenas[1]5_carry__1_n_5\,
      I3 => \string_cent_decenas[1]5__27_carry_n_5\,
      O => \string_cent_decenas[1]5__124_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5__124_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__124_carry__0_n_0\,
      CO(3) => \string_cent_decenas[1]5__124_carry__1_n_0\,
      CO(2) => \string_cent_decenas[1]5__124_carry__1_n_1\,
      CO(1) => \string_cent_decenas[1]5__124_carry__1_n_2\,
      CO(0) => \string_cent_decenas[1]5__124_carry__1_n_3\,
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__124_carry__1_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__124_carry__1_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__124_carry__1_i_3_n_0\,
      DI(0) => \string_cent_decenas[1]5__124_carry__1_i_4_n_0\,
      O(3 downto 0) => \NLW_string_cent_decenas[1]5__124_carry__1_O_UNCONNECTED\(3 downto 0),
      S(3) => \string_cent_decenas[1]5__124_carry__1_i_5_n_0\,
      S(2) => \string_cent_decenas[1]5__124_carry__1_i_6_n_0\,
      S(1) => \string_cent_decenas[1]5__124_carry__1_i_7_n_0\,
      S(0) => \string_cent_decenas[1]5__124_carry__1_i_8_n_0\
    );
\string_cent_decenas[1]5__124_carry__1_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8B82"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry__1_n_7\,
      I1 => \string_cent_decenas[1]5__55_carry__0_n_7\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I3 => \string_cent_decenas[1]5__55_carry_n_4\,
      O => \string_cent_decenas[1]5__124_carry__1_i_1_n_0\
    );
\string_cent_decenas[1]5__124_carry__1_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8B82"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry__0_n_4\,
      I1 => \string_cent_decenas[1]5__55_carry_n_4\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I3 => \string_cent_decenas[1]5__55_carry_n_5\,
      O => \string_cent_decenas[1]5__124_carry__1_i_2_n_0\
    );
\string_cent_decenas[1]5__124_carry__1_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8B82"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5__55_carry_n_5\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I3 => \string_cent_decenas[1]5__55_carry_n_6\,
      O => \string_cent_decenas[1]5__124_carry__1_i_3_n_0\
    );
\string_cent_decenas[1]5__124_carry__1_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8B82"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry__0_n_6\,
      I1 => \string_cent_decenas[1]5__55_carry_n_6\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I3 => \string_cent_decenas[1]5_carry_n_7\,
      O => \string_cent_decenas[1]5__124_carry__1_i_4_n_0\
    );
\string_cent_decenas[1]5__124_carry__1_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3CC3F00F87781EE1"
    )
        port map (
      I0 => \string_cent_decenas[1]5__55_carry_n_4\,
      I1 => \string_cent_decenas[1]5__27_carry__1_n_7\,
      I2 => \string_cent_decenas[1]5__27_carry__1_n_6\,
      I3 => \string_cent_decenas[1]5__124_carry__1_i_9_n_0\,
      I4 => \string_cent_decenas[1]5__55_carry__0_n_7\,
      I5 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__124_carry__1_i_5_n_0\
    );
\string_cent_decenas[1]5__124_carry__1_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96969669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__1_i_2_n_0\,
      I1 => \string_cent_decenas[1]5__27_carry__1_n_7\,
      I2 => \string_cent_decenas[1]5__55_carry__0_n_7\,
      I3 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I4 => \string_cent_decenas[1]5__55_carry_n_4\,
      O => \string_cent_decenas[1]5__124_carry__1_i_6_n_0\
    );
\string_cent_decenas[1]5__124_carry__1_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99966669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry__0_n_4\,
      I1 => \string_cent_decenas[1]5__55_carry_n_4\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I3 => \string_cent_decenas[1]5__55_carry_n_5\,
      I4 => \string_cent_decenas[1]5__124_carry__1_i_3_n_0\,
      O => \string_cent_decenas[1]5__124_carry__1_i_7_n_0\
    );
\string_cent_decenas[1]5__124_carry__1_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99966669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5__55_carry_n_5\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I3 => \string_cent_decenas[1]5__55_carry_n_6\,
      I4 => \string_cent_decenas[1]5__124_carry__1_i_4_n_0\,
      O => \string_cent_decenas[1]5__124_carry__1_i_8_n_0\
    );
\string_cent_decenas[1]5__124_carry__1_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \string_cent_decenas[1]5__55_carry__0_n_6\,
      I1 => cent(0),
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__124_carry__1_i_9_n_0\
    );
\string_cent_decenas[1]5__124_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__124_carry__1_n_0\,
      CO(3) => \string_cent_decenas[1]5__124_carry__2_n_0\,
      CO(2) => \string_cent_decenas[1]5__124_carry__2_n_1\,
      CO(1) => \string_cent_decenas[1]5__124_carry__2_n_2\,
      CO(0) => \string_cent_decenas[1]5__124_carry__2_n_3\,
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__124_carry__2_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__124_carry__2_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__124_carry__2_i_3_n_0\,
      DI(0) => \string_cent_decenas[1]5__124_carry__2_i_4_n_0\,
      O(3 downto 0) => \NLW_string_cent_decenas[1]5__124_carry__2_O_UNCONNECTED\(3 downto 0),
      S(3) => \string_cent_decenas[1]5__124_carry__2_i_5_n_0\,
      S(2) => \string_cent_decenas[1]5__124_carry__2_i_6_n_0\,
      S(1) => \string_cent_decenas[1]5__124_carry__2_i_7_n_0\,
      S(0) => \string_cent_decenas[1]5__124_carry__2_i_8_n_0\
    );
\string_cent_decenas[1]5__124_carry__2_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"517520FB20FB5175"
    )
        port map (
      I0 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I1 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I2 => \string_cent_decenas[1]5__55_carry__0_n_4\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__55_carry__1_n_7\,
      I5 => \string_cent_decenas[1]5__124_carry__2_i_9_n_0\,
      O => \string_cent_decenas[1]5__124_carry__2_i_1_n_0\
    );
\string_cent_decenas[1]5__124_carry__2_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I1 => \string_cent_decenas[1]5__55_carry__1_n_7\,
      I2 => \string_cent_decenas[1]5__124_carry__2_i_9_n_0\,
      O => \string_cent_decenas[1]5__124_carry__2_i_10_n_0\
    );
\string_cent_decenas[1]5__124_carry__2_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__2_i_9_n_0\,
      I1 => \string_cent_decenas[1]5__55_carry__1_n_7\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__124_carry__2_i_11_n_0\
    );
\string_cent_decenas[1]5__124_carry__2_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \string_cent_decenas[1]5__55_carry__0_n_6\,
      I1 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I2 => cent(0),
      O => \string_cent_decenas[1]5__124_carry__2_i_12_n_0\
    );
\string_cent_decenas[1]5__124_carry__2_i_13\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \string_cent_decenas[1]5__55_carry__0_n_4\,
      I1 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__124_carry__2_i_13_n_0\
    );
\string_cent_decenas[1]5__124_carry__2_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \string_cent_decenas[1]5__55_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I2 => cent(1),
      O => \string_cent_decenas[1]5__124_carry__2_i_14_n_0\
    );
\string_cent_decenas[1]5__124_carry__2_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"96FF14D714D70096"
    )
        port map (
      I0 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I1 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I2 => \string_cent_decenas[1]5__55_carry__0_n_4\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => cent(1),
      I5 => \string_cent_decenas[1]5__55_carry__0_n_5\,
      O => \string_cent_decenas[1]5__124_carry__2_i_2_n_0\
    );
\string_cent_decenas[1]5__124_carry__2_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"32B380FE80FE32B3"
    )
        port map (
      I0 => \string_cent_decenas[1]5__55_carry__0_n_6\,
      I1 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I2 => cent(0),
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => cent(1),
      I5 => \string_cent_decenas[1]5__55_carry__0_n_5\,
      O => \string_cent_decenas[1]5__124_carry__2_i_3_n_0\
    );
\string_cent_decenas[1]5__124_carry__2_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7D416900"
    )
        port map (
      I0 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I1 => cent(0),
      I2 => \string_cent_decenas[1]5__55_carry__0_n_6\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_6\,
      I4 => \string_cent_decenas[1]5__55_carry__0_n_7\,
      O => \string_cent_decenas[1]5__124_carry__2_i_4_n_0\
    );
\string_cent_decenas[1]5__124_carry__2_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__2_i_1_n_0\,
      I1 => \string_cent_decenas[1]5__124_carry__2_i_10_n_0\,
      I2 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I3 => \string_cent_decenas[1]5__79_carry_n_6\,
      I4 => \string_cent_decenas[1]5__55_carry__1_n_6\,
      I5 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__124_carry__2_i_5_n_0\
    );
\string_cent_decenas[1]5__124_carry__2_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6A5695A995A96A56"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__2_i_2_n_0\,
      I1 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I2 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I3 => \string_cent_decenas[1]5__55_carry__0_n_4\,
      I4 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I5 => \string_cent_decenas[1]5__124_carry__2_i_11_n_0\,
      O => \string_cent_decenas[1]5__124_carry__2_i_6_n_0\
    );
\string_cent_decenas[1]5__124_carry__2_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E78E8E18187171E7"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__2_i_12_n_0\,
      I1 => cent(1),
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I3 => \string_cent_decenas[1]5__55_carry__0_n_5\,
      I4 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I5 => \string_cent_decenas[1]5__124_carry__2_i_13_n_0\,
      O => \string_cent_decenas[1]5__124_carry__2_i_7_n_0\
    );
\string_cent_decenas[1]5__124_carry__2_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"65A69A599A5965A6"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__2_i_4_n_0\,
      I1 => \string_cent_decenas[1]5__55_carry__0_n_6\,
      I2 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I3 => cent(0),
      I4 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I5 => \string_cent_decenas[1]5__124_carry__2_i_14_n_0\,
      O => \string_cent_decenas[1]5__124_carry__2_i_8_n_0\
    );
\string_cent_decenas[1]5__124_carry__2_i_9\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(3),
      I1 => cent(0),
      O => \string_cent_decenas[1]5__124_carry__2_i_9_n_0\
    );
\string_cent_decenas[1]5__124_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__124_carry__2_n_0\,
      CO(3) => \string_cent_decenas[1]5__124_carry__3_n_0\,
      CO(2) => \string_cent_decenas[1]5__124_carry__3_n_1\,
      CO(1) => \string_cent_decenas[1]5__124_carry__3_n_2\,
      CO(0) => \string_cent_decenas[1]5__124_carry__3_n_3\,
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__124_carry__3_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__124_carry__3_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__124_carry__3_i_3_n_0\,
      DI(0) => \string_cent_decenas[1]5__124_carry__3_i_4_n_0\,
      O(3 downto 0) => \NLW_string_cent_decenas[1]5__124_carry__3_O_UNCONNECTED\(3 downto 0),
      S(3) => \string_cent_decenas[1]5__124_carry__3_i_5_n_0\,
      S(2) => \string_cent_decenas[1]5__124_carry__3_i_6_n_0\,
      S(1) => \string_cent_decenas[1]5__124_carry__3_i_7_n_0\,
      S(0) => \string_cent_decenas[1]5__124_carry__3_i_8_n_0\
    );
\string_cent_decenas[1]5__124_carry__3_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"D4FF00D4"
    )
        port map (
      I0 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I1 => \string_cent_decenas[1]5__55_carry__1_n_4\,
      I2 => \string_cent_decenas[1]5__79_carry_n_4\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__124_carry__3_i_9_n_0\,
      O => \string_cent_decenas[1]5__124_carry__3_i_1_n_0\
    );
\string_cent_decenas[1]5__124_carry__3_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I1 => \string_cent_decenas[1]5__55_carry__1_n_4\,
      I2 => \string_cent_decenas[1]5__79_carry_n_4\,
      O => \string_cent_decenas[1]5__124_carry__3_i_10_n_0\
    );
\string_cent_decenas[1]5__124_carry__3_i_11\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__55_carry__1_n_0\,
      CO(3 downto 1) => \NLW_string_cent_decenas[1]5__124_carry__3_i_11_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_string_cent_decenas[1]5__124_carry__3_i_11_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
\string_cent_decenas[1]5__124_carry__3_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \string_cent_decenas[1]5__79_carry__0_n_6\,
      I1 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I2 => \string_cent_decenas[1]5__100_carry_n_6\,
      O => \string_cent_decenas[1]5__124_carry__3_i_12_n_0\
    );
\string_cent_decenas[1]5__124_carry__3_i_13\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I1 => \string_cent_decenas[1]5__55_carry__1_n_5\,
      I2 => \string_cent_decenas[1]5__79_carry_n_5\,
      O => \string_cent_decenas[1]5__124_carry__3_i_13_n_0\
    );
\string_cent_decenas[1]5__124_carry__3_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I1 => \string_cent_decenas[1]5__55_carry__1_n_6\,
      I2 => \string_cent_decenas[1]5__79_carry_n_6\,
      O => \string_cent_decenas[1]5__124_carry__3_i_14_n_0\
    );
\string_cent_decenas[1]5__124_carry__3_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"54D580FE80FE54D5"
    )
        port map (
      I0 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I1 => \string_cent_decenas[1]5__55_carry__1_n_5\,
      I2 => \string_cent_decenas[1]5__79_carry_n_5\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__55_carry__1_n_4\,
      I5 => \string_cent_decenas[1]5__79_carry_n_4\,
      O => \string_cent_decenas[1]5__124_carry__3_i_2_n_0\
    );
\string_cent_decenas[1]5__124_carry__3_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"54D580FE80FE54D5"
    )
        port map (
      I0 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I1 => \string_cent_decenas[1]5__55_carry__1_n_6\,
      I2 => \string_cent_decenas[1]5__79_carry_n_6\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__55_carry__1_n_5\,
      I5 => \string_cent_decenas[1]5__79_carry_n_5\,
      O => \string_cent_decenas[1]5__124_carry__3_i_3_n_0\
    );
\string_cent_decenas[1]5__124_carry__3_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"54D580FE80FE54D5"
    )
        port map (
      I0 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      I1 => \string_cent_decenas[1]5__55_carry__1_n_7\,
      I2 => \string_cent_decenas[1]5__124_carry__2_i_9_n_0\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__55_carry__1_n_6\,
      I5 => \string_cent_decenas[1]5__79_carry_n_6\,
      O => \string_cent_decenas[1]5__124_carry__3_i_4_n_0\
    );
\string_cent_decenas[1]5__124_carry__3_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"188E8EE7E7717118"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__3_i_10_n_0\,
      I1 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I2 => \string_cent_decenas[1]5__100_carry_n_7\,
      I3 => \string_cent_decenas[1]5__79_carry__0_n_7\,
      I4 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I5 => \string_cent_decenas[1]5__124_carry__3_i_12_n_0\,
      O => \string_cent_decenas[1]5__124_carry__3_i_5_n_0\
    );
\string_cent_decenas[1]5__124_carry__3_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__3_i_2_n_0\,
      I1 => \string_cent_decenas[1]5__124_carry__3_i_10_n_0\,
      I2 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I3 => \string_cent_decenas[1]5__79_carry__0_n_7\,
      I4 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I5 => \string_cent_decenas[1]5__100_carry_n_7\,
      O => \string_cent_decenas[1]5__124_carry__3_i_6_n_0\
    );
\string_cent_decenas[1]5__124_carry__3_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__3_i_3_n_0\,
      I1 => \string_cent_decenas[1]5__124_carry__3_i_13_n_0\,
      I2 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I3 => \string_cent_decenas[1]5__79_carry_n_4\,
      I4 => \string_cent_decenas[1]5__55_carry__1_n_4\,
      I5 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__124_carry__3_i_7_n_0\
    );
\string_cent_decenas[1]5__124_carry__3_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__3_i_4_n_0\,
      I1 => \string_cent_decenas[1]5__124_carry__3_i_14_n_0\,
      I2 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I3 => \string_cent_decenas[1]5__79_carry_n_5\,
      I4 => \string_cent_decenas[1]5__55_carry__1_n_5\,
      I5 => \string_cent_decenas[1]5__100_carry_i_8_n_3\,
      O => \string_cent_decenas[1]5__124_carry__3_i_8_n_0\
    );
\string_cent_decenas[1]5__124_carry__3_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \string_cent_decenas[1]5__79_carry__0_n_7\,
      I1 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I2 => \string_cent_decenas[1]5__100_carry_n_7\,
      O => \string_cent_decenas[1]5__124_carry__3_i_9_n_0\
    );
\string_cent_decenas[1]5__124_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__124_carry__3_n_0\,
      CO(3) => \string_cent_decenas[1]5__124_carry__4_n_0\,
      CO(2) => \string_cent_decenas[1]5__124_carry__4_n_1\,
      CO(1) => \string_cent_decenas[1]5__124_carry__4_n_2\,
      CO(0) => \string_cent_decenas[1]5__124_carry__4_n_3\,
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__124_carry__4_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__124_carry__4_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__124_carry__4_i_3_n_0\,
      DI(0) => \string_cent_decenas[1]5__124_carry__4_i_4_n_0\,
      O(3) => \string_cent_decenas[1]5__124_carry__4_n_4\,
      O(2 downto 0) => \NLW_string_cent_decenas[1]5__124_carry__4_O_UNCONNECTED\(2 downto 0),
      S(3) => \string_cent_decenas[1]5__124_carry__4_i_5_n_0\,
      S(2) => \string_cent_decenas[1]5__124_carry__4_i_6_n_0\,
      S(1) => \string_cent_decenas[1]5__124_carry__4_i_7_n_0\,
      S(0) => \string_cent_decenas[1]5__124_carry__4_i_8_n_0\
    );
\string_cent_decenas[1]5__124_carry__4_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"54D580FE80FE54D5"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I1 => \string_cent_decenas[1]5__100_carry_n_4\,
      I2 => \string_cent_decenas[1]5__79_carry__0_n_4\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__100_carry__0_n_7\,
      I5 => \string_cent_decenas[1]5__79_carry__1_n_7\,
      O => \string_cent_decenas[1]5__124_carry__4_i_1_n_0\
    );
\string_cent_decenas[1]5__124_carry__4_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I1 => \string_cent_decenas[1]5__100_carry_n_4\,
      I2 => \string_cent_decenas[1]5__79_carry__0_n_4\,
      O => \string_cent_decenas[1]5__124_carry__4_i_10_n_0\
    );
\string_cent_decenas[1]5__124_carry__4_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \string_cent_decenas[1]5__79_carry__0_n_4\,
      I1 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I2 => \string_cent_decenas[1]5__100_carry_n_4\,
      O => \string_cent_decenas[1]5__124_carry__4_i_11_n_0\
    );
\string_cent_decenas[1]5__124_carry__4_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I1 => \string_cent_decenas[1]5__100_carry_n_6\,
      I2 => \string_cent_decenas[1]5__79_carry__0_n_6\,
      O => \string_cent_decenas[1]5__124_carry__4_i_12_n_0\
    );
\string_cent_decenas[1]5__124_carry__4_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7DD71D471D471441"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I1 => \string_cent_decenas[1]5__100_carry_n_4\,
      I2 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I3 => \string_cent_decenas[1]5__79_carry__0_n_4\,
      I4 => \string_cent_decenas[1]5__100_carry_n_5\,
      I5 => \string_cent_decenas[1]5__79_carry__0_n_5\,
      O => \string_cent_decenas[1]5__124_carry__4_i_2_n_0\
    );
\string_cent_decenas[1]5__124_carry__4_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"54D580FE80FE54D5"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I1 => \string_cent_decenas[1]5__100_carry_n_6\,
      I2 => \string_cent_decenas[1]5__79_carry__0_n_6\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__100_carry_n_5\,
      I5 => \string_cent_decenas[1]5__79_carry__0_n_5\,
      O => \string_cent_decenas[1]5__124_carry__4_i_3_n_0\
    );
\string_cent_decenas[1]5__124_carry__4_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"54D580FE80FE54D5"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I1 => \string_cent_decenas[1]5__100_carry_n_7\,
      I2 => \string_cent_decenas[1]5__79_carry__0_n_7\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__100_carry_n_6\,
      I5 => \string_cent_decenas[1]5__79_carry__0_n_6\,
      O => \string_cent_decenas[1]5__124_carry__4_i_4_n_0\
    );
\string_cent_decenas[1]5__124_carry__4_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9696699669966969"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__4_i_1_n_0\,
      I1 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5__124_carry__4_i_9_n_0\,
      I3 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I4 => \string_cent_decenas[1]5__100_carry__0_n_7\,
      I5 => \string_cent_decenas[1]5__79_carry__1_n_7\,
      O => \string_cent_decenas[1]5__124_carry__4_i_5_n_0\
    );
\string_cent_decenas[1]5__124_carry__4_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__4_i_2_n_0\,
      I1 => \string_cent_decenas[1]5__124_carry__4_i_10_n_0\,
      I2 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I3 => \string_cent_decenas[1]5__79_carry__1_n_7\,
      I4 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I5 => \string_cent_decenas[1]5__100_carry__0_n_7\,
      O => \string_cent_decenas[1]5__124_carry__4_i_6_n_0\
    );
\string_cent_decenas[1]5__124_carry__4_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9696699669966969"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__4_i_3_n_0\,
      I1 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5__124_carry__4_i_11_n_0\,
      I3 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I4 => \string_cent_decenas[1]5__100_carry_n_5\,
      I5 => \string_cent_decenas[1]5__79_carry__0_n_5\,
      O => \string_cent_decenas[1]5__124_carry__4_i_7_n_0\
    );
\string_cent_decenas[1]5__124_carry__4_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__4_i_4_n_0\,
      I1 => \string_cent_decenas[1]5__124_carry__4_i_12_n_0\,
      I2 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I3 => \string_cent_decenas[1]5__79_carry__0_n_5\,
      I4 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I5 => \string_cent_decenas[1]5__100_carry_n_5\,
      O => \string_cent_decenas[1]5__124_carry__4_i_8_n_0\
    );
\string_cent_decenas[1]5__124_carry__4_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \string_cent_decenas[1]5__79_carry__1_n_6\,
      I1 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I2 => \string_cent_decenas[1]5__100_carry__0_n_6\,
      O => \string_cent_decenas[1]5__124_carry__4_i_9_n_0\
    );
\string_cent_decenas[1]5__124_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__124_carry__4_n_0\,
      CO(3 downto 2) => \NLW_string_cent_decenas[1]5__124_carry__5_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \string_cent_decenas[1]5__124_carry__5_n_2\,
      CO(0) => \string_cent_decenas[1]5__124_carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \string_cent_decenas[1]5__124_carry__5_i_1_n_0\,
      DI(0) => \string_cent_decenas[1]5__124_carry__5_i_2_n_0\,
      O(3) => \NLW_string_cent_decenas[1]5__124_carry__5_O_UNCONNECTED\(3),
      O(2) => \string_cent_decenas[1]5__124_carry__5_n_5\,
      O(1) => \string_cent_decenas[1]5__124_carry__5_n_6\,
      O(0) => \string_cent_decenas[1]5__124_carry__5_n_7\,
      S(3) => '0',
      S(2) => \string_cent_decenas[1]5__124_carry__5_i_3_n_0\,
      S(1) => \string_cent_decenas[1]5__124_carry__5_i_4_n_0\,
      S(0) => \string_cent_decenas[1]5__124_carry__5_i_5_n_0\
    );
\string_cent_decenas[1]5__124_carry__5_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"80FE54D554D580FE"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I1 => \string_cent_decenas[1]5__100_carry__0_n_6\,
      I2 => \string_cent_decenas[1]5__79_carry__1_n_6\,
      I3 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__100_carry__0_n_5\,
      I5 => \string_cent_decenas[1]5__79_carry__1_n_1\,
      O => \string_cent_decenas[1]5__124_carry__5_i_1_n_0\
    );
\string_cent_decenas[1]5__124_carry__5_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7DD71D471D471441"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I1 => \string_cent_decenas[1]5__100_carry__0_n_6\,
      I2 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I3 => \string_cent_decenas[1]5__79_carry__1_n_6\,
      I4 => \string_cent_decenas[1]5__100_carry__0_n_7\,
      I5 => \string_cent_decenas[1]5__79_carry__1_n_7\,
      O => \string_cent_decenas[1]5__124_carry__5_i_2_n_0\
    );
\string_cent_decenas[1]5__124_carry__5_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEF80107E0801F7F"
    )
        port map (
      I0 => \string_cent_decenas[1]5__100_carry__0_n_5\,
      I1 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I2 => \string_cent_decenas[1]5__100_carry__0_n_4\,
      I3 => \string_cent_decenas[1]5__79_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__100_carry__1_n_7\,
      I5 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      O => \string_cent_decenas[1]5__124_carry__5_i_3_n_0\
    );
\string_cent_decenas[1]5__124_carry__5_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__5_i_1_n_0\,
      I1 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I2 => \string_cent_decenas[1]5__100_carry__0_n_4\,
      I3 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I4 => \string_cent_decenas[1]5__79_carry__1_n_1\,
      I5 => \string_cent_decenas[1]5__100_carry__0_n_5\,
      O => \string_cent_decenas[1]5__124_carry__5_i_4_n_0\
    );
\string_cent_decenas[1]5__124_carry__5_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6996966996696996"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__5_i_2_n_0\,
      I1 => \string_cent_decenas[1]5__124_carry__5_i_6_n_0\,
      I2 => \string_cent_decenas[1]5__27_carry__1_n_1\,
      I3 => \string_cent_decenas[1]5__79_carry__1_n_1\,
      I4 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I5 => \string_cent_decenas[1]5__100_carry__0_n_5\,
      O => \string_cent_decenas[1]5__124_carry__5_i_5_n_0\
    );
\string_cent_decenas[1]5__124_carry__5_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__3_i_11_n_3\,
      I1 => \string_cent_decenas[1]5__100_carry__0_n_6\,
      I2 => \string_cent_decenas[1]5__79_carry__1_n_6\,
      O => \string_cent_decenas[1]5__124_carry__5_i_6_n_0\
    );
\string_cent_decenas[1]5__124_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry_n_7\,
      I1 => \string_cent_decenas[1]5_carry__1_n_7\,
      O => \string_cent_decenas[1]5__124_carry_i_1_n_0\
    );
\string_cent_decenas[1]5__124_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__0_n_4\,
      I1 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      O => \string_cent_decenas[1]5__124_carry_i_2_n_0\
    );
\string_cent_decenas[1]5__124_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__0_n_5\,
      I1 => cent(1),
      O => \string_cent_decenas[1]5__124_carry_i_3_n_0\
    );
\string_cent_decenas[1]5__124_carry_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__0_n_6\,
      I1 => cent(0),
      O => \string_cent_decenas[1]5__124_carry_i_4_n_0\
    );
\string_cent_decenas[1]5__124_carry_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__1_n_7\,
      I1 => \string_cent_decenas[1]5__27_carry_n_7\,
      I2 => \string_cent_decenas[1]5_carry__1_n_6\,
      I3 => \string_cent_decenas[1]5__27_carry_n_6\,
      O => \string_cent_decenas[1]5__124_carry_i_5_n_0\
    );
\string_cent_decenas[1]5__124_carry_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4BB4"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I1 => \string_cent_decenas[1]5_carry__0_n_4\,
      I2 => \string_cent_decenas[1]5_carry__1_n_7\,
      I3 => \string_cent_decenas[1]5__27_carry_n_7\,
      O => \string_cent_decenas[1]5__124_carry_i_6_n_0\
    );
\string_cent_decenas[1]5__124_carry_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7887"
    )
        port map (
      I0 => cent(1),
      I1 => \string_cent_decenas[1]5_carry__0_n_5\,
      I2 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I3 => \string_cent_decenas[1]5_carry__0_n_4\,
      O => \string_cent_decenas[1]5__124_carry_i_7_n_0\
    );
\string_cent_decenas[1]5__124_carry_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => cent(0),
      I1 => \string_cent_decenas[1]5_carry__0_n_6\,
      I2 => cent(1),
      I3 => \string_cent_decenas[1]5_carry__0_n_5\,
      O => \string_cent_decenas[1]5__124_carry_i_8_n_0\
    );
\string_cent_decenas[1]5__180_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3 downto 2) => \NLW_string_cent_decenas[1]5__180_carry_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \string_cent_decenas[1]5__180_carry_n_2\,
      CO(0) => \string_cent_decenas[1]5__180_carry_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \string_cent_decenas[1]5__124_carry__5_n_6\,
      DI(0) => '0',
      O(3) => \NLW_string_cent_decenas[1]5__180_carry_O_UNCONNECTED\(3),
      O(2) => \string_cent_decenas[1]5__180_carry_n_5\,
      O(1) => \string_cent_decenas[1]5__180_carry_n_6\,
      O(0) => \string_cent_decenas[1]5__180_carry_n_7\,
      S(3) => '0',
      S(2) => \string_cent_decenas[1]5__180_carry_i_1_n_0\,
      S(1) => \string_cent_decenas[1]5__180_carry_i_2_n_0\,
      S(0) => \string_cent_decenas[1]5__124_carry__5_n_7\
    );
\string_cent_decenas[1]5__180_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__5_n_7\,
      I1 => \string_cent_decenas[1]5__124_carry__5_n_5\,
      O => \string_cent_decenas[1]5__180_carry_i_1_n_0\
    );
\string_cent_decenas[1]5__180_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \string_cent_decenas[1]5__124_carry__5_n_6\,
      I1 => \string_cent_decenas[1]5__124_carry__4_n_4\,
      O => \string_cent_decenas[1]5__180_carry_i_2_n_0\
    );
\string_cent_decenas[1]5__186_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5__186_carry_n_0\,
      CO(2) => \string_cent_decenas[1]5__186_carry_n_1\,
      CO(1) => \string_cent_decenas[1]5__186_carry_n_2\,
      CO(0) => \string_cent_decenas[1]5__186_carry_n_3\,
      CYINIT => '1',
      DI(3) => cent(3),
      DI(2) => \string_cent_decenas[1]5__186_carry_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__186_carry_i_3_n_0\,
      DI(0) => cent(0),
      O(3) => \string_cent_decenas[1]5__186_carry_n_4\,
      O(2) => \string_cent_decenas[1]5__186_carry_n_5\,
      O(1) => \string_cent_decenas[1]5__186_carry_n_6\,
      O(0) => \^o\(0),
      S(3) => \string_cent_decenas[1]5__186_carry_i_4_n_0\,
      S(2) => \string_cent_decenas[1]5__186_carry_i_5_n_0\,
      S(1) => \string_cent_decenas[1]5__186_carry_i_6_n_0\,
      S(0) => \string_cent_decenas[1]5__186_carry_i_7_n_0\
    );
\string_cent_decenas[1]5__186_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__186_carry_n_0\,
      CO(3 downto 0) => \NLW_string_cent_decenas[1]5__186_carry__0_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_string_cent_decenas[1]5__186_carry__0_O_UNCONNECTED\(3 downto 1),
      O(0) => \string_cent_decenas[1]5__186_carry__0_n_7\,
      S(3 downto 1) => B"000",
      S(0) => \string_cent_decenas[1]5__186_carry__0_i_1_n_0\
    );
\string_cent_decenas[1]5__186_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \string_cent_decenas[1]5__180_carry_n_5\,
      I1 => cent(4),
      O => \string_cent_decenas[1]5__186_carry__0_i_1_n_0\
    );
\string_cent_decenas[1]5__186_carry_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCC3"
    )
        port map (
      I0 => \^o23\(3),
      I1 => \euros1__339_carry_n_4\,
      I2 => \string_cent_decenas[1]5__186_carry_i_8_n_0\,
      I3 => \euros1__339_carry_n_5\,
      I4 => \^string_cent_decenas[1]5_carry_i_10_0\,
      O => cent(3)
    );
\string_cent_decenas[1]5__186_carry_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      O => \string_cent_decenas[1]5__186_carry_i_2_n_0\
    );
\string_cent_decenas[1]5__186_carry_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^o23\(1),
      I1 => \^string_cent_decenas[1]5_carry_i_10_0\,
      I2 => p_1_in(1),
      O => \string_cent_decenas[1]5__186_carry_i_3_n_0\
    );
\string_cent_decenas[1]5__186_carry_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(3),
      I1 => \string_cent_decenas[1]5__180_carry_n_6\,
      O => \string_cent_decenas[1]5__186_carry_i_4_n_0\
    );
\string_cent_decenas[1]5__186_carry_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I1 => \string_cent_decenas[1]5__180_carry_n_7\,
      O => \string_cent_decenas[1]5__186_carry_i_5_n_0\
    );
\string_cent_decenas[1]5__186_carry_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(1),
      I1 => \string_cent_decenas[1]5__124_carry__4_n_4\,
      O => \string_cent_decenas[1]5__186_carry_i_6_n_0\
    );
\string_cent_decenas[1]5__186_carry_i_7\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(0),
      O => \string_cent_decenas[1]5__186_carry_i_7_n_0\
    );
\string_cent_decenas[1]5__186_carry_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001555555555555"
    )
        port map (
      I0 => \euros1__339_carry__0_n_4\,
      I1 => \euros1__339_carry_n_5\,
      I2 => \euros1__339_carry_n_4\,
      I3 => \euros1__339_carry__0_n_7\,
      I4 => \euros1__339_carry__0_n_6\,
      I5 => \euros1__339_carry__0_n_5\,
      O => \string_cent_decenas[1]5__186_carry_i_8_n_0\
    );
\string_cent_decenas[1]5__27_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5__27_carry_n_0\,
      CO(2) => \string_cent_decenas[1]5__27_carry_n_1\,
      CO(1) => \string_cent_decenas[1]5__27_carry_n_2\,
      CO(0) => \string_cent_decenas[1]5__27_carry_n_3\,
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__27_carry_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__27_carry_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__27_carry_i_3_n_0\,
      DI(0) => '0',
      O(3) => \string_cent_decenas[1]5__27_carry_n_4\,
      O(2) => \string_cent_decenas[1]5__27_carry_n_5\,
      O(1) => \string_cent_decenas[1]5__27_carry_n_6\,
      O(0) => \string_cent_decenas[1]5__27_carry_n_7\,
      S(3) => \string_cent_decenas[1]5__27_carry_i_4_n_0\,
      S(2) => \string_cent_decenas[1]5__27_carry_i_5_n_0\,
      S(1) => \string_cent_decenas[1]5__27_carry_i_6_n_0\,
      S(0) => \string_cent_decenas[1]5__27_carry_i_7_n_0\
    );
\string_cent_decenas[1]5__27_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__27_carry_n_0\,
      CO(3) => \string_cent_decenas[1]5__27_carry__0_n_0\,
      CO(2) => \string_cent_decenas[1]5__27_carry__0_n_1\,
      CO(1) => \string_cent_decenas[1]5__27_carry__0_n_2\,
      CO(0) => \string_cent_decenas[1]5__27_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__27_carry__0_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__27_carry__0_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__27_carry__0_i_3_n_0\,
      DI(0) => \string_cent_decenas[1]5__27_carry__0_i_4_n_0\,
      O(3) => \string_cent_decenas[1]5__27_carry__0_n_4\,
      O(2) => \string_cent_decenas[1]5__27_carry__0_n_5\,
      O(1) => \string_cent_decenas[1]5__27_carry__0_n_6\,
      O(0) => \string_cent_decenas[1]5__27_carry__0_n_7\,
      S(3) => \string_cent_decenas[1]5__27_carry__0_i_5_n_0\,
      S(2) => \string_cent_decenas[1]5__27_carry__0_i_6_n_0\,
      S(1) => \string_cent_decenas[1]5__27_carry__0_i_7_n_0\,
      S(0) => \string_cent_decenas[1]5__27_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5__27_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => cent(4),
      I1 => cent(6),
      O => \string_cent_decenas[1]5__27_carry__0_i_1_n_0\
    );
\string_cent_decenas[1]5__27_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => cent(3),
      I1 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      O => \string_cent_decenas[1]5__27_carry__0_i_2_n_0\
    );
\string_cent_decenas[1]5__27_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(4),
      I1 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      O => \string_cent_decenas[1]5__27_carry__0_i_3_n_0\
    );
\string_cent_decenas[1]5__27_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(4),
      I1 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      O => \string_cent_decenas[1]5__27_carry__0_i_4_n_0\
    );
\string_cent_decenas[1]5__27_carry__0_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B4"
    )
        port map (
      I0 => cent(6),
      I1 => cent(4),
      I2 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      O => \string_cent_decenas[1]5__27_carry__0_i_5_n_0\
    );
\string_cent_decenas[1]5__27_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7887"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I1 => cent(3),
      I2 => cent(6),
      I3 => cent(4),
      O => \string_cent_decenas[1]5__27_carry__0_i_6_n_0\
    );
\string_cent_decenas[1]5__27_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1EE1"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I1 => cent(4),
      I2 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I3 => cent(3),
      O => \string_cent_decenas[1]5__27_carry__0_i_7_n_0\
    );
\string_cent_decenas[1]5__27_carry__0_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8E71718E"
    )
        port map (
      I0 => cent(6),
      I1 => cent(1),
      I2 => cent(3),
      I3 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I4 => cent(4),
      O => \string_cent_decenas[1]5__27_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5__27_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__27_carry__0_n_0\,
      CO(3) => \NLW_string_cent_decenas[1]5__27_carry__1_CO_UNCONNECTED\(3),
      CO(2) => \string_cent_decenas[1]5__27_carry__1_n_1\,
      CO(1) => \NLW_string_cent_decenas[1]5__27_carry__1_CO_UNCONNECTED\(1),
      CO(0) => \string_cent_decenas[1]5__27_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"001",
      DI(0) => \string_cent_decenas[1]5__27_carry__1_i_1_n_0\,
      O(3 downto 2) => \NLW_string_cent_decenas[1]5__27_carry__1_O_UNCONNECTED\(3 downto 2),
      O(1) => \string_cent_decenas[1]5__27_carry__1_n_6\,
      O(0) => \string_cent_decenas[1]5__27_carry__1_n_7\,
      S(3 downto 2) => B"01",
      S(1) => \string_cent_decenas[1]5__27_carry__1_i_2_n_0\,
      S(0) => \string_cent_decenas[1]5__27_carry__1_i_3_n_0\
    );
\string_cent_decenas[1]5__27_carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      O => \string_cent_decenas[1]5__27_carry__1_i_1_n_0\
    );
\string_cent_decenas[1]5__27_carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(6),
      O => \string_cent_decenas[1]5__27_carry__1_i_2_n_0\
    );
\string_cent_decenas[1]5__27_carry__1_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I1 => cent(6),
      O => \string_cent_decenas[1]5__27_carry__1_i_3_n_0\
    );
\string_cent_decenas[1]5__27_carry_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8E"
    )
        port map (
      I0 => cent(0),
      I1 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I2 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      O => \string_cent_decenas[1]5__27_carry_i_1_n_0\
    );
\string_cent_decenas[1]5__27_carry_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => cent(0),
      I1 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I2 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      O => \string_cent_decenas[1]5__27_carry_i_2_n_0\
    );
\string_cent_decenas[1]5__27_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => cent(3),
      I1 => cent(0),
      O => \string_cent_decenas[1]5__27_carry_i_3_n_0\
    );
\string_cent_decenas[1]5__27_carry_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry_i_1_n_0\,
      I1 => cent(3),
      I2 => cent(1),
      I3 => cent(6),
      O => \string_cent_decenas[1]5__27_carry_i_4_n_0\
    );
\string_cent_decenas[1]5__27_carry_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69966969"
    )
        port map (
      I0 => cent(0),
      I1 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I2 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I3 => cent(1),
      I4 => cent(4),
      O => \string_cent_decenas[1]5__27_carry_i_5_n_0\
    );
\string_cent_decenas[1]5__27_carry_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => cent(0),
      I1 => cent(3),
      I2 => cent(1),
      I3 => cent(4),
      O => \string_cent_decenas[1]5__27_carry_i_6_n_0\
    );
\string_cent_decenas[1]5__27_carry_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(3),
      I1 => cent(0),
      O => \string_cent_decenas[1]5__27_carry_i_7_n_0\
    );
\string_cent_decenas[1]5__55_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5__55_carry_n_0\,
      CO(2) => \string_cent_decenas[1]5__55_carry_n_1\,
      CO(1) => \string_cent_decenas[1]5__55_carry_n_2\,
      CO(0) => \string_cent_decenas[1]5__55_carry_n_3\,
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__55_carry_i_1_n_0\,
      DI(2) => cent(0),
      DI(1 downto 0) => B"01",
      O(3) => \string_cent_decenas[1]5__55_carry_n_4\,
      O(2) => \string_cent_decenas[1]5__55_carry_n_5\,
      O(1) => \string_cent_decenas[1]5__55_carry_n_6\,
      O(0) => \NLW_string_cent_decenas[1]5__55_carry_O_UNCONNECTED\(0),
      S(3) => \string_cent_decenas[1]5__55_carry_i_2_n_0\,
      S(2) => \string_cent_decenas[1]5__55_carry_i_3_n_0\,
      S(1) => \string_cent_decenas[1]5__55_carry_i_4_n_0\,
      S(0) => \string_cent_decenas[1]5__55_carry_i_5_n_0\
    );
\string_cent_decenas[1]5__55_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__55_carry_n_0\,
      CO(3) => \string_cent_decenas[1]5__55_carry__0_n_0\,
      CO(2) => \string_cent_decenas[1]5__55_carry__0_n_1\,
      CO(1) => \string_cent_decenas[1]5__55_carry__0_n_2\,
      CO(0) => \string_cent_decenas[1]5__55_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__55_carry__0_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5_carry__0_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__55_carry__0_i_2_n_0\,
      DI(0) => \string_cent_decenas[1]5__55_carry__0_i_3_n_0\,
      O(3) => \string_cent_decenas[1]5__55_carry__0_n_4\,
      O(2) => \string_cent_decenas[1]5__55_carry__0_n_5\,
      O(1) => \string_cent_decenas[1]5__55_carry__0_n_6\,
      O(0) => \string_cent_decenas[1]5__55_carry__0_n_7\,
      S(3) => \string_cent_decenas[1]5__55_carry__0_i_4_n_0\,
      S(2) => \string_cent_decenas[1]5__55_carry__0_i_5_n_0\,
      S(1) => \string_cent_decenas[1]5__55_carry__0_i_6_n_0\,
      S(0) => \string_cent_decenas[1]5__55_carry__0_i_7_n_0\
    );
\string_cent_decenas[1]5__55_carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4D"
    )
        port map (
      I0 => cent(6),
      I1 => cent(4),
      I2 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      O => \string_cent_decenas[1]5__55_carry__0_i_1_n_0\
    );
\string_cent_decenas[1]5__55_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => cent(1),
      I1 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I2 => cent(3),
      O => \string_cent_decenas[1]5__55_carry__0_i_2_n_0\
    );
\string_cent_decenas[1]5__55_carry__0_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      O => \string_cent_decenas[1]5__55_carry__0_i_3_n_0\
    );
\string_cent_decenas[1]5__55_carry__0_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4DB2B24D"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I1 => cent(4),
      I2 => cent(6),
      I3 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I4 => cent(3),
      O => \string_cent_decenas[1]5__55_carry__0_i_4_n_0\
    );
\string_cent_decenas[1]5__55_carry__0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => cent(1),
      I1 => cent(3),
      I2 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I3 => cent(4),
      I4 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I5 => cent(6),
      O => \string_cent_decenas[1]5__55_carry__0_i_5_n_0\
    );
\string_cent_decenas[1]5__55_carry__0_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96699696"
    )
        port map (
      I0 => cent(3),
      I1 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I2 => cent(1),
      I3 => cent(4),
      I4 => cent(0),
      O => \string_cent_decenas[1]5__55_carry__0_i_6_n_0\
    );
\string_cent_decenas[1]5__55_carry__0_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => cent(0),
      I1 => cent(4),
      I2 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      O => \string_cent_decenas[1]5__55_carry__0_i_7_n_0\
    );
\string_cent_decenas[1]5__55_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__55_carry__0_n_0\,
      CO(3) => \string_cent_decenas[1]5__55_carry__1_n_0\,
      CO(2) => \string_cent_decenas[1]5__55_carry__1_n_1\,
      CO(1) => \string_cent_decenas[1]5__55_carry__1_n_2\,
      CO(0) => \string_cent_decenas[1]5__55_carry__1_n_3\,
      CYINIT => '0',
      DI(3) => '1',
      DI(2) => \string_cent_decenas[1]5__55_carry__1_i_1_n_0\,
      DI(1) => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      DI(0) => \string_cent_decenas[1]5__55_carry__1_i_2_n_0\,
      O(3) => \string_cent_decenas[1]5__55_carry__1_n_4\,
      O(2) => \string_cent_decenas[1]5__55_carry__1_n_5\,
      O(1) => \string_cent_decenas[1]5__55_carry__1_n_6\,
      O(0) => \string_cent_decenas[1]5__55_carry__1_n_7\,
      S(3) => \string_cent_decenas[1]5__55_carry__1_i_3_n_0\,
      S(2) => \string_cent_decenas[1]5__55_carry__1_i_4_n_0\,
      S(1) => \string_cent_decenas[1]5__55_carry__1_i_5_n_0\,
      S(0) => \string_cent_decenas[1]5__55_carry__1_i_6_n_0\
    );
\string_cent_decenas[1]5__55_carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(6),
      O => \string_cent_decenas[1]5__55_carry__1_i_1_n_0\
    );
\string_cent_decenas[1]5__55_carry__1_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(4),
      I1 => cent(6),
      O => \string_cent_decenas[1]5__55_carry__1_i_2_n_0\
    );
\string_cent_decenas[1]5__55_carry__1_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(6),
      O => \string_cent_decenas[1]5__55_carry__1_i_3_n_0\
    );
\string_cent_decenas[1]5__55_carry__1_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I1 => cent(6),
      O => \string_cent_decenas[1]5__55_carry__1_i_4_n_0\
    );
\string_cent_decenas[1]5__55_carry__1_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1E"
    )
        port map (
      I0 => cent(6),
      I1 => cent(4),
      I2 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      O => \string_cent_decenas[1]5__55_carry__1_i_5_n_0\
    );
\string_cent_decenas[1]5__55_carry__1_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4BB4"
    )
        port map (
      I0 => cent(3),
      I1 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I2 => cent(6),
      I3 => cent(4),
      O => \string_cent_decenas[1]5__55_carry__1_i_6_n_0\
    );
\string_cent_decenas[1]5__55_carry_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^o23\(1),
      I1 => \^string_cent_decenas[1]5_carry_i_10_0\,
      I2 => p_1_in(1),
      O => \string_cent_decenas[1]5__55_carry_i_1_n_0\
    );
\string_cent_decenas[1]5__55_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(1),
      I1 => cent(3),
      O => \string_cent_decenas[1]5__55_carry_i_2_n_0\
    );
\string_cent_decenas[1]5__55_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(0),
      I1 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      O => \string_cent_decenas[1]5__55_carry_i_3_n_0\
    );
\string_cent_decenas[1]5__55_carry_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(1),
      O => \string_cent_decenas[1]5__55_carry_i_4_n_0\
    );
\string_cent_decenas[1]5__55_carry_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEF00000020"
    )
        port map (
      I0 => \^o23\(0),
      I1 => \string_cent_decenas[1]5_carry_i_7_n_0\,
      I2 => \string_cent_decenas[1]5_carry_i_8_n_0\,
      I3 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I4 => \string_cent_decenas[1]5_carry_i_10_n_0\,
      I5 => p_1_in(0),
      O => \string_cent_decenas[1]5__55_carry_i_5_n_0\
    );
\string_cent_decenas[1]5__79_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5__79_carry_n_0\,
      CO(2) => \string_cent_decenas[1]5__79_carry_n_1\,
      CO(1) => \string_cent_decenas[1]5__79_carry_n_2\,
      CO(0) => \string_cent_decenas[1]5__79_carry_n_3\,
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__27_carry_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__79_carry_i_1_n_0\,
      DI(1) => \string_cent_decenas[1]5__79_carry_i_2_n_0\,
      DI(0) => '0',
      O(3) => \string_cent_decenas[1]5__79_carry_n_4\,
      O(2) => \string_cent_decenas[1]5__79_carry_n_5\,
      O(1) => \string_cent_decenas[1]5__79_carry_n_6\,
      O(0) => \NLW_string_cent_decenas[1]5__79_carry_O_UNCONNECTED\(0),
      S(3) => \string_cent_decenas[1]5__79_carry_i_3_n_0\,
      S(2) => \string_cent_decenas[1]5__79_carry_i_4_n_0\,
      S(1) => \string_cent_decenas[1]5__79_carry_i_5_n_0\,
      S(0) => \string_cent_decenas[1]5__79_carry_i_6_n_0\
    );
\string_cent_decenas[1]5__79_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__79_carry_n_0\,
      CO(3) => \string_cent_decenas[1]5__79_carry__0_n_0\,
      CO(2) => \string_cent_decenas[1]5__79_carry__0_n_1\,
      CO(1) => \string_cent_decenas[1]5__79_carry__0_n_2\,
      CO(0) => \string_cent_decenas[1]5__79_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5__79_carry__0_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5__79_carry__0_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5__79_carry__0_i_3_n_0\,
      DI(0) => \string_cent_decenas[1]5__79_carry__0_i_4_n_0\,
      O(3) => \string_cent_decenas[1]5__79_carry__0_n_4\,
      O(2) => \string_cent_decenas[1]5__79_carry__0_n_5\,
      O(1) => \string_cent_decenas[1]5__79_carry__0_n_6\,
      O(0) => \string_cent_decenas[1]5__79_carry__0_n_7\,
      S(3) => \string_cent_decenas[1]5__79_carry__0_i_5_n_0\,
      S(2) => \string_cent_decenas[1]5__79_carry__0_i_6_n_0\,
      S(1) => \string_cent_decenas[1]5__79_carry__0_i_7_n_0\,
      S(0) => \string_cent_decenas[1]5__79_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5__79_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => cent(4),
      I1 => cent(6),
      O => \string_cent_decenas[1]5__79_carry__0_i_1_n_0\
    );
\string_cent_decenas[1]5__79_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => cent(3),
      I1 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      O => \string_cent_decenas[1]5__79_carry__0_i_2_n_0\
    );
\string_cent_decenas[1]5__79_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(4),
      I1 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      O => \string_cent_decenas[1]5__79_carry__0_i_3_n_0\
    );
\string_cent_decenas[1]5__79_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(4),
      I1 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      O => \string_cent_decenas[1]5__79_carry__0_i_4_n_0\
    );
\string_cent_decenas[1]5__79_carry__0_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B4"
    )
        port map (
      I0 => cent(6),
      I1 => cent(4),
      I2 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      O => \string_cent_decenas[1]5__79_carry__0_i_5_n_0\
    );
\string_cent_decenas[1]5__79_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7887"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I1 => cent(3),
      I2 => cent(6),
      I3 => cent(4),
      O => \string_cent_decenas[1]5__79_carry__0_i_6_n_0\
    );
\string_cent_decenas[1]5__79_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1EE1"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I1 => cent(4),
      I2 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I3 => cent(3),
      O => \string_cent_decenas[1]5__79_carry__0_i_7_n_0\
    );
\string_cent_decenas[1]5__79_carry__0_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8E71718E"
    )
        port map (
      I0 => cent(6),
      I1 => cent(1),
      I2 => cent(3),
      I3 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I4 => cent(4),
      O => \string_cent_decenas[1]5__79_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5__79_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5__79_carry__0_n_0\,
      CO(3) => \NLW_string_cent_decenas[1]5__79_carry__1_CO_UNCONNECTED\(3),
      CO(2) => \string_cent_decenas[1]5__79_carry__1_n_1\,
      CO(1) => \NLW_string_cent_decenas[1]5__79_carry__1_CO_UNCONNECTED\(1),
      CO(0) => \string_cent_decenas[1]5__79_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"001",
      DI(0) => \string_cent_decenas[1]5__79_carry__1_i_1_n_0\,
      O(3 downto 2) => \NLW_string_cent_decenas[1]5__79_carry__1_O_UNCONNECTED\(3 downto 2),
      O(1) => \string_cent_decenas[1]5__79_carry__1_n_6\,
      O(0) => \string_cent_decenas[1]5__79_carry__1_n_7\,
      S(3 downto 2) => B"01",
      S(1) => \string_cent_decenas[1]5__79_carry__1_i_2_n_0\,
      S(0) => \string_cent_decenas[1]5__79_carry__1_i_3_n_0\
    );
\string_cent_decenas[1]5__79_carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      O => \string_cent_decenas[1]5__79_carry__1_i_1_n_0\
    );
\string_cent_decenas[1]5__79_carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(6),
      O => \string_cent_decenas[1]5__79_carry__1_i_2_n_0\
    );
\string_cent_decenas[1]5__79_carry__1_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I1 => cent(6),
      O => \string_cent_decenas[1]5__79_carry__1_i_3_n_0\
    );
\string_cent_decenas[1]5__79_carry_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => cent(0),
      I1 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I2 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      O => \string_cent_decenas[1]5__79_carry_i_1_n_0\
    );
\string_cent_decenas[1]5__79_carry_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => cent(3),
      I1 => cent(0),
      O => \string_cent_decenas[1]5__79_carry_i_2_n_0\
    );
\string_cent_decenas[1]5__79_carry_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \string_cent_decenas[1]5__27_carry_i_1_n_0\,
      I1 => cent(3),
      I2 => cent(1),
      I3 => cent(6),
      O => \string_cent_decenas[1]5__79_carry_i_3_n_0\
    );
\string_cent_decenas[1]5__79_carry_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69966969"
    )
        port map (
      I0 => cent(0),
      I1 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I2 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I3 => cent(1),
      I4 => cent(4),
      O => \string_cent_decenas[1]5__79_carry_i_4_n_0\
    );
\string_cent_decenas[1]5__79_carry_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => cent(0),
      I1 => cent(3),
      I2 => cent(1),
      I3 => cent(4),
      O => \string_cent_decenas[1]5__79_carry_i_5_n_0\
    );
\string_cent_decenas[1]5__79_carry_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(3),
      I1 => cent(0),
      O => \string_cent_decenas[1]5__79_carry_i_6_n_0\
    );
\string_cent_decenas[1]5_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5_carry_n_0\,
      CO(2) => \string_cent_decenas[1]5_carry_n_1\,
      CO(1) => \string_cent_decenas[1]5_carry_n_2\,
      CO(0) => \string_cent_decenas[1]5_carry_n_3\,
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5_carry_i_1_n_0\,
      DI(2) => cent(0),
      DI(1 downto 0) => B"01",
      O(3 downto 1) => \NLW_string_cent_decenas[1]5_carry_O_UNCONNECTED\(3 downto 1),
      O(0) => \string_cent_decenas[1]5_carry_n_7\,
      S(3) => \string_cent_decenas[1]5_carry_i_3_n_0\,
      S(2) => \string_cent_decenas[1]5_carry_i_4_n_0\,
      S(1) => \string_cent_decenas[1]5_carry_i_5_n_0\,
      S(0) => \string_cent_decenas[1]5_carry_i_6_n_0\
    );
\string_cent_decenas[1]5_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_carry_n_0\,
      CO(3) => \string_cent_decenas[1]5_carry__0_n_0\,
      CO(2) => \string_cent_decenas[1]5_carry__0_n_1\,
      CO(1) => \string_cent_decenas[1]5_carry__0_n_2\,
      CO(0) => \string_cent_decenas[1]5_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \string_cent_decenas[1]5_carry__0_i_1_n_0\,
      DI(2) => \string_cent_decenas[1]5_carry__0_i_2_n_0\,
      DI(1) => \string_cent_decenas[1]5_carry__0_i_3_n_0\,
      DI(0) => \string_cent_decenas[1]5_carry__0_i_4_n_0\,
      O(3) => \string_cent_decenas[1]5_carry__0_n_4\,
      O(2) => \string_cent_decenas[1]5_carry__0_n_5\,
      O(1) => \string_cent_decenas[1]5_carry__0_n_6\,
      O(0) => \NLW_string_cent_decenas[1]5_carry__0_O_UNCONNECTED\(0),
      S(3) => \string_cent_decenas[1]5_carry__0_i_5_n_0\,
      S(2) => \string_cent_decenas[1]5_carry__0_i_6_n_0\,
      S(1) => \string_cent_decenas[1]5_carry__0_i_7_n_0\,
      S(0) => \string_cent_decenas[1]5_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5_carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4D"
    )
        port map (
      I0 => cent(6),
      I1 => cent(4),
      I2 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      O => \string_cent_decenas[1]5_carry__0_i_1_n_0\
    );
\string_cent_decenas[1]5_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I1 => cent(3),
      I2 => cent(1),
      O => \string_cent_decenas[1]5_carry__0_i_2_n_0\
    );
\string_cent_decenas[1]5_carry__0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => cent(1),
      I1 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I2 => cent(3),
      O => \string_cent_decenas[1]5_carry__0_i_3_n_0\
    );
\string_cent_decenas[1]5_carry__0_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      O => \string_cent_decenas[1]5_carry__0_i_4_n_0\
    );
\string_cent_decenas[1]5_carry__0_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4DB2B24D"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I1 => cent(4),
      I2 => cent(6),
      I3 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I4 => cent(3),
      O => \string_cent_decenas[1]5_carry__0_i_5_n_0\
    );
\string_cent_decenas[1]5_carry__0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => cent(1),
      I1 => cent(3),
      I2 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I3 => cent(4),
      I4 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      I5 => cent(6),
      O => \string_cent_decenas[1]5_carry__0_i_6_n_0\
    );
\string_cent_decenas[1]5_carry__0_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96699696"
    )
        port map (
      I0 => cent(3),
      I1 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I2 => cent(1),
      I3 => cent(4),
      I4 => cent(0),
      O => \string_cent_decenas[1]5_carry__0_i_7_n_0\
    );
\string_cent_decenas[1]5_carry__0_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => cent(0),
      I1 => cent(4),
      I2 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      O => \string_cent_decenas[1]5_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_carry__0_n_0\,
      CO(3) => \string_cent_decenas[1]5_carry__1_n_0\,
      CO(2) => \string_cent_decenas[1]5_carry__1_n_1\,
      CO(1) => \string_cent_decenas[1]5_carry__1_n_2\,
      CO(0) => \string_cent_decenas[1]5_carry__1_n_3\,
      CYINIT => '0',
      DI(3) => '1',
      DI(2) => \string_cent_decenas[1]5_carry__1_i_1_n_0\,
      DI(1) => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      DI(0) => \string_cent_decenas[1]5_carry__1_i_3_n_0\,
      O(3) => \string_cent_decenas[1]5_carry__1_n_4\,
      O(2) => \string_cent_decenas[1]5_carry__1_n_5\,
      O(1) => \string_cent_decenas[1]5_carry__1_n_6\,
      O(0) => \string_cent_decenas[1]5_carry__1_n_7\,
      S(3) => \string_cent_decenas[1]5_carry__1_i_4_n_0\,
      S(2) => \string_cent_decenas[1]5_carry__1_i_5_n_0\,
      S(1) => \string_cent_decenas[1]5_carry__1_i_6_n_0\,
      S(0) => \string_cent_decenas[1]5_carry__1_i_7_n_0\
    );
\string_cent_decenas[1]5_carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(6),
      O => \string_cent_decenas[1]5_carry__1_i_1_n_0\
    );
\string_cent_decenas[1]5_carry__1_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"555555550FF30CF3"
    )
        port map (
      I0 => \^o23\(5),
      I1 => \euros1__339_carry__0_n_4\,
      I2 => \string_cent_decenas[1]5_carry__1_i_8_n_0\,
      I3 => \euros1__339_carry__0_n_6\,
      I4 => \euros1__339_carry__0_n_5\,
      I5 => \^string_cent_decenas[1]5_carry_i_10_0\,
      O => \string_cent_decenas[1]5_carry__1_i_2_n_0\
    );
\string_cent_decenas[1]5_carry__1_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(4),
      I1 => cent(6),
      O => \string_cent_decenas[1]5_carry__1_i_3_n_0\
    );
\string_cent_decenas[1]5_carry__1_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(6),
      O => \string_cent_decenas[1]5_carry__1_i_4_n_0\
    );
\string_cent_decenas[1]5_carry__1_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I1 => cent(6),
      O => \string_cent_decenas[1]5_carry__1_i_5_n_0\
    );
\string_cent_decenas[1]5_carry__1_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1E"
    )
        port map (
      I0 => cent(6),
      I1 => cent(4),
      I2 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      O => \string_cent_decenas[1]5_carry__1_i_6_n_0\
    );
\string_cent_decenas[1]5_carry__1_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4BB4"
    )
        port map (
      I0 => cent(3),
      I1 => \string_cent_decenas[1]5_carry__1_i_2_n_0\,
      I2 => cent(6),
      I3 => cent(4),
      O => \string_cent_decenas[1]5_carry__1_i_7_n_0\
    );
\string_cent_decenas[1]5_carry__1_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \euros1__339_carry_n_5\,
      I1 => \euros1__339_carry_n_4\,
      I2 => \euros1__339_carry__0_n_7\,
      O => \string_cent_decenas[1]5_carry__1_i_8_n_0\
    );
\string_cent_decenas[1]5_carry_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^o23\(1),
      I1 => \^string_cent_decenas[1]5_carry_i_10_0\,
      I2 => p_1_in(1),
      O => \string_cent_decenas[1]5_carry_i_1_n_0\
    );
\string_cent_decenas[1]5_carry_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FE000000"
    )
        port map (
      I0 => \^o23\(2),
      I1 => \^o23\(4),
      I2 => \^o23\(3),
      I3 => \^o23\(6),
      I4 => \^o23\(5),
      O => \string_cent_decenas[1]5_carry_i_10_n_0\
    );
\string_cent_decenas[1]5_carry_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^o23\(1),
      I1 => \^string_cent_decenas[1]5_carry_i_10_0\,
      I2 => p_1_in(1),
      O => cent(1)
    );
\string_cent_decenas[1]5_carry_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"553C"
    )
        port map (
      I0 => \^o23\(2),
      I1 => \euros1__339_carry_n_5\,
      I2 => \string_cent_decenas[1]5__186_carry_i_8_n_0\,
      I3 => \^string_cent_decenas[1]5_carry_i_10_0\,
      O => \string_cent_decenas[1]5_carry_i_12_n_0\
    );
\string_cent_decenas[1]5_carry_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \euros1__1_carry__3_i_9_n_5\,
      I1 => \euros1__1_carry__3_i_9_n_7\,
      I2 => \euros1__1_carry__5_i_9_n_5\,
      I3 => \euros1__1_carry__7_i_1_n_6\,
      O => \string_cent_decenas[1]5_carry_i_13_n_0\
    );
\string_cent_decenas[1]5_carry_i_14\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \euros1__169_carry__3_i_1_n_7\,
      I1 => \euros1__169_carry__3_i_1_n_5\,
      O => \string_cent_decenas[1]5_carry_i_14_n_0\
    );
\string_cent_decenas[1]5_carry_i_15\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^o23\(7),
      I1 => \euros1__169_carry__1_i_1_n_6\,
      O => \string_cent_decenas[1]5_carry_i_15_n_0\
    );
\string_cent_decenas[1]5_carry_i_16\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_4\,
      I1 => \euros1__169_carry__2_i_1_n_6\,
      O => \string_cent_decenas[1]5_carry_i_16_n_0\
    );
\string_cent_decenas[1]5_carry_i_17\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \euros1__169_carry__1_i_1_n_7\,
      I1 => \euros1__169_carry__1_i_1_n_5\,
      O => \string_cent_decenas[1]5_carry_i_17_n_0\
    );
\string_cent_decenas[1]5_carry_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEF00000020"
    )
        port map (
      I0 => \^o23\(0),
      I1 => \string_cent_decenas[1]5_carry_i_7_n_0\,
      I2 => \string_cent_decenas[1]5_carry_i_8_n_0\,
      I3 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I4 => \string_cent_decenas[1]5_carry_i_10_n_0\,
      I5 => p_1_in(0),
      O => cent(0)
    );
\string_cent_decenas[1]5_carry_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => cent(1),
      I1 => cent(3),
      O => \string_cent_decenas[1]5_carry_i_3_n_0\
    );
\string_cent_decenas[1]5_carry_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cent(0),
      I1 => \string_cent_decenas[1]5_carry_i_12_n_0\,
      O => \string_cent_decenas[1]5_carry_i_4_n_0\
    );
\string_cent_decenas[1]5_carry_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cent(1),
      O => \string_cent_decenas[1]5_carry_i_5_n_0\
    );
\string_cent_decenas[1]5_carry_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEF00000020"
    )
        port map (
      I0 => \^o23\(0),
      I1 => \string_cent_decenas[1]5_carry_i_7_n_0\,
      I2 => \string_cent_decenas[1]5_carry_i_8_n_0\,
      I3 => \string_cent_decenas[1]5_carry_i_9_n_0\,
      I4 => \string_cent_decenas[1]5_carry_i_10_n_0\,
      I5 => p_1_in(0),
      O => \string_cent_decenas[1]5_carry_i_6_n_0\
    );
\string_cent_decenas[1]5_carry_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \euros1__1_carry__3_i_9_n_4\,
      I1 => \euros1__1_carry__5_i_9_n_7\,
      I2 => \euros1__1_carry__7_i_1_n_5\,
      I3 => \euros1__1_carry__7_i_1_n_4\,
      I4 => \string_cent_decenas[1]5_carry_i_13_n_0\,
      O => \string_cent_decenas[1]5_carry_i_7_n_0\
    );
\string_cent_decenas[1]5_carry_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000010000"
    )
        port map (
      I0 => \euros1__169_carry__3_i_1_n_4\,
      I1 => \euros1__1_carry__3_i_9_n_6\,
      I2 => \euros1__169_carry__2_i_1_n_7\,
      I3 => \euros1__169_carry__2_i_1_n_5\,
      I4 => \string_cent_decenas[1]5_carry_i_14_n_0\,
      I5 => \string_cent_decenas[1]5_carry_i_15_n_0\,
      O => \string_cent_decenas[1]5_carry_i_8_n_0\
    );
\string_cent_decenas[1]5_carry_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEFFFFFFFFF"
    )
        port map (
      I0 => \euros1__169_carry__2_i_1_n_4\,
      I1 => \euros1__169_carry__3_i_1_n_6\,
      I2 => \string_cent_decenas[1]5_carry_i_16_n_0\,
      I3 => \euros1__1_carry__7_i_1_n_7\,
      I4 => \euros1__1_carry__5_i_9_n_6\,
      I5 => \string_cent_decenas[1]5_carry_i_17_n_0\,
      O => \string_cent_decenas[1]5_carry_i_9_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___0_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___0_carry_n_0\,
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___0_carry_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___0_carry_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___0_carry_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => cent(4 downto 3),
      DI(1) => \i___0_carry_i_2_n_0\,
      DI(0) => '0',
      O(3 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___0_carry_O_UNCONNECTED\(3 downto 0),
      S(3) => \i___0_carry_i_3_n_0\,
      S(2) => \i___0_carry_i_4_n_0\,
      S(1) => \i___0_carry_i_5_n_0\,
      S(0) => \i___0_carry_i_6_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___0_carry_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_0\,
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \i___0_carry__0_i_1_n_0\,
      DI(2) => \i___0_carry__0_i_2_n_0\,
      DI(1) => cent(6),
      DI(0) => \i___0_carry__0_i_4_n_0\,
      O(3) => \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_4\,
      O(2 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__0_O_UNCONNECTED\(2 downto 0),
      S(3) => \i___0_carry__0_i_5_n_0\,
      S(2) => \i___0_carry__0_i_6_n_0\,
      S(1) => \i___0_carry__0_i_7_n_0\,
      S(0) => \i___0_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___0_carry__0_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_0\,
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \i___0_carry__1_i_1_n_0\,
      O(3) => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_4\,
      O(2) => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_7\,
      S(3) => \i___0_carry__1_i_2_n_0\,
      S(2) => \i___0_carry__1_i_3_n_0\,
      S(1) => \i___0_carry__1_i_4_n_0\,
      S(0) => \i___0_carry__1_i_5_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___0_carry__1_n_0\,
      CO(3 downto 2) => \NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__2_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___0_carry__2_n_2\,
      CO(0) => \NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__2_CO_UNCONNECTED\(0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_string_cent_decenas[1]5_inferred__0/i___0_carry__2_O_UNCONNECTED\(3 downto 1),
      O(0) => \string_cent_decenas[1]5_inferred__0/i___0_carry__2_n_7\,
      S(3 downto 1) => B"001",
      S(0) => \i___0_carry__2_i_1_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___106_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___106_carry_n_0\,
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___106_carry_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___106_carry_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___106_carry_n_3\,
      CYINIT => '0',
      DI(3) => \i___106_carry_i_1_n_0\,
      DI(2) => \i___106_carry_i_2_n_0\,
      DI(1) => \i___106_carry_i_3_n_0\,
      DI(0) => \i___106_carry_i_4_n_0\,
      O(3 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry_O_UNCONNECTED\(3 downto 0),
      S(3) => \i___106_carry_i_5_n_0\,
      S(2) => \i___106_carry_i_6_n_0\,
      S(1) => \i___106_carry_i_7_n_0\,
      S(0) => \i___106_carry_i_8_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___106_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___106_carry_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___106_carry__0_n_0\,
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___106_carry__0_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___106_carry__0_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___106_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \i___106_carry__0_i_1_n_0\,
      DI(2) => \i___106_carry__0_i_2_n_0\,
      DI(1) => \i___106_carry__0_i_3_n_0\,
      DI(0) => \i___106_carry__0_i_4_n_0\,
      O(3 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \i___106_carry__0_i_5_n_0\,
      S(2) => \i___106_carry__0_i_6_n_0\,
      S(1) => \i___106_carry__0_i_7_n_0\,
      S(0) => \i___106_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___106_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___106_carry__0_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___106_carry__1_n_0\,
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___106_carry__1_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___106_carry__1_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___106_carry__1_n_3\,
      CYINIT => '0',
      DI(3) => \i___106_carry__1_i_1_n_0\,
      DI(2) => \i___106_carry__1_i_2_n_0\,
      DI(1) => \i___106_carry__1_i_3_n_0\,
      DI(0) => \i___106_carry__1_i_4_n_0\,
      O(3 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__1_O_UNCONNECTED\(3 downto 0),
      S(3) => \i___106_carry__1_i_5_n_0\,
      S(2) => \i___106_carry__1_i_6_n_0\,
      S(1) => \i___106_carry__1_i_7_n_0\,
      S(0) => \i___106_carry__1_i_8_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___106_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___106_carry__1_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___106_carry__2_n_0\,
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___106_carry__2_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___106_carry__2_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___106_carry__2_n_3\,
      CYINIT => '0',
      DI(3) => \i___106_carry__2_i_1_n_0\,
      DI(2) => \i___106_carry__2_i_2_n_0\,
      DI(1) => \i___106_carry__2_i_3_n_0\,
      DI(0) => \i___106_carry__2_i_4_n_0\,
      O(3 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__2_O_UNCONNECTED\(3 downto 0),
      S(3) => \i___106_carry__2_i_5_n_0\,
      S(2) => \i___106_carry__2_i_6_n_0\,
      S(1) => \i___106_carry__2_i_7_n_0\,
      S(0) => \i___106_carry__2_i_8_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___106_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___106_carry__2_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___106_carry__3_n_0\,
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___106_carry__3_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___106_carry__3_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___106_carry__3_n_3\,
      CYINIT => '0',
      DI(3) => \i___106_carry__3_i_1_n_0\,
      DI(2) => \i___106_carry__3_i_2_n_0\,
      DI(1) => \i___106_carry__3_i_3_n_0\,
      DI(0) => \i___106_carry__3_i_4_n_0\,
      O(3 downto 0) => \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__3_O_UNCONNECTED\(3 downto 0),
      S(3) => \i___106_carry__3_i_5_n_0\,
      S(2) => \i___106_carry__3_i_6_n_0\,
      S(1) => \i___106_carry__3_i_7_n_0\,
      S(0) => \i___106_carry__3_i_8_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___106_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___106_carry__3_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_0\,
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \i___106_carry__4_i_1_n_0\,
      DI(1) => \i___106_carry__4_i_2_n_0\,
      DI(0) => \i___106_carry__4_i_3_n_0\,
      O(3) => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_4\,
      O(2) => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_7\,
      S(3) => \i___106_carry__4_i_4_n_0\,
      S(2) => \i___106_carry__4_i_5_n_0\,
      S(1) => \i___106_carry__4_i_6_n_0\,
      S(0) => \i___106_carry__4_i_7_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___106_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___106_carry__4_n_0\,
      CO(3 downto 1) => \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__5_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___106_carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 2) => \NLW_string_cent_decenas[1]5_inferred__0/i___106_carry__5_O_UNCONNECTED\(3 downto 2),
      O(1) => \string_cent_decenas[1]5_inferred__0/i___106_carry__5_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___106_carry__5_n_7\,
      S(3 downto 2) => B"00",
      S(1) => \i___106_carry__5_i_1_n_0\,
      S(0) => \i___106_carry__5_i_2_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___163_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3 downto 2) => \NLW_string_cent_decenas[1]5_inferred__0/i___163_carry_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___163_carry_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___163_carry_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \i___163_carry_i_1_n_0\,
      DI(0) => '0',
      O(3) => \NLW_string_cent_decenas[1]5_inferred__0/i___163_carry_O_UNCONNECTED\(3),
      O(2) => \string_cent_decenas[1]5_inferred__0/i___163_carry_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i___163_carry_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___163_carry_n_7\,
      S(3) => '0',
      S(2) => \i___163_carry_i_2_n_0\,
      S(1) => \i___163_carry_i_3_n_0\,
      S(0) => \i___163_carry_i_4_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___169_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_0\,
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_3\,
      CYINIT => '1',
      DI(3 downto 2) => cent(3 downto 2),
      DI(1) => \i___169_carry_i_2_n_0\,
      DI(0) => cent(0),
      O(3) => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_4\,
      O(2) => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_5\,
      O(1) => \string_cent_decenas[1]5\(1),
      O(0) => \NLW_string_cent_decenas[1]5_inferred__0/i___169_carry_O_UNCONNECTED\(0),
      S(3) => \i___169_carry_i_3_n_0\,
      S(2) => \i___169_carry_i_4_n_0\,
      S(1) => \i___169_carry_i_5_n_0\,
      S(0) => \i___169_carry_i_6_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___169_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___169_carry_n_0\,
      CO(3) => \NLW_string_cent_decenas[1]5_inferred__0/i___169_carry__0_CO_UNCONNECTED\(3),
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => cent(6),
      DI(1) => \i___169_carry__0_i_1_n_0\,
      DI(0) => cent(4),
      O(3) => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_4\,
      O(2) => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___169_carry__0_n_7\,
      S(3) => \i___169_carry__0_i_2_n_0\,
      S(2) => \i___169_carry__0_i_3_n_0\,
      S(1) => \i___169_carry__0_i_4_n_0\,
      S(0) => \i___169_carry__0_i_5_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___27_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_0\,
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_3\,
      CYINIT => '0',
      DI(3) => \i___27_carry_i_1_n_0\,
      DI(2) => cent(0),
      DI(1 downto 0) => B"01",
      O(3) => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_4\,
      O(2) => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_6\,
      O(0) => \NLW_string_cent_decenas[1]5_inferred__0/i___27_carry_O_UNCONNECTED\(0),
      S(3) => \i___27_carry_i_2_n_0\,
      S(2) => \i___27_carry_i_3_n_0\,
      S(1) => \i___27_carry_i_4_n_0\,
      S(0) => \i___27_carry_i_5_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___27_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___27_carry_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_0\,
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \i___27_carry__0_i_1_n_0\,
      DI(2) => \i___27_carry__0_i_2_n_0\,
      DI(1) => \i___27_carry__0_i_3_n_0\,
      DI(0) => \i___27_carry__0_i_4_n_0\,
      O(3) => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_4\,
      O(2) => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_7\,
      S(3) => \i___27_carry__0_i_5_n_0\,
      S(2) => \i___27_carry__0_i_6_n_0\,
      S(1) => \i___27_carry__0_i_7_n_0\,
      S(0) => \i___27_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___27_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___27_carry__0_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_0\,
      CO(2) => \NLW_string_cent_decenas[1]5_inferred__0/i___27_carry__1_CO_UNCONNECTED\(2),
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"01",
      DI(1) => \i___27_carry__1_i_1_n_0\,
      DI(0) => \i___27_carry__1_i_2_n_0\,
      O(3) => \NLW_string_cent_decenas[1]5_inferred__0/i___27_carry__1_O_UNCONNECTED\(3),
      O(2) => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___27_carry__1_n_7\,
      S(3) => '1',
      S(2) => \i___27_carry__1_i_3_n_0\,
      S(1) => \i___27_carry__1_i_4_n_0\,
      S(0) => \i___27_carry__1_i_5_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___56_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_0\,
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_3\,
      CYINIT => '0',
      DI(3) => cent(0),
      DI(2 downto 0) => B"001",
      O(3) => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_4\,
      O(2) => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_6\,
      O(0) => \NLW_string_cent_decenas[1]5_inferred__0/i___56_carry_O_UNCONNECTED\(0),
      S(3) => \i___56_carry_i_1_n_0\,
      S(2) => \i___56_carry_i_2_n_0\,
      S(1) => \i___56_carry_i_3_n_0\,
      S(0) => \i___56_carry_i_4_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___56_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___56_carry_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_0\,
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => cent(4 downto 3),
      DI(1) => \i___56_carry__0_i_1_n_0\,
      DI(0) => \i___56_carry__0_i_2_n_0\,
      O(3) => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_4\,
      O(2) => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_7\,
      S(3) => \i___56_carry__0_i_3_n_0\,
      S(2) => \i___56_carry__0_i_4_n_0\,
      S(1) => \i___56_carry__0_i_5_n_0\,
      S(0) => \i___56_carry__0_i_6_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___56_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___56_carry__0_n_0\,
      CO(3) => \NLW_string_cent_decenas[1]5_inferred__0/i___56_carry__1_CO_UNCONNECTED\(3),
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_1\,
      CO(1) => \NLW_string_cent_decenas[1]5_inferred__0/i___56_carry__1_CO_UNCONNECTED\(1),
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1 downto 0) => cent(6 downto 5),
      O(3 downto 2) => \NLW_string_cent_decenas[1]5_inferred__0/i___56_carry__1_O_UNCONNECTED\(3 downto 2),
      O(1) => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___56_carry__1_n_7\,
      S(3 downto 2) => B"01",
      S(1) => \i___56_carry__1_i_2_n_0\,
      S(0) => \i___56_carry__1_i_3_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___80_carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_0\,
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_3\,
      CYINIT => '0',
      DI(3) => cent(6),
      DI(2) => \i___80_carry_i_1_n_0\,
      DI(1) => cent(4),
      DI(0) => '0',
      O(3) => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_4\,
      O(2) => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_7\,
      S(3) => \i___80_carry_i_2_n_0\,
      S(2) => \i___80_carry_i_3_n_0\,
      S(1) => \i___80_carry_i_4_n_0\,
      S(0) => \i___80_carry_i_5_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___80_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___80_carry_n_0\,
      CO(3) => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_0\,
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_1\,
      CO(1) => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_2\,
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \i___80_carry__0_i_1_n_0\,
      DI(2) => \i___80_carry__0_i_2_n_0\,
      DI(1) => \i___80_carry__0_i_3_n_0\,
      DI(0) => \i___80_carry__0_i_4_n_0\,
      O(3) => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_4\,
      O(2) => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_5\,
      O(1) => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_7\,
      S(3) => \i___80_carry__0_i_5_n_0\,
      S(2) => \i___80_carry__0_i_6_n_0\,
      S(1) => \i___80_carry__0_i_7_n_0\,
      S(0) => \i___80_carry__0_i_8_n_0\
    );
\string_cent_decenas[1]5_inferred__0/i___80_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \string_cent_decenas[1]5_inferred__0/i___80_carry__0_n_0\,
      CO(3) => \NLW_string_cent_decenas[1]5_inferred__0/i___80_carry__1_CO_UNCONNECTED\(3),
      CO(2) => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_1\,
      CO(1) => \NLW_string_cent_decenas[1]5_inferred__0/i___80_carry__1_CO_UNCONNECTED\(1),
      CO(0) => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \i___80_carry__1_i_1_n_0\,
      O(3 downto 2) => \NLW_string_cent_decenas[1]5_inferred__0/i___80_carry__1_O_UNCONNECTED\(3 downto 2),
      O(1) => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_6\,
      O(0) => \string_cent_decenas[1]5_inferred__0/i___80_carry__1_n_7\,
      S(3 downto 2) => B"01",
      S(1) => \i___80_carry__1_i_2_n_0\,
      S(0) => \i___80_carry__1_i_3_n_0\
    );
\tot_OBUF[3]_inst_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \tot_OBUF[3]_inst_i_1_n_0\,
      CO(2) => \tot_OBUF[3]_inst_i_1_n_1\,
      CO(1) => \tot_OBUF[3]_inst_i_1_n_2\,
      CO(0) => \tot_OBUF[3]_inst_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \^o23\(3 downto 0),
      O(3 downto 0) => \^o23\(3 downto 0),
      S(3) => \tot_OBUF[3]_inst_i_2_n_0\,
      S(2) => \tot_OBUF[3]_inst_i_3_n_0\,
      S(1) => \tot_OBUF[3]_inst_i_4_n_0\,
      S(0) => \tot_OBUF[3]_inst_i_5_n_0\
    );
\tot_OBUF[3]_inst_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^o23\(3),
      I1 => v_mon_OBUF(3),
      O => \tot_OBUF[3]_inst_i_2_n_0\
    );
\tot_OBUF[3]_inst_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^o23\(2),
      I1 => v_mon_OBUF(2),
      O => \tot_OBUF[3]_inst_i_3_n_0\
    );
\tot_OBUF[3]_inst_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^o23\(1),
      I1 => v_mon_OBUF(1),
      O => \tot_OBUF[3]_inst_i_4_n_0\
    );
\tot_OBUF[3]_inst_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^o23\(0),
      I1 => v_mon_OBUF(0),
      O => \tot_OBUF[3]_inst_i_5_n_0\
    );
\tot_OBUF[7]_inst_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tot_OBUF[3]_inst_i_1_n_0\,
      CO(3) => \tot_OBUF[7]_inst_i_1_n_0\,
      CO(2) => \tot_OBUF[7]_inst_i_1_n_1\,
      CO(1) => \tot_OBUF[7]_inst_i_1_n_2\,
      CO(0) => \tot_OBUF[7]_inst_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \^o23\(7 downto 4),
      O(3 downto 0) => \^o23\(7 downto 4),
      S(3) => \tot_OBUF[7]_inst_i_2_n_0\,
      S(2) => \tot_OBUF[7]_inst_i_3_n_0\,
      S(1) => \tot_OBUF[7]_inst_i_4_n_0\,
      S(0) => \tot_OBUF[7]_inst_i_5_n_0\
    );
\tot_OBUF[7]_inst_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^o23\(7),
      I1 => v_mon_OBUF(7),
      O => \tot_OBUF[7]_inst_i_2_n_0\
    );
\tot_OBUF[7]_inst_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^o23\(6),
      I1 => v_mon_OBUF(6),
      O => \tot_OBUF[7]_inst_i_3_n_0\
    );
\tot_OBUF[7]_inst_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^o23\(5),
      I1 => v_mon_OBUF(5),
      O => \tot_OBUF[7]_inst_i_4_n_0\
    );
\tot_OBUF[7]_inst_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^o23\(4),
      I1 => v_mon_OBUF(4),
      O => \tot_OBUF[7]_inst_i_5_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gestion_dinero is
  port (
    \disp[5]_OBUF\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    O23 : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \string_cent_decenas[1]5_carry_i_10\ : out STD_LOGIC;
    \disp[3]_OBUF\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \disp[2]_OBUF\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    O : out STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_refresco[5]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_refresco[3]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \disp_refresco[7]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp[3][1]\ : in STD_LOGIC;
    \disp[3][1]_0\ : in STD_LOGIC;
    \disp[1]_OBUF\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_refresco[4]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \disp_refresco[8]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    valor_moneda_IBUF : in STD_LOGIC_VECTOR ( 22 downto 0 );
    v_mon_OBUF : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
end gestion_dinero;

architecture STRUCTURE of gestion_dinero is
begin
inst_int_to_string: entity work.int_to_string
     port map (
      O(0) => O(0),
      O23(7 downto 0) => O23(7 downto 0),
      Q(1 downto 0) => Q(1 downto 0),
      \disp[1]_OBUF\(0) => \disp[1]_OBUF\(0),
      \disp[2]_OBUF\(2 downto 0) => \disp[2]_OBUF\(2 downto 0),
      \disp[3][1]\ => \disp[3][1]\,
      \disp[3][1]_0\ => \disp[3][1]_0\,
      \disp[3]_OBUF\(3 downto 0) => \disp[3]_OBUF\(3 downto 0),
      \disp[5]_OBUF\(0) => \disp[5]_OBUF\(0),
      \disp_refresco[3]\(1 downto 0) => \disp_refresco[3]\(1 downto 0),
      \disp_refresco[4]\(0) => \disp_refresco[4]\(0),
      \disp_refresco[5]\(0) => \disp_refresco[5]\(0),
      \disp_refresco[7]\(0) => \disp_refresco[7]\(0),
      \disp_refresco[8]\(0) => \disp_refresco[8]\(0),
      \string_cent_decenas[1]5_carry_i_10_0\ => \string_cent_decenas[1]5_carry_i_10\,
      v_mon_OBUF(7 downto 0) => v_mon_OBUF(7 downto 0),
      valor_moneda_IBUF(22 downto 0) => valor_moneda_IBUF(22 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity fsm is
  port (
    RESET : in STD_LOGIC;
    clk : in STD_LOGIC;
    valor_moneda : in STD_LOGIC_VECTOR ( 30 downto 0 );
    button_ok : in STD_LOGIC;
    ref_option : in STD_LOGIC_VECTOR ( 3 downto 0 );
    devolver : out STD_LOGIC;
    refresco : out STD_LOGIC;
    \disp[8]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \disp[7]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \disp[6]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \disp[5]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \disp[4]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \disp[3]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \disp[2]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \disp[1]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    st : out STD_LOGIC_VECTOR ( 1 downto 0 );
    st_dinero : out STD_LOGIC_VECTOR ( 1 downto 0 );
    v_mon : out STD_LOGIC_VECTOR ( 7 downto 0 );
    tot : out STD_LOGIC_VECTOR ( 7 downto 0 );
    bdf_ref : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of fsm : entity is true;
  attribute num_refrescos : integer;
  attribute num_refrescos of fsm : entity is 4;
  attribute width_word : integer;
  attribute width_word of fsm : entity is 8;
end fsm;

architecture STRUCTURE of fsm is
  signal \FSM_sequential_current_state[1]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_10_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_5_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_7_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_8_n_0\ : STD_LOGIC;
  signal \FSM_sequential_next_state_reg[1]_i_9_n_0\ : STD_LOGIC;
  signal Inst_gestion_dinero_n_9 : STD_LOGIC;
  signal Inst_gestion_refresco_n_25 : STD_LOGIC;
  signal RESET_IBUF : STD_LOGIC;
  signal button_ok_IBUF : STD_LOGIC;
  signal clk_IBUF : STD_LOGIC;
  signal clk_IBUF_BUFG : STD_LOGIC;
  signal \disp[1]_OBUF\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \disp[2]_OBUF\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \disp[3][1]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \disp[3]_OBUF\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \disp[4]_OBUF\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \disp[5]_OBUF\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \disp[6]_OBUF\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \disp[7]_OBUF\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \disp[8]_OBUF\ : STD_LOGIC_VECTOR ( 4 downto 1 );
  signal \disp_aux[1]_0\ : STD_LOGIC;
  signal \disp_dinero[2]\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \disp_refresco[3]\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \disp_refresco[4]\ : STD_LOGIC_VECTOR ( 6 to 6 );
  signal \disp_refresco[5]\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \disp_refresco[7]\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \disp_refresco[8]\ : STD_LOGIC_VECTOR ( 1 to 1 );
  signal next_state : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal ref_option_IBUF : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal st_OBUF : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal tot_OBUF : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal v_mon_OBUF : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal valor_moneda_IBUF : STD_LOGIC_VECTOR ( 30 downto 8 );
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_current_state_reg[0]\ : label is "s1:01,s2:10,s0:00,s3:11";
  attribute FSM_ENCODED_STATES of \FSM_sequential_current_state_reg[1]\ : label is "s1:01,s2:10,s0:00,s3:11";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \FSM_sequential_next_state_reg[0]\ : label is "LD";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_next_state_reg[0]_i_1\ : label is "soft_lutpair54";
  attribute XILINX_LEGACY_PRIM of \FSM_sequential_next_state_reg[1]\ : label is "LD";
  attribute SOFT_HLUTNM of \FSM_sequential_next_state_reg[1]_i_1\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \FSM_sequential_next_state_reg[1]_i_2\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \disp[3][1]_INST_0_i_4\ : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of \disp[4][2]_INST_0_i_1\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \disp[8][4]_INST_0_i_1\ : label is "soft_lutpair56";
begin
\FSM_sequential_current_state[1]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => RESET_IBUF,
      O => \FSM_sequential_current_state[1]_i_1_n_0\
    );
\FSM_sequential_current_state_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => next_state(0),
      Q => st_OBUF(0)
    );
\FSM_sequential_current_state_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \FSM_sequential_current_state[1]_i_1_n_0\,
      D => next_state(1),
      Q => st_OBUF(1)
    );
\FSM_sequential_next_state_reg[0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \FSM_sequential_next_state_reg[0]_i_1_n_0\,
      G => \FSM_sequential_next_state_reg[1]_i_2_n_0\,
      GE => '1',
      Q => next_state(0)
    );
\FSM_sequential_next_state_reg[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"0B"
    )
        port map (
      I0 => \FSM_sequential_next_state_reg[1]_i_3_n_0\,
      I1 => st_OBUF(1),
      I2 => st_OBUF(0),
      O => \FSM_sequential_next_state_reg[0]_i_1_n_0\
    );
\FSM_sequential_next_state_reg[1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \FSM_sequential_next_state_reg[1]_i_1_n_0\,
      G => \FSM_sequential_next_state_reg[1]_i_2_n_0\,
      GE => '1',
      Q => next_state(1)
    );
\FSM_sequential_next_state_reg[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F8"
    )
        port map (
      I0 => st_OBUF(1),
      I1 => \FSM_sequential_next_state_reg[1]_i_3_n_0\,
      I2 => st_OBUF(0),
      O => \FSM_sequential_next_state_reg[1]_i_1_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => valor_moneda_IBUF(14),
      I1 => valor_moneda_IBUF(13),
      I2 => valor_moneda_IBUF(16),
      I3 => valor_moneda_IBUF(15),
      O => \FSM_sequential_next_state_reg[1]_i_10_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0CFA0C0A"
    )
        port map (
      I0 => \disp_aux[1]_0\,
      I1 => button_ok_IBUF,
      I2 => st_OBUF(1),
      I3 => st_OBUF(0),
      I4 => \FSM_sequential_next_state_reg[1]_i_3_n_0\,
      O => \FSM_sequential_next_state_reg[1]_i_2_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \FSM_sequential_next_state_reg[1]_i_4_n_0\,
      I1 => \FSM_sequential_next_state_reg[1]_i_5_n_0\,
      I2 => v_mon_OBUF(0),
      I3 => valor_moneda_IBUF(29),
      I4 => valor_moneda_IBUF(30),
      I5 => \FSM_sequential_next_state_reg[1]_i_6_n_0\,
      O => \FSM_sequential_next_state_reg[1]_i_3_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \FSM_sequential_next_state_reg[1]_i_7_n_0\,
      I1 => v_mon_OBUF(2),
      I2 => v_mon_OBUF(1),
      I3 => v_mon_OBUF(4),
      I4 => v_mon_OBUF(3),
      I5 => \FSM_sequential_next_state_reg[1]_i_8_n_0\,
      O => \FSM_sequential_next_state_reg[1]_i_4_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => valor_moneda_IBUF(26),
      I1 => valor_moneda_IBUF(25),
      I2 => valor_moneda_IBUF(28),
      I3 => valor_moneda_IBUF(27),
      O => \FSM_sequential_next_state_reg[1]_i_5_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => valor_moneda_IBUF(19),
      I1 => valor_moneda_IBUF(20),
      I2 => valor_moneda_IBUF(17),
      I3 => valor_moneda_IBUF(18),
      I4 => \FSM_sequential_next_state_reg[1]_i_9_n_0\,
      O => \FSM_sequential_next_state_reg[1]_i_6_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => v_mon_OBUF(6),
      I1 => v_mon_OBUF(5),
      I2 => valor_moneda_IBUF(8),
      I3 => v_mon_OBUF(7),
      O => \FSM_sequential_next_state_reg[1]_i_7_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => valor_moneda_IBUF(11),
      I1 => valor_moneda_IBUF(12),
      I2 => valor_moneda_IBUF(9),
      I3 => valor_moneda_IBUF(10),
      I4 => \FSM_sequential_next_state_reg[1]_i_10_n_0\,
      O => \FSM_sequential_next_state_reg[1]_i_8_n_0\
    );
\FSM_sequential_next_state_reg[1]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => valor_moneda_IBUF(22),
      I1 => valor_moneda_IBUF(21),
      I2 => valor_moneda_IBUF(24),
      I3 => valor_moneda_IBUF(23),
      O => \FSM_sequential_next_state_reg[1]_i_9_n_0\
    );
Inst_gestion_dinero: entity work.gestion_dinero
     port map (
      O(0) => \disp_dinero[2]\(0),
      O23(7 downto 0) => tot_OBUF(7 downto 0),
      Q(1 downto 0) => st_OBUF(1 downto 0),
      \disp[1]_OBUF\(0) => \disp[1]_OBUF\(3),
      \disp[2]_OBUF\(2 downto 0) => \disp[2]_OBUF\(3 downto 1),
      \disp[3][1]\ => Inst_gestion_refresco_n_25,
      \disp[3][1]_0\ => \disp[3][1]_INST_0_i_4_n_0\,
      \disp[3]_OBUF\(3 downto 0) => \disp[3]_OBUF\(3 downto 0),
      \disp[5]_OBUF\(0) => \disp[5]_OBUF\(0),
      \disp_refresco[3]\(1) => \disp_refresco[3]\(6),
      \disp_refresco[3]\(0) => \disp_refresco[3]\(0),
      \disp_refresco[4]\(0) => \disp_refresco[4]\(6),
      \disp_refresco[5]\(0) => \disp_refresco[5]\(0),
      \disp_refresco[7]\(0) => \disp_refresco[7]\(3),
      \disp_refresco[8]\(0) => \disp_refresco[8]\(1),
      \string_cent_decenas[1]5_carry_i_10\ => Inst_gestion_dinero_n_9,
      v_mon_OBUF(7 downto 0) => v_mon_OBUF(7 downto 0),
      valor_moneda_IBUF(22 downto 0) => valor_moneda_IBUF(30 downto 8)
    );
Inst_gestion_refresco: entity work.gestion_refresco
     port map (
      E(0) => \disp_aux[1]_0\,
      \FSM_sequential_current_state_reg[1]\ => Inst_gestion_refresco_n_25,
      O(0) => \disp_dinero[2]\(0),
      Q(1 downto 0) => st_OBUF(1 downto 0),
      \disp[1]_OBUF\(3 downto 0) => \disp[1]_OBUF\(3 downto 0),
      \disp[2]_OBUF\(2) => \disp[2]_OBUF\(6),
      \disp[2]_OBUF\(1) => \disp[2]_OBUF\(4),
      \disp[2]_OBUF\(0) => \disp[2]_OBUF\(0),
      \disp[3]_OBUF\(1) => \disp[3]_OBUF\(6),
      \disp[3]_OBUF\(0) => \disp[3]_OBUF\(4),
      \disp[4]_OBUF\(3 downto 2) => \disp[4]_OBUF\(4 downto 3),
      \disp[4]_OBUF\(1 downto 0) => \disp[4]_OBUF\(1 downto 0),
      \disp[5][4]\ => Inst_gestion_dinero_n_9,
      \disp[5]_OBUF\(4) => \disp[5]_OBUF\(6),
      \disp[5]_OBUF\(3 downto 0) => \disp[5]_OBUF\(4 downto 1),
      \disp[6]_OBUF\(3 downto 0) => \disp[6]_OBUF\(3 downto 0),
      \disp[7]_OBUF\(2 downto 0) => \disp[7]_OBUF\(2 downto 0),
      \disp[8]_OBUF\(2 downto 0) => \disp[8]_OBUF\(3 downto 1),
      \disp_aux_reg[6][3]_i_2_0\(0) => \disp_refresco[4]\(6),
      \disp_aux_reg[6][3]_i_2_1\(1) => \disp_refresco[3]\(6),
      \disp_aux_reg[6][3]_i_2_1\(0) => \disp_refresco[3]\(0),
      \disp_aux_reg[6][3]_i_2_2\(0) => \disp_refresco[8]\(1),
      \disp_refresco[5]\(0) => \disp_refresco[5]\(0),
      \disp_refresco[7]\(0) => \disp_refresco[7]\(3),
      ref_option_IBUF(3 downto 0) => ref_option_IBUF(3 downto 0)
    );
RESET_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => RESET,
      O => RESET_IBUF
    );
bdf_ref_OBUF_inst: unisim.vcomponents.OBUFT
     port map (
      I => '0',
      O => bdf_ref,
      T => '1'
    );
button_ok_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => button_ok,
      O => button_ok_IBUF
    );
clk_IBUF_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => clk_IBUF,
      O => clk_IBUF_BUFG
    );
clk_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => clk,
      O => clk_IBUF
    );
devolver_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => devolver
    );
\disp[1][0]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[1]_OBUF\(0),
      O => \disp[1]\(0)
    );
\disp[1][1]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[1]_OBUF\(1),
      O => \disp[1]\(1)
    );
\disp[1][2]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[1]_OBUF\(2),
      O => \disp[1]\(2)
    );
\disp[1][3]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[1]_OBUF\(3),
      O => \disp[1]\(3)
    );
\disp[1][4]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[1]\(4)
    );
\disp[1][5]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[1]\(5)
    );
\disp[1][6]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[1]_OBUF\(0),
      O => \disp[1]\(6)
    );
\disp[1][7]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[1]\(7)
    );
\disp[2][0]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[2]_OBUF\(0),
      O => \disp[2]\(0)
    );
\disp[2][1]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[2]_OBUF\(1),
      O => \disp[2]\(1)
    );
\disp[2][2]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[2]_OBUF\(2),
      O => \disp[2]\(2)
    );
\disp[2][3]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[2]_OBUF\(3),
      O => \disp[2]\(3)
    );
\disp[2][4]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[2]_OBUF\(4),
      O => \disp[2]\(4)
    );
\disp[2][5]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[2]\(5)
    );
\disp[2][6]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[2]_OBUF\(6),
      O => \disp[2]\(6)
    );
\disp[2][7]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[2]\(7)
    );
\disp[3][0]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[3]_OBUF\(0),
      O => \disp[3]\(0)
    );
\disp[3][1]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[3]_OBUF\(1),
      O => \disp[3]\(1)
    );
\disp[3][1]_INST_0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => st_OBUF(0),
      I1 => st_OBUF(1),
      O => \disp[3][1]_INST_0_i_4_n_0\
    );
\disp[3][2]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[3]_OBUF\(2),
      O => \disp[3]\(2)
    );
\disp[3][3]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[3]_OBUF\(3),
      O => \disp[3]\(3)
    );
\disp[3][4]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[3]_OBUF\(4),
      O => \disp[3]\(4)
    );
\disp[3][5]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[3]\(5)
    );
\disp[3][6]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[3]_OBUF\(6),
      O => \disp[3]\(6)
    );
\disp[3][7]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[3]\(7)
    );
\disp[4][0]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[4]_OBUF\(0),
      O => \disp[4]\(0)
    );
\disp[4][1]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[4]_OBUF\(1),
      O => \disp[4]\(1)
    );
\disp[4][2]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[4]_OBUF\(2),
      O => \disp[4]\(2)
    );
\disp[4][2]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => Inst_gestion_dinero_n_9,
      I1 => st_OBUF(1),
      I2 => st_OBUF(0),
      O => \disp[4]_OBUF\(2)
    );
\disp[4][3]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[4]_OBUF\(3),
      O => \disp[4]\(3)
    );
\disp[4][4]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[4]_OBUF\(4),
      O => \disp[4]\(4)
    );
\disp[4][5]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[4]\(5)
    );
\disp[4][6]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[2]_OBUF\(6),
      O => \disp[4]\(6)
    );
\disp[4][7]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[4]\(7)
    );
\disp[5][0]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[5]_OBUF\(0),
      O => \disp[5]\(0)
    );
\disp[5][1]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[5]_OBUF\(1),
      O => \disp[5]\(1)
    );
\disp[5][2]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[5]_OBUF\(2),
      O => \disp[5]\(2)
    );
\disp[5][3]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[5]_OBUF\(3),
      O => \disp[5]\(3)
    );
\disp[5][4]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[5]_OBUF\(4),
      O => \disp[5]\(4)
    );
\disp[5][5]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[5]\(5)
    );
\disp[5][6]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[5]_OBUF\(6),
      O => \disp[5]\(6)
    );
\disp[5][7]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[5]\(7)
    );
\disp[6][0]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[6]_OBUF\(0),
      O => \disp[6]\(0)
    );
\disp[6][1]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[6]_OBUF\(1),
      O => \disp[6]\(1)
    );
\disp[6][2]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[6]_OBUF\(2),
      O => \disp[6]\(2)
    );
\disp[6][3]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[6]_OBUF\(3),
      O => \disp[6]\(3)
    );
\disp[6][4]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[6]\(4)
    );
\disp[6][5]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[6]\(5)
    );
\disp[6][6]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[5]_OBUF\(6),
      O => \disp[6]\(6)
    );
\disp[6][7]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[6]\(7)
    );
\disp[7][0]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[7]_OBUF\(0),
      O => \disp[7]\(0)
    );
\disp[7][1]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[7]_OBUF\(1),
      O => \disp[7]\(1)
    );
\disp[7][2]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[7]_OBUF\(2),
      O => \disp[7]\(2)
    );
\disp[7][3]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[7]_OBUF\(1),
      O => \disp[7]\(3)
    );
\disp[7][4]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[7]\(4)
    );
\disp[7][5]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[7]\(5)
    );
\disp[7][6]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[7]_OBUF\(0),
      O => \disp[7]\(6)
    );
\disp[7][7]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[7]\(7)
    );
\disp[8][0]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[6]_OBUF\(0),
      O => \disp[8]\(0)
    );
\disp[8][1]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[8]_OBUF\(1),
      O => \disp[8]\(1)
    );
\disp[8][2]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[8]_OBUF\(2),
      O => \disp[8]\(2)
    );
\disp[8][3]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[8]_OBUF\(3),
      O => \disp[8]\(3)
    );
\disp[8][4]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[8]_OBUF\(4),
      O => \disp[8]\(4)
    );
\disp[8][4]_INST_0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => st_OBUF(0),
      I1 => st_OBUF(1),
      O => \disp[8]_OBUF\(4)
    );
\disp[8][5]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[8]\(5)
    );
\disp[8][6]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[7]_OBUF\(0),
      O => \disp[8]\(6)
    );
\disp[8][7]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[8]\(7)
    );
\ref_option_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ref_option(0),
      O => ref_option_IBUF(0)
    );
\ref_option_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ref_option(1),
      O => ref_option_IBUF(1)
    );
\ref_option_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ref_option(2),
      O => ref_option_IBUF(2)
    );
\ref_option_IBUF[3]_inst\: unisim.vcomponents.IBUF
     port map (
      I => ref_option(3),
      O => ref_option_IBUF(3)
    );
refresco_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => refresco
    );
\st_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => st_OBUF(0),
      O => st(0)
    );
\st_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => st_OBUF(1),
      O => st(1)
    );
\st_dinero_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => st_dinero(0)
    );
\st_dinero_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => st_dinero(1)
    );
\tot_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_OBUF(0),
      O => tot(0)
    );
\tot_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_OBUF(1),
      O => tot(1)
    );
\tot_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_OBUF(2),
      O => tot(2)
    );
\tot_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_OBUF(3),
      O => tot(3)
    );
\tot_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_OBUF(4),
      O => tot(4)
    );
\tot_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_OBUF(5),
      O => tot(5)
    );
\tot_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_OBUF(6),
      O => tot(6)
    );
\tot_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => tot_OBUF(7),
      O => tot(7)
    );
\v_mon_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => v_mon_OBUF(0),
      O => v_mon(0)
    );
\v_mon_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => v_mon_OBUF(1),
      O => v_mon(1)
    );
\v_mon_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => v_mon_OBUF(2),
      O => v_mon(2)
    );
\v_mon_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => v_mon_OBUF(3),
      O => v_mon(3)
    );
\v_mon_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => v_mon_OBUF(4),
      O => v_mon(4)
    );
\v_mon_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => v_mon_OBUF(5),
      O => v_mon(5)
    );
\v_mon_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => v_mon_OBUF(6),
      O => v_mon(6)
    );
\v_mon_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => v_mon_OBUF(7),
      O => v_mon(7)
    );
\valor_moneda_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(0),
      O => v_mon_OBUF(0)
    );
\valor_moneda_IBUF[10]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(10),
      O => valor_moneda_IBUF(10)
    );
\valor_moneda_IBUF[11]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(11),
      O => valor_moneda_IBUF(11)
    );
\valor_moneda_IBUF[12]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(12),
      O => valor_moneda_IBUF(12)
    );
\valor_moneda_IBUF[13]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(13),
      O => valor_moneda_IBUF(13)
    );
\valor_moneda_IBUF[14]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(14),
      O => valor_moneda_IBUF(14)
    );
\valor_moneda_IBUF[15]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(15),
      O => valor_moneda_IBUF(15)
    );
\valor_moneda_IBUF[16]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(16),
      O => valor_moneda_IBUF(16)
    );
\valor_moneda_IBUF[17]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(17),
      O => valor_moneda_IBUF(17)
    );
\valor_moneda_IBUF[18]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(18),
      O => valor_moneda_IBUF(18)
    );
\valor_moneda_IBUF[19]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(19),
      O => valor_moneda_IBUF(19)
    );
\valor_moneda_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(1),
      O => v_mon_OBUF(1)
    );
\valor_moneda_IBUF[20]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(20),
      O => valor_moneda_IBUF(20)
    );
\valor_moneda_IBUF[21]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(21),
      O => valor_moneda_IBUF(21)
    );
\valor_moneda_IBUF[22]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(22),
      O => valor_moneda_IBUF(22)
    );
\valor_moneda_IBUF[23]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(23),
      O => valor_moneda_IBUF(23)
    );
\valor_moneda_IBUF[24]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(24),
      O => valor_moneda_IBUF(24)
    );
\valor_moneda_IBUF[25]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(25),
      O => valor_moneda_IBUF(25)
    );
\valor_moneda_IBUF[26]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(26),
      O => valor_moneda_IBUF(26)
    );
\valor_moneda_IBUF[27]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(27),
      O => valor_moneda_IBUF(27)
    );
\valor_moneda_IBUF[28]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(28),
      O => valor_moneda_IBUF(28)
    );
\valor_moneda_IBUF[29]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(29),
      O => valor_moneda_IBUF(29)
    );
\valor_moneda_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(2),
      O => v_mon_OBUF(2)
    );
\valor_moneda_IBUF[30]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(30),
      O => valor_moneda_IBUF(30)
    );
\valor_moneda_IBUF[3]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(3),
      O => v_mon_OBUF(3)
    );
\valor_moneda_IBUF[4]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(4),
      O => v_mon_OBUF(4)
    );
\valor_moneda_IBUF[5]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(5),
      O => v_mon_OBUF(5)
    );
\valor_moneda_IBUF[6]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(6),
      O => v_mon_OBUF(6)
    );
\valor_moneda_IBUF[7]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(7),
      O => v_mon_OBUF(7)
    );
\valor_moneda_IBUF[8]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(8),
      O => valor_moneda_IBUF(8)
    );
\valor_moneda_IBUF[9]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(9),
      O => valor_moneda_IBUF(9)
    );
end STRUCTURE;
