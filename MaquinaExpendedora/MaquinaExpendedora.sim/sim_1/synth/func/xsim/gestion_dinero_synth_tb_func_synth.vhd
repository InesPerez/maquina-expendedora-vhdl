-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Thu Feb  4 19:36:27 2021
-- Host        : LAPTOP-7BL7BHFF running 64-bit major release  (build 9200)
-- Command     : write_vhdl -mode funcsim -nolib -force -file
--               C:/Users/Inees/Desktop/Trabajo_SED/MaquinaExpendedora/MaquinaExpendedora.sim/sim_1/synth/func/xsim/gestion_dinero_synth_tb_func_synth.vhd
-- Design      : gestion_dinero
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tcsg324-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity gestion_dinero is
  port (
    RESET : in STD_LOGIC;
    clk : in STD_LOGIC;
    valor_moneda : in STD_LOGIC_VECTOR ( 30 downto 0 );
    button_ok : in STD_LOGIC;
    devolver : out STD_LOGIC;
    refresco : out STD_LOGIC;
    \disp[8]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \disp[7]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \disp[6]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \disp[5]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \disp[4]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \disp[3]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \disp[2]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \disp[1]\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    bdf : out STD_LOGIC;
    st : out STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of gestion_dinero : entity is true;
  attribute width_word : integer;
  attribute width_word of gestion_dinero : entity is 8;
end gestion_dinero;

architecture STRUCTURE of gestion_dinero is
  signal \FSM_sequential_current_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_current_state[1]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_current_state[1]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_current_state[1]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_current_state[1]_i_5_n_0\ : STD_LOGIC;
  signal RESET_IBUF : STD_LOGIC;
  signal bdf_OBUF : STD_LOGIC;
  signal button_ok_IBUF : STD_LOGIC;
  signal clk_IBUF : STD_LOGIC;
  signal clk_IBUF_BUFG : STD_LOGIC;
  signal current_state : STD_LOGIC_VECTOR ( 0 to 0 );
  signal devolver_OBUF : STD_LOGIC;
  signal \disp[1]_OBUF\ : STD_LOGIC_VECTOR ( 2 downto 1 );
  signal \disp[2][0]_INST_0_i_100_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_101_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_102_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_103_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_104_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_105_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_106_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_107_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_108_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_109_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_109_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_109_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_109_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_10_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_110_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_111_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_112_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_113_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_114_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_115_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_116_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_117_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_118_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_119_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_119_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_119_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_119_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_11_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_120_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_121_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_122_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_123_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_124_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_125_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_126_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_127_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_128_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_128_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_128_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_128_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_128_n_4\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_128_n_5\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_128_n_6\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_128_n_7\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_129_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_12_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_130_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_131_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_132_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_133_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_133_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_133_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_133_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_133_n_4\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_133_n_5\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_133_n_6\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_133_n_7\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_134_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_135_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_136_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_137_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_138_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_139_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_13_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_13_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_13_n_5\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_13_n_6\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_13_n_7\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_140_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_141_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_142_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_143_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_144_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_145_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_146_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_147_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_148_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_149_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_149_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_149_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_149_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_149_n_4\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_149_n_5\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_149_n_6\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_149_n_7\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_14_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_14_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_14_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_14_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_14_n_4\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_150_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_151_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_152_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_153_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_154_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_155_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_156_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_157_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_158_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_158_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_158_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_158_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_159_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_15_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_15_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_15_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_15_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_15_n_4\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_15_n_5\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_160_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_161_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_162_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_163_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_164_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_165_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_166_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_167_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_168_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_168_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_168_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_168_n_5\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_168_n_6\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_168_n_7\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_169_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_169_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_169_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_169_n_5\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_169_n_6\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_169_n_7\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_16_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_170_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_170_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_170_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_170_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_171_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_172_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_173_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_174_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_175_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_176_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_177_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_178_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_179_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_179_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_179_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_179_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_179_n_4\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_179_n_5\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_179_n_6\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_17_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_17_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_17_n_5\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_17_n_6\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_17_n_7\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_180_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_180_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_180_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_180_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_180_n_7\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_181_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_182_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_183_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_184_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_185_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_186_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_187_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_188_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_189_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_189_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_189_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_189_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_189_n_4\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_189_n_5\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_189_n_6\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_18_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_190_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_191_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_192_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_193_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_194_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_195_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_196_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_197_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_198_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_199_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_19_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_1_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_1_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_1_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_1_n_4\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_1_n_5\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_1_n_6\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_200_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_201_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_202_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_203_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_204_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_204_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_204_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_204_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_205_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_206_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_207_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_208_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_209_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_20_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_20_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_20_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_20_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_210_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_211_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_212_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_213_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_213_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_213_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_213_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_213_n_4\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_213_n_5\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_213_n_6\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_213_n_7\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_214_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_214_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_214_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_214_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_214_n_4\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_214_n_5\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_214_n_6\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_214_n_7\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_215_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_215_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_215_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_215_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_215_n_4\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_215_n_5\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_215_n_6\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_215_n_7\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_216_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_217_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_218_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_219_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_21_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_220_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_221_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_222_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_223_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_224_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_225_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_226_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_227_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_228_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_229_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_22_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_230_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_231_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_232_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_233_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_234_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_235_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_236_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_237_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_238_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_239_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_23_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_240_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_241_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_242_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_243_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_244_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_245_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_246_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_247_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_248_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_249_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_249_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_249_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_249_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_24_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_250_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_251_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_252_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_253_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_254_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_255_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_256_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_257_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_258_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_258_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_258_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_258_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_258_n_4\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_258_n_5\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_258_n_6\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_258_n_7\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_259_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_25_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_260_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_261_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_262_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_263_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_264_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_265_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_266_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_267_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_267_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_267_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_267_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_267_n_4\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_267_n_5\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_267_n_6\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_267_n_7\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_268_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_269_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_26_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_270_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_271_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_272_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_273_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_274_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_274_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_274_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_274_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_274_n_4\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_274_n_5\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_274_n_6\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_275_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_276_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_277_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_278_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_279_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_279_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_279_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_279_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_27_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_280_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_281_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_282_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_283_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_284_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_285_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_286_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_287_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_288_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_289_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_28_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_290_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_291_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_292_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_293_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_294_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_295_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_295_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_295_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_295_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_295_n_4\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_295_n_5\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_295_n_6\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_295_n_7\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_296_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_297_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_298_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_299_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_29_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_300_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_301_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_302_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_303_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_304_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_305_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_306_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_307_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_308_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_309_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_30_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_310_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_311_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_312_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_313_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_314_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_315_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_315_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_315_n_6\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_315_n_7\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_316_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_316_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_316_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_316_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_316_n_4\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_316_n_5\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_316_n_6\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_316_n_7\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_317_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_318_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_319_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_31_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_320_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_321_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_322_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_322_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_322_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_322_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_322_n_4\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_323_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_324_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_325_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_326_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_326_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_326_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_326_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_327_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_328_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_329_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_32_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_330_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_331_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_332_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_333_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_334_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_33_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_34_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_35_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_36_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_37_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_38_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_38_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_38_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_38_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_39_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_40_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_41_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_42_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_43_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_44_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_45_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_46_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_47_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_47_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_47_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_47_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_47_n_4\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_47_n_5\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_47_n_6\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_47_n_7\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_48_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_49_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_49_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_49_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_49_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_49_n_4\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_49_n_5\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_49_n_6\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_49_n_7\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_50_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_50_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_50_n_6\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_50_n_7\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_51_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_51_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_51_n_6\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_51_n_7\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_52_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_52_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_52_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_52_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_52_n_4\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_52_n_5\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_52_n_6\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_52_n_7\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_53_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_54_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_55_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_56_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_57_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_57_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_57_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_57_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_57_n_4\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_57_n_5\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_57_n_6\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_57_n_7\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_58_n_7\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_59_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_60_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_60_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_60_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_60_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_61_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_62_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_63_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_64_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_65_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_66_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_67_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_68_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_69_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_70_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_71_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_71_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_71_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_71_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_71_n_4\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_71_n_5\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_71_n_6\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_71_n_7\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_72_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_72_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_72_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_72_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_72_n_4\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_72_n_5\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_72_n_6\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_73_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_74_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_75_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_76_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_77_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_78_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_79_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_7_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_80_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_81_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_82_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_83_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_84_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_85_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_86_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_87_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_88_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_89_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_8_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_90_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_91_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_92_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_92_n_1\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_92_n_2\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_92_n_3\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_92_n_4\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_92_n_5\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_92_n_6\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_92_n_7\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_93_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_94_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_95_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_96_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_97_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_98_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_99_n_0\ : STD_LOGIC;
  signal \disp[2][0]_INST_0_i_9_n_0\ : STD_LOGIC;
  signal \disp[2][3]_INST_0_i_2_n_7\ : STD_LOGIC;
  signal \disp[2][3]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \disp[2][3]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \disp[2]_OBUF\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \disp[3][0]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \disp[3][0]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \disp[3][0]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \disp[3][0]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \disp[3][0]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_100_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_101_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_102_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_102_n_1\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_102_n_2\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_102_n_3\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_102_n_4\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_102_n_5\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_102_n_6\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_103_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_104_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_105_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_106_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_107_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_108_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_109_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_10_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_10_n_1\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_10_n_2\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_10_n_3\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_10_n_4\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_10_n_5\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_110_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_111_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_111_n_1\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_111_n_2\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_111_n_3\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_111_n_4\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_111_n_5\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_111_n_6\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_113_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_114_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_115_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_116_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_117_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_118_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_119_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_11_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_120_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_120_n_1\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_120_n_2\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_120_n_3\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_121_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_122_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_123_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_124_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_125_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_126_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_127_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_128_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_129_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_12_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_130_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_131_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_132_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_133_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_134_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_135_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_136_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_137_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_138_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_139_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_13_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_140_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_141_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_142_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_143_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_144_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_145_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_146_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_147_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_148_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_149_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_14_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_150_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_151_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_152_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_153_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_154_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_155_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_156_n_2\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_156_n_7\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_157_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_157_n_1\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_157_n_2\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_157_n_3\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_157_n_4\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_157_n_5\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_157_n_6\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_157_n_7\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_158_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_159_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_159_n_1\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_159_n_2\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_159_n_3\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_159_n_4\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_15_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_160_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_161_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_162_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_163_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_164_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_165_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_166_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_166_n_1\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_166_n_2\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_166_n_3\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_167_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_168_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_169_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_16_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_170_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_171_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_172_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_173_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_174_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_175_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_176_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_177_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_178_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_179_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_17_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_180_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_181_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_18_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_19_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_20_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_25_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_26_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_27_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_28_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_29_n_2\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_29_n_3\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_29_n_5\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_29_n_6\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_29_n_7\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_32_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_32_n_1\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_32_n_2\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_32_n_3\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_32_n_4\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_32_n_5\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_32_n_6\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_32_n_7\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_33_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_34_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_35_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_36_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_37_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_37_n_1\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_37_n_2\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_37_n_3\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_38_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_39_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_40_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_41_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_42_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_43_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_44_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_45_n_3\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_45_n_6\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_45_n_7\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_46_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_46_n_1\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_46_n_2\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_46_n_3\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_47_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_48_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_49_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_50_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_51_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_52_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_53_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_54_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_55_n_1\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_55_n_3\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_55_n_6\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_55_n_7\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_56_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_56_n_2\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_56_n_3\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_56_n_5\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_56_n_6\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_56_n_7\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_57_n_1\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_57_n_3\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_57_n_6\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_57_n_7\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_58_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_59_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_60_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_60_n_1\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_60_n_2\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_60_n_3\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_61_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_62_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_63_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_64_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_65_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_66_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_67_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_68_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_69_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_69_n_1\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_69_n_2\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_69_n_3\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_69_n_4\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_69_n_5\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_69_n_6\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_69_n_7\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_70_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_71_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_72_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_73_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_73_n_1\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_73_n_2\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_73_n_3\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_73_n_4\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_73_n_5\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_73_n_6\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_73_n_7\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_74_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_75_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_76_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_77_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_78_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_79_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_79_n_1\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_79_n_2\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_79_n_3\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_79_n_4\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_79_n_5\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_79_n_6\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_79_n_7\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_7_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_80_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_81_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_82_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_83_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_84_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_84_n_1\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_84_n_2\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_84_n_3\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_85_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_86_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_87_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_88_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_89_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_8_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_90_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_91_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_92_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_93_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_93_n_1\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_93_n_2\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_93_n_3\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_93_n_4\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_93_n_5\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_93_n_6\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_93_n_7\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_94_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_95_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_96_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_97_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_98_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_99_n_0\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_9_n_1\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_9_n_2\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_9_n_3\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_9_n_4\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_9_n_5\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_9_n_6\ : STD_LOGIC;
  signal \disp[3][2]_INST_0_i_9_n_7\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_10_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_11_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_12_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_13_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_14_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_15_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_16_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_7_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_8_n_0\ : STD_LOGIC;
  signal \disp[3][3]_INST_0_i_9_n_0\ : STD_LOGIC;
  signal \disp[3]_OBUF\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \disp[5][0]_INST_0_i_100_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_101_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_102_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_103_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_103_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_103_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_103_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_103_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_103_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_103_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_103_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_104_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_105_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_106_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_107_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_108_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_109_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_10_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_10_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_10_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_10_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_110_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_111_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_112_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_113_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_113_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_113_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_113_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_114_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_115_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_116_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_117_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_118_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_119_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_11_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_11_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_11_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_11_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_11_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_11_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_11_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_11_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_120_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_121_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_122_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_122_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_122_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_122_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_122_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_122_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_122_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_122_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_123_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_124_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_125_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_125_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_125_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_125_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_125_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_125_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_125_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_125_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_126_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_127_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_127_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_127_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_127_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_127_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_127_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_127_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_127_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_128_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_128_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_128_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_128_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_128_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_128_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_128_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_128_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_129_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_12_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_12_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_12_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_12_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_12_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_12_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_12_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_12_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_130_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_131_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_132_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_133_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_134_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_135_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_136_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_137_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_138_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_139_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_13_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_140_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_141_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_142_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_143_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_144_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_145_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_146_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_147_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_148_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_14_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_153_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_154_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_155_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_156_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_157_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_158_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_159_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_15_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_160_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_161_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_162_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_163_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_164_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_169_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_16_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_16_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_16_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_16_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_170_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_171_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_172_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_173_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_174_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_175_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_176_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_177_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_178_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_179_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_17_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_180_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_181_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_182_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_183_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_184_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_185_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_186_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_187_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_188_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_189_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_18_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_190_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_191_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_192_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_193_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_194_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_195_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_196_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_197_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_198_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_199_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_19_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_200_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_201_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_202_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_203_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_204_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_205_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_205_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_205_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_205_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_206_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_207_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_208_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_209_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_20_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_210_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_211_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_212_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_213_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_214_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_214_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_214_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_214_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_215_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_216_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_217_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_218_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_219_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_219_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_219_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_219_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_219_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_219_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_219_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_219_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_21_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_220_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_221_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_222_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_223_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_224_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_225_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_226_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_227_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_228_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_228_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_228_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_228_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_228_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_228_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_228_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_228_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_229_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_22_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_230_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_231_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_232_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_233_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_234_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_235_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_236_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_237_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_237_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_237_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_237_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_237_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_237_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_237_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_237_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_238_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_238_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_238_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_238_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_239_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_239_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_239_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_239_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_239_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_239_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_23_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_240_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_240_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_240_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_240_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_241_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_241_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_241_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_241_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_242_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_243_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_244_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_245_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_246_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_247_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_248_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_249_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_24_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_250_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_250_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_250_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_250_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_250_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_250_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_250_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_250_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_251_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_252_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_253_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_253_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_253_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_253_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_253_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_253_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_253_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_253_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_254_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_255_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_255_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_255_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_255_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_255_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_255_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_255_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_255_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_256_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_257_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_257_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_257_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_257_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_257_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_257_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_257_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_257_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_258_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_259_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_25_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_25_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_25_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_25_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_260_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_261_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_262_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_263_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_264_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_265_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_266_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_267_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_268_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_269_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_26_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_271_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_272_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_273_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_274_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_275_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_276_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_277_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_278_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_279_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_27_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_280_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_281_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_282_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_283_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_284_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_285_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_286_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_287_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_288_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_289_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_28_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_290_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_291_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_292_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_293_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_294_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_294_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_294_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_294_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_295_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_295_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_295_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_295_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_296_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_296_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_296_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_296_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_298_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_298_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_298_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_298_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_299_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_29_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_2_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_300_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_301_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_302_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_303_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_304_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_305_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_306_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_307_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_307_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_307_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_307_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_307_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_308_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_308_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_308_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_308_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_308_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_308_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_308_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_308_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_309_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_30_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_310_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_311_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_312_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_313_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_314_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_315_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_316_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_317_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_317_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_317_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_317_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_317_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_317_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_317_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_317_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_318_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_319_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_31_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_320_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_321_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_322_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_323_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_324_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_325_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_326_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_326_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_326_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_326_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_326_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_326_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_326_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_326_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_327_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_327_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_327_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_327_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_327_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_327_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_327_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_327_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_328_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_329_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_32_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_330_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_331_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_332_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_333_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_334_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_335_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_336_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_338_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_339_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_339_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_339_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_339_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_339_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_339_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_339_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_339_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_33_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_340_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_341_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_342_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_343_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_344_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_345_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_346_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_348_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_349_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_34_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_34_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_34_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_34_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_350_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_350_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_350_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_350_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_351_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_352_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_353_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_354_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_355_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_356_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_357_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_358_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_35_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_35_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_35_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_35_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_35_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_35_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_35_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_360_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_361_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_362_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_362_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_362_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_362_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_362_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_362_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_362_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_363_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_364_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_364_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_364_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_364_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_364_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_364_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_364_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_364_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_365_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_365_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_365_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_365_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_365_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_365_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_365_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_365_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_367_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_368_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_369_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_36_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_370_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_371_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_372_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_373_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_374_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_375_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_376_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_377_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_378_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_379_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_37_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_380_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_381_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_382_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_383_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_384_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_385_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_386_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_387_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_388_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_389_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_38_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_390_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_391_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_392_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_393_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_394_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_395_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_396_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_397_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_39_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_3_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_401_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_402_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_403_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_404_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_405_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_406_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_407_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_408_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_409_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_409_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_409_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_409_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_40_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_40_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_40_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_40_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_40_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_40_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_40_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_40_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_410_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_411_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_412_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_413_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_414_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_415_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_416_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_417_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_418_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_419_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_41_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_420_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_421_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_422_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_422_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_422_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_422_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_422_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_422_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_422_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_422_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_423_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_424_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_425_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_426_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_427_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_428_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_429_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_42_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_430_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_431_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_431_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_431_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_431_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_431_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_431_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_431_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_431_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_432_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_433_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_434_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_435_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_436_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_437_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_438_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_439_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_43_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_440_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_440_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_440_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_440_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_440_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_440_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_440_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_440_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_441_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_441_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_441_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_441_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_441_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_441_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_441_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_441_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_442_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_443_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_444_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_445_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_446_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_447_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_448_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_449_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_44_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_450_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_451_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_452_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_457_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_458_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_459_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_45_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_460_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_461_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_462_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_463_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_464_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_465_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_466_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_467_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_468_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_469_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_46_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_470_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_471_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_472_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_473_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_474_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_475_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_476_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_477_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_477_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_477_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_477_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_477_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_477_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_477_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_477_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_478_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_479_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_47_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_480_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_481_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_482_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_483_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_484_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_485_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_486_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_487_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_488_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_489_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_48_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_490_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_490_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_490_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_490_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_490_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_490_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_490_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_490_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_491_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_492_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_493_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_494_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_495_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_496_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_497_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_498_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_499_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_499_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_499_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_499_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_49_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_49_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_49_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_49_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_49_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_49_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_49_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_49_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_4_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_500_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_501_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_502_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_503_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_504_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_505_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_506_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_507_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_508_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_508_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_508_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_509_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_50_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_510_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_511_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_512_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_513_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_514_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_515_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_516_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_517_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_518_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_519_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_51_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_51_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_51_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_51_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_520_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_521_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_522_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_523_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_524_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_524_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_524_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_524_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_524_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_524_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_524_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_524_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_525_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_525_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_526_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_527_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_527_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_527_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_527_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_527_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_527_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_527_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_527_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_528_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_529_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_529_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_529_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_529_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_529_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_529_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_529_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_529_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_52_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_530_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_531_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_532_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_533_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_534_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_535_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_536_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_537_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_538_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_539_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_53_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_540_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_541_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_542_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_547_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_548_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_549_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_54_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_550_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_551_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_551_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_551_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_551_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_551_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_552_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_553_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_554_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_555_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_556_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_557_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_558_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_559_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_55_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_560_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_561_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_562_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_563_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_564_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_565_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_566_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_567_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_568_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_569_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_56_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_570_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_571_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_572_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_573_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_574_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_575_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_576_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_577_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_578_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_579_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_57_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_580_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_581_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_582_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_583_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_584_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_585_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_586_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_587_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_588_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_58_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_593_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_594_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_595_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_596_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_597_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_598_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_599_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_59_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_5_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_5_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_5_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_600_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_601_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_602_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_602_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_602_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_602_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_603_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_605_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_606_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_607_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_608_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_609_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_60_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_60_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_60_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_60_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_60_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_60_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_60_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_60_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_610_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_611_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_612_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_613_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_614_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_615_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_616_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_617_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_618_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_61_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_61_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_61_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_61_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_61_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_61_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_61_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_61_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_62_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_62_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_62_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_62_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_62_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_62_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_62_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_62_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_63_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_63_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_63_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_63_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_63_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_63_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_63_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_63_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_64_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_65_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_66_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_66_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_66_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_66_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_66_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_66_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_66_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_66_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_67_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_68_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_68_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_68_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_68_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_68_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_68_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_68_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_68_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_69_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_69_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_69_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_69_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_69_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_69_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_69_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_69_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_6_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_6_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_6_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_6_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_6_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_6_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_6_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_70_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_71_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_72_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_72_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_72_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_72_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_72_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_72_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_72_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_72_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_73_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_73_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_73_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_73_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_73_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_73_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_73_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_73_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_74_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_75_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_76_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_77_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_77_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_77_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_77_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_78_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_79_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_7_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_7_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_7_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_7_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_80_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_81_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_82_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_83_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_84_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_85_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_86_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_86_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_86_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_86_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_87_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_88_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_89_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_8_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_90_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_91_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_92_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_93_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_93_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_93_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_93_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_93_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_93_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_93_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_94_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_94_n_1\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_94_n_2\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_94_n_3\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_94_n_4\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_94_n_5\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_94_n_6\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_94_n_7\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_95_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_96_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_97_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_98_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_99_n_0\ : STD_LOGIC;
  signal \disp[5][0]_INST_0_i_9_n_0\ : STD_LOGIC;
  signal \disp[5]_OBUF\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \inst_int_to_string/cent\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \inst_int_to_string/euros2\ : STD_LOGIC_VECTOR ( 30 downto 0 );
  signal \inst_int_to_string/euros3\ : STD_LOGIC_VECTOR ( 30 downto 1 );
  signal \inst_int_to_string/p_1_in\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \inst_int_to_string/string_cent_decenas[1]5\ : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \next_state__0\ : STD_LOGIC_VECTOR ( 1 to 1 );
  signal st_OBUF : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \total[3]_i_2_n_0\ : STD_LOGIC;
  signal \total[3]_i_3_n_0\ : STD_LOGIC;
  signal \total[3]_i_4_n_0\ : STD_LOGIC;
  signal \total[3]_i_5_n_0\ : STD_LOGIC;
  signal \total[3]_i_6_n_0\ : STD_LOGIC;
  signal \total[3]_i_7_n_0\ : STD_LOGIC;
  signal \total[3]_i_8_n_0\ : STD_LOGIC;
  signal \total[3]_i_9_n_0\ : STD_LOGIC;
  signal \total[7]_i_2_n_0\ : STD_LOGIC;
  signal \total[7]_i_3_n_0\ : STD_LOGIC;
  signal \total[7]_i_4_n_0\ : STD_LOGIC;
  signal \total[7]_i_5_n_0\ : STD_LOGIC;
  signal \total[7]_i_6_n_0\ : STD_LOGIC;
  signal \total[7]_i_7_n_0\ : STD_LOGIC;
  signal \total[7]_i_8_n_0\ : STD_LOGIC;
  signal total_out : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal total_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \total_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \total_reg[3]_i_1_n_1\ : STD_LOGIC;
  signal \total_reg[3]_i_1_n_2\ : STD_LOGIC;
  signal \total_reg[3]_i_1_n_3\ : STD_LOGIC;
  signal \total_reg[3]_i_1_n_4\ : STD_LOGIC;
  signal \total_reg[3]_i_1_n_5\ : STD_LOGIC;
  signal \total_reg[3]_i_1_n_6\ : STD_LOGIC;
  signal \total_reg[3]_i_1_n_7\ : STD_LOGIC;
  signal \total_reg[7]_i_1_n_1\ : STD_LOGIC;
  signal \total_reg[7]_i_1_n_2\ : STD_LOGIC;
  signal \total_reg[7]_i_1_n_3\ : STD_LOGIC;
  signal \total_reg[7]_i_1_n_4\ : STD_LOGIC;
  signal \total_reg[7]_i_1_n_5\ : STD_LOGIC;
  signal \total_reg[7]_i_1_n_6\ : STD_LOGIC;
  signal \total_reg[7]_i_1_n_7\ : STD_LOGIC;
  signal \total_string[3]_i_2_n_0\ : STD_LOGIC;
  signal \total_string[3]_i_3_n_0\ : STD_LOGIC;
  signal \total_string[3]_i_4_n_0\ : STD_LOGIC;
  signal \total_string[3]_i_5_n_0\ : STD_LOGIC;
  signal \total_string[3]_i_6_n_0\ : STD_LOGIC;
  signal \total_string[3]_i_7_n_0\ : STD_LOGIC;
  signal \total_string[3]_i_8_n_0\ : STD_LOGIC;
  signal \total_string[3]_i_9_n_0\ : STD_LOGIC;
  signal \total_string[7]_i_2_n_0\ : STD_LOGIC;
  signal \total_string[7]_i_3_n_0\ : STD_LOGIC;
  signal \total_string[7]_i_4_n_0\ : STD_LOGIC;
  signal \total_string[7]_i_5_n_0\ : STD_LOGIC;
  signal \total_string[7]_i_6_n_0\ : STD_LOGIC;
  signal \total_string[7]_i_7_n_0\ : STD_LOGIC;
  signal \total_string[7]_i_8_n_0\ : STD_LOGIC;
  signal \total_string_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \total_string_reg[3]_i_1_n_1\ : STD_LOGIC;
  signal \total_string_reg[3]_i_1_n_2\ : STD_LOGIC;
  signal \total_string_reg[3]_i_1_n_3\ : STD_LOGIC;
  signal \total_string_reg[3]_i_1_n_4\ : STD_LOGIC;
  signal \total_string_reg[3]_i_1_n_5\ : STD_LOGIC;
  signal \total_string_reg[3]_i_1_n_6\ : STD_LOGIC;
  signal \total_string_reg[3]_i_1_n_7\ : STD_LOGIC;
  signal \total_string_reg[7]_i_1_n_1\ : STD_LOGIC;
  signal \total_string_reg[7]_i_1_n_2\ : STD_LOGIC;
  signal \total_string_reg[7]_i_1_n_3\ : STD_LOGIC;
  signal \total_string_reg[7]_i_1_n_4\ : STD_LOGIC;
  signal \total_string_reg[7]_i_1_n_5\ : STD_LOGIC;
  signal \total_string_reg[7]_i_1_n_6\ : STD_LOGIC;
  signal \total_string_reg[7]_i_1_n_7\ : STD_LOGIC;
  signal valor_moneda_IBUF : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_disp[2][0]_INST_0_i_109_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[2][0]_INST_0_i_119_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[2][0]_INST_0_i_13_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_disp[2][0]_INST_0_i_13_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_disp[2][0]_INST_0_i_14_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_disp[2][0]_INST_0_i_158_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[2][0]_INST_0_i_167_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_disp[2][0]_INST_0_i_167_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[2][0]_INST_0_i_168_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \NLW_disp[2][0]_INST_0_i_168_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_disp[2][0]_INST_0_i_169_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \NLW_disp[2][0]_INST_0_i_169_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_disp[2][0]_INST_0_i_17_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_disp[2][0]_INST_0_i_17_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_disp[2][0]_INST_0_i_170_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[2][0]_INST_0_i_179_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_disp[2][0]_INST_0_i_180_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_disp[2][0]_INST_0_i_189_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_disp[2][0]_INST_0_i_20_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[2][0]_INST_0_i_204_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[2][0]_INST_0_i_249_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[2][0]_INST_0_i_274_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_disp[2][0]_INST_0_i_279_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[2][0]_INST_0_i_315_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_disp[2][0]_INST_0_i_315_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_disp[2][0]_INST_0_i_322_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_disp[2][0]_INST_0_i_326_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[2][0]_INST_0_i_38_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[2][0]_INST_0_i_48_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_disp[2][0]_INST_0_i_48_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[2][0]_INST_0_i_50_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_disp[2][0]_INST_0_i_50_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_disp[2][0]_INST_0_i_51_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_disp[2][0]_INST_0_i_51_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_disp[2][0]_INST_0_i_58_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[2][0]_INST_0_i_58_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_disp[2][0]_INST_0_i_60_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[2][0]_INST_0_i_70_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_disp[2][0]_INST_0_i_70_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[2][0]_INST_0_i_72_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_disp[2][3]_INST_0_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[2][3]_INST_0_i_2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_disp[3][2]_INST_0_i_10_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_disp[3][2]_INST_0_i_102_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_disp[3][2]_INST_0_i_111_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_disp[3][2]_INST_0_i_120_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[3][2]_INST_0_i_156_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[3][2]_INST_0_i_156_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_disp[3][2]_INST_0_i_159_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_disp[3][2]_INST_0_i_166_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[3][2]_INST_0_i_29_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_disp[3][2]_INST_0_i_29_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_disp[3][2]_INST_0_i_37_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[3][2]_INST_0_i_45_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_disp[3][2]_INST_0_i_45_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_disp[3][2]_INST_0_i_46_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[3][2]_INST_0_i_55_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_disp[3][2]_INST_0_i_55_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_disp[3][2]_INST_0_i_56_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \NLW_disp[3][2]_INST_0_i_56_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_disp[3][2]_INST_0_i_57_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_disp[3][2]_INST_0_i_57_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_disp[3][2]_INST_0_i_60_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[3][2]_INST_0_i_84_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[3][2]_INST_0_i_9_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_disp[5][0]_INST_0_i_113_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[5][0]_INST_0_i_16_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[5][0]_INST_0_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_disp[5][0]_INST_0_i_2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[5][0]_INST_0_i_205_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[5][0]_INST_0_i_238_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_disp[5][0]_INST_0_i_238_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_disp[5][0]_INST_0_i_239_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \NLW_disp[5][0]_INST_0_i_239_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_disp[5][0]_INST_0_i_240_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_disp[5][0]_INST_0_i_240_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_disp[5][0]_INST_0_i_241_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[5][0]_INST_0_i_25_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[5][0]_INST_0_i_298_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[5][0]_INST_0_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_disp[5][0]_INST_0_i_3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_disp[5][0]_INST_0_i_307_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_disp[5][0]_INST_0_i_307_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_disp[5][0]_INST_0_i_350_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[5][0]_INST_0_i_362_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_disp[5][0]_INST_0_i_4_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_disp[5][0]_INST_0_i_4_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[5][0]_INST_0_i_409_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[5][0]_INST_0_i_499_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[5][0]_INST_0_i_5_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_disp[5][0]_INST_0_i_5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_disp[5][0]_INST_0_i_50_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[5][0]_INST_0_i_50_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_disp[5][0]_INST_0_i_508_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_disp[5][0]_INST_0_i_508_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_disp[5][0]_INST_0_i_51_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[5][0]_INST_0_i_525_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[5][0]_INST_0_i_525_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_disp[5][0]_INST_0_i_551_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \NLW_disp[5][0]_INST_0_i_602_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[5][0]_INST_0_i_7_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[5][0]_INST_0_i_77_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_disp[5][0]_INST_0_i_93_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_total_reg[7]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_total_string_reg[7]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_current_state[0]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \FSM_sequential_current_state[1]_i_1\ : label is "soft_lutpair3";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_current_state_reg[0]\ : label is "s2:11,s1:01,s0:00,s3:10";
  attribute FSM_ENCODED_STATES of \FSM_sequential_current_state_reg[1]\ : label is "s2:11,s1:01,s0:00,s3:10";
  attribute SOFT_HLUTNM of bdf_OBUF_inst_i_1 : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of devolver_OBUF_inst_i_1 : label is "soft_lutpair35";
  attribute HLUTNM : string;
  attribute HLUTNM of \disp[2][0]_INST_0_i_102\ : label is "lutpair84";
  attribute HLUTNM of \disp[2][0]_INST_0_i_103\ : label is "lutpair83";
  attribute HLUTNM of \disp[2][0]_INST_0_i_104\ : label is "lutpair82";
  attribute HLUTNM of \disp[2][0]_INST_0_i_107\ : label is "lutpair84";
  attribute HLUTNM of \disp[2][0]_INST_0_i_108\ : label is "lutpair83";
  attribute HLUTNM of \disp[2][0]_INST_0_i_112\ : label is "lutpair5";
  attribute HLUTNM of \disp[2][0]_INST_0_i_117\ : label is "lutpair5";
  attribute SOFT_HLUTNM of \disp[2][0]_INST_0_i_129\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \disp[2][0]_INST_0_i_130\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \disp[2][0]_INST_0_i_131\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \disp[2][0]_INST_0_i_132\ : label is "soft_lutpair27";
  attribute HLUTNM of \disp[2][0]_INST_0_i_142\ : label is "lutpair80";
  attribute HLUTNM of \disp[2][0]_INST_0_i_146\ : label is "lutpair80";
  attribute HLUTNM of \disp[2][0]_INST_0_i_171\ : label is "lutpair86";
  attribute HLUTNM of \disp[2][0]_INST_0_i_172\ : label is "lutpair85";
  attribute HLUTNM of \disp[2][0]_INST_0_i_176\ : label is "lutpair86";
  attribute HLUTNM of \disp[2][0]_INST_0_i_177\ : label is "lutpair85";
  attribute HLUTNM of \disp[2][0]_INST_0_i_282\ : label is "lutpair4";
  attribute HLUTNM of \disp[2][0]_INST_0_i_283\ : label is "lutpair3";
  attribute HLUTNM of \disp[2][0]_INST_0_i_287\ : label is "lutpair4";
  attribute HLUTNM of \disp[2][0]_INST_0_i_296\ : label is "lutpair2";
  attribute HLUTNM of \disp[2][0]_INST_0_i_297\ : label is "lutpair1";
  attribute HLUTNM of \disp[2][0]_INST_0_i_298\ : label is "lutpair0";
  attribute HLUTNM of \disp[2][0]_INST_0_i_299\ : label is "lutpair91";
  attribute HLUTNM of \disp[2][0]_INST_0_i_301\ : label is "lutpair2";
  attribute HLUTNM of \disp[2][0]_INST_0_i_302\ : label is "lutpair1";
  attribute HLUTNM of \disp[2][0]_INST_0_i_303\ : label is "lutpair0";
  attribute HLUTNM of \disp[2][0]_INST_0_i_311\ : label is "lutpair3";
  attribute HLUTNM of \disp[2][0]_INST_0_i_319\ : label is "lutpair91";
  attribute SOFT_HLUTNM of \disp[2][0]_INST_0_i_53\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \disp[2][0]_INST_0_i_54\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \disp[2][0]_INST_0_i_55\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \disp[2][0]_INST_0_i_56\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \disp[2][0]_INST_0_i_69\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \disp[2][0]_INST_0_i_73\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \disp[2][0]_INST_0_i_74\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \disp[2][0]_INST_0_i_75\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \disp[2][0]_INST_0_i_76\ : label is "soft_lutpair26";
  attribute HLUTNM of \disp[2][0]_INST_0_i_85\ : label is "lutpair81";
  attribute HLUTNM of \disp[2][0]_INST_0_i_88\ : label is "lutpair82";
  attribute HLUTNM of \disp[2][0]_INST_0_i_89\ : label is "lutpair81";
  attribute SOFT_HLUTNM of \disp[2][1]_INST_0_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \disp[2][2]_INST_0_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \disp[2][3]_INST_0_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \disp[3][0]_INST_0_i_3\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \disp[3][0]_INST_0_i_6\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \disp[3][1]_INST_0_i_1\ : label is "soft_lutpair2";
  attribute HLUTNM of \disp[3][2]_INST_0_i_105\ : label is "lutpair87";
  attribute HLUTNM of \disp[3][2]_INST_0_i_110\ : label is "lutpair87";
  attribute HLUTNM of \disp[3][2]_INST_0_i_123\ : label is "lutpair89";
  attribute HLUTNM of \disp[3][2]_INST_0_i_124\ : label is "lutpair88";
  attribute HLUTNM of \disp[3][2]_INST_0_i_128\ : label is "lutpair89";
  attribute SOFT_HLUTNM of \disp[3][2]_INST_0_i_129\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \disp[3][2]_INST_0_i_13\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \disp[3][2]_INST_0_i_130\ : label is "soft_lutpair32";
  attribute HLUTNM of \disp[3][2]_INST_0_i_152\ : label is "lutpair88";
  attribute SOFT_HLUTNM of \disp[3][2]_INST_0_i_158\ : label is "soft_lutpair31";
  attribute HLUTNM of \disp[3][2]_INST_0_i_39\ : label is "lutpair90";
  attribute HLUTNM of \disp[3][2]_INST_0_i_44\ : label is "lutpair90";
  attribute SOFT_HLUTNM of \disp[3][2]_INST_0_i_7\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \disp[3][3]_INST_0_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \disp[3][3]_INST_0_i_13\ : label is "soft_lutpair5";
  attribute HLUTNM of \disp[5][0]_INST_0_i_100\ : label is "lutpair77";
  attribute HLUTNM of \disp[5][0]_INST_0_i_101\ : label is "lutpair76";
  attribute HLUTNM of \disp[5][0]_INST_0_i_102\ : label is "lutpair75";
  attribute HLUTNM of \disp[5][0]_INST_0_i_104\ : label is "lutpair60";
  attribute HLUTNM of \disp[5][0]_INST_0_i_105\ : label is "lutpair59";
  attribute HLUTNM of \disp[5][0]_INST_0_i_106\ : label is "lutpair58";
  attribute HLUTNM of \disp[5][0]_INST_0_i_107\ : label is "lutpair57";
  attribute HLUTNM of \disp[5][0]_INST_0_i_109\ : label is "lutpair60";
  attribute HLUTNM of \disp[5][0]_INST_0_i_110\ : label is "lutpair59";
  attribute HLUTNM of \disp[5][0]_INST_0_i_111\ : label is "lutpair58";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_123\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_124\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_129\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_130\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_131\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_132\ : label is "soft_lutpair9";
  attribute HLUTNM of \disp[5][0]_INST_0_i_137\ : label is "lutpair39";
  attribute HLUTNM of \disp[5][0]_INST_0_i_141\ : label is "lutpair31";
  attribute HLUTNM of \disp[5][0]_INST_0_i_142\ : label is "lutpair30";
  attribute HLUTNM of \disp[5][0]_INST_0_i_143\ : label is "lutpair29";
  attribute HLUTNM of \disp[5][0]_INST_0_i_144\ : label is "lutpair28";
  attribute HLUTNM of \disp[5][0]_INST_0_i_145\ : label is "lutpair32";
  attribute HLUTNM of \disp[5][0]_INST_0_i_146\ : label is "lutpair31";
  attribute HLUTNM of \disp[5][0]_INST_0_i_147\ : label is "lutpair30";
  attribute HLUTNM of \disp[5][0]_INST_0_i_148\ : label is "lutpair29";
  attribute HLUTNM of \disp[5][0]_INST_0_i_159\ : label is "lutpair20";
  attribute HLUTNM of \disp[5][0]_INST_0_i_160\ : label is "lutpair19";
  attribute HLUTNM of \disp[5][0]_INST_0_i_164\ : label is "lutpair20";
  attribute HLUTNM of \disp[5][0]_INST_0_i_173\ : label is "lutpair27";
  attribute HLUTNM of \disp[5][0]_INST_0_i_174\ : label is "lutpair26";
  attribute HLUTNM of \disp[5][0]_INST_0_i_175\ : label is "lutpair25";
  attribute HLUTNM of \disp[5][0]_INST_0_i_176\ : label is "lutpair24";
  attribute HLUTNM of \disp[5][0]_INST_0_i_177\ : label is "lutpair28";
  attribute HLUTNM of \disp[5][0]_INST_0_i_178\ : label is "lutpair27";
  attribute HLUTNM of \disp[5][0]_INST_0_i_179\ : label is "lutpair26";
  attribute HLUTNM of \disp[5][0]_INST_0_i_180\ : label is "lutpair25";
  attribute HLUTNM of \disp[5][0]_INST_0_i_181\ : label is "lutpair18";
  attribute HLUTNM of \disp[5][0]_INST_0_i_182\ : label is "lutpair17";
  attribute HLUTNM of \disp[5][0]_INST_0_i_183\ : label is "lutpair16";
  attribute HLUTNM of \disp[5][0]_INST_0_i_184\ : label is "lutpair15";
  attribute HLUTNM of \disp[5][0]_INST_0_i_185\ : label is "lutpair19";
  attribute HLUTNM of \disp[5][0]_INST_0_i_186\ : label is "lutpair18";
  attribute HLUTNM of \disp[5][0]_INST_0_i_187\ : label is "lutpair17";
  attribute HLUTNM of \disp[5][0]_INST_0_i_188\ : label is "lutpair16";
  attribute HLUTNM of \disp[5][0]_INST_0_i_189\ : label is "lutpair35";
  attribute HLUTNM of \disp[5][0]_INST_0_i_190\ : label is "lutpair34";
  attribute HLUTNM of \disp[5][0]_INST_0_i_191\ : label is "lutpair33";
  attribute HLUTNM of \disp[5][0]_INST_0_i_192\ : label is "lutpair32";
  attribute HLUTNM of \disp[5][0]_INST_0_i_193\ : label is "lutpair36";
  attribute HLUTNM of \disp[5][0]_INST_0_i_194\ : label is "lutpair35";
  attribute HLUTNM of \disp[5][0]_INST_0_i_195\ : label is "lutpair34";
  attribute HLUTNM of \disp[5][0]_INST_0_i_196\ : label is "lutpair33";
  attribute HLUTNM of \disp[5][0]_INST_0_i_197\ : label is "lutpair42";
  attribute HLUTNM of \disp[5][0]_INST_0_i_198\ : label is "lutpair41";
  attribute HLUTNM of \disp[5][0]_INST_0_i_199\ : label is "lutpair40";
  attribute HLUTNM of \disp[5][0]_INST_0_i_200\ : label is "lutpair39";
  attribute HLUTNM of \disp[5][0]_INST_0_i_201\ : label is "lutpair43";
  attribute HLUTNM of \disp[5][0]_INST_0_i_202\ : label is "lutpair42";
  attribute HLUTNM of \disp[5][0]_INST_0_i_203\ : label is "lutpair41";
  attribute HLUTNM of \disp[5][0]_INST_0_i_204\ : label is "lutpair40";
  attribute HLUTNM of \disp[5][0]_INST_0_i_220\ : label is "lutpair73";
  attribute HLUTNM of \disp[5][0]_INST_0_i_221\ : label is "lutpair72";
  attribute HLUTNM of \disp[5][0]_INST_0_i_222\ : label is "lutpair71";
  attribute HLUTNM of \disp[5][0]_INST_0_i_223\ : label is "lutpair70";
  attribute HLUTNM of \disp[5][0]_INST_0_i_224\ : label is "lutpair74";
  attribute HLUTNM of \disp[5][0]_INST_0_i_225\ : label is "lutpair73";
  attribute HLUTNM of \disp[5][0]_INST_0_i_226\ : label is "lutpair72";
  attribute HLUTNM of \disp[5][0]_INST_0_i_227\ : label is "lutpair71";
  attribute HLUTNM of \disp[5][0]_INST_0_i_233\ : label is "lutpair57";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_251\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_252\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_254\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_256\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_258\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_259\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_260\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_261\ : label is "soft_lutpair30";
  attribute HLUTNM of \disp[5][0]_INST_0_i_278\ : label is "lutpair23";
  attribute HLUTNM of \disp[5][0]_INST_0_i_279\ : label is "lutpair22";
  attribute HLUTNM of \disp[5][0]_INST_0_i_280\ : label is "lutpair21";
  attribute HLUTNM of \disp[5][0]_INST_0_i_282\ : label is "lutpair24";
  attribute HLUTNM of \disp[5][0]_INST_0_i_283\ : label is "lutpair23";
  attribute HLUTNM of \disp[5][0]_INST_0_i_284\ : label is "lutpair22";
  attribute HLUTNM of \disp[5][0]_INST_0_i_285\ : label is "lutpair21";
  attribute HLUTNM of \disp[5][0]_INST_0_i_286\ : label is "lutpair14";
  attribute HLUTNM of \disp[5][0]_INST_0_i_287\ : label is "lutpair13";
  attribute HLUTNM of \disp[5][0]_INST_0_i_288\ : label is "lutpair12";
  attribute HLUTNM of \disp[5][0]_INST_0_i_289\ : label is "lutpair11";
  attribute HLUTNM of \disp[5][0]_INST_0_i_290\ : label is "lutpair15";
  attribute HLUTNM of \disp[5][0]_INST_0_i_291\ : label is "lutpair14";
  attribute HLUTNM of \disp[5][0]_INST_0_i_292\ : label is "lutpair13";
  attribute HLUTNM of \disp[5][0]_INST_0_i_293\ : label is "lutpair12";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_297\ : label is "soft_lutpair23";
  attribute HLUTNM of \disp[5][0]_INST_0_i_309\ : label is "lutpair69";
  attribute HLUTNM of \disp[5][0]_INST_0_i_310\ : label is "lutpair68";
  attribute HLUTNM of \disp[5][0]_INST_0_i_311\ : label is "lutpair67";
  attribute HLUTNM of \disp[5][0]_INST_0_i_312\ : label is "lutpair66";
  attribute HLUTNM of \disp[5][0]_INST_0_i_313\ : label is "lutpair70";
  attribute HLUTNM of \disp[5][0]_INST_0_i_314\ : label is "lutpair69";
  attribute HLUTNM of \disp[5][0]_INST_0_i_315\ : label is "lutpair68";
  attribute HLUTNM of \disp[5][0]_INST_0_i_316\ : label is "lutpair67";
  attribute HLUTNM of \disp[5][0]_INST_0_i_353\ : label is "lutpair56";
  attribute HLUTNM of \disp[5][0]_INST_0_i_354\ : label is "lutpair55";
  attribute HLUTNM of \disp[5][0]_INST_0_i_358\ : label is "lutpair56";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_361\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_363\ : label is "soft_lutpair1";
  attribute HLUTNM of \disp[5][0]_INST_0_i_381\ : label is "lutpair10";
  attribute HLUTNM of \disp[5][0]_INST_0_i_382\ : label is "lutpair9";
  attribute HLUTNM of \disp[5][0]_INST_0_i_383\ : label is "lutpair8";
  attribute HLUTNM of \disp[5][0]_INST_0_i_384\ : label is "lutpair7";
  attribute HLUTNM of \disp[5][0]_INST_0_i_385\ : label is "lutpair11";
  attribute HLUTNM of \disp[5][0]_INST_0_i_386\ : label is "lutpair10";
  attribute HLUTNM of \disp[5][0]_INST_0_i_387\ : label is "lutpair9";
  attribute HLUTNM of \disp[5][0]_INST_0_i_388\ : label is "lutpair8";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_397\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_398\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_399\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_400\ : label is "soft_lutpair24";
  attribute HLUTNM of \disp[5][0]_INST_0_i_423\ : label is "lutpair65";
  attribute HLUTNM of \disp[5][0]_INST_0_i_424\ : label is "lutpair64";
  attribute HLUTNM of \disp[5][0]_INST_0_i_425\ : label is "lutpair63";
  attribute HLUTNM of \disp[5][0]_INST_0_i_426\ : label is "lutpair62";
  attribute HLUTNM of \disp[5][0]_INST_0_i_427\ : label is "lutpair66";
  attribute HLUTNM of \disp[5][0]_INST_0_i_428\ : label is "lutpair65";
  attribute HLUTNM of \disp[5][0]_INST_0_i_429\ : label is "lutpair64";
  attribute HLUTNM of \disp[5][0]_INST_0_i_43\ : label is "lutpair79";
  attribute HLUTNM of \disp[5][0]_INST_0_i_430\ : label is "lutpair63";
  attribute HLUTNM of \disp[5][0]_INST_0_i_44\ : label is "lutpair78";
  attribute HLUTNM of \disp[5][0]_INST_0_i_445\ : label is "lutpair54";
  attribute HLUTNM of \disp[5][0]_INST_0_i_446\ : label is "lutpair53";
  attribute HLUTNM of \disp[5][0]_INST_0_i_447\ : label is "lutpair52";
  attribute HLUTNM of \disp[5][0]_INST_0_i_448\ : label is "lutpair51";
  attribute HLUTNM of \disp[5][0]_INST_0_i_450\ : label is "lutpair54";
  attribute HLUTNM of \disp[5][0]_INST_0_i_451\ : label is "lutpair53";
  attribute HLUTNM of \disp[5][0]_INST_0_i_452\ : label is "lutpair52";
  attribute HLUTNM of \disp[5][0]_INST_0_i_462\ : label is "lutpair38";
  attribute HLUTNM of \disp[5][0]_INST_0_i_463\ : label is "lutpair37";
  attribute HLUTNM of \disp[5][0]_INST_0_i_464\ : label is "lutpair36";
  attribute HLUTNM of \disp[5][0]_INST_0_i_467\ : label is "lutpair38";
  attribute HLUTNM of \disp[5][0]_INST_0_i_468\ : label is "lutpair37";
  attribute HLUTNM of \disp[5][0]_INST_0_i_473\ : label is "lutpair55";
  attribute HLUTNM of \disp[5][0]_INST_0_i_48\ : label is "lutpair79";
  attribute HLUTNM of \disp[5][0]_INST_0_i_482\ : label is "lutpair6";
  attribute HLUTNM of \disp[5][0]_INST_0_i_486\ : label is "lutpair7";
  attribute HLUTNM of \disp[5][0]_INST_0_i_487\ : label is "lutpair6";
  attribute HLUTNM of \disp[5][0]_INST_0_i_509\ : label is "lutpair61";
  attribute HLUTNM of \disp[5][0]_INST_0_i_512\ : label is "lutpair62";
  attribute HLUTNM of \disp[5][0]_INST_0_i_513\ : label is "lutpair61";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_528\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_530\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_533\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_534\ : label is "soft_lutpair21";
  attribute HLUTNM of \disp[5][0]_INST_0_i_535\ : label is "lutpair50";
  attribute HLUTNM of \disp[5][0]_INST_0_i_536\ : label is "lutpair49";
  attribute HLUTNM of \disp[5][0]_INST_0_i_537\ : label is "lutpair48";
  attribute HLUTNM of \disp[5][0]_INST_0_i_538\ : label is "lutpair47";
  attribute HLUTNM of \disp[5][0]_INST_0_i_539\ : label is "lutpair51";
  attribute HLUTNM of \disp[5][0]_INST_0_i_540\ : label is "lutpair50";
  attribute HLUTNM of \disp[5][0]_INST_0_i_541\ : label is "lutpair49";
  attribute HLUTNM of \disp[5][0]_INST_0_i_542\ : label is "lutpair48";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_572\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_573\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_574\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_575\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_576\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_577\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_578\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_579\ : label is "soft_lutpair17";
  attribute HLUTNM of \disp[5][0]_INST_0_i_580\ : label is "lutpair46";
  attribute HLUTNM of \disp[5][0]_INST_0_i_581\ : label is "lutpair45";
  attribute HLUTNM of \disp[5][0]_INST_0_i_582\ : label is "lutpair44";
  attribute HLUTNM of \disp[5][0]_INST_0_i_583\ : label is "lutpair43";
  attribute HLUTNM of \disp[5][0]_INST_0_i_584\ : label is "lutpair47";
  attribute HLUTNM of \disp[5][0]_INST_0_i_585\ : label is "lutpair46";
  attribute HLUTNM of \disp[5][0]_INST_0_i_586\ : label is "lutpair45";
  attribute HLUTNM of \disp[5][0]_INST_0_i_587\ : label is "lutpair44";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_618\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_64\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_65\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_67\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_70\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_71\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_74\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_75\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \disp[5][0]_INST_0_i_76\ : label is "soft_lutpair13";
  attribute HLUTNM of \disp[5][0]_INST_0_i_95\ : label is "lutpair77";
  attribute HLUTNM of \disp[5][0]_INST_0_i_96\ : label is "lutpair76";
  attribute HLUTNM of \disp[5][0]_INST_0_i_97\ : label is "lutpair75";
  attribute HLUTNM of \disp[5][0]_INST_0_i_98\ : label is "lutpair74";
  attribute HLUTNM of \disp[5][0]_INST_0_i_99\ : label is "lutpair78";
begin
\FSM_sequential_current_state[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3530"
    )
        port map (
      I0 => st_OBUF(1),
      I1 => button_ok_IBUF,
      I2 => current_state(0),
      I3 => \FSM_sequential_current_state[1]_i_3_n_0\,
      O => \FSM_sequential_current_state[0]_i_1_n_0\
    );
\FSM_sequential_current_state[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EFEA2222"
    )
        port map (
      I0 => st_OBUF(1),
      I1 => button_ok_IBUF,
      I2 => current_state(0),
      I3 => \FSM_sequential_current_state[1]_i_3_n_0\,
      I4 => \next_state__0\(1),
      O => \FSM_sequential_current_state[1]_i_1_n_0\
    );
\FSM_sequential_current_state[1]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => RESET_IBUF,
      O => \FSM_sequential_current_state[1]_i_2_n_0\
    );
\FSM_sequential_current_state[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFAAA80000"
    )
        port map (
      I0 => total_reg(5),
      I1 => total_reg(2),
      I2 => total_reg(3),
      I3 => total_reg(4),
      I4 => total_reg(6),
      I5 => total_reg(7),
      O => \FSM_sequential_current_state[1]_i_3_n_0\
    );
\FSM_sequential_current_state[1]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000FFEF00EF"
    )
        port map (
      I0 => total_reg(7),
      I1 => \FSM_sequential_current_state[1]_i_5_n_0\,
      I2 => total_reg(5),
      I3 => current_state(0),
      I4 => button_ok_IBUF,
      I5 => st_OBUF(1),
      O => \next_state__0\(1)
    );
\FSM_sequential_current_state[1]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFDFFFFFFFF"
    )
        port map (
      I0 => total_reg(6),
      I1 => total_reg(1),
      I2 => total_reg(4),
      I3 => total_reg(0),
      I4 => total_reg(3),
      I5 => total_reg(2),
      O => \FSM_sequential_current_state[1]_i_5_n_0\
    );
\FSM_sequential_current_state_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \FSM_sequential_current_state[1]_i_2_n_0\,
      D => \FSM_sequential_current_state[0]_i_1_n_0\,
      Q => current_state(0)
    );
\FSM_sequential_current_state_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \FSM_sequential_current_state[1]_i_2_n_0\,
      D => \FSM_sequential_current_state[1]_i_1_n_0\,
      Q => st_OBUF(1)
    );
RESET_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => RESET,
      O => RESET_IBUF
    );
bdf_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => bdf_OBUF,
      O => bdf
    );
bdf_OBUF_inst_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => st_OBUF(1),
      I1 => current_state(0),
      O => bdf_OBUF
    );
button_ok_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => button_ok,
      O => button_ok_IBUF
    );
clk_IBUF_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => clk_IBUF,
      O => clk_IBUF_BUFG
    );
clk_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => clk,
      O => clk_IBUF
    );
devolver_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => devolver_OBUF,
      O => devolver
    );
devolver_OBUF_inst_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => current_state(0),
      I1 => st_OBUF(1),
      O => devolver_OBUF
    );
\disp[1][0]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[1]\(0)
    );
\disp[1][1]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[1]_OBUF\(1),
      O => \disp[1]\(1)
    );
\disp[1][1]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1515151515151555"
    )
        port map (
      I0 => total_out(7),
      I1 => total_out(5),
      I2 => total_out(6),
      I3 => total_out(3),
      I4 => total_out(4),
      I5 => total_out(2),
      O => \disp[1]_OBUF\(1)
    );
\disp[1][2]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[1]_OBUF\(2),
      O => \disp[1]\(2)
    );
\disp[1][3]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[1]\(3)
    );
\disp[1][4]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[1]\(4)
    );
\disp[1][5]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[1]\(5)
    );
\disp[1][6]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[1]\(6)
    );
\disp[1][7]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[1]\(7)
    );
\disp[2][0]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[2]_OBUF\(0),
      O => \disp[2]\(0)
    );
\disp[2][0]_INST_0_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \disp[2][0]_INST_0_i_1_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_1_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_1_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_1_n_3\,
      CYINIT => '1',
      DI(3) => \disp[2][0]_INST_0_i_2_n_0\,
      DI(2) => \disp[2][0]_INST_0_i_3_n_0\,
      DI(1) => \disp[2][0]_INST_0_i_4_n_0\,
      DI(0) => \disp[2][0]_INST_0_i_5_n_0\,
      O(3) => \disp[2][0]_INST_0_i_1_n_4\,
      O(2) => \disp[2][0]_INST_0_i_1_n_5\,
      O(1) => \disp[2][0]_INST_0_i_1_n_6\,
      O(0) => \disp[2]_OBUF\(0),
      S(3) => \disp[2][0]_INST_0_i_6_n_0\,
      S(2) => \disp[2][0]_INST_0_i_7_n_0\,
      S(1) => \disp[2][0]_INST_0_i_8_n_0\,
      S(0) => \disp[2][0]_INST_0_i_9_n_0\
    );
\disp[2][0]_INST_0_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"44477774"
    )
        port map (
      I0 => total_out(3),
      I1 => \disp[1]_OBUF\(1),
      I2 => \disp[2][0]_INST_0_i_15_n_5\,
      I3 => \disp[2][0]_INST_0_i_16_n_0\,
      I4 => \disp[2][0]_INST_0_i_15_n_4\,
      O => \disp[2][0]_INST_0_i_10_n_0\
    );
\disp[2][0]_INST_0_i_100\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \inst_int_to_string/cent\(5),
      I1 => \inst_int_to_string/cent\(6),
      O => \disp[2][0]_INST_0_i_100_n_0\
    );
\disp[2][0]_INST_0_i_101\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_70_n_3\,
      I1 => \inst_int_to_string/cent\(5),
      O => \disp[2][0]_INST_0_i_101_n_0\
    );
\disp[2][0]_INST_0_i_102\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_10_n_0\,
      I1 => \disp[2][0]_INST_0_i_70_n_3\,
      I2 => \inst_int_to_string/cent\(5),
      O => \disp[2][0]_INST_0_i_102_n_0\
    );
\disp[2][0]_INST_0_i_103\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_11_n_0\,
      I1 => \disp[2][0]_INST_0_i_70_n_3\,
      I2 => \disp[2][3]_INST_0_i_4_n_0\,
      O => \disp[2][0]_INST_0_i_103_n_0\
    );
\disp[2][0]_INST_0_i_104\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_12_n_0\,
      I1 => \disp[2][0]_INST_0_i_70_n_3\,
      I2 => \disp[2][0]_INST_0_i_10_n_0\,
      O => \disp[2][0]_INST_0_i_104_n_0\
    );
\disp[2][0]_INST_0_i_105\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1E87"
    )
        port map (
      I0 => \inst_int_to_string/cent\(6),
      I1 => \disp[2][3]_INST_0_i_4_n_0\,
      I2 => \inst_int_to_string/cent\(5),
      I3 => \disp[2][0]_INST_0_i_70_n_3\,
      O => \disp[2][0]_INST_0_i_105_n_0\
    );
\disp[2][0]_INST_0_i_106\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_102_n_0\,
      I1 => \disp[2][3]_INST_0_i_4_n_0\,
      I2 => \inst_int_to_string/cent\(6),
      I3 => \disp[2][0]_INST_0_i_70_n_3\,
      O => \disp[2][0]_INST_0_i_106_n_0\
    );
\disp[2][0]_INST_0_i_107\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_10_n_0\,
      I1 => \disp[2][0]_INST_0_i_70_n_3\,
      I2 => \inst_int_to_string/cent\(5),
      I3 => \disp[2][0]_INST_0_i_103_n_0\,
      O => \disp[2][0]_INST_0_i_107_n_0\
    );
\disp[2][0]_INST_0_i_108\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_11_n_0\,
      I1 => \disp[2][0]_INST_0_i_70_n_3\,
      I2 => \disp[2][3]_INST_0_i_4_n_0\,
      I3 => \disp[2][0]_INST_0_i_104_n_0\,
      O => \disp[2][0]_INST_0_i_108_n_0\
    );
\disp[2][0]_INST_0_i_109\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_158_n_0\,
      CO(3) => \disp[2][0]_INST_0_i_109_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_109_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_109_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_109_n_3\,
      CYINIT => '0',
      DI(3) => \disp[2][0]_INST_0_i_159_n_0\,
      DI(2) => \disp[2][0]_INST_0_i_160_n_0\,
      DI(1) => \disp[2][0]_INST_0_i_161_n_0\,
      DI(0) => \disp[2][0]_INST_0_i_162_n_0\,
      O(3 downto 0) => \NLW_disp[2][0]_INST_0_i_109_O_UNCONNECTED\(3 downto 0),
      S(3) => \disp[2][0]_INST_0_i_163_n_0\,
      S(2) => \disp[2][0]_INST_0_i_164_n_0\,
      S(1) => \disp[2][0]_INST_0_i_165_n_0\,
      S(0) => \disp[2][0]_INST_0_i_166_n_0\
    );
\disp[2][0]_INST_0_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4774"
    )
        port map (
      I0 => total_out(2),
      I1 => \disp[1]_OBUF\(1),
      I2 => \disp[2][0]_INST_0_i_15_n_5\,
      I3 => \disp[2][0]_INST_0_i_16_n_0\,
      O => \disp[2][0]_INST_0_i_11_n_0\
    );
\disp[2][0]_INST_0_i_110\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_167_n_3\,
      I1 => \disp[2][0]_INST_0_i_168_n_0\,
      O => \disp[2][0]_INST_0_i_110_n_0\
    );
\disp[2][0]_INST_0_i_111\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"044A"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_169_n_0\,
      I1 => \disp[2][0]_INST_0_i_169_n_5\,
      I2 => \disp[2][0]_INST_0_i_167_n_3\,
      I3 => \disp[2][0]_INST_0_i_168_n_0\,
      O => \disp[2][0]_INST_0_i_111_n_0\
    );
\disp[2][0]_INST_0_i_112\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_169_n_6\,
      I1 => \disp[2][0]_INST_0_i_169_n_5\,
      I2 => \disp[2][0]_INST_0_i_168_n_0\,
      I3 => \disp[2][0]_INST_0_i_167_n_3\,
      O => \disp[2][0]_INST_0_i_112_n_0\
    );
\disp[2][0]_INST_0_i_113\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_169_n_7\,
      I1 => \disp[2][0]_INST_0_i_169_n_6\,
      I2 => \disp[2][0]_INST_0_i_168_n_0\,
      I3 => \disp[2][0]_INST_0_i_167_n_3\,
      O => \disp[2][0]_INST_0_i_113_n_0\
    );
\disp[2][0]_INST_0_i_114\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_169_n_0\,
      I1 => \disp[2][0]_INST_0_i_167_n_3\,
      I2 => \disp[2][0]_INST_0_i_168_n_0\,
      O => \disp[2][0]_INST_0_i_114_n_0\
    );
\disp[2][0]_INST_0_i_115\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0113"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_169_n_5\,
      I1 => \disp[2][0]_INST_0_i_169_n_0\,
      I2 => \disp[2][0]_INST_0_i_167_n_3\,
      I3 => \disp[2][0]_INST_0_i_168_n_0\,
      O => \disp[2][0]_INST_0_i_115_n_0\
    );
\disp[2][0]_INST_0_i_116\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"566AA995"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_112_n_0\,
      I1 => \disp[2][0]_INST_0_i_168_n_0\,
      I2 => \disp[2][0]_INST_0_i_167_n_3\,
      I3 => \disp[2][0]_INST_0_i_169_n_5\,
      I4 => \disp[2][0]_INST_0_i_169_n_0\,
      O => \disp[2][0]_INST_0_i_116_n_0\
    );
\disp[2][0]_INST_0_i_117\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCC9C999"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_169_n_6\,
      I1 => \disp[2][0]_INST_0_i_169_n_5\,
      I2 => \disp[2][0]_INST_0_i_168_n_0\,
      I3 => \disp[2][0]_INST_0_i_167_n_3\,
      I4 => \disp[2][0]_INST_0_i_169_n_7\,
      O => \disp[2][0]_INST_0_i_117_n_0\
    );
\disp[2][0]_INST_0_i_118\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"95"
    )
        port map (
      I0 => \inst_int_to_string/cent\(6),
      I1 => \disp[2][0]_INST_0_i_70_n_3\,
      I2 => \inst_int_to_string/cent\(5),
      O => \disp[2][0]_INST_0_i_118_n_0\
    );
\disp[2][0]_INST_0_i_119\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_170_n_0\,
      CO(3) => \disp[2][0]_INST_0_i_119_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_119_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_119_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_119_n_3\,
      CYINIT => '0',
      DI(3) => \disp[2][0]_INST_0_i_171_n_0\,
      DI(2) => \disp[2][0]_INST_0_i_172_n_0\,
      DI(1) => \disp[2][0]_INST_0_i_173_n_0\,
      DI(0) => \disp[2][0]_INST_0_i_174_n_0\,
      O(3 downto 0) => \NLW_disp[2][0]_INST_0_i_119_O_UNCONNECTED\(3 downto 0),
      S(3) => \disp[2][0]_INST_0_i_175_n_0\,
      S(2) => \disp[2][0]_INST_0_i_176_n_0\,
      S(1) => \disp[2][0]_INST_0_i_177_n_0\,
      S(0) => \disp[2][0]_INST_0_i_178_n_0\
    );
\disp[2][0]_INST_0_i_12\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => total_out(1),
      I1 => \disp[1]_OBUF\(1),
      I2 => \inst_int_to_string/p_1_in\(1),
      O => \disp[2][0]_INST_0_i_12_n_0\
    );
\disp[2][0]_INST_0_i_120\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B190"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_128_n_7\,
      I1 => \disp[2][0]_INST_0_i_70_n_3\,
      I2 => \disp[2][0]_INST_0_i_50_n_7\,
      I3 => \disp[2][0]_INST_0_i_179_n_4\,
      O => \disp[2][0]_INST_0_i_120_n_0\
    );
\disp[2][0]_INST_0_i_121\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B190"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_179_n_4\,
      I1 => \disp[2][0]_INST_0_i_70_n_3\,
      I2 => \disp[2][0]_INST_0_i_92_n_4\,
      I3 => \disp[2][0]_INST_0_i_179_n_5\,
      O => \disp[2][0]_INST_0_i_121_n_0\
    );
\disp[2][0]_INST_0_i_122\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B190"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_179_n_5\,
      I1 => \disp[2][0]_INST_0_i_70_n_3\,
      I2 => \disp[2][0]_INST_0_i_92_n_5\,
      I3 => \disp[2][0]_INST_0_i_179_n_6\,
      O => \disp[2][0]_INST_0_i_122_n_0\
    );
\disp[2][0]_INST_0_i_123\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A832"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_92_n_6\,
      I1 => \disp[2][0]_INST_0_i_70_n_3\,
      I2 => \disp[2][0]_INST_0_i_180_n_7\,
      I3 => \disp[2][0]_INST_0_i_179_n_6\,
      O => \disp[2][0]_INST_0_i_123_n_0\
    );
\disp[2][0]_INST_0_i_124\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669966996696996"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_120_n_0\,
      I1 => \disp[2][0]_INST_0_i_50_n_6\,
      I2 => \disp[2][0]_INST_0_i_128_n_6\,
      I3 => \disp[3][0]_INST_0_i_5_n_0\,
      I4 => \disp[2][0]_INST_0_i_70_n_3\,
      I5 => \disp[2][0]_INST_0_i_128_n_7\,
      O => \disp[2][0]_INST_0_i_124_n_0\
    );
\disp[2][0]_INST_0_i_125\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C3873C780F1EF0E1"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_179_n_5\,
      I1 => \disp[2][0]_INST_0_i_92_n_4\,
      I2 => \disp[2][0]_INST_0_i_50_n_7\,
      I3 => \disp[2][0]_INST_0_i_70_n_3\,
      I4 => \disp[2][0]_INST_0_i_128_n_7\,
      I5 => \disp[2][0]_INST_0_i_179_n_4\,
      O => \disp[2][0]_INST_0_i_125_n_0\
    );
\disp[2][0]_INST_0_i_126\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C3873C780F1EF0E1"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_179_n_6\,
      I1 => \disp[2][0]_INST_0_i_92_n_5\,
      I2 => \disp[2][0]_INST_0_i_92_n_4\,
      I3 => \disp[2][0]_INST_0_i_70_n_3\,
      I4 => \disp[2][0]_INST_0_i_179_n_4\,
      I5 => \disp[2][0]_INST_0_i_179_n_5\,
      O => \disp[2][0]_INST_0_i_126_n_0\
    );
\disp[2][0]_INST_0_i_127\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C3873C780F1EF0E1"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_180_n_7\,
      I1 => \disp[2][0]_INST_0_i_92_n_6\,
      I2 => \disp[2][0]_INST_0_i_92_n_5\,
      I3 => \disp[2][0]_INST_0_i_70_n_3\,
      I4 => \disp[2][0]_INST_0_i_179_n_5\,
      I5 => \disp[2][0]_INST_0_i_179_n_6\,
      O => \disp[2][0]_INST_0_i_127_n_0\
    );
\disp[2][0]_INST_0_i_128\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_179_n_0\,
      CO(3) => \disp[2][0]_INST_0_i_128_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_128_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_128_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_128_n_3\,
      CYINIT => '0',
      DI(3) => \disp[2][0]_INST_0_i_181_n_0\,
      DI(2) => \disp[2][0]_INST_0_i_182_n_0\,
      DI(1) => \disp[2][0]_INST_0_i_183_n_0\,
      DI(0) => \disp[2][0]_INST_0_i_184_n_0\,
      O(3) => \disp[2][0]_INST_0_i_128_n_4\,
      O(2) => \disp[2][0]_INST_0_i_128_n_5\,
      O(1) => \disp[2][0]_INST_0_i_128_n_6\,
      O(0) => \disp[2][0]_INST_0_i_128_n_7\,
      S(3) => \disp[2][0]_INST_0_i_185_n_0\,
      S(2) => \disp[2][0]_INST_0_i_186_n_0\,
      S(1) => \disp[2][0]_INST_0_i_187_n_0\,
      S(0) => \disp[2][0]_INST_0_i_188_n_0\
    );
\disp[2][0]_INST_0_i_129\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B22B"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_70_n_3\,
      I1 => \disp[2][0]_INST_0_i_71_n_7\,
      I2 => \disp[2][0]_INST_0_i_10_n_0\,
      I3 => \disp[3][0]_INST_0_i_5_n_0\,
      O => \disp[2][0]_INST_0_i_129_n_0\
    );
\disp[2][0]_INST_0_i_13\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3 downto 2) => \NLW_disp[2][0]_INST_0_i_13_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \disp[2][0]_INST_0_i_13_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_13_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \disp[2][0]_INST_0_i_17_n_6\,
      DI(0) => '0',
      O(3) => \NLW_disp[2][0]_INST_0_i_13_O_UNCONNECTED\(3),
      O(2) => \disp[2][0]_INST_0_i_13_n_5\,
      O(1) => \disp[2][0]_INST_0_i_13_n_6\,
      O(0) => \disp[2][0]_INST_0_i_13_n_7\,
      S(3) => '0',
      S(2) => \disp[2][0]_INST_0_i_18_n_0\,
      S(1) => \disp[2][0]_INST_0_i_19_n_0\,
      S(0) => \disp[2][0]_INST_0_i_17_n_7\
    );
\disp[2][0]_INST_0_i_130\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_5_n_0\,
      I1 => \disp[2][0]_INST_0_i_10_n_0\,
      I2 => \disp[2][0]_INST_0_i_71_n_7\,
      I3 => \disp[2][0]_INST_0_i_70_n_3\,
      O => \disp[2][0]_INST_0_i_130_n_0\
    );
\disp[2][0]_INST_0_i_131\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_70_n_3\,
      I1 => \disp[2][0]_INST_0_i_12_n_0\,
      I2 => \disp[2][0]_INST_0_i_128_n_5\,
      O => \disp[2][0]_INST_0_i_131_n_0\
    );
\disp[2][0]_INST_0_i_132\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_128_n_5\,
      I1 => \disp[2][0]_INST_0_i_12_n_0\,
      I2 => \disp[2][0]_INST_0_i_70_n_3\,
      O => \disp[2][0]_INST_0_i_132_n_0\
    );
\disp[2][0]_INST_0_i_133\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_189_n_0\,
      CO(3) => \disp[2][0]_INST_0_i_133_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_133_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_133_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_133_n_3\,
      CYINIT => '0',
      DI(3) => \disp[2][0]_INST_0_i_190_n_0\,
      DI(2) => \disp[2][0]_INST_0_i_191_n_0\,
      DI(1) => \disp[2][0]_INST_0_i_192_n_0\,
      DI(0) => \disp[2][0]_INST_0_i_193_n_0\,
      O(3) => \disp[2][0]_INST_0_i_133_n_4\,
      O(2) => \disp[2][0]_INST_0_i_133_n_5\,
      O(1) => \disp[2][0]_INST_0_i_133_n_6\,
      O(0) => \disp[2][0]_INST_0_i_133_n_7\,
      S(3) => \disp[2][0]_INST_0_i_194_n_0\,
      S(2) => \disp[2][0]_INST_0_i_195_n_0\,
      S(1) => \disp[2][0]_INST_0_i_196_n_0\,
      S(0) => \disp[2][0]_INST_0_i_197_n_0\
    );
\disp[2][0]_INST_0_i_134\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_out(6),
      I1 => \disp[1]_OBUF\(1),
      I2 => \disp[5][0]_INST_0_i_91_n_0\,
      O => \disp[2][0]_INST_0_i_134_n_0\
    );
\disp[2][0]_INST_0_i_135\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \inst_int_to_string/cent\(6),
      O => \disp[2][0]_INST_0_i_135_n_0\
    );
\disp[2][0]_INST_0_i_136\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \inst_int_to_string/cent\(6),
      I1 => \disp[2][3]_INST_0_i_4_n_0\,
      O => \disp[2][0]_INST_0_i_136_n_0\
    );
\disp[2][0]_INST_0_i_137\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \inst_int_to_string/cent\(5),
      I1 => \disp[2][0]_INST_0_i_10_n_0\,
      O => \disp[2][0]_INST_0_i_137_n_0\
    );
\disp[2][0]_INST_0_i_138\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \inst_int_to_string/cent\(6),
      O => \disp[2][0]_INST_0_i_138_n_0\
    );
\disp[2][0]_INST_0_i_139\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \inst_int_to_string/cent\(5),
      I1 => \inst_int_to_string/cent\(6),
      O => \disp[2][0]_INST_0_i_139_n_0\
    );
\disp[2][0]_INST_0_i_14\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_20_n_0\,
      CO(3) => \disp[2][0]_INST_0_i_14_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_14_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_14_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_14_n_3\,
      CYINIT => '0',
      DI(3) => \disp[2][0]_INST_0_i_21_n_0\,
      DI(2) => \disp[2][0]_INST_0_i_22_n_0\,
      DI(1) => \disp[2][0]_INST_0_i_23_n_0\,
      DI(0) => \disp[2][0]_INST_0_i_24_n_0\,
      O(3) => \disp[2][0]_INST_0_i_14_n_4\,
      O(2 downto 0) => \NLW_disp[2][0]_INST_0_i_14_O_UNCONNECTED\(2 downto 0),
      S(3) => \disp[2][0]_INST_0_i_25_n_0\,
      S(2) => \disp[2][0]_INST_0_i_26_n_0\,
      S(1) => \disp[2][0]_INST_0_i_27_n_0\,
      S(0) => \disp[2][0]_INST_0_i_28_n_0\
    );
\disp[2][0]_INST_0_i_140\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B4"
    )
        port map (
      I0 => \inst_int_to_string/cent\(6),
      I1 => \disp[2][3]_INST_0_i_4_n_0\,
      I2 => \inst_int_to_string/cent\(5),
      O => \disp[2][0]_INST_0_i_140_n_0\
    );
\disp[2][0]_INST_0_i_141\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"D22D"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_10_n_0\,
      I1 => \inst_int_to_string/cent\(5),
      I2 => \disp[2][3]_INST_0_i_4_n_0\,
      I3 => \inst_int_to_string/cent\(6),
      O => \disp[2][0]_INST_0_i_141_n_0\
    );
\disp[2][0]_INST_0_i_142\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8E"
    )
        port map (
      I0 => \inst_int_to_string/cent\(5),
      I1 => \disp[2][0]_INST_0_i_11_n_0\,
      I2 => \disp[3][0]_INST_0_i_5_n_0\,
      O => \disp[2][0]_INST_0_i_142_n_0\
    );
\disp[2][0]_INST_0_i_143\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \inst_int_to_string/cent\(5),
      I1 => \disp[2][0]_INST_0_i_11_n_0\,
      I2 => \disp[3][0]_INST_0_i_5_n_0\,
      O => \disp[2][0]_INST_0_i_143_n_0\
    );
\disp[2][0]_INST_0_i_144\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_5_n_0\,
      I1 => \disp[2][0]_INST_0_i_10_n_0\,
      O => \disp[2][0]_INST_0_i_144_n_0\
    );
\disp[2][0]_INST_0_i_145\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_142_n_0\,
      I1 => \disp[2][0]_INST_0_i_10_n_0\,
      I2 => \disp[2][0]_INST_0_i_12_n_0\,
      I3 => \inst_int_to_string/cent\(6),
      O => \disp[2][0]_INST_0_i_145_n_0\
    );
\disp[2][0]_INST_0_i_146\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69966969"
    )
        port map (
      I0 => \inst_int_to_string/cent\(5),
      I1 => \disp[2][0]_INST_0_i_11_n_0\,
      I2 => \disp[3][0]_INST_0_i_5_n_0\,
      I3 => \disp[2][3]_INST_0_i_4_n_0\,
      I4 => \disp[2][0]_INST_0_i_12_n_0\,
      O => \disp[2][0]_INST_0_i_146_n_0\
    );
\disp[2][0]_INST_0_i_147\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_10_n_0\,
      I1 => \disp[3][0]_INST_0_i_5_n_0\,
      I2 => \disp[2][0]_INST_0_i_12_n_0\,
      I3 => \disp[2][3]_INST_0_i_4_n_0\,
      O => \disp[2][0]_INST_0_i_147_n_0\
    );
\disp[2][0]_INST_0_i_148\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_10_n_0\,
      I1 => \disp[3][0]_INST_0_i_5_n_0\,
      O => \disp[2][0]_INST_0_i_148_n_0\
    );
\disp[2][0]_INST_0_i_149\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \disp[2][0]_INST_0_i_149_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_149_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_149_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_149_n_3\,
      CYINIT => '0',
      DI(3) => \disp[2][0]_INST_0_i_142_n_0\,
      DI(2) => \disp[2][0]_INST_0_i_198_n_0\,
      DI(1) => \disp[2][0]_INST_0_i_199_n_0\,
      DI(0) => '0',
      O(3) => \disp[2][0]_INST_0_i_149_n_4\,
      O(2) => \disp[2][0]_INST_0_i_149_n_5\,
      O(1) => \disp[2][0]_INST_0_i_149_n_6\,
      O(0) => \disp[2][0]_INST_0_i_149_n_7\,
      S(3) => \disp[2][0]_INST_0_i_200_n_0\,
      S(2) => \disp[2][0]_INST_0_i_201_n_0\,
      S(1) => \disp[2][0]_INST_0_i_202_n_0\,
      S(0) => \disp[2][0]_INST_0_i_203_n_0\
    );
\disp[2][0]_INST_0_i_15\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \disp[2][0]_INST_0_i_15_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_15_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_15_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_15_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => total_out(3 downto 0),
      O(3) => \disp[2][0]_INST_0_i_15_n_4\,
      O(2) => \disp[2][0]_INST_0_i_15_n_5\,
      O(1 downto 0) => \inst_int_to_string/p_1_in\(1 downto 0),
      S(3) => \disp[2][0]_INST_0_i_29_n_0\,
      S(2) => \disp[2][0]_INST_0_i_30_n_0\,
      S(1) => \disp[2][0]_INST_0_i_31_n_0\,
      S(0) => \disp[2][0]_INST_0_i_32_n_0\
    );
\disp[2][0]_INST_0_i_150\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[2][3]_INST_0_i_4_n_0\,
      I1 => \inst_int_to_string/cent\(6),
      O => \disp[2][0]_INST_0_i_150_n_0\
    );
\disp[2][0]_INST_0_i_151\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_10_n_0\,
      I1 => \inst_int_to_string/cent\(5),
      O => \disp[2][0]_INST_0_i_151_n_0\
    );
\disp[2][0]_INST_0_i_152\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \disp[2][3]_INST_0_i_4_n_0\,
      I1 => \disp[2][0]_INST_0_i_11_n_0\,
      O => \disp[2][0]_INST_0_i_152_n_0\
    );
\disp[2][0]_INST_0_i_153\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_10_n_0\,
      I1 => \disp[2][0]_INST_0_i_12_n_0\,
      I2 => \inst_int_to_string/cent\(6),
      O => \disp[2][0]_INST_0_i_153_n_0\
    );
\disp[2][0]_INST_0_i_154\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1E"
    )
        port map (
      I0 => \inst_int_to_string/cent\(6),
      I1 => \disp[2][3]_INST_0_i_4_n_0\,
      I2 => \inst_int_to_string/cent\(5),
      O => \disp[2][0]_INST_0_i_154_n_0\
    );
\disp[2][0]_INST_0_i_155\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1EE1"
    )
        port map (
      I0 => \inst_int_to_string/cent\(5),
      I1 => \disp[2][0]_INST_0_i_10_n_0\,
      I2 => \inst_int_to_string/cent\(6),
      I3 => \disp[2][3]_INST_0_i_4_n_0\,
      O => \disp[2][0]_INST_0_i_155_n_0\
    );
\disp[2][0]_INST_0_i_156\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4BB4"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_11_n_0\,
      I1 => \disp[2][3]_INST_0_i_4_n_0\,
      I2 => \inst_int_to_string/cent\(5),
      I3 => \disp[2][0]_INST_0_i_10_n_0\,
      O => \disp[2][0]_INST_0_i_156_n_0\
    );
\disp[2][0]_INST_0_i_157\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4DB2B24D"
    )
        port map (
      I0 => \inst_int_to_string/cent\(6),
      I1 => \disp[2][0]_INST_0_i_12_n_0\,
      I2 => \disp[2][0]_INST_0_i_10_n_0\,
      I3 => \disp[2][0]_INST_0_i_11_n_0\,
      I4 => \disp[2][3]_INST_0_i_4_n_0\,
      O => \disp[2][0]_INST_0_i_157_n_0\
    );
\disp[2][0]_INST_0_i_158\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_204_n_0\,
      CO(3) => \disp[2][0]_INST_0_i_158_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_158_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_158_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_158_n_3\,
      CYINIT => '0',
      DI(3) => \disp[2][0]_INST_0_i_205_n_0\,
      DI(2) => \disp[2][0]_INST_0_i_206_n_0\,
      DI(1) => \disp[2][0]_INST_0_i_207_n_0\,
      DI(0) => \disp[2][0]_INST_0_i_208_n_0\,
      O(3 downto 0) => \NLW_disp[2][0]_INST_0_i_158_O_UNCONNECTED\(3 downto 0),
      S(3) => \disp[2][0]_INST_0_i_209_n_0\,
      S(2) => \disp[2][0]_INST_0_i_210_n_0\,
      S(1) => \disp[2][0]_INST_0_i_211_n_0\,
      S(0) => \disp[2][0]_INST_0_i_212_n_0\
    );
\disp[2][0]_INST_0_i_159\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_213_n_4\,
      I1 => \disp[2][0]_INST_0_i_169_n_7\,
      I2 => \disp[2][0]_INST_0_i_168_n_0\,
      I3 => \disp[2][0]_INST_0_i_167_n_3\,
      O => \disp[2][0]_INST_0_i_159_n_0\
    );
\disp[2][0]_INST_0_i_16\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000005557FFFF"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_93_n_6\,
      I1 => \disp[5][0]_INST_0_i_93_n_7\,
      I2 => \disp[2][0]_INST_0_i_15_n_4\,
      I3 => \disp[2][0]_INST_0_i_15_n_5\,
      I4 => \disp[5][0]_INST_0_i_93_n_5\,
      I5 => \disp[5][0]_INST_0_i_93_n_4\,
      O => \disp[2][0]_INST_0_i_16_n_0\
    );
\disp[2][0]_INST_0_i_160\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_213_n_5\,
      I1 => \disp[2][0]_INST_0_i_213_n_4\,
      I2 => \disp[2][0]_INST_0_i_168_n_0\,
      I3 => \disp[2][0]_INST_0_i_167_n_3\,
      O => \disp[2][0]_INST_0_i_160_n_0\
    );
\disp[2][0]_INST_0_i_161\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_213_n_6\,
      I1 => \disp[2][0]_INST_0_i_213_n_5\,
      I2 => \disp[2][0]_INST_0_i_168_n_0\,
      I3 => \disp[2][0]_INST_0_i_167_n_3\,
      O => \disp[2][0]_INST_0_i_161_n_0\
    );
\disp[2][0]_INST_0_i_162\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"90660060"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_213_n_6\,
      I1 => \disp[2][0]_INST_0_i_168_n_0\,
      I2 => \disp[2][0]_INST_0_i_213_n_7\,
      I3 => \disp[2][0]_INST_0_i_167_n_3\,
      I4 => \disp[2][0]_INST_0_i_168_n_5\,
      O => \disp[2][0]_INST_0_i_162_n_0\
    );
\disp[2][0]_INST_0_i_163\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0E1E1C3"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_213_n_4\,
      I1 => \disp[2][0]_INST_0_i_169_n_7\,
      I2 => \disp[2][0]_INST_0_i_169_n_6\,
      I3 => \disp[2][0]_INST_0_i_168_n_0\,
      I4 => \disp[2][0]_INST_0_i_167_n_3\,
      O => \disp[2][0]_INST_0_i_163_n_0\
    );
\disp[2][0]_INST_0_i_164\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0E1E1C3"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_213_n_5\,
      I1 => \disp[2][0]_INST_0_i_213_n_4\,
      I2 => \disp[2][0]_INST_0_i_169_n_7\,
      I3 => \disp[2][0]_INST_0_i_168_n_0\,
      I4 => \disp[2][0]_INST_0_i_167_n_3\,
      O => \disp[2][0]_INST_0_i_164_n_0\
    );
\disp[2][0]_INST_0_i_165\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0E1E1C3"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_213_n_6\,
      I1 => \disp[2][0]_INST_0_i_213_n_5\,
      I2 => \disp[2][0]_INST_0_i_213_n_4\,
      I3 => \disp[2][0]_INST_0_i_168_n_0\,
      I4 => \disp[2][0]_INST_0_i_167_n_3\,
      O => \disp[2][0]_INST_0_i_165_n_0\
    );
\disp[2][0]_INST_0_i_166\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7F80F807FE01E01F"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_168_n_5\,
      I1 => \disp[2][0]_INST_0_i_213_n_7\,
      I2 => \disp[2][0]_INST_0_i_213_n_6\,
      I3 => \disp[2][0]_INST_0_i_213_n_5\,
      I4 => \disp[2][0]_INST_0_i_168_n_0\,
      I5 => \disp[2][0]_INST_0_i_167_n_3\,
      O => \disp[2][0]_INST_0_i_166_n_0\
    );
\disp[2][0]_INST_0_i_167\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_214_n_0\,
      CO(3 downto 1) => \NLW_disp[2][0]_INST_0_i_167_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \disp[2][0]_INST_0_i_167_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_disp[2][0]_INST_0_i_167_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
\disp[2][0]_INST_0_i_168\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_215_n_0\,
      CO(3) => \disp[2][0]_INST_0_i_168_n_0\,
      CO(2) => \NLW_disp[2][0]_INST_0_i_168_CO_UNCONNECTED\(2),
      CO(1) => \disp[2][0]_INST_0_i_168_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_168_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => total_out(7 downto 5),
      O(3) => \NLW_disp[2][0]_INST_0_i_168_O_UNCONNECTED\(3),
      O(2) => \disp[2][0]_INST_0_i_168_n_5\,
      O(1) => \disp[2][0]_INST_0_i_168_n_6\,
      O(0) => \disp[2][0]_INST_0_i_168_n_7\,
      S(3) => '1',
      S(2) => \disp[2][0]_INST_0_i_216_n_0\,
      S(1) => \disp[2][0]_INST_0_i_217_n_0\,
      S(0) => \disp[2][0]_INST_0_i_218_n_0\
    );
\disp[2][0]_INST_0_i_169\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_213_n_0\,
      CO(3) => \disp[2][0]_INST_0_i_169_n_0\,
      CO(2) => \NLW_disp[2][0]_INST_0_i_169_CO_UNCONNECTED\(2),
      CO(1) => \disp[2][0]_INST_0_i_169_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_169_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => total_out(6),
      DI(0) => \disp[2][0]_INST_0_i_219_n_0\,
      O(3) => \NLW_disp[2][0]_INST_0_i_169_O_UNCONNECTED\(3),
      O(2) => \disp[2][0]_INST_0_i_169_n_5\,
      O(1) => \disp[2][0]_INST_0_i_169_n_6\,
      O(0) => \disp[2][0]_INST_0_i_169_n_7\,
      S(3) => '1',
      S(2) => total_out(7),
      S(1) => \disp[2][0]_INST_0_i_220_n_0\,
      S(0) => \disp[2][0]_INST_0_i_221_n_0\
    );
\disp[2][0]_INST_0_i_17\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_14_n_0\,
      CO(3 downto 2) => \NLW_disp[2][0]_INST_0_i_17_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \disp[2][0]_INST_0_i_17_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_17_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \disp[2][0]_INST_0_i_33_n_0\,
      DI(0) => \disp[2][0]_INST_0_i_34_n_0\,
      O(3) => \NLW_disp[2][0]_INST_0_i_17_O_UNCONNECTED\(3),
      O(2) => \disp[2][0]_INST_0_i_17_n_5\,
      O(1) => \disp[2][0]_INST_0_i_17_n_6\,
      O(0) => \disp[2][0]_INST_0_i_17_n_7\,
      S(3) => '0',
      S(2) => \disp[2][0]_INST_0_i_35_n_0\,
      S(1) => \disp[2][0]_INST_0_i_36_n_0\,
      S(0) => \disp[2][0]_INST_0_i_37_n_0\
    );
\disp[2][0]_INST_0_i_170\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \disp[2][0]_INST_0_i_170_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_170_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_170_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_170_n_3\,
      CYINIT => '0',
      DI(3) => \disp[2][0]_INST_0_i_222_n_0\,
      DI(2) => \disp[2][0]_INST_0_i_223_n_0\,
      DI(1) => \disp[2][0]_INST_0_i_224_n_0\,
      DI(0) => \disp[2][0]_INST_0_i_225_n_0\,
      O(3 downto 0) => \NLW_disp[2][0]_INST_0_i_170_O_UNCONNECTED\(3 downto 0),
      S(3) => \disp[2][0]_INST_0_i_226_n_0\,
      S(2) => \disp[2][0]_INST_0_i_227_n_0\,
      S(1) => \disp[2][0]_INST_0_i_228_n_0\,
      S(0) => \disp[2][0]_INST_0_i_229_n_0\
    );
\disp[2][0]_INST_0_i_171\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"82"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_92_n_7\,
      I1 => \disp[2][0]_INST_0_i_180_n_7\,
      I2 => \disp[2][0]_INST_0_i_70_n_3\,
      O => \disp[2][0]_INST_0_i_171_n_0\
    );
\disp[2][0]_INST_0_i_172\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_149_n_4\,
      I1 => \disp[2][0]_INST_0_i_133_n_4\,
      O => \disp[2][0]_INST_0_i_172_n_0\
    );
\disp[2][0]_INST_0_i_173\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_149_n_5\,
      I1 => \disp[2][0]_INST_0_i_133_n_5\,
      O => \disp[2][0]_INST_0_i_173_n_0\
    );
\disp[2][0]_INST_0_i_174\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_149_n_6\,
      I1 => \disp[2][0]_INST_0_i_133_n_6\,
      O => \disp[2][0]_INST_0_i_174_n_0\
    );
\disp[2][0]_INST_0_i_175\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99966669"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_171_n_0\,
      I1 => \disp[2][0]_INST_0_i_92_n_6\,
      I2 => \disp[2][0]_INST_0_i_70_n_3\,
      I3 => \disp[2][0]_INST_0_i_180_n_7\,
      I4 => \disp[2][0]_INST_0_i_179_n_6\,
      O => \disp[2][0]_INST_0_i_175_n_0\
    );
\disp[2][0]_INST_0_i_176\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_92_n_7\,
      I1 => \disp[2][0]_INST_0_i_180_n_7\,
      I2 => \disp[2][0]_INST_0_i_70_n_3\,
      I3 => \disp[2][0]_INST_0_i_172_n_0\,
      O => \disp[2][0]_INST_0_i_176_n_0\
    );
\disp[2][0]_INST_0_i_177\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_149_n_4\,
      I1 => \disp[2][0]_INST_0_i_133_n_4\,
      I2 => \disp[2][0]_INST_0_i_133_n_5\,
      I3 => \disp[2][0]_INST_0_i_149_n_5\,
      O => \disp[2][0]_INST_0_i_177_n_0\
    );
\disp[2][0]_INST_0_i_178\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_133_n_6\,
      I1 => \disp[2][0]_INST_0_i_149_n_6\,
      I2 => \disp[2][0]_INST_0_i_133_n_5\,
      I3 => \disp[2][0]_INST_0_i_149_n_5\,
      O => \disp[2][0]_INST_0_i_178_n_0\
    );
\disp[2][0]_INST_0_i_179\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \disp[2][0]_INST_0_i_179_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_179_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_179_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_179_n_3\,
      CYINIT => '0',
      DI(3) => \disp[2][0]_INST_0_i_230_n_0\,
      DI(2) => \disp[2][0]_INST_0_i_231_n_0\,
      DI(1 downto 0) => B"01",
      O(3) => \disp[2][0]_INST_0_i_179_n_4\,
      O(2) => \disp[2][0]_INST_0_i_179_n_5\,
      O(1) => \disp[2][0]_INST_0_i_179_n_6\,
      O(0) => \NLW_disp[2][0]_INST_0_i_179_O_UNCONNECTED\(0),
      S(3) => \disp[2][0]_INST_0_i_232_n_0\,
      S(2) => \disp[2][0]_INST_0_i_233_n_0\,
      S(1) => \disp[2][0]_INST_0_i_234_n_0\,
      S(0) => \disp[2][0]_INST_0_i_235_n_0\
    );
\disp[2][0]_INST_0_i_18\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_17_n_7\,
      I1 => \disp[2][0]_INST_0_i_17_n_5\,
      O => \disp[2][0]_INST_0_i_18_n_0\
    );
\disp[2][0]_INST_0_i_180\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \disp[2][0]_INST_0_i_180_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_180_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_180_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_180_n_3\,
      CYINIT => '0',
      DI(3) => \disp[2][0]_INST_0_i_236_n_0\,
      DI(2) => \disp[2][0]_INST_0_i_237_n_0\,
      DI(1 downto 0) => B"01",
      O(3 downto 1) => \NLW_disp[2][0]_INST_0_i_180_O_UNCONNECTED\(3 downto 1),
      O(0) => \disp[2][0]_INST_0_i_180_n_7\,
      S(3) => \disp[2][0]_INST_0_i_238_n_0\,
      S(2) => \disp[2][0]_INST_0_i_239_n_0\,
      S(1) => \disp[2][0]_INST_0_i_240_n_0\,
      S(0) => \disp[2][0]_INST_0_i_241_n_0\
    );
\disp[2][0]_INST_0_i_181\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_10_n_0\,
      I1 => \inst_int_to_string/cent\(5),
      O => \disp[2][0]_INST_0_i_181_n_0\
    );
\disp[2][0]_INST_0_i_182\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_10_n_0\,
      I1 => \inst_int_to_string/cent\(5),
      I2 => \disp[2][0]_INST_0_i_12_n_0\,
      O => \disp[2][0]_INST_0_i_182_n_0\
    );
\disp[2][0]_INST_0_i_183\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_12_n_0\,
      I1 => \inst_int_to_string/cent\(5),
      I2 => \disp[2][0]_INST_0_i_10_n_0\,
      O => \disp[2][0]_INST_0_i_183_n_0\
    );
\disp[2][0]_INST_0_i_184\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_11_n_0\,
      O => \disp[2][0]_INST_0_i_184_n_0\
    );
\disp[2][0]_INST_0_i_185\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"17E8E817"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_11_n_0\,
      I1 => \inst_int_to_string/cent\(6),
      I2 => \disp[2][3]_INST_0_i_4_n_0\,
      I3 => \inst_int_to_string/cent\(5),
      I4 => \disp[2][0]_INST_0_i_10_n_0\,
      O => \disp[2][0]_INST_0_i_185_n_0\
    );
\disp[2][0]_INST_0_i_186\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_12_n_0\,
      I1 => \inst_int_to_string/cent\(5),
      I2 => \disp[2][0]_INST_0_i_10_n_0\,
      I3 => \disp[2][3]_INST_0_i_4_n_0\,
      I4 => \inst_int_to_string/cent\(6),
      I5 => \disp[2][0]_INST_0_i_11_n_0\,
      O => \disp[2][0]_INST_0_i_186_n_0\
    );
\disp[2][0]_INST_0_i_187\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69966969"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_10_n_0\,
      I1 => \inst_int_to_string/cent\(5),
      I2 => \disp[2][0]_INST_0_i_12_n_0\,
      I3 => \disp[3][0]_INST_0_i_5_n_0\,
      I4 => \disp[2][3]_INST_0_i_4_n_0\,
      O => \disp[2][0]_INST_0_i_187_n_0\
    );
\disp[2][0]_INST_0_i_188\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_5_n_0\,
      I1 => \disp[2][3]_INST_0_i_4_n_0\,
      I2 => \disp[2][0]_INST_0_i_11_n_0\,
      O => \disp[2][0]_INST_0_i_188_n_0\
    );
\disp[2][0]_INST_0_i_189\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_180_n_0\,
      CO(3) => \disp[2][0]_INST_0_i_189_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_189_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_189_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_189_n_3\,
      CYINIT => '0',
      DI(3) => \disp[2][0]_INST_0_i_242_n_0\,
      DI(2) => \disp[2][0]_INST_0_i_182_n_0\,
      DI(1) => \disp[2][0]_INST_0_i_243_n_0\,
      DI(0) => \disp[2][0]_INST_0_i_244_n_0\,
      O(3) => \disp[2][0]_INST_0_i_189_n_4\,
      O(2) => \disp[2][0]_INST_0_i_189_n_5\,
      O(1) => \disp[2][0]_INST_0_i_189_n_6\,
      O(0) => \NLW_disp[2][0]_INST_0_i_189_O_UNCONNECTED\(0),
      S(3) => \disp[2][0]_INST_0_i_245_n_0\,
      S(2) => \disp[2][0]_INST_0_i_246_n_0\,
      S(1) => \disp[2][0]_INST_0_i_247_n_0\,
      S(0) => \disp[2][0]_INST_0_i_248_n_0\
    );
\disp[2][0]_INST_0_i_19\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_17_n_6\,
      I1 => \disp[2][0]_INST_0_i_14_n_4\,
      O => \disp[2][0]_INST_0_i_19_n_0\
    );
\disp[2][0]_INST_0_i_190\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_out(6),
      I1 => \disp[1]_OBUF\(1),
      I2 => \disp[5][0]_INST_0_i_91_n_0\,
      O => \disp[2][0]_INST_0_i_190_n_0\
    );
\disp[2][0]_INST_0_i_191\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \inst_int_to_string/cent\(6),
      O => \disp[2][0]_INST_0_i_191_n_0\
    );
\disp[2][0]_INST_0_i_192\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \inst_int_to_string/cent\(6),
      I1 => \disp[2][3]_INST_0_i_4_n_0\,
      O => \disp[2][0]_INST_0_i_192_n_0\
    );
\disp[2][0]_INST_0_i_193\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \inst_int_to_string/cent\(5),
      I1 => \disp[2][0]_INST_0_i_10_n_0\,
      O => \disp[2][0]_INST_0_i_193_n_0\
    );
\disp[2][0]_INST_0_i_194\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \inst_int_to_string/cent\(6),
      O => \disp[2][0]_INST_0_i_194_n_0\
    );
\disp[2][0]_INST_0_i_195\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \inst_int_to_string/cent\(5),
      I1 => \inst_int_to_string/cent\(6),
      O => \disp[2][0]_INST_0_i_195_n_0\
    );
\disp[2][0]_INST_0_i_196\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B4"
    )
        port map (
      I0 => \inst_int_to_string/cent\(6),
      I1 => \disp[2][3]_INST_0_i_4_n_0\,
      I2 => \inst_int_to_string/cent\(5),
      O => \disp[2][0]_INST_0_i_196_n_0\
    );
\disp[2][0]_INST_0_i_197\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"D22D"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_10_n_0\,
      I1 => \inst_int_to_string/cent\(5),
      I2 => \disp[2][3]_INST_0_i_4_n_0\,
      I3 => \inst_int_to_string/cent\(6),
      O => \disp[2][0]_INST_0_i_197_n_0\
    );
\disp[2][0]_INST_0_i_198\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \inst_int_to_string/cent\(5),
      I1 => \disp[2][0]_INST_0_i_11_n_0\,
      I2 => \disp[3][0]_INST_0_i_5_n_0\,
      O => \disp[2][0]_INST_0_i_198_n_0\
    );
\disp[2][0]_INST_0_i_199\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_5_n_0\,
      I1 => \disp[2][0]_INST_0_i_10_n_0\,
      O => \disp[2][0]_INST_0_i_199_n_0\
    );
\disp[2][0]_INST_0_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_10_n_0\,
      O => \disp[2][0]_INST_0_i_2_n_0\
    );
\disp[2][0]_INST_0_i_20\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_38_n_0\,
      CO(3) => \disp[2][0]_INST_0_i_20_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_20_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_20_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_20_n_3\,
      CYINIT => '0',
      DI(3) => \disp[2][0]_INST_0_i_39_n_0\,
      DI(2) => \disp[2][0]_INST_0_i_40_n_0\,
      DI(1) => \disp[2][0]_INST_0_i_41_n_0\,
      DI(0) => \disp[2][0]_INST_0_i_42_n_0\,
      O(3 downto 0) => \NLW_disp[2][0]_INST_0_i_20_O_UNCONNECTED\(3 downto 0),
      S(3) => \disp[2][0]_INST_0_i_43_n_0\,
      S(2) => \disp[2][0]_INST_0_i_44_n_0\,
      S(1) => \disp[2][0]_INST_0_i_45_n_0\,
      S(0) => \disp[2][0]_INST_0_i_46_n_0\
    );
\disp[2][0]_INST_0_i_200\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_142_n_0\,
      I1 => \disp[2][0]_INST_0_i_10_n_0\,
      I2 => \disp[2][0]_INST_0_i_12_n_0\,
      I3 => \inst_int_to_string/cent\(6),
      O => \disp[2][0]_INST_0_i_200_n_0\
    );
\disp[2][0]_INST_0_i_201\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69966969"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_5_n_0\,
      I1 => \disp[2][0]_INST_0_i_11_n_0\,
      I2 => \inst_int_to_string/cent\(5),
      I3 => \disp[2][3]_INST_0_i_4_n_0\,
      I4 => \disp[2][0]_INST_0_i_12_n_0\,
      O => \disp[2][0]_INST_0_i_201_n_0\
    );
\disp[2][0]_INST_0_i_202\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_10_n_0\,
      I1 => \disp[3][0]_INST_0_i_5_n_0\,
      I2 => \disp[2][0]_INST_0_i_12_n_0\,
      I3 => \disp[2][3]_INST_0_i_4_n_0\,
      O => \disp[2][0]_INST_0_i_202_n_0\
    );
\disp[2][0]_INST_0_i_203\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_10_n_0\,
      I1 => \disp[3][0]_INST_0_i_5_n_0\,
      O => \disp[2][0]_INST_0_i_203_n_0\
    );
\disp[2][0]_INST_0_i_204\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_249_n_0\,
      CO(3) => \disp[2][0]_INST_0_i_204_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_204_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_204_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_204_n_3\,
      CYINIT => '0',
      DI(3) => \disp[2][0]_INST_0_i_250_n_0\,
      DI(2) => \disp[2][0]_INST_0_i_251_n_0\,
      DI(1) => \disp[2][0]_INST_0_i_252_n_0\,
      DI(0) => \disp[2][0]_INST_0_i_253_n_0\,
      O(3 downto 0) => \NLW_disp[2][0]_INST_0_i_204_O_UNCONNECTED\(3 downto 0),
      S(3) => \disp[2][0]_INST_0_i_254_n_0\,
      S(2) => \disp[2][0]_INST_0_i_255_n_0\,
      S(1) => \disp[2][0]_INST_0_i_256_n_0\,
      S(0) => \disp[2][0]_INST_0_i_257_n_0\
    );
\disp[2][0]_INST_0_i_205\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60990090"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_168_n_5\,
      I1 => \disp[2][0]_INST_0_i_213_n_7\,
      I2 => \disp[2][0]_INST_0_i_258_n_4\,
      I3 => \disp[2][0]_INST_0_i_167_n_3\,
      I4 => \disp[2][0]_INST_0_i_168_n_6\,
      O => \disp[2][0]_INST_0_i_205_n_0\
    );
\disp[2][0]_INST_0_i_206\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60990090"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_168_n_6\,
      I1 => \disp[2][0]_INST_0_i_258_n_4\,
      I2 => \disp[2][0]_INST_0_i_258_n_5\,
      I3 => \disp[2][0]_INST_0_i_167_n_3\,
      I4 => \disp[2][0]_INST_0_i_168_n_7\,
      O => \disp[2][0]_INST_0_i_206_n_0\
    );
\disp[2][0]_INST_0_i_207\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60990090"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_168_n_7\,
      I1 => \disp[2][0]_INST_0_i_258_n_5\,
      I2 => \disp[2][0]_INST_0_i_258_n_6\,
      I3 => \disp[2][0]_INST_0_i_167_n_3\,
      I4 => \disp[2][0]_INST_0_i_215_n_4\,
      O => \disp[2][0]_INST_0_i_207_n_0\
    );
\disp[2][0]_INST_0_i_208\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E8E800E80000E8"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_215_n_5\,
      I1 => \disp[2][0]_INST_0_i_214_n_4\,
      I2 => \disp[2][0]_INST_0_i_258_n_7\,
      I3 => \disp[2][0]_INST_0_i_215_n_4\,
      I4 => \disp[2][0]_INST_0_i_167_n_3\,
      I5 => \disp[2][0]_INST_0_i_258_n_6\,
      O => \disp[2][0]_INST_0_i_208_n_0\
    );
\disp[2][0]_INST_0_i_209\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"95566AA96AA99556"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_205_n_0\,
      I1 => \disp[2][0]_INST_0_i_168_n_5\,
      I2 => \disp[2][0]_INST_0_i_167_n_3\,
      I3 => \disp[2][0]_INST_0_i_213_n_7\,
      I4 => \disp[2][0]_INST_0_i_168_n_0\,
      I5 => \disp[2][0]_INST_0_i_213_n_6\,
      O => \disp[2][0]_INST_0_i_209_n_0\
    );
\disp[2][0]_INST_0_i_21\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"32B380FE80FE32B3"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_47_n_4\,
      I1 => \disp[2][0]_INST_0_i_48_n_3\,
      I2 => \disp[2][0]_INST_0_i_49_n_4\,
      I3 => \disp[2][0]_INST_0_i_50_n_1\,
      I4 => \disp[2][0]_INST_0_i_51_n_7\,
      I5 => \disp[2][0]_INST_0_i_52_n_7\,
      O => \disp[2][0]_INST_0_i_21_n_0\
    );
\disp[2][0]_INST_0_i_210\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AA9955695566AA9"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_206_n_0\,
      I1 => \disp[2][0]_INST_0_i_168_n_6\,
      I2 => \disp[2][0]_INST_0_i_167_n_3\,
      I3 => \disp[2][0]_INST_0_i_258_n_4\,
      I4 => \disp[2][0]_INST_0_i_213_n_7\,
      I5 => \disp[2][0]_INST_0_i_168_n_5\,
      O => \disp[2][0]_INST_0_i_210_n_0\
    );
\disp[2][0]_INST_0_i_211\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AA9955695566AA9"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_207_n_0\,
      I1 => \disp[2][0]_INST_0_i_168_n_7\,
      I2 => \disp[2][0]_INST_0_i_167_n_3\,
      I3 => \disp[2][0]_INST_0_i_258_n_5\,
      I4 => \disp[2][0]_INST_0_i_258_n_4\,
      I5 => \disp[2][0]_INST_0_i_168_n_6\,
      O => \disp[2][0]_INST_0_i_211_n_0\
    );
\disp[2][0]_INST_0_i_212\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AA9955695566AA9"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_208_n_0\,
      I1 => \disp[2][0]_INST_0_i_215_n_4\,
      I2 => \disp[2][0]_INST_0_i_167_n_3\,
      I3 => \disp[2][0]_INST_0_i_258_n_6\,
      I4 => \disp[2][0]_INST_0_i_258_n_5\,
      I5 => \disp[2][0]_INST_0_i_168_n_7\,
      O => \disp[2][0]_INST_0_i_212_n_0\
    );
\disp[2][0]_INST_0_i_213\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_258_n_0\,
      CO(3) => \disp[2][0]_INST_0_i_213_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_213_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_213_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_213_n_3\,
      CYINIT => '0',
      DI(3) => \disp[2][0]_INST_0_i_259_n_0\,
      DI(2) => \disp[2][0]_INST_0_i_260_n_0\,
      DI(1) => \disp[2][0]_INST_0_i_261_n_0\,
      DI(0) => \disp[2][0]_INST_0_i_262_n_0\,
      O(3) => \disp[2][0]_INST_0_i_213_n_4\,
      O(2) => \disp[2][0]_INST_0_i_213_n_5\,
      O(1) => \disp[2][0]_INST_0_i_213_n_6\,
      O(0) => \disp[2][0]_INST_0_i_213_n_7\,
      S(3) => \disp[2][0]_INST_0_i_263_n_0\,
      S(2) => \disp[2][0]_INST_0_i_264_n_0\,
      S(1) => \disp[2][0]_INST_0_i_265_n_0\,
      S(0) => \disp[2][0]_INST_0_i_266_n_0\
    );
\disp[2][0]_INST_0_i_214\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_267_n_0\,
      CO(3) => \disp[2][0]_INST_0_i_214_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_214_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_214_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_214_n_3\,
      CYINIT => '0',
      DI(3) => '1',
      DI(2) => total_out(6),
      DI(1) => \disp[2][0]_INST_0_i_268_n_0\,
      DI(0) => \disp[2][0]_INST_0_i_269_n_0\,
      O(3) => \disp[2][0]_INST_0_i_214_n_4\,
      O(2) => \disp[2][0]_INST_0_i_214_n_5\,
      O(1) => \disp[2][0]_INST_0_i_214_n_6\,
      O(0) => \disp[2][0]_INST_0_i_214_n_7\,
      S(3) => \disp[2][0]_INST_0_i_270_n_0\,
      S(2) => \disp[2][0]_INST_0_i_271_n_0\,
      S(1) => \disp[2][0]_INST_0_i_272_n_0\,
      S(0) => \disp[2][0]_INST_0_i_273_n_0\
    );
\disp[2][0]_INST_0_i_215\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_274_n_0\,
      CO(3) => \disp[2][0]_INST_0_i_215_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_215_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_215_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_215_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => total_out(4 downto 1),
      O(3) => \disp[2][0]_INST_0_i_215_n_4\,
      O(2) => \disp[2][0]_INST_0_i_215_n_5\,
      O(1) => \disp[2][0]_INST_0_i_215_n_6\,
      O(0) => \disp[2][0]_INST_0_i_215_n_7\,
      S(3) => \disp[2][0]_INST_0_i_275_n_0\,
      S(2) => \disp[2][0]_INST_0_i_276_n_0\,
      S(1) => \disp[2][0]_INST_0_i_277_n_0\,
      S(0) => \disp[2][0]_INST_0_i_278_n_0\
    );
\disp[2][0]_INST_0_i_216\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => total_out(7),
      O => \disp[2][0]_INST_0_i_216_n_0\
    );
\disp[2][0]_INST_0_i_217\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => total_out(6),
      O => \disp[2][0]_INST_0_i_217_n_0\
    );
\disp[2][0]_INST_0_i_218\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => total_out(5),
      O => \disp[2][0]_INST_0_i_218_n_0\
    );
\disp[2][0]_INST_0_i_219\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => total_out(6),
      I1 => total_out(4),
      O => \disp[2][0]_INST_0_i_219_n_0\
    );
\disp[2][0]_INST_0_i_22\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"32B380FE80FE32B3"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_47_n_5\,
      I1 => \disp[2][0]_INST_0_i_48_n_3\,
      I2 => \disp[2][0]_INST_0_i_49_n_5\,
      I3 => \disp[2][0]_INST_0_i_50_n_1\,
      I4 => \disp[2][0]_INST_0_i_47_n_4\,
      I5 => \disp[2][0]_INST_0_i_49_n_4\,
      O => \disp[2][0]_INST_0_i_22_n_0\
    );
\disp[2][0]_INST_0_i_220\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => total_out(7),
      I1 => total_out(5),
      I2 => total_out(6),
      O => \disp[2][0]_INST_0_i_220_n_0\
    );
\disp[2][0]_INST_0_i_221\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => total_out(4),
      I1 => total_out(6),
      I2 => total_out(7),
      I3 => total_out(5),
      O => \disp[2][0]_INST_0_i_221_n_0\
    );
\disp[2][0]_INST_0_i_222\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_149_n_7\,
      I1 => \disp[2][0]_INST_0_i_133_n_7\,
      O => \disp[2][0]_INST_0_i_222_n_0\
    );
\disp[2][0]_INST_0_i_223\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_189_n_4\,
      I1 => \disp[2][0]_INST_0_i_11_n_0\,
      O => \disp[2][0]_INST_0_i_223_n_0\
    );
\disp[2][0]_INST_0_i_224\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_189_n_5\,
      I1 => \disp[2][0]_INST_0_i_12_n_0\,
      O => \disp[2][0]_INST_0_i_224_n_0\
    );
\disp[2][0]_INST_0_i_225\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_189_n_6\,
      I1 => \disp[3][0]_INST_0_i_5_n_0\,
      O => \disp[2][0]_INST_0_i_225_n_0\
    );
\disp[2][0]_INST_0_i_226\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_133_n_7\,
      I1 => \disp[2][0]_INST_0_i_149_n_7\,
      I2 => \disp[2][0]_INST_0_i_133_n_6\,
      I3 => \disp[2][0]_INST_0_i_149_n_6\,
      O => \disp[2][0]_INST_0_i_226_n_0\
    );
\disp[2][0]_INST_0_i_227\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4BB4"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_11_n_0\,
      I1 => \disp[2][0]_INST_0_i_189_n_4\,
      I2 => \disp[2][0]_INST_0_i_133_n_7\,
      I3 => \disp[2][0]_INST_0_i_149_n_7\,
      O => \disp[2][0]_INST_0_i_227_n_0\
    );
\disp[2][0]_INST_0_i_228\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B44B"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_12_n_0\,
      I1 => \disp[2][0]_INST_0_i_189_n_5\,
      I2 => \disp[2][0]_INST_0_i_11_n_0\,
      I3 => \disp[2][0]_INST_0_i_189_n_4\,
      O => \disp[2][0]_INST_0_i_228_n_0\
    );
\disp[2][0]_INST_0_i_229\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B44B"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_5_n_0\,
      I1 => \disp[2][0]_INST_0_i_189_n_6\,
      I2 => \disp[2][0]_INST_0_i_12_n_0\,
      I3 => \disp[2][0]_INST_0_i_189_n_5\,
      O => \disp[2][0]_INST_0_i_229_n_0\
    );
\disp[2][0]_INST_0_i_23\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"32B380FE80FE32B3"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_47_n_6\,
      I1 => \disp[2][0]_INST_0_i_48_n_3\,
      I2 => \disp[2][0]_INST_0_i_49_n_6\,
      I3 => \disp[2][0]_INST_0_i_50_n_1\,
      I4 => \disp[2][0]_INST_0_i_47_n_5\,
      I5 => \disp[2][0]_INST_0_i_49_n_5\,
      O => \disp[2][0]_INST_0_i_23_n_0\
    );
\disp[2][0]_INST_0_i_230\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_12_n_0\,
      O => \disp[2][0]_INST_0_i_230_n_0\
    );
\disp[2][0]_INST_0_i_231\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_5_n_0\,
      O => \disp[2][0]_INST_0_i_231_n_0\
    );
\disp[2][0]_INST_0_i_232\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_12_n_0\,
      I1 => \disp[2][0]_INST_0_i_10_n_0\,
      O => \disp[2][0]_INST_0_i_232_n_0\
    );
\disp[2][0]_INST_0_i_233\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_5_n_0\,
      I1 => \disp[2][0]_INST_0_i_11_n_0\,
      O => \disp[2][0]_INST_0_i_233_n_0\
    );
\disp[2][0]_INST_0_i_234\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => total_out(1),
      I1 => \disp[1]_OBUF\(1),
      I2 => \inst_int_to_string/p_1_in\(1),
      O => \disp[2][0]_INST_0_i_234_n_0\
    );
\disp[2][0]_INST_0_i_235\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_5_n_0\,
      O => \disp[2][0]_INST_0_i_235_n_0\
    );
\disp[2][0]_INST_0_i_236\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_12_n_0\,
      O => \disp[2][0]_INST_0_i_236_n_0\
    );
\disp[2][0]_INST_0_i_237\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_5_n_0\,
      O => \disp[2][0]_INST_0_i_237_n_0\
    );
\disp[2][0]_INST_0_i_238\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_12_n_0\,
      I1 => \disp[2][0]_INST_0_i_10_n_0\,
      O => \disp[2][0]_INST_0_i_238_n_0\
    );
\disp[2][0]_INST_0_i_239\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_5_n_0\,
      I1 => \disp[2][0]_INST_0_i_11_n_0\,
      O => \disp[2][0]_INST_0_i_239_n_0\
    );
\disp[2][0]_INST_0_i_24\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"69FF217B217B0069"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_47_n_6\,
      I1 => \disp[2][0]_INST_0_i_48_n_3\,
      I2 => \disp[2][0]_INST_0_i_49_n_6\,
      I3 => \disp[2][0]_INST_0_i_50_n_1\,
      I4 => \disp[2][0]_INST_0_i_49_n_7\,
      I5 => \disp[2][0]_INST_0_i_47_n_7\,
      O => \disp[2][0]_INST_0_i_24_n_0\
    );
\disp[2][0]_INST_0_i_240\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => total_out(1),
      I1 => \disp[1]_OBUF\(1),
      I2 => \inst_int_to_string/p_1_in\(1),
      O => \disp[2][0]_INST_0_i_240_n_0\
    );
\disp[2][0]_INST_0_i_241\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_5_n_0\,
      O => \disp[2][0]_INST_0_i_241_n_0\
    );
\disp[2][0]_INST_0_i_242\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_10_n_0\,
      I1 => \inst_int_to_string/cent\(5),
      O => \disp[2][0]_INST_0_i_242_n_0\
    );
\disp[2][0]_INST_0_i_243\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_12_n_0\,
      I1 => \inst_int_to_string/cent\(5),
      I2 => \disp[2][0]_INST_0_i_10_n_0\,
      O => \disp[2][0]_INST_0_i_243_n_0\
    );
\disp[2][0]_INST_0_i_244\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_11_n_0\,
      O => \disp[2][0]_INST_0_i_244_n_0\
    );
\disp[2][0]_INST_0_i_245\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"17E8E817"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_11_n_0\,
      I1 => \inst_int_to_string/cent\(6),
      I2 => \disp[2][3]_INST_0_i_4_n_0\,
      I3 => \inst_int_to_string/cent\(5),
      I4 => \disp[2][0]_INST_0_i_10_n_0\,
      O => \disp[2][0]_INST_0_i_245_n_0\
    );
\disp[2][0]_INST_0_i_246\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_12_n_0\,
      I1 => \inst_int_to_string/cent\(5),
      I2 => \disp[2][0]_INST_0_i_10_n_0\,
      I3 => \disp[2][3]_INST_0_i_4_n_0\,
      I4 => \inst_int_to_string/cent\(6),
      I5 => \disp[2][0]_INST_0_i_11_n_0\,
      O => \disp[2][0]_INST_0_i_246_n_0\
    );
\disp[2][0]_INST_0_i_247\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69966969"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_10_n_0\,
      I1 => \inst_int_to_string/cent\(5),
      I2 => \disp[2][0]_INST_0_i_12_n_0\,
      I3 => \disp[3][0]_INST_0_i_5_n_0\,
      I4 => \disp[2][3]_INST_0_i_4_n_0\,
      O => \disp[2][0]_INST_0_i_247_n_0\
    );
\disp[2][0]_INST_0_i_248\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_5_n_0\,
      I1 => \disp[2][3]_INST_0_i_4_n_0\,
      I2 => \disp[2][0]_INST_0_i_11_n_0\,
      O => \disp[2][0]_INST_0_i_248_n_0\
    );
\disp[2][0]_INST_0_i_249\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_279_n_0\,
      CO(3) => \disp[2][0]_INST_0_i_249_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_249_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_249_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_249_n_3\,
      CYINIT => '0',
      DI(3) => \disp[2][0]_INST_0_i_280_n_0\,
      DI(2) => \disp[2][0]_INST_0_i_281_n_0\,
      DI(1) => \disp[2][0]_INST_0_i_282_n_0\,
      DI(0) => \disp[2][0]_INST_0_i_283_n_0\,
      O(3 downto 0) => \NLW_disp[2][0]_INST_0_i_249_O_UNCONNECTED\(3 downto 0),
      S(3) => \disp[2][0]_INST_0_i_284_n_0\,
      S(2) => \disp[2][0]_INST_0_i_285_n_0\,
      S(1) => \disp[2][0]_INST_0_i_286_n_0\,
      S(0) => \disp[2][0]_INST_0_i_287_n_0\
    );
\disp[2][0]_INST_0_i_25\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9A5965A665A69A59"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_21_n_0\,
      I1 => \disp[2][0]_INST_0_i_51_n_7\,
      I2 => \disp[2][0]_INST_0_i_48_n_3\,
      I3 => \disp[2][0]_INST_0_i_52_n_7\,
      I4 => \disp[2][0]_INST_0_i_50_n_1\,
      I5 => \disp[2][0]_INST_0_i_53_n_0\,
      O => \disp[2][0]_INST_0_i_25_n_0\
    );
\disp[2][0]_INST_0_i_250\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E80000E800E8E800"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_215_n_6\,
      I1 => \disp[2][0]_INST_0_i_214_n_5\,
      I2 => total_out(2),
      I3 => \disp[2][0]_INST_0_i_214_n_4\,
      I4 => \disp[2][0]_INST_0_i_258_n_7\,
      I5 => \disp[2][0]_INST_0_i_215_n_5\,
      O => \disp[2][0]_INST_0_i_250_n_0\
    );
\disp[2][0]_INST_0_i_251\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E80000E800E8E800"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_215_n_7\,
      I1 => \disp[2][0]_INST_0_i_214_n_6\,
      I2 => total_out(1),
      I3 => total_out(2),
      I4 => \disp[2][0]_INST_0_i_214_n_5\,
      I5 => \disp[2][0]_INST_0_i_215_n_6\,
      O => \disp[2][0]_INST_0_i_251_n_0\
    );
\disp[2][0]_INST_0_i_252\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E80000E800E8E800"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_274_n_4\,
      I1 => \disp[2][0]_INST_0_i_214_n_7\,
      I2 => total_out(0),
      I3 => total_out(1),
      I4 => \disp[2][0]_INST_0_i_214_n_6\,
      I5 => \disp[2][0]_INST_0_i_215_n_7\,
      O => \disp[2][0]_INST_0_i_252_n_0\
    );
\disp[2][0]_INST_0_i_253\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80080880"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_267_n_4\,
      I1 => \disp[2][0]_INST_0_i_274_n_5\,
      I2 => total_out(0),
      I3 => \disp[2][0]_INST_0_i_214_n_7\,
      I4 => \disp[2][0]_INST_0_i_274_n_4\,
      O => \disp[2][0]_INST_0_i_253_n_0\
    );
\disp[2][0]_INST_0_i_254\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99969666"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_250_n_0\,
      I1 => \disp[2][0]_INST_0_i_288_n_0\,
      I2 => \disp[2][0]_INST_0_i_258_n_7\,
      I3 => \disp[2][0]_INST_0_i_214_n_4\,
      I4 => \disp[2][0]_INST_0_i_215_n_5\,
      O => \disp[2][0]_INST_0_i_254_n_0\
    );
\disp[2][0]_INST_0_i_255\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66696999"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_251_n_0\,
      I1 => \disp[2][0]_INST_0_i_289_n_0\,
      I2 => total_out(2),
      I3 => \disp[2][0]_INST_0_i_214_n_5\,
      I4 => \disp[2][0]_INST_0_i_215_n_6\,
      O => \disp[2][0]_INST_0_i_255_n_0\
    );
\disp[2][0]_INST_0_i_256\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66696999"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_252_n_0\,
      I1 => \disp[2][0]_INST_0_i_290_n_0\,
      I2 => total_out(1),
      I3 => \disp[2][0]_INST_0_i_214_n_6\,
      I4 => \disp[2][0]_INST_0_i_215_n_7\,
      O => \disp[2][0]_INST_0_i_256_n_0\
    );
\disp[2][0]_INST_0_i_257\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66696999"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_253_n_0\,
      I1 => \disp[2][0]_INST_0_i_291_n_0\,
      I2 => total_out(0),
      I3 => \disp[2][0]_INST_0_i_214_n_7\,
      I4 => \disp[2][0]_INST_0_i_274_n_4\,
      O => \disp[2][0]_INST_0_i_257_n_0\
    );
\disp[2][0]_INST_0_i_258\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \disp[2][0]_INST_0_i_258_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_258_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_258_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_258_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => total_out(6 downto 4),
      DI(0) => '0',
      O(3) => \disp[2][0]_INST_0_i_258_n_4\,
      O(2) => \disp[2][0]_INST_0_i_258_n_5\,
      O(1) => \disp[2][0]_INST_0_i_258_n_6\,
      O(0) => \disp[2][0]_INST_0_i_258_n_7\,
      S(3) => \disp[2][0]_INST_0_i_292_n_0\,
      S(2) => \disp[2][0]_INST_0_i_293_n_0\,
      S(1) => \disp[2][0]_INST_0_i_294_n_0\,
      S(0) => total_out(3)
    );
\disp[2][0]_INST_0_i_259\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => total_out(5),
      I1 => total_out(3),
      O => \disp[2][0]_INST_0_i_259_n_0\
    );
\disp[2][0]_INST_0_i_26\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9A5965A665A69A59"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_22_n_0\,
      I1 => \disp[2][0]_INST_0_i_47_n_4\,
      I2 => \disp[2][0]_INST_0_i_48_n_3\,
      I3 => \disp[2][0]_INST_0_i_49_n_4\,
      I4 => \disp[2][0]_INST_0_i_50_n_1\,
      I5 => \disp[2][0]_INST_0_i_54_n_0\,
      O => \disp[2][0]_INST_0_i_26_n_0\
    );
\disp[2][0]_INST_0_i_260\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => total_out(2),
      I1 => total_out(4),
      O => \disp[2][0]_INST_0_i_260_n_0\
    );
\disp[2][0]_INST_0_i_261\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E8"
    )
        port map (
      I0 => total_out(1),
      I1 => total_out(3),
      I2 => total_out(7),
      O => \disp[2][0]_INST_0_i_261_n_0\
    );
\disp[2][0]_INST_0_i_262\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => total_out(7),
      I1 => total_out(1),
      I2 => total_out(3),
      O => \disp[2][0]_INST_0_i_262_n_0\
    );
\disp[2][0]_INST_0_i_263\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => total_out(3),
      I1 => total_out(5),
      I2 => total_out(4),
      I3 => total_out(6),
      O => \disp[2][0]_INST_0_i_263_n_0\
    );
\disp[2][0]_INST_0_i_264\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => total_out(4),
      I1 => total_out(2),
      I2 => total_out(3),
      I3 => total_out(5),
      O => \disp[2][0]_INST_0_i_264_n_0\
    );
\disp[2][0]_INST_0_i_265\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E81717E8"
    )
        port map (
      I0 => total_out(7),
      I1 => total_out(3),
      I2 => total_out(1),
      I3 => total_out(2),
      I4 => total_out(4),
      O => \disp[2][0]_INST_0_i_265_n_0\
    );
\disp[2][0]_INST_0_i_266\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => total_out(3),
      I1 => total_out(1),
      I2 => total_out(7),
      I3 => total_out(2),
      I4 => total_out(0),
      O => \disp[2][0]_INST_0_i_266_n_0\
    );
\disp[2][0]_INST_0_i_267\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_295_n_0\,
      CO(3) => \disp[2][0]_INST_0_i_267_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_267_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_267_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_267_n_3\,
      CYINIT => '0',
      DI(3) => \disp[2][0]_INST_0_i_296_n_0\,
      DI(2) => \disp[2][0]_INST_0_i_297_n_0\,
      DI(1) => \disp[2][0]_INST_0_i_298_n_0\,
      DI(0) => \disp[2][0]_INST_0_i_299_n_0\,
      O(3) => \disp[2][0]_INST_0_i_267_n_4\,
      O(2) => \disp[2][0]_INST_0_i_267_n_5\,
      O(1) => \disp[2][0]_INST_0_i_267_n_6\,
      O(0) => \disp[2][0]_INST_0_i_267_n_7\,
      S(3) => \disp[2][0]_INST_0_i_300_n_0\,
      S(2) => \disp[2][0]_INST_0_i_301_n_0\,
      S(1) => \disp[2][0]_INST_0_i_302_n_0\,
      S(0) => \disp[2][0]_INST_0_i_303_n_0\
    );
\disp[2][0]_INST_0_i_268\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => total_out(4),
      I1 => total_out(6),
      O => \disp[2][0]_INST_0_i_268_n_0\
    );
\disp[2][0]_INST_0_i_269\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_out(6),
      I1 => total_out(4),
      O => \disp[2][0]_INST_0_i_269_n_0\
    );
\disp[2][0]_INST_0_i_27\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9A5965A665A69A59"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_23_n_0\,
      I1 => \disp[2][0]_INST_0_i_47_n_5\,
      I2 => \disp[2][0]_INST_0_i_48_n_3\,
      I3 => \disp[2][0]_INST_0_i_49_n_5\,
      I4 => \disp[2][0]_INST_0_i_50_n_1\,
      I5 => \disp[2][0]_INST_0_i_55_n_0\,
      O => \disp[2][0]_INST_0_i_27_n_0\
    );
\disp[2][0]_INST_0_i_270\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => total_out(7),
      O => \disp[2][0]_INST_0_i_270_n_0\
    );
\disp[2][0]_INST_0_i_271\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2D"
    )
        port map (
      I0 => total_out(7),
      I1 => total_out(5),
      I2 => total_out(6),
      O => \disp[2][0]_INST_0_i_271_n_0\
    );
\disp[2][0]_INST_0_i_272\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"D22D"
    )
        port map (
      I0 => total_out(6),
      I1 => total_out(4),
      I2 => total_out(7),
      I3 => total_out(5),
      O => \disp[2][0]_INST_0_i_272_n_0\
    );
\disp[2][0]_INST_0_i_273\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4DB2B24D"
    )
        port map (
      I0 => total_out(7),
      I1 => total_out(3),
      I2 => total_out(5),
      I3 => total_out(4),
      I4 => total_out(6),
      O => \disp[2][0]_INST_0_i_273_n_0\
    );
\disp[2][0]_INST_0_i_274\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \disp[2][0]_INST_0_i_274_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_274_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_274_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_274_n_3\,
      CYINIT => '0',
      DI(3) => total_out(0),
      DI(2 downto 0) => B"001",
      O(3) => \disp[2][0]_INST_0_i_274_n_4\,
      O(2) => \disp[2][0]_INST_0_i_274_n_5\,
      O(1) => \disp[2][0]_INST_0_i_274_n_6\,
      O(0) => \NLW_disp[2][0]_INST_0_i_274_O_UNCONNECTED\(0),
      S(3) => \disp[2][0]_INST_0_i_304_n_0\,
      S(2) => \disp[2][0]_INST_0_i_305_n_0\,
      S(1) => \disp[2][0]_INST_0_i_306_n_0\,
      S(0) => total_out(0)
    );
\disp[2][0]_INST_0_i_275\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_out(4),
      I1 => total_out(7),
      O => \disp[2][0]_INST_0_i_275_n_0\
    );
\disp[2][0]_INST_0_i_276\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_out(3),
      I1 => total_out(6),
      O => \disp[2][0]_INST_0_i_276_n_0\
    );
\disp[2][0]_INST_0_i_277\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_out(2),
      I1 => total_out(5),
      O => \disp[2][0]_INST_0_i_277_n_0\
    );
\disp[2][0]_INST_0_i_278\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_out(1),
      I1 => total_out(4),
      O => \disp[2][0]_INST_0_i_278_n_0\
    );
\disp[2][0]_INST_0_i_279\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \disp[2][0]_INST_0_i_279_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_279_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_279_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_279_n_3\,
      CYINIT => '0',
      DI(3) => \disp[2][0]_INST_0_i_307_n_0\,
      DI(2) => \disp[2][0]_INST_0_i_308_n_0\,
      DI(1) => \disp[2][0]_INST_0_i_309_n_0\,
      DI(0) => \disp[2][0]_INST_0_i_310_n_0\,
      O(3 downto 0) => \NLW_disp[2][0]_INST_0_i_279_O_UNCONNECTED\(3 downto 0),
      S(3) => \disp[2][0]_INST_0_i_311_n_0\,
      S(2) => \disp[2][0]_INST_0_i_312_n_0\,
      S(1) => \disp[2][0]_INST_0_i_313_n_0\,
      S(0) => \disp[2][0]_INST_0_i_314_n_0\
    );
\disp[2][0]_INST_0_i_28\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9A5965A665A69A59"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_24_n_0\,
      I1 => \disp[2][0]_INST_0_i_47_n_6\,
      I2 => \disp[2][0]_INST_0_i_48_n_3\,
      I3 => \disp[2][0]_INST_0_i_49_n_6\,
      I4 => \disp[2][0]_INST_0_i_50_n_1\,
      I5 => \disp[2][0]_INST_0_i_56_n_0\,
      O => \disp[2][0]_INST_0_i_28_n_0\
    );
\disp[2][0]_INST_0_i_280\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BE282828"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_315_n_1\,
      I1 => \disp[2][0]_INST_0_i_267_n_4\,
      I2 => \disp[2][0]_INST_0_i_274_n_5\,
      I3 => \disp[2][0]_INST_0_i_274_n_6\,
      I4 => \disp[2][0]_INST_0_i_267_n_5\,
      O => \disp[2][0]_INST_0_i_280_n_0\
    );
\disp[2][0]_INST_0_i_281\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BE282828"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_315_n_6\,
      I1 => \disp[2][0]_INST_0_i_267_n_5\,
      I2 => \disp[2][0]_INST_0_i_274_n_6\,
      I3 => total_out(0),
      I4 => \disp[2][0]_INST_0_i_267_n_6\,
      O => \disp[2][0]_INST_0_i_281_n_0\
    );
\disp[2][0]_INST_0_i_282\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"28"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_315_n_7\,
      I1 => \disp[2][0]_INST_0_i_267_n_6\,
      I2 => total_out(0),
      O => \disp[2][0]_INST_0_i_282_n_0\
    );
\disp[2][0]_INST_0_i_283\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_316_n_4\,
      I1 => \disp[2][0]_INST_0_i_267_n_7\,
      O => \disp[2][0]_INST_0_i_283_n_0\
    );
\disp[2][0]_INST_0_i_284\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7F80F807F807807F"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_267_n_5\,
      I1 => \disp[2][0]_INST_0_i_274_n_6\,
      I2 => \disp[2][0]_INST_0_i_315_n_1\,
      I3 => \disp[2][0]_INST_0_i_317_n_0\,
      I4 => \disp[2][0]_INST_0_i_267_n_4\,
      I5 => \disp[2][0]_INST_0_i_274_n_5\,
      O => \disp[2][0]_INST_0_i_284_n_0\
    );
\disp[2][0]_INST_0_i_285\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669966996"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_281_n_0\,
      I1 => \disp[2][0]_INST_0_i_315_n_1\,
      I2 => \disp[2][0]_INST_0_i_267_n_4\,
      I3 => \disp[2][0]_INST_0_i_274_n_5\,
      I4 => \disp[2][0]_INST_0_i_274_n_6\,
      I5 => \disp[2][0]_INST_0_i_267_n_5\,
      O => \disp[2][0]_INST_0_i_285_n_0\
    );
\disp[2][0]_INST_0_i_286\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669966996"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_282_n_0\,
      I1 => \disp[2][0]_INST_0_i_315_n_6\,
      I2 => \disp[2][0]_INST_0_i_267_n_5\,
      I3 => \disp[2][0]_INST_0_i_274_n_6\,
      I4 => total_out(0),
      I5 => \disp[2][0]_INST_0_i_267_n_6\,
      O => \disp[2][0]_INST_0_i_286_n_0\
    );
\disp[2][0]_INST_0_i_287\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_315_n_7\,
      I1 => \disp[2][0]_INST_0_i_267_n_6\,
      I2 => total_out(0),
      I3 => \disp[2][0]_INST_0_i_283_n_0\,
      O => \disp[2][0]_INST_0_i_287_n_0\
    );
\disp[2][0]_INST_0_i_288\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_258_n_6\,
      I1 => \disp[2][0]_INST_0_i_167_n_3\,
      I2 => \disp[2][0]_INST_0_i_215_n_4\,
      O => \disp[2][0]_INST_0_i_288_n_0\
    );
\disp[2][0]_INST_0_i_289\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_215_n_5\,
      I1 => \disp[2][0]_INST_0_i_258_n_7\,
      I2 => \disp[2][0]_INST_0_i_214_n_4\,
      O => \disp[2][0]_INST_0_i_289_n_0\
    );
\disp[2][0]_INST_0_i_29\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_out(3),
      I1 => \disp[2][0]_INST_0_i_57_n_6\,
      O => \disp[2][0]_INST_0_i_29_n_0\
    );
\disp[2][0]_INST_0_i_290\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_215_n_6\,
      I1 => \disp[2][0]_INST_0_i_214_n_5\,
      I2 => total_out(2),
      O => \disp[2][0]_INST_0_i_290_n_0\
    );
\disp[2][0]_INST_0_i_291\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_215_n_7\,
      I1 => \disp[2][0]_INST_0_i_214_n_6\,
      I2 => total_out(1),
      O => \disp[2][0]_INST_0_i_291_n_0\
    );
\disp[2][0]_INST_0_i_292\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => total_out(0),
      I1 => total_out(2),
      I2 => total_out(6),
      O => \disp[2][0]_INST_0_i_292_n_0\
    );
\disp[2][0]_INST_0_i_293\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_out(5),
      I1 => total_out(1),
      O => \disp[2][0]_INST_0_i_293_n_0\
    );
\disp[2][0]_INST_0_i_294\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_out(4),
      I1 => total_out(0),
      O => \disp[2][0]_INST_0_i_294_n_0\
    );
\disp[2][0]_INST_0_i_295\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \disp[2][0]_INST_0_i_295_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_295_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_295_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_295_n_3\,
      CYINIT => '0',
      DI(3) => \disp[2][0]_INST_0_i_318_n_0\,
      DI(2) => total_out(0),
      DI(1 downto 0) => B"01",
      O(3) => \disp[2][0]_INST_0_i_295_n_4\,
      O(2) => \disp[2][0]_INST_0_i_295_n_5\,
      O(1) => \disp[2][0]_INST_0_i_295_n_6\,
      O(0) => \disp[2][0]_INST_0_i_295_n_7\,
      S(3) => \disp[2][0]_INST_0_i_319_n_0\,
      S(2) => \disp[2][0]_INST_0_i_320_n_0\,
      S(1) => \disp[2][0]_INST_0_i_321_n_0\,
      S(0) => total_out(0)
    );
\disp[2][0]_INST_0_i_296\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => total_out(2),
      I1 => total_out(4),
      I2 => total_out(6),
      O => \disp[2][0]_INST_0_i_296_n_0\
    );
\disp[2][0]_INST_0_i_297\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => total_out(1),
      I1 => total_out(3),
      I2 => total_out(5),
      O => \disp[2][0]_INST_0_i_297_n_0\
    );
\disp[2][0]_INST_0_i_298\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => total_out(0),
      I1 => total_out(4),
      I2 => total_out(2),
      O => \disp[2][0]_INST_0_i_298_n_0\
    );
\disp[2][0]_INST_0_i_299\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => total_out(1),
      I1 => total_out(3),
      O => \disp[2][0]_INST_0_i_299_n_0\
    );
\disp[2][0]_INST_0_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_11_n_0\,
      O => \disp[2][0]_INST_0_i_3_n_0\
    );
\disp[2][0]_INST_0_i_30\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_out(2),
      I1 => \disp[2][0]_INST_0_i_57_n_7\,
      O => \disp[2][0]_INST_0_i_30_n_0\
    );
\disp[2][0]_INST_0_i_300\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_296_n_0\,
      I1 => total_out(5),
      I2 => total_out(3),
      I3 => total_out(7),
      O => \disp[2][0]_INST_0_i_300_n_0\
    );
\disp[2][0]_INST_0_i_301\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_out(2),
      I1 => total_out(4),
      I2 => total_out(6),
      I3 => \disp[2][0]_INST_0_i_297_n_0\,
      O => \disp[2][0]_INST_0_i_301_n_0\
    );
\disp[2][0]_INST_0_i_302\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_out(1),
      I1 => total_out(3),
      I2 => total_out(5),
      I3 => \disp[2][0]_INST_0_i_298_n_0\,
      O => \disp[2][0]_INST_0_i_302_n_0\
    );
\disp[2][0]_INST_0_i_303\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => total_out(0),
      I1 => total_out(4),
      I2 => total_out(2),
      I3 => \disp[2][0]_INST_0_i_299_n_0\,
      O => \disp[2][0]_INST_0_i_303_n_0\
    );
\disp[2][0]_INST_0_i_304\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_out(0),
      I1 => total_out(3),
      O => \disp[2][0]_INST_0_i_304_n_0\
    );
\disp[2][0]_INST_0_i_305\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => total_out(2),
      O => \disp[2][0]_INST_0_i_305_n_0\
    );
\disp[2][0]_INST_0_i_306\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => total_out(1),
      O => \disp[2][0]_INST_0_i_306_n_0\
    );
\disp[2][0]_INST_0_i_307\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_316_n_5\,
      I1 => \disp[2][0]_INST_0_i_295_n_4\,
      O => \disp[2][0]_INST_0_i_307_n_0\
    );
\disp[2][0]_INST_0_i_308\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_316_n_6\,
      I1 => \disp[2][0]_INST_0_i_295_n_5\,
      O => \disp[2][0]_INST_0_i_308_n_0\
    );
\disp[2][0]_INST_0_i_309\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_316_n_7\,
      I1 => \disp[2][0]_INST_0_i_295_n_6\,
      O => \disp[2][0]_INST_0_i_309_n_0\
    );
\disp[2][0]_INST_0_i_31\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => total_out(1),
      O => \disp[2][0]_INST_0_i_31_n_0\
    );
\disp[2][0]_INST_0_i_310\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_295_n_7\,
      I1 => \disp[2][0]_INST_0_i_322_n_4\,
      O => \disp[2][0]_INST_0_i_310_n_0\
    );
\disp[2][0]_INST_0_i_311\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_316_n_4\,
      I1 => \disp[2][0]_INST_0_i_267_n_7\,
      I2 => \disp[2][0]_INST_0_i_295_n_4\,
      I3 => \disp[2][0]_INST_0_i_316_n_5\,
      O => \disp[2][0]_INST_0_i_311_n_0\
    );
\disp[2][0]_INST_0_i_312\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_295_n_5\,
      I1 => \disp[2][0]_INST_0_i_316_n_6\,
      I2 => \disp[2][0]_INST_0_i_295_n_4\,
      I3 => \disp[2][0]_INST_0_i_316_n_5\,
      O => \disp[2][0]_INST_0_i_312_n_0\
    );
\disp[2][0]_INST_0_i_313\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_295_n_6\,
      I1 => \disp[2][0]_INST_0_i_316_n_7\,
      I2 => \disp[2][0]_INST_0_i_295_n_5\,
      I3 => \disp[2][0]_INST_0_i_316_n_6\,
      O => \disp[2][0]_INST_0_i_313_n_0\
    );
\disp[2][0]_INST_0_i_314\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_322_n_4\,
      I1 => \disp[2][0]_INST_0_i_295_n_7\,
      I2 => \disp[2][0]_INST_0_i_295_n_6\,
      I3 => \disp[2][0]_INST_0_i_316_n_7\,
      O => \disp[2][0]_INST_0_i_314_n_0\
    );
\disp[2][0]_INST_0_i_315\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_316_n_0\,
      CO(3) => \NLW_disp[2][0]_INST_0_i_315_CO_UNCONNECTED\(3),
      CO(2) => \disp[2][0]_INST_0_i_315_n_1\,
      CO(1) => \NLW_disp[2][0]_INST_0_i_315_CO_UNCONNECTED\(1),
      CO(0) => \disp[2][0]_INST_0_i_315_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 2) => \NLW_disp[2][0]_INST_0_i_315_O_UNCONNECTED\(3 downto 2),
      O(1) => \disp[2][0]_INST_0_i_315_n_6\,
      O(0) => \disp[2][0]_INST_0_i_315_n_7\,
      S(3 downto 2) => B"01",
      S(1 downto 0) => total_out(7 downto 6)
    );
\disp[2][0]_INST_0_i_316\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_322_n_0\,
      CO(3) => \disp[2][0]_INST_0_i_316_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_316_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_316_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_316_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => total_out(3),
      DI(0) => \disp[2][0]_INST_0_i_323_n_0\,
      O(3) => \disp[2][0]_INST_0_i_316_n_4\,
      O(2) => \disp[2][0]_INST_0_i_316_n_5\,
      O(1) => \disp[2][0]_INST_0_i_316_n_6\,
      O(0) => \disp[2][0]_INST_0_i_316_n_7\,
      S(3 downto 2) => total_out(5 downto 4),
      S(1) => \disp[2][0]_INST_0_i_324_n_0\,
      S(0) => \disp[2][0]_INST_0_i_325_n_0\
    );
\disp[2][0]_INST_0_i_317\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_274_n_4\,
      I1 => \disp[2][0]_INST_0_i_214_n_7\,
      I2 => total_out(0),
      O => \disp[2][0]_INST_0_i_317_n_0\
    );
\disp[2][0]_INST_0_i_318\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => total_out(0),
      O => \disp[2][0]_INST_0_i_318_n_0\
    );
\disp[2][0]_INST_0_i_319\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => total_out(1),
      I1 => total_out(3),
      I2 => total_out(0),
      O => \disp[2][0]_INST_0_i_319_n_0\
    );
\disp[2][0]_INST_0_i_32\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => total_out(0),
      O => \disp[2][0]_INST_0_i_32_n_0\
    );
\disp[2][0]_INST_0_i_320\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_out(0),
      I1 => total_out(2),
      O => \disp[2][0]_INST_0_i_320_n_0\
    );
\disp[2][0]_INST_0_i_321\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => total_out(1),
      O => \disp[2][0]_INST_0_i_321_n_0\
    );
\disp[2][0]_INST_0_i_322\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_326_n_0\,
      CO(3) => \disp[2][0]_INST_0_i_322_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_322_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_322_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_322_n_3\,
      CYINIT => '0',
      DI(3) => \disp[2][0]_INST_0_i_327_n_0\,
      DI(2 downto 0) => total_out(7 downto 5),
      O(3) => \disp[2][0]_INST_0_i_322_n_4\,
      O(2 downto 0) => \NLW_disp[2][0]_INST_0_i_322_O_UNCONNECTED\(2 downto 0),
      S(3) => \disp[2][0]_INST_0_i_328_n_0\,
      S(2) => \disp[2][0]_INST_0_i_329_n_0\,
      S(1) => \disp[2][0]_INST_0_i_330_n_0\,
      S(0) => \disp[2][0]_INST_0_i_331_n_0\
    );
\disp[2][0]_INST_0_i_323\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => total_out(6),
      I1 => total_out(1),
      O => \disp[2][0]_INST_0_i_323_n_0\
    );
\disp[2][0]_INST_0_i_324\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => total_out(7),
      I1 => total_out(2),
      I2 => total_out(3),
      O => \disp[2][0]_INST_0_i_324_n_0\
    );
\disp[2][0]_INST_0_i_325\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => total_out(1),
      I1 => total_out(6),
      I2 => total_out(7),
      I3 => total_out(2),
      O => \disp[2][0]_INST_0_i_325_n_0\
    );
\disp[2][0]_INST_0_i_326\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \disp[2][0]_INST_0_i_326_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_326_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_326_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_326_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => total_out(4 downto 2),
      DI(0) => '0',
      O(3 downto 0) => \NLW_disp[2][0]_INST_0_i_326_O_UNCONNECTED\(3 downto 0),
      S(3) => \disp[2][0]_INST_0_i_332_n_0\,
      S(2) => \disp[2][0]_INST_0_i_333_n_0\,
      S(1) => \disp[2][0]_INST_0_i_334_n_0\,
      S(0) => total_out(1)
    );
\disp[2][0]_INST_0_i_327\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_out(6),
      I1 => total_out(1),
      O => \disp[2][0]_INST_0_i_327_n_0\
    );
\disp[2][0]_INST_0_i_328\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => total_out(1),
      I1 => total_out(6),
      I2 => total_out(0),
      I3 => total_out(5),
      O => \disp[2][0]_INST_0_i_328_n_0\
    );
\disp[2][0]_INST_0_i_329\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => total_out(0),
      I1 => total_out(5),
      I2 => total_out(7),
      O => \disp[2][0]_INST_0_i_329_n_0\
    );
\disp[2][0]_INST_0_i_33\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"80FE32B332B380FE"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_51_n_6\,
      I1 => \disp[2][0]_INST_0_i_48_n_3\,
      I2 => \disp[2][0]_INST_0_i_52_n_6\,
      I3 => \disp[2][0]_INST_0_i_50_n_1\,
      I4 => \disp[2][0]_INST_0_i_51_n_1\,
      I5 => \disp[2][0]_INST_0_i_52_n_5\,
      O => \disp[2][0]_INST_0_i_33_n_0\
    );
\disp[2][0]_INST_0_i_330\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_out(6),
      I1 => total_out(4),
      O => \disp[2][0]_INST_0_i_330_n_0\
    );
\disp[2][0]_INST_0_i_331\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_out(5),
      I1 => total_out(3),
      O => \disp[2][0]_INST_0_i_331_n_0\
    );
\disp[2][0]_INST_0_i_332\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_out(4),
      I1 => total_out(2),
      O => \disp[2][0]_INST_0_i_332_n_0\
    );
\disp[2][0]_INST_0_i_333\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_out(3),
      I1 => total_out(1),
      O => \disp[2][0]_INST_0_i_333_n_0\
    );
\disp[2][0]_INST_0_i_334\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => total_out(2),
      I1 => total_out(0),
      O => \disp[2][0]_INST_0_i_334_n_0\
    );
\disp[2][0]_INST_0_i_34\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"32B380FE80FE32B3"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_51_n_7\,
      I1 => \disp[2][0]_INST_0_i_48_n_3\,
      I2 => \disp[2][0]_INST_0_i_52_n_7\,
      I3 => \disp[2][0]_INST_0_i_50_n_1\,
      I4 => \disp[2][0]_INST_0_i_51_n_6\,
      I5 => \disp[2][0]_INST_0_i_52_n_6\,
      O => \disp[2][0]_INST_0_i_34_n_0\
    );
\disp[2][0]_INST_0_i_35\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAA995A9955555"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_58_n_7\,
      I1 => \disp[2][0]_INST_0_i_52_n_5\,
      I2 => \disp[2][0]_INST_0_i_48_n_3\,
      I3 => \disp[2][0]_INST_0_i_51_n_1\,
      I4 => \disp[2][0]_INST_0_i_52_n_4\,
      I5 => \disp[2][0]_INST_0_i_50_n_1\,
      O => \disp[2][0]_INST_0_i_35_n_0\
    );
\disp[2][0]_INST_0_i_36\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"566AA995A995566A"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_33_n_0\,
      I1 => \disp[2][0]_INST_0_i_51_n_1\,
      I2 => \disp[2][0]_INST_0_i_48_n_3\,
      I3 => \disp[2][0]_INST_0_i_52_n_5\,
      I4 => \disp[2][0]_INST_0_i_50_n_1\,
      I5 => \disp[2][0]_INST_0_i_52_n_4\,
      O => \disp[2][0]_INST_0_i_36_n_0\
    );
\disp[2][0]_INST_0_i_37\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9A5965A665A69A59"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_34_n_0\,
      I1 => \disp[2][0]_INST_0_i_51_n_6\,
      I2 => \disp[2][0]_INST_0_i_48_n_3\,
      I3 => \disp[2][0]_INST_0_i_52_n_6\,
      I4 => \disp[2][0]_INST_0_i_50_n_1\,
      I5 => \disp[2][0]_INST_0_i_59_n_0\,
      O => \disp[2][0]_INST_0_i_37_n_0\
    );
\disp[2][0]_INST_0_i_38\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_60_n_0\,
      CO(3) => \disp[2][0]_INST_0_i_38_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_38_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_38_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_38_n_3\,
      CYINIT => '0',
      DI(3) => \disp[2][0]_INST_0_i_61_n_0\,
      DI(2) => \disp[2][0]_INST_0_i_62_n_0\,
      DI(1) => \disp[2][0]_INST_0_i_63_n_0\,
      DI(0) => \disp[2][0]_INST_0_i_64_n_0\,
      O(3 downto 0) => \NLW_disp[2][0]_INST_0_i_38_O_UNCONNECTED\(3 downto 0),
      S(3) => \disp[2][0]_INST_0_i_65_n_0\,
      S(2) => \disp[2][0]_INST_0_i_66_n_0\,
      S(1) => \disp[2][0]_INST_0_i_67_n_0\,
      S(0) => \disp[2][0]_INST_0_i_68_n_0\
    );
\disp[2][0]_INST_0_i_39\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BB2B2B22"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_69_n_0\,
      I1 => \disp[2][0]_INST_0_i_50_n_1\,
      I2 => \disp[2][0]_INST_0_i_70_n_3\,
      I3 => \disp[2][0]_INST_0_i_71_n_4\,
      I4 => \disp[2][0]_INST_0_i_72_n_4\,
      O => \disp[2][0]_INST_0_i_39_n_0\
    );
\disp[2][0]_INST_0_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_12_n_0\,
      O => \disp[2][0]_INST_0_i_4_n_0\
    );
\disp[2][0]_INST_0_i_40\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7DD7355335531441"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_50_n_1\,
      I1 => \disp[2][0]_INST_0_i_70_n_3\,
      I2 => \disp[2][0]_INST_0_i_71_n_4\,
      I3 => \disp[2][0]_INST_0_i_72_n_4\,
      I4 => \disp[2][0]_INST_0_i_71_n_5\,
      I5 => \disp[2][0]_INST_0_i_72_n_5\,
      O => \disp[2][0]_INST_0_i_40_n_0\
    );
\disp[2][0]_INST_0_i_41\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7DD7355335531441"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_50_n_1\,
      I1 => \disp[2][0]_INST_0_i_70_n_3\,
      I2 => \disp[2][0]_INST_0_i_71_n_5\,
      I3 => \disp[2][0]_INST_0_i_72_n_5\,
      I4 => \disp[2][0]_INST_0_i_71_n_6\,
      I5 => \disp[2][0]_INST_0_i_72_n_6\,
      O => \disp[2][0]_INST_0_i_41_n_0\
    );
\disp[2][0]_INST_0_i_42\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7DD7355335531441"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_50_n_1\,
      I1 => \disp[2][0]_INST_0_i_70_n_3\,
      I2 => \disp[2][0]_INST_0_i_71_n_6\,
      I3 => \disp[2][0]_INST_0_i_72_n_6\,
      I4 => \disp[2][0]_INST_0_i_71_n_7\,
      I5 => \disp[2][0]_INST_0_i_73_n_0\,
      O => \disp[2][0]_INST_0_i_42_n_0\
    );
\disp[2][0]_INST_0_i_43\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A665599A599AA665"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_39_n_0\,
      I1 => \disp[2][0]_INST_0_i_48_n_3\,
      I2 => \disp[2][0]_INST_0_i_49_n_7\,
      I3 => \disp[2][0]_INST_0_i_47_n_7\,
      I4 => \disp[2][0]_INST_0_i_50_n_1\,
      I5 => \disp[2][0]_INST_0_i_74_n_0\,
      O => \disp[2][0]_INST_0_i_43_n_0\
    );
\disp[2][0]_INST_0_i_44\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A665599A599AA665"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_40_n_0\,
      I1 => \disp[2][0]_INST_0_i_70_n_3\,
      I2 => \disp[2][0]_INST_0_i_71_n_4\,
      I3 => \disp[2][0]_INST_0_i_72_n_4\,
      I4 => \disp[2][0]_INST_0_i_50_n_1\,
      I5 => \disp[2][0]_INST_0_i_69_n_0\,
      O => \disp[2][0]_INST_0_i_44_n_0\
    );
\disp[2][0]_INST_0_i_45\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"599AA665A665599A"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_41_n_0\,
      I1 => \disp[2][0]_INST_0_i_70_n_3\,
      I2 => \disp[2][0]_INST_0_i_71_n_5\,
      I3 => \disp[2][0]_INST_0_i_72_n_5\,
      I4 => \disp[2][0]_INST_0_i_50_n_1\,
      I5 => \disp[2][0]_INST_0_i_75_n_0\,
      O => \disp[2][0]_INST_0_i_45_n_0\
    );
\disp[2][0]_INST_0_i_46\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"599AA665A665599A"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_42_n_0\,
      I1 => \disp[2][0]_INST_0_i_70_n_3\,
      I2 => \disp[2][0]_INST_0_i_71_n_6\,
      I3 => \disp[2][0]_INST_0_i_72_n_6\,
      I4 => \disp[2][0]_INST_0_i_50_n_1\,
      I5 => \disp[2][0]_INST_0_i_76_n_0\,
      O => \disp[2][0]_INST_0_i_46_n_0\
    );
\disp[2][0]_INST_0_i_47\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_72_n_0\,
      CO(3) => \disp[2][0]_INST_0_i_47_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_47_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_47_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_47_n_3\,
      CYINIT => '0',
      DI(3) => \disp[2][0]_INST_0_i_77_n_0\,
      DI(2) => \disp[2][0]_INST_0_i_78_n_0\,
      DI(1) => \disp[2][0]_INST_0_i_79_n_0\,
      DI(0) => \disp[2][0]_INST_0_i_80_n_0\,
      O(3) => \disp[2][0]_INST_0_i_47_n_4\,
      O(2) => \disp[2][0]_INST_0_i_47_n_5\,
      O(1) => \disp[2][0]_INST_0_i_47_n_6\,
      O(0) => \disp[2][0]_INST_0_i_47_n_7\,
      S(3) => \disp[2][0]_INST_0_i_81_n_0\,
      S(2) => \disp[2][0]_INST_0_i_82_n_0\,
      S(1) => \disp[2][0]_INST_0_i_83_n_0\,
      S(0) => \disp[2][0]_INST_0_i_84_n_0\
    );
\disp[2][0]_INST_0_i_48\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_71_n_0\,
      CO(3 downto 1) => \NLW_disp[2][0]_INST_0_i_48_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \disp[2][0]_INST_0_i_48_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_disp[2][0]_INST_0_i_48_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
\disp[2][0]_INST_0_i_49\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \disp[2][0]_INST_0_i_49_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_49_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_49_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_49_n_3\,
      CYINIT => '0',
      DI(3) => \disp[2][0]_INST_0_i_85_n_0\,
      DI(2) => \disp[2][0]_INST_0_i_86_n_0\,
      DI(1) => \disp[2][0]_INST_0_i_87_n_0\,
      DI(0) => '0',
      O(3) => \disp[2][0]_INST_0_i_49_n_4\,
      O(2) => \disp[2][0]_INST_0_i_49_n_5\,
      O(1) => \disp[2][0]_INST_0_i_49_n_6\,
      O(0) => \disp[2][0]_INST_0_i_49_n_7\,
      S(3) => \disp[2][0]_INST_0_i_88_n_0\,
      S(2) => \disp[2][0]_INST_0_i_89_n_0\,
      S(1) => \disp[2][0]_INST_0_i_90_n_0\,
      S(0) => \disp[2][0]_INST_0_i_91_n_0\
    );
\disp[2][0]_INST_0_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_5_n_0\,
      O => \disp[2][0]_INST_0_i_5_n_0\
    );
\disp[2][0]_INST_0_i_50\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_92_n_0\,
      CO(3) => \NLW_disp[2][0]_INST_0_i_50_CO_UNCONNECTED\(3),
      CO(2) => \disp[2][0]_INST_0_i_50_n_1\,
      CO(1) => \NLW_disp[2][0]_INST_0_i_50_CO_UNCONNECTED\(1),
      CO(0) => \disp[2][0]_INST_0_i_50_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \disp[2][0]_INST_0_i_93_n_0\,
      DI(0) => \disp[2][0]_INST_0_i_94_n_0\,
      O(3 downto 2) => \NLW_disp[2][0]_INST_0_i_50_O_UNCONNECTED\(3 downto 2),
      O(1) => \disp[2][0]_INST_0_i_50_n_6\,
      O(0) => \disp[2][0]_INST_0_i_50_n_7\,
      S(3 downto 2) => B"01",
      S(1) => \disp[2][0]_INST_0_i_95_n_0\,
      S(0) => \disp[2][0]_INST_0_i_96_n_0\
    );
\disp[2][0]_INST_0_i_51\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_47_n_0\,
      CO(3) => \NLW_disp[2][0]_INST_0_i_51_CO_UNCONNECTED\(3),
      CO(2) => \disp[2][0]_INST_0_i_51_n_1\,
      CO(1) => \NLW_disp[2][0]_INST_0_i_51_CO_UNCONNECTED\(1),
      CO(0) => \disp[2][0]_INST_0_i_51_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \disp[2][0]_INST_0_i_97_n_0\,
      DI(0) => \disp[2][0]_INST_0_i_98_n_0\,
      O(3 downto 2) => \NLW_disp[2][0]_INST_0_i_51_O_UNCONNECTED\(3 downto 2),
      O(1) => \disp[2][0]_INST_0_i_51_n_6\,
      O(0) => \disp[2][0]_INST_0_i_51_n_7\,
      S(3 downto 2) => B"01",
      S(1) => \disp[2][0]_INST_0_i_99_n_0\,
      S(0) => \disp[2][0]_INST_0_i_100_n_0\
    );
\disp[2][0]_INST_0_i_52\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_49_n_0\,
      CO(3) => \disp[2][0]_INST_0_i_52_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_52_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_52_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_52_n_3\,
      CYINIT => '0',
      DI(3) => \disp[2][0]_INST_0_i_101_n_0\,
      DI(2) => \disp[2][0]_INST_0_i_102_n_0\,
      DI(1) => \disp[2][0]_INST_0_i_103_n_0\,
      DI(0) => \disp[2][0]_INST_0_i_104_n_0\,
      O(3) => \disp[2][0]_INST_0_i_52_n_4\,
      O(2) => \disp[2][0]_INST_0_i_52_n_5\,
      O(1) => \disp[2][0]_INST_0_i_52_n_6\,
      O(0) => \disp[2][0]_INST_0_i_52_n_7\,
      S(3) => \disp[2][0]_INST_0_i_105_n_0\,
      S(2) => \disp[2][0]_INST_0_i_106_n_0\,
      S(1) => \disp[2][0]_INST_0_i_107_n_0\,
      S(0) => \disp[2][0]_INST_0_i_108_n_0\
    );
\disp[2][0]_INST_0_i_53\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_52_n_6\,
      I1 => \disp[2][0]_INST_0_i_48_n_3\,
      I2 => \disp[2][0]_INST_0_i_51_n_6\,
      O => \disp[2][0]_INST_0_i_53_n_0\
    );
\disp[2][0]_INST_0_i_54\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_52_n_7\,
      I1 => \disp[2][0]_INST_0_i_48_n_3\,
      I2 => \disp[2][0]_INST_0_i_51_n_7\,
      O => \disp[2][0]_INST_0_i_54_n_0\
    );
\disp[2][0]_INST_0_i_55\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_49_n_4\,
      I1 => \disp[2][0]_INST_0_i_48_n_3\,
      I2 => \disp[2][0]_INST_0_i_47_n_4\,
      O => \disp[2][0]_INST_0_i_55_n_0\
    );
\disp[2][0]_INST_0_i_56\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_49_n_5\,
      I1 => \disp[2][0]_INST_0_i_48_n_3\,
      I2 => \disp[2][0]_INST_0_i_47_n_5\,
      O => \disp[2][0]_INST_0_i_56_n_0\
    );
\disp[2][0]_INST_0_i_57\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_109_n_0\,
      CO(3) => \disp[2][0]_INST_0_i_57_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_57_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_57_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_57_n_3\,
      CYINIT => '0',
      DI(3) => \disp[2][0]_INST_0_i_110_n_0\,
      DI(2) => \disp[2][0]_INST_0_i_111_n_0\,
      DI(1) => \disp[2][0]_INST_0_i_112_n_0\,
      DI(0) => \disp[2][0]_INST_0_i_113_n_0\,
      O(3) => \disp[2][0]_INST_0_i_57_n_4\,
      O(2) => \disp[2][0]_INST_0_i_57_n_5\,
      O(1) => \disp[2][0]_INST_0_i_57_n_6\,
      O(0) => \disp[2][0]_INST_0_i_57_n_7\,
      S(3) => \disp[2][0]_INST_0_i_114_n_0\,
      S(2) => \disp[2][0]_INST_0_i_115_n_0\,
      S(1) => \disp[2][0]_INST_0_i_116_n_0\,
      S(0) => \disp[2][0]_INST_0_i_117_n_0\
    );
\disp[2][0]_INST_0_i_58\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_52_n_0\,
      CO(3 downto 0) => \NLW_disp[2][0]_INST_0_i_58_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_disp[2][0]_INST_0_i_58_O_UNCONNECTED\(3 downto 1),
      O(0) => \disp[2][0]_INST_0_i_58_n_7\,
      S(3 downto 1) => B"000",
      S(0) => \disp[2][0]_INST_0_i_118_n_0\
    );
\disp[2][0]_INST_0_i_59\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_52_n_5\,
      I1 => \disp[2][0]_INST_0_i_48_n_3\,
      I2 => \disp[2][0]_INST_0_i_51_n_1\,
      O => \disp[2][0]_INST_0_i_59_n_0\
    );
\disp[2][0]_INST_0_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_10_n_0\,
      I1 => \disp[2][0]_INST_0_i_13_n_6\,
      O => \disp[2][0]_INST_0_i_6_n_0\
    );
\disp[2][0]_INST_0_i_60\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_119_n_0\,
      CO(3) => \disp[2][0]_INST_0_i_60_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_60_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_60_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_60_n_3\,
      CYINIT => '0',
      DI(3) => \disp[2][0]_INST_0_i_120_n_0\,
      DI(2) => \disp[2][0]_INST_0_i_121_n_0\,
      DI(1) => \disp[2][0]_INST_0_i_122_n_0\,
      DI(0) => \disp[2][0]_INST_0_i_123_n_0\,
      O(3 downto 0) => \NLW_disp[2][0]_INST_0_i_60_O_UNCONNECTED\(3 downto 0),
      S(3) => \disp[2][0]_INST_0_i_124_n_0\,
      S(2) => \disp[2][0]_INST_0_i_125_n_0\,
      S(1) => \disp[2][0]_INST_0_i_126_n_0\,
      S(0) => \disp[2][0]_INST_0_i_127_n_0\
    );
\disp[2][0]_INST_0_i_61\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"233B08EF08EF233B"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_128_n_4\,
      I1 => \disp[2][0]_INST_0_i_70_n_3\,
      I2 => \disp[2][0]_INST_0_i_11_n_0\,
      I3 => \disp[2][0]_INST_0_i_50_n_1\,
      I4 => \disp[2][0]_INST_0_i_71_n_7\,
      I5 => \disp[2][0]_INST_0_i_73_n_0\,
      O => \disp[2][0]_INST_0_i_61_n_0\
    );
\disp[2][0]_INST_0_i_62\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5D4504DF04DF5D45"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_50_n_1\,
      I1 => \disp[2][0]_INST_0_i_128_n_5\,
      I2 => \disp[2][0]_INST_0_i_12_n_0\,
      I3 => \disp[2][0]_INST_0_i_70_n_3\,
      I4 => \disp[2][0]_INST_0_i_128_n_4\,
      I5 => \disp[2][0]_INST_0_i_11_n_0\,
      O => \disp[2][0]_INST_0_i_62_n_0\
    );
\disp[2][0]_INST_0_i_63\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"08EF233B233B08EF"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_128_n_6\,
      I1 => \disp[2][0]_INST_0_i_70_n_3\,
      I2 => \disp[3][0]_INST_0_i_5_n_0\,
      I3 => \disp[2][0]_INST_0_i_50_n_1\,
      I4 => \disp[2][0]_INST_0_i_12_n_0\,
      I5 => \disp[2][0]_INST_0_i_128_n_5\,
      O => \disp[2][0]_INST_0_i_63_n_0\
    );
\disp[2][0]_INST_0_i_64\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"82BE8228"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_50_n_6\,
      I1 => \disp[2][0]_INST_0_i_128_n_6\,
      I2 => \disp[3][0]_INST_0_i_5_n_0\,
      I3 => \disp[2][0]_INST_0_i_70_n_3\,
      I4 => \disp[2][0]_INST_0_i_128_n_7\,
      O => \disp[2][0]_INST_0_i_64_n_0\
    );
\disp[2][0]_INST_0_i_65\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_61_n_0\,
      I1 => \disp[2][0]_INST_0_i_129_n_0\,
      I2 => \disp[2][0]_INST_0_i_50_n_1\,
      I3 => \disp[2][0]_INST_0_i_70_n_3\,
      I4 => \disp[2][0]_INST_0_i_71_n_6\,
      I5 => \disp[2][0]_INST_0_i_72_n_6\,
      O => \disp[2][0]_INST_0_i_65_n_0\
    );
\disp[2][0]_INST_0_i_66\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A665599A599AA665"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_62_n_0\,
      I1 => \disp[2][0]_INST_0_i_128_n_4\,
      I2 => \disp[2][0]_INST_0_i_70_n_3\,
      I3 => \disp[2][0]_INST_0_i_11_n_0\,
      I4 => \disp[2][0]_INST_0_i_50_n_1\,
      I5 => \disp[2][0]_INST_0_i_130_n_0\,
      O => \disp[2][0]_INST_0_i_66_n_0\
    );
\disp[2][0]_INST_0_i_67\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_63_n_0\,
      I1 => \disp[2][0]_INST_0_i_50_n_1\,
      I2 => \disp[2][0]_INST_0_i_131_n_0\,
      I3 => \disp[2][0]_INST_0_i_128_n_4\,
      I4 => \disp[2][0]_INST_0_i_11_n_0\,
      I5 => \disp[2][0]_INST_0_i_70_n_3\,
      O => \disp[2][0]_INST_0_i_67_n_0\
    );
\disp[2][0]_INST_0_i_68\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A665599A599AA665"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_64_n_0\,
      I1 => \disp[2][0]_INST_0_i_128_n_6\,
      I2 => \disp[2][0]_INST_0_i_70_n_3\,
      I3 => \disp[3][0]_INST_0_i_5_n_0\,
      I4 => \disp[2][0]_INST_0_i_50_n_1\,
      I5 => \disp[2][0]_INST_0_i_132_n_0\,
      O => \disp[2][0]_INST_0_i_68_n_0\
    );
\disp[2][0]_INST_0_i_69\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_47_n_7\,
      I1 => \disp[2][0]_INST_0_i_48_n_3\,
      I2 => \disp[2][0]_INST_0_i_49_n_7\,
      O => \disp[2][0]_INST_0_i_69_n_0\
    );
\disp[2][0]_INST_0_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_11_n_0\,
      I1 => \disp[2][0]_INST_0_i_13_n_7\,
      O => \disp[2][0]_INST_0_i_7_n_0\
    );
\disp[2][0]_INST_0_i_70\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_133_n_0\,
      CO(3 downto 1) => \NLW_disp[2][0]_INST_0_i_70_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \disp[2][0]_INST_0_i_70_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_disp[2][0]_INST_0_i_70_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
\disp[2][0]_INST_0_i_71\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_128_n_0\,
      CO(3) => \disp[2][0]_INST_0_i_71_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_71_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_71_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_71_n_3\,
      CYINIT => '0',
      DI(3) => \disp[2][0]_INST_0_i_134_n_0\,
      DI(2) => \disp[2][0]_INST_0_i_135_n_0\,
      DI(1) => \disp[2][0]_INST_0_i_136_n_0\,
      DI(0) => \disp[2][0]_INST_0_i_137_n_0\,
      O(3) => \disp[2][0]_INST_0_i_71_n_4\,
      O(2) => \disp[2][0]_INST_0_i_71_n_5\,
      O(1) => \disp[2][0]_INST_0_i_71_n_6\,
      O(0) => \disp[2][0]_INST_0_i_71_n_7\,
      S(3) => \disp[2][0]_INST_0_i_138_n_0\,
      S(2) => \disp[2][0]_INST_0_i_139_n_0\,
      S(1) => \disp[2][0]_INST_0_i_140_n_0\,
      S(0) => \disp[2][0]_INST_0_i_141_n_0\
    );
\disp[2][0]_INST_0_i_72\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \disp[2][0]_INST_0_i_72_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_72_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_72_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_72_n_3\,
      CYINIT => '0',
      DI(3) => \disp[2][0]_INST_0_i_142_n_0\,
      DI(2) => \disp[2][0]_INST_0_i_143_n_0\,
      DI(1) => \disp[2][0]_INST_0_i_144_n_0\,
      DI(0) => '0',
      O(3) => \disp[2][0]_INST_0_i_72_n_4\,
      O(2) => \disp[2][0]_INST_0_i_72_n_5\,
      O(1) => \disp[2][0]_INST_0_i_72_n_6\,
      O(0) => \NLW_disp[2][0]_INST_0_i_72_O_UNCONNECTED\(0),
      S(3) => \disp[2][0]_INST_0_i_145_n_0\,
      S(2) => \disp[2][0]_INST_0_i_146_n_0\,
      S(1) => \disp[2][0]_INST_0_i_147_n_0\,
      S(0) => \disp[2][0]_INST_0_i_148_n_0\
    );
\disp[2][0]_INST_0_i_73\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_10_n_0\,
      I1 => \disp[3][0]_INST_0_i_5_n_0\,
      O => \disp[2][0]_INST_0_i_73_n_0\
    );
\disp[2][0]_INST_0_i_74\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_49_n_6\,
      I1 => \disp[2][0]_INST_0_i_48_n_3\,
      I2 => \disp[2][0]_INST_0_i_47_n_6\,
      O => \disp[2][0]_INST_0_i_74_n_0\
    );
\disp[2][0]_INST_0_i_75\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_72_n_4\,
      I1 => \disp[2][0]_INST_0_i_71_n_4\,
      I2 => \disp[2][0]_INST_0_i_70_n_3\,
      O => \disp[2][0]_INST_0_i_75_n_0\
    );
\disp[2][0]_INST_0_i_76\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_72_n_5\,
      I1 => \disp[2][0]_INST_0_i_71_n_5\,
      I2 => \disp[2][0]_INST_0_i_70_n_3\,
      O => \disp[2][0]_INST_0_i_76_n_0\
    );
\disp[2][0]_INST_0_i_77\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[2][3]_INST_0_i_4_n_0\,
      I1 => \inst_int_to_string/cent\(6),
      O => \disp[2][0]_INST_0_i_77_n_0\
    );
\disp[2][0]_INST_0_i_78\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_10_n_0\,
      I1 => \inst_int_to_string/cent\(5),
      O => \disp[2][0]_INST_0_i_78_n_0\
    );
\disp[2][0]_INST_0_i_79\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \disp[2][3]_INST_0_i_4_n_0\,
      I1 => \disp[2][0]_INST_0_i_11_n_0\,
      O => \disp[2][0]_INST_0_i_79_n_0\
    );
\disp[2][0]_INST_0_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_12_n_0\,
      I1 => \disp[2][0]_INST_0_i_14_n_4\,
      O => \disp[2][0]_INST_0_i_8_n_0\
    );
\disp[2][0]_INST_0_i_80\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_10_n_0\,
      I1 => \disp[2][0]_INST_0_i_12_n_0\,
      I2 => \inst_int_to_string/cent\(6),
      O => \disp[2][0]_INST_0_i_80_n_0\
    );
\disp[2][0]_INST_0_i_81\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1E"
    )
        port map (
      I0 => \inst_int_to_string/cent\(6),
      I1 => \disp[2][3]_INST_0_i_4_n_0\,
      I2 => \inst_int_to_string/cent\(5),
      O => \disp[2][0]_INST_0_i_81_n_0\
    );
\disp[2][0]_INST_0_i_82\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1EE1"
    )
        port map (
      I0 => \inst_int_to_string/cent\(5),
      I1 => \disp[2][0]_INST_0_i_10_n_0\,
      I2 => \inst_int_to_string/cent\(6),
      I3 => \disp[2][3]_INST_0_i_4_n_0\,
      O => \disp[2][0]_INST_0_i_82_n_0\
    );
\disp[2][0]_INST_0_i_83\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4BB4"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_11_n_0\,
      I1 => \disp[2][3]_INST_0_i_4_n_0\,
      I2 => \inst_int_to_string/cent\(5),
      I3 => \disp[2][0]_INST_0_i_10_n_0\,
      O => \disp[2][0]_INST_0_i_83_n_0\
    );
\disp[2][0]_INST_0_i_84\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4DB2B24D"
    )
        port map (
      I0 => \inst_int_to_string/cent\(6),
      I1 => \disp[2][0]_INST_0_i_12_n_0\,
      I2 => \disp[2][0]_INST_0_i_10_n_0\,
      I3 => \disp[2][0]_INST_0_i_11_n_0\,
      I4 => \disp[2][3]_INST_0_i_4_n_0\,
      O => \disp[2][0]_INST_0_i_84_n_0\
    );
\disp[2][0]_INST_0_i_85\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"71"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_5_n_0\,
      I1 => \disp[2][0]_INST_0_i_70_n_3\,
      I2 => \disp[2][0]_INST_0_i_11_n_0\,
      O => \disp[2][0]_INST_0_i_85_n_0\
    );
\disp[2][0]_INST_0_i_86\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_5_n_0\,
      I1 => \disp[2][0]_INST_0_i_11_n_0\,
      I2 => \disp[2][0]_INST_0_i_70_n_3\,
      O => \disp[2][0]_INST_0_i_86_n_0\
    );
\disp[2][0]_INST_0_i_87\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_5_n_0\,
      I1 => \disp[2][0]_INST_0_i_70_n_3\,
      O => \disp[2][0]_INST_0_i_87_n_0\
    );
\disp[2][0]_INST_0_i_88\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_12_n_0\,
      I1 => \disp[2][0]_INST_0_i_70_n_3\,
      I2 => \disp[2][0]_INST_0_i_10_n_0\,
      I3 => \disp[2][0]_INST_0_i_85_n_0\,
      O => \disp[2][0]_INST_0_i_88_n_0\
    );
\disp[2][0]_INST_0_i_89\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A596"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_5_n_0\,
      I1 => \disp[2][0]_INST_0_i_70_n_3\,
      I2 => \disp[2][0]_INST_0_i_11_n_0\,
      I3 => \disp[2][0]_INST_0_i_12_n_0\,
      O => \disp[2][0]_INST_0_i_89_n_0\
    );
\disp[2][0]_INST_0_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => total_out(0),
      I1 => \disp[1]_OBUF\(1),
      I2 => \inst_int_to_string/p_1_in\(0),
      O => \disp[2][0]_INST_0_i_9_n_0\
    );
\disp[2][0]_INST_0_i_90\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6C"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_5_n_0\,
      I1 => \disp[2][0]_INST_0_i_12_n_0\,
      I2 => \disp[2][0]_INST_0_i_70_n_3\,
      O => \disp[2][0]_INST_0_i_90_n_0\
    );
\disp[2][0]_INST_0_i_91\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_5_n_0\,
      I1 => \disp[2][0]_INST_0_i_70_n_3\,
      O => \disp[2][0]_INST_0_i_91_n_0\
    );
\disp[2][0]_INST_0_i_92\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_149_n_0\,
      CO(3) => \disp[2][0]_INST_0_i_92_n_0\,
      CO(2) => \disp[2][0]_INST_0_i_92_n_1\,
      CO(1) => \disp[2][0]_INST_0_i_92_n_2\,
      CO(0) => \disp[2][0]_INST_0_i_92_n_3\,
      CYINIT => '0',
      DI(3) => \disp[2][0]_INST_0_i_150_n_0\,
      DI(2) => \disp[2][0]_INST_0_i_151_n_0\,
      DI(1) => \disp[2][0]_INST_0_i_152_n_0\,
      DI(0) => \disp[2][0]_INST_0_i_153_n_0\,
      O(3) => \disp[2][0]_INST_0_i_92_n_4\,
      O(2) => \disp[2][0]_INST_0_i_92_n_5\,
      O(1) => \disp[2][0]_INST_0_i_92_n_6\,
      O(0) => \disp[2][0]_INST_0_i_92_n_7\,
      S(3) => \disp[2][0]_INST_0_i_154_n_0\,
      S(2) => \disp[2][0]_INST_0_i_155_n_0\,
      S(1) => \disp[2][0]_INST_0_i_156_n_0\,
      S(0) => \disp[2][0]_INST_0_i_157_n_0\
    );
\disp[2][0]_INST_0_i_93\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_out(6),
      I1 => \disp[1]_OBUF\(1),
      I2 => \disp[5][0]_INST_0_i_91_n_0\,
      O => \disp[2][0]_INST_0_i_93_n_0\
    );
\disp[2][0]_INST_0_i_94\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \inst_int_to_string/cent\(6),
      O => \disp[2][0]_INST_0_i_94_n_0\
    );
\disp[2][0]_INST_0_i_95\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \inst_int_to_string/cent\(6),
      O => \disp[2][0]_INST_0_i_95_n_0\
    );
\disp[2][0]_INST_0_i_96\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \inst_int_to_string/cent\(5),
      I1 => \inst_int_to_string/cent\(6),
      O => \disp[2][0]_INST_0_i_96_n_0\
    );
\disp[2][0]_INST_0_i_97\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_out(6),
      I1 => \disp[1]_OBUF\(1),
      I2 => \disp[5][0]_INST_0_i_91_n_0\,
      O => \disp[2][0]_INST_0_i_97_n_0\
    );
\disp[2][0]_INST_0_i_98\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \inst_int_to_string/cent\(6),
      O => \disp[2][0]_INST_0_i_98_n_0\
    );
\disp[2][0]_INST_0_i_99\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \inst_int_to_string/cent\(6),
      O => \disp[2][0]_INST_0_i_99_n_0\
    );
\disp[2][1]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[2]_OBUF\(1),
      O => \disp[2]\(1)
    );
\disp[2][1]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0958"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_1_n_4\,
      I1 => \disp[2][0]_INST_0_i_1_n_5\,
      I2 => \disp[2][0]_INST_0_i_1_n_6\,
      I3 => \disp[2][3]_INST_0_i_2_n_7\,
      O => \disp[2]_OBUF\(1)
    );
\disp[2][2]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[2]_OBUF\(2),
      O => \disp[2]\(2)
    );
\disp[2][2]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"D102"
    )
        port map (
      I0 => \disp[2][3]_INST_0_i_2_n_7\,
      I1 => \disp[2][0]_INST_0_i_1_n_4\,
      I2 => \disp[2][0]_INST_0_i_1_n_6\,
      I3 => \disp[2][0]_INST_0_i_1_n_5\,
      O => \disp[2]_OBUF\(2)
    );
\disp[2][3]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[2]_OBUF\(3),
      O => \disp[2]\(3)
    );
\disp[2][3]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"444A"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_1_n_4\,
      I1 => \disp[2][3]_INST_0_i_2_n_7\,
      I2 => \disp[2][0]_INST_0_i_1_n_6\,
      I3 => \disp[2][0]_INST_0_i_1_n_5\,
      O => \disp[2]_OBUF\(3)
    );
\disp[2][3]_INST_0_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_1_n_0\,
      CO(3 downto 0) => \NLW_disp[2][3]_INST_0_i_2_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_disp[2][3]_INST_0_i_2_O_UNCONNECTED\(3 downto 1),
      O(0) => \disp[2][3]_INST_0_i_2_n_7\,
      S(3 downto 1) => B"000",
      S(0) => \disp[2][3]_INST_0_i_3_n_0\
    );
\disp[2][3]_INST_0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_13_n_5\,
      I1 => \disp[2][3]_INST_0_i_4_n_0\,
      O => \disp[2][3]_INST_0_i_3_n_0\
    );
\disp[2][3]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4444777744477774"
    )
        port map (
      I0 => total_out(4),
      I1 => \disp[1]_OBUF\(1),
      I2 => \disp[2][0]_INST_0_i_15_n_4\,
      I3 => \disp[2][0]_INST_0_i_15_n_5\,
      I4 => \disp[5][0]_INST_0_i_93_n_7\,
      I5 => \disp[2][0]_INST_0_i_16_n_0\,
      O => \disp[2][3]_INST_0_i_4_n_0\
    );
\disp[2][4]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[2]\(4)
    );
\disp[2][5]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[2]\(5)
    );
\disp[2][6]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[2]\(6)
    );
\disp[2][7]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[2]\(7)
    );
\disp[3][0]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[3]_OBUF\(0),
      O => \disp[3]\(0)
    );
\disp[3][0]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F696FFFF00009690"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_2_n_0\,
      I1 => \disp[3][3]_INST_0_i_5_n_0\,
      I2 => \disp[3][0]_INST_0_i_2_n_0\,
      I3 => \disp[3][0]_INST_0_i_3_n_0\,
      I4 => \disp[3][3]_INST_0_i_3_n_0\,
      I5 => \disp[3][0]_INST_0_i_4_n_0\,
      O => \disp[3]_OBUF\(0)
    );
\disp[3][0]_INST_0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"55555555A99AA66A"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_8_n_0\,
      I1 => \inst_int_to_string/string_cent_decenas[1]5\(1),
      I2 => \disp[3][0]_INST_0_i_5_n_0\,
      I3 => \disp[2]_OBUF\(0),
      I4 => \disp[3][0]_INST_0_i_6_n_0\,
      I5 => \disp[3][3]_INST_0_i_2_n_0\,
      O => \disp[3][0]_INST_0_i_2_n_0\
    );
\disp[3][0]_INST_0_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69999996"
    )
        port map (
      I0 => \inst_int_to_string/string_cent_decenas[1]5\(1),
      I1 => \disp[3][0]_INST_0_i_6_n_0\,
      I2 => \disp[3][0]_INST_0_i_5_n_0\,
      I3 => \disp[2]_OBUF\(0),
      I4 => \disp[3][3]_INST_0_i_2_n_0\,
      O => \disp[3][0]_INST_0_i_3_n_0\
    );
\disp[3][0]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"69424297D69796BD"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_3_n_0\,
      I1 => \disp[3][2]_INST_0_i_6_n_0\,
      I2 => \disp[3][2]_INST_0_i_5_n_0\,
      I3 => \disp[3][2]_INST_0_i_4_n_0\,
      I4 => \disp[3][2]_INST_0_i_2_n_0\,
      I5 => \disp[3][0]_INST_0_i_2_n_0\,
      O => \disp[3][0]_INST_0_i_4_n_0\
    );
\disp[3][0]_INST_0_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => total_out(0),
      I1 => \disp[1]_OBUF\(1),
      I2 => \inst_int_to_string/p_1_in\(0),
      O => \disp[3][0]_INST_0_i_5_n_0\
    );
\disp[3][0]_INST_0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CD99"
    )
        port map (
      I0 => \disp[2][3]_INST_0_i_2_n_7\,
      I1 => \disp[2][0]_INST_0_i_1_n_6\,
      I2 => \disp[2][0]_INST_0_i_1_n_5\,
      I3 => \disp[2][0]_INST_0_i_1_n_4\,
      O => \disp[3][0]_INST_0_i_6_n_0\
    );
\disp[3][1]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[3]_OBUF\(1),
      O => \disp[3]\(1)
    );
\disp[3][1]_INST_0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \disp[3][3]_INST_0_i_6_n_0\,
      I1 => \disp[3][3]_INST_0_i_3_n_0\,
      I2 => \disp[3][3]_INST_0_i_2_n_0\,
      O => \disp[3]_OBUF\(1)
    );
\disp[3][2]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[3]_OBUF\(2),
      O => \disp[3]\(2)
    );
\disp[3][2]_INST_0_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"301C1C0C"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_2_n_0\,
      I1 => \disp[3][2]_INST_0_i_3_n_0\,
      I2 => \disp[3][2]_INST_0_i_4_n_0\,
      I3 => \disp[3][2]_INST_0_i_5_n_0\,
      I4 => \disp[3][2]_INST_0_i_6_n_0\,
      O => \disp[3]_OBUF\(2)
    );
\disp[3][2]_INST_0_i_10\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \disp[3][2]_INST_0_i_10_n_0\,
      CO(2) => \disp[3][2]_INST_0_i_10_n_1\,
      CO(1) => \disp[3][2]_INST_0_i_10_n_2\,
      CO(0) => \disp[3][2]_INST_0_i_10_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => \inst_int_to_string/cent\(3 downto 0),
      O(3) => \disp[3][2]_INST_0_i_10_n_4\,
      O(2) => \disp[3][2]_INST_0_i_10_n_5\,
      O(1) => \inst_int_to_string/string_cent_decenas[1]5\(1),
      O(0) => \NLW_disp[3][2]_INST_0_i_10_O_UNCONNECTED\(0),
      S(3) => \disp[3][2]_INST_0_i_25_n_0\,
      S(2) => \disp[3][2]_INST_0_i_26_n_0\,
      S(1) => \disp[3][2]_INST_0_i_27_n_0\,
      S(0) => \disp[3][2]_INST_0_i_28_n_0\
    );
\disp[3][2]_INST_0_i_100\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1EE1"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_10_n_0\,
      I1 => \disp[2][0]_INST_0_i_12_n_0\,
      I2 => \disp[2][3]_INST_0_i_4_n_0\,
      I3 => \disp[2][0]_INST_0_i_11_n_0\,
      O => \disp[3][2]_INST_0_i_100_n_0\
    );
\disp[3][2]_INST_0_i_101\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6669"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_12_n_0\,
      I1 => \disp[2][0]_INST_0_i_10_n_0\,
      I2 => \disp[2][0]_INST_0_i_11_n_0\,
      I3 => \disp[3][0]_INST_0_i_5_n_0\,
      O => \disp[3][2]_INST_0_i_101_n_0\
    );
\disp[3][2]_INST_0_i_102\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \disp[3][2]_INST_0_i_102_n_0\,
      CO(2) => \disp[3][2]_INST_0_i_102_n_1\,
      CO(1) => \disp[3][2]_INST_0_i_102_n_2\,
      CO(0) => \disp[3][2]_INST_0_i_102_n_3\,
      CYINIT => '0',
      DI(3) => \disp[3][0]_INST_0_i_5_n_0\,
      DI(2) => \disp[3][2]_INST_0_i_138_n_0\,
      DI(1 downto 0) => B"01",
      O(3) => \disp[3][2]_INST_0_i_102_n_4\,
      O(2) => \disp[3][2]_INST_0_i_102_n_5\,
      O(1) => \disp[3][2]_INST_0_i_102_n_6\,
      O(0) => \NLW_disp[3][2]_INST_0_i_102_O_UNCONNECTED\(0),
      S(3) => \disp[3][2]_INST_0_i_139_n_0\,
      S(2) => \disp[3][2]_INST_0_i_140_n_0\,
      S(1) => \disp[3][2]_INST_0_i_141_n_0\,
      S(0) => \disp[3][2]_INST_0_i_142_n_0\
    );
\disp[3][2]_INST_0_i_103\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4D"
    )
        port map (
      I0 => \inst_int_to_string/cent\(6),
      I1 => \disp[2][3]_INST_0_i_4_n_0\,
      I2 => \disp[2][0]_INST_0_i_11_n_0\,
      O => \disp[3][2]_INST_0_i_103_n_0\
    );
\disp[3][2]_INST_0_i_104\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_11_n_0\,
      I1 => \inst_int_to_string/cent\(6),
      I2 => \disp[2][3]_INST_0_i_4_n_0\,
      O => \disp[3][2]_INST_0_i_104_n_0\
    );
\disp[3][2]_INST_0_i_105\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D4"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_5_n_0\,
      I1 => \disp[2][0]_INST_0_i_11_n_0\,
      I2 => \disp[2][3]_INST_0_i_4_n_0\,
      O => \disp[3][2]_INST_0_i_105_n_0\
    );
\disp[3][2]_INST_0_i_106\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \disp[2][3]_INST_0_i_4_n_0\,
      I1 => \disp[2][0]_INST_0_i_11_n_0\,
      I2 => \disp[3][0]_INST_0_i_5_n_0\,
      O => \disp[3][2]_INST_0_i_106_n_0\
    );
\disp[3][2]_INST_0_i_107\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B24D4DB2"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_11_n_0\,
      I1 => \disp[2][3]_INST_0_i_4_n_0\,
      I2 => \inst_int_to_string/cent\(6),
      I3 => \inst_int_to_string/cent\(5),
      I4 => \disp[2][0]_INST_0_i_10_n_0\,
      O => \disp[3][2]_INST_0_i_107_n_0\
    );
\disp[3][2]_INST_0_i_108\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8E71718E718E8E71"
    )
        port map (
      I0 => \inst_int_to_string/cent\(5),
      I1 => \disp[2][0]_INST_0_i_12_n_0\,
      I2 => \disp[2][0]_INST_0_i_10_n_0\,
      I3 => \disp[2][3]_INST_0_i_4_n_0\,
      I4 => \inst_int_to_string/cent\(6),
      I5 => \disp[2][0]_INST_0_i_11_n_0\,
      O => \disp[3][2]_INST_0_i_108_n_0\
    );
\disp[3][2]_INST_0_i_109\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_105_n_0\,
      I1 => \disp[2][0]_INST_0_i_10_n_0\,
      I2 => \disp[2][0]_INST_0_i_12_n_0\,
      I3 => \inst_int_to_string/cent\(5),
      O => \disp[3][2]_INST_0_i_109_n_0\
    );
\disp[3][2]_INST_0_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEAAAAAAAAA"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_9_n_4\,
      I1 => \disp[3][2]_INST_0_i_9_n_5\,
      I2 => \disp[3][2]_INST_0_i_10_n_4\,
      I3 => \disp[3][2]_INST_0_i_10_n_5\,
      I4 => \disp[3][2]_INST_0_i_9_n_7\,
      I5 => \disp[3][2]_INST_0_i_9_n_6\,
      O => \disp[3][2]_INST_0_i_11_n_0\
    );
\disp[3][2]_INST_0_i_110\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696969"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_5_n_0\,
      I1 => \disp[2][0]_INST_0_i_11_n_0\,
      I2 => \disp[2][3]_INST_0_i_4_n_0\,
      I3 => \disp[2][0]_INST_0_i_12_n_0\,
      I4 => \disp[2][0]_INST_0_i_10_n_0\,
      O => \disp[3][2]_INST_0_i_110_n_0\
    );
\disp[3][2]_INST_0_i_111\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \disp[3][2]_INST_0_i_111_n_0\,
      CO(2) => \disp[3][2]_INST_0_i_111_n_1\,
      CO(1) => \disp[3][2]_INST_0_i_111_n_2\,
      CO(0) => \disp[3][2]_INST_0_i_111_n_3\,
      CYINIT => '0',
      DI(3) => \disp[3][2]_INST_0_i_143_n_0\,
      DI(2 downto 0) => B"001",
      O(3) => \disp[3][2]_INST_0_i_111_n_4\,
      O(2) => \disp[3][2]_INST_0_i_111_n_5\,
      O(1) => \disp[3][2]_INST_0_i_111_n_6\,
      O(0) => \NLW_disp[3][2]_INST_0_i_111_O_UNCONNECTED\(0),
      S(3) => \disp[3][2]_INST_0_i_144_n_0\,
      S(2) => \disp[3][2]_INST_0_i_145_n_0\,
      S(1) => \disp[3][2]_INST_0_i_146_n_0\,
      S(0) => \disp[3][2]_INST_0_i_147_n_0\
    );
\disp[3][2]_INST_0_i_112\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[2][3]_INST_0_i_4_n_0\,
      O => \inst_int_to_string/cent\(4)
    );
\disp[3][2]_INST_0_i_113\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_10_n_0\,
      O => \disp[3][2]_INST_0_i_113_n_0\
    );
\disp[3][2]_INST_0_i_114\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_11_n_0\,
      O => \disp[3][2]_INST_0_i_114_n_0\
    );
\disp[3][2]_INST_0_i_115\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_12_n_0\,
      O => \disp[3][2]_INST_0_i_115_n_0\
    );
\disp[3][2]_INST_0_i_116\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4444777744477774"
    )
        port map (
      I0 => total_out(4),
      I1 => \disp[1]_OBUF\(1),
      I2 => \disp[2][0]_INST_0_i_15_n_4\,
      I3 => \disp[2][0]_INST_0_i_15_n_5\,
      I4 => \disp[5][0]_INST_0_i_93_n_7\,
      I5 => \disp[2][0]_INST_0_i_16_n_0\,
      O => \disp[3][2]_INST_0_i_116_n_0\
    );
\disp[3][2]_INST_0_i_117\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_10_n_0\,
      I1 => \inst_int_to_string/cent\(6),
      O => \disp[3][2]_INST_0_i_117_n_0\
    );
\disp[3][2]_INST_0_i_118\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_11_n_0\,
      I1 => \inst_int_to_string/cent\(5),
      O => \disp[3][2]_INST_0_i_118_n_0\
    );
\disp[3][2]_INST_0_i_119\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_12_n_0\,
      I1 => \disp[2][3]_INST_0_i_4_n_0\,
      O => \disp[3][2]_INST_0_i_119_n_0\
    );
\disp[3][2]_INST_0_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"32660000FFFF3266"
    )
        port map (
      I0 => \disp[2][3]_INST_0_i_2_n_7\,
      I1 => \disp[2][0]_INST_0_i_1_n_6\,
      I2 => \disp[2][0]_INST_0_i_1_n_5\,
      I3 => \disp[2][0]_INST_0_i_1_n_4\,
      I4 => \disp[3][3]_INST_0_i_12_n_0\,
      I5 => \inst_int_to_string/string_cent_decenas[1]5\(1),
      O => \disp[3][2]_INST_0_i_12_n_0\
    );
\disp[3][2]_INST_0_i_120\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \disp[3][2]_INST_0_i_120_n_0\,
      CO(2) => \disp[3][2]_INST_0_i_120_n_1\,
      CO(1) => \disp[3][2]_INST_0_i_120_n_2\,
      CO(0) => \disp[3][2]_INST_0_i_120_n_3\,
      CYINIT => '0',
      DI(3) => \disp[3][2]_INST_0_i_148_n_0\,
      DI(2) => \disp[3][2]_INST_0_i_149_n_0\,
      DI(1) => \disp[3][2]_INST_0_i_150_n_0\,
      DI(0) => \disp[3][2]_INST_0_i_151_n_0\,
      O(3 downto 0) => \NLW_disp[3][2]_INST_0_i_120_O_UNCONNECTED\(3 downto 0),
      S(3) => \disp[3][2]_INST_0_i_152_n_0\,
      S(2) => \disp[3][2]_INST_0_i_153_n_0\,
      S(1) => \disp[3][2]_INST_0_i_154_n_0\,
      S(0) => \disp[3][2]_INST_0_i_155_n_0\
    );
\disp[3][2]_INST_0_i_121\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0880"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_73_n_5\,
      I1 => \disp[3][2]_INST_0_i_111_n_6\,
      I2 => \disp[3][2]_INST_0_i_73_n_4\,
      I3 => \disp[3][2]_INST_0_i_111_n_5\,
      O => \disp[3][2]_INST_0_i_121_n_0\
    );
\disp[3][2]_INST_0_i_122\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"2828BE28"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_156_n_2\,
      I1 => \disp[3][2]_INST_0_i_73_n_5\,
      I2 => \disp[3][2]_INST_0_i_111_n_6\,
      I3 => \disp[3][2]_INST_0_i_73_n_6\,
      I4 => \disp[3][0]_INST_0_i_5_n_0\,
      O => \disp[3][2]_INST_0_i_122_n_0\
    );
\disp[3][2]_INST_0_i_123\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"82"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_156_n_7\,
      I1 => \disp[3][2]_INST_0_i_73_n_6\,
      I2 => \disp[3][0]_INST_0_i_5_n_0\,
      O => \disp[3][2]_INST_0_i_123_n_0\
    );
\disp[3][2]_INST_0_i_124\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_157_n_4\,
      I1 => \disp[3][2]_INST_0_i_73_n_7\,
      O => \disp[3][2]_INST_0_i_124_n_0\
    );
\disp[3][2]_INST_0_i_125\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F087870F"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_111_n_6\,
      I1 => \disp[3][2]_INST_0_i_73_n_5\,
      I2 => \disp[3][2]_INST_0_i_158_n_0\,
      I3 => \disp[3][2]_INST_0_i_73_n_4\,
      I4 => \disp[3][2]_INST_0_i_111_n_5\,
      O => \disp[3][2]_INST_0_i_125_n_0\
    );
\disp[3][2]_INST_0_i_126\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_122_n_0\,
      I1 => \disp[3][2]_INST_0_i_111_n_5\,
      I2 => \disp[3][2]_INST_0_i_73_n_4\,
      I3 => \disp[3][2]_INST_0_i_111_n_6\,
      I4 => \disp[3][2]_INST_0_i_73_n_5\,
      O => \disp[3][2]_INST_0_i_126_n_0\
    );
\disp[3][2]_INST_0_i_127\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6996699696696996"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_123_n_0\,
      I1 => \disp[3][2]_INST_0_i_156_n_2\,
      I2 => \disp[3][2]_INST_0_i_73_n_5\,
      I3 => \disp[3][2]_INST_0_i_111_n_6\,
      I4 => \disp[3][2]_INST_0_i_73_n_6\,
      I5 => \disp[3][0]_INST_0_i_5_n_0\,
      O => \disp[3][2]_INST_0_i_127_n_0\
    );
\disp[3][2]_INST_0_i_128\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_156_n_7\,
      I1 => \disp[3][2]_INST_0_i_73_n_6\,
      I2 => \disp[3][0]_INST_0_i_5_n_0\,
      I3 => \disp[3][2]_INST_0_i_124_n_0\,
      O => \disp[3][2]_INST_0_i_128_n_0\
    );
\disp[3][2]_INST_0_i_129\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2B"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_11_n_0\,
      I1 => \disp[3][2]_INST_0_i_79_n_6\,
      I2 => \disp[3][2]_INST_0_i_56_n_5\,
      O => \disp[3][2]_INST_0_i_129_n_0\
    );
\disp[3][2]_INST_0_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6675"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_1_n_5\,
      I1 => \disp[2][0]_INST_0_i_1_n_6\,
      I2 => \disp[2][0]_INST_0_i_1_n_4\,
      I3 => \disp[2][3]_INST_0_i_2_n_7\,
      O => \disp[3][2]_INST_0_i_13_n_0\
    );
\disp[3][2]_INST_0_i_130\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_56_n_5\,
      I1 => \disp[2][0]_INST_0_i_11_n_0\,
      I2 => \disp[3][2]_INST_0_i_79_n_6\,
      O => \disp[3][2]_INST_0_i_130_n_0\
    );
\disp[3][2]_INST_0_i_131\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_56_n_6\,
      I1 => \disp[2][0]_INST_0_i_12_n_0\,
      I2 => \disp[3][2]_INST_0_i_79_n_7\,
      O => \disp[3][2]_INST_0_i_131_n_0\
    );
\disp[3][2]_INST_0_i_132\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_out(5),
      I1 => \disp[1]_OBUF\(1),
      I2 => \disp[5][0]_INST_0_i_92_n_0\,
      O => \disp[3][2]_INST_0_i_132_n_0\
    );
\disp[3][2]_INST_0_i_133\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_5_n_0\,
      O => \disp[3][2]_INST_0_i_133_n_0\
    );
\disp[3][2]_INST_0_i_134\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_5_n_0\,
      I1 => \disp[2][0]_INST_0_i_11_n_0\,
      I2 => \inst_int_to_string/cent\(6),
      O => \disp[3][2]_INST_0_i_134_n_0\
    );
\disp[3][2]_INST_0_i_135\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \inst_int_to_string/cent\(5),
      I1 => \disp[2][0]_INST_0_i_12_n_0\,
      O => \disp[3][2]_INST_0_i_135_n_0\
    );
\disp[3][2]_INST_0_i_136\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \disp[2][3]_INST_0_i_4_n_0\,
      I1 => \disp[3][0]_INST_0_i_5_n_0\,
      O => \disp[3][2]_INST_0_i_136_n_0\
    );
\disp[3][2]_INST_0_i_137\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_10_n_0\,
      O => \disp[3][2]_INST_0_i_137_n_0\
    );
\disp[3][2]_INST_0_i_138\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_5_n_0\,
      O => \disp[3][2]_INST_0_i_138_n_0\
    );
\disp[3][2]_INST_0_i_139\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_5_n_0\,
      I1 => \disp[2][0]_INST_0_i_12_n_0\,
      I2 => \disp[2][0]_INST_0_i_10_n_0\,
      O => \disp[3][2]_INST_0_i_139_n_0\
    );
\disp[3][2]_INST_0_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_out(6),
      I1 => \disp[1]_OBUF\(1),
      I2 => \disp[5][0]_INST_0_i_91_n_0\,
      O => \disp[3][2]_INST_0_i_14_n_0\
    );
\disp[3][2]_INST_0_i_140\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_5_n_0\,
      I1 => \disp[2][0]_INST_0_i_11_n_0\,
      O => \disp[3][2]_INST_0_i_140_n_0\
    );
\disp[3][2]_INST_0_i_141\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => total_out(1),
      I1 => \disp[1]_OBUF\(1),
      I2 => \inst_int_to_string/p_1_in\(1),
      O => \disp[3][2]_INST_0_i_141_n_0\
    );
\disp[3][2]_INST_0_i_142\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_5_n_0\,
      O => \disp[3][2]_INST_0_i_142_n_0\
    );
\disp[3][2]_INST_0_i_143\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_5_n_0\,
      O => \disp[3][2]_INST_0_i_143_n_0\
    );
\disp[3][2]_INST_0_i_144\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_5_n_0\,
      I1 => \disp[2][0]_INST_0_i_10_n_0\,
      O => \disp[3][2]_INST_0_i_144_n_0\
    );
\disp[3][2]_INST_0_i_145\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4774"
    )
        port map (
      I0 => total_out(2),
      I1 => \disp[1]_OBUF\(1),
      I2 => \disp[2][0]_INST_0_i_15_n_5\,
      I3 => \disp[2][0]_INST_0_i_16_n_0\,
      O => \disp[3][2]_INST_0_i_145_n_0\
    );
\disp[3][2]_INST_0_i_146\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => total_out(1),
      I1 => \disp[1]_OBUF\(1),
      I2 => \inst_int_to_string/p_1_in\(1),
      O => \disp[3][2]_INST_0_i_146_n_0\
    );
\disp[3][2]_INST_0_i_147\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_5_n_0\,
      O => \disp[3][2]_INST_0_i_147_n_0\
    );
\disp[3][2]_INST_0_i_148\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_157_n_5\,
      I1 => \disp[3][2]_INST_0_i_102_n_4\,
      O => \disp[3][2]_INST_0_i_148_n_0\
    );
\disp[3][2]_INST_0_i_149\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_157_n_6\,
      I1 => \disp[3][2]_INST_0_i_102_n_5\,
      O => \disp[3][2]_INST_0_i_149_n_0\
    );
\disp[3][2]_INST_0_i_15\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_out(5),
      I1 => \disp[1]_OBUF\(1),
      I2 => \disp[5][0]_INST_0_i_92_n_0\,
      O => \disp[3][2]_INST_0_i_15_n_0\
    );
\disp[3][2]_INST_0_i_150\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_157_n_7\,
      I1 => \disp[3][2]_INST_0_i_102_n_6\,
      O => \disp[3][2]_INST_0_i_150_n_0\
    );
\disp[3][2]_INST_0_i_151\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_159_n_4\,
      I1 => \disp[3][0]_INST_0_i_5_n_0\,
      O => \disp[3][2]_INST_0_i_151_n_0\
    );
\disp[3][2]_INST_0_i_152\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_157_n_4\,
      I1 => \disp[3][2]_INST_0_i_73_n_7\,
      I2 => \disp[3][2]_INST_0_i_102_n_4\,
      I3 => \disp[3][2]_INST_0_i_157_n_5\,
      O => \disp[3][2]_INST_0_i_152_n_0\
    );
\disp[3][2]_INST_0_i_153\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_102_n_5\,
      I1 => \disp[3][2]_INST_0_i_157_n_6\,
      I2 => \disp[3][2]_INST_0_i_102_n_4\,
      I3 => \disp[3][2]_INST_0_i_157_n_5\,
      O => \disp[3][2]_INST_0_i_153_n_0\
    );
\disp[3][2]_INST_0_i_154\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_102_n_6\,
      I1 => \disp[3][2]_INST_0_i_157_n_7\,
      I2 => \disp[3][2]_INST_0_i_102_n_5\,
      I3 => \disp[3][2]_INST_0_i_157_n_6\,
      O => \disp[3][2]_INST_0_i_154_n_0\
    );
\disp[3][2]_INST_0_i_155\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4BB4"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_5_n_0\,
      I1 => \disp[3][2]_INST_0_i_159_n_4\,
      I2 => \disp[3][2]_INST_0_i_102_n_6\,
      I3 => \disp[3][2]_INST_0_i_157_n_7\,
      O => \disp[3][2]_INST_0_i_155_n_0\
    );
\disp[3][2]_INST_0_i_156\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[3][2]_INST_0_i_157_n_0\,
      CO(3 downto 2) => \NLW_disp[3][2]_INST_0_i_156_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \disp[3][2]_INST_0_i_156_n_2\,
      CO(0) => \NLW_disp[3][2]_INST_0_i_156_CO_UNCONNECTED\(0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_disp[3][2]_INST_0_i_156_O_UNCONNECTED\(3 downto 1),
      O(0) => \disp[3][2]_INST_0_i_156_n_7\,
      S(3 downto 1) => B"001",
      S(0) => \disp[3][2]_INST_0_i_160_n_0\
    );
\disp[3][2]_INST_0_i_157\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[3][2]_INST_0_i_159_n_0\,
      CO(3) => \disp[3][2]_INST_0_i_157_n_0\,
      CO(2) => \disp[3][2]_INST_0_i_157_n_1\,
      CO(1) => \disp[3][2]_INST_0_i_157_n_2\,
      CO(0) => \disp[3][2]_INST_0_i_157_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \disp[3][2]_INST_0_i_161_n_0\,
      O(3) => \disp[3][2]_INST_0_i_157_n_4\,
      O(2) => \disp[3][2]_INST_0_i_157_n_5\,
      O(1) => \disp[3][2]_INST_0_i_157_n_6\,
      O(0) => \disp[3][2]_INST_0_i_157_n_7\,
      S(3) => \disp[3][2]_INST_0_i_162_n_0\,
      S(2) => \disp[3][2]_INST_0_i_163_n_0\,
      S(1) => \disp[3][2]_INST_0_i_164_n_0\,
      S(0) => \disp[3][2]_INST_0_i_165_n_0\
    );
\disp[3][2]_INST_0_i_158\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_56_n_7\,
      I1 => \disp[3][0]_INST_0_i_5_n_0\,
      I2 => \disp[3][2]_INST_0_i_111_n_4\,
      O => \disp[3][2]_INST_0_i_158_n_0\
    );
\disp[3][2]_INST_0_i_159\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[3][2]_INST_0_i_166_n_0\,
      CO(3) => \disp[3][2]_INST_0_i_159_n_0\,
      CO(2) => \disp[3][2]_INST_0_i_159_n_1\,
      CO(1) => \disp[3][2]_INST_0_i_159_n_2\,
      CO(0) => \disp[3][2]_INST_0_i_159_n_3\,
      CYINIT => '0',
      DI(3) => \disp[3][2]_INST_0_i_167_n_0\,
      DI(2) => \disp[3][2]_INST_0_i_168_n_0\,
      DI(1) => \disp[3][2]_INST_0_i_169_n_0\,
      DI(0) => \disp[3][2]_INST_0_i_170_n_0\,
      O(3) => \disp[3][2]_INST_0_i_159_n_4\,
      O(2 downto 0) => \NLW_disp[3][2]_INST_0_i_159_O_UNCONNECTED\(2 downto 0),
      S(3) => \disp[3][2]_INST_0_i_171_n_0\,
      S(2) => \disp[3][2]_INST_0_i_172_n_0\,
      S(1) => \disp[3][2]_INST_0_i_173_n_0\,
      S(0) => \disp[3][2]_INST_0_i_174_n_0\
    );
\disp[3][2]_INST_0_i_16\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[2][3]_INST_0_i_4_n_0\,
      O => \disp[3][2]_INST_0_i_16_n_0\
    );
\disp[3][2]_INST_0_i_160\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_out(6),
      I1 => \disp[1]_OBUF\(1),
      I2 => \disp[5][0]_INST_0_i_91_n_0\,
      O => \disp[3][2]_INST_0_i_160_n_0\
    );
\disp[3][2]_INST_0_i_161\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/cent\(6),
      I1 => \disp[2][0]_INST_0_i_12_n_0\,
      O => \disp[3][2]_INST_0_i_161_n_0\
    );
\disp[3][2]_INST_0_i_162\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_out(5),
      I1 => \disp[1]_OBUF\(1),
      I2 => \disp[5][0]_INST_0_i_92_n_0\,
      O => \disp[3][2]_INST_0_i_162_n_0\
    );
\disp[3][2]_INST_0_i_163\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[2][3]_INST_0_i_4_n_0\,
      O => \disp[3][2]_INST_0_i_163_n_0\
    );
\disp[3][2]_INST_0_i_164\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_10_n_0\,
      O => \disp[3][2]_INST_0_i_164_n_0\
    );
\disp[3][2]_INST_0_i_165\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4B"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_12_n_0\,
      I1 => \inst_int_to_string/cent\(6),
      I2 => \disp[2][0]_INST_0_i_11_n_0\,
      O => \disp[3][2]_INST_0_i_165_n_0\
    );
\disp[3][2]_INST_0_i_166\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \disp[3][2]_INST_0_i_166_n_0\,
      CO(2) => \disp[3][2]_INST_0_i_166_n_1\,
      CO(1) => \disp[3][2]_INST_0_i_166_n_2\,
      CO(0) => \disp[3][2]_INST_0_i_166_n_3\,
      CYINIT => '0',
      DI(3) => \disp[3][2]_INST_0_i_175_n_0\,
      DI(2) => \disp[3][2]_INST_0_i_176_n_0\,
      DI(1) => \disp[3][2]_INST_0_i_177_n_0\,
      DI(0) => '0',
      O(3 downto 0) => \NLW_disp[3][2]_INST_0_i_166_O_UNCONNECTED\(3 downto 0),
      S(3) => \disp[3][2]_INST_0_i_178_n_0\,
      S(2) => \disp[3][2]_INST_0_i_179_n_0\,
      S(1) => \disp[3][2]_INST_0_i_180_n_0\,
      S(0) => \disp[3][2]_INST_0_i_181_n_0\
    );
\disp[3][2]_INST_0_i_167\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \inst_int_to_string/cent\(6),
      I1 => \disp[2][0]_INST_0_i_12_n_0\,
      O => \disp[3][2]_INST_0_i_167_n_0\
    );
\disp[3][2]_INST_0_i_168\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \inst_int_to_string/cent\(5),
      I1 => \disp[3][0]_INST_0_i_5_n_0\,
      O => \disp[3][2]_INST_0_i_168_n_0\
    );
\disp[3][2]_INST_0_i_169\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_out(6),
      I1 => \disp[1]_OBUF\(1),
      I2 => \disp[5][0]_INST_0_i_91_n_0\,
      O => \disp[3][2]_INST_0_i_169_n_0\
    );
\disp[3][2]_INST_0_i_17\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_29_n_5\,
      O => \disp[3][2]_INST_0_i_17_n_0\
    );
\disp[3][2]_INST_0_i_170\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_out(5),
      I1 => \disp[1]_OBUF\(1),
      I2 => \disp[5][0]_INST_0_i_92_n_0\,
      O => \disp[3][2]_INST_0_i_170_n_0\
    );
\disp[3][2]_INST_0_i_171\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9699"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_12_n_0\,
      I1 => \inst_int_to_string/cent\(6),
      I2 => \disp[3][0]_INST_0_i_5_n_0\,
      I3 => \inst_int_to_string/cent\(5),
      O => \disp[3][2]_INST_0_i_171_n_0\
    );
\disp[3][2]_INST_0_i_172\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \inst_int_to_string/cent\(5),
      I1 => \disp[3][0]_INST_0_i_5_n_0\,
      O => \disp[3][2]_INST_0_i_172_n_0\
    );
\disp[3][2]_INST_0_i_173\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \inst_int_to_string/cent\(6),
      I1 => \disp[2][3]_INST_0_i_4_n_0\,
      O => \disp[3][2]_INST_0_i_173_n_0\
    );
\disp[3][2]_INST_0_i_174\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \inst_int_to_string/cent\(5),
      I1 => \disp[2][0]_INST_0_i_10_n_0\,
      O => \disp[3][2]_INST_0_i_174_n_0\
    );
\disp[3][2]_INST_0_i_175\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_11_n_0\,
      O => \disp[3][2]_INST_0_i_175_n_0\
    );
\disp[3][2]_INST_0_i_176\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_10_n_0\,
      O => \disp[3][2]_INST_0_i_176_n_0\
    );
\disp[3][2]_INST_0_i_177\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_11_n_0\,
      O => \disp[3][2]_INST_0_i_177_n_0\
    );
\disp[3][2]_INST_0_i_178\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_11_n_0\,
      I1 => \disp[2][3]_INST_0_i_4_n_0\,
      O => \disp[3][2]_INST_0_i_178_n_0\
    );
\disp[3][2]_INST_0_i_179\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_10_n_0\,
      I1 => \disp[2][0]_INST_0_i_12_n_0\,
      O => \disp[3][2]_INST_0_i_179_n_0\
    );
\disp[3][2]_INST_0_i_18\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \inst_int_to_string/cent\(6),
      I1 => \disp[3][2]_INST_0_i_29_n_6\,
      O => \disp[3][2]_INST_0_i_18_n_0\
    );
\disp[3][2]_INST_0_i_180\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_11_n_0\,
      I1 => \disp[3][0]_INST_0_i_5_n_0\,
      O => \disp[3][2]_INST_0_i_180_n_0\
    );
\disp[3][2]_INST_0_i_181\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_12_n_0\,
      O => \disp[3][2]_INST_0_i_181_n_0\
    );
\disp[3][2]_INST_0_i_19\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \inst_int_to_string/cent\(5),
      I1 => \disp[3][2]_INST_0_i_29_n_7\,
      O => \disp[3][2]_INST_0_i_19_n_0\
    );
\disp[3][2]_INST_0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969696996966996"
    )
        port map (
      I0 => \disp[2]_OBUF\(3),
      I1 => \disp[3][3]_INST_0_i_8_n_0\,
      I2 => \disp[3][3]_INST_0_i_7_n_0\,
      I3 => \disp[3][2]_INST_0_i_7_n_0\,
      I4 => \disp[3][2]_INST_0_i_8_n_0\,
      I5 => \disp[3][3]_INST_0_i_2_n_0\,
      O => \disp[3][2]_INST_0_i_2_n_0\
    );
\disp[3][2]_INST_0_i_20\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \disp[2][3]_INST_0_i_4_n_0\,
      I1 => \disp[3][2]_INST_0_i_32_n_5\,
      O => \disp[3][2]_INST_0_i_20_n_0\
    );
\disp[3][2]_INST_0_i_21\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_10_n_0\,
      O => \inst_int_to_string/cent\(3)
    );
\disp[3][2]_INST_0_i_22\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_11_n_0\,
      O => \inst_int_to_string/cent\(2)
    );
\disp[3][2]_INST_0_i_23\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_12_n_0\,
      O => \inst_int_to_string/cent\(1)
    );
\disp[3][2]_INST_0_i_24\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_5_n_0\,
      O => \inst_int_to_string/cent\(0)
    );
\disp[3][2]_INST_0_i_25\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_10_n_0\,
      I1 => \disp[3][2]_INST_0_i_32_n_6\,
      O => \disp[3][2]_INST_0_i_25_n_0\
    );
\disp[3][2]_INST_0_i_26\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_11_n_0\,
      I1 => \disp[3][2]_INST_0_i_32_n_7\,
      O => \disp[3][2]_INST_0_i_26_n_0\
    );
\disp[3][2]_INST_0_i_27\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => total_out(1),
      I1 => \disp[1]_OBUF\(1),
      I2 => \inst_int_to_string/p_1_in\(1),
      O => \disp[3][2]_INST_0_i_27_n_0\
    );
\disp[3][2]_INST_0_i_28\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => total_out(0),
      I1 => \disp[1]_OBUF\(1),
      I2 => \inst_int_to_string/p_1_in\(0),
      O => \disp[3][2]_INST_0_i_28_n_0\
    );
\disp[3][2]_INST_0_i_29\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3 downto 2) => \NLW_disp[3][2]_INST_0_i_29_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \disp[3][2]_INST_0_i_29_n_2\,
      CO(0) => \disp[3][2]_INST_0_i_29_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \disp[3][2]_INST_0_i_33_n_0\,
      DI(0) => '0',
      O(3) => \NLW_disp[3][2]_INST_0_i_29_O_UNCONNECTED\(3),
      O(2) => \disp[3][2]_INST_0_i_29_n_5\,
      O(1) => \disp[3][2]_INST_0_i_29_n_6\,
      O(0) => \disp[3][2]_INST_0_i_29_n_7\,
      S(3) => '0',
      S(2) => \disp[3][2]_INST_0_i_34_n_0\,
      S(1) => \disp[3][2]_INST_0_i_35_n_0\,
      S(0) => \disp[3][2]_INST_0_i_36_n_0\
    );
\disp[3][2]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4A4A4A4A4A4A4AAA"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_9_n_5\,
      I1 => \disp[3][2]_INST_0_i_9_n_4\,
      I2 => \disp[3][2]_INST_0_i_9_n_6\,
      I3 => \disp[3][2]_INST_0_i_9_n_7\,
      I4 => \disp[3][2]_INST_0_i_10_n_5\,
      I5 => \disp[3][2]_INST_0_i_10_n_4\,
      O => \disp[3][2]_INST_0_i_3_n_0\
    );
\disp[3][2]_INST_0_i_30\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_out(6),
      I1 => \disp[1]_OBUF\(1),
      I2 => \disp[5][0]_INST_0_i_91_n_0\,
      O => \inst_int_to_string/cent\(6)
    );
\disp[3][2]_INST_0_i_31\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_out(5),
      I1 => \disp[1]_OBUF\(1),
      I2 => \disp[5][0]_INST_0_i_92_n_0\,
      O => \inst_int_to_string/cent\(5)
    );
\disp[3][2]_INST_0_i_32\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[3][2]_INST_0_i_37_n_0\,
      CO(3) => \disp[3][2]_INST_0_i_32_n_0\,
      CO(2) => \disp[3][2]_INST_0_i_32_n_1\,
      CO(1) => \disp[3][2]_INST_0_i_32_n_2\,
      CO(0) => \disp[3][2]_INST_0_i_32_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \disp[3][2]_INST_0_i_38_n_0\,
      DI(1) => \disp[3][2]_INST_0_i_39_n_0\,
      DI(0) => \disp[3][2]_INST_0_i_40_n_0\,
      O(3) => \disp[3][2]_INST_0_i_32_n_4\,
      O(2) => \disp[3][2]_INST_0_i_32_n_5\,
      O(1) => \disp[3][2]_INST_0_i_32_n_6\,
      O(0) => \disp[3][2]_INST_0_i_32_n_7\,
      S(3) => \disp[3][2]_INST_0_i_41_n_0\,
      S(2) => \disp[3][2]_INST_0_i_42_n_0\,
      S(1) => \disp[3][2]_INST_0_i_43_n_0\,
      S(0) => \disp[3][2]_INST_0_i_44_n_0\
    );
\disp[3][2]_INST_0_i_33\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_32_n_4\,
      I1 => \disp[3][2]_INST_0_i_32_n_7\,
      O => \disp[3][2]_INST_0_i_33_n_0\
    );
\disp[3][2]_INST_0_i_34\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66969969"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_45_n_6\,
      I1 => \disp[3][2]_INST_0_i_32_n_5\,
      I2 => \disp[3][2]_INST_0_i_45_n_7\,
      I3 => \disp[3][2]_INST_0_i_32_n_6\,
      I4 => \disp[3][2]_INST_0_i_32_n_7\,
      O => \disp[3][2]_INST_0_i_34_n_0\
    );
\disp[3][2]_INST_0_i_35\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_32_n_7\,
      I1 => \disp[3][2]_INST_0_i_32_n_4\,
      I2 => \disp[3][2]_INST_0_i_45_n_7\,
      I3 => \disp[3][2]_INST_0_i_32_n_6\,
      O => \disp[3][2]_INST_0_i_35_n_0\
    );
\disp[3][2]_INST_0_i_36\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_32_n_4\,
      I1 => \disp[3][2]_INST_0_i_32_n_7\,
      O => \disp[3][2]_INST_0_i_36_n_0\
    );
\disp[3][2]_INST_0_i_37\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[3][2]_INST_0_i_46_n_0\,
      CO(3) => \disp[3][2]_INST_0_i_37_n_0\,
      CO(2) => \disp[3][2]_INST_0_i_37_n_1\,
      CO(1) => \disp[3][2]_INST_0_i_37_n_2\,
      CO(0) => \disp[3][2]_INST_0_i_37_n_3\,
      CYINIT => '0',
      DI(3) => \disp[3][2]_INST_0_i_47_n_0\,
      DI(2) => \disp[3][2]_INST_0_i_48_n_0\,
      DI(1) => \disp[3][2]_INST_0_i_49_n_0\,
      DI(0) => \disp[3][2]_INST_0_i_50_n_0\,
      O(3 downto 0) => \NLW_disp[3][2]_INST_0_i_37_O_UNCONNECTED\(3 downto 0),
      S(3) => \disp[3][2]_INST_0_i_51_n_0\,
      S(2) => \disp[3][2]_INST_0_i_52_n_0\,
      S(1) => \disp[3][2]_INST_0_i_53_n_0\,
      S(0) => \disp[3][2]_INST_0_i_54_n_0\
    );
\disp[3][2]_INST_0_i_38\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"28"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_55_n_1\,
      I1 => \disp[3][2]_INST_0_i_56_n_0\,
      I2 => \disp[3][2]_INST_0_i_57_n_1\,
      O => \disp[3][2]_INST_0_i_38_n_0\
    );
\disp[3][2]_INST_0_i_39\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_55_n_6\,
      I1 => \disp[3][2]_INST_0_i_55_n_1\,
      I2 => \disp[3][2]_INST_0_i_57_n_1\,
      I3 => \disp[3][2]_INST_0_i_56_n_0\,
      O => \disp[3][2]_INST_0_i_39_n_0\
    );
\disp[3][2]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"010101FFFE00FE00"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_9_n_7\,
      I1 => \disp[3][2]_INST_0_i_10_n_5\,
      I2 => \disp[3][2]_INST_0_i_10_n_4\,
      I3 => \disp[3][2]_INST_0_i_9_n_4\,
      I4 => \disp[3][2]_INST_0_i_9_n_5\,
      I5 => \disp[3][2]_INST_0_i_9_n_6\,
      O => \disp[3][2]_INST_0_i_4_n_0\
    );
\disp[3][2]_INST_0_i_40\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_55_n_7\,
      I1 => \disp[3][2]_INST_0_i_55_n_6\,
      I2 => \disp[3][2]_INST_0_i_57_n_1\,
      I3 => \disp[3][2]_INST_0_i_56_n_0\,
      O => \disp[3][2]_INST_0_i_40_n_0\
    );
\disp[3][2]_INST_0_i_41\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_56_n_0\,
      I1 => \disp[3][2]_INST_0_i_57_n_1\,
      O => \disp[3][2]_INST_0_i_41_n_0\
    );
\disp[3][2]_INST_0_i_42\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_55_n_1\,
      I1 => \disp[3][2]_INST_0_i_57_n_1\,
      I2 => \disp[3][2]_INST_0_i_56_n_0\,
      O => \disp[3][2]_INST_0_i_42_n_0\
    );
\disp[3][2]_INST_0_i_43\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A995"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_39_n_0\,
      I1 => \disp[3][2]_INST_0_i_57_n_1\,
      I2 => \disp[3][2]_INST_0_i_56_n_0\,
      I3 => \disp[3][2]_INST_0_i_55_n_1\,
      O => \disp[3][2]_INST_0_i_43_n_0\
    );
\disp[3][2]_INST_0_i_44\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCC9C999"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_55_n_6\,
      I1 => \disp[3][2]_INST_0_i_55_n_1\,
      I2 => \disp[3][2]_INST_0_i_57_n_1\,
      I3 => \disp[3][2]_INST_0_i_56_n_0\,
      I4 => \disp[3][2]_INST_0_i_55_n_7\,
      O => \disp[3][2]_INST_0_i_44_n_0\
    );
\disp[3][2]_INST_0_i_45\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[3][2]_INST_0_i_32_n_0\,
      CO(3 downto 1) => \NLW_disp[3][2]_INST_0_i_45_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \disp[3][2]_INST_0_i_45_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 2) => \NLW_disp[3][2]_INST_0_i_45_O_UNCONNECTED\(3 downto 2),
      O(1) => \disp[3][2]_INST_0_i_45_n_6\,
      O(0) => \disp[3][2]_INST_0_i_45_n_7\,
      S(3 downto 2) => B"00",
      S(1) => \disp[3][2]_INST_0_i_58_n_0\,
      S(0) => \disp[3][2]_INST_0_i_59_n_0\
    );
\disp[3][2]_INST_0_i_46\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[3][2]_INST_0_i_60_n_0\,
      CO(3) => \disp[3][2]_INST_0_i_46_n_0\,
      CO(2) => \disp[3][2]_INST_0_i_46_n_1\,
      CO(1) => \disp[3][2]_INST_0_i_46_n_2\,
      CO(0) => \disp[3][2]_INST_0_i_46_n_3\,
      CYINIT => '0',
      DI(3) => \disp[3][2]_INST_0_i_61_n_0\,
      DI(2) => \disp[3][2]_INST_0_i_62_n_0\,
      DI(1) => \disp[3][2]_INST_0_i_63_n_0\,
      DI(0) => \disp[3][2]_INST_0_i_64_n_0\,
      O(3 downto 0) => \NLW_disp[3][2]_INST_0_i_46_O_UNCONNECTED\(3 downto 0),
      S(3) => \disp[3][2]_INST_0_i_65_n_0\,
      S(2) => \disp[3][2]_INST_0_i_66_n_0\,
      S(1) => \disp[3][2]_INST_0_i_67_n_0\,
      S(0) => \disp[3][2]_INST_0_i_68_n_0\
    );
\disp[3][2]_INST_0_i_47\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_69_n_4\,
      I1 => \disp[3][2]_INST_0_i_55_n_7\,
      I2 => \disp[3][2]_INST_0_i_57_n_1\,
      I3 => \disp[3][2]_INST_0_i_56_n_0\,
      O => \disp[3][2]_INST_0_i_47_n_0\
    );
\disp[3][2]_INST_0_i_48\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_69_n_5\,
      I1 => \disp[3][2]_INST_0_i_69_n_4\,
      I2 => \disp[3][2]_INST_0_i_57_n_1\,
      I3 => \disp[3][2]_INST_0_i_56_n_0\,
      O => \disp[3][2]_INST_0_i_48_n_0\
    );
\disp[3][2]_INST_0_i_49\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_69_n_6\,
      I1 => \disp[3][2]_INST_0_i_69_n_5\,
      I2 => \disp[3][2]_INST_0_i_57_n_1\,
      I3 => \disp[3][2]_INST_0_i_56_n_0\,
      O => \disp[3][2]_INST_0_i_49_n_0\
    );
\disp[3][2]_INST_0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFF1000AFFF5000A"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_9_n_4\,
      I1 => \disp[3][2]_INST_0_i_9_n_5\,
      I2 => \disp[3][2]_INST_0_i_10_n_4\,
      I3 => \disp[3][2]_INST_0_i_10_n_5\,
      I4 => \disp[3][2]_INST_0_i_9_n_7\,
      I5 => \disp[3][2]_INST_0_i_9_n_6\,
      O => \disp[3][2]_INST_0_i_5_n_0\
    );
\disp[3][2]_INST_0_i_50\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"044A"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_69_n_6\,
      I1 => \disp[3][2]_INST_0_i_69_n_7\,
      I2 => \disp[3][2]_INST_0_i_56_n_0\,
      I3 => \disp[3][2]_INST_0_i_57_n_1\,
      O => \disp[3][2]_INST_0_i_50_n_0\
    );
\disp[3][2]_INST_0_i_51\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0E1E1C3"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_69_n_4\,
      I1 => \disp[3][2]_INST_0_i_55_n_7\,
      I2 => \disp[3][2]_INST_0_i_55_n_6\,
      I3 => \disp[3][2]_INST_0_i_57_n_1\,
      I4 => \disp[3][2]_INST_0_i_56_n_0\,
      O => \disp[3][2]_INST_0_i_51_n_0\
    );
\disp[3][2]_INST_0_i_52\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0E1E1C3"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_69_n_5\,
      I1 => \disp[3][2]_INST_0_i_69_n_4\,
      I2 => \disp[3][2]_INST_0_i_55_n_7\,
      I3 => \disp[3][2]_INST_0_i_57_n_1\,
      I4 => \disp[3][2]_INST_0_i_56_n_0\,
      O => \disp[3][2]_INST_0_i_52_n_0\
    );
\disp[3][2]_INST_0_i_53\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0E1E1C3"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_69_n_6\,
      I1 => \disp[3][2]_INST_0_i_69_n_5\,
      I2 => \disp[3][2]_INST_0_i_69_n_4\,
      I3 => \disp[3][2]_INST_0_i_57_n_1\,
      I4 => \disp[3][2]_INST_0_i_56_n_0\,
      O => \disp[3][2]_INST_0_i_53_n_0\
    );
\disp[3][2]_INST_0_i_54\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0E1E1C3"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_69_n_7\,
      I1 => \disp[3][2]_INST_0_i_69_n_6\,
      I2 => \disp[3][2]_INST_0_i_69_n_5\,
      I3 => \disp[3][2]_INST_0_i_57_n_1\,
      I4 => \disp[3][2]_INST_0_i_56_n_0\,
      O => \disp[3][2]_INST_0_i_54_n_0\
    );
\disp[3][2]_INST_0_i_55\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[3][2]_INST_0_i_69_n_0\,
      CO(3) => \NLW_disp[3][2]_INST_0_i_55_CO_UNCONNECTED\(3),
      CO(2) => \disp[3][2]_INST_0_i_55_n_1\,
      CO(1) => \NLW_disp[3][2]_INST_0_i_55_CO_UNCONNECTED\(1),
      CO(0) => \disp[3][2]_INST_0_i_55_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \disp[3][2]_INST_0_i_70_n_0\,
      O(3 downto 2) => \NLW_disp[3][2]_INST_0_i_55_O_UNCONNECTED\(3 downto 2),
      O(1) => \disp[3][2]_INST_0_i_55_n_6\,
      O(0) => \disp[3][2]_INST_0_i_55_n_7\,
      S(3 downto 2) => B"01",
      S(1) => \disp[3][2]_INST_0_i_71_n_0\,
      S(0) => \disp[3][2]_INST_0_i_72_n_0\
    );
\disp[3][2]_INST_0_i_56\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[3][2]_INST_0_i_73_n_0\,
      CO(3) => \disp[3][2]_INST_0_i_56_n_0\,
      CO(2) => \NLW_disp[3][2]_INST_0_i_56_CO_UNCONNECTED\(2),
      CO(1) => \disp[3][2]_INST_0_i_56_n_2\,
      CO(0) => \disp[3][2]_INST_0_i_56_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"01",
      DI(1) => \disp[3][2]_INST_0_i_74_n_0\,
      DI(0) => \disp[3][2]_INST_0_i_75_n_0\,
      O(3) => \NLW_disp[3][2]_INST_0_i_56_O_UNCONNECTED\(3),
      O(2) => \disp[3][2]_INST_0_i_56_n_5\,
      O(1) => \disp[3][2]_INST_0_i_56_n_6\,
      O(0) => \disp[3][2]_INST_0_i_56_n_7\,
      S(3) => '1',
      S(2) => \disp[3][2]_INST_0_i_76_n_0\,
      S(1) => \disp[3][2]_INST_0_i_77_n_0\,
      S(0) => \disp[3][2]_INST_0_i_78_n_0\
    );
\disp[3][2]_INST_0_i_57\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[3][2]_INST_0_i_79_n_0\,
      CO(3) => \NLW_disp[3][2]_INST_0_i_57_CO_UNCONNECTED\(3),
      CO(2) => \disp[3][2]_INST_0_i_57_n_1\,
      CO(1) => \NLW_disp[3][2]_INST_0_i_57_CO_UNCONNECTED\(1),
      CO(0) => \disp[3][2]_INST_0_i_57_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \disp[3][2]_INST_0_i_80_n_0\,
      DI(0) => \disp[3][2]_INST_0_i_81_n_0\,
      O(3 downto 2) => \NLW_disp[3][2]_INST_0_i_57_O_UNCONNECTED\(3 downto 2),
      O(1) => \disp[3][2]_INST_0_i_57_n_6\,
      O(0) => \disp[3][2]_INST_0_i_57_n_7\,
      S(3 downto 2) => B"01",
      S(1) => \disp[3][2]_INST_0_i_82_n_0\,
      S(0) => \disp[3][2]_INST_0_i_83_n_0\
    );
\disp[3][2]_INST_0_i_58\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_56_n_0\,
      I1 => \disp[3][2]_INST_0_i_57_n_1\,
      O => \disp[3][2]_INST_0_i_58_n_0\
    );
\disp[3][2]_INST_0_i_59\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_56_n_0\,
      I1 => \disp[3][2]_INST_0_i_57_n_1\,
      O => \disp[3][2]_INST_0_i_59_n_0\
    );
\disp[3][2]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5D34DF7545105D34"
    )
        port map (
      I0 => \disp[2]_OBUF\(3),
      I1 => \disp[3][2]_INST_0_i_10_n_5\,
      I2 => \disp[3][2]_INST_0_i_11_n_0\,
      I3 => \disp[3][2]_INST_0_i_10_n_4\,
      I4 => \disp[3][2]_INST_0_i_12_n_0\,
      I5 => \disp[3][2]_INST_0_i_13_n_0\,
      O => \disp[3][2]_INST_0_i_6_n_0\
    );
\disp[3][2]_INST_0_i_60\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[3][2]_INST_0_i_84_n_0\,
      CO(3) => \disp[3][2]_INST_0_i_60_n_0\,
      CO(2) => \disp[3][2]_INST_0_i_60_n_1\,
      CO(1) => \disp[3][2]_INST_0_i_60_n_2\,
      CO(0) => \disp[3][2]_INST_0_i_60_n_3\,
      CYINIT => '0',
      DI(3) => \disp[3][2]_INST_0_i_85_n_0\,
      DI(2) => \disp[3][2]_INST_0_i_86_n_0\,
      DI(1) => \disp[3][2]_INST_0_i_87_n_0\,
      DI(0) => \disp[3][2]_INST_0_i_88_n_0\,
      O(3 downto 0) => \NLW_disp[3][2]_INST_0_i_60_O_UNCONNECTED\(3 downto 0),
      S(3) => \disp[3][2]_INST_0_i_89_n_0\,
      S(2) => \disp[3][2]_INST_0_i_90_n_0\,
      S(1) => \disp[3][2]_INST_0_i_91_n_0\,
      S(0) => \disp[3][2]_INST_0_i_92_n_0\
    );
\disp[3][2]_INST_0_i_61\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"90660060"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_57_n_1\,
      I1 => \disp[3][2]_INST_0_i_69_n_7\,
      I2 => \disp[3][2]_INST_0_i_93_n_4\,
      I3 => \disp[3][2]_INST_0_i_56_n_0\,
      I4 => \disp[3][2]_INST_0_i_57_n_6\,
      O => \disp[3][2]_INST_0_i_61_n_0\
    );
\disp[3][2]_INST_0_i_62\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60990090"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_57_n_6\,
      I1 => \disp[3][2]_INST_0_i_93_n_4\,
      I2 => \disp[3][2]_INST_0_i_93_n_5\,
      I3 => \disp[3][2]_INST_0_i_56_n_0\,
      I4 => \disp[3][2]_INST_0_i_57_n_7\,
      O => \disp[3][2]_INST_0_i_62_n_0\
    );
\disp[3][2]_INST_0_i_63\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60990090"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_57_n_7\,
      I1 => \disp[3][2]_INST_0_i_93_n_5\,
      I2 => \disp[3][2]_INST_0_i_93_n_6\,
      I3 => \disp[3][2]_INST_0_i_56_n_0\,
      I4 => \disp[3][2]_INST_0_i_79_n_4\,
      O => \disp[3][2]_INST_0_i_63_n_0\
    );
\disp[3][2]_INST_0_i_64\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08E0800E"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_79_n_5\,
      I1 => \disp[3][2]_INST_0_i_93_n_7\,
      I2 => \disp[3][2]_INST_0_i_79_n_4\,
      I3 => \disp[3][2]_INST_0_i_56_n_0\,
      I4 => \disp[3][2]_INST_0_i_93_n_6\,
      O => \disp[3][2]_INST_0_i_64_n_0\
    );
\disp[3][2]_INST_0_i_65\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"566AA995"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_61_n_0\,
      I1 => \disp[3][2]_INST_0_i_57_n_1\,
      I2 => \disp[3][2]_INST_0_i_56_n_0\,
      I3 => \disp[3][2]_INST_0_i_69_n_7\,
      I4 => \disp[3][2]_INST_0_i_69_n_6\,
      O => \disp[3][2]_INST_0_i_65_n_0\
    );
\disp[3][2]_INST_0_i_66\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"95566AA96AA99556"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_62_n_0\,
      I1 => \disp[3][2]_INST_0_i_57_n_6\,
      I2 => \disp[3][2]_INST_0_i_56_n_0\,
      I3 => \disp[3][2]_INST_0_i_93_n_4\,
      I4 => \disp[3][2]_INST_0_i_69_n_7\,
      I5 => \disp[3][2]_INST_0_i_57_n_1\,
      O => \disp[3][2]_INST_0_i_66_n_0\
    );
\disp[3][2]_INST_0_i_67\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AA9955695566AA9"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_63_n_0\,
      I1 => \disp[3][2]_INST_0_i_57_n_7\,
      I2 => \disp[3][2]_INST_0_i_56_n_0\,
      I3 => \disp[3][2]_INST_0_i_93_n_5\,
      I4 => \disp[3][2]_INST_0_i_93_n_4\,
      I5 => \disp[3][2]_INST_0_i_57_n_6\,
      O => \disp[3][2]_INST_0_i_67_n_0\
    );
\disp[3][2]_INST_0_i_68\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AA9955695566AA9"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_64_n_0\,
      I1 => \disp[3][2]_INST_0_i_79_n_4\,
      I2 => \disp[3][2]_INST_0_i_56_n_0\,
      I3 => \disp[3][2]_INST_0_i_93_n_6\,
      I4 => \disp[3][2]_INST_0_i_93_n_5\,
      I5 => \disp[3][2]_INST_0_i_57_n_7\,
      O => \disp[3][2]_INST_0_i_68_n_0\
    );
\disp[3][2]_INST_0_i_69\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[3][2]_INST_0_i_93_n_0\,
      CO(3) => \disp[3][2]_INST_0_i_69_n_0\,
      CO(2) => \disp[3][2]_INST_0_i_69_n_1\,
      CO(1) => \disp[3][2]_INST_0_i_69_n_2\,
      CO(0) => \disp[3][2]_INST_0_i_69_n_3\,
      CYINIT => '0',
      DI(3) => \disp[3][2]_INST_0_i_94_n_0\,
      DI(2) => \disp[3][2]_INST_0_i_95_n_0\,
      DI(1) => \disp[3][2]_INST_0_i_96_n_0\,
      DI(0) => \disp[3][2]_INST_0_i_97_n_0\,
      O(3) => \disp[3][2]_INST_0_i_69_n_4\,
      O(2) => \disp[3][2]_INST_0_i_69_n_5\,
      O(1) => \disp[3][2]_INST_0_i_69_n_6\,
      O(0) => \disp[3][2]_INST_0_i_69_n_7\,
      S(3) => \disp[3][2]_INST_0_i_98_n_0\,
      S(2) => \disp[3][2]_INST_0_i_99_n_0\,
      S(1) => \disp[3][2]_INST_0_i_100_n_0\,
      S(0) => \disp[3][2]_INST_0_i_101_n_0\
    );
\disp[3][2]_INST_0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1428"
    )
        port map (
      I0 => \inst_int_to_string/string_cent_decenas[1]5\(1),
      I1 => \disp[3][0]_INST_0_i_5_n_0\,
      I2 => \disp[2]_OBUF\(0),
      I3 => \disp[3][0]_INST_0_i_6_n_0\,
      O => \disp[3][2]_INST_0_i_7_n_0\
    );
\disp[3][2]_INST_0_i_70\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/cent\(6),
      I1 => \disp[2][3]_INST_0_i_4_n_0\,
      O => \disp[3][2]_INST_0_i_70_n_0\
    );
\disp[3][2]_INST_0_i_71\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_out(6),
      I1 => \disp[1]_OBUF\(1),
      I2 => \disp[5][0]_INST_0_i_91_n_0\,
      O => \disp[3][2]_INST_0_i_71_n_0\
    );
\disp[3][2]_INST_0_i_72\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B4"
    )
        port map (
      I0 => \disp[2][3]_INST_0_i_4_n_0\,
      I1 => \inst_int_to_string/cent\(6),
      I2 => \inst_int_to_string/cent\(5),
      O => \disp[3][2]_INST_0_i_72_n_0\
    );
\disp[3][2]_INST_0_i_73\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[3][2]_INST_0_i_102_n_0\,
      CO(3) => \disp[3][2]_INST_0_i_73_n_0\,
      CO(2) => \disp[3][2]_INST_0_i_73_n_1\,
      CO(1) => \disp[3][2]_INST_0_i_73_n_2\,
      CO(0) => \disp[3][2]_INST_0_i_73_n_3\,
      CYINIT => '0',
      DI(3) => \disp[3][2]_INST_0_i_103_n_0\,
      DI(2) => \disp[3][2]_INST_0_i_104_n_0\,
      DI(1) => \disp[3][2]_INST_0_i_105_n_0\,
      DI(0) => \disp[3][2]_INST_0_i_106_n_0\,
      O(3) => \disp[3][2]_INST_0_i_73_n_4\,
      O(2) => \disp[3][2]_INST_0_i_73_n_5\,
      O(1) => \disp[3][2]_INST_0_i_73_n_6\,
      O(0) => \disp[3][2]_INST_0_i_73_n_7\,
      S(3) => \disp[3][2]_INST_0_i_107_n_0\,
      S(2) => \disp[3][2]_INST_0_i_108_n_0\,
      S(1) => \disp[3][2]_INST_0_i_109_n_0\,
      S(0) => \disp[3][2]_INST_0_i_110_n_0\
    );
\disp[3][2]_INST_0_i_74\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \inst_int_to_string/cent\(6),
      I1 => \disp[2][3]_INST_0_i_4_n_0\,
      O => \disp[3][2]_INST_0_i_74_n_0\
    );
\disp[3][2]_INST_0_i_75\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \inst_int_to_string/cent\(5),
      I1 => \disp[2][0]_INST_0_i_10_n_0\,
      O => \disp[3][2]_INST_0_i_75_n_0\
    );
\disp[3][2]_INST_0_i_76\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \inst_int_to_string/cent\(6),
      O => \disp[3][2]_INST_0_i_76_n_0\
    );
\disp[3][2]_INST_0_i_77\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"87"
    )
        port map (
      I0 => \inst_int_to_string/cent\(6),
      I1 => \disp[2][3]_INST_0_i_4_n_0\,
      I2 => \inst_int_to_string/cent\(5),
      O => \disp[3][2]_INST_0_i_77_n_0\
    );
\disp[3][2]_INST_0_i_78\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \inst_int_to_string/cent\(5),
      I1 => \disp[2][0]_INST_0_i_10_n_0\,
      I2 => \inst_int_to_string/cent\(6),
      I3 => \disp[2][3]_INST_0_i_4_n_0\,
      O => \disp[3][2]_INST_0_i_78_n_0\
    );
\disp[3][2]_INST_0_i_79\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[3][2]_INST_0_i_111_n_0\,
      CO(3) => \disp[3][2]_INST_0_i_79_n_0\,
      CO(2) => \disp[3][2]_INST_0_i_79_n_1\,
      CO(1) => \disp[3][2]_INST_0_i_79_n_2\,
      CO(0) => \disp[3][2]_INST_0_i_79_n_3\,
      CYINIT => '0',
      DI(3) => \inst_int_to_string/cent\(4),
      DI(2) => \disp[3][2]_INST_0_i_113_n_0\,
      DI(1) => \disp[3][2]_INST_0_i_114_n_0\,
      DI(0) => \disp[3][2]_INST_0_i_115_n_0\,
      O(3) => \disp[3][2]_INST_0_i_79_n_4\,
      O(2) => \disp[3][2]_INST_0_i_79_n_5\,
      O(1) => \disp[3][2]_INST_0_i_79_n_6\,
      O(0) => \disp[3][2]_INST_0_i_79_n_7\,
      S(3) => \disp[3][2]_INST_0_i_116_n_0\,
      S(2) => \disp[3][2]_INST_0_i_117_n_0\,
      S(1) => \disp[3][2]_INST_0_i_118_n_0\,
      S(0) => \disp[3][2]_INST_0_i_119_n_0\
    );
\disp[3][2]_INST_0_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A6656559599A9AA6"
    )
        port map (
      I0 => \disp[3][3]_INST_0_i_14_n_0\,
      I1 => \inst_int_to_string/string_cent_decenas[1]5\(1),
      I2 => \disp[3][3]_INST_0_i_12_n_0\,
      I3 => \disp[3][3]_INST_0_i_13_n_0\,
      I4 => \disp[2][0]_INST_0_i_1_n_6\,
      I5 => \disp[2][0]_INST_0_i_1_n_5\,
      O => \disp[3][2]_INST_0_i_8_n_0\
    );
\disp[3][2]_INST_0_i_80\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_out(6),
      I1 => \disp[1]_OBUF\(1),
      I2 => \disp[5][0]_INST_0_i_91_n_0\,
      O => \disp[3][2]_INST_0_i_80_n_0\
    );
\disp[3][2]_INST_0_i_81\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => total_out(5),
      I1 => \disp[1]_OBUF\(1),
      I2 => \disp[5][0]_INST_0_i_92_n_0\,
      O => \disp[3][2]_INST_0_i_81_n_0\
    );
\disp[3][2]_INST_0_i_82\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \inst_int_to_string/cent\(6),
      O => \disp[3][2]_INST_0_i_82_n_0\
    );
\disp[3][2]_INST_0_i_83\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \inst_int_to_string/cent\(5),
      O => \disp[3][2]_INST_0_i_83_n_0\
    );
\disp[3][2]_INST_0_i_84\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[3][2]_INST_0_i_120_n_0\,
      CO(3) => \disp[3][2]_INST_0_i_84_n_0\,
      CO(2) => \disp[3][2]_INST_0_i_84_n_1\,
      CO(1) => \disp[3][2]_INST_0_i_84_n_2\,
      CO(0) => \disp[3][2]_INST_0_i_84_n_3\,
      CYINIT => '0',
      DI(3) => \disp[3][2]_INST_0_i_121_n_0\,
      DI(2) => \disp[3][2]_INST_0_i_122_n_0\,
      DI(1) => \disp[3][2]_INST_0_i_123_n_0\,
      DI(0) => \disp[3][2]_INST_0_i_124_n_0\,
      O(3 downto 0) => \NLW_disp[3][2]_INST_0_i_84_O_UNCONNECTED\(3 downto 0),
      S(3) => \disp[3][2]_INST_0_i_125_n_0\,
      S(2) => \disp[3][2]_INST_0_i_126_n_0\,
      S(1) => \disp[3][2]_INST_0_i_127_n_0\,
      S(0) => \disp[3][2]_INST_0_i_128_n_0\
    );
\disp[3][2]_INST_0_i_85\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"008E8E008E00008E"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_56_n_5\,
      I1 => \disp[3][2]_INST_0_i_79_n_6\,
      I2 => \disp[2][0]_INST_0_i_11_n_0\,
      I3 => \disp[3][2]_INST_0_i_56_n_0\,
      I4 => \disp[3][2]_INST_0_i_93_n_7\,
      I5 => \disp[3][2]_INST_0_i_79_n_5\,
      O => \disp[3][2]_INST_0_i_85_n_0\
    );
\disp[3][2]_INST_0_i_86\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00B2B200B20000B2"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_56_n_6\,
      I1 => \disp[2][0]_INST_0_i_12_n_0\,
      I2 => \disp[3][2]_INST_0_i_79_n_7\,
      I3 => \disp[3][2]_INST_0_i_79_n_6\,
      I4 => \disp[2][0]_INST_0_i_11_n_0\,
      I5 => \disp[3][2]_INST_0_i_56_n_5\,
      O => \disp[3][2]_INST_0_i_86_n_0\
    );
\disp[3][2]_INST_0_i_87\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00B2B200B20000B2"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_56_n_7\,
      I1 => \disp[3][0]_INST_0_i_5_n_0\,
      I2 => \disp[3][2]_INST_0_i_111_n_4\,
      I3 => \disp[3][2]_INST_0_i_79_n_7\,
      I4 => \disp[2][0]_INST_0_i_12_n_0\,
      I5 => \disp[3][2]_INST_0_i_56_n_6\,
      O => \disp[3][2]_INST_0_i_87_n_0\
    );
\disp[3][2]_INST_0_i_88\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08808008"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_73_n_4\,
      I1 => \disp[3][2]_INST_0_i_111_n_5\,
      I2 => \disp[3][2]_INST_0_i_111_n_4\,
      I3 => \disp[3][0]_INST_0_i_5_n_0\,
      I4 => \disp[3][2]_INST_0_i_56_n_7\,
      O => \disp[3][2]_INST_0_i_88_n_0\
    );
\disp[3][2]_INST_0_i_89\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6996996699669669"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_85_n_0\,
      I1 => \disp[3][2]_INST_0_i_93_n_6\,
      I2 => \disp[3][2]_INST_0_i_56_n_0\,
      I3 => \disp[3][2]_INST_0_i_79_n_4\,
      I4 => \disp[3][2]_INST_0_i_93_n_7\,
      I5 => \disp[3][2]_INST_0_i_79_n_5\,
      O => \disp[3][2]_INST_0_i_89_n_0\
    );
\disp[3][2]_INST_0_i_9\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[3][2]_INST_0_i_10_n_0\,
      CO(3) => \NLW_disp[3][2]_INST_0_i_9_CO_UNCONNECTED\(3),
      CO(2) => \disp[3][2]_INST_0_i_9_n_1\,
      CO(1) => \disp[3][2]_INST_0_i_9_n_2\,
      CO(0) => \disp[3][2]_INST_0_i_9_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \disp[3][2]_INST_0_i_14_n_0\,
      DI(1) => \disp[3][2]_INST_0_i_15_n_0\,
      DI(0) => \disp[3][2]_INST_0_i_16_n_0\,
      O(3) => \disp[3][2]_INST_0_i_9_n_4\,
      O(2) => \disp[3][2]_INST_0_i_9_n_5\,
      O(1) => \disp[3][2]_INST_0_i_9_n_6\,
      O(0) => \disp[3][2]_INST_0_i_9_n_7\,
      S(3) => \disp[3][2]_INST_0_i_17_n_0\,
      S(2) => \disp[3][2]_INST_0_i_18_n_0\,
      S(1) => \disp[3][2]_INST_0_i_19_n_0\,
      S(0) => \disp[3][2]_INST_0_i_20_n_0\
    );
\disp[3][2]_INST_0_i_90\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696996"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_86_n_0\,
      I1 => \disp[3][2]_INST_0_i_79_n_5\,
      I2 => \disp[3][2]_INST_0_i_93_n_7\,
      I3 => \disp[3][2]_INST_0_i_56_n_0\,
      I4 => \disp[3][2]_INST_0_i_129_n_0\,
      O => \disp[3][2]_INST_0_i_90_n_0\
    );
\disp[3][2]_INST_0_i_91\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69669969"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_87_n_0\,
      I1 => \disp[3][2]_INST_0_i_130_n_0\,
      I2 => \disp[3][2]_INST_0_i_56_n_6\,
      I3 => \disp[2][0]_INST_0_i_12_n_0\,
      I4 => \disp[3][2]_INST_0_i_79_n_7\,
      O => \disp[3][2]_INST_0_i_91_n_0\
    );
\disp[3][2]_INST_0_i_92\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F87F80F807807F07"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_111_n_5\,
      I1 => \disp[3][2]_INST_0_i_73_n_4\,
      I2 => \disp[3][2]_INST_0_i_56_n_7\,
      I3 => \disp[3][0]_INST_0_i_5_n_0\,
      I4 => \disp[3][2]_INST_0_i_111_n_4\,
      I5 => \disp[3][2]_INST_0_i_131_n_0\,
      O => \disp[3][2]_INST_0_i_92_n_0\
    );
\disp[3][2]_INST_0_i_93\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \disp[3][2]_INST_0_i_93_n_0\,
      CO(2) => \disp[3][2]_INST_0_i_93_n_1\,
      CO(1) => \disp[3][2]_INST_0_i_93_n_2\,
      CO(0) => \disp[3][2]_INST_0_i_93_n_3\,
      CYINIT => '0',
      DI(3) => \inst_int_to_string/cent\(6),
      DI(2) => \disp[3][2]_INST_0_i_132_n_0\,
      DI(1) => \disp[3][2]_INST_0_i_133_n_0\,
      DI(0) => '0',
      O(3) => \disp[3][2]_INST_0_i_93_n_4\,
      O(2) => \disp[3][2]_INST_0_i_93_n_5\,
      O(1) => \disp[3][2]_INST_0_i_93_n_6\,
      O(0) => \disp[3][2]_INST_0_i_93_n_7\,
      S(3) => \disp[3][2]_INST_0_i_134_n_0\,
      S(2) => \disp[3][2]_INST_0_i_135_n_0\,
      S(1) => \disp[3][2]_INST_0_i_136_n_0\,
      S(0) => \disp[3][2]_INST_0_i_137_n_0\
    );
\disp[3][2]_INST_0_i_94\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/cent\(5),
      I1 => \disp[2][0]_INST_0_i_10_n_0\,
      O => \disp[3][2]_INST_0_i_94_n_0\
    );
\disp[3][2]_INST_0_i_95\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_11_n_0\,
      I1 => \disp[2][3]_INST_0_i_4_n_0\,
      O => \disp[3][2]_INST_0_i_95_n_0\
    );
\disp[3][2]_INST_0_i_96\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_12_n_0\,
      I1 => \disp[2][0]_INST_0_i_10_n_0\,
      O => \disp[3][2]_INST_0_i_96_n_0\
    );
\disp[3][2]_INST_0_i_97\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_10_n_0\,
      I1 => \disp[2][0]_INST_0_i_12_n_0\,
      O => \disp[3][2]_INST_0_i_97_n_0\
    );
\disp[3][2]_INST_0_i_98\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B44B"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_10_n_0\,
      I1 => \inst_int_to_string/cent\(5),
      I2 => \disp[2][3]_INST_0_i_4_n_0\,
      I3 => \inst_int_to_string/cent\(6),
      O => \disp[3][2]_INST_0_i_98_n_0\
    );
\disp[3][2]_INST_0_i_99\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"E11E"
    )
        port map (
      I0 => \disp[2][3]_INST_0_i_4_n_0\,
      I1 => \disp[2][0]_INST_0_i_11_n_0\,
      I2 => \disp[2][0]_INST_0_i_10_n_0\,
      I3 => \inst_int_to_string/cent\(5),
      O => \disp[3][2]_INST_0_i_99_n_0\
    );
\disp[3][3]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[3]_OBUF\(3),
      O => \disp[3]\(3)
    );
\disp[3][3]_INST_0_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"5155FFFF"
    )
        port map (
      I0 => \disp[3][3]_INST_0_i_2_n_0\,
      I1 => \disp[3][3]_INST_0_i_3_n_0\,
      I2 => \disp[3][3]_INST_0_i_4_n_0\,
      I3 => \disp[3][3]_INST_0_i_5_n_0\,
      I4 => \disp[3][3]_INST_0_i_6_n_0\,
      O => \disp[3]_OBUF\(3)
    );
\disp[3][3]_INST_0_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6666996999996696"
    )
        port map (
      I0 => \disp[3][3]_INST_0_i_16_n_0\,
      I1 => \disp[3][3]_INST_0_i_11_n_0\,
      I2 => \disp[3][2]_INST_0_i_7_n_0\,
      I3 => \disp[3][2]_INST_0_i_8_n_0\,
      I4 => \disp[3][3]_INST_0_i_2_n_0\,
      I5 => \disp[3][3]_INST_0_i_6_n_0\,
      O => \disp[3][3]_INST_0_i_10_n_0\
    );
\disp[3][3]_INST_0_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"D24BB4D22DB44B2D"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_13_n_0\,
      I1 => \disp[3][2]_INST_0_i_12_n_0\,
      I2 => \disp[3][2]_INST_0_i_10_n_4\,
      I3 => \disp[3][2]_INST_0_i_11_n_0\,
      I4 => \disp[3][2]_INST_0_i_10_n_5\,
      I5 => \disp[2]_OBUF\(3),
      O => \disp[3][3]_INST_0_i_11_n_0\
    );
\disp[3][3]_INST_0_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"02A2"
    )
        port map (
      I0 => \disp[2]_OBUF\(0),
      I1 => \inst_int_to_string/p_1_in\(0),
      I2 => \disp[1]_OBUF\(1),
      I3 => total_out(0),
      O => \disp[3][3]_INST_0_i_12_n_0\
    );
\disp[3][3]_INST_0_i_13\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0057"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_1_n_4\,
      I1 => \disp[2][0]_INST_0_i_1_n_5\,
      I2 => \disp[2][0]_INST_0_i_1_n_6\,
      I3 => \disp[2][3]_INST_0_i_2_n_7\,
      O => \disp[3][3]_INST_0_i_13_n_0\
    );
\disp[3][3]_INST_0_i_14\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0F0F0F0F5A58F0F0"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_9_n_6\,
      I1 => \disp[3][2]_INST_0_i_9_n_7\,
      I2 => \disp[3][2]_INST_0_i_10_n_5\,
      I3 => \disp[3][2]_INST_0_i_10_n_4\,
      I4 => \disp[3][2]_INST_0_i_9_n_5\,
      I5 => \disp[3][2]_INST_0_i_9_n_4\,
      O => \disp[3][3]_INST_0_i_14_n_0\
    );
\disp[3][3]_INST_0_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0110044002200880"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_13_n_0\,
      I1 => \disp[3][0]_INST_0_i_6_n_0\,
      I2 => \disp[2]_OBUF\(0),
      I3 => \disp[3][0]_INST_0_i_5_n_0\,
      I4 => \inst_int_to_string/string_cent_decenas[1]5\(1),
      I5 => \disp[3][3]_INST_0_i_14_n_0\,
      O => \disp[3][3]_INST_0_i_15_n_0\
    );
\disp[3][3]_INST_0_i_16\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0FFF000FE000EEE0"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_3_n_0\,
      I1 => \disp[3][2]_INST_0_i_4_n_0\,
      I2 => \disp[2]_OBUF\(3),
      I3 => \disp[3][3]_INST_0_i_8_n_0\,
      I4 => \disp[3][3]_INST_0_i_7_n_0\,
      I5 => \disp[3][2]_INST_0_i_5_n_0\,
      O => \disp[3][3]_INST_0_i_16_n_0\
    );
\disp[3][3]_INST_0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFEEFEFEFF"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_4_n_0\,
      I1 => \disp[3][2]_INST_0_i_5_n_0\,
      I2 => \disp[3][3]_INST_0_i_7_n_0\,
      I3 => \disp[3][3]_INST_0_i_8_n_0\,
      I4 => \disp[2]_OBUF\(3),
      I5 => \disp[3][2]_INST_0_i_3_n_0\,
      O => \disp[3][3]_INST_0_i_2_n_0\
    );
\disp[3][3]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"388EE33FE33CC33B"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_2_n_0\,
      I1 => \disp[3][2]_INST_0_i_3_n_0\,
      I2 => \disp[3][2]_INST_0_i_6_n_0\,
      I3 => \disp[3][2]_INST_0_i_5_n_0\,
      I4 => \disp[3][2]_INST_0_i_4_n_0\,
      I5 => \disp[3][2]_INST_0_i_2_n_0\,
      O => \disp[3][3]_INST_0_i_3_n_0\
    );
\disp[3][3]_INST_0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0AF2BC0C0080AF0"
    )
        port map (
      I0 => \disp[3][0]_INST_0_i_3_n_0\,
      I1 => \disp[3][3]_INST_0_i_9_n_0\,
      I2 => \disp[3][0]_INST_0_i_2_n_0\,
      I3 => \disp[3][2]_INST_0_i_2_n_0\,
      I4 => \disp[3][3]_INST_0_i_5_n_0\,
      I5 => \disp[3][3]_INST_0_i_10_n_0\,
      O => \disp[3][3]_INST_0_i_4_n_0\
    );
\disp[3][3]_INST_0_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"2BD5BFD5"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_3_n_0\,
      I1 => \disp[3][2]_INST_0_i_6_n_0\,
      I2 => \disp[3][2]_INST_0_i_5_n_0\,
      I3 => \disp[3][2]_INST_0_i_4_n_0\,
      I4 => \disp[3][3]_INST_0_i_11_n_0\,
      O => \disp[3][3]_INST_0_i_5_n_0\
    );
\disp[3][3]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"55151511FFFFFFFF"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_4_n_0\,
      I1 => \disp[3][2]_INST_0_i_5_n_0\,
      I2 => \disp[3][3]_INST_0_i_7_n_0\,
      I3 => \disp[3][3]_INST_0_i_8_n_0\,
      I4 => \disp[2]_OBUF\(3),
      I5 => \disp[3][2]_INST_0_i_3_n_0\,
      O => \disp[3][3]_INST_0_i_6_n_0\
    );
\disp[3][3]_INST_0_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2BBFFFF200022BB0"
    )
        port map (
      I0 => \inst_int_to_string/string_cent_decenas[1]5\(1),
      I1 => \disp[3][3]_INST_0_i_12_n_0\,
      I2 => \disp[3][3]_INST_0_i_13_n_0\,
      I3 => \disp[2][0]_INST_0_i_1_n_6\,
      I4 => \disp[2][0]_INST_0_i_1_n_5\,
      I5 => \disp[3][3]_INST_0_i_14_n_0\,
      O => \disp[3][3]_INST_0_i_7_n_0\
    );
\disp[3][3]_INST_0_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0FE10FE50FA50FA5"
    )
        port map (
      I0 => \disp[3][2]_INST_0_i_9_n_4\,
      I1 => \disp[3][2]_INST_0_i_9_n_5\,
      I2 => \disp[3][2]_INST_0_i_10_n_4\,
      I3 => \disp[3][2]_INST_0_i_10_n_5\,
      I4 => \disp[3][2]_INST_0_i_9_n_7\,
      I5 => \disp[3][2]_INST_0_i_9_n_6\,
      O => \disp[3][3]_INST_0_i_8_n_0\
    );
\disp[3][3]_INST_0_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3FFCCCC0033FFCC9"
    )
        port map (
      I0 => \disp[3][3]_INST_0_i_15_n_0\,
      I1 => \disp[3][3]_INST_0_i_11_n_0\,
      I2 => \disp[3][2]_INST_0_i_5_n_0\,
      I3 => \disp[3][2]_INST_0_i_6_n_0\,
      I4 => \disp[3][2]_INST_0_i_4_n_0\,
      I5 => \disp[3][2]_INST_0_i_3_n_0\,
      O => \disp[3][3]_INST_0_i_9_n_0\
    );
\disp[3][4]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[3]\(4)
    );
\disp[3][5]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[3]\(5)
    );
\disp[3][6]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[3]\(6)
    );
\disp[3][7]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[3]\(7)
    );
\disp[4][0]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[4]\(0)
    );
\disp[4][1]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[1]_OBUF\(2),
      O => \disp[4]\(1)
    );
\disp[4][2]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[1]_OBUF\(2),
      O => \disp[4]\(2)
    );
\disp[4][3]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[1]_OBUF\(2),
      O => \disp[4]\(3)
    );
\disp[4][4]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[4]\(4)
    );
\disp[4][5]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[4]\(5)
    );
\disp[4][6]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[4]\(6)
    );
\disp[4][7]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[4]\(7)
    );
\disp[5][0]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[5]_OBUF\(0),
      O => \disp[5]\(0)
    );
\disp[5][0]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5545444400101111"
    )
        port map (
      I0 => \disp[1]_OBUF\(1),
      I1 => \disp[5][0]_INST_0_i_2_n_3\,
      I2 => \inst_int_to_string/euros3\(30),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_5_n_6\,
      I5 => \disp[5][0]_INST_0_i_6_n_7\,
      O => \disp[5]_OBUF\(0)
    );
\disp[5][0]_INST_0_i_10\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_34_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_10_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_10_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_10_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_10_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \inst_int_to_string/euros3\(28 downto 25),
      S(3) => \disp[5][0]_INST_0_i_4_n_3\,
      S(2) => \disp[5][0]_INST_0_i_4_n_3\,
      S(1) => \disp[5][0]_INST_0_i_4_n_3\,
      S(0) => \disp[5][0]_INST_0_i_4_n_3\
    );
\disp[5][0]_INST_0_i_100\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_103_n_7\,
      I1 => \disp[5][0]_INST_0_i_103_n_5\,
      I2 => \disp[5][0]_INST_0_i_49_n_6\,
      I3 => \disp[5][0]_INST_0_i_96_n_0\,
      O => \disp[5][0]_INST_0_i_100_n_0\
    );
\disp[5][0]_INST_0_i_101\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_228_n_4\,
      I1 => \disp[5][0]_INST_0_i_103_n_6\,
      I2 => \disp[5][0]_INST_0_i_49_n_7\,
      I3 => \disp[5][0]_INST_0_i_97_n_0\,
      O => \disp[5][0]_INST_0_i_101_n_0\
    );
\disp[5][0]_INST_0_i_102\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_228_n_5\,
      I1 => \disp[5][0]_INST_0_i_103_n_7\,
      I2 => \disp[5][0]_INST_0_i_103_n_4\,
      I3 => \disp[5][0]_INST_0_i_98_n_0\,
      O => \disp[5][0]_INST_0_i_102_n_0\
    );
\disp[5][0]_INST_0_i_103\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_228_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_103_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_103_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_103_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_103_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_229_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_230_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_231_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_232_n_0\,
      O(3) => \disp[5][0]_INST_0_i_103_n_4\,
      O(2) => \disp[5][0]_INST_0_i_103_n_5\,
      O(1) => \disp[5][0]_INST_0_i_103_n_6\,
      O(0) => \disp[5][0]_INST_0_i_103_n_7\,
      S(3) => \disp[5][0]_INST_0_i_233_n_0\,
      S(2) => \disp[5][0]_INST_0_i_234_n_0\,
      S(1) => \disp[5][0]_INST_0_i_235_n_0\,
      S(0) => \disp[5][0]_INST_0_i_236_n_0\
    );
\disp[5][0]_INST_0_i_104\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_237_n_4\,
      I1 => \disp[5][0]_INST_0_i_238_n_7\,
      I2 => \disp[5][0]_INST_0_i_239_n_0\,
      I3 => \disp[5][0]_INST_0_i_240_n_1\,
      O => \disp[5][0]_INST_0_i_104_n_0\
    );
\disp[5][0]_INST_0_i_105\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_237_n_5\,
      I1 => \disp[5][0]_INST_0_i_237_n_4\,
      I2 => \disp[5][0]_INST_0_i_239_n_0\,
      I3 => \disp[5][0]_INST_0_i_240_n_1\,
      O => \disp[5][0]_INST_0_i_105_n_0\
    );
\disp[5][0]_INST_0_i_106\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_237_n_6\,
      I1 => \disp[5][0]_INST_0_i_237_n_5\,
      I2 => \disp[5][0]_INST_0_i_239_n_0\,
      I3 => \disp[5][0]_INST_0_i_240_n_1\,
      O => \disp[5][0]_INST_0_i_106_n_0\
    );
\disp[5][0]_INST_0_i_107\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"022C"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_237_n_7\,
      I1 => \disp[5][0]_INST_0_i_237_n_6\,
      I2 => \disp[5][0]_INST_0_i_239_n_0\,
      I3 => \disp[5][0]_INST_0_i_240_n_1\,
      O => \disp[5][0]_INST_0_i_107_n_0\
    );
\disp[5][0]_INST_0_i_108\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"566AA995"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_104_n_0\,
      I1 => \disp[5][0]_INST_0_i_238_n_7\,
      I2 => \disp[5][0]_INST_0_i_239_n_0\,
      I3 => \disp[5][0]_INST_0_i_240_n_1\,
      I4 => \disp[5][0]_INST_0_i_238_n_6\,
      O => \disp[5][0]_INST_0_i_108_n_0\
    );
\disp[5][0]_INST_0_i_109\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"366CC993"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_237_n_4\,
      I1 => \disp[5][0]_INST_0_i_238_n_7\,
      I2 => \disp[5][0]_INST_0_i_239_n_0\,
      I3 => \disp[5][0]_INST_0_i_240_n_1\,
      I4 => \disp[5][0]_INST_0_i_105_n_0\,
      O => \disp[5][0]_INST_0_i_109_n_0\
    );
\disp[5][0]_INST_0_i_11\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_35_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_11_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_11_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_11_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_11_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => total_out(7 downto 4),
      O(3) => \disp[5][0]_INST_0_i_11_n_4\,
      O(2) => \disp[5][0]_INST_0_i_11_n_5\,
      O(1) => \disp[5][0]_INST_0_i_11_n_6\,
      O(0) => \disp[5][0]_INST_0_i_11_n_7\,
      S(3) => \disp[5][0]_INST_0_i_36_n_0\,
      S(2) => \disp[5][0]_INST_0_i_37_n_0\,
      S(1) => \disp[5][0]_INST_0_i_38_n_0\,
      S(0) => \disp[5][0]_INST_0_i_39_n_0\
    );
\disp[5][0]_INST_0_i_110\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"366CC993"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_237_n_5\,
      I1 => \disp[5][0]_INST_0_i_237_n_4\,
      I2 => \disp[5][0]_INST_0_i_239_n_0\,
      I3 => \disp[5][0]_INST_0_i_240_n_1\,
      I4 => \disp[5][0]_INST_0_i_106_n_0\,
      O => \disp[5][0]_INST_0_i_110_n_0\
    );
\disp[5][0]_INST_0_i_111\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"366CC993"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_237_n_6\,
      I1 => \disp[5][0]_INST_0_i_237_n_5\,
      I2 => \disp[5][0]_INST_0_i_239_n_0\,
      I3 => \disp[5][0]_INST_0_i_240_n_1\,
      I4 => \disp[5][0]_INST_0_i_107_n_0\,
      O => \disp[5][0]_INST_0_i_111_n_0\
    );
\disp[5][0]_INST_0_i_112\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE80017"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_238_n_7\,
      I1 => \disp[5][0]_INST_0_i_240_n_1\,
      I2 => \disp[5][0]_INST_0_i_239_n_0\,
      I3 => \disp[5][0]_INST_0_i_238_n_6\,
      I4 => \disp[5][0]_INST_0_i_238_n_1\,
      O => \disp[5][0]_INST_0_i_112_n_0\
    );
\disp[5][0]_INST_0_i_113\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_241_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_113_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_113_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_113_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_113_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_242_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_243_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_244_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_245_n_0\,
      O(3 downto 0) => \NLW_disp[5][0]_INST_0_i_113_O_UNCONNECTED\(3 downto 0),
      S(3) => \disp[5][0]_INST_0_i_246_n_0\,
      S(2) => \disp[5][0]_INST_0_i_247_n_0\,
      S(1) => \disp[5][0]_INST_0_i_248_n_0\,
      S(0) => \disp[5][0]_INST_0_i_249_n_0\
    );
\disp[5][0]_INST_0_i_114\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_250_n_4\,
      I1 => \disp[5][0]_INST_0_i_127_n_5\,
      I2 => \disp[5][0]_INST_0_i_125_n_6\,
      I3 => \disp[5][0]_INST_0_i_128_n_5\,
      I4 => \disp[5][0]_INST_0_i_251_n_0\,
      O => \disp[5][0]_INST_0_i_114_n_0\
    );
\disp[5][0]_INST_0_i_115\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_250_n_5\,
      I1 => \disp[5][0]_INST_0_i_127_n_6\,
      I2 => \disp[5][0]_INST_0_i_125_n_7\,
      I3 => \disp[5][0]_INST_0_i_128_n_6\,
      I4 => \disp[5][0]_INST_0_i_252_n_0\,
      O => \disp[5][0]_INST_0_i_115_n_0\
    );
\disp[5][0]_INST_0_i_116\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_250_n_6\,
      I1 => \disp[5][0]_INST_0_i_127_n_7\,
      I2 => \disp[5][0]_INST_0_i_253_n_4\,
      I3 => \disp[5][0]_INST_0_i_128_n_7\,
      I4 => \disp[5][0]_INST_0_i_254_n_0\,
      O => \disp[5][0]_INST_0_i_116_n_0\
    );
\disp[5][0]_INST_0_i_117\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_255_n_4\,
      I1 => \disp[5][0]_INST_0_i_256_n_0\,
      I2 => \disp[5][0]_INST_0_i_250_n_7\,
      I3 => \disp[5][0]_INST_0_i_257_n_4\,
      I4 => \disp[5][0]_INST_0_i_253_n_5\,
      O => \disp[5][0]_INST_0_i_117_n_0\
    );
\disp[5][0]_INST_0_i_118\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_114_n_0\,
      I1 => \disp[5][0]_INST_0_i_258_n_0\,
      I2 => \disp[5][0]_INST_0_i_125_n_4\,
      I3 => \disp[5][0]_INST_0_i_68_n_7\,
      I4 => \disp[5][0]_INST_0_i_122_n_6\,
      I5 => \disp[5][0]_INST_0_i_128_n_4\,
      O => \disp[5][0]_INST_0_i_118_n_0\
    );
\disp[5][0]_INST_0_i_119\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_115_n_0\,
      I1 => \disp[5][0]_INST_0_i_259_n_0\,
      I2 => \disp[5][0]_INST_0_i_125_n_5\,
      I3 => \disp[5][0]_INST_0_i_127_n_4\,
      I4 => \disp[5][0]_INST_0_i_122_n_7\,
      I5 => \disp[5][0]_INST_0_i_128_n_5\,
      O => \disp[5][0]_INST_0_i_119_n_0\
    );
\disp[5][0]_INST_0_i_12\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_40_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_12_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_12_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_12_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_12_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_41_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_42_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_43_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_44_n_0\,
      O(3) => \disp[5][0]_INST_0_i_12_n_4\,
      O(2) => \disp[5][0]_INST_0_i_12_n_5\,
      O(1) => \disp[5][0]_INST_0_i_12_n_6\,
      O(0) => \disp[5][0]_INST_0_i_12_n_7\,
      S(3) => \disp[5][0]_INST_0_i_45_n_0\,
      S(2) => \disp[5][0]_INST_0_i_46_n_0\,
      S(1) => \disp[5][0]_INST_0_i_47_n_0\,
      S(0) => \disp[5][0]_INST_0_i_48_n_0\
    );
\disp[5][0]_INST_0_i_120\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_116_n_0\,
      I1 => \disp[5][0]_INST_0_i_260_n_0\,
      I2 => \disp[5][0]_INST_0_i_125_n_6\,
      I3 => \disp[5][0]_INST_0_i_127_n_5\,
      I4 => \disp[5][0]_INST_0_i_250_n_4\,
      I5 => \disp[5][0]_INST_0_i_128_n_6\,
      O => \disp[5][0]_INST_0_i_120_n_0\
    );
\disp[5][0]_INST_0_i_121\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_117_n_0\,
      I1 => \disp[5][0]_INST_0_i_261_n_0\,
      I2 => \disp[5][0]_INST_0_i_125_n_7\,
      I3 => \disp[5][0]_INST_0_i_127_n_6\,
      I4 => \disp[5][0]_INST_0_i_250_n_5\,
      I5 => \disp[5][0]_INST_0_i_128_n_7\,
      O => \disp[5][0]_INST_0_i_121_n_0\
    );
\disp[5][0]_INST_0_i_122\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_250_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_122_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_122_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_122_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_122_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_262_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_263_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_264_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_265_n_0\,
      O(3) => \disp[5][0]_INST_0_i_122_n_4\,
      O(2) => \disp[5][0]_INST_0_i_122_n_5\,
      O(1) => \disp[5][0]_INST_0_i_122_n_6\,
      O(0) => \disp[5][0]_INST_0_i_122_n_7\,
      S(3) => \disp[5][0]_INST_0_i_266_n_0\,
      S(2) => \disp[5][0]_INST_0_i_267_n_0\,
      S(1) => \disp[5][0]_INST_0_i_268_n_0\,
      S(0) => \disp[5][0]_INST_0_i_269_n_0\
    );
\disp[5][0]_INST_0_i_123\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_66_n_5\,
      I1 => \disp[5][0]_INST_0_i_68_n_4\,
      I2 => \disp[5][0]_INST_0_i_60_n_7\,
      O => \disp[5][0]_INST_0_i_123_n_0\
    );
\disp[5][0]_INST_0_i_124\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_66_n_6\,
      I1 => \disp[5][0]_INST_0_i_68_n_5\,
      I2 => \disp[5][0]_INST_0_i_122_n_4\,
      O => \disp[5][0]_INST_0_i_124_n_0\
    );
\disp[5][0]_INST_0_i_125\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_253_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_125_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_125_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_125_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_125_n_3\,
      CYINIT => '0',
      DI(3) => \inst_int_to_string/euros2\(8),
      DI(2) => \disp[5][0]_INST_0_i_271_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_272_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_273_n_0\,
      O(3) => \disp[5][0]_INST_0_i_125_n_4\,
      O(2) => \disp[5][0]_INST_0_i_125_n_5\,
      O(1) => \disp[5][0]_INST_0_i_125_n_6\,
      O(0) => \disp[5][0]_INST_0_i_125_n_7\,
      S(3) => \disp[5][0]_INST_0_i_274_n_0\,
      S(2) => \disp[5][0]_INST_0_i_275_n_0\,
      S(1) => \disp[5][0]_INST_0_i_276_n_0\,
      S(0) => \disp[5][0]_INST_0_i_277_n_0\
    );
\disp[5][0]_INST_0_i_126\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_66_n_7\,
      I1 => \disp[5][0]_INST_0_i_68_n_6\,
      I2 => \disp[5][0]_INST_0_i_122_n_5\,
      O => \disp[5][0]_INST_0_i_126_n_0\
    );
\disp[5][0]_INST_0_i_127\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_257_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_127_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_127_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_127_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_127_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_278_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_279_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_280_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_281_n_0\,
      O(3) => \disp[5][0]_INST_0_i_127_n_4\,
      O(2) => \disp[5][0]_INST_0_i_127_n_5\,
      O(1) => \disp[5][0]_INST_0_i_127_n_6\,
      O(0) => \disp[5][0]_INST_0_i_127_n_7\,
      S(3) => \disp[5][0]_INST_0_i_282_n_0\,
      S(2) => \disp[5][0]_INST_0_i_283_n_0\,
      S(1) => \disp[5][0]_INST_0_i_284_n_0\,
      S(0) => \disp[5][0]_INST_0_i_285_n_0\
    );
\disp[5][0]_INST_0_i_128\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_255_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_128_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_128_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_128_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_128_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_286_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_287_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_288_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_289_n_0\,
      O(3) => \disp[5][0]_INST_0_i_128_n_4\,
      O(2) => \disp[5][0]_INST_0_i_128_n_5\,
      O(1) => \disp[5][0]_INST_0_i_128_n_6\,
      O(0) => \disp[5][0]_INST_0_i_128_n_7\,
      S(3) => \disp[5][0]_INST_0_i_290_n_0\,
      S(2) => \disp[5][0]_INST_0_i_291_n_0\,
      S(1) => \disp[5][0]_INST_0_i_292_n_0\,
      S(0) => \disp[5][0]_INST_0_i_293_n_0\
    );
\disp[5][0]_INST_0_i_129\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_125_n_4\,
      I1 => \disp[5][0]_INST_0_i_68_n_7\,
      I2 => \disp[5][0]_INST_0_i_122_n_6\,
      O => \disp[5][0]_INST_0_i_129_n_0\
    );
\disp[5][0]_INST_0_i_13\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_49_n_6\,
      I1 => \disp[5][0]_INST_0_i_49_n_4\,
      O => \disp[5][0]_INST_0_i_13_n_0\
    );
\disp[5][0]_INST_0_i_130\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_60_n_7\,
      I1 => \disp[5][0]_INST_0_i_68_n_4\,
      I2 => \disp[5][0]_INST_0_i_66_n_5\,
      O => \disp[5][0]_INST_0_i_130_n_0\
    );
\disp[5][0]_INST_0_i_131\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_122_n_4\,
      I1 => \disp[5][0]_INST_0_i_68_n_5\,
      I2 => \disp[5][0]_INST_0_i_66_n_6\,
      O => \disp[5][0]_INST_0_i_131_n_0\
    );
\disp[5][0]_INST_0_i_132\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_122_n_6\,
      I1 => \disp[5][0]_INST_0_i_68_n_7\,
      I2 => \disp[5][0]_INST_0_i_125_n_4\,
      O => \disp[5][0]_INST_0_i_132_n_0\
    );
\disp[5][0]_INST_0_i_133\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(13),
      I1 => \inst_int_to_string/euros3\(9),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(7),
      O => \disp[5][0]_INST_0_i_133_n_0\
    );
\disp[5][0]_INST_0_i_134\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(12),
      I1 => \inst_int_to_string/euros3\(8),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(6),
      O => \disp[5][0]_INST_0_i_134_n_0\
    );
\disp[5][0]_INST_0_i_135\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCFA00FACCA000A0"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(7),
      I1 => \disp[5][0]_INST_0_i_11_n_4\,
      I2 => \inst_int_to_string/euros3\(5),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_11_n_6\,
      I5 => \inst_int_to_string/euros3\(11),
      O => \disp[5][0]_INST_0_i_135_n_0\
    );
\disp[5][0]_INST_0_i_136\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCFA00FACCA000A0"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(6),
      I1 => \disp[5][0]_INST_0_i_11_n_5\,
      I2 => \inst_int_to_string/euros3\(4),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_11_n_7\,
      I5 => \inst_int_to_string/euros3\(10),
      O => \disp[5][0]_INST_0_i_136_n_0\
    );
\disp[5][0]_INST_0_i_137\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(14),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(8),
      I3 => \inst_int_to_string/euros3\(10),
      I4 => \disp[5][0]_INST_0_i_133_n_0\,
      O => \disp[5][0]_INST_0_i_137_n_0\
    );
\disp[5][0]_INST_0_i_138\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5659A6A95956A9A6"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_134_n_0\,
      I1 => \inst_int_to_string/euros3\(13),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(7),
      I4 => \disp[5][0]_INST_0_i_11_n_4\,
      I5 => \inst_int_to_string/euros3\(9),
      O => \disp[5][0]_INST_0_i_138_n_0\
    );
\disp[5][0]_INST_0_i_139\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5A665A995A995A66"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_135_n_0\,
      I1 => \inst_int_to_string/euros3\(6),
      I2 => \disp[5][0]_INST_0_i_11_n_5\,
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \inst_int_to_string/euros3\(8),
      I5 => \inst_int_to_string/euros3\(12),
      O => \disp[5][0]_INST_0_i_139_n_0\
    );
\disp[5][0]_INST_0_i_14\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"65"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_49_n_4\,
      I1 => \disp[5][0]_INST_0_i_50_n_7\,
      I2 => \disp[5][0]_INST_0_i_49_n_5\,
      O => \disp[5][0]_INST_0_i_14_n_0\
    );
\disp[5][0]_INST_0_i_140\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9969666999966696"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_136_n_0\,
      I1 => \inst_int_to_string/euros2\(5),
      I2 => \inst_int_to_string/euros3\(7),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_11_n_4\,
      I5 => \inst_int_to_string/euros3\(11),
      O => \disp[5][0]_INST_0_i_140_n_0\
    );
\disp[5][0]_INST_0_i_141\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(22),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(18),
      I3 => \inst_int_to_string/euros3\(20),
      O => \disp[5][0]_INST_0_i_141_n_0\
    );
\disp[5][0]_INST_0_i_142\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(21),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(17),
      I3 => \inst_int_to_string/euros3\(19),
      O => \disp[5][0]_INST_0_i_142_n_0\
    );
\disp[5][0]_INST_0_i_143\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(20),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(16),
      I3 => \inst_int_to_string/euros3\(18),
      O => \disp[5][0]_INST_0_i_143_n_0\
    );
\disp[5][0]_INST_0_i_144\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(19),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(15),
      I3 => \inst_int_to_string/euros3\(17),
      O => \disp[5][0]_INST_0_i_144_n_0\
    );
\disp[5][0]_INST_0_i_145\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(23),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(19),
      I3 => \inst_int_to_string/euros3\(21),
      I4 => \disp[5][0]_INST_0_i_141_n_0\,
      O => \disp[5][0]_INST_0_i_145_n_0\
    );
\disp[5][0]_INST_0_i_146\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(22),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(18),
      I3 => \inst_int_to_string/euros3\(20),
      I4 => \disp[5][0]_INST_0_i_142_n_0\,
      O => \disp[5][0]_INST_0_i_146_n_0\
    );
\disp[5][0]_INST_0_i_147\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(21),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(17),
      I3 => \inst_int_to_string/euros3\(19),
      I4 => \disp[5][0]_INST_0_i_143_n_0\,
      O => \disp[5][0]_INST_0_i_147_n_0\
    );
\disp[5][0]_INST_0_i_148\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(20),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(16),
      I3 => \inst_int_to_string/euros3\(18),
      I4 => \disp[5][0]_INST_0_i_144_n_0\,
      O => \disp[5][0]_INST_0_i_148_n_0\
    );
\disp[5][0]_INST_0_i_149\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(16),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      O => \inst_int_to_string/euros2\(16)
    );
\disp[5][0]_INST_0_i_15\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B44B"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_49_n_4\,
      I1 => \disp[5][0]_INST_0_i_49_n_6\,
      I2 => \disp[5][0]_INST_0_i_50_n_7\,
      I3 => \disp[5][0]_INST_0_i_49_n_5\,
      O => \disp[5][0]_INST_0_i_15_n_0\
    );
\disp[5][0]_INST_0_i_150\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(15),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      O => \inst_int_to_string/euros2\(15)
    );
\disp[5][0]_INST_0_i_151\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(14),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      O => \inst_int_to_string/euros2\(14)
    );
\disp[5][0]_INST_0_i_152\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(13),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      O => \inst_int_to_string/euros2\(13)
    );
\disp[5][0]_INST_0_i_153\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(16),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(19),
      O => \disp[5][0]_INST_0_i_153_n_0\
    );
\disp[5][0]_INST_0_i_154\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(15),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(18),
      O => \disp[5][0]_INST_0_i_154_n_0\
    );
\disp[5][0]_INST_0_i_155\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(14),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(17),
      O => \disp[5][0]_INST_0_i_155_n_0\
    );
\disp[5][0]_INST_0_i_156\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(13),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(16),
      O => \disp[5][0]_INST_0_i_156_n_0\
    );
\disp[5][0]_INST_0_i_157\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(29),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(24),
      O => \disp[5][0]_INST_0_i_157_n_0\
    );
\disp[5][0]_INST_0_i_158\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(28),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(30),
      I3 => \inst_int_to_string/euros3\(23),
      O => \disp[5][0]_INST_0_i_158_n_0\
    );
\disp[5][0]_INST_0_i_159\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(27),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(29),
      I3 => \inst_int_to_string/euros3\(22),
      O => \disp[5][0]_INST_0_i_159_n_0\
    );
\disp[5][0]_INST_0_i_16\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_51_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_16_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_16_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_16_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_16_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_52_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_53_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_54_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_55_n_0\,
      O(3 downto 0) => \NLW_disp[5][0]_INST_0_i_16_O_UNCONNECTED\(3 downto 0),
      S(3) => \disp[5][0]_INST_0_i_56_n_0\,
      S(2) => \disp[5][0]_INST_0_i_57_n_0\,
      S(1) => \disp[5][0]_INST_0_i_58_n_0\,
      S(0) => \disp[5][0]_INST_0_i_59_n_0\
    );
\disp[5][0]_INST_0_i_160\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(26),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(28),
      I3 => \inst_int_to_string/euros3\(21),
      O => \disp[5][0]_INST_0_i_160_n_0\
    );
\disp[5][0]_INST_0_i_161\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00870078"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(24),
      I1 => \inst_int_to_string/euros3\(29),
      I2 => \inst_int_to_string/euros3\(30),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \inst_int_to_string/euros3\(25),
      O => \disp[5][0]_INST_0_i_161_n_0\
    );
\disp[5][0]_INST_0_i_162\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000E817000017E8"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(23),
      I1 => \inst_int_to_string/euros3\(30),
      I2 => \inst_int_to_string/euros3\(28),
      I3 => \inst_int_to_string/euros3\(29),
      I4 => \disp[5][0]_INST_0_i_4_n_3\,
      I5 => \inst_int_to_string/euros3\(24),
      O => \disp[5][0]_INST_0_i_162_n_0\
    );
\disp[5][0]_INST_0_i_163\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A6A9A9A6"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_159_n_0\,
      I1 => \inst_int_to_string/euros3\(28),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(30),
      I4 => \inst_int_to_string/euros3\(23),
      O => \disp[5][0]_INST_0_i_163_n_0\
    );
\disp[5][0]_INST_0_i_164\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(27),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(29),
      I3 => \inst_int_to_string/euros3\(22),
      I4 => \disp[5][0]_INST_0_i_160_n_0\,
      O => \disp[5][0]_INST_0_i_164_n_0\
    );
\disp[5][0]_INST_0_i_165\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(12),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      O => \inst_int_to_string/euros2\(12)
    );
\disp[5][0]_INST_0_i_166\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(11),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      O => \inst_int_to_string/euros2\(11)
    );
\disp[5][0]_INST_0_i_167\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(10),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      O => \inst_int_to_string/euros2\(10)
    );
\disp[5][0]_INST_0_i_168\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(9),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      O => \inst_int_to_string/euros2\(9)
    );
\disp[5][0]_INST_0_i_169\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(12),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(15),
      O => \disp[5][0]_INST_0_i_169_n_0\
    );
\disp[5][0]_INST_0_i_17\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_60_n_4\,
      I1 => \disp[5][0]_INST_0_i_61_n_5\,
      I2 => \disp[5][0]_INST_0_i_62_n_6\,
      I3 => \disp[5][0]_INST_0_i_63_n_5\,
      I4 => \disp[5][0]_INST_0_i_64_n_0\,
      O => \disp[5][0]_INST_0_i_17_n_0\
    );
\disp[5][0]_INST_0_i_170\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(11),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(14),
      O => \disp[5][0]_INST_0_i_170_n_0\
    );
\disp[5][0]_INST_0_i_171\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(10),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(13),
      O => \disp[5][0]_INST_0_i_171_n_0\
    );
\disp[5][0]_INST_0_i_172\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(9),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(12),
      O => \disp[5][0]_INST_0_i_172_n_0\
    );
\disp[5][0]_INST_0_i_173\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(18),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(14),
      I3 => \inst_int_to_string/euros3\(16),
      O => \disp[5][0]_INST_0_i_173_n_0\
    );
\disp[5][0]_INST_0_i_174\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(17),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(13),
      I3 => \inst_int_to_string/euros3\(15),
      O => \disp[5][0]_INST_0_i_174_n_0\
    );
\disp[5][0]_INST_0_i_175\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(16),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(12),
      I3 => \inst_int_to_string/euros3\(14),
      O => \disp[5][0]_INST_0_i_175_n_0\
    );
\disp[5][0]_INST_0_i_176\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(15),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(11),
      I3 => \inst_int_to_string/euros3\(13),
      O => \disp[5][0]_INST_0_i_176_n_0\
    );
\disp[5][0]_INST_0_i_177\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(19),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(15),
      I3 => \inst_int_to_string/euros3\(17),
      I4 => \disp[5][0]_INST_0_i_173_n_0\,
      O => \disp[5][0]_INST_0_i_177_n_0\
    );
\disp[5][0]_INST_0_i_178\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(18),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(14),
      I3 => \inst_int_to_string/euros3\(16),
      I4 => \disp[5][0]_INST_0_i_174_n_0\,
      O => \disp[5][0]_INST_0_i_178_n_0\
    );
\disp[5][0]_INST_0_i_179\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(17),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(13),
      I3 => \inst_int_to_string/euros3\(15),
      I4 => \disp[5][0]_INST_0_i_175_n_0\,
      O => \disp[5][0]_INST_0_i_179_n_0\
    );
\disp[5][0]_INST_0_i_18\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_60_n_5\,
      I1 => \disp[5][0]_INST_0_i_61_n_6\,
      I2 => \disp[5][0]_INST_0_i_62_n_7\,
      I3 => \disp[5][0]_INST_0_i_63_n_6\,
      I4 => \disp[5][0]_INST_0_i_65_n_0\,
      O => \disp[5][0]_INST_0_i_18_n_0\
    );
\disp[5][0]_INST_0_i_180\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(16),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(12),
      I3 => \inst_int_to_string/euros3\(14),
      I4 => \disp[5][0]_INST_0_i_176_n_0\,
      O => \disp[5][0]_INST_0_i_180_n_0\
    );
\disp[5][0]_INST_0_i_181\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(25),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(27),
      I3 => \inst_int_to_string/euros3\(20),
      O => \disp[5][0]_INST_0_i_181_n_0\
    );
\disp[5][0]_INST_0_i_182\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(24),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(26),
      I3 => \inst_int_to_string/euros3\(19),
      O => \disp[5][0]_INST_0_i_182_n_0\
    );
\disp[5][0]_INST_0_i_183\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(23),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(25),
      I3 => \inst_int_to_string/euros3\(18),
      O => \disp[5][0]_INST_0_i_183_n_0\
    );
\disp[5][0]_INST_0_i_184\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(22),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(24),
      I3 => \inst_int_to_string/euros3\(17),
      O => \disp[5][0]_INST_0_i_184_n_0\
    );
\disp[5][0]_INST_0_i_185\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(26),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(28),
      I3 => \inst_int_to_string/euros3\(21),
      I4 => \disp[5][0]_INST_0_i_181_n_0\,
      O => \disp[5][0]_INST_0_i_185_n_0\
    );
\disp[5][0]_INST_0_i_186\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(25),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(27),
      I3 => \inst_int_to_string/euros3\(20),
      I4 => \disp[5][0]_INST_0_i_182_n_0\,
      O => \disp[5][0]_INST_0_i_186_n_0\
    );
\disp[5][0]_INST_0_i_187\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(24),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(26),
      I3 => \inst_int_to_string/euros3\(19),
      I4 => \disp[5][0]_INST_0_i_183_n_0\,
      O => \disp[5][0]_INST_0_i_187_n_0\
    );
\disp[5][0]_INST_0_i_188\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(23),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(25),
      I3 => \inst_int_to_string/euros3\(18),
      I4 => \disp[5][0]_INST_0_i_184_n_0\,
      O => \disp[5][0]_INST_0_i_188_n_0\
    );
\disp[5][0]_INST_0_i_189\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(26),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(22),
      I3 => \inst_int_to_string/euros3\(24),
      O => \disp[5][0]_INST_0_i_189_n_0\
    );
\disp[5][0]_INST_0_i_19\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_60_n_6\,
      I1 => \disp[5][0]_INST_0_i_61_n_7\,
      I2 => \disp[5][0]_INST_0_i_66_n_4\,
      I3 => \disp[5][0]_INST_0_i_63_n_7\,
      I4 => \disp[5][0]_INST_0_i_67_n_0\,
      O => \disp[5][0]_INST_0_i_19_n_0\
    );
\disp[5][0]_INST_0_i_190\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(25),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(21),
      I3 => \inst_int_to_string/euros3\(23),
      O => \disp[5][0]_INST_0_i_190_n_0\
    );
\disp[5][0]_INST_0_i_191\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(24),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(20),
      I3 => \inst_int_to_string/euros3\(22),
      O => \disp[5][0]_INST_0_i_191_n_0\
    );
\disp[5][0]_INST_0_i_192\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(23),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(19),
      I3 => \inst_int_to_string/euros3\(21),
      O => \disp[5][0]_INST_0_i_192_n_0\
    );
\disp[5][0]_INST_0_i_193\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(27),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(23),
      I3 => \inst_int_to_string/euros3\(25),
      I4 => \disp[5][0]_INST_0_i_189_n_0\,
      O => \disp[5][0]_INST_0_i_193_n_0\
    );
\disp[5][0]_INST_0_i_194\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(26),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(22),
      I3 => \inst_int_to_string/euros3\(24),
      I4 => \disp[5][0]_INST_0_i_190_n_0\,
      O => \disp[5][0]_INST_0_i_194_n_0\
    );
\disp[5][0]_INST_0_i_195\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(25),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(21),
      I3 => \inst_int_to_string/euros3\(23),
      I4 => \disp[5][0]_INST_0_i_191_n_0\,
      O => \disp[5][0]_INST_0_i_195_n_0\
    );
\disp[5][0]_INST_0_i_196\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(24),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(20),
      I3 => \inst_int_to_string/euros3\(22),
      I4 => \disp[5][0]_INST_0_i_192_n_0\,
      O => \disp[5][0]_INST_0_i_196_n_0\
    );
\disp[5][0]_INST_0_i_197\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(17),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(13),
      I3 => \inst_int_to_string/euros3\(11),
      O => \disp[5][0]_INST_0_i_197_n_0\
    );
\disp[5][0]_INST_0_i_198\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(16),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(12),
      I3 => \inst_int_to_string/euros3\(10),
      O => \disp[5][0]_INST_0_i_198_n_0\
    );
\disp[5][0]_INST_0_i_199\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(15),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(11),
      I3 => \inst_int_to_string/euros3\(9),
      O => \disp[5][0]_INST_0_i_199_n_0\
    );
\disp[5][0]_INST_0_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_7_n_0\,
      CO(3 downto 1) => \NLW_disp[5][0]_INST_0_i_2_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \disp[5][0]_INST_0_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \disp[5][0]_INST_0_i_8_n_0\,
      O(3 downto 0) => \NLW_disp[5][0]_INST_0_i_2_O_UNCONNECTED\(3 downto 0),
      S(3 downto 1) => B"000",
      S(0) => \disp[5][0]_INST_0_i_9_n_0\
    );
\disp[5][0]_INST_0_i_20\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_60_n_7\,
      I1 => \disp[5][0]_INST_0_i_68_n_4\,
      I2 => \disp[5][0]_INST_0_i_66_n_5\,
      I3 => \disp[5][0]_INST_0_i_69_n_4\,
      I4 => \disp[5][0]_INST_0_i_70_n_0\,
      O => \disp[5][0]_INST_0_i_20_n_0\
    );
\disp[5][0]_INST_0_i_200\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(14),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(8),
      I3 => \inst_int_to_string/euros3\(10),
      O => \disp[5][0]_INST_0_i_200_n_0\
    );
\disp[5][0]_INST_0_i_201\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(18),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(14),
      I3 => \inst_int_to_string/euros3\(12),
      I4 => \disp[5][0]_INST_0_i_197_n_0\,
      O => \disp[5][0]_INST_0_i_201_n_0\
    );
\disp[5][0]_INST_0_i_202\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(17),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(13),
      I3 => \inst_int_to_string/euros3\(11),
      I4 => \disp[5][0]_INST_0_i_198_n_0\,
      O => \disp[5][0]_INST_0_i_202_n_0\
    );
\disp[5][0]_INST_0_i_203\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(16),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(12),
      I3 => \inst_int_to_string/euros3\(10),
      I4 => \disp[5][0]_INST_0_i_199_n_0\,
      O => \disp[5][0]_INST_0_i_203_n_0\
    );
\disp[5][0]_INST_0_i_204\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(15),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(11),
      I3 => \inst_int_to_string/euros3\(9),
      I4 => \disp[5][0]_INST_0_i_200_n_0\,
      O => \disp[5][0]_INST_0_i_204_n_0\
    );
\disp[5][0]_INST_0_i_205\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_298_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_205_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_205_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_205_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_205_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_299_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_300_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_301_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_302_n_0\,
      O(3 downto 0) => \NLW_disp[5][0]_INST_0_i_205_O_UNCONNECTED\(3 downto 0),
      S(3) => \disp[5][0]_INST_0_i_303_n_0\,
      S(2) => \disp[5][0]_INST_0_i_304_n_0\,
      S(1) => \disp[5][0]_INST_0_i_305_n_0\,
      S(0) => \disp[5][0]_INST_0_i_306_n_0\
    );
\disp[5][0]_INST_0_i_206\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_94_n_4\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(20),
      O => \disp[5][0]_INST_0_i_206_n_0\
    );
\disp[5][0]_INST_0_i_207\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_94_n_5\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(19),
      O => \disp[5][0]_INST_0_i_207_n_0\
    );
\disp[5][0]_INST_0_i_208\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_94_n_6\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(18),
      O => \disp[5][0]_INST_0_i_208_n_0\
    );
\disp[5][0]_INST_0_i_209\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_94_n_7\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(17),
      O => \disp[5][0]_INST_0_i_209_n_0\
    );
\disp[5][0]_INST_0_i_21\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_17_n_0\,
      I1 => \disp[5][0]_INST_0_i_71_n_0\,
      I2 => \disp[5][0]_INST_0_i_62_n_4\,
      I3 => \disp[5][0]_INST_0_i_72_n_7\,
      I4 => \disp[5][0]_INST_0_i_73_n_6\,
      I5 => \disp[5][0]_INST_0_i_63_n_4\,
      O => \disp[5][0]_INST_0_i_21_n_0\
    );
\disp[5][0]_INST_0_i_210\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(20),
      I1 => \disp[5][0]_INST_0_i_94_n_4\,
      I2 => \inst_int_to_string/euros3\(21),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_40_n_7\,
      O => \disp[5][0]_INST_0_i_210_n_0\
    );
\disp[5][0]_INST_0_i_211\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(19),
      I1 => \disp[5][0]_INST_0_i_94_n_5\,
      I2 => \inst_int_to_string/euros3\(20),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_94_n_4\,
      O => \disp[5][0]_INST_0_i_211_n_0\
    );
\disp[5][0]_INST_0_i_212\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(18),
      I1 => \disp[5][0]_INST_0_i_94_n_6\,
      I2 => \inst_int_to_string/euros3\(19),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_94_n_5\,
      O => \disp[5][0]_INST_0_i_212_n_0\
    );
\disp[5][0]_INST_0_i_213\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(17),
      I1 => \disp[5][0]_INST_0_i_94_n_7\,
      I2 => \inst_int_to_string/euros3\(18),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_94_n_6\,
      O => \disp[5][0]_INST_0_i_213_n_0\
    );
\disp[5][0]_INST_0_i_214\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_294_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_214_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_214_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_214_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_214_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \inst_int_to_string/euros3\(16 downto 13),
      S(3) => \disp[5][0]_INST_0_i_4_n_3\,
      S(2) => \disp[5][0]_INST_0_i_4_n_3\,
      S(1) => \disp[5][0]_INST_0_i_4_n_3\,
      S(0) => \disp[5][0]_INST_0_i_4_n_3\
    );
\disp[5][0]_INST_0_i_215\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_out(7),
      I1 => \disp[5][0]_INST_0_i_307_n_5\,
      O => \disp[5][0]_INST_0_i_215_n_0\
    );
\disp[5][0]_INST_0_i_216\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_out(6),
      I1 => \disp[5][0]_INST_0_i_307_n_6\,
      O => \disp[5][0]_INST_0_i_216_n_0\
    );
\disp[5][0]_INST_0_i_217\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_out(5),
      I1 => \disp[5][0]_INST_0_i_307_n_7\,
      O => \disp[5][0]_INST_0_i_217_n_0\
    );
\disp[5][0]_INST_0_i_218\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_out(4),
      I1 => \disp[2][0]_INST_0_i_57_n_5\,
      O => \disp[5][0]_INST_0_i_218_n_0\
    );
\disp[5][0]_INST_0_i_219\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_308_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_219_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_219_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_219_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_219_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_309_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_310_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_311_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_312_n_0\,
      O(3) => \disp[5][0]_INST_0_i_219_n_4\,
      O(2) => \disp[5][0]_INST_0_i_219_n_5\,
      O(1) => \disp[5][0]_INST_0_i_219_n_6\,
      O(0) => \disp[5][0]_INST_0_i_219_n_7\,
      S(3) => \disp[5][0]_INST_0_i_313_n_0\,
      S(2) => \disp[5][0]_INST_0_i_314_n_0\,
      S(1) => \disp[5][0]_INST_0_i_315_n_0\,
      S(0) => \disp[5][0]_INST_0_i_316_n_0\
    );
\disp[5][0]_INST_0_i_22\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_18_n_0\,
      I1 => \disp[5][0]_INST_0_i_74_n_0\,
      I2 => \disp[5][0]_INST_0_i_62_n_5\,
      I3 => \disp[5][0]_INST_0_i_61_n_4\,
      I4 => \disp[5][0]_INST_0_i_73_n_7\,
      I5 => \disp[5][0]_INST_0_i_63_n_5\,
      O => \disp[5][0]_INST_0_i_22_n_0\
    );
\disp[5][0]_INST_0_i_220\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_228_n_7\,
      I1 => \disp[5][0]_INST_0_i_228_n_5\,
      I2 => \disp[5][0]_INST_0_i_103_n_6\,
      O => \disp[5][0]_INST_0_i_220_n_0\
    );
\disp[5][0]_INST_0_i_221\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_317_n_4\,
      I1 => \disp[5][0]_INST_0_i_228_n_6\,
      I2 => \disp[5][0]_INST_0_i_103_n_7\,
      O => \disp[5][0]_INST_0_i_221_n_0\
    );
\disp[5][0]_INST_0_i_222\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_317_n_5\,
      I1 => \disp[5][0]_INST_0_i_228_n_7\,
      I2 => \disp[5][0]_INST_0_i_228_n_4\,
      O => \disp[5][0]_INST_0_i_222_n_0\
    );
\disp[5][0]_INST_0_i_223\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_317_n_6\,
      I1 => \disp[5][0]_INST_0_i_317_n_4\,
      I2 => \disp[5][0]_INST_0_i_228_n_5\,
      O => \disp[5][0]_INST_0_i_223_n_0\
    );
\disp[5][0]_INST_0_i_224\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_228_n_6\,
      I1 => \disp[5][0]_INST_0_i_228_n_4\,
      I2 => \disp[5][0]_INST_0_i_103_n_5\,
      I3 => \disp[5][0]_INST_0_i_220_n_0\,
      O => \disp[5][0]_INST_0_i_224_n_0\
    );
\disp[5][0]_INST_0_i_225\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_228_n_7\,
      I1 => \disp[5][0]_INST_0_i_228_n_5\,
      I2 => \disp[5][0]_INST_0_i_103_n_6\,
      I3 => \disp[5][0]_INST_0_i_221_n_0\,
      O => \disp[5][0]_INST_0_i_225_n_0\
    );
\disp[5][0]_INST_0_i_226\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_317_n_4\,
      I1 => \disp[5][0]_INST_0_i_228_n_6\,
      I2 => \disp[5][0]_INST_0_i_103_n_7\,
      I3 => \disp[5][0]_INST_0_i_222_n_0\,
      O => \disp[5][0]_INST_0_i_226_n_0\
    );
\disp[5][0]_INST_0_i_227\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_317_n_5\,
      I1 => \disp[5][0]_INST_0_i_228_n_7\,
      I2 => \disp[5][0]_INST_0_i_228_n_4\,
      I3 => \disp[5][0]_INST_0_i_223_n_0\,
      O => \disp[5][0]_INST_0_i_227_n_0\
    );
\disp[5][0]_INST_0_i_228\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_317_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_228_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_228_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_228_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_228_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_318_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_319_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_320_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_321_n_0\,
      O(3) => \disp[5][0]_INST_0_i_228_n_4\,
      O(2) => \disp[5][0]_INST_0_i_228_n_5\,
      O(1) => \disp[5][0]_INST_0_i_228_n_6\,
      O(0) => \disp[5][0]_INST_0_i_228_n_7\,
      S(3) => \disp[5][0]_INST_0_i_322_n_0\,
      S(2) => \disp[5][0]_INST_0_i_323_n_0\,
      S(1) => \disp[5][0]_INST_0_i_324_n_0\,
      S(0) => \disp[5][0]_INST_0_i_325_n_0\
    );
\disp[5][0]_INST_0_i_229\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"800E08E0"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_326_n_4\,
      I1 => \disp[5][0]_INST_0_i_240_n_6\,
      I2 => \disp[5][0]_INST_0_i_237_n_7\,
      I3 => \disp[5][0]_INST_0_i_239_n_0\,
      I4 => \disp[5][0]_INST_0_i_240_n_1\,
      O => \disp[5][0]_INST_0_i_229_n_0\
    );
\disp[5][0]_INST_0_i_23\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_19_n_0\,
      I1 => \disp[5][0]_INST_0_i_75_n_0\,
      I2 => \disp[5][0]_INST_0_i_62_n_6\,
      I3 => \disp[5][0]_INST_0_i_61_n_5\,
      I4 => \disp[5][0]_INST_0_i_60_n_4\,
      I5 => \disp[5][0]_INST_0_i_63_n_6\,
      O => \disp[5][0]_INST_0_i_23_n_0\
    );
\disp[5][0]_INST_0_i_230\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60990090"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_240_n_6\,
      I1 => \disp[5][0]_INST_0_i_326_n_4\,
      I2 => \disp[5][0]_INST_0_i_326_n_5\,
      I3 => \disp[5][0]_INST_0_i_239_n_0\,
      I4 => \disp[5][0]_INST_0_i_240_n_7\,
      O => \disp[5][0]_INST_0_i_230_n_0\
    );
\disp[5][0]_INST_0_i_231\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60990090"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_240_n_7\,
      I1 => \disp[5][0]_INST_0_i_326_n_5\,
      I2 => \disp[5][0]_INST_0_i_326_n_6\,
      I3 => \disp[5][0]_INST_0_i_239_n_0\,
      I4 => \disp[5][0]_INST_0_i_327_n_4\,
      O => \disp[5][0]_INST_0_i_231_n_0\
    );
\disp[5][0]_INST_0_i_232\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"60990090"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_327_n_4\,
      I1 => \disp[5][0]_INST_0_i_326_n_6\,
      I2 => \disp[5][0]_INST_0_i_326_n_7\,
      I3 => \disp[5][0]_INST_0_i_239_n_0\,
      I4 => \disp[5][0]_INST_0_i_327_n_5\,
      O => \disp[5][0]_INST_0_i_232_n_0\
    );
\disp[5][0]_INST_0_i_233\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"366CC993"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_237_n_7\,
      I1 => \disp[5][0]_INST_0_i_237_n_6\,
      I2 => \disp[5][0]_INST_0_i_239_n_0\,
      I3 => \disp[5][0]_INST_0_i_240_n_1\,
      I4 => \disp[5][0]_INST_0_i_229_n_0\,
      O => \disp[5][0]_INST_0_i_233_n_0\
    );
\disp[5][0]_INST_0_i_234\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669669966996996"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_230_n_0\,
      I1 => \disp[5][0]_INST_0_i_240_n_1\,
      I2 => \disp[5][0]_INST_0_i_239_n_0\,
      I3 => \disp[5][0]_INST_0_i_237_n_7\,
      I4 => \disp[5][0]_INST_0_i_240_n_6\,
      I5 => \disp[5][0]_INST_0_i_326_n_4\,
      O => \disp[5][0]_INST_0_i_234_n_0\
    );
\disp[5][0]_INST_0_i_235\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AA9955695566AA9"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_231_n_0\,
      I1 => \disp[5][0]_INST_0_i_240_n_7\,
      I2 => \disp[5][0]_INST_0_i_239_n_0\,
      I3 => \disp[5][0]_INST_0_i_326_n_5\,
      I4 => \disp[5][0]_INST_0_i_326_n_4\,
      I5 => \disp[5][0]_INST_0_i_240_n_6\,
      O => \disp[5][0]_INST_0_i_235_n_0\
    );
\disp[5][0]_INST_0_i_236\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AA9955695566AA9"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_232_n_0\,
      I1 => \disp[5][0]_INST_0_i_327_n_4\,
      I2 => \disp[5][0]_INST_0_i_239_n_0\,
      I3 => \disp[5][0]_INST_0_i_326_n_6\,
      I4 => \disp[5][0]_INST_0_i_326_n_5\,
      I5 => \disp[5][0]_INST_0_i_240_n_7\,
      O => \disp[5][0]_INST_0_i_236_n_0\
    );
\disp[5][0]_INST_0_i_237\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_326_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_237_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_237_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_237_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_237_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_328_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_329_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_330_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_331_n_0\,
      O(3) => \disp[5][0]_INST_0_i_237_n_4\,
      O(2) => \disp[5][0]_INST_0_i_237_n_5\,
      O(1) => \disp[5][0]_INST_0_i_237_n_6\,
      O(0) => \disp[5][0]_INST_0_i_237_n_7\,
      S(3) => \disp[5][0]_INST_0_i_332_n_0\,
      S(2) => \disp[5][0]_INST_0_i_333_n_0\,
      S(1) => \disp[5][0]_INST_0_i_334_n_0\,
      S(0) => \disp[5][0]_INST_0_i_335_n_0\
    );
\disp[5][0]_INST_0_i_238\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_237_n_0\,
      CO(3) => \NLW_disp[5][0]_INST_0_i_238_CO_UNCONNECTED\(3),
      CO(2) => \disp[5][0]_INST_0_i_238_n_1\,
      CO(1) => \NLW_disp[5][0]_INST_0_i_238_CO_UNCONNECTED\(1),
      CO(0) => \disp[5][0]_INST_0_i_238_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \disp[5][0]_INST_0_i_336_n_0\,
      O(3 downto 2) => \NLW_disp[5][0]_INST_0_i_238_O_UNCONNECTED\(3 downto 2),
      O(1) => \disp[5][0]_INST_0_i_238_n_6\,
      O(0) => \disp[5][0]_INST_0_i_238_n_7\,
      S(3 downto 2) => B"01",
      S(1) => \inst_int_to_string/euros2\(30),
      S(0) => \disp[5][0]_INST_0_i_338_n_0\
    );
\disp[5][0]_INST_0_i_239\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_339_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_239_n_0\,
      CO(2) => \NLW_disp[5][0]_INST_0_i_239_CO_UNCONNECTED\(2),
      CO(1) => \disp[5][0]_INST_0_i_239_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_239_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \disp[5][0]_INST_0_i_340_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_341_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_342_n_0\,
      O(3) => \NLW_disp[5][0]_INST_0_i_239_O_UNCONNECTED\(3),
      O(2) => \disp[5][0]_INST_0_i_239_n_5\,
      O(1) => \disp[5][0]_INST_0_i_239_n_6\,
      O(0) => \disp[5][0]_INST_0_i_239_n_7\,
      S(3) => '1',
      S(2) => \disp[5][0]_INST_0_i_343_n_0\,
      S(1) => \disp[5][0]_INST_0_i_344_n_0\,
      S(0) => \disp[5][0]_INST_0_i_345_n_0\
    );
\disp[5][0]_INST_0_i_24\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_20_n_0\,
      I1 => \disp[5][0]_INST_0_i_76_n_0\,
      I2 => \disp[5][0]_INST_0_i_62_n_7\,
      I3 => \disp[5][0]_INST_0_i_61_n_6\,
      I4 => \disp[5][0]_INST_0_i_60_n_5\,
      I5 => \disp[5][0]_INST_0_i_63_n_7\,
      O => \disp[5][0]_INST_0_i_24_n_0\
    );
\disp[5][0]_INST_0_i_240\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_327_n_0\,
      CO(3) => \NLW_disp[5][0]_INST_0_i_240_CO_UNCONNECTED\(3),
      CO(2) => \disp[5][0]_INST_0_i_240_n_1\,
      CO(1) => \NLW_disp[5][0]_INST_0_i_240_CO_UNCONNECTED\(1),
      CO(0) => \disp[5][0]_INST_0_i_240_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \disp[5][0]_INST_0_i_346_n_0\,
      DI(0) => \inst_int_to_string/euros2\(29),
      O(3 downto 2) => \NLW_disp[5][0]_INST_0_i_240_O_UNCONNECTED\(3 downto 2),
      O(1) => \disp[5][0]_INST_0_i_240_n_6\,
      O(0) => \disp[5][0]_INST_0_i_240_n_7\,
      S(3 downto 2) => B"01",
      S(1) => \disp[5][0]_INST_0_i_348_n_0\,
      S(0) => \disp[5][0]_INST_0_i_349_n_0\
    );
\disp[5][0]_INST_0_i_241\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_350_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_241_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_241_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_241_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_241_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_351_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_352_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_353_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_354_n_0\,
      O(3 downto 0) => \NLW_disp[5][0]_INST_0_i_241_O_UNCONNECTED\(3 downto 0),
      S(3) => \disp[5][0]_INST_0_i_355_n_0\,
      S(2) => \disp[5][0]_INST_0_i_356_n_0\,
      S(1) => \disp[5][0]_INST_0_i_357_n_0\,
      S(0) => \disp[5][0]_INST_0_i_358_n_0\
    );
\disp[5][0]_INST_0_i_242\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_253_n_6\,
      I1 => \inst_int_to_string/euros2\(2),
      I2 => \disp[5][0]_INST_0_i_257_n_5\,
      I3 => \disp[5][0]_INST_0_i_255_n_5\,
      I4 => \disp[5][0]_INST_0_i_360_n_0\,
      O => \disp[5][0]_INST_0_i_242_n_0\
    );
\disp[5][0]_INST_0_i_243\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF969600"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_253_n_6\,
      I1 => \inst_int_to_string/euros2\(2),
      I2 => \disp[5][0]_INST_0_i_257_n_5\,
      I3 => \disp[5][0]_INST_0_i_255_n_6\,
      I4 => \disp[5][0]_INST_0_i_361_n_0\,
      O => \disp[5][0]_INST_0_i_243_n_0\
    );
\disp[5][0]_INST_0_i_244\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E800FFE8"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_362_n_4\,
      I1 => \inst_int_to_string/euros2\(0),
      I2 => \disp[5][0]_INST_0_i_257_n_7\,
      I3 => \disp[5][0]_INST_0_i_255_n_7\,
      I4 => \disp[5][0]_INST_0_i_363_n_0\,
      O => \disp[5][0]_INST_0_i_244_n_0\
    );
\disp[5][0]_INST_0_i_245\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EBBE822882288228"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_364_n_4\,
      I1 => \disp[5][0]_INST_0_i_362_n_4\,
      I2 => \inst_int_to_string/euros2\(0),
      I3 => \disp[5][0]_INST_0_i_257_n_7\,
      I4 => \disp[5][0]_INST_0_i_362_n_5\,
      I5 => \disp[5][0]_INST_0_i_365_n_4\,
      O => \disp[5][0]_INST_0_i_245_n_0\
    );
\disp[5][0]_INST_0_i_246\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_242_n_0\,
      I1 => \disp[5][0]_INST_0_i_255_n_4\,
      I2 => \disp[5][0]_INST_0_i_256_n_0\,
      I3 => \disp[5][0]_INST_0_i_250_n_7\,
      I4 => \disp[5][0]_INST_0_i_257_n_4\,
      I5 => \disp[5][0]_INST_0_i_253_n_5\,
      O => \disp[5][0]_INST_0_i_246_n_0\
    );
\disp[5][0]_INST_0_i_247\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"566AA995A995566A"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_243_n_0\,
      I1 => \disp[5][0]_INST_0_i_253_n_6\,
      I2 => \inst_int_to_string/euros2\(2),
      I3 => \disp[5][0]_INST_0_i_257_n_5\,
      I4 => \disp[5][0]_INST_0_i_255_n_5\,
      I5 => \disp[5][0]_INST_0_i_360_n_0\,
      O => \disp[5][0]_INST_0_i_247_n_0\
    );
\disp[5][0]_INST_0_i_248\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6996966996696996"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_244_n_0\,
      I1 => \disp[5][0]_INST_0_i_257_n_5\,
      I2 => \inst_int_to_string/euros2\(2),
      I3 => \disp[5][0]_INST_0_i_253_n_6\,
      I4 => \disp[5][0]_INST_0_i_255_n_6\,
      I5 => \disp[5][0]_INST_0_i_361_n_0\,
      O => \disp[5][0]_INST_0_i_248_n_0\
    );
\disp[5][0]_INST_0_i_249\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A995566A566AA995"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_245_n_0\,
      I1 => \disp[5][0]_INST_0_i_362_n_4\,
      I2 => \inst_int_to_string/euros2\(0),
      I3 => \disp[5][0]_INST_0_i_257_n_7\,
      I4 => \disp[5][0]_INST_0_i_255_n_7\,
      I5 => \disp[5][0]_INST_0_i_363_n_0\,
      O => \disp[5][0]_INST_0_i_249_n_0\
    );
\disp[5][0]_INST_0_i_25\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_77_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_25_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_25_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_25_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_25_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_78_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_79_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_80_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_81_n_0\,
      O(3 downto 0) => \NLW_disp[5][0]_INST_0_i_25_O_UNCONNECTED\(3 downto 0),
      S(3) => \disp[5][0]_INST_0_i_82_n_0\,
      S(2) => \disp[5][0]_INST_0_i_83_n_0\,
      S(1) => \disp[5][0]_INST_0_i_84_n_0\,
      S(0) => \disp[5][0]_INST_0_i_85_n_0\
    );
\disp[5][0]_INST_0_i_250\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \disp[5][0]_INST_0_i_250_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_250_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_250_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_250_n_3\,
      CYINIT => '0',
      DI(3) => \inst_int_to_string/euros2\(6),
      DI(2) => \disp[5][0]_INST_0_i_367_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_368_n_0\,
      DI(0) => '0',
      O(3) => \disp[5][0]_INST_0_i_250_n_4\,
      O(2) => \disp[5][0]_INST_0_i_250_n_5\,
      O(1) => \disp[5][0]_INST_0_i_250_n_6\,
      O(0) => \disp[5][0]_INST_0_i_250_n_7\,
      S(3) => \disp[5][0]_INST_0_i_369_n_0\,
      S(2) => \disp[5][0]_INST_0_i_370_n_0\,
      S(1) => \disp[5][0]_INST_0_i_371_n_0\,
      S(0) => \disp[5][0]_INST_0_i_372_n_0\
    );
\disp[5][0]_INST_0_i_251\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_125_n_5\,
      I1 => \disp[5][0]_INST_0_i_127_n_4\,
      I2 => \disp[5][0]_INST_0_i_122_n_7\,
      O => \disp[5][0]_INST_0_i_251_n_0\
    );
\disp[5][0]_INST_0_i_252\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_125_n_6\,
      I1 => \disp[5][0]_INST_0_i_127_n_5\,
      I2 => \disp[5][0]_INST_0_i_250_n_4\,
      O => \disp[5][0]_INST_0_i_252_n_0\
    );
\disp[5][0]_INST_0_i_253\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_362_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_253_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_253_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_253_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_253_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_373_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_374_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_375_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_376_n_0\,
      O(3) => \disp[5][0]_INST_0_i_253_n_4\,
      O(2) => \disp[5][0]_INST_0_i_253_n_5\,
      O(1) => \disp[5][0]_INST_0_i_253_n_6\,
      O(0) => \disp[5][0]_INST_0_i_253_n_7\,
      S(3) => \disp[5][0]_INST_0_i_377_n_0\,
      S(2) => \disp[5][0]_INST_0_i_378_n_0\,
      S(1) => \disp[5][0]_INST_0_i_379_n_0\,
      S(0) => \disp[5][0]_INST_0_i_380_n_0\
    );
\disp[5][0]_INST_0_i_254\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_125_n_7\,
      I1 => \disp[5][0]_INST_0_i_127_n_6\,
      I2 => \disp[5][0]_INST_0_i_250_n_5\,
      O => \disp[5][0]_INST_0_i_254_n_0\
    );
\disp[5][0]_INST_0_i_255\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_364_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_255_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_255_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_255_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_255_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_381_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_382_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_383_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_384_n_0\,
      O(3) => \disp[5][0]_INST_0_i_255_n_4\,
      O(2) => \disp[5][0]_INST_0_i_255_n_5\,
      O(1) => \disp[5][0]_INST_0_i_255_n_6\,
      O(0) => \disp[5][0]_INST_0_i_255_n_7\,
      S(3) => \disp[5][0]_INST_0_i_385_n_0\,
      S(2) => \disp[5][0]_INST_0_i_386_n_0\,
      S(1) => \disp[5][0]_INST_0_i_387_n_0\,
      S(0) => \disp[5][0]_INST_0_i_388_n_0\
    );
\disp[5][0]_INST_0_i_256\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_253_n_4\,
      I1 => \disp[5][0]_INST_0_i_127_n_7\,
      I2 => \disp[5][0]_INST_0_i_250_n_6\,
      O => \disp[5][0]_INST_0_i_256_n_0\
    );
\disp[5][0]_INST_0_i_257\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_365_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_257_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_257_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_257_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_257_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_389_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_390_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_391_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_392_n_0\,
      O(3) => \disp[5][0]_INST_0_i_257_n_4\,
      O(2) => \disp[5][0]_INST_0_i_257_n_5\,
      O(1) => \disp[5][0]_INST_0_i_257_n_6\,
      O(0) => \disp[5][0]_INST_0_i_257_n_7\,
      S(3) => \disp[5][0]_INST_0_i_393_n_0\,
      S(2) => \disp[5][0]_INST_0_i_394_n_0\,
      S(1) => \disp[5][0]_INST_0_i_395_n_0\,
      S(0) => \disp[5][0]_INST_0_i_396_n_0\
    );
\disp[5][0]_INST_0_i_258\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_122_n_7\,
      I1 => \disp[5][0]_INST_0_i_127_n_4\,
      I2 => \disp[5][0]_INST_0_i_125_n_5\,
      O => \disp[5][0]_INST_0_i_258_n_0\
    );
\disp[5][0]_INST_0_i_259\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_250_n_4\,
      I1 => \disp[5][0]_INST_0_i_127_n_5\,
      I2 => \disp[5][0]_INST_0_i_125_n_6\,
      O => \disp[5][0]_INST_0_i_259_n_0\
    );
\disp[5][0]_INST_0_i_26\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_12_n_4\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(28),
      O => \disp[5][0]_INST_0_i_26_n_0\
    );
\disp[5][0]_INST_0_i_260\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_250_n_5\,
      I1 => \disp[5][0]_INST_0_i_127_n_6\,
      I2 => \disp[5][0]_INST_0_i_125_n_7\,
      O => \disp[5][0]_INST_0_i_260_n_0\
    );
\disp[5][0]_INST_0_i_261\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_250_n_6\,
      I1 => \disp[5][0]_INST_0_i_127_n_7\,
      I2 => \disp[5][0]_INST_0_i_253_n_4\,
      O => \disp[5][0]_INST_0_i_261_n_0\
    );
\disp[5][0]_INST_0_i_262\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCFA00FACCA000A0"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(5),
      I1 => \disp[5][0]_INST_0_i_11_n_6\,
      I2 => \inst_int_to_string/euros3\(3),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_35_n_4\,
      I5 => \inst_int_to_string/euros3\(9),
      O => \disp[5][0]_INST_0_i_262_n_0\
    );
\disp[5][0]_INST_0_i_263\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCFA00FACCA000A0"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(2),
      I1 => \disp[5][0]_INST_0_i_35_n_5\,
      I2 => \inst_int_to_string/euros3\(4),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_11_n_7\,
      I5 => \inst_int_to_string/euros3\(8),
      O => \disp[5][0]_INST_0_i_263_n_0\
    );
\disp[5][0]_INST_0_i_264\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DFD54F45D5D04540"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_397_n_0\,
      I1 => \disp[5][0]_INST_0_i_35_n_4\,
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(3),
      I4 => \disp[5][0]_INST_0_i_35_n_6\,
      I5 => \inst_int_to_string/euros3\(1),
      O => \disp[5][0]_INST_0_i_264_n_0\
    );
\disp[5][0]_INST_0_i_265\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"656A959A6A659A95"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_397_n_0\,
      I1 => \disp[5][0]_INST_0_i_35_n_6\,
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(1),
      I4 => \disp[5][0]_INST_0_i_35_n_4\,
      I5 => \inst_int_to_string/euros3\(3),
      O => \disp[5][0]_INST_0_i_265_n_0\
    );
\disp[5][0]_INST_0_i_266\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A6A956595956A9A6"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_262_n_0\,
      I1 => \inst_int_to_string/euros3\(10),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(6),
      I4 => \disp[5][0]_INST_0_i_11_n_5\,
      I5 => \inst_int_to_string/euros2\(4),
      O => \disp[5][0]_INST_0_i_266_n_0\
    );
\disp[5][0]_INST_0_i_267\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A659A95656A959A6"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_263_n_0\,
      I1 => \inst_int_to_string/euros3\(9),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros2\(5),
      I4 => \inst_int_to_string/euros3\(3),
      I5 => \disp[5][0]_INST_0_i_35_n_4\,
      O => \disp[5][0]_INST_0_i_267_n_0\
    );
\disp[5][0]_INST_0_i_268\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A6A956595956A9A6"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_264_n_0\,
      I1 => \inst_int_to_string/euros3\(8),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(4),
      I4 => \disp[5][0]_INST_0_i_11_n_7\,
      I5 => \inst_int_to_string/euros2\(2),
      O => \disp[5][0]_INST_0_i_268_n_0\
    );
\disp[5][0]_INST_0_i_269\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"96696969"
    )
        port map (
      I0 => \inst_int_to_string/euros2\(3),
      I1 => \inst_int_to_string/euros2\(1),
      I2 => \disp[5][0]_INST_0_i_397_n_0\,
      I3 => \inst_int_to_string/euros2\(0),
      I4 => \inst_int_to_string/euros2\(2),
      O => \disp[5][0]_INST_0_i_269_n_0\
    );
\disp[5][0]_INST_0_i_27\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_12_n_5\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(27),
      O => \disp[5][0]_INST_0_i_27_n_0\
    );
\disp[5][0]_INST_0_i_270\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(8),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      O => \inst_int_to_string/euros2\(8)
    );
\disp[5][0]_INST_0_i_271\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(7),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \disp[5][0]_INST_0_i_11_n_4\,
      O => \disp[5][0]_INST_0_i_271_n_0\
    );
\disp[5][0]_INST_0_i_272\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(6),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \disp[5][0]_INST_0_i_11_n_5\,
      O => \disp[5][0]_INST_0_i_272_n_0\
    );
\disp[5][0]_INST_0_i_273\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_11_n_6\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(5),
      O => \disp[5][0]_INST_0_i_273_n_0\
    );
\disp[5][0]_INST_0_i_274\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(8),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(11),
      O => \disp[5][0]_INST_0_i_274_n_0\
    );
\disp[5][0]_INST_0_i_275\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3A35"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(7),
      I1 => \disp[5][0]_INST_0_i_11_n_4\,
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(10),
      O => \disp[5][0]_INST_0_i_275_n_0\
    );
\disp[5][0]_INST_0_i_276\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3A35"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(6),
      I1 => \disp[5][0]_INST_0_i_11_n_5\,
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(9),
      O => \disp[5][0]_INST_0_i_276_n_0\
    );
\disp[5][0]_INST_0_i_277\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3A35"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(5),
      I1 => \disp[5][0]_INST_0_i_11_n_6\,
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(8),
      O => \disp[5][0]_INST_0_i_277_n_0\
    );
\disp[5][0]_INST_0_i_278\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(14),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(10),
      I3 => \inst_int_to_string/euros3\(12),
      O => \disp[5][0]_INST_0_i_278_n_0\
    );
\disp[5][0]_INST_0_i_279\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(13),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(9),
      I3 => \inst_int_to_string/euros3\(11),
      O => \disp[5][0]_INST_0_i_279_n_0\
    );
\disp[5][0]_INST_0_i_28\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_12_n_6\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(26),
      O => \disp[5][0]_INST_0_i_28_n_0\
    );
\disp[5][0]_INST_0_i_280\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(12),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(8),
      I3 => \inst_int_to_string/euros3\(10),
      O => \disp[5][0]_INST_0_i_280_n_0\
    );
\disp[5][0]_INST_0_i_281\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFCD"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(9),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(11),
      I3 => \inst_int_to_string/euros3\(7),
      O => \disp[5][0]_INST_0_i_281_n_0\
    );
\disp[5][0]_INST_0_i_282\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(15),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(11),
      I3 => \inst_int_to_string/euros3\(13),
      I4 => \disp[5][0]_INST_0_i_278_n_0\,
      O => \disp[5][0]_INST_0_i_282_n_0\
    );
\disp[5][0]_INST_0_i_283\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(14),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(10),
      I3 => \inst_int_to_string/euros3\(12),
      I4 => \disp[5][0]_INST_0_i_279_n_0\,
      O => \disp[5][0]_INST_0_i_283_n_0\
    );
\disp[5][0]_INST_0_i_284\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(13),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(9),
      I3 => \inst_int_to_string/euros3\(11),
      I4 => \disp[5][0]_INST_0_i_280_n_0\,
      O => \disp[5][0]_INST_0_i_284_n_0\
    );
\disp[5][0]_INST_0_i_285\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(12),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(8),
      I3 => \inst_int_to_string/euros3\(10),
      I4 => \disp[5][0]_INST_0_i_281_n_0\,
      O => \disp[5][0]_INST_0_i_285_n_0\
    );
\disp[5][0]_INST_0_i_286\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(21),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(23),
      I3 => \inst_int_to_string/euros3\(16),
      O => \disp[5][0]_INST_0_i_286_n_0\
    );
\disp[5][0]_INST_0_i_287\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(20),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(22),
      I3 => \inst_int_to_string/euros3\(15),
      O => \disp[5][0]_INST_0_i_287_n_0\
    );
\disp[5][0]_INST_0_i_288\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(19),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(21),
      I3 => \inst_int_to_string/euros3\(14),
      O => \disp[5][0]_INST_0_i_288_n_0\
    );
\disp[5][0]_INST_0_i_289\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(18),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(20),
      I3 => \inst_int_to_string/euros3\(13),
      O => \disp[5][0]_INST_0_i_289_n_0\
    );
\disp[5][0]_INST_0_i_29\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_12_n_7\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(25),
      O => \disp[5][0]_INST_0_i_29_n_0\
    );
\disp[5][0]_INST_0_i_290\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(22),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(24),
      I3 => \inst_int_to_string/euros3\(17),
      I4 => \disp[5][0]_INST_0_i_286_n_0\,
      O => \disp[5][0]_INST_0_i_290_n_0\
    );
\disp[5][0]_INST_0_i_291\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(21),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(23),
      I3 => \inst_int_to_string/euros3\(16),
      I4 => \disp[5][0]_INST_0_i_287_n_0\,
      O => \disp[5][0]_INST_0_i_291_n_0\
    );
\disp[5][0]_INST_0_i_292\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(20),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(22),
      I3 => \inst_int_to_string/euros3\(15),
      I4 => \disp[5][0]_INST_0_i_288_n_0\,
      O => \disp[5][0]_INST_0_i_292_n_0\
    );
\disp[5][0]_INST_0_i_293\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(19),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(21),
      I3 => \inst_int_to_string/euros3\(14),
      I4 => \disp[5][0]_INST_0_i_289_n_0\,
      O => \disp[5][0]_INST_0_i_293_n_0\
    );
\disp[5][0]_INST_0_i_294\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_295_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_294_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_294_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_294_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_294_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \inst_int_to_string/euros3\(12 downto 9),
      S(3) => \disp[5][0]_INST_0_i_4_n_3\,
      S(2) => \disp[5][0]_INST_0_i_4_n_3\,
      S(1) => \disp[5][0]_INST_0_i_4_n_3\,
      S(0) => \disp[5][0]_INST_0_i_4_n_3\
    );
\disp[5][0]_INST_0_i_295\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_296_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_295_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_295_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_295_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_295_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \inst_int_to_string/euros3\(8 downto 5),
      S(3) => \disp[5][0]_INST_0_i_4_n_3\,
      S(2) => \disp[5][0]_INST_0_i_401_n_0\,
      S(1) => \disp[5][0]_INST_0_i_402_n_0\,
      S(0) => \disp[5][0]_INST_0_i_403_n_0\
    );
\disp[5][0]_INST_0_i_296\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \disp[5][0]_INST_0_i_296_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_296_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_296_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_296_n_3\,
      CYINIT => \disp[5][0]_INST_0_i_404_n_0\,
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \inst_int_to_string/euros3\(4 downto 1),
      S(3) => \disp[5][0]_INST_0_i_405_n_0\,
      S(2) => \disp[5][0]_INST_0_i_406_n_0\,
      S(1) => \disp[5][0]_INST_0_i_407_n_0\,
      S(0) => \disp[5][0]_INST_0_i_408_n_0\
    );
\disp[5][0]_INST_0_i_297\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_11_n_6\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(5),
      O => \inst_int_to_string/euros2\(5)
    );
\disp[5][0]_INST_0_i_298\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_409_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_298_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_298_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_298_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_298_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_410_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_411_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_412_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_413_n_0\,
      O(3 downto 0) => \NLW_disp[5][0]_INST_0_i_298_O_UNCONNECTED\(3 downto 0),
      S(3) => \disp[5][0]_INST_0_i_414_n_0\,
      S(2) => \disp[5][0]_INST_0_i_415_n_0\,
      S(1) => \disp[5][0]_INST_0_i_416_n_0\,
      S(0) => \disp[5][0]_INST_0_i_417_n_0\
    );
\disp[5][0]_INST_0_i_299\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_219_n_4\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(16),
      O => \disp[5][0]_INST_0_i_299_n_0\
    );
\disp[5][0]_INST_0_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_10_n_0\,
      CO(3 downto 1) => \NLW_disp[5][0]_INST_0_i_3_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \disp[5][0]_INST_0_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 2) => \NLW_disp[5][0]_INST_0_i_3_O_UNCONNECTED\(3 downto 2),
      O(1 downto 0) => \inst_int_to_string/euros3\(30 downto 29),
      S(3 downto 2) => B"00",
      S(1) => \disp[5][0]_INST_0_i_4_n_3\,
      S(0) => \disp[5][0]_INST_0_i_4_n_3\
    );
\disp[5][0]_INST_0_i_30\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(28),
      I1 => \disp[5][0]_INST_0_i_12_n_4\,
      I2 => \inst_int_to_string/euros3\(29),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_5_n_7\,
      O => \disp[5][0]_INST_0_i_30_n_0\
    );
\disp[5][0]_INST_0_i_300\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_219_n_5\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(15),
      O => \disp[5][0]_INST_0_i_300_n_0\
    );
\disp[5][0]_INST_0_i_301\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_219_n_6\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(14),
      O => \disp[5][0]_INST_0_i_301_n_0\
    );
\disp[5][0]_INST_0_i_302\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_219_n_7\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(13),
      O => \disp[5][0]_INST_0_i_302_n_0\
    );
\disp[5][0]_INST_0_i_303\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(16),
      I1 => \disp[5][0]_INST_0_i_219_n_4\,
      I2 => \inst_int_to_string/euros3\(17),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_94_n_7\,
      O => \disp[5][0]_INST_0_i_303_n_0\
    );
\disp[5][0]_INST_0_i_304\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(15),
      I1 => \disp[5][0]_INST_0_i_219_n_5\,
      I2 => \inst_int_to_string/euros3\(16),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_219_n_4\,
      O => \disp[5][0]_INST_0_i_304_n_0\
    );
\disp[5][0]_INST_0_i_305\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(14),
      I1 => \disp[5][0]_INST_0_i_219_n_6\,
      I2 => \inst_int_to_string/euros3\(15),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_219_n_5\,
      O => \disp[5][0]_INST_0_i_305_n_0\
    );
\disp[5][0]_INST_0_i_306\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(13),
      I1 => \disp[5][0]_INST_0_i_219_n_7\,
      I2 => \inst_int_to_string/euros3\(14),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_219_n_6\,
      O => \disp[5][0]_INST_0_i_306_n_0\
    );
\disp[5][0]_INST_0_i_307\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3 downto 2) => \NLW_disp[5][0]_INST_0_i_307_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \disp[5][0]_INST_0_i_307_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_307_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \disp[5][0]_INST_0_i_418_n_0\,
      DI(0) => '0',
      O(3) => \NLW_disp[5][0]_INST_0_i_307_O_UNCONNECTED\(3),
      O(2) => \disp[5][0]_INST_0_i_307_n_5\,
      O(1) => \disp[5][0]_INST_0_i_307_n_6\,
      O(0) => \disp[5][0]_INST_0_i_307_n_7\,
      S(3) => '0',
      S(2) => \disp[5][0]_INST_0_i_419_n_0\,
      S(1) => \disp[5][0]_INST_0_i_420_n_0\,
      S(0) => \disp[5][0]_INST_0_i_421_n_0\
    );
\disp[5][0]_INST_0_i_308\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_422_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_308_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_308_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_308_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_308_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_423_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_424_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_425_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_426_n_0\,
      O(3) => \disp[5][0]_INST_0_i_308_n_4\,
      O(2) => \disp[5][0]_INST_0_i_308_n_5\,
      O(1) => \disp[5][0]_INST_0_i_308_n_6\,
      O(0) => \disp[5][0]_INST_0_i_308_n_7\,
      S(3) => \disp[5][0]_INST_0_i_427_n_0\,
      S(2) => \disp[5][0]_INST_0_i_428_n_0\,
      S(1) => \disp[5][0]_INST_0_i_429_n_0\,
      S(0) => \disp[5][0]_INST_0_i_430_n_0\
    );
\disp[5][0]_INST_0_i_309\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_317_n_7\,
      I1 => \disp[5][0]_INST_0_i_317_n_5\,
      I2 => \disp[5][0]_INST_0_i_228_n_6\,
      O => \disp[5][0]_INST_0_i_309_n_0\
    );
\disp[5][0]_INST_0_i_31\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(27),
      I1 => \disp[5][0]_INST_0_i_12_n_5\,
      I2 => \inst_int_to_string/euros3\(28),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_12_n_4\,
      O => \disp[5][0]_INST_0_i_31_n_0\
    );
\disp[5][0]_INST_0_i_310\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_431_n_4\,
      I1 => \disp[5][0]_INST_0_i_317_n_6\,
      I2 => \disp[5][0]_INST_0_i_228_n_7\,
      O => \disp[5][0]_INST_0_i_310_n_0\
    );
\disp[5][0]_INST_0_i_311\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_431_n_5\,
      I1 => \disp[5][0]_INST_0_i_317_n_7\,
      I2 => \disp[5][0]_INST_0_i_317_n_4\,
      O => \disp[5][0]_INST_0_i_311_n_0\
    );
\disp[5][0]_INST_0_i_312\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_431_n_6\,
      I1 => \disp[5][0]_INST_0_i_431_n_4\,
      I2 => \disp[5][0]_INST_0_i_317_n_5\,
      O => \disp[5][0]_INST_0_i_312_n_0\
    );
\disp[5][0]_INST_0_i_313\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_317_n_6\,
      I1 => \disp[5][0]_INST_0_i_317_n_4\,
      I2 => \disp[5][0]_INST_0_i_228_n_5\,
      I3 => \disp[5][0]_INST_0_i_309_n_0\,
      O => \disp[5][0]_INST_0_i_313_n_0\
    );
\disp[5][0]_INST_0_i_314\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_317_n_7\,
      I1 => \disp[5][0]_INST_0_i_317_n_5\,
      I2 => \disp[5][0]_INST_0_i_228_n_6\,
      I3 => \disp[5][0]_INST_0_i_310_n_0\,
      O => \disp[5][0]_INST_0_i_314_n_0\
    );
\disp[5][0]_INST_0_i_315\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_431_n_4\,
      I1 => \disp[5][0]_INST_0_i_317_n_6\,
      I2 => \disp[5][0]_INST_0_i_228_n_7\,
      I3 => \disp[5][0]_INST_0_i_311_n_0\,
      O => \disp[5][0]_INST_0_i_315_n_0\
    );
\disp[5][0]_INST_0_i_316\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_431_n_5\,
      I1 => \disp[5][0]_INST_0_i_317_n_7\,
      I2 => \disp[5][0]_INST_0_i_317_n_4\,
      I3 => \disp[5][0]_INST_0_i_312_n_0\,
      O => \disp[5][0]_INST_0_i_316_n_0\
    );
\disp[5][0]_INST_0_i_317\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_431_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_317_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_317_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_317_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_317_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_432_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_433_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_434_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_435_n_0\,
      O(3) => \disp[5][0]_INST_0_i_317_n_4\,
      O(2) => \disp[5][0]_INST_0_i_317_n_5\,
      O(1) => \disp[5][0]_INST_0_i_317_n_6\,
      O(0) => \disp[5][0]_INST_0_i_317_n_7\,
      S(3) => \disp[5][0]_INST_0_i_436_n_0\,
      S(2) => \disp[5][0]_INST_0_i_437_n_0\,
      S(1) => \disp[5][0]_INST_0_i_438_n_0\,
      S(0) => \disp[5][0]_INST_0_i_439_n_0\
    );
\disp[5][0]_INST_0_i_318\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E8E800E80000E8"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_327_n_6\,
      I1 => \disp[5][0]_INST_0_i_239_n_5\,
      I2 => \disp[5][0]_INST_0_i_440_n_4\,
      I3 => \disp[5][0]_INST_0_i_327_n_5\,
      I4 => \disp[5][0]_INST_0_i_239_n_0\,
      I5 => \disp[5][0]_INST_0_i_326_n_7\,
      O => \disp[5][0]_INST_0_i_318_n_0\
    );
\disp[5][0]_INST_0_i_319\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E80000E800E8E800"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_327_n_7\,
      I1 => \disp[5][0]_INST_0_i_239_n_6\,
      I2 => \disp[5][0]_INST_0_i_440_n_5\,
      I3 => \disp[5][0]_INST_0_i_239_n_5\,
      I4 => \disp[5][0]_INST_0_i_440_n_4\,
      I5 => \disp[5][0]_INST_0_i_327_n_6\,
      O => \disp[5][0]_INST_0_i_319_n_0\
    );
\disp[5][0]_INST_0_i_32\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(26),
      I1 => \disp[5][0]_INST_0_i_12_n_6\,
      I2 => \inst_int_to_string/euros3\(27),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_12_n_5\,
      O => \disp[5][0]_INST_0_i_32_n_0\
    );
\disp[5][0]_INST_0_i_320\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E80000E800E8E800"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_441_n_4\,
      I1 => \disp[5][0]_INST_0_i_239_n_7\,
      I2 => \disp[5][0]_INST_0_i_440_n_6\,
      I3 => \disp[5][0]_INST_0_i_239_n_6\,
      I4 => \disp[5][0]_INST_0_i_440_n_5\,
      I5 => \disp[5][0]_INST_0_i_327_n_7\,
      O => \disp[5][0]_INST_0_i_320_n_0\
    );
\disp[5][0]_INST_0_i_321\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E80000E800E8E800"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_441_n_5\,
      I1 => \disp[5][0]_INST_0_i_339_n_4\,
      I2 => \disp[5][0]_INST_0_i_440_n_7\,
      I3 => \disp[5][0]_INST_0_i_239_n_7\,
      I4 => \disp[5][0]_INST_0_i_440_n_6\,
      I5 => \disp[5][0]_INST_0_i_441_n_4\,
      O => \disp[5][0]_INST_0_i_321_n_0\
    );
\disp[5][0]_INST_0_i_322\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AA9955695566AA9"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_318_n_0\,
      I1 => \disp[5][0]_INST_0_i_327_n_5\,
      I2 => \disp[5][0]_INST_0_i_239_n_0\,
      I3 => \disp[5][0]_INST_0_i_326_n_7\,
      I4 => \disp[5][0]_INST_0_i_326_n_6\,
      I5 => \disp[5][0]_INST_0_i_327_n_4\,
      O => \disp[5][0]_INST_0_i_322_n_0\
    );
\disp[5][0]_INST_0_i_323\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99969666"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_319_n_0\,
      I1 => \disp[5][0]_INST_0_i_442_n_0\,
      I2 => \disp[5][0]_INST_0_i_440_n_4\,
      I3 => \disp[5][0]_INST_0_i_239_n_5\,
      I4 => \disp[5][0]_INST_0_i_327_n_6\,
      O => \disp[5][0]_INST_0_i_323_n_0\
    );
\disp[5][0]_INST_0_i_324\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66696999"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_320_n_0\,
      I1 => \disp[5][0]_INST_0_i_443_n_0\,
      I2 => \disp[5][0]_INST_0_i_440_n_5\,
      I3 => \disp[5][0]_INST_0_i_239_n_6\,
      I4 => \disp[5][0]_INST_0_i_327_n_7\,
      O => \disp[5][0]_INST_0_i_324_n_0\
    );
\disp[5][0]_INST_0_i_325\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66696999"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_321_n_0\,
      I1 => \disp[5][0]_INST_0_i_444_n_0\,
      I2 => \disp[5][0]_INST_0_i_440_n_6\,
      I3 => \disp[5][0]_INST_0_i_239_n_7\,
      I4 => \disp[5][0]_INST_0_i_441_n_4\,
      O => \disp[5][0]_INST_0_i_325_n_0\
    );
\disp[5][0]_INST_0_i_326\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_440_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_326_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_326_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_326_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_326_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_445_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_446_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_447_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_448_n_0\,
      O(3) => \disp[5][0]_INST_0_i_326_n_4\,
      O(2) => \disp[5][0]_INST_0_i_326_n_5\,
      O(1) => \disp[5][0]_INST_0_i_326_n_6\,
      O(0) => \disp[5][0]_INST_0_i_326_n_7\,
      S(3) => \disp[5][0]_INST_0_i_449_n_0\,
      S(2) => \disp[5][0]_INST_0_i_450_n_0\,
      S(1) => \disp[5][0]_INST_0_i_451_n_0\,
      S(0) => \disp[5][0]_INST_0_i_452_n_0\
    );
\disp[5][0]_INST_0_i_327\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_441_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_327_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_327_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_327_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_327_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \inst_int_to_string/euros2\(28 downto 25),
      O(3) => \disp[5][0]_INST_0_i_327_n_4\,
      O(2) => \disp[5][0]_INST_0_i_327_n_5\,
      O(1) => \disp[5][0]_INST_0_i_327_n_6\,
      O(0) => \disp[5][0]_INST_0_i_327_n_7\,
      S(3) => \disp[5][0]_INST_0_i_457_n_0\,
      S(2) => \disp[5][0]_INST_0_i_458_n_0\,
      S(1) => \disp[5][0]_INST_0_i_459_n_0\,
      S(0) => \disp[5][0]_INST_0_i_460_n_0\
    );
\disp[5][0]_INST_0_i_328\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(29),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(27),
      O => \disp[5][0]_INST_0_i_328_n_0\
    );
\disp[5][0]_INST_0_i_329\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(28),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(26),
      O => \disp[5][0]_INST_0_i_329_n_0\
    );
\disp[5][0]_INST_0_i_33\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(25),
      I1 => \disp[5][0]_INST_0_i_12_n_7\,
      I2 => \inst_int_to_string/euros3\(26),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_12_n_6\,
      O => \disp[5][0]_INST_0_i_33_n_0\
    );
\disp[5][0]_INST_0_i_330\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(27),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(25),
      O => \disp[5][0]_INST_0_i_330_n_0\
    );
\disp[5][0]_INST_0_i_331\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"12"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(25),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(27),
      O => \disp[5][0]_INST_0_i_331_n_0\
    );
\disp[5][0]_INST_0_i_332\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00870078"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(27),
      I1 => \inst_int_to_string/euros3\(29),
      I2 => \inst_int_to_string/euros3\(30),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \inst_int_to_string/euros3\(28),
      O => \disp[5][0]_INST_0_i_332_n_0\
    );
\disp[5][0]_INST_0_i_333\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00870078"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(26),
      I1 => \inst_int_to_string/euros3\(28),
      I2 => \inst_int_to_string/euros3\(29),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \inst_int_to_string/euros3\(27),
      O => \disp[5][0]_INST_0_i_333_n_0\
    );
\disp[5][0]_INST_0_i_334\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00870078"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(25),
      I1 => \inst_int_to_string/euros3\(27),
      I2 => \inst_int_to_string/euros3\(28),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \inst_int_to_string/euros3\(26),
      O => \disp[5][0]_INST_0_i_334_n_0\
    );
\disp[5][0]_INST_0_i_335\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000E817000017E8"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(24),
      I1 => \inst_int_to_string/euros3\(26),
      I2 => \inst_int_to_string/euros3\(30),
      I3 => \inst_int_to_string/euros3\(27),
      I4 => \disp[5][0]_INST_0_i_4_n_3\,
      I5 => \inst_int_to_string/euros3\(25),
      O => \disp[5][0]_INST_0_i_335_n_0\
    );
\disp[5][0]_INST_0_i_336\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(30),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(28),
      O => \disp[5][0]_INST_0_i_336_n_0\
    );
\disp[5][0]_INST_0_i_337\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(30),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      O => \inst_int_to_string/euros2\(30)
    );
\disp[5][0]_INST_0_i_338\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0708"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(28),
      I1 => \inst_int_to_string/euros3\(30),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(29),
      O => \disp[5][0]_INST_0_i_338_n_0\
    );
\disp[5][0]_INST_0_i_339\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_72_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_339_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_339_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_339_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_339_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_461_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_462_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_463_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_464_n_0\,
      O(3) => \disp[5][0]_INST_0_i_339_n_4\,
      O(2) => \disp[5][0]_INST_0_i_339_n_5\,
      O(1) => \disp[5][0]_INST_0_i_339_n_6\,
      O(0) => \disp[5][0]_INST_0_i_339_n_7\,
      S(3) => \disp[5][0]_INST_0_i_465_n_0\,
      S(2) => \disp[5][0]_INST_0_i_466_n_0\,
      S(1) => \disp[5][0]_INST_0_i_467_n_0\,
      S(0) => \disp[5][0]_INST_0_i_468_n_0\
    );
\disp[5][0]_INST_0_i_34\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_86_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_34_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_34_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_34_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_34_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \inst_int_to_string/euros3\(24 downto 21),
      S(3) => \disp[5][0]_INST_0_i_4_n_3\,
      S(2) => \disp[5][0]_INST_0_i_4_n_3\,
      S(1) => \disp[5][0]_INST_0_i_4_n_3\,
      S(0) => \disp[5][0]_INST_0_i_4_n_3\
    );
\disp[5][0]_INST_0_i_340\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(30),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      O => \disp[5][0]_INST_0_i_340_n_0\
    );
\disp[5][0]_INST_0_i_341\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(29),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      O => \disp[5][0]_INST_0_i_341_n_0\
    );
\disp[5][0]_INST_0_i_342\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EF"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(27),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(29),
      O => \disp[5][0]_INST_0_i_342_n_0\
    );
\disp[5][0]_INST_0_i_343\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_4_n_3\,
      I1 => \inst_int_to_string/euros3\(30),
      O => \disp[5][0]_INST_0_i_343_n_0\
    );
\disp[5][0]_INST_0_i_344\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F2FD"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(30),
      I1 => \inst_int_to_string/euros3\(28),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(29),
      O => \disp[5][0]_INST_0_i_344_n_0\
    );
\disp[5][0]_INST_0_i_345\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFD2FF2D"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(29),
      I1 => \inst_int_to_string/euros3\(27),
      I2 => \inst_int_to_string/euros3\(30),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \inst_int_to_string/euros3\(28),
      O => \disp[5][0]_INST_0_i_345_n_0\
    );
\disp[5][0]_INST_0_i_346\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(30),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      O => \disp[5][0]_INST_0_i_346_n_0\
    );
\disp[5][0]_INST_0_i_347\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(29),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      O => \inst_int_to_string/euros2\(29)
    );
\disp[5][0]_INST_0_i_348\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_4_n_3\,
      I1 => \inst_int_to_string/euros3\(30),
      O => \disp[5][0]_INST_0_i_348_n_0\
    );
\disp[5][0]_INST_0_i_349\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_4_n_3\,
      I1 => \inst_int_to_string/euros3\(29),
      O => \disp[5][0]_INST_0_i_349_n_0\
    );
\disp[5][0]_INST_0_i_35\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \disp[5][0]_INST_0_i_35_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_35_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_35_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_35_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => total_out(3 downto 0),
      O(3) => \disp[5][0]_INST_0_i_35_n_4\,
      O(2) => \disp[5][0]_INST_0_i_35_n_5\,
      O(1) => \disp[5][0]_INST_0_i_35_n_6\,
      O(0) => \inst_int_to_string/euros2\(0),
      S(3) => \disp[5][0]_INST_0_i_87_n_0\,
      S(2) => \disp[5][0]_INST_0_i_88_n_0\,
      S(1) => \disp[5][0]_INST_0_i_89_n_0\,
      S(0) => \disp[5][0]_INST_0_i_90_n_0\
    );
\disp[5][0]_INST_0_i_350\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \disp[5][0]_INST_0_i_350_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_350_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_350_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_350_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_469_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_470_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_471_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_472_n_0\,
      O(3 downto 0) => \NLW_disp[5][0]_INST_0_i_350_O_UNCONNECTED\(3 downto 0),
      S(3) => \disp[5][0]_INST_0_i_473_n_0\,
      S(2) => \disp[5][0]_INST_0_i_474_n_0\,
      S(1) => \disp[5][0]_INST_0_i_475_n_0\,
      S(0) => \disp[5][0]_INST_0_i_476_n_0\
    );
\disp[5][0]_INST_0_i_351\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BE282828"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_364_n_5\,
      I1 => \disp[5][0]_INST_0_i_365_n_4\,
      I2 => \disp[5][0]_INST_0_i_362_n_5\,
      I3 => \disp[5][0]_INST_0_i_362_n_6\,
      I4 => \disp[5][0]_INST_0_i_365_n_5\,
      O => \disp[5][0]_INST_0_i_351_n_0\
    );
\disp[5][0]_INST_0_i_352\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BE282828"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_364_n_6\,
      I1 => \disp[5][0]_INST_0_i_365_n_5\,
      I2 => \disp[5][0]_INST_0_i_362_n_6\,
      I3 => \inst_int_to_string/euros2\(0),
      I4 => \disp[5][0]_INST_0_i_365_n_6\,
      O => \disp[5][0]_INST_0_i_352_n_0\
    );
\disp[5][0]_INST_0_i_353\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"28"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_364_n_7\,
      I1 => \disp[5][0]_INST_0_i_365_n_6\,
      I2 => \inst_int_to_string/euros2\(0),
      O => \disp[5][0]_INST_0_i_353_n_0\
    );
\disp[5][0]_INST_0_i_354\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_477_n_4\,
      I1 => \disp[5][0]_INST_0_i_365_n_7\,
      O => \disp[5][0]_INST_0_i_354_n_0\
    );
\disp[5][0]_INST_0_i_355\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_351_n_0\,
      I1 => \disp[5][0]_INST_0_i_364_n_4\,
      I2 => \disp[5][0]_INST_0_i_478_n_0\,
      I3 => \disp[5][0]_INST_0_i_362_n_5\,
      I4 => \disp[5][0]_INST_0_i_365_n_4\,
      O => \disp[5][0]_INST_0_i_355_n_0\
    );
\disp[5][0]_INST_0_i_356\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669966996"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_352_n_0\,
      I1 => \disp[5][0]_INST_0_i_364_n_5\,
      I2 => \disp[5][0]_INST_0_i_365_n_4\,
      I3 => \disp[5][0]_INST_0_i_362_n_5\,
      I4 => \disp[5][0]_INST_0_i_362_n_6\,
      I5 => \disp[5][0]_INST_0_i_365_n_5\,
      O => \disp[5][0]_INST_0_i_356_n_0\
    );
\disp[5][0]_INST_0_i_357\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669966996"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_353_n_0\,
      I1 => \disp[5][0]_INST_0_i_364_n_6\,
      I2 => \disp[5][0]_INST_0_i_365_n_5\,
      I3 => \disp[5][0]_INST_0_i_362_n_6\,
      I4 => \inst_int_to_string/euros2\(0),
      I5 => \disp[5][0]_INST_0_i_365_n_6\,
      O => \disp[5][0]_INST_0_i_357_n_0\
    );
\disp[5][0]_INST_0_i_358\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6996"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_364_n_7\,
      I1 => \disp[5][0]_INST_0_i_365_n_6\,
      I2 => \inst_int_to_string/euros2\(0),
      I3 => \disp[5][0]_INST_0_i_354_n_0\,
      O => \disp[5][0]_INST_0_i_358_n_0\
    );
\disp[5][0]_INST_0_i_359\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_35_n_5\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(2),
      O => \inst_int_to_string/euros2\(2)
    );
\disp[5][0]_INST_0_i_36\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => total_out(7),
      O => \disp[5][0]_INST_0_i_36_n_0\
    );
\disp[5][0]_INST_0_i_360\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_253_n_5\,
      I1 => \disp[5][0]_INST_0_i_257_n_4\,
      I2 => \disp[5][0]_INST_0_i_250_n_7\,
      O => \disp[5][0]_INST_0_i_360_n_0\
    );
\disp[5][0]_INST_0_i_361\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEAEA808"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_253_n_7\,
      I1 => \inst_int_to_string/euros3\(1),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \disp[5][0]_INST_0_i_35_n_6\,
      I4 => \disp[5][0]_INST_0_i_257_n_6\,
      O => \disp[5][0]_INST_0_i_361_n_0\
    );
\disp[5][0]_INST_0_i_362\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \disp[5][0]_INST_0_i_362_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_362_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_362_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_362_n_3\,
      CYINIT => '0',
      DI(3) => \inst_int_to_string/euros2\(0),
      DI(2 downto 0) => B"001",
      O(3) => \disp[5][0]_INST_0_i_362_n_4\,
      O(2) => \disp[5][0]_INST_0_i_362_n_5\,
      O(1) => \disp[5][0]_INST_0_i_362_n_6\,
      O(0) => \NLW_disp[5][0]_INST_0_i_362_O_UNCONNECTED\(0),
      S(3) => \disp[5][0]_INST_0_i_479_n_0\,
      S(2) => \disp[5][0]_INST_0_i_480_n_0\,
      S(1) => \disp[5][0]_INST_0_i_481_n_0\,
      S(0) => \inst_int_to_string/euros2\(0)
    );
\disp[5][0]_INST_0_i_363\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_257_n_6\,
      I1 => \disp[5][0]_INST_0_i_35_n_6\,
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(1),
      I4 => \disp[5][0]_INST_0_i_253_n_7\,
      O => \disp[5][0]_INST_0_i_363_n_0\
    );
\disp[5][0]_INST_0_i_364\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_477_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_364_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_364_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_364_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_364_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_482_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_483_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_484_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_485_n_0\,
      O(3) => \disp[5][0]_INST_0_i_364_n_4\,
      O(2) => \disp[5][0]_INST_0_i_364_n_5\,
      O(1) => \disp[5][0]_INST_0_i_364_n_6\,
      O(0) => \disp[5][0]_INST_0_i_364_n_7\,
      S(3) => \disp[5][0]_INST_0_i_486_n_0\,
      S(2) => \disp[5][0]_INST_0_i_487_n_0\,
      S(1) => \disp[5][0]_INST_0_i_488_n_0\,
      S(0) => \disp[5][0]_INST_0_i_489_n_0\
    );
\disp[5][0]_INST_0_i_365\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_490_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_365_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_365_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_365_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_365_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_491_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_492_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_493_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_494_n_0\,
      O(3) => \disp[5][0]_INST_0_i_365_n_4\,
      O(2) => \disp[5][0]_INST_0_i_365_n_5\,
      O(1) => \disp[5][0]_INST_0_i_365_n_6\,
      O(0) => \disp[5][0]_INST_0_i_365_n_7\,
      S(3) => \disp[5][0]_INST_0_i_495_n_0\,
      S(2) => \disp[5][0]_INST_0_i_496_n_0\,
      S(1) => \disp[5][0]_INST_0_i_497_n_0\,
      S(0) => \disp[5][0]_INST_0_i_498_n_0\
    );
\disp[5][0]_INST_0_i_366\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(6),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \disp[5][0]_INST_0_i_11_n_5\,
      O => \inst_int_to_string/euros2\(6)
    );
\disp[5][0]_INST_0_i_367\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_11_n_6\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(5),
      O => \disp[5][0]_INST_0_i_367_n_0\
    );
\disp[5][0]_INST_0_i_368\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_11_n_7\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(4),
      O => \disp[5][0]_INST_0_i_368_n_0\
    );
\disp[5][0]_INST_0_i_369\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9999A55A6666A55A"
    )
        port map (
      I0 => \inst_int_to_string/euros2\(0),
      I1 => \disp[5][0]_INST_0_i_35_n_5\,
      I2 => \inst_int_to_string/euros3\(2),
      I3 => \inst_int_to_string/euros3\(6),
      I4 => \disp[5][0]_INST_0_i_4_n_3\,
      I5 => \disp[5][0]_INST_0_i_11_n_5\,
      O => \disp[5][0]_INST_0_i_369_n_0\
    );
\disp[5][0]_INST_0_i_37\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_out(6),
      I1 => \disp[5][0]_INST_0_i_91_n_0\,
      O => \disp[5][0]_INST_0_i_37_n_0\
    );
\disp[5][0]_INST_0_i_370\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"335ACC5A"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(5),
      I1 => \disp[5][0]_INST_0_i_11_n_6\,
      I2 => \inst_int_to_string/euros3\(1),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_35_n_6\,
      O => \disp[5][0]_INST_0_i_370_n_0\
    );
\disp[5][0]_INST_0_i_371\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1DE2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(4),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \disp[5][0]_INST_0_i_11_n_7\,
      I3 => \inst_int_to_string/euros2\(0),
      O => \disp[5][0]_INST_0_i_371_n_0\
    );
\disp[5][0]_INST_0_i_372\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_35_n_4\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(3),
      O => \disp[5][0]_INST_0_i_372_n_0\
    );
\disp[5][0]_INST_0_i_373\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_11_n_7\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(4),
      O => \disp[5][0]_INST_0_i_373_n_0\
    );
\disp[5][0]_INST_0_i_374\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_35_n_4\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(3),
      O => \disp[5][0]_INST_0_i_374_n_0\
    );
\disp[5][0]_INST_0_i_375\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_35_n_5\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(2),
      O => \disp[5][0]_INST_0_i_375_n_0\
    );
\disp[5][0]_INST_0_i_376\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_35_n_6\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(1),
      O => \disp[5][0]_INST_0_i_376_n_0\
    );
\disp[5][0]_INST_0_i_377\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCA533A5"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(4),
      I1 => \disp[5][0]_INST_0_i_11_n_7\,
      I2 => \inst_int_to_string/euros3\(7),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_11_n_4\,
      O => \disp[5][0]_INST_0_i_377_n_0\
    );
\disp[5][0]_INST_0_i_378\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCA533A5"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(3),
      I1 => \disp[5][0]_INST_0_i_35_n_4\,
      I2 => \inst_int_to_string/euros3\(6),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_11_n_5\,
      O => \disp[5][0]_INST_0_i_378_n_0\
    );
\disp[5][0]_INST_0_i_379\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCA533A5"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(2),
      I1 => \disp[5][0]_INST_0_i_35_n_5\,
      I2 => \inst_int_to_string/euros3\(5),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_11_n_6\,
      O => \disp[5][0]_INST_0_i_379_n_0\
    );
\disp[5][0]_INST_0_i_38\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_out(5),
      I1 => \disp[5][0]_INST_0_i_92_n_0\,
      O => \disp[5][0]_INST_0_i_38_n_0\
    );
\disp[5][0]_INST_0_i_380\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCA533A5"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(1),
      I1 => \disp[5][0]_INST_0_i_35_n_6\,
      I2 => \inst_int_to_string/euros3\(4),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_11_n_7\,
      O => \disp[5][0]_INST_0_i_380_n_0\
    );
\disp[5][0]_INST_0_i_381\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(17),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(19),
      I3 => \inst_int_to_string/euros3\(12),
      O => \disp[5][0]_INST_0_i_381_n_0\
    );
\disp[5][0]_INST_0_i_382\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(16),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(18),
      I3 => \inst_int_to_string/euros3\(11),
      O => \disp[5][0]_INST_0_i_382_n_0\
    );
\disp[5][0]_INST_0_i_383\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(15),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(17),
      I3 => \inst_int_to_string/euros3\(10),
      O => \disp[5][0]_INST_0_i_383_n_0\
    );
\disp[5][0]_INST_0_i_384\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(14),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(16),
      I3 => \inst_int_to_string/euros3\(9),
      O => \disp[5][0]_INST_0_i_384_n_0\
    );
\disp[5][0]_INST_0_i_385\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(18),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(20),
      I3 => \inst_int_to_string/euros3\(13),
      I4 => \disp[5][0]_INST_0_i_381_n_0\,
      O => \disp[5][0]_INST_0_i_385_n_0\
    );
\disp[5][0]_INST_0_i_386\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(17),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(19),
      I3 => \inst_int_to_string/euros3\(12),
      I4 => \disp[5][0]_INST_0_i_382_n_0\,
      O => \disp[5][0]_INST_0_i_386_n_0\
    );
\disp[5][0]_INST_0_i_387\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(16),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(18),
      I3 => \inst_int_to_string/euros3\(11),
      I4 => \disp[5][0]_INST_0_i_383_n_0\,
      O => \disp[5][0]_INST_0_i_387_n_0\
    );
\disp[5][0]_INST_0_i_388\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(15),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(17),
      I3 => \inst_int_to_string/euros3\(10),
      I4 => \disp[5][0]_INST_0_i_384_n_0\,
      O => \disp[5][0]_INST_0_i_388_n_0\
    );
\disp[5][0]_INST_0_i_389\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F2FB"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(6),
      I1 => \inst_int_to_string/euros3\(8),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(10),
      O => \disp[5][0]_INST_0_i_389_n_0\
    );
\disp[5][0]_INST_0_i_39\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AA55A956"
    )
        port map (
      I0 => total_out(4),
      I1 => \disp[2][0]_INST_0_i_15_n_4\,
      I2 => \disp[2][0]_INST_0_i_15_n_5\,
      I3 => \disp[5][0]_INST_0_i_93_n_7\,
      I4 => \disp[2][0]_INST_0_i_16_n_0\,
      O => \disp[5][0]_INST_0_i_39_n_0\
    );
\disp[5][0]_INST_0_i_390\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFFF4777CCCF4447"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_11_n_4\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(9),
      I3 => \inst_int_to_string/euros3\(7),
      I4 => \disp[5][0]_INST_0_i_11_n_6\,
      I5 => \inst_int_to_string/euros3\(5),
      O => \disp[5][0]_INST_0_i_390_n_0\
    );
\disp[5][0]_INST_0_i_391\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFFF4777CCCF4447"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_11_n_5\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(8),
      I3 => \inst_int_to_string/euros3\(6),
      I4 => \disp[5][0]_INST_0_i_11_n_7\,
      I5 => \inst_int_to_string/euros3\(4),
      O => \disp[5][0]_INST_0_i_391_n_0\
    );
\disp[5][0]_INST_0_i_392\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"57F7075751F10151"
    )
        port map (
      I0 => \inst_int_to_string/euros2\(5),
      I1 => \inst_int_to_string/euros3\(7),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \disp[5][0]_INST_0_i_11_n_4\,
      I4 => \disp[5][0]_INST_0_i_35_n_4\,
      I5 => \inst_int_to_string/euros3\(3),
      O => \disp[5][0]_INST_0_i_392_n_0\
    );
\disp[5][0]_INST_0_i_393\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5659A6A95956A9A6"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_389_n_0\,
      I1 => \inst_int_to_string/euros3\(11),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(7),
      I4 => \disp[5][0]_INST_0_i_11_n_4\,
      I5 => \inst_int_to_string/euros3\(9),
      O => \disp[5][0]_INST_0_i_393_n_0\
    );
\disp[5][0]_INST_0_i_394\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5A665A995A995A66"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_390_n_0\,
      I1 => \inst_int_to_string/euros3\(6),
      I2 => \disp[5][0]_INST_0_i_11_n_5\,
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \inst_int_to_string/euros3\(8),
      I5 => \inst_int_to_string/euros3\(10),
      O => \disp[5][0]_INST_0_i_394_n_0\
    );
\disp[5][0]_INST_0_i_395\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9969666999966696"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_391_n_0\,
      I1 => \inst_int_to_string/euros2\(5),
      I2 => \inst_int_to_string/euros3\(7),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_11_n_4\,
      I5 => \inst_int_to_string/euros3\(9),
      O => \disp[5][0]_INST_0_i_395_n_0\
    );
\disp[5][0]_INST_0_i_396\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A65659A9A95956A6"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_392_n_0\,
      I1 => \inst_int_to_string/euros3\(6),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \disp[5][0]_INST_0_i_11_n_5\,
      I4 => \inst_int_to_string/euros2\(4),
      I5 => \inst_int_to_string/euros3\(8),
      O => \disp[5][0]_INST_0_i_396_n_0\
    );
\disp[5][0]_INST_0_i_397\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_11_n_4\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(7),
      O => \disp[5][0]_INST_0_i_397_n_0\
    );
\disp[5][0]_INST_0_i_398\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_11_n_7\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(4),
      O => \inst_int_to_string/euros2\(4)
    );
\disp[5][0]_INST_0_i_399\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_35_n_4\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(3),
      O => \inst_int_to_string/euros2\(3)
    );
\disp[5][0]_INST_0_i_4\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_11_n_0\,
      CO(3 downto 1) => \NLW_disp[5][0]_INST_0_i_4_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \disp[5][0]_INST_0_i_4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_disp[5][0]_INST_0_i_4_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
\disp[5][0]_INST_0_i_40\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_94_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_40_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_40_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_40_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_40_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_95_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_96_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_97_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_98_n_0\,
      O(3) => \disp[5][0]_INST_0_i_40_n_4\,
      O(2) => \disp[5][0]_INST_0_i_40_n_5\,
      O(1) => \disp[5][0]_INST_0_i_40_n_6\,
      O(0) => \disp[5][0]_INST_0_i_40_n_7\,
      S(3) => \disp[5][0]_INST_0_i_99_n_0\,
      S(2) => \disp[5][0]_INST_0_i_100_n_0\,
      S(1) => \disp[5][0]_INST_0_i_101_n_0\,
      S(0) => \disp[5][0]_INST_0_i_102_n_0\
    );
\disp[5][0]_INST_0_i_400\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_35_n_6\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(1),
      O => \inst_int_to_string/euros2\(1)
    );
\disp[5][0]_INST_0_i_401\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_11_n_4\,
      O => \disp[5][0]_INST_0_i_401_n_0\
    );
\disp[5][0]_INST_0_i_402\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_11_n_5\,
      O => \disp[5][0]_INST_0_i_402_n_0\
    );
\disp[5][0]_INST_0_i_403\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_11_n_6\,
      O => \disp[5][0]_INST_0_i_403_n_0\
    );
\disp[5][0]_INST_0_i_404\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \inst_int_to_string/euros2\(0),
      O => \disp[5][0]_INST_0_i_404_n_0\
    );
\disp[5][0]_INST_0_i_405\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_11_n_7\,
      O => \disp[5][0]_INST_0_i_405_n_0\
    );
\disp[5][0]_INST_0_i_406\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_35_n_4\,
      O => \disp[5][0]_INST_0_i_406_n_0\
    );
\disp[5][0]_INST_0_i_407\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_35_n_5\,
      O => \disp[5][0]_INST_0_i_407_n_0\
    );
\disp[5][0]_INST_0_i_408\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_35_n_6\,
      O => \disp[5][0]_INST_0_i_408_n_0\
    );
\disp[5][0]_INST_0_i_409\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_499_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_409_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_409_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_409_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_409_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_500_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_501_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_502_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_503_n_0\,
      O(3 downto 0) => \NLW_disp[5][0]_INST_0_i_409_O_UNCONNECTED\(3 downto 0),
      S(3) => \disp[5][0]_INST_0_i_504_n_0\,
      S(2) => \disp[5][0]_INST_0_i_505_n_0\,
      S(1) => \disp[5][0]_INST_0_i_506_n_0\,
      S(0) => \disp[5][0]_INST_0_i_507_n_0\
    );
\disp[5][0]_INST_0_i_41\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_49_n_7\,
      I1 => \disp[5][0]_INST_0_i_49_n_5\,
      O => \disp[5][0]_INST_0_i_41_n_0\
    );
\disp[5][0]_INST_0_i_410\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_308_n_4\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(12),
      O => \disp[5][0]_INST_0_i_410_n_0\
    );
\disp[5][0]_INST_0_i_411\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_308_n_5\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(11),
      O => \disp[5][0]_INST_0_i_411_n_0\
    );
\disp[5][0]_INST_0_i_412\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_308_n_6\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(10),
      O => \disp[5][0]_INST_0_i_412_n_0\
    );
\disp[5][0]_INST_0_i_413\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_308_n_7\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(9),
      O => \disp[5][0]_INST_0_i_413_n_0\
    );
\disp[5][0]_INST_0_i_414\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(12),
      I1 => \disp[5][0]_INST_0_i_308_n_4\,
      I2 => \inst_int_to_string/euros3\(13),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_219_n_7\,
      O => \disp[5][0]_INST_0_i_414_n_0\
    );
\disp[5][0]_INST_0_i_415\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(11),
      I1 => \disp[5][0]_INST_0_i_308_n_5\,
      I2 => \inst_int_to_string/euros3\(12),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_308_n_4\,
      O => \disp[5][0]_INST_0_i_415_n_0\
    );
\disp[5][0]_INST_0_i_416\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(10),
      I1 => \disp[5][0]_INST_0_i_308_n_6\,
      I2 => \inst_int_to_string/euros3\(11),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_308_n_5\,
      O => \disp[5][0]_INST_0_i_416_n_0\
    );
\disp[5][0]_INST_0_i_417\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(9),
      I1 => \disp[5][0]_INST_0_i_308_n_7\,
      I2 => \inst_int_to_string/euros3\(10),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_308_n_6\,
      O => \disp[5][0]_INST_0_i_417_n_0\
    );
\disp[5][0]_INST_0_i_418\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_57_n_4\,
      I1 => \disp[2][0]_INST_0_i_57_n_7\,
      O => \disp[5][0]_INST_0_i_418_n_0\
    );
\disp[5][0]_INST_0_i_419\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66969969"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_508_n_6\,
      I1 => \disp[2][0]_INST_0_i_57_n_5\,
      I2 => \disp[5][0]_INST_0_i_508_n_7\,
      I3 => \disp[2][0]_INST_0_i_57_n_6\,
      I4 => \disp[2][0]_INST_0_i_57_n_7\,
      O => \disp[5][0]_INST_0_i_419_n_0\
    );
\disp[5][0]_INST_0_i_42\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_103_n_4\,
      I1 => \disp[5][0]_INST_0_i_49_n_6\,
      I2 => \disp[5][0]_INST_0_i_50_n_7\,
      O => \disp[5][0]_INST_0_i_42_n_0\
    );
\disp[5][0]_INST_0_i_420\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_57_n_7\,
      I1 => \disp[2][0]_INST_0_i_57_n_4\,
      I2 => \disp[5][0]_INST_0_i_508_n_7\,
      I3 => \disp[2][0]_INST_0_i_57_n_6\,
      O => \disp[5][0]_INST_0_i_420_n_0\
    );
\disp[5][0]_INST_0_i_421\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_57_n_4\,
      I1 => \disp[2][0]_INST_0_i_57_n_7\,
      O => \disp[5][0]_INST_0_i_421_n_0\
    );
\disp[5][0]_INST_0_i_422\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \disp[5][0]_INST_0_i_422_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_422_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_422_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_422_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_509_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_510_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_511_n_0\,
      DI(0) => '0',
      O(3) => \disp[5][0]_INST_0_i_422_n_4\,
      O(2) => \disp[5][0]_INST_0_i_422_n_5\,
      O(1) => \disp[5][0]_INST_0_i_422_n_6\,
      O(0) => \disp[5][0]_INST_0_i_422_n_7\,
      S(3) => \disp[5][0]_INST_0_i_512_n_0\,
      S(2) => \disp[5][0]_INST_0_i_513_n_0\,
      S(1) => \disp[5][0]_INST_0_i_514_n_0\,
      S(0) => \disp[5][0]_INST_0_i_515_n_0\
    );
\disp[5][0]_INST_0_i_423\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_431_n_7\,
      I1 => \disp[5][0]_INST_0_i_431_n_5\,
      I2 => \disp[5][0]_INST_0_i_317_n_6\,
      O => \disp[5][0]_INST_0_i_423_n_0\
    );
\disp[5][0]_INST_0_i_424\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_6_n_4\,
      I1 => \disp[5][0]_INST_0_i_431_n_6\,
      I2 => \disp[5][0]_INST_0_i_317_n_7\,
      O => \disp[5][0]_INST_0_i_424_n_0\
    );
\disp[5][0]_INST_0_i_425\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_6_n_5\,
      I1 => \disp[5][0]_INST_0_i_431_n_7\,
      I2 => \disp[5][0]_INST_0_i_431_n_4\,
      O => \disp[5][0]_INST_0_i_425_n_0\
    );
\disp[5][0]_INST_0_i_426\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_6_n_6\,
      I1 => \disp[5][0]_INST_0_i_6_n_4\,
      I2 => \disp[5][0]_INST_0_i_431_n_5\,
      O => \disp[5][0]_INST_0_i_426_n_0\
    );
\disp[5][0]_INST_0_i_427\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_431_n_6\,
      I1 => \disp[5][0]_INST_0_i_431_n_4\,
      I2 => \disp[5][0]_INST_0_i_317_n_5\,
      I3 => \disp[5][0]_INST_0_i_423_n_0\,
      O => \disp[5][0]_INST_0_i_427_n_0\
    );
\disp[5][0]_INST_0_i_428\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_431_n_7\,
      I1 => \disp[5][0]_INST_0_i_431_n_5\,
      I2 => \disp[5][0]_INST_0_i_317_n_6\,
      I3 => \disp[5][0]_INST_0_i_424_n_0\,
      O => \disp[5][0]_INST_0_i_428_n_0\
    );
\disp[5][0]_INST_0_i_429\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_6_n_4\,
      I1 => \disp[5][0]_INST_0_i_431_n_6\,
      I2 => \disp[5][0]_INST_0_i_317_n_7\,
      I3 => \disp[5][0]_INST_0_i_425_n_0\,
      O => \disp[5][0]_INST_0_i_429_n_0\
    );
\disp[5][0]_INST_0_i_43\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_103_n_5\,
      I1 => \disp[5][0]_INST_0_i_49_n_7\,
      I2 => \disp[5][0]_INST_0_i_49_n_4\,
      O => \disp[5][0]_INST_0_i_43_n_0\
    );
\disp[5][0]_INST_0_i_430\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_6_n_5\,
      I1 => \disp[5][0]_INST_0_i_431_n_7\,
      I2 => \disp[5][0]_INST_0_i_431_n_4\,
      I3 => \disp[5][0]_INST_0_i_426_n_0\,
      O => \disp[5][0]_INST_0_i_430_n_0\
    );
\disp[5][0]_INST_0_i_431\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_6_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_431_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_431_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_431_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_431_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_516_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_517_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_518_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_519_n_0\,
      O(3) => \disp[5][0]_INST_0_i_431_n_4\,
      O(2) => \disp[5][0]_INST_0_i_431_n_5\,
      O(1) => \disp[5][0]_INST_0_i_431_n_6\,
      O(0) => \disp[5][0]_INST_0_i_431_n_7\,
      S(3) => \disp[5][0]_INST_0_i_520_n_0\,
      S(2) => \disp[5][0]_INST_0_i_521_n_0\,
      S(1) => \disp[5][0]_INST_0_i_522_n_0\,
      S(0) => \disp[5][0]_INST_0_i_523_n_0\
    );
\disp[5][0]_INST_0_i_432\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E80000E800E8E800"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_441_n_6\,
      I1 => \disp[5][0]_INST_0_i_339_n_5\,
      I2 => \disp[5][0]_INST_0_i_524_n_4\,
      I3 => \disp[5][0]_INST_0_i_339_n_4\,
      I4 => \disp[5][0]_INST_0_i_440_n_7\,
      I5 => \disp[5][0]_INST_0_i_441_n_5\,
      O => \disp[5][0]_INST_0_i_432_n_0\
    );
\disp[5][0]_INST_0_i_433\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_524_n_5\,
      I1 => \disp[5][0]_INST_0_i_339_n_6\,
      I2 => \disp[5][0]_INST_0_i_441_n_7\,
      I3 => \disp[5][0]_INST_0_i_525_n_2\,
      I4 => \disp[5][0]_INST_0_i_526_n_0\,
      O => \disp[5][0]_INST_0_i_433_n_0\
    );
\disp[5][0]_INST_0_i_434\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_524_n_6\,
      I1 => \disp[5][0]_INST_0_i_339_n_7\,
      I2 => \disp[5][0]_INST_0_i_527_n_4\,
      I3 => \disp[5][0]_INST_0_i_525_n_7\,
      I4 => \disp[5][0]_INST_0_i_528_n_0\,
      O => \disp[5][0]_INST_0_i_434_n_0\
    );
\disp[5][0]_INST_0_i_435\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_524_n_7\,
      I1 => \disp[5][0]_INST_0_i_72_n_4\,
      I2 => \disp[5][0]_INST_0_i_527_n_5\,
      I3 => \disp[5][0]_INST_0_i_529_n_4\,
      I4 => \disp[5][0]_INST_0_i_530_n_0\,
      O => \disp[5][0]_INST_0_i_435_n_0\
    );
\disp[5][0]_INST_0_i_436\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66696999"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_432_n_0\,
      I1 => \disp[5][0]_INST_0_i_531_n_0\,
      I2 => \disp[5][0]_INST_0_i_440_n_7\,
      I3 => \disp[5][0]_INST_0_i_339_n_4\,
      I4 => \disp[5][0]_INST_0_i_441_n_5\,
      O => \disp[5][0]_INST_0_i_436_n_0\
    );
\disp[5][0]_INST_0_i_437\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"66696999"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_433_n_0\,
      I1 => \disp[5][0]_INST_0_i_532_n_0\,
      I2 => \disp[5][0]_INST_0_i_524_n_4\,
      I3 => \disp[5][0]_INST_0_i_339_n_5\,
      I4 => \disp[5][0]_INST_0_i_441_n_6\,
      O => \disp[5][0]_INST_0_i_437_n_0\
    );
\disp[5][0]_INST_0_i_438\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_434_n_0\,
      I1 => \disp[5][0]_INST_0_i_533_n_0\,
      I2 => \disp[5][0]_INST_0_i_441_n_6\,
      I3 => \disp[5][0]_INST_0_i_339_n_5\,
      I4 => \disp[5][0]_INST_0_i_524_n_4\,
      I5 => \disp[5][0]_INST_0_i_525_n_2\,
      O => \disp[5][0]_INST_0_i_438_n_0\
    );
\disp[5][0]_INST_0_i_439\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_435_n_0\,
      I1 => \disp[5][0]_INST_0_i_534_n_0\,
      I2 => \disp[5][0]_INST_0_i_441_n_7\,
      I3 => \disp[5][0]_INST_0_i_339_n_6\,
      I4 => \disp[5][0]_INST_0_i_524_n_5\,
      I5 => \disp[5][0]_INST_0_i_525_n_7\,
      O => \disp[5][0]_INST_0_i_439_n_0\
    );
\disp[5][0]_INST_0_i_44\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_103_n_6\,
      I1 => \disp[5][0]_INST_0_i_103_n_4\,
      I2 => \disp[5][0]_INST_0_i_49_n_5\,
      O => \disp[5][0]_INST_0_i_44_n_0\
    );
\disp[5][0]_INST_0_i_440\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_524_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_440_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_440_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_440_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_440_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_535_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_536_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_537_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_538_n_0\,
      O(3) => \disp[5][0]_INST_0_i_440_n_4\,
      O(2) => \disp[5][0]_INST_0_i_440_n_5\,
      O(1) => \disp[5][0]_INST_0_i_440_n_6\,
      O(0) => \disp[5][0]_INST_0_i_440_n_7\,
      S(3) => \disp[5][0]_INST_0_i_539_n_0\,
      S(2) => \disp[5][0]_INST_0_i_540_n_0\,
      S(1) => \disp[5][0]_INST_0_i_541_n_0\,
      S(0) => \disp[5][0]_INST_0_i_542_n_0\
    );
\disp[5][0]_INST_0_i_441\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_527_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_441_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_441_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_441_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_441_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \inst_int_to_string/euros2\(24 downto 21),
      O(3) => \disp[5][0]_INST_0_i_441_n_4\,
      O(2) => \disp[5][0]_INST_0_i_441_n_5\,
      O(1) => \disp[5][0]_INST_0_i_441_n_6\,
      O(0) => \disp[5][0]_INST_0_i_441_n_7\,
      S(3) => \disp[5][0]_INST_0_i_547_n_0\,
      S(2) => \disp[5][0]_INST_0_i_548_n_0\,
      S(1) => \disp[5][0]_INST_0_i_549_n_0\,
      S(0) => \disp[5][0]_INST_0_i_550_n_0\
    );
\disp[5][0]_INST_0_i_442\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_326_n_7\,
      I1 => \disp[5][0]_INST_0_i_239_n_0\,
      I2 => \disp[5][0]_INST_0_i_327_n_5\,
      O => \disp[5][0]_INST_0_i_442_n_0\
    );
\disp[5][0]_INST_0_i_443\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_327_n_6\,
      I1 => \disp[5][0]_INST_0_i_440_n_4\,
      I2 => \disp[5][0]_INST_0_i_239_n_5\,
      O => \disp[5][0]_INST_0_i_443_n_0\
    );
\disp[5][0]_INST_0_i_444\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_327_n_7\,
      I1 => \disp[5][0]_INST_0_i_440_n_5\,
      I2 => \disp[5][0]_INST_0_i_239_n_6\,
      O => \disp[5][0]_INST_0_i_444_n_0\
    );
\disp[5][0]_INST_0_i_445\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(29),
      I1 => \inst_int_to_string/euros3\(25),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(23),
      O => \disp[5][0]_INST_0_i_445_n_0\
    );
\disp[5][0]_INST_0_i_446\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(28),
      I1 => \inst_int_to_string/euros3\(24),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(22),
      O => \disp[5][0]_INST_0_i_446_n_0\
    );
\disp[5][0]_INST_0_i_447\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(27),
      I1 => \inst_int_to_string/euros3\(23),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(21),
      O => \disp[5][0]_INST_0_i_447_n_0\
    );
\disp[5][0]_INST_0_i_448\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(26),
      I1 => \inst_int_to_string/euros3\(22),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(20),
      O => \disp[5][0]_INST_0_i_448_n_0\
    );
\disp[5][0]_INST_0_i_449\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A6A9A9A6"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_445_n_0\,
      I1 => \inst_int_to_string/euros3\(30),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(24),
      I4 => \inst_int_to_string/euros3\(26),
      O => \disp[5][0]_INST_0_i_449_n_0\
    );
\disp[5][0]_INST_0_i_45\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B44B"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_49_n_5\,
      I1 => \disp[5][0]_INST_0_i_49_n_7\,
      I2 => \disp[5][0]_INST_0_i_49_n_4\,
      I3 => \disp[5][0]_INST_0_i_49_n_6\,
      O => \disp[5][0]_INST_0_i_45_n_0\
    );
\disp[5][0]_INST_0_i_450\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F6F90906"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(29),
      I1 => \inst_int_to_string/euros3\(25),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(23),
      I4 => \disp[5][0]_INST_0_i_446_n_0\,
      O => \disp[5][0]_INST_0_i_450_n_0\
    );
\disp[5][0]_INST_0_i_451\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F6F90906"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(28),
      I1 => \inst_int_to_string/euros3\(24),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(22),
      I4 => \disp[5][0]_INST_0_i_447_n_0\,
      O => \disp[5][0]_INST_0_i_451_n_0\
    );
\disp[5][0]_INST_0_i_452\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F6F90906"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(27),
      I1 => \inst_int_to_string/euros3\(23),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(21),
      I4 => \disp[5][0]_INST_0_i_448_n_0\,
      O => \disp[5][0]_INST_0_i_452_n_0\
    );
\disp[5][0]_INST_0_i_453\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(28),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      O => \inst_int_to_string/euros2\(28)
    );
\disp[5][0]_INST_0_i_454\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(27),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      O => \inst_int_to_string/euros2\(27)
    );
\disp[5][0]_INST_0_i_455\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(26),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      O => \inst_int_to_string/euros2\(26)
    );
\disp[5][0]_INST_0_i_456\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(25),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      O => \inst_int_to_string/euros2\(25)
    );
\disp[5][0]_INST_0_i_457\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_4_n_3\,
      I1 => \inst_int_to_string/euros3\(28),
      O => \disp[5][0]_INST_0_i_457_n_0\
    );
\disp[5][0]_INST_0_i_458\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(27),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(30),
      O => \disp[5][0]_INST_0_i_458_n_0\
    );
\disp[5][0]_INST_0_i_459\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(26),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(29),
      O => \disp[5][0]_INST_0_i_459_n_0\
    );
\disp[5][0]_INST_0_i_46\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4DB2B24D"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_50_n_7\,
      I1 => \disp[5][0]_INST_0_i_49_n_6\,
      I2 => \disp[5][0]_INST_0_i_103_n_4\,
      I3 => \disp[5][0]_INST_0_i_49_n_5\,
      I4 => \disp[5][0]_INST_0_i_49_n_7\,
      O => \disp[5][0]_INST_0_i_46_n_0\
    );
\disp[5][0]_INST_0_i_460\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(25),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(28),
      O => \disp[5][0]_INST_0_i_460_n_0\
    );
\disp[5][0]_INST_0_i_461\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFCD"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(30),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(28),
      I3 => \inst_int_to_string/euros3\(26),
      O => \disp[5][0]_INST_0_i_461_n_0\
    );
\disp[5][0]_INST_0_i_462\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFCD"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(29),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(27),
      I3 => \inst_int_to_string/euros3\(25),
      O => \disp[5][0]_INST_0_i_462_n_0\
    );
\disp[5][0]_INST_0_i_463\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(28),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(24),
      I3 => \inst_int_to_string/euros3\(26),
      O => \disp[5][0]_INST_0_i_463_n_0\
    );
\disp[5][0]_INST_0_i_464\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DCFD"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(27),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(23),
      I3 => \inst_int_to_string/euros3\(25),
      O => \disp[5][0]_INST_0_i_464_n_0\
    );
\disp[5][0]_INST_0_i_465\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF2BD4FFFFD42B"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(26),
      I1 => \inst_int_to_string/euros3\(28),
      I2 => \inst_int_to_string/euros3\(30),
      I3 => \inst_int_to_string/euros3\(29),
      I4 => \disp[5][0]_INST_0_i_4_n_3\,
      I5 => \inst_int_to_string/euros3\(27),
      O => \disp[5][0]_INST_0_i_465_n_0\
    );
\disp[5][0]_INST_0_i_466\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A6A9A9A6"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_462_n_0\,
      I1 => \inst_int_to_string/euros3\(30),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(26),
      I4 => \inst_int_to_string/euros3\(28),
      O => \disp[5][0]_INST_0_i_466_n_0\
    );
\disp[5][0]_INST_0_i_467\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(29),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(27),
      I3 => \inst_int_to_string/euros3\(25),
      I4 => \disp[5][0]_INST_0_i_463_n_0\,
      O => \disp[5][0]_INST_0_i_467_n_0\
    );
\disp[5][0]_INST_0_i_468\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(28),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(24),
      I3 => \inst_int_to_string/euros3\(26),
      I4 => \disp[5][0]_INST_0_i_464_n_0\,
      O => \disp[5][0]_INST_0_i_468_n_0\
    );
\disp[5][0]_INST_0_i_469\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_477_n_5\,
      I1 => \disp[5][0]_INST_0_i_490_n_4\,
      O => \disp[5][0]_INST_0_i_469_n_0\
    );
\disp[5][0]_INST_0_i_47\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_43_n_0\,
      I1 => \disp[5][0]_INST_0_i_103_n_4\,
      I2 => \disp[5][0]_INST_0_i_49_n_6\,
      I3 => \disp[5][0]_INST_0_i_50_n_7\,
      O => \disp[5][0]_INST_0_i_47_n_0\
    );
\disp[5][0]_INST_0_i_470\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_477_n_6\,
      I1 => \disp[5][0]_INST_0_i_490_n_5\,
      O => \disp[5][0]_INST_0_i_470_n_0\
    );
\disp[5][0]_INST_0_i_471\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_477_n_7\,
      I1 => \disp[5][0]_INST_0_i_490_n_6\,
      O => \disp[5][0]_INST_0_i_471_n_0\
    );
\disp[5][0]_INST_0_i_472\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_490_n_7\,
      I1 => \disp[5][0]_INST_0_i_551_n_4\,
      O => \disp[5][0]_INST_0_i_472_n_0\
    );
\disp[5][0]_INST_0_i_473\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9666"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_477_n_4\,
      I1 => \disp[5][0]_INST_0_i_365_n_7\,
      I2 => \disp[5][0]_INST_0_i_490_n_4\,
      I3 => \disp[5][0]_INST_0_i_477_n_5\,
      O => \disp[5][0]_INST_0_i_473_n_0\
    );
\disp[5][0]_INST_0_i_474\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_490_n_5\,
      I1 => \disp[5][0]_INST_0_i_477_n_6\,
      I2 => \disp[5][0]_INST_0_i_490_n_4\,
      I3 => \disp[5][0]_INST_0_i_477_n_5\,
      O => \disp[5][0]_INST_0_i_474_n_0\
    );
\disp[5][0]_INST_0_i_475\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_490_n_6\,
      I1 => \disp[5][0]_INST_0_i_477_n_7\,
      I2 => \disp[5][0]_INST_0_i_490_n_5\,
      I3 => \disp[5][0]_INST_0_i_477_n_6\,
      O => \disp[5][0]_INST_0_i_475_n_0\
    );
\disp[5][0]_INST_0_i_476\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8778"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_551_n_4\,
      I1 => \disp[5][0]_INST_0_i_490_n_7\,
      I2 => \disp[5][0]_INST_0_i_490_n_6\,
      I3 => \disp[5][0]_INST_0_i_477_n_7\,
      O => \disp[5][0]_INST_0_i_476_n_0\
    );
\disp[5][0]_INST_0_i_477\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_551_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_477_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_477_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_477_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_477_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_552_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_553_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_554_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_555_n_0\,
      O(3) => \disp[5][0]_INST_0_i_477_n_4\,
      O(2) => \disp[5][0]_INST_0_i_477_n_5\,
      O(1) => \disp[5][0]_INST_0_i_477_n_6\,
      O(0) => \disp[5][0]_INST_0_i_477_n_7\,
      S(3) => \disp[5][0]_INST_0_i_556_n_0\,
      S(2) => \disp[5][0]_INST_0_i_557_n_0\,
      S(1) => \disp[5][0]_INST_0_i_558_n_0\,
      S(0) => \disp[5][0]_INST_0_i_559_n_0\
    );
\disp[5][0]_INST_0_i_478\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_257_n_7\,
      I1 => \inst_int_to_string/euros2\(0),
      I2 => \disp[5][0]_INST_0_i_362_n_4\,
      O => \disp[5][0]_INST_0_i_478_n_0\
    );
\disp[5][0]_INST_0_i_479\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A959"
    )
        port map (
      I0 => \inst_int_to_string/euros2\(0),
      I1 => \inst_int_to_string/euros3\(3),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \disp[5][0]_INST_0_i_35_n_4\,
      O => \disp[5][0]_INST_0_i_479_n_0\
    );
\disp[5][0]_INST_0_i_48\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_103_n_5\,
      I1 => \disp[5][0]_INST_0_i_49_n_7\,
      I2 => \disp[5][0]_INST_0_i_49_n_4\,
      I3 => \disp[5][0]_INST_0_i_44_n_0\,
      O => \disp[5][0]_INST_0_i_48_n_0\
    );
\disp[5][0]_INST_0_i_480\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1D"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(2),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \disp[5][0]_INST_0_i_35_n_5\,
      O => \disp[5][0]_INST_0_i_480_n_0\
    );
\disp[5][0]_INST_0_i_481\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1D"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(1),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \disp[5][0]_INST_0_i_35_n_6\,
      O => \disp[5][0]_INST_0_i_481_n_0\
    );
\disp[5][0]_INST_0_i_482\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(13),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(15),
      I3 => \inst_int_to_string/euros3\(8),
      O => \disp[5][0]_INST_0_i_482_n_0\
    );
\disp[5][0]_INST_0_i_483\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(14),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(12),
      I3 => \inst_int_to_string/euros3\(7),
      O => \disp[5][0]_INST_0_i_483_n_0\
    );
\disp[5][0]_INST_0_i_484\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(13),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(11),
      I3 => \inst_int_to_string/euros3\(6),
      O => \disp[5][0]_INST_0_i_484_n_0\
    );
\disp[5][0]_INST_0_i_485\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"5440"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_4_n_3\,
      I1 => \inst_int_to_string/euros3\(5),
      I2 => \inst_int_to_string/euros3\(12),
      I3 => \inst_int_to_string/euros3\(10),
      O => \disp[5][0]_INST_0_i_485_n_0\
    );
\disp[5][0]_INST_0_i_486\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(14),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(16),
      I3 => \inst_int_to_string/euros3\(9),
      I4 => \disp[5][0]_INST_0_i_482_n_0\,
      O => \disp[5][0]_INST_0_i_486_n_0\
    );
\disp[5][0]_INST_0_i_487\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DEED2112"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(13),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(15),
      I3 => \inst_int_to_string/euros3\(8),
      I4 => \disp[5][0]_INST_0_i_483_n_0\,
      O => \disp[5][0]_INST_0_i_487_n_0\
    );
\disp[5][0]_INST_0_i_488\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5659A6A95956A9A6"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_484_n_0\,
      I1 => \inst_int_to_string/euros3\(12),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(14),
      I4 => \disp[5][0]_INST_0_i_11_n_4\,
      I5 => \inst_int_to_string/euros3\(7),
      O => \disp[5][0]_INST_0_i_488_n_0\
    );
\disp[5][0]_INST_0_i_489\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5659A6A95956A9A6"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_485_n_0\,
      I1 => \inst_int_to_string/euros3\(11),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(13),
      I4 => \disp[5][0]_INST_0_i_11_n_5\,
      I5 => \inst_int_to_string/euros3\(6),
      O => \disp[5][0]_INST_0_i_489_n_0\
    );
\disp[5][0]_INST_0_i_49\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_103_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_49_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_49_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_49_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_49_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_104_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_105_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_106_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_107_n_0\,
      O(3) => \disp[5][0]_INST_0_i_49_n_4\,
      O(2) => \disp[5][0]_INST_0_i_49_n_5\,
      O(1) => \disp[5][0]_INST_0_i_49_n_6\,
      O(0) => \disp[5][0]_INST_0_i_49_n_7\,
      S(3) => \disp[5][0]_INST_0_i_108_n_0\,
      S(2) => \disp[5][0]_INST_0_i_109_n_0\,
      S(1) => \disp[5][0]_INST_0_i_110_n_0\,
      S(0) => \disp[5][0]_INST_0_i_111_n_0\
    );
\disp[5][0]_INST_0_i_490\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \disp[5][0]_INST_0_i_490_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_490_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_490_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_490_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_404_n_0\,
      DI(2) => \inst_int_to_string/euros2\(0),
      DI(1 downto 0) => B"01",
      O(3) => \disp[5][0]_INST_0_i_490_n_4\,
      O(2) => \disp[5][0]_INST_0_i_490_n_5\,
      O(1) => \disp[5][0]_INST_0_i_490_n_6\,
      O(0) => \disp[5][0]_INST_0_i_490_n_7\,
      S(3) => \disp[5][0]_INST_0_i_560_n_0\,
      S(2) => \disp[5][0]_INST_0_i_561_n_0\,
      S(1) => \disp[5][0]_INST_0_i_562_n_0\,
      S(0) => \inst_int_to_string/euros2\(0)
    );
\disp[5][0]_INST_0_i_491\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"335FFF5F00053305"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(4),
      I1 => \disp[5][0]_INST_0_i_11_n_7\,
      I2 => \inst_int_to_string/euros3\(6),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_11_n_5\,
      I5 => \inst_int_to_string/euros2\(2),
      O => \disp[5][0]_INST_0_i_491_n_0\
    );
\disp[5][0]_INST_0_i_492\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"57F7075751F10151"
    )
        port map (
      I0 => \inst_int_to_string/euros2\(5),
      I1 => \inst_int_to_string/euros3\(3),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \disp[5][0]_INST_0_i_35_n_4\,
      I4 => \disp[5][0]_INST_0_i_35_n_6\,
      I5 => \inst_int_to_string/euros3\(1),
      O => \disp[5][0]_INST_0_i_492_n_0\
    );
\disp[5][0]_INST_0_i_493\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"47007703CF44FF47"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_11_n_7\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(4),
      I3 => \inst_int_to_string/euros2\(0),
      I4 => \inst_int_to_string/euros3\(2),
      I5 => \disp[5][0]_INST_0_i_35_n_5\,
      O => \disp[5][0]_INST_0_i_493_n_0\
    );
\disp[5][0]_INST_0_i_494\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00053305"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(1),
      I1 => \disp[5][0]_INST_0_i_35_n_6\,
      I2 => \inst_int_to_string/euros3\(3),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_35_n_4\,
      O => \disp[5][0]_INST_0_i_494_n_0\
    );
\disp[5][0]_INST_0_i_495\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9996669666699969"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_491_n_0\,
      I1 => \inst_int_to_string/euros2\(5),
      I2 => \inst_int_to_string/euros3\(3),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_35_n_4\,
      I5 => \disp[5][0]_INST_0_i_397_n_0\,
      O => \disp[5][0]_INST_0_i_495_n_0\
    );
\disp[5][0]_INST_0_i_496\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"56A6A959A95956A6"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_492_n_0\,
      I1 => \inst_int_to_string/euros3\(6),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \disp[5][0]_INST_0_i_11_n_5\,
      I4 => \inst_int_to_string/euros2\(4),
      I5 => \inst_int_to_string/euros2\(2),
      O => \disp[5][0]_INST_0_i_496_n_0\
    );
\disp[5][0]_INST_0_i_497\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6996696969969696"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_493_n_0\,
      I1 => \inst_int_to_string/euros2\(5),
      I2 => \inst_int_to_string/euros2\(3),
      I3 => \disp[5][0]_INST_0_i_35_n_6\,
      I4 => \disp[5][0]_INST_0_i_4_n_3\,
      I5 => \inst_int_to_string/euros3\(1),
      O => \disp[5][0]_INST_0_i_497_n_0\
    );
\disp[5][0]_INST_0_i_498\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6966699996999666"
    )
        port map (
      I0 => \inst_int_to_string/euros2\(0),
      I1 => \inst_int_to_string/euros2\(2),
      I2 => \disp[5][0]_INST_0_i_11_n_7\,
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \inst_int_to_string/euros3\(4),
      I5 => \disp[5][0]_INST_0_i_494_n_0\,
      O => \disp[5][0]_INST_0_i_498_n_0\
    );
\disp[5][0]_INST_0_i_499\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \disp[5][0]_INST_0_i_499_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_499_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_499_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_499_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_563_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_564_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_565_n_0\,
      DI(0) => '0',
      O(3 downto 0) => \NLW_disp[5][0]_INST_0_i_499_O_UNCONNECTED\(3 downto 0),
      S(3) => \disp[5][0]_INST_0_i_566_n_0\,
      S(2) => \disp[5][0]_INST_0_i_567_n_0\,
      S(1) => \disp[5][0]_INST_0_i_568_n_0\,
      S(0) => \disp[5][0]_INST_0_i_569_n_0\
    );
\disp[5][0]_INST_0_i_5\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_12_n_0\,
      CO(3 downto 1) => \NLW_disp[5][0]_INST_0_i_5_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \disp[5][0]_INST_0_i_5_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \disp[5][0]_INST_0_i_13_n_0\,
      O(3 downto 2) => \NLW_disp[5][0]_INST_0_i_5_O_UNCONNECTED\(3 downto 2),
      O(1) => \disp[5][0]_INST_0_i_5_n_6\,
      O(0) => \disp[5][0]_INST_0_i_5_n_7\,
      S(3 downto 2) => B"00",
      S(1) => \disp[5][0]_INST_0_i_14_n_0\,
      S(0) => \disp[5][0]_INST_0_i_15_n_0\
    );
\disp[5][0]_INST_0_i_50\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_49_n_0\,
      CO(3 downto 0) => \NLW_disp[5][0]_INST_0_i_50_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_disp[5][0]_INST_0_i_50_O_UNCONNECTED\(3 downto 1),
      O(0) => \disp[5][0]_INST_0_i_50_n_7\,
      S(3 downto 1) => B"000",
      S(0) => \disp[5][0]_INST_0_i_112_n_0\
    );
\disp[5][0]_INST_0_i_500\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_422_n_4\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(8),
      O => \disp[5][0]_INST_0_i_500_n_0\
    );
\disp[5][0]_INST_0_i_501\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"02A2"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_422_n_5\,
      I1 => \inst_int_to_string/euros3\(7),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \disp[5][0]_INST_0_i_11_n_4\,
      O => \disp[5][0]_INST_0_i_501_n_0\
    );
\disp[5][0]_INST_0_i_502\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"ABFB"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_422_n_6\,
      I1 => \inst_int_to_string/euros3\(6),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \disp[5][0]_INST_0_i_11_n_5\,
      O => \disp[5][0]_INST_0_i_502_n_0\
    );
\disp[5][0]_INST_0_i_503\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"ABFB"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_422_n_7\,
      I1 => \inst_int_to_string/euros3\(5),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \disp[5][0]_INST_0_i_11_n_6\,
      O => \disp[5][0]_INST_0_i_503_n_0\
    );
\disp[5][0]_INST_0_i_504\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(8),
      I1 => \disp[5][0]_INST_0_i_422_n_4\,
      I2 => \inst_int_to_string/euros3\(9),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_308_n_7\,
      O => \disp[5][0]_INST_0_i_504_n_0\
    );
\disp[5][0]_INST_0_i_505\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5050CF30AFAF30CF"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_11_n_4\,
      I1 => \inst_int_to_string/euros3\(7),
      I2 => \disp[5][0]_INST_0_i_422_n_5\,
      I3 => \inst_int_to_string/euros3\(8),
      I4 => \disp[5][0]_INST_0_i_4_n_3\,
      I5 => \disp[5][0]_INST_0_i_422_n_4\,
      O => \disp[5][0]_INST_0_i_505_n_0\
    );
\disp[5][0]_INST_0_i_506\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF4700B800B8FF47"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_11_n_5\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(6),
      I3 => \disp[5][0]_INST_0_i_422_n_6\,
      I4 => \disp[5][0]_INST_0_i_397_n_0\,
      I5 => \disp[5][0]_INST_0_i_422_n_5\,
      O => \disp[5][0]_INST_0_i_506_n_0\
    );
\disp[5][0]_INST_0_i_507\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"D2DDD2222D222DDD"
    )
        port map (
      I0 => \inst_int_to_string/euros2\(5),
      I1 => \disp[5][0]_INST_0_i_422_n_7\,
      I2 => \disp[5][0]_INST_0_i_11_n_5\,
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \inst_int_to_string/euros3\(6),
      I5 => \disp[5][0]_INST_0_i_422_n_6\,
      O => \disp[5][0]_INST_0_i_507_n_0\
    );
\disp[5][0]_INST_0_i_508\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_57_n_0\,
      CO(3 downto 1) => \NLW_disp[5][0]_INST_0_i_508_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \disp[5][0]_INST_0_i_508_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 2) => \NLW_disp[5][0]_INST_0_i_508_O_UNCONNECTED\(3 downto 2),
      O(1) => \disp[5][0]_INST_0_i_508_n_6\,
      O(0) => \disp[5][0]_INST_0_i_508_n_7\,
      S(3 downto 2) => B"00",
      S(1) => \disp[5][0]_INST_0_i_570_n_0\,
      S(0) => \disp[5][0]_INST_0_i_571_n_0\
    );
\disp[5][0]_INST_0_i_509\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_6_n_7\,
      I1 => \disp[5][0]_INST_0_i_6_n_5\,
      I2 => \disp[5][0]_INST_0_i_431_n_6\,
      O => \disp[5][0]_INST_0_i_509_n_0\
    );
\disp[5][0]_INST_0_i_51\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_113_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_51_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_51_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_51_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_51_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_114_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_115_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_116_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_117_n_0\,
      O(3 downto 0) => \NLW_disp[5][0]_INST_0_i_51_O_UNCONNECTED\(3 downto 0),
      S(3) => \disp[5][0]_INST_0_i_118_n_0\,
      S(2) => \disp[5][0]_INST_0_i_119_n_0\,
      S(1) => \disp[5][0]_INST_0_i_120_n_0\,
      S(0) => \disp[5][0]_INST_0_i_121_n_0\
    );
\disp[5][0]_INST_0_i_510\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_431_n_6\,
      I1 => \disp[5][0]_INST_0_i_6_n_5\,
      I2 => \disp[5][0]_INST_0_i_6_n_7\,
      O => \disp[5][0]_INST_0_i_510_n_0\
    );
\disp[5][0]_INST_0_i_511\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_6_n_4\,
      I1 => \disp[5][0]_INST_0_i_6_n_7\,
      O => \disp[5][0]_INST_0_i_511_n_0\
    );
\disp[5][0]_INST_0_i_512\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_6_n_6\,
      I1 => \disp[5][0]_INST_0_i_6_n_4\,
      I2 => \disp[5][0]_INST_0_i_431_n_5\,
      I3 => \disp[5][0]_INST_0_i_509_n_0\,
      O => \disp[5][0]_INST_0_i_512_n_0\
    );
\disp[5][0]_INST_0_i_513\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69966969"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_6_n_7\,
      I1 => \disp[5][0]_INST_0_i_6_n_5\,
      I2 => \disp[5][0]_INST_0_i_431_n_6\,
      I3 => \disp[5][0]_INST_0_i_6_n_6\,
      I4 => \disp[5][0]_INST_0_i_431_n_7\,
      O => \disp[5][0]_INST_0_i_513_n_0\
    );
\disp[5][0]_INST_0_i_514\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2DD2"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_6_n_7\,
      I1 => \disp[5][0]_INST_0_i_6_n_4\,
      I2 => \disp[5][0]_INST_0_i_431_n_7\,
      I3 => \disp[5][0]_INST_0_i_6_n_6\,
      O => \disp[5][0]_INST_0_i_514_n_0\
    );
\disp[5][0]_INST_0_i_515\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_6_n_4\,
      I1 => \disp[5][0]_INST_0_i_6_n_7\,
      O => \disp[5][0]_INST_0_i_515_n_0\
    );
\disp[5][0]_INST_0_i_516\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_73_n_4\,
      I1 => \disp[5][0]_INST_0_i_72_n_5\,
      I2 => \disp[5][0]_INST_0_i_527_n_6\,
      I3 => \disp[5][0]_INST_0_i_529_n_5\,
      I4 => \disp[5][0]_INST_0_i_572_n_0\,
      O => \disp[5][0]_INST_0_i_516_n_0\
    );
\disp[5][0]_INST_0_i_517\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_73_n_5\,
      I1 => \disp[5][0]_INST_0_i_72_n_6\,
      I2 => \disp[5][0]_INST_0_i_527_n_7\,
      I3 => \disp[5][0]_INST_0_i_529_n_6\,
      I4 => \disp[5][0]_INST_0_i_573_n_0\,
      O => \disp[5][0]_INST_0_i_517_n_0\
    );
\disp[5][0]_INST_0_i_518\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_73_n_6\,
      I1 => \disp[5][0]_INST_0_i_72_n_7\,
      I2 => \disp[5][0]_INST_0_i_62_n_4\,
      I3 => \disp[5][0]_INST_0_i_529_n_7\,
      I4 => \disp[5][0]_INST_0_i_574_n_0\,
      O => \disp[5][0]_INST_0_i_518_n_0\
    );
\disp[5][0]_INST_0_i_519\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_73_n_7\,
      I1 => \disp[5][0]_INST_0_i_61_n_4\,
      I2 => \disp[5][0]_INST_0_i_62_n_5\,
      I3 => \disp[5][0]_INST_0_i_63_n_4\,
      I4 => \disp[5][0]_INST_0_i_575_n_0\,
      O => \disp[5][0]_INST_0_i_519_n_0\
    );
\disp[5][0]_INST_0_i_52\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_122_n_4\,
      I1 => \disp[5][0]_INST_0_i_68_n_5\,
      I2 => \disp[5][0]_INST_0_i_66_n_6\,
      I3 => \disp[5][0]_INST_0_i_69_n_5\,
      I4 => \disp[5][0]_INST_0_i_123_n_0\,
      O => \disp[5][0]_INST_0_i_52_n_0\
    );
\disp[5][0]_INST_0_i_520\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_516_n_0\,
      I1 => \disp[5][0]_INST_0_i_576_n_0\,
      I2 => \disp[5][0]_INST_0_i_527_n_4\,
      I3 => \disp[5][0]_INST_0_i_339_n_7\,
      I4 => \disp[5][0]_INST_0_i_524_n_6\,
      I5 => \disp[5][0]_INST_0_i_529_n_4\,
      O => \disp[5][0]_INST_0_i_520_n_0\
    );
\disp[5][0]_INST_0_i_521\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_517_n_0\,
      I1 => \disp[5][0]_INST_0_i_577_n_0\,
      I2 => \disp[5][0]_INST_0_i_527_n_5\,
      I3 => \disp[5][0]_INST_0_i_72_n_4\,
      I4 => \disp[5][0]_INST_0_i_524_n_7\,
      I5 => \disp[5][0]_INST_0_i_529_n_5\,
      O => \disp[5][0]_INST_0_i_521_n_0\
    );
\disp[5][0]_INST_0_i_522\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_518_n_0\,
      I1 => \disp[5][0]_INST_0_i_578_n_0\,
      I2 => \disp[5][0]_INST_0_i_527_n_6\,
      I3 => \disp[5][0]_INST_0_i_72_n_5\,
      I4 => \disp[5][0]_INST_0_i_73_n_4\,
      I5 => \disp[5][0]_INST_0_i_529_n_6\,
      O => \disp[5][0]_INST_0_i_522_n_0\
    );
\disp[5][0]_INST_0_i_523\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_519_n_0\,
      I1 => \disp[5][0]_INST_0_i_579_n_0\,
      I2 => \disp[5][0]_INST_0_i_527_n_7\,
      I3 => \disp[5][0]_INST_0_i_72_n_6\,
      I4 => \disp[5][0]_INST_0_i_73_n_5\,
      I5 => \disp[5][0]_INST_0_i_529_n_7\,
      O => \disp[5][0]_INST_0_i_523_n_0\
    );
\disp[5][0]_INST_0_i_524\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_73_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_524_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_524_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_524_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_524_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_580_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_581_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_582_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_583_n_0\,
      O(3) => \disp[5][0]_INST_0_i_524_n_4\,
      O(2) => \disp[5][0]_INST_0_i_524_n_5\,
      O(1) => \disp[5][0]_INST_0_i_524_n_6\,
      O(0) => \disp[5][0]_INST_0_i_524_n_7\,
      S(3) => \disp[5][0]_INST_0_i_584_n_0\,
      S(2) => \disp[5][0]_INST_0_i_585_n_0\,
      S(1) => \disp[5][0]_INST_0_i_586_n_0\,
      S(0) => \disp[5][0]_INST_0_i_587_n_0\
    );
\disp[5][0]_INST_0_i_525\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_529_n_0\,
      CO(3 downto 2) => \NLW_disp[5][0]_INST_0_i_525_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \disp[5][0]_INST_0_i_525_n_2\,
      CO(0) => \NLW_disp[5][0]_INST_0_i_525_CO_UNCONNECTED\(0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_disp[5][0]_INST_0_i_525_O_UNCONNECTED\(3 downto 1),
      O(0) => \disp[5][0]_INST_0_i_525_n_7\,
      S(3 downto 1) => B"001",
      S(0) => \disp[5][0]_INST_0_i_588_n_0\
    );
\disp[5][0]_INST_0_i_526\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_441_n_6\,
      I1 => \disp[5][0]_INST_0_i_339_n_5\,
      I2 => \disp[5][0]_INST_0_i_524_n_4\,
      O => \disp[5][0]_INST_0_i_526_n_0\
    );
\disp[5][0]_INST_0_i_527\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_62_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_527_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_527_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_527_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_527_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \inst_int_to_string/euros2\(20 downto 17),
      O(3) => \disp[5][0]_INST_0_i_527_n_4\,
      O(2) => \disp[5][0]_INST_0_i_527_n_5\,
      O(1) => \disp[5][0]_INST_0_i_527_n_6\,
      O(0) => \disp[5][0]_INST_0_i_527_n_7\,
      S(3) => \disp[5][0]_INST_0_i_593_n_0\,
      S(2) => \disp[5][0]_INST_0_i_594_n_0\,
      S(1) => \disp[5][0]_INST_0_i_595_n_0\,
      S(0) => \disp[5][0]_INST_0_i_596_n_0\
    );
\disp[5][0]_INST_0_i_528\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_441_n_7\,
      I1 => \disp[5][0]_INST_0_i_339_n_6\,
      I2 => \disp[5][0]_INST_0_i_524_n_5\,
      O => \disp[5][0]_INST_0_i_528_n_0\
    );
\disp[5][0]_INST_0_i_529\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_63_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_529_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_529_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_529_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_529_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \disp[5][0]_INST_0_i_597_n_0\,
      O(3) => \disp[5][0]_INST_0_i_529_n_4\,
      O(2) => \disp[5][0]_INST_0_i_529_n_5\,
      O(1) => \disp[5][0]_INST_0_i_529_n_6\,
      O(0) => \disp[5][0]_INST_0_i_529_n_7\,
      S(3) => \disp[5][0]_INST_0_i_598_n_0\,
      S(2) => \disp[5][0]_INST_0_i_599_n_0\,
      S(1) => \disp[5][0]_INST_0_i_600_n_0\,
      S(0) => \disp[5][0]_INST_0_i_601_n_0\
    );
\disp[5][0]_INST_0_i_53\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEE8E888"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_69_n_6\,
      I1 => \disp[5][0]_INST_0_i_124_n_0\,
      I2 => \disp[5][0]_INST_0_i_122_n_5\,
      I3 => \disp[5][0]_INST_0_i_68_n_6\,
      I4 => \disp[5][0]_INST_0_i_66_n_7\,
      O => \disp[5][0]_INST_0_i_53_n_0\
    );
\disp[5][0]_INST_0_i_530\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_527_n_4\,
      I1 => \disp[5][0]_INST_0_i_339_n_7\,
      I2 => \disp[5][0]_INST_0_i_524_n_6\,
      O => \disp[5][0]_INST_0_i_530_n_0\
    );
\disp[5][0]_INST_0_i_531\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_441_n_4\,
      I1 => \disp[5][0]_INST_0_i_440_n_6\,
      I2 => \disp[5][0]_INST_0_i_239_n_7\,
      O => \disp[5][0]_INST_0_i_531_n_0\
    );
\disp[5][0]_INST_0_i_532\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_441_n_5\,
      I1 => \disp[5][0]_INST_0_i_440_n_7\,
      I2 => \disp[5][0]_INST_0_i_339_n_4\,
      O => \disp[5][0]_INST_0_i_532_n_0\
    );
\disp[5][0]_INST_0_i_533\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_524_n_5\,
      I1 => \disp[5][0]_INST_0_i_339_n_6\,
      I2 => \disp[5][0]_INST_0_i_441_n_7\,
      O => \disp[5][0]_INST_0_i_533_n_0\
    );
\disp[5][0]_INST_0_i_534\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_524_n_6\,
      I1 => \disp[5][0]_INST_0_i_339_n_7\,
      I2 => \disp[5][0]_INST_0_i_527_n_4\,
      O => \disp[5][0]_INST_0_i_534_n_0\
    );
\disp[5][0]_INST_0_i_535\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(25),
      I1 => \inst_int_to_string/euros3\(21),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(19),
      O => \disp[5][0]_INST_0_i_535_n_0\
    );
\disp[5][0]_INST_0_i_536\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(24),
      I1 => \inst_int_to_string/euros3\(20),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(18),
      O => \disp[5][0]_INST_0_i_536_n_0\
    );
\disp[5][0]_INST_0_i_537\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(23),
      I1 => \inst_int_to_string/euros3\(19),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(17),
      O => \disp[5][0]_INST_0_i_537_n_0\
    );
\disp[5][0]_INST_0_i_538\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(22),
      I1 => \inst_int_to_string/euros3\(18),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(16),
      O => \disp[5][0]_INST_0_i_538_n_0\
    );
\disp[5][0]_INST_0_i_539\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F6F90906"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(26),
      I1 => \inst_int_to_string/euros3\(22),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(20),
      I4 => \disp[5][0]_INST_0_i_535_n_0\,
      O => \disp[5][0]_INST_0_i_539_n_0\
    );
\disp[5][0]_INST_0_i_54\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_122_n_6\,
      I1 => \disp[5][0]_INST_0_i_68_n_7\,
      I2 => \disp[5][0]_INST_0_i_125_n_4\,
      I3 => \disp[5][0]_INST_0_i_69_n_7\,
      I4 => \disp[5][0]_INST_0_i_126_n_0\,
      O => \disp[5][0]_INST_0_i_54_n_0\
    );
\disp[5][0]_INST_0_i_540\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F6F90906"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(25),
      I1 => \inst_int_to_string/euros3\(21),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(19),
      I4 => \disp[5][0]_INST_0_i_536_n_0\,
      O => \disp[5][0]_INST_0_i_540_n_0\
    );
\disp[5][0]_INST_0_i_541\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F6F90906"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(24),
      I1 => \inst_int_to_string/euros3\(20),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(18),
      I4 => \disp[5][0]_INST_0_i_537_n_0\,
      O => \disp[5][0]_INST_0_i_541_n_0\
    );
\disp[5][0]_INST_0_i_542\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F6F90906"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(23),
      I1 => \inst_int_to_string/euros3\(19),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(17),
      I4 => \disp[5][0]_INST_0_i_538_n_0\,
      O => \disp[5][0]_INST_0_i_542_n_0\
    );
\disp[5][0]_INST_0_i_543\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(24),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      O => \inst_int_to_string/euros2\(24)
    );
\disp[5][0]_INST_0_i_544\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(23),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      O => \inst_int_to_string/euros2\(23)
    );
\disp[5][0]_INST_0_i_545\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(22),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      O => \inst_int_to_string/euros2\(22)
    );
\disp[5][0]_INST_0_i_546\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(21),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      O => \inst_int_to_string/euros2\(21)
    );
\disp[5][0]_INST_0_i_547\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(24),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(27),
      O => \disp[5][0]_INST_0_i_547_n_0\
    );
\disp[5][0]_INST_0_i_548\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(23),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(26),
      O => \disp[5][0]_INST_0_i_548_n_0\
    );
\disp[5][0]_INST_0_i_549\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(22),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(25),
      O => \disp[5][0]_INST_0_i_549_n_0\
    );
\disp[5][0]_INST_0_i_55\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_122_n_7\,
      I1 => \disp[5][0]_INST_0_i_127_n_4\,
      I2 => \disp[5][0]_INST_0_i_125_n_5\,
      I3 => \disp[5][0]_INST_0_i_128_n_4\,
      I4 => \disp[5][0]_INST_0_i_129_n_0\,
      O => \disp[5][0]_INST_0_i_55_n_0\
    );
\disp[5][0]_INST_0_i_550\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(21),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(24),
      O => \disp[5][0]_INST_0_i_550_n_0\
    );
\disp[5][0]_INST_0_i_551\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_602_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_551_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_551_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_551_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_551_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_603_n_0\,
      DI(2) => \inst_int_to_string/euros2\(7),
      DI(1) => \disp[5][0]_INST_0_i_605_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_606_n_0\,
      O(3) => \disp[5][0]_INST_0_i_551_n_4\,
      O(2 downto 0) => \NLW_disp[5][0]_INST_0_i_551_O_UNCONNECTED\(2 downto 0),
      S(3) => \disp[5][0]_INST_0_i_607_n_0\,
      S(2) => \disp[5][0]_INST_0_i_608_n_0\,
      S(1) => \disp[5][0]_INST_0_i_609_n_0\,
      S(0) => \disp[5][0]_INST_0_i_610_n_0\
    );
\disp[5][0]_INST_0_i_552\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"5440"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_4_n_3\,
      I1 => \inst_int_to_string/euros3\(4),
      I2 => \inst_int_to_string/euros3\(11),
      I3 => \inst_int_to_string/euros3\(9),
      O => \disp[5][0]_INST_0_i_552_n_0\
    );
\disp[5][0]_INST_0_i_553\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(10),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(8),
      I3 => \inst_int_to_string/euros3\(3),
      O => \disp[5][0]_INST_0_i_553_n_0\
    );
\disp[5][0]_INST_0_i_554\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCA0F0ACAC00A00"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(7),
      I1 => \disp[5][0]_INST_0_i_11_n_4\,
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(9),
      I4 => \disp[5][0]_INST_0_i_35_n_5\,
      I5 => \inst_int_to_string/euros3\(2),
      O => \disp[5][0]_INST_0_i_554_n_0\
    );
\disp[5][0]_INST_0_i_555\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAFC00FCAAC000C0"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_35_n_6\,
      I1 => \inst_int_to_string/euros3\(1),
      I2 => \inst_int_to_string/euros3\(8),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_11_n_5\,
      I5 => \inst_int_to_string/euros3\(6),
      O => \disp[5][0]_INST_0_i_555_n_0\
    );
\disp[5][0]_INST_0_i_556\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5659A6A95956A9A6"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_552_n_0\,
      I1 => \inst_int_to_string/euros3\(10),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(12),
      I4 => \disp[5][0]_INST_0_i_11_n_6\,
      I5 => \inst_int_to_string/euros3\(5),
      O => \disp[5][0]_INST_0_i_556_n_0\
    );
\disp[5][0]_INST_0_i_557\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5659A6A95956A9A6"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_553_n_0\,
      I1 => \inst_int_to_string/euros3\(9),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(11),
      I4 => \disp[5][0]_INST_0_i_11_n_7\,
      I5 => \inst_int_to_string/euros3\(4),
      O => \disp[5][0]_INST_0_i_557_n_0\
    );
\disp[5][0]_INST_0_i_558\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5659A6A95956A9A6"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_554_n_0\,
      I1 => \inst_int_to_string/euros3\(8),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(10),
      I4 => \disp[5][0]_INST_0_i_35_n_4\,
      I5 => \inst_int_to_string/euros3\(3),
      O => \disp[5][0]_INST_0_i_558_n_0\
    );
\disp[5][0]_INST_0_i_559\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9969666999966696"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_555_n_0\,
      I1 => \inst_int_to_string/euros2\(2),
      I2 => \inst_int_to_string/euros3\(7),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_11_n_4\,
      I5 => \inst_int_to_string/euros3\(9),
      O => \disp[5][0]_INST_0_i_559_n_0\
    );
\disp[5][0]_INST_0_i_56\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_52_n_0\,
      I1 => \disp[5][0]_INST_0_i_130_n_0\,
      I2 => \disp[5][0]_INST_0_i_66_n_4\,
      I3 => \disp[5][0]_INST_0_i_61_n_7\,
      I4 => \disp[5][0]_INST_0_i_60_n_6\,
      I5 => \disp[5][0]_INST_0_i_69_n_4\,
      O => \disp[5][0]_INST_0_i_56_n_0\
    );
\disp[5][0]_INST_0_i_560\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"656A959A6A659A95"
    )
        port map (
      I0 => \inst_int_to_string/euros2\(0),
      I1 => \disp[5][0]_INST_0_i_35_n_6\,
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(1),
      I4 => \disp[5][0]_INST_0_i_35_n_4\,
      I5 => \inst_int_to_string/euros3\(3),
      O => \disp[5][0]_INST_0_i_560_n_0\
    );
\disp[5][0]_INST_0_i_561\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9A95"
    )
        port map (
      I0 => \inst_int_to_string/euros2\(0),
      I1 => \disp[5][0]_INST_0_i_35_n_5\,
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(2),
      O => \disp[5][0]_INST_0_i_561_n_0\
    );
\disp[5][0]_INST_0_i_562\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1D"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(1),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \disp[5][0]_INST_0_i_35_n_6\,
      O => \disp[5][0]_INST_0_i_562_n_0\
    );
\disp[5][0]_INST_0_i_563\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"02A2"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_6_n_5\,
      I1 => \inst_int_to_string/euros3\(4),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \disp[5][0]_INST_0_i_11_n_7\,
      O => \disp[5][0]_INST_0_i_563_n_0\
    );
\disp[5][0]_INST_0_i_564\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"02A2"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_6_n_6\,
      I1 => \inst_int_to_string/euros3\(3),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \disp[5][0]_INST_0_i_35_n_4\,
      O => \disp[5][0]_INST_0_i_564_n_0\
    );
\disp[5][0]_INST_0_i_565\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"ABFB"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_6_n_7\,
      I1 => \inst_int_to_string/euros3\(2),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \disp[5][0]_INST_0_i_35_n_5\,
      O => \disp[5][0]_INST_0_i_565_n_0\
    );
\disp[5][0]_INST_0_i_566\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4700B8FFB8FF4700"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_11_n_7\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(4),
      I3 => \disp[5][0]_INST_0_i_6_n_5\,
      I4 => \inst_int_to_string/euros2\(5),
      I5 => \disp[5][0]_INST_0_i_422_n_7\,
      O => \disp[5][0]_INST_0_i_566_n_0\
    );
\disp[5][0]_INST_0_i_567\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8FF47004700B8FF"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_35_n_4\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(3),
      I3 => \disp[5][0]_INST_0_i_6_n_6\,
      I4 => \inst_int_to_string/euros2\(4),
      I5 => \disp[5][0]_INST_0_i_6_n_5\,
      O => \disp[5][0]_INST_0_i_567_n_0\
    );
\disp[5][0]_INST_0_i_568\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2D222DDDD2DDD222"
    )
        port map (
      I0 => \inst_int_to_string/euros2\(2),
      I1 => \disp[5][0]_INST_0_i_6_n_7\,
      I2 => \disp[5][0]_INST_0_i_35_n_4\,
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \inst_int_to_string/euros3\(3),
      I5 => \disp[5][0]_INST_0_i_6_n_6\,
      O => \disp[5][0]_INST_0_i_568_n_0\
    );
\disp[5][0]_INST_0_i_569\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1DE2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(2),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \disp[5][0]_INST_0_i_35_n_5\,
      I3 => \disp[5][0]_INST_0_i_6_n_7\,
      O => \disp[5][0]_INST_0_i_569_n_0\
    );
\disp[5][0]_INST_0_i_57\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_53_n_0\,
      I1 => \disp[5][0]_INST_0_i_131_n_0\,
      I2 => \disp[5][0]_INST_0_i_66_n_5\,
      I3 => \disp[5][0]_INST_0_i_68_n_4\,
      I4 => \disp[5][0]_INST_0_i_60_n_7\,
      I5 => \disp[5][0]_INST_0_i_69_n_5\,
      O => \disp[5][0]_INST_0_i_57_n_0\
    );
\disp[5][0]_INST_0_i_570\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_167_n_3\,
      I1 => \disp[2][0]_INST_0_i_168_n_0\,
      O => \disp[5][0]_INST_0_i_570_n_0\
    );
\disp[5][0]_INST_0_i_571\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \disp[2][0]_INST_0_i_167_n_3\,
      I1 => \disp[2][0]_INST_0_i_168_n_0\,
      O => \disp[5][0]_INST_0_i_571_n_0\
    );
\disp[5][0]_INST_0_i_572\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_527_n_5\,
      I1 => \disp[5][0]_INST_0_i_72_n_4\,
      I2 => \disp[5][0]_INST_0_i_524_n_7\,
      O => \disp[5][0]_INST_0_i_572_n_0\
    );
\disp[5][0]_INST_0_i_573\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_527_n_6\,
      I1 => \disp[5][0]_INST_0_i_72_n_5\,
      I2 => \disp[5][0]_INST_0_i_73_n_4\,
      O => \disp[5][0]_INST_0_i_573_n_0\
    );
\disp[5][0]_INST_0_i_574\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_527_n_7\,
      I1 => \disp[5][0]_INST_0_i_72_n_6\,
      I2 => \disp[5][0]_INST_0_i_73_n_5\,
      O => \disp[5][0]_INST_0_i_574_n_0\
    );
\disp[5][0]_INST_0_i_575\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_62_n_4\,
      I1 => \disp[5][0]_INST_0_i_72_n_7\,
      I2 => \disp[5][0]_INST_0_i_73_n_6\,
      O => \disp[5][0]_INST_0_i_575_n_0\
    );
\disp[5][0]_INST_0_i_576\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_524_n_7\,
      I1 => \disp[5][0]_INST_0_i_72_n_4\,
      I2 => \disp[5][0]_INST_0_i_527_n_5\,
      O => \disp[5][0]_INST_0_i_576_n_0\
    );
\disp[5][0]_INST_0_i_577\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_73_n_4\,
      I1 => \disp[5][0]_INST_0_i_72_n_5\,
      I2 => \disp[5][0]_INST_0_i_527_n_6\,
      O => \disp[5][0]_INST_0_i_577_n_0\
    );
\disp[5][0]_INST_0_i_578\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_73_n_5\,
      I1 => \disp[5][0]_INST_0_i_72_n_6\,
      I2 => \disp[5][0]_INST_0_i_527_n_7\,
      O => \disp[5][0]_INST_0_i_578_n_0\
    );
\disp[5][0]_INST_0_i_579\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_73_n_6\,
      I1 => \disp[5][0]_INST_0_i_72_n_7\,
      I2 => \disp[5][0]_INST_0_i_62_n_4\,
      O => \disp[5][0]_INST_0_i_579_n_0\
    );
\disp[5][0]_INST_0_i_58\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6969699669969696"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_54_n_0\,
      I1 => \disp[5][0]_INST_0_i_69_n_6\,
      I2 => \disp[5][0]_INST_0_i_124_n_0\,
      I3 => \disp[5][0]_INST_0_i_122_n_5\,
      I4 => \disp[5][0]_INST_0_i_68_n_6\,
      I5 => \disp[5][0]_INST_0_i_66_n_7\,
      O => \disp[5][0]_INST_0_i_58_n_0\
    );
\disp[5][0]_INST_0_i_580\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(21),
      I1 => \inst_int_to_string/euros3\(17),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(15),
      O => \disp[5][0]_INST_0_i_580_n_0\
    );
\disp[5][0]_INST_0_i_581\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(20),
      I1 => \inst_int_to_string/euros3\(16),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(14),
      O => \disp[5][0]_INST_0_i_581_n_0\
    );
\disp[5][0]_INST_0_i_582\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0E08"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(19),
      I1 => \inst_int_to_string/euros3\(15),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(13),
      O => \disp[5][0]_INST_0_i_582_n_0\
    );
\disp[5][0]_INST_0_i_583\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3220"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(18),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(14),
      I3 => \inst_int_to_string/euros3\(12),
      O => \disp[5][0]_INST_0_i_583_n_0\
    );
\disp[5][0]_INST_0_i_584\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F6F90906"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(22),
      I1 => \inst_int_to_string/euros3\(18),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(16),
      I4 => \disp[5][0]_INST_0_i_580_n_0\,
      O => \disp[5][0]_INST_0_i_584_n_0\
    );
\disp[5][0]_INST_0_i_585\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F6F90906"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(21),
      I1 => \inst_int_to_string/euros3\(17),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(15),
      I4 => \disp[5][0]_INST_0_i_581_n_0\,
      O => \disp[5][0]_INST_0_i_585_n_0\
    );
\disp[5][0]_INST_0_i_586\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F6F90906"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(20),
      I1 => \inst_int_to_string/euros3\(16),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(14),
      I4 => \disp[5][0]_INST_0_i_582_n_0\,
      O => \disp[5][0]_INST_0_i_586_n_0\
    );
\disp[5][0]_INST_0_i_587\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F6F90906"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(19),
      I1 => \inst_int_to_string/euros3\(15),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(13),
      I4 => \disp[5][0]_INST_0_i_583_n_0\,
      O => \disp[5][0]_INST_0_i_587_n_0\
    );
\disp[5][0]_INST_0_i_588\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(30),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      O => \disp[5][0]_INST_0_i_588_n_0\
    );
\disp[5][0]_INST_0_i_589\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(20),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      O => \inst_int_to_string/euros2\(20)
    );
\disp[5][0]_INST_0_i_59\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9669699669969669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_55_n_0\,
      I1 => \disp[5][0]_INST_0_i_132_n_0\,
      I2 => \disp[5][0]_INST_0_i_66_n_7\,
      I3 => \disp[5][0]_INST_0_i_68_n_6\,
      I4 => \disp[5][0]_INST_0_i_122_n_5\,
      I5 => \disp[5][0]_INST_0_i_69_n_7\,
      O => \disp[5][0]_INST_0_i_59_n_0\
    );
\disp[5][0]_INST_0_i_590\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(19),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      O => \inst_int_to_string/euros2\(19)
    );
\disp[5][0]_INST_0_i_591\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(18),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      O => \inst_int_to_string/euros2\(18)
    );
\disp[5][0]_INST_0_i_592\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(17),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      O => \inst_int_to_string/euros2\(17)
    );
\disp[5][0]_INST_0_i_593\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(20),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(23),
      O => \disp[5][0]_INST_0_i_593_n_0\
    );
\disp[5][0]_INST_0_i_594\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(19),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(22),
      O => \disp[5][0]_INST_0_i_594_n_0\
    );
\disp[5][0]_INST_0_i_595\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(18),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(21),
      O => \disp[5][0]_INST_0_i_595_n_0\
    );
\disp[5][0]_INST_0_i_596\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"ED"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(17),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(20),
      O => \disp[5][0]_INST_0_i_596_n_0\
    );
\disp[5][0]_INST_0_i_597\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(26),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      O => \disp[5][0]_INST_0_i_597_n_0\
    );
\disp[5][0]_INST_0_i_598\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(29),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      O => \disp[5][0]_INST_0_i_598_n_0\
    );
\disp[5][0]_INST_0_i_599\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(28),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      O => \disp[5][0]_INST_0_i_599_n_0\
    );
\disp[5][0]_INST_0_i_6\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_16_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_6_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_6_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_6_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_6_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_17_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_18_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_19_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_20_n_0\,
      O(3) => \disp[5][0]_INST_0_i_6_n_4\,
      O(2) => \disp[5][0]_INST_0_i_6_n_5\,
      O(1) => \disp[5][0]_INST_0_i_6_n_6\,
      O(0) => \disp[5][0]_INST_0_i_6_n_7\,
      S(3) => \disp[5][0]_INST_0_i_21_n_0\,
      S(2) => \disp[5][0]_INST_0_i_22_n_0\,
      S(1) => \disp[5][0]_INST_0_i_23_n_0\,
      S(0) => \disp[5][0]_INST_0_i_24_n_0\
    );
\disp[5][0]_INST_0_i_60\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_122_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_60_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_60_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_60_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_60_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_133_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_134_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_135_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_136_n_0\,
      O(3) => \disp[5][0]_INST_0_i_60_n_4\,
      O(2) => \disp[5][0]_INST_0_i_60_n_5\,
      O(1) => \disp[5][0]_INST_0_i_60_n_6\,
      O(0) => \disp[5][0]_INST_0_i_60_n_7\,
      S(3) => \disp[5][0]_INST_0_i_137_n_0\,
      S(2) => \disp[5][0]_INST_0_i_138_n_0\,
      S(1) => \disp[5][0]_INST_0_i_139_n_0\,
      S(0) => \disp[5][0]_INST_0_i_140_n_0\
    );
\disp[5][0]_INST_0_i_600\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(27),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      O => \disp[5][0]_INST_0_i_600_n_0\
    );
\disp[5][0]_INST_0_i_601\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0708"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(25),
      I1 => \inst_int_to_string/euros3\(30),
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(26),
      O => \disp[5][0]_INST_0_i_601_n_0\
    );
\disp[5][0]_INST_0_i_602\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \disp[5][0]_INST_0_i_602_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_602_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_602_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_602_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_611_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_612_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_613_n_0\,
      DI(0) => '0',
      O(3 downto 0) => \NLW_disp[5][0]_INST_0_i_602_O_UNCONNECTED\(3 downto 0),
      S(3) => \disp[5][0]_INST_0_i_614_n_0\,
      S(2) => \disp[5][0]_INST_0_i_615_n_0\,
      S(1) => \disp[5][0]_INST_0_i_616_n_0\,
      S(0) => \disp[5][0]_INST_0_i_617_n_0\
    );
\disp[5][0]_INST_0_i_603\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3A35CAC5353AC5CA"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(8),
      I1 => \disp[5][0]_INST_0_i_35_n_6\,
      I2 => \disp[5][0]_INST_0_i_4_n_3\,
      I3 => \inst_int_to_string/euros3\(1),
      I4 => \disp[5][0]_INST_0_i_11_n_5\,
      I5 => \inst_int_to_string/euros3\(6),
      O => \disp[5][0]_INST_0_i_603_n_0\
    );
\disp[5][0]_INST_0_i_604\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(7),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \disp[5][0]_INST_0_i_11_n_4\,
      O => \inst_int_to_string/euros2\(7)
    );
\disp[5][0]_INST_0_i_605\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(6),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \disp[5][0]_INST_0_i_11_n_5\,
      O => \disp[5][0]_INST_0_i_605_n_0\
    );
\disp[5][0]_INST_0_i_606\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_11_n_6\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(5),
      O => \disp[5][0]_INST_0_i_606_n_0\
    );
\disp[5][0]_INST_0_i_607\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6696996999699969"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_618_n_0\,
      I1 => \inst_int_to_string/euros2\(1),
      I2 => \inst_int_to_string/euros3\(8),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \inst_int_to_string/euros2\(0),
      I5 => \inst_int_to_string/euros2\(5),
      O => \disp[5][0]_INST_0_i_607_n_0\
    );
\disp[5][0]_INST_0_i_608\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9999A55A6666A55A"
    )
        port map (
      I0 => \inst_int_to_string/euros2\(0),
      I1 => \disp[5][0]_INST_0_i_11_n_6\,
      I2 => \inst_int_to_string/euros3\(5),
      I3 => \inst_int_to_string/euros3\(7),
      I4 => \disp[5][0]_INST_0_i_4_n_3\,
      I5 => \disp[5][0]_INST_0_i_11_n_4\,
      O => \disp[5][0]_INST_0_i_608_n_0\
    );
\disp[5][0]_INST_0_i_609\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"335ACC5A"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(6),
      I1 => \disp[5][0]_INST_0_i_11_n_5\,
      I2 => \inst_int_to_string/euros3\(4),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_11_n_7\,
      O => \disp[5][0]_INST_0_i_609_n_0\
    );
\disp[5][0]_INST_0_i_61\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_68_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_61_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_61_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_61_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_61_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_141_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_142_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_143_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_144_n_0\,
      O(3) => \disp[5][0]_INST_0_i_61_n_4\,
      O(2) => \disp[5][0]_INST_0_i_61_n_5\,
      O(1) => \disp[5][0]_INST_0_i_61_n_6\,
      O(0) => \disp[5][0]_INST_0_i_61_n_7\,
      S(3) => \disp[5][0]_INST_0_i_145_n_0\,
      S(2) => \disp[5][0]_INST_0_i_146_n_0\,
      S(1) => \disp[5][0]_INST_0_i_147_n_0\,
      S(0) => \disp[5][0]_INST_0_i_148_n_0\
    );
\disp[5][0]_INST_0_i_610\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"335ACC5A"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(5),
      I1 => \disp[5][0]_INST_0_i_11_n_6\,
      I2 => \inst_int_to_string/euros3\(3),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_35_n_4\,
      O => \disp[5][0]_INST_0_i_610_n_0\
    );
\disp[5][0]_INST_0_i_611\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_11_n_7\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(4),
      O => \disp[5][0]_INST_0_i_611_n_0\
    );
\disp[5][0]_INST_0_i_612\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_35_n_4\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(3),
      O => \disp[5][0]_INST_0_i_612_n_0\
    );
\disp[5][0]_INST_0_i_613\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_35_n_5\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(2),
      O => \disp[5][0]_INST_0_i_613_n_0\
    );
\disp[5][0]_INST_0_i_614\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"335ACC5A"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(4),
      I1 => \disp[5][0]_INST_0_i_11_n_7\,
      I2 => \inst_int_to_string/euros3\(2),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_35_n_5\,
      O => \disp[5][0]_INST_0_i_614_n_0\
    );
\disp[5][0]_INST_0_i_615\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"335ACC5A"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(3),
      I1 => \disp[5][0]_INST_0_i_35_n_4\,
      I2 => \inst_int_to_string/euros3\(1),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_35_n_6\,
      O => \disp[5][0]_INST_0_i_615_n_0\
    );
\disp[5][0]_INST_0_i_616\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1DE2"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(2),
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \disp[5][0]_INST_0_i_35_n_5\,
      I3 => \inst_int_to_string/euros2\(0),
      O => \disp[5][0]_INST_0_i_616_n_0\
    );
\disp[5][0]_INST_0_i_617\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_35_n_6\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(1),
      O => \disp[5][0]_INST_0_i_617_n_0\
    );
\disp[5][0]_INST_0_i_618\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"47"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_11_n_5\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(6),
      O => \disp[5][0]_INST_0_i_618_n_0\
    );
\disp[5][0]_INST_0_i_62\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_66_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_62_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_62_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_62_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_62_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \inst_int_to_string/euros2\(16 downto 13),
      O(3) => \disp[5][0]_INST_0_i_62_n_4\,
      O(2) => \disp[5][0]_INST_0_i_62_n_5\,
      O(1) => \disp[5][0]_INST_0_i_62_n_6\,
      O(0) => \disp[5][0]_INST_0_i_62_n_7\,
      S(3) => \disp[5][0]_INST_0_i_153_n_0\,
      S(2) => \disp[5][0]_INST_0_i_154_n_0\,
      S(1) => \disp[5][0]_INST_0_i_155_n_0\,
      S(0) => \disp[5][0]_INST_0_i_156_n_0\
    );
\disp[5][0]_INST_0_i_63\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_69_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_63_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_63_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_63_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_63_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_157_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_158_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_159_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_160_n_0\,
      O(3) => \disp[5][0]_INST_0_i_63_n_4\,
      O(2) => \disp[5][0]_INST_0_i_63_n_5\,
      O(1) => \disp[5][0]_INST_0_i_63_n_6\,
      O(0) => \disp[5][0]_INST_0_i_63_n_7\,
      S(3) => \disp[5][0]_INST_0_i_161_n_0\,
      S(2) => \disp[5][0]_INST_0_i_162_n_0\,
      S(1) => \disp[5][0]_INST_0_i_163_n_0\,
      S(0) => \disp[5][0]_INST_0_i_164_n_0\
    );
\disp[5][0]_INST_0_i_64\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_62_n_5\,
      I1 => \disp[5][0]_INST_0_i_61_n_4\,
      I2 => \disp[5][0]_INST_0_i_73_n_7\,
      O => \disp[5][0]_INST_0_i_64_n_0\
    );
\disp[5][0]_INST_0_i_65\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_62_n_6\,
      I1 => \disp[5][0]_INST_0_i_61_n_5\,
      I2 => \disp[5][0]_INST_0_i_60_n_4\,
      O => \disp[5][0]_INST_0_i_65_n_0\
    );
\disp[5][0]_INST_0_i_66\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_125_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_66_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_66_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_66_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_66_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \inst_int_to_string/euros2\(12 downto 9),
      O(3) => \disp[5][0]_INST_0_i_66_n_4\,
      O(2) => \disp[5][0]_INST_0_i_66_n_5\,
      O(1) => \disp[5][0]_INST_0_i_66_n_6\,
      O(0) => \disp[5][0]_INST_0_i_66_n_7\,
      S(3) => \disp[5][0]_INST_0_i_169_n_0\,
      S(2) => \disp[5][0]_INST_0_i_170_n_0\,
      S(1) => \disp[5][0]_INST_0_i_171_n_0\,
      S(0) => \disp[5][0]_INST_0_i_172_n_0\
    );
\disp[5][0]_INST_0_i_67\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_62_n_7\,
      I1 => \disp[5][0]_INST_0_i_61_n_6\,
      I2 => \disp[5][0]_INST_0_i_60_n_5\,
      O => \disp[5][0]_INST_0_i_67_n_0\
    );
\disp[5][0]_INST_0_i_68\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_127_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_68_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_68_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_68_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_68_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_173_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_174_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_175_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_176_n_0\,
      O(3) => \disp[5][0]_INST_0_i_68_n_4\,
      O(2) => \disp[5][0]_INST_0_i_68_n_5\,
      O(1) => \disp[5][0]_INST_0_i_68_n_6\,
      O(0) => \disp[5][0]_INST_0_i_68_n_7\,
      S(3) => \disp[5][0]_INST_0_i_177_n_0\,
      S(2) => \disp[5][0]_INST_0_i_178_n_0\,
      S(1) => \disp[5][0]_INST_0_i_179_n_0\,
      S(0) => \disp[5][0]_INST_0_i_180_n_0\
    );
\disp[5][0]_INST_0_i_69\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_128_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_69_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_69_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_69_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_69_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_181_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_182_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_183_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_184_n_0\,
      O(3) => \disp[5][0]_INST_0_i_69_n_4\,
      O(2) => \disp[5][0]_INST_0_i_69_n_5\,
      O(1) => \disp[5][0]_INST_0_i_69_n_6\,
      O(0) => \disp[5][0]_INST_0_i_69_n_7\,
      S(3) => \disp[5][0]_INST_0_i_185_n_0\,
      S(2) => \disp[5][0]_INST_0_i_186_n_0\,
      S(1) => \disp[5][0]_INST_0_i_187_n_0\,
      S(0) => \disp[5][0]_INST_0_i_188_n_0\
    );
\disp[5][0]_INST_0_i_7\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_25_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_7_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_7_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_7_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_7_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_26_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_27_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_28_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_29_n_0\,
      O(3 downto 0) => \NLW_disp[5][0]_INST_0_i_7_O_UNCONNECTED\(3 downto 0),
      S(3) => \disp[5][0]_INST_0_i_30_n_0\,
      S(2) => \disp[5][0]_INST_0_i_31_n_0\,
      S(1) => \disp[5][0]_INST_0_i_32_n_0\,
      S(0) => \disp[5][0]_INST_0_i_33_n_0\
    );
\disp[5][0]_INST_0_i_70\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_66_n_4\,
      I1 => \disp[5][0]_INST_0_i_61_n_7\,
      I2 => \disp[5][0]_INST_0_i_60_n_6\,
      O => \disp[5][0]_INST_0_i_70_n_0\
    );
\disp[5][0]_INST_0_i_71\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_73_n_7\,
      I1 => \disp[5][0]_INST_0_i_61_n_4\,
      I2 => \disp[5][0]_INST_0_i_62_n_5\,
      O => \disp[5][0]_INST_0_i_71_n_0\
    );
\disp[5][0]_INST_0_i_72\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_61_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_72_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_72_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_72_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_72_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_189_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_190_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_191_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_192_n_0\,
      O(3) => \disp[5][0]_INST_0_i_72_n_4\,
      O(2) => \disp[5][0]_INST_0_i_72_n_5\,
      O(1) => \disp[5][0]_INST_0_i_72_n_6\,
      O(0) => \disp[5][0]_INST_0_i_72_n_7\,
      S(3) => \disp[5][0]_INST_0_i_193_n_0\,
      S(2) => \disp[5][0]_INST_0_i_194_n_0\,
      S(1) => \disp[5][0]_INST_0_i_195_n_0\,
      S(0) => \disp[5][0]_INST_0_i_196_n_0\
    );
\disp[5][0]_INST_0_i_73\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_60_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_73_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_73_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_73_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_73_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_197_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_198_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_199_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_200_n_0\,
      O(3) => \disp[5][0]_INST_0_i_73_n_4\,
      O(2) => \disp[5][0]_INST_0_i_73_n_5\,
      O(1) => \disp[5][0]_INST_0_i_73_n_6\,
      O(0) => \disp[5][0]_INST_0_i_73_n_7\,
      S(3) => \disp[5][0]_INST_0_i_201_n_0\,
      S(2) => \disp[5][0]_INST_0_i_202_n_0\,
      S(1) => \disp[5][0]_INST_0_i_203_n_0\,
      S(0) => \disp[5][0]_INST_0_i_204_n_0\
    );
\disp[5][0]_INST_0_i_74\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_60_n_4\,
      I1 => \disp[5][0]_INST_0_i_61_n_5\,
      I2 => \disp[5][0]_INST_0_i_62_n_6\,
      O => \disp[5][0]_INST_0_i_74_n_0\
    );
\disp[5][0]_INST_0_i_75\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_60_n_5\,
      I1 => \disp[5][0]_INST_0_i_61_n_6\,
      I2 => \disp[5][0]_INST_0_i_62_n_7\,
      O => \disp[5][0]_INST_0_i_75_n_0\
    );
\disp[5][0]_INST_0_i_76\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"17"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_60_n_6\,
      I1 => \disp[5][0]_INST_0_i_61_n_7\,
      I2 => \disp[5][0]_INST_0_i_66_n_4\,
      O => \disp[5][0]_INST_0_i_76_n_0\
    );
\disp[5][0]_INST_0_i_77\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_205_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_77_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_77_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_77_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_77_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_206_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_207_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_208_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_209_n_0\,
      O(3 downto 0) => \NLW_disp[5][0]_INST_0_i_77_O_UNCONNECTED\(3 downto 0),
      S(3) => \disp[5][0]_INST_0_i_210_n_0\,
      S(2) => \disp[5][0]_INST_0_i_211_n_0\,
      S(1) => \disp[5][0]_INST_0_i_212_n_0\,
      S(0) => \disp[5][0]_INST_0_i_213_n_0\
    );
\disp[5][0]_INST_0_i_78\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_40_n_4\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(24),
      O => \disp[5][0]_INST_0_i_78_n_0\
    );
\disp[5][0]_INST_0_i_79\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_40_n_5\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(23),
      O => \disp[5][0]_INST_0_i_79_n_0\
    );
\disp[5][0]_INST_0_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_5_n_7\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(29),
      O => \disp[5][0]_INST_0_i_8_n_0\
    );
\disp[5][0]_INST_0_i_80\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_40_n_6\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(22),
      O => \disp[5][0]_INST_0_i_80_n_0\
    );
\disp[5][0]_INST_0_i_81\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_40_n_7\,
      I1 => \disp[5][0]_INST_0_i_4_n_3\,
      I2 => \inst_int_to_string/euros3\(21),
      O => \disp[5][0]_INST_0_i_81_n_0\
    );
\disp[5][0]_INST_0_i_82\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(24),
      I1 => \disp[5][0]_INST_0_i_40_n_4\,
      I2 => \inst_int_to_string/euros3\(25),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_12_n_7\,
      O => \disp[5][0]_INST_0_i_82_n_0\
    );
\disp[5][0]_INST_0_i_83\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(23),
      I1 => \disp[5][0]_INST_0_i_40_n_5\,
      I2 => \inst_int_to_string/euros3\(24),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_40_n_4\,
      O => \disp[5][0]_INST_0_i_83_n_0\
    );
\disp[5][0]_INST_0_i_84\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(22),
      I1 => \disp[5][0]_INST_0_i_40_n_6\,
      I2 => \inst_int_to_string/euros3\(23),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_40_n_5\,
      O => \disp[5][0]_INST_0_i_84_n_0\
    );
\disp[5][0]_INST_0_i_85\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(21),
      I1 => \disp[5][0]_INST_0_i_40_n_7\,
      I2 => \inst_int_to_string/euros3\(22),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_40_n_6\,
      O => \disp[5][0]_INST_0_i_85_n_0\
    );
\disp[5][0]_INST_0_i_86\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_214_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_86_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_86_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_86_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_86_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \inst_int_to_string/euros3\(20 downto 17),
      S(3) => \disp[5][0]_INST_0_i_4_n_3\,
      S(2) => \disp[5][0]_INST_0_i_4_n_3\,
      S(1) => \disp[5][0]_INST_0_i_4_n_3\,
      S(0) => \disp[5][0]_INST_0_i_4_n_3\
    );
\disp[5][0]_INST_0_i_87\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A956"
    )
        port map (
      I0 => total_out(3),
      I1 => \disp[2][0]_INST_0_i_15_n_5\,
      I2 => \disp[2][0]_INST_0_i_16_n_0\,
      I3 => \disp[2][0]_INST_0_i_15_n_4\,
      O => \disp[5][0]_INST_0_i_87_n_0\
    );
\disp[5][0]_INST_0_i_88\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => total_out(2),
      I1 => \disp[2][0]_INST_0_i_16_n_0\,
      I2 => \disp[2][0]_INST_0_i_15_n_5\,
      O => \disp[5][0]_INST_0_i_88_n_0\
    );
\disp[5][0]_INST_0_i_89\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_out(1),
      I1 => \inst_int_to_string/p_1_in\(1),
      O => \disp[5][0]_INST_0_i_89_n_0\
    );
\disp[5][0]_INST_0_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCB4334B"
    )
        port map (
      I0 => \inst_int_to_string/euros3\(29),
      I1 => \disp[5][0]_INST_0_i_5_n_7\,
      I2 => \inst_int_to_string/euros3\(30),
      I3 => \disp[5][0]_INST_0_i_4_n_3\,
      I4 => \disp[5][0]_INST_0_i_5_n_6\,
      O => \disp[5][0]_INST_0_i_9_n_0\
    );
\disp[5][0]_INST_0_i_90\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => total_out(0),
      I1 => \inst_int_to_string/p_1_in\(0),
      O => \disp[5][0]_INST_0_i_90_n_0\
    );
\disp[5][0]_INST_0_i_91\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222CCCCCCCCC"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_93_n_4\,
      I1 => \disp[5][0]_INST_0_i_93_n_5\,
      I2 => \disp[2][0]_INST_0_i_15_n_5\,
      I3 => \disp[2][0]_INST_0_i_15_n_4\,
      I4 => \disp[5][0]_INST_0_i_93_n_7\,
      I5 => \disp[5][0]_INST_0_i_93_n_6\,
      O => \disp[5][0]_INST_0_i_91_n_0\
    );
\disp[5][0]_INST_0_i_92\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"555655560002AAAA"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_93_n_6\,
      I1 => \disp[5][0]_INST_0_i_93_n_7\,
      I2 => \disp[2][0]_INST_0_i_15_n_4\,
      I3 => \disp[2][0]_INST_0_i_15_n_5\,
      I4 => \disp[5][0]_INST_0_i_93_n_5\,
      I5 => \disp[5][0]_INST_0_i_93_n_4\,
      O => \disp[5][0]_INST_0_i_92_n_0\
    );
\disp[5][0]_INST_0_i_93\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[2][0]_INST_0_i_15_n_0\,
      CO(3) => \NLW_disp[5][0]_INST_0_i_93_CO_UNCONNECTED\(3),
      CO(2) => \disp[5][0]_INST_0_i_93_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_93_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_93_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => total_out(6 downto 4),
      O(3) => \disp[5][0]_INST_0_i_93_n_4\,
      O(2) => \disp[5][0]_INST_0_i_93_n_5\,
      O(1) => \disp[5][0]_INST_0_i_93_n_6\,
      O(0) => \disp[5][0]_INST_0_i_93_n_7\,
      S(3) => \disp[5][0]_INST_0_i_215_n_0\,
      S(2) => \disp[5][0]_INST_0_i_216_n_0\,
      S(1) => \disp[5][0]_INST_0_i_217_n_0\,
      S(0) => \disp[5][0]_INST_0_i_218_n_0\
    );
\disp[5][0]_INST_0_i_94\: unisim.vcomponents.CARRY4
     port map (
      CI => \disp[5][0]_INST_0_i_219_n_0\,
      CO(3) => \disp[5][0]_INST_0_i_94_n_0\,
      CO(2) => \disp[5][0]_INST_0_i_94_n_1\,
      CO(1) => \disp[5][0]_INST_0_i_94_n_2\,
      CO(0) => \disp[5][0]_INST_0_i_94_n_3\,
      CYINIT => '0',
      DI(3) => \disp[5][0]_INST_0_i_220_n_0\,
      DI(2) => \disp[5][0]_INST_0_i_221_n_0\,
      DI(1) => \disp[5][0]_INST_0_i_222_n_0\,
      DI(0) => \disp[5][0]_INST_0_i_223_n_0\,
      O(3) => \disp[5][0]_INST_0_i_94_n_4\,
      O(2) => \disp[5][0]_INST_0_i_94_n_5\,
      O(1) => \disp[5][0]_INST_0_i_94_n_6\,
      O(0) => \disp[5][0]_INST_0_i_94_n_7\,
      S(3) => \disp[5][0]_INST_0_i_224_n_0\,
      S(2) => \disp[5][0]_INST_0_i_225_n_0\,
      S(1) => \disp[5][0]_INST_0_i_226_n_0\,
      S(0) => \disp[5][0]_INST_0_i_227_n_0\
    );
\disp[5][0]_INST_0_i_95\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_103_n_7\,
      I1 => \disp[5][0]_INST_0_i_103_n_5\,
      I2 => \disp[5][0]_INST_0_i_49_n_6\,
      O => \disp[5][0]_INST_0_i_95_n_0\
    );
\disp[5][0]_INST_0_i_96\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_228_n_4\,
      I1 => \disp[5][0]_INST_0_i_103_n_6\,
      I2 => \disp[5][0]_INST_0_i_49_n_7\,
      O => \disp[5][0]_INST_0_i_96_n_0\
    );
\disp[5][0]_INST_0_i_97\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_228_n_5\,
      I1 => \disp[5][0]_INST_0_i_103_n_7\,
      I2 => \disp[5][0]_INST_0_i_103_n_4\,
      O => \disp[5][0]_INST_0_i_97_n_0\
    );
\disp[5][0]_INST_0_i_98\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B2"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_228_n_6\,
      I1 => \disp[5][0]_INST_0_i_228_n_4\,
      I2 => \disp[5][0]_INST_0_i_103_n_5\,
      O => \disp[5][0]_INST_0_i_98_n_0\
    );
\disp[5][0]_INST_0_i_99\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9669"
    )
        port map (
      I0 => \disp[5][0]_INST_0_i_103_n_6\,
      I1 => \disp[5][0]_INST_0_i_103_n_4\,
      I2 => \disp[5][0]_INST_0_i_49_n_5\,
      I3 => \disp[5][0]_INST_0_i_95_n_0\,
      O => \disp[5][0]_INST_0_i_99_n_0\
    );
\disp[5][1]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[5]\(1)
    );
\disp[5][2]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[5]\(2)
    );
\disp[5][3]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[5]\(3)
    );
\disp[5][4]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => \disp[1]_OBUF\(2),
      O => \disp[5]\(4)
    );
\disp[5][4]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFE000000"
    )
        port map (
      I0 => total_out(2),
      I1 => total_out(4),
      I2 => total_out(3),
      I3 => total_out(6),
      I4 => total_out(5),
      I5 => total_out(7),
      O => \disp[1]_OBUF\(2)
    );
\disp[5][5]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[5]\(5)
    );
\disp[5][6]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[5]\(6)
    );
\disp[5][7]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[5]\(7)
    );
\disp[6][0]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[6]\(0)
    );
\disp[6][1]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[6]\(1)
    );
\disp[6][2]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[6]\(2)
    );
\disp[6][3]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[6]\(3)
    );
\disp[6][4]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[6]\(4)
    );
\disp[6][5]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[6]\(5)
    );
\disp[6][6]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[6]\(6)
    );
\disp[6][7]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[6]\(7)
    );
\disp[7][0]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[7]\(0)
    );
\disp[7][1]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[7]\(1)
    );
\disp[7][2]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[7]\(2)
    );
\disp[7][3]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[7]\(3)
    );
\disp[7][4]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[7]\(4)
    );
\disp[7][5]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[7]\(5)
    );
\disp[7][6]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[7]\(6)
    );
\disp[7][7]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[7]\(7)
    );
\disp[8][0]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[8]\(0)
    );
\disp[8][1]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[8]\(1)
    );
\disp[8][2]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[8]\(2)
    );
\disp[8][3]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[8]\(3)
    );
\disp[8][4]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[8]\(4)
    );
\disp[8][5]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => \disp[8]\(5)
    );
\disp[8][6]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[8]\(6)
    );
\disp[8][7]_INST_0\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => \disp[8]\(7)
    );
refresco_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => st_OBUF(0),
      O => refresco
    );
refresco_OBUF_inst_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => st_OBUF(1),
      I1 => current_state(0),
      O => st_OBUF(0)
    );
\st_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => st_OBUF(0),
      O => st(0)
    );
\st_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => st_OBUF(1),
      O => st(1)
    );
\total[3]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => current_state(0),
      I1 => valor_moneda_IBUF(3),
      I2 => st_OBUF(1),
      O => \total[3]_i_2_n_0\
    );
\total[3]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => current_state(0),
      I1 => valor_moneda_IBUF(2),
      I2 => st_OBUF(1),
      O => \total[3]_i_3_n_0\
    );
\total[3]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => current_state(0),
      I1 => valor_moneda_IBUF(1),
      I2 => st_OBUF(1),
      O => \total[3]_i_4_n_0\
    );
\total[3]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => current_state(0),
      I1 => valor_moneda_IBUF(0),
      I2 => st_OBUF(1),
      O => \total[3]_i_5_n_0\
    );
\total[3]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0012"
    )
        port map (
      I0 => valor_moneda_IBUF(3),
      I1 => st_OBUF(1),
      I2 => total_reg(3),
      I3 => current_state(0),
      O => \total[3]_i_6_n_0\
    );
\total[3]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0012"
    )
        port map (
      I0 => valor_moneda_IBUF(2),
      I1 => st_OBUF(1),
      I2 => total_reg(2),
      I3 => current_state(0),
      O => \total[3]_i_7_n_0\
    );
\total[3]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0012"
    )
        port map (
      I0 => valor_moneda_IBUF(1),
      I1 => st_OBUF(1),
      I2 => total_reg(1),
      I3 => current_state(0),
      O => \total[3]_i_8_n_0\
    );
\total[3]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0012"
    )
        port map (
      I0 => valor_moneda_IBUF(0),
      I1 => st_OBUF(1),
      I2 => total_reg(0),
      I3 => current_state(0),
      O => \total[3]_i_9_n_0\
    );
\total[7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => current_state(0),
      I1 => valor_moneda_IBUF(6),
      I2 => st_OBUF(1),
      O => \total[7]_i_2_n_0\
    );
\total[7]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => current_state(0),
      I1 => valor_moneda_IBUF(5),
      I2 => st_OBUF(1),
      O => \total[7]_i_3_n_0\
    );
\total[7]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => current_state(0),
      I1 => valor_moneda_IBUF(4),
      I2 => st_OBUF(1),
      O => \total[7]_i_4_n_0\
    );
\total[7]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0014"
    )
        port map (
      I0 => current_state(0),
      I1 => total_reg(7),
      I2 => valor_moneda_IBUF(7),
      I3 => st_OBUF(1),
      O => \total[7]_i_5_n_0\
    );
\total[7]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0012"
    )
        port map (
      I0 => valor_moneda_IBUF(6),
      I1 => st_OBUF(1),
      I2 => total_reg(6),
      I3 => current_state(0),
      O => \total[7]_i_6_n_0\
    );
\total[7]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0012"
    )
        port map (
      I0 => valor_moneda_IBUF(5),
      I1 => st_OBUF(1),
      I2 => total_reg(5),
      I3 => current_state(0),
      O => \total[7]_i_7_n_0\
    );
\total[7]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0012"
    )
        port map (
      I0 => valor_moneda_IBUF(4),
      I1 => st_OBUF(1),
      I2 => total_reg(4),
      I3 => current_state(0),
      O => \total[7]_i_8_n_0\
    );
\total_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \FSM_sequential_current_state[1]_i_2_n_0\,
      D => \total_reg[3]_i_1_n_7\,
      Q => total_reg(0)
    );
\total_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \FSM_sequential_current_state[1]_i_2_n_0\,
      D => \total_reg[3]_i_1_n_6\,
      Q => total_reg(1)
    );
\total_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \FSM_sequential_current_state[1]_i_2_n_0\,
      D => \total_reg[3]_i_1_n_5\,
      Q => total_reg(2)
    );
\total_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \FSM_sequential_current_state[1]_i_2_n_0\,
      D => \total_reg[3]_i_1_n_4\,
      Q => total_reg(3)
    );
\total_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \total_reg[3]_i_1_n_0\,
      CO(2) => \total_reg[3]_i_1_n_1\,
      CO(1) => \total_reg[3]_i_1_n_2\,
      CO(0) => \total_reg[3]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => \total[3]_i_2_n_0\,
      DI(2) => \total[3]_i_3_n_0\,
      DI(1) => \total[3]_i_4_n_0\,
      DI(0) => \total[3]_i_5_n_0\,
      O(3) => \total_reg[3]_i_1_n_4\,
      O(2) => \total_reg[3]_i_1_n_5\,
      O(1) => \total_reg[3]_i_1_n_6\,
      O(0) => \total_reg[3]_i_1_n_7\,
      S(3) => \total[3]_i_6_n_0\,
      S(2) => \total[3]_i_7_n_0\,
      S(1) => \total[3]_i_8_n_0\,
      S(0) => \total[3]_i_9_n_0\
    );
\total_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \FSM_sequential_current_state[1]_i_2_n_0\,
      D => \total_reg[7]_i_1_n_7\,
      Q => total_reg(4)
    );
\total_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \FSM_sequential_current_state[1]_i_2_n_0\,
      D => \total_reg[7]_i_1_n_6\,
      Q => total_reg(5)
    );
\total_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \FSM_sequential_current_state[1]_i_2_n_0\,
      D => \total_reg[7]_i_1_n_5\,
      Q => total_reg(6)
    );
\total_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \FSM_sequential_current_state[1]_i_2_n_0\,
      D => \total_reg[7]_i_1_n_4\,
      Q => total_reg(7)
    );
\total_reg[7]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \total_reg[3]_i_1_n_0\,
      CO(3) => \NLW_total_reg[7]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \total_reg[7]_i_1_n_1\,
      CO(1) => \total_reg[7]_i_1_n_2\,
      CO(0) => \total_reg[7]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \total[7]_i_2_n_0\,
      DI(1) => \total[7]_i_3_n_0\,
      DI(0) => \total[7]_i_4_n_0\,
      O(3) => \total_reg[7]_i_1_n_4\,
      O(2) => \total_reg[7]_i_1_n_5\,
      O(1) => \total_reg[7]_i_1_n_6\,
      O(0) => \total_reg[7]_i_1_n_7\,
      S(3) => \total[7]_i_5_n_0\,
      S(2) => \total[7]_i_6_n_0\,
      S(1) => \total[7]_i_7_n_0\,
      S(0) => \total[7]_i_8_n_0\
    );
\total_string[3]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => valor_moneda_IBUF(3),
      I1 => current_state(0),
      I2 => st_OBUF(1),
      O => \total_string[3]_i_2_n_0\
    );
\total_string[3]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => valor_moneda_IBUF(2),
      I1 => current_state(0),
      I2 => st_OBUF(1),
      O => \total_string[3]_i_3_n_0\
    );
\total_string[3]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => valor_moneda_IBUF(1),
      I1 => current_state(0),
      I2 => st_OBUF(1),
      O => \total_string[3]_i_4_n_0\
    );
\total_string[3]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => valor_moneda_IBUF(0),
      I1 => current_state(0),
      I2 => st_OBUF(1),
      O => \total_string[3]_i_5_n_0\
    );
\total_string[3]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF60006"
    )
        port map (
      I0 => valor_moneda_IBUF(3),
      I1 => total_reg(3),
      I2 => current_state(0),
      I3 => st_OBUF(1),
      I4 => total_out(3),
      O => \total_string[3]_i_6_n_0\
    );
\total_string[3]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF60006"
    )
        port map (
      I0 => valor_moneda_IBUF(2),
      I1 => total_reg(2),
      I2 => current_state(0),
      I3 => st_OBUF(1),
      I4 => total_out(2),
      O => \total_string[3]_i_7_n_0\
    );
\total_string[3]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF60006"
    )
        port map (
      I0 => valor_moneda_IBUF(1),
      I1 => total_reg(1),
      I2 => current_state(0),
      I3 => st_OBUF(1),
      I4 => total_out(1),
      O => \total_string[3]_i_8_n_0\
    );
\total_string[3]_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF60006"
    )
        port map (
      I0 => valor_moneda_IBUF(0),
      I1 => total_reg(0),
      I2 => current_state(0),
      I3 => st_OBUF(1),
      I4 => total_out(0),
      O => \total_string[3]_i_9_n_0\
    );
\total_string[7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => valor_moneda_IBUF(6),
      I1 => current_state(0),
      I2 => st_OBUF(1),
      O => \total_string[7]_i_2_n_0\
    );
\total_string[7]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => valor_moneda_IBUF(5),
      I1 => current_state(0),
      I2 => st_OBUF(1),
      O => \total_string[7]_i_3_n_0\
    );
\total_string[7]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => valor_moneda_IBUF(4),
      I1 => current_state(0),
      I2 => st_OBUF(1),
      O => \total_string[7]_i_4_n_0\
    );
\total_string[7]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF60006"
    )
        port map (
      I0 => valor_moneda_IBUF(7),
      I1 => total_reg(7),
      I2 => current_state(0),
      I3 => st_OBUF(1),
      I4 => total_out(7),
      O => \total_string[7]_i_5_n_0\
    );
\total_string[7]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF60006"
    )
        port map (
      I0 => valor_moneda_IBUF(6),
      I1 => total_reg(6),
      I2 => current_state(0),
      I3 => st_OBUF(1),
      I4 => total_out(6),
      O => \total_string[7]_i_6_n_0\
    );
\total_string[7]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF60006"
    )
        port map (
      I0 => valor_moneda_IBUF(5),
      I1 => total_reg(5),
      I2 => current_state(0),
      I3 => st_OBUF(1),
      I4 => total_out(5),
      O => \total_string[7]_i_7_n_0\
    );
\total_string[7]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF60006"
    )
        port map (
      I0 => valor_moneda_IBUF(4),
      I1 => total_reg(4),
      I2 => current_state(0),
      I3 => st_OBUF(1),
      I4 => total_out(4),
      O => \total_string[7]_i_8_n_0\
    );
\total_string_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => RESET_IBUF,
      D => \total_string_reg[3]_i_1_n_7\,
      Q => total_out(0),
      R => '0'
    );
\total_string_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => RESET_IBUF,
      D => \total_string_reg[3]_i_1_n_6\,
      Q => total_out(1),
      R => '0'
    );
\total_string_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => RESET_IBUF,
      D => \total_string_reg[3]_i_1_n_5\,
      Q => total_out(2),
      R => '0'
    );
\total_string_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => RESET_IBUF,
      D => \total_string_reg[3]_i_1_n_4\,
      Q => total_out(3),
      R => '0'
    );
\total_string_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \total_string_reg[3]_i_1_n_0\,
      CO(2) => \total_string_reg[3]_i_1_n_1\,
      CO(1) => \total_string_reg[3]_i_1_n_2\,
      CO(0) => \total_string_reg[3]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => \total_string[3]_i_2_n_0\,
      DI(2) => \total_string[3]_i_3_n_0\,
      DI(1) => \total_string[3]_i_4_n_0\,
      DI(0) => \total_string[3]_i_5_n_0\,
      O(3) => \total_string_reg[3]_i_1_n_4\,
      O(2) => \total_string_reg[3]_i_1_n_5\,
      O(1) => \total_string_reg[3]_i_1_n_6\,
      O(0) => \total_string_reg[3]_i_1_n_7\,
      S(3) => \total_string[3]_i_6_n_0\,
      S(2) => \total_string[3]_i_7_n_0\,
      S(1) => \total_string[3]_i_8_n_0\,
      S(0) => \total_string[3]_i_9_n_0\
    );
\total_string_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => RESET_IBUF,
      D => \total_string_reg[7]_i_1_n_7\,
      Q => total_out(4),
      R => '0'
    );
\total_string_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => RESET_IBUF,
      D => \total_string_reg[7]_i_1_n_6\,
      Q => total_out(5),
      R => '0'
    );
\total_string_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => RESET_IBUF,
      D => \total_string_reg[7]_i_1_n_5\,
      Q => total_out(6),
      R => '0'
    );
\total_string_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => RESET_IBUF,
      D => \total_string_reg[7]_i_1_n_4\,
      Q => total_out(7),
      R => '0'
    );
\total_string_reg[7]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \total_string_reg[3]_i_1_n_0\,
      CO(3) => \NLW_total_string_reg[7]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \total_string_reg[7]_i_1_n_1\,
      CO(1) => \total_string_reg[7]_i_1_n_2\,
      CO(0) => \total_string_reg[7]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \total_string[7]_i_2_n_0\,
      DI(1) => \total_string[7]_i_3_n_0\,
      DI(0) => \total_string[7]_i_4_n_0\,
      O(3) => \total_string_reg[7]_i_1_n_4\,
      O(2) => \total_string_reg[7]_i_1_n_5\,
      O(1) => \total_string_reg[7]_i_1_n_6\,
      O(0) => \total_string_reg[7]_i_1_n_7\,
      S(3) => \total_string[7]_i_5_n_0\,
      S(2) => \total_string[7]_i_6_n_0\,
      S(1) => \total_string[7]_i_7_n_0\,
      S(0) => \total_string[7]_i_8_n_0\
    );
\valor_moneda_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(0),
      O => valor_moneda_IBUF(0)
    );
\valor_moneda_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(1),
      O => valor_moneda_IBUF(1)
    );
\valor_moneda_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(2),
      O => valor_moneda_IBUF(2)
    );
\valor_moneda_IBUF[3]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(3),
      O => valor_moneda_IBUF(3)
    );
\valor_moneda_IBUF[4]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(4),
      O => valor_moneda_IBUF(4)
    );
\valor_moneda_IBUF[5]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(5),
      O => valor_moneda_IBUF(5)
    );
\valor_moneda_IBUF[6]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(6),
      O => valor_moneda_IBUF(6)
    );
\valor_moneda_IBUF[7]_inst\: unisim.vcomponents.IBUF
     port map (
      I => valor_moneda(7),
      O => valor_moneda_IBUF(7)
    );
end STRUCTURE;
