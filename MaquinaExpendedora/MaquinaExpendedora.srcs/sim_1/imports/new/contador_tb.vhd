library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity contador_tb is
end contador_tb;

architecture Behavioral of contador_tb is
    constant counter_size: integer:=3;
    constant num_dig: integer := 8;
    component contador is
        generic(
            size: integer;
            max: integer
        );
        
        Port(
            clr_n: in std_logic;
            clk:   in std_logic;
            count: out std_logic_vector(size-1 downto 0)
        );
    end component;
    signal clr_n: std_logic := '1';
    signal clk:   std_logic := '0';
    signal count: std_logic_vector(counter_size-1 downto 0);
    
    constant k: time := 10 ns;
begin
uut: contador
    Generic map(
        size => counter_size,
        max => num_dig
    )
    Port map(
        clr_n => clr_n,
        clk => clk,
        count => count
        );
    
    clk <= not clk after 0.5*k;
    clr_n <= not clr_n after 10*k;
    
    process
    begin
        wait until rising_edge(clk);
        wait for 22*k;
        wait for 5*k;
        wait for k;
        wait for 4*k;
        wait for 10*k;
        wait for k;
        wait for 10*k;
        
        ASSERT FALSE
        REPORT "Simlación finalizada."
        SEVERITY FAILURE;
    end process;
    
end Behavioral;
