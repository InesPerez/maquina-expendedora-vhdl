library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity prescaler_tb is
--  Port ( );
end prescaler_tb;

architecture Behavioral of prescaler_tb is
component prescaler is
    Generic(
        size_counter: integer := 17;
        digit_number: integer := 125000
    );
    Port(
        clk: in std_logic;
        clk_prescaler: out std_logic
    );
end component;

signal clk: std_logic := '0';
signal clk_prescaler: std_logic;

constant k: time := 5 ns;
constant kk: time := 1 ms;
begin
uut: prescaler Port Map(
    clk => clk,
    clk_prescaler => clk_prescaler
);

clk <= not clk after k;

process
begin
wait for 32*kk;
ASSERT FALSE
        REPORT "Simlación finalizada."
        SEVERITY FAILURE;
end process;
end Behavioral;
