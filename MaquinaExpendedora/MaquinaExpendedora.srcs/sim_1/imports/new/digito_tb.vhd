library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity digito_tb is
end digito_tb;

architecture Behavioral of digito_tb is

component digito is
    Port( letra: in character;
          digit: out std_logic_vector(6 downto 0)
    );
end component;

signal letra: character;
signal digit: std_logic_vector(6 downto 0);

type abecedario is array (0 to 36) of character;
signal abc:abecedario:="abcdefghijklmn�opqrstuvwxyz0123456789";
begin
    uut: digito Port map(
        letra => letra,
        digit => digit
    );
    
    process
    begin
            letra <= ' ';
            wait for 30 ns;
        for i in 0 to abc'high loop
            letra <= abc(i);
            wait for 10 ns;
            ASSERT not (digit = "1111110")
            REPORT "Caracter incorrecto: " & letra
            SEVERITY WARNING;
        end loop;
        
        ASSERT FALSE
            REPORT "Simulaci�n finalizada"
            SEVERITY FAILURE;
    end process;
end Behavioral;
