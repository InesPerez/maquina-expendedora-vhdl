library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity display_tb is
end display_tb;

architecture Behavioral of display_tb is
    constant width_word: integer := 8;
    constant digit_number: integer := 8; --numero de digitos de la placa
    component display is
        generic(
            width_word: integer;
            digit_number: integer --numero de digitos de la placa
        );
        Port( disp:  in string (width_word downto 1);--palabra a mostrar
              reset: in std_logic;
              clk:   in std_logic;
              digit: out std_logic_vector (6 downto 0);--segmentos del digito
              dot:   out std_logic;--punto
              elem:  out std_logic_vector (digit_number-1 downto 0);--8 digitos placa
              letra: out character
              );
    end component;
    signal disp:  string (width_word downto 1);--palabra a mostrar
    signal reset: std_logic := '1';
    signal clk:   std_logic := '0';
    signal digit: std_logic_vector (6 downto 0);--segmentos del digito
    signal dot:   std_logic;--punto
    signal elem:  std_logic_vector (digit_number-1 downto 0);--8 digitos placa
    signal letra: character;
    
    constant k: time := 2.5 ms;--periodo para 400Hz
begin
uut: display
    Generic map(
        width_word => width_word,
        digit_number => digit_number
    )
    Port map(
     disp => disp,
     reset => reset,
     clk => clk,
     digit => digit,
     dot => dot,
     elem => elem,
     letra => letra
    );
    
    clk <= not clk after 0.5*k;
    
    process
    begin
        wait until rising_edge(clk);
        disp <= "cocacola";
        wait for 19*k;
        disp <= "    20.c";
        wait for 24*k;
        disp <= "  nestea";
        wait for 40*k;
        disp <= "fanta n.";
        wait for 20*k;
        
        ASSERT FALSE
        REPORT "Simlación finalizada."
        SEVERITY FAILURE;
    end process;

end Behavioral;
