library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decoder_3x8_n_tb is
end decoder_3x8_n_tb;

architecture Behavioral of decoder_3x8_n_tb is
component decoder_3x8_n is
    Port(
        input:  in std_logic_vector(2 downto 0);
        ce:     in std_logic;
        output: out std_logic_vector(7 downto 0)
    );
end component;

signal input:  std_logic_vector(2 downto 0);
signal ce:     std_logic := '1';
signal output: std_logic_vector(7 downto 0);

constant k: time := 10 ns;
begin
uut: decoder_3x8_n
    Port map(
        input => input,
        ce => ce,
        output => output
    );
ce <= not ce after 5*k;
process
begin
    input <= "000";
    wait for 3*k;
    input <= "101";
    wait for 5*k;
    input <= "010";
    wait for 7*k;
    ASSERT false
    REPORT "Simulacin finalizada. Test superado."
    SEVERITY FAILURE;
end process;

end Behavioral;
