library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity gestion_dinero_tb is
end gestion_dinero_tb;

architecture behavioral of gestion_dinero_tb is
    constant digit_number: integer := 8; --numero de digitos de la placa

    signal reset: std_logic := '1';
    signal clk:   std_logic := '0';
    signal valor_moneda: natural := 0;
    signal button_ok: std_logic := '0';
    signal devolver:   std_logic;
    signal refresco:   std_logic;
    signal disp: string (digit_number downto 1);
    signal bdf: std_logic;
    
    constant k: time := 10 ns;--periodo para 100MHz
    
    component gestion_dinero is
    generic(
        width_word: integer := digit_number
    );
    port (
        RESET:        in std_logic;
        clk:          in std_logic;
        valor_moneda: in natural;
        button_ok:    in std_logic;
        devolver:     out std_logic;
        refresco:     out std_logic;
        disp:         out string (width_word downto 1);
        bdf:          out std_logic
    );
    end component;
    
begin

uut: gestion_dinero
    port map (
        RESET=>reset,
        clk=>clk,
        valor_moneda=>valor_moneda,
        disp=>disp,
        devolver=>devolver,
        refresco=>refresco,
        button_ok=>button_ok,
        bdf=>bdf
    );
    
    clk<=not clk after 0.5*k;
    
    process
    begin
        valor_moneda<=5;
        wait for k;
        valor_moneda<=0;
        wait for 3*k;        
        valor_moneda<=50;
        wait for k;
        valor_moneda<=0;
        wait for 3*k;
        valor_moneda<=100;
        wait for k;
        valor_moneda<=0;
        wait for 3*k;
        valor_moneda<=20;
        wait for 3*k;
        valor_moneda<=0;
        wait for 10*k;
        button_ok <= '1';
        wait for 3*k;
        button_ok <= '0';
        wait for 10*k;
        valor_moneda<=100;
        wait for 2*k;
        valor_moneda<=0;
        wait for 10*k;
        button_ok <= '1';
        wait for 10*k;
        ASSERT FALSE
        REPORT "Simlación finalizada."
        SEVERITY FAILURE;
    end process;

end behavioral;

