library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decod_monedas_tb is
end decod_monedas_tb;

architecture behavioral of decod_monedas_tb is

signal clk:   std_logic := '0';
signal monedas: std_logic_vector (4 downto 0);
signal valor : natural;

constant k: time := 10 ns;--periodo para 100MHz

component decod_monedas is
    port (
        clk : in std_logic;
        monedas : in std_logic_vector(4 downto 0);
        valor : out natural range 0 to 100:=0
     );
end component;

begin

uut: decod_monedas
    port map(
    clk=>clk,
    monedas=>monedas,
    valor=>valor
    );
    
clk<=not clk after 0.5*k;
    
process
    begin
        monedas<="00000";
        wait for k;
        monedas<="00001";
        wait for 10*k;
        monedas<="00100";
        wait for 10*k;
        monedas<="10000";
        wait for 10*k;
        monedas<="00100";
        wait for 50*k;
        ASSERT FALSE
        REPORT "Simlación finalizada."
        SEVERITY FAILURE;
    end process;

end behavioral;
