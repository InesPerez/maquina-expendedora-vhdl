library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity top_synt_tb is
end top_synt_tb;

architecture behavioral of top_synt_tb is
    constant digit_number: integer := 8; --numero de digitos de la placa
    constant num_refrescos: integer := 4;
    constant width_word: integer := 8;
    constant monedas_types: integer := 5;

    signal reset: std_logic := '1';
    signal clk:   std_logic := '0';
    signal digit: std_logic_vector (6 downto 0);--segmentos del digito
    signal dot:   std_logic;--punto
    signal elem:  std_logic_vector (digit_number-1 downto 0);--8 digitos placa
    signal devolver:   std_logic;
    signal refresco:   std_logic;
    signal monedas: std_logic_vector (4 downto 0);
    signal button_ok: std_logic := '0';
    signal ref_option:  std_logic_vector(num_refrescos-1 downto 0) := (others => '0');
    
    constant kk: time := 10 ns;--periodo para 100MHz
    constant k: time := 2.5 ms;--periodo 400Hz
    component top is
        port (
            clk_100Mh: in std_logic;
            monedas : in std_logic_vector (4 downto 0);
            ref_option:  in std_logic_vector(num_refrescos-1 downto 0);
            button_ok: in std_logic;
            reset : in std_logic;
            devolver: out std_logic;
            refresco : out std_logic := '0';
            digit: out std_logic_vector (6 downto 0);--segmentos del digito
            dot:   out std_logic := '1';--punto
            elem:  out std_logic_vector (digit_number-1 downto 0)--8 digitos placa
        );
        end component;
        
begin

uut: top
    port map(
        clk_100Mh=>clk,
        monedas=>monedas,
        reset=>reset,
        ref_option=>ref_option,
        button_ok=>button_ok,
        devolver=>devolver,
        refresco=>refresco,
        digit=>digit,
        dot=>dot,
        elem=>elem
    );
    
    clk<=not clk after 0.5*kk;

refrescos: process
    begin
        --wait for 10*k;
        --ref_option <= "1000";
        --wait for 5*k;
        --ref_option <= "0100";
        --wait for 5*k;
        --ref_option <= "0000";
        --wait for 4*k;
        ref_option <= "0010";
        wait for k;
        ref_option <= "0000";
        wait for 6*k;
        button_ok <= '1';
        wait for k;
        button_ok <= '0';
        wait for 30*k;
    end process;
    
dinero: process
    begin
        monedas<="00000";
        wait until button_ok'event and button_ok ='0';
        wait for 3*k;
        monedas<="00001";
        wait for k;
        monedas<="00000";
        wait for 10*k;        
        monedas<="01000";
        wait for k;
        monedas<="00000";
        wait for 10*k;
        monedas<="10000";
        wait for k;
        monedas<="00000";
        wait for 10*k;
        reset <= '0';
        wait for 2*k;
        reset <='1';
        wait for k;
        wait until button_ok='1';
        monedas<="00100";
        wait for k;
        monedas<="00000";
        wait until button_ok ='1';
        monedas<="10000";
        wait for k;
        monedas<="00000";
        --wait until refresco = '1';
       -- wait for k;
        --wait until button_ok ='1';
        wait for 10*k;
    end process;

    process
    begin
        wait for 1.5 sec;
        ASSERT FALSE
        REPORT "Simlación finalizada."
        SEVERITY FAILURE;    
    end process;
end behavioral;