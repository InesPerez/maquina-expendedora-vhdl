library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity fsm_tb is
end fsm_tb;

architecture behavioral of fsm_tb is
    constant width_word: integer := 8;
    constant num_refrescos: integer :=4; --numero de digitos de la placa

    signal reset:        std_logic := '1';
    signal clk:          std_logic := '0';
    signal valor_moneda: natural := 0;
    signal total:        natural;
    signal button_ok:    std_logic;
    signal devolver:     std_logic;
    signal refresco:     std_logic;
    signal ref_option:   std_logic_vector(num_refrescos-1 downto 0) := (others => '0');
    signal disp:         string(width_word downto 1);
    
    constant k: time := 2.5 ms;--periodo para 400Hz
    
    component fsm is
    generic(
        width_word: integer := width_word;
        num_refrescos: integer := num_refrescos
    );
    port (
        RESET : in std_logic;
        clk : in std_logic;
        valor_moneda : in natural;
        button_ok: in std_logic;
        ref_option:  in std_logic_vector(num_refrescos-1 downto 0);
        devolver : out std_logic;
        refresco : out std_logic;
        disp:      out string(width_word downto 1)
    );
    end component;
    
begin

uut: fsm
    port map (
        RESET=>reset,
        clk=>clk,
        valor_moneda=>valor_moneda,
        devolver=>devolver,
        refresco=>refresco,
        button_ok=>button_ok,
        disp=>disp,
        ref_option=>ref_option
    );
    
    clk<=not clk after 0.5*k;
    
refrescos: process
    begin
        wait for 10*k;
        ref_option <= "1000";
        wait for 5*k;
        ref_option <= "0100";
        wait for 5*k;
        ref_option <= "0000";
        wait for 4*k;
        ref_option <= "0010";
        wait for 4*k;
        ref_option <= "0001";
        wait for 6*k;
        button_ok <= '1';
        wait for 4*k;
        button_ok <= '0';
        wait for 4*k;
    end process;
    
dinero: process
    begin
        valor_moneda<=0;
        wait until button_ok'event and button_ok ='0';
        valor_moneda<=0;
        wait for 1.75*k;
        valor_moneda<=5;
        wait for k;
        valor_moneda<=0;
        wait for 3*k;        
        valor_moneda<=50;
        wait for k;
        valor_moneda<=0;
        wait for 3*k;
        valor_moneda<=100;
        wait for k;
        valor_moneda<=0;
        wait for 3*k;
        valor_moneda<=20;
        wait for 3*k;
        valor_moneda<=0;
        wait for 10*k;
        wait until button_ok'event and button_ok ='0';
        valor_moneda<=100;
        wait for k;
        valor_moneda<=0;
        wait until refresco = '1';
        wait for k;
        wait until button_ok ='1';
        wait for 10*k;
    end process;

process
begin
    wait for 1 sec;
    ASSERT FALSE
    REPORT "Simlación finalizada."
    SEVERITY FAILURE;    
end process;
end behavioral;
