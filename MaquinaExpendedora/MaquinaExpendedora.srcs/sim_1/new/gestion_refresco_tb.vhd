library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity gestion_refresco_tb is
end gestion_refresco_tb;

architecture Behavioral of gestion_refresco_tb is
    constant width_word:    integer := 8;
    constant num_refrescos: integer := 4;
    constant k:             time    := 2.5 ms; --periodo 400Hz
component gestion_refresco is
    generic(
        width_word: integer := width_word;
        num_refrescos: integer := num_refrescos
    );

    port(
        clk :      in std_logic;
        refresco:  in std_logic_vector(num_refrescos-1 downto 0);
        disp:      out string(width_word downto 1)
    );
end component;

signal clk:       std_logic := '0';
signal refresco:  std_logic_vector(num_refrescos-1 downto 0);
signal disp:      string(width_word downto 1);

begin
uut: gestion_refresco
    generic map(
        width_word => width_word,
        num_refrescos => num_refrescos
    )
    port map(
        clk => clk,
        refresco => refresco,
        disp => disp
    );
    
    clk <= not clk after 0.5*k;
    
    process
    begin
        wait for 10*k;
        refresco <= "1000";
        wait for 3*k;
        refresco <= "0100";
        wait for 5*k;
        refresco <= "0000";
        wait for 4*k;
        refresco <= "0010";
        wait for 4*k;
        wait for 4*k;
        refresco <= "0001";
        wait for 6*k;
        ASSERT FALSE
        REPORT "Simlación finalizada."
        SEVERITY FAILURE;
    end process;

end Behavioral;
