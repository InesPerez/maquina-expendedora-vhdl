library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity detectorflancos_tb is
end detectorflancos_tb;

architecture behavioral of detectorflancos_tb is

constant edge_1_size: integer := 5;
constant edge_2_size: integer := 4;

signal clk : std_logic:='0';
signal sync_in : std_logic_vector(edge_1_size+edge_2_size downto 0);
signal edge_1_out : std_logic_vector(edge_1_size-1 downto 0);
signal edge_2_out : std_logic_vector(edge_2_size-1 downto 0);
signal edge_3_out : std_logic;

component detectorflancos is
    generic(
        edge_1_size: integer;
        edge_2_size: integer
    );
    port ( 
        clk : in std_logic;
        sync_in : in std_logic_vector(edge_1_size+edge_2_size downto 0);
        edge_1_out : out std_logic_vector (edge_1_size-1 downto 0);
        edge_2_out : out std_logic_vector(edge_2_size-1 downto 0);
        edge_3_out : out std_logic
    );
end component;

constant k: time := 10 ns;

begin

uut: detectorflancos
    generic map(
        edge_1_size=>edge_1_size,
        edge_2_size=>edge_2_size
    )
    port map(
        clk=>clk,
        sync_in=>sync_in,
        edge_1_out=>edge_1_out,
        edge_2_out=>edge_2_out,
        edge_3_out=>edge_3_out
    );

clk<=not clk after 0.5*k;

process
begin
    sync_in<="0000000000";
    wait for 0.75*k;
    sync_in<="0100000100";
    wait for 2*k;
    sync_in<="0000000010";
    wait for k;
    sync_in<="0000000001";
    wait for k;
    sync_in<="0000000000";
    wait for 5*k;
    ASSERT FALSE
    REPORT "Simlación finalizada."
    SEVERITY FAILURE;
end process;

end behavioral;
