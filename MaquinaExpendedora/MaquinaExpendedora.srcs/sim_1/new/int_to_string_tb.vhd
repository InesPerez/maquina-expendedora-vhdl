library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity int_to_string_tb is
end int_to_string_tb;

architecture behavioral of int_to_string_tb is

constant width_word: integer := 8;

signal total : natural;
signal display_string : string (width_word downto 1);

component int_to_string is
    generic(
        width_word: integer := width_word
    );
    port ( 
        total : in natural;
        display_string : out string (width_word downto 1)
    );
end component;

begin
uut: int_to_string
    port map(
      total=>total,
      display_string=>display_string
    );

process
begin
    total<=145;
    wait for 10 ns;
    total<=10;
    wait for 10 ns;
    total<=120;
    wait for 5 ns;
    ASSERT FALSE
    REPORT "Simlación finalizada."
    SEVERITY FAILURE;
end process;
end behavioral;
