library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity sincronizador_tb is
end sincronizador_tb;

architecture behavioral of sincronizador_tb is

constant async_1_size: integer := 5;
constant async_2_size: integer := 4;

signal clk : std_logic:='0';
signal async_1_in : std_logic_vector(async_1_size-1 downto 0);
signal async_2_in : std_logic_vector(async_2_size-1 downto 0);
signal async_3_in : std_logic;
signal sync_out : std_logic_vector (async_1_size+async_2_size downto 0);

constant k: time :=10ns;

component sincronizador is
    generic(
        async_1_size: integer;
        async_2_size: integer 
    );
    port ( 
        clk : in std_logic;
        async_1_in : in std_logic_vector (async_1_size-1 downto 0);
        async_2_in : in std_logic_vector(async_2_size-1 downto 0);
        async_3_in : in std_logic;
        sync_out : out std_logic_vector (async_1_size+async_2_size downto 0)
    );
end component;

begin
uut : sincronizador
    generic map(
        async_1_size=>async_1_size,
        async_2_size=>async_2_size
    )
    port map(
        clk=>clk,
        async_1_in=>async_1_in,
        async_2_in=>async_2_in,
        async_3_in=>async_3_in,
        sync_out=>sync_out
    );

clk<=not clk after 0.5*k;

process
begin
    async_1_in<="00000";
    async_2_in<="0000";
    async_3_in<='0';
    wait for 0.75*k;
    async_1_in<="01100";
    async_2_in<="0010";
    wait for 2*k;
    async_1_in<="00000";
    async_2_in<="0000";
    wait for k;
    async_2_in<="0001";
    wait for k;
    async_2_in<="0000";
    wait for k;
    async_3_in<='1';
    wait for k;
    async_3_in<='0';
    wait for 5*k;
    ASSERT FALSE
    REPORT "Simlación finalizada."
    SEVERITY FAILURE;
end process;
end behavioral;
