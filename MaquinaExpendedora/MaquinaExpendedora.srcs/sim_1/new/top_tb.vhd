library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity top_tb is
end top_tb;

architecture behavioral of top_tb is
    constant width_word: integer := 8;
    constant digit_number: integer := 8; --numero de digitos de la placa
    constant size_counter: integer := 3;
    constant size_prescaler: integer := 17;
    constant division_prescaler: integer := 250000;
    constant num_refrescos: integer := 4;
    constant monedas_types: integer := 5;

    signal reset: std_logic := '1';
    signal clk:   std_logic := '0';
    signal digit: std_logic_vector (6 downto 0);--segmentos del digito
    signal dot:   std_logic;--punto
    signal elem:  std_logic_vector (digit_number-1 downto 0);--8 digitos placa
    signal devolver:   std_logic;
    signal refresco:   std_logic;
    signal monedas: std_logic_vector (monedas_types-1 downto 0);
    signal button_ok: std_logic := '0';
    signal ref_option:  std_logic_vector(num_refrescos-1 downto 0);
    
    constant kk: time := 10 ns;--periodo para 100MHz
    constant k: time := 2.5 ms;--periodo 400Hz
    component top is
        generic(
            width_word: integer;
            digit_number: integer; --numero de digitos de la placa
            size_counter: integer;
            size_prescaler: integer;
            division_prescaler: integer;
            num_refrescos: integer;
            monedas_types: integer
        );
        port (
            clk_100Mh: in std_logic;
            monedas : in std_logic_vector (monedas_types-1 downto 0);
            ref_option:  in std_logic_vector(num_refrescos-1 downto 0);
            button_ok: in std_logic;
            reset : in std_logic;
            devolver: out std_logic;
            refresco : out std_logic := '0';
            digit: out std_logic_vector (6 downto 0);--segmentos del digito
            dot:   out std_logic := '1';--punto
            elem:  out std_logic_vector (digit_number-1 downto 0)--8 digitos placa
        );
        end component;
        
        signal aux: std_logic;
begin

uut: top
    generic map(
        width_word=>width_word,
        digit_number=>digit_number,
        size_counter=>size_counter,
        size_prescaler=>size_prescaler,
        division_prescaler=>division_prescaler,
        num_refrescos=>num_refrescos,
        monedas_types=>monedas_types
    )
    port map(
        clk_100Mh=>clk,
        monedas=>monedas,
        reset=>reset,
        ref_option=>ref_option,
        button_ok=>button_ok,
        devolver=>devolver,
        refresco=>refresco,
        digit=>digit,
        dot=>dot,
        elem=>elem
    );
    
    clk<=not clk after 0.5*kk;
    
refrescos: process
    begin
    wait for 3*k;
    if aux = '1' then
        ref_option <= "0000";
        button_ok <= '0';
        wait for 10*k;
        --ref_option <= "1000";--cocacola
        --wait for 4*k;
        --ref_option <= "0000";
        --wait for 10*k;
        ref_option <= "0100";--nestea
        wait for 2*k;
        ref_option <= "0000";
        --wait for 10*k;
        --ref_option <= "0010";--fanta n.
        --wait for 10*k;
        --ref_option <= "0000";
        wait for 30*k;
        --ref_option <= "0001";--agua
        --wait for 30*k;
        button_ok <= '1';
        wait for k;
        button_ok <= '0';
        wait for 20*k;
    else
        wait for 30*k;
        button_ok <= '1';
        wait for 10*k;
        button_ok <= '0';
        wait for 30*k;
    end if;
    end process;
    
dinero: process
    begin
        aux<='1';
        monedas<="00000";
        wait for k;
        wait until button_ok = '1';
        wait for 30*k;        
        monedas<="01000";
        wait for k;
        monedas<="00000";
        wait for 30*k;
        monedas<="00001";
        wait for k;
        monedas<="00000";
        wait for 30*k;
        monedas<="10000";
        wait for k;
        monedas<="00000";
        wait for 5*k;
        monedas<="00100";
        wait for k;
        monedas<="00000";
        wait until button_ok'event and button_ok ='0';
        aux <= '0';
        wait until button_ok'event and button_ok ='0';
        wait until button_ok'event and button_ok ='0';
        monedas<="10000";
        wait for k;
        monedas<="00000";
        wait until button_ok='0';
        aux<='1';
        wait until refresco = '1';
        wait until button_ok ='1';
        wait for 10*k;
    end process;

    process
    begin
        wait for 1.5 sec;
        ASSERT FALSE
        REPORT "Simlación finalizada."
        SEVERITY FAILURE;    
    end process;
    
end behavioral;
