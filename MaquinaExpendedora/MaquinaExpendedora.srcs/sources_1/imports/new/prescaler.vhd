library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity prescaler is
    Generic(
        size_counter: integer := 17;
        digit_number: integer := 250000
    );
    Port(
        clk: in std_logic;
        clk_prescaler: out std_logic
    );
end prescaler;

architecture Behavioral of prescaler is
    component contador is
        generic(
            size: integer:= size_counter;
            max: integer := digit_number --valor semiperiodo
        );
        Port(
            clr_n: in std_logic;
            clk:   in std_logic;
            count: out std_logic_vector(size-1 downto 0)
        );
    end component;
    signal count: std_logic_vector(size_counter-1 downto 0);
begin
uut: contador
    Port Map(
        clr_n => '1',
        clk => clk,
        count => count
    );
    process (clk)
    variable c: integer;
    variable clk_pre: std_logic := '0';
    begin
        c := to_integer(unsigned(count));
        if rising_edge(clk) then
            if c = digit_number/2-1 then
                clk_pre := not clk_pre;
            end if;
        end if;
    clk_prescaler <= clk_pre;
    end process;
end Behavioral;
