library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity display is
    generic(
        width_word: integer := 8;
        digit_number: integer := 8; --numero de digitos de la placa
        size_counter: integer := 3
    );
    Port( disp:  in string (width_word downto 1);--palabra a mostrar
          reset: in std_logic;
          clk:   in std_logic;
          digit: out std_logic_vector (6 downto 0);--segmentos del digito
          dot:   out std_logic := '1';--punto
          elem:  out std_logic_vector (digit_number-1 downto 0);--8 digitos placa
          letra: out character --salida auxiliar para simular
          );
end display;

architecture Behavioral of display is
    component digito is
        Port( letra: in character;
              digit: out std_logic_vector(6 downto 0)
              );
    end component;
    signal caracter: character;
    
    component contador is
        generic(
            size: integer:= size_counter;
            max: integer := digit_number --la placa solo tiene 8 digitos
        );
        Port(
            clr_n: in std_logic;
            clk:   in std_logic;
            count: out std_logic_vector(size-1 downto 0)
        );
    end component;
    signal elem_on: std_logic_vector(3-1 downto 0);
    
    component decoder_3x8_n is
    Port(
        input:  in std_logic_vector(3-1 downto 0);
        ce:     in std_logic;
        output: out std_logic_vector(digit_number-1 downto 0)
    );
    end component;
    
begin
    Inst_digito: digito Port map(
        letra => caracter,
        digit => digit
    );
    
    Inst_contador: contador Port map(
        clr_n => reset,
        clk => clk,
        count => elem_on
    );
    
    Inst_dec: decoder_3x8_n Port map(
        ce => reset,
        input => elem_on,
        output => elem
    );
    
update: process (clk,reset)
    variable element_enable: integer;
    variable charact_dot: integer := 0;
    begin
        element_enable := to_integer(unsigned(elem_on)+1);
        if reset = '0' then
            dot <= '1';
            caracter <= ' ';
        elsif rising_edge(clk) then
            if disp(element_enable+1)='.' then 
                caracter <= disp(element_enable+2);
                dot <= '0';
                charact_dot :=1;
             else
                 if (element_enable+charact_dot) < digit_number then 
                    caracter <= disp(element_enable+1+charact_dot);
                    dot <= '1';
                 else
                    charact_dot :=0;
                    caracter <= ' ';
                 end if;
            end if;
        end if;
    end process;
    
    letra <= caracter;--simulación
end Behavioral;


