library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity detectorflancos is
    generic(
        edge_1_size: integer := 5;
        edge_2_size: integer := 4
    );
    port ( 
        clk : in std_logic;
        sync_in : in std_logic_vector(edge_1_size+edge_2_size downto 0);
        edge_1_out : out std_logic_vector (edge_1_size-1 downto 0);
        edge_2_out : out std_logic_vector(edge_2_size-1 downto 0);
        edge_3_out : out std_logic
    );
end;

architecture structural of detectorflancos is
 
    component edgedtctr
        port(
            clk : in std_logic;
            sync_in : in std_logic;
            edge : out std_logic
        );
    end component;
        
begin

    inst_edgedtctr0 : edgedtctr
        port map(
            clk=>clk,
            sync_in=>sync_in(9),
            edge=>edge_1_out(4)
        );
        
    inst_edgedtctr1 : edgedtctr
        port map(
            clk=>clk,
            sync_in=>sync_in(8),
            edge=>edge_1_out(3)        
        );
        
    inst_edgedtctr2 : edgedtctr
        port map(
            clk=>clk,
            sync_in=>sync_in(7),
            edge=>edge_1_out(2)        
        );
        
    inst_edgedtctr3 : edgedtctr
        port map(
            clk=>clk,
            sync_in=>sync_in(6),
            edge=>edge_1_out(1)        
        );
        
    inst_edgedtctr4 : edgedtctr
        port map(
            clk=>clk,
            sync_in=>sync_in(5),
            edge=>edge_1_out(0)        
        );
        
    inst_edgedtctr5 : edgedtctr
        port map(
            clk=>clk,
            sync_in=>sync_in(4),
            edge=>edge_2_out(3)        
        );
        
    inst_edgedtctr6 : edgedtctr
        port map(
            clk=>clk,
            sync_in=>sync_in(3),
            edge=>edge_2_out(2)        
        );
        
    inst_edgedtctr7 : edgedtctr
        port map(
            clk=>clk,
            sync_in=>sync_in(2),
            edge=>edge_2_out(1)        
        );
        
    inst_edgedtctr8 : edgedtctr
        port map(
            clk=>clk,
            sync_in=>sync_in(1),
            edge=>edge_2_out(0)        
        );
        
    inst_edgedtctr9 : edgedtctr
        port map(
            clk=>clk,
            sync_in=>sync_in(0),
            edge=>edge_3_out        
        );
end structural;