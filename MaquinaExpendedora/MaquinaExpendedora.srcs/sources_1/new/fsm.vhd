library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity fsm is
    generic(
        width_word: integer := 8;
        num_refrescos: integer :=4
    );
    port (
        RESET:        in std_logic;
        clk:          in std_logic;
        valor_moneda: in natural;
        button_ok:    in std_logic;
        ref_option:   in std_logic_vector(num_refrescos-1 downto 0);
        devolver:     out std_logic;
        refresco:     out std_logic;
        disp:         out string(width_word downto 1)
    );
end fsm;

architecture behavioral of fsm is
    component gestion_refresco is
        generic(
            width_word:    integer := width_word;
            num_refrescos: integer := num_refrescos
        );
    
        port(
            clk :      in std_logic;
            refresco:  in std_logic_vector(num_refrescos-1 downto 0);
            disp:      out string(width_word downto 1)
        );
    end component;
    
    component gestion_dinero is
        generic(
            width_word: integer := width_word
        );
        port (
            RESET:        in std_logic;
            clk:          in std_logic;
            valor_moneda: in natural;
            button_ok:    in std_logic;
            devolver:     out std_logic;
            refresco:     out std_logic;
            disp:         out string (width_word downto 1);
            bdf:   out std_logic
        );
    end component;
    
    type STATES is (S0, S1, S2, S3);
    signal current_state: STATES;
    signal next_state: STATES;
    
    signal stop: boolean;
    
    signal disp_refresco: string (width_word downto 1);
    signal disp_dinero: string (width_word downto 1);
    signal clr_dinero: std_logic;
    signal clr_dinero_r: std_logic;
    signal bdf_dinero: std_logic;
    
begin
Inst_gestion_refresco: gestion_refresco
    port map(
        clk => clk,
        refresco => ref_option,
        disp => disp_refresco
    );
Inst_gestion_dinero: gestion_dinero
    port map(
        reset => clr_dinero,
        clk => clk,
        valor_moneda => valor_moneda,
        button_ok => button_ok,
        devolver => devolver,
        refresco => refresco,
        disp => disp_dinero,
        bdf => bdf_dinero
    );

state_register: process (RESET, clk)
    begin
        if RESET='0' then
            current_state <= S0;
            clr_dinero_r <= '0';    
        elsif rising_edge(clk) then
            clr_dinero_r <= '1';
            current_state <= next_state;
        end if;
    end process;
    
nextstate_decod: process (current_state,button_ok,valor_moneda,bdf_dinero,ref_option)
variable aux: integer; 
    begin
        next_state <= current_state;
        aux := to_integer(unsigned(ref_option));
        case current_state is
        when S0 =>
            if aux /= 0 then
                next_state <= S1;
            end if;
        when S1 =>
            if button_ok = '1' then
                next_state <= S2;
            end if;
        when S2 =>
            if valor_moneda /= 0 then
                next_state <= S3;
            end if;
        when S3 =>
            if bdf_dinero='1' then
                next_state <= S0;
            end if;
        when others =>
            next_state <= S0;            
        end case;
    end process;
    
output_decod: process (current_state,disp_refresco,disp_dinero,clr_dinero_r)
    begin
        case current_state is
        when S0 =>
            disp <= "refresco";
            clr_dinero <= '0' and clr_dinero_r;
        when S1 =>
            disp <= disp_refresco;
            clr_dinero <= '0'and clr_dinero_r;
        when S2 =>
            disp <= "  dinero";
            clr_dinero <= '1' and clr_dinero_r;
        when S3 =>
            disp <= disp_dinero;
            clr_dinero <= '1' and clr_dinero_r;
        when others =>
            disp <= "xxxxxxxx";
            clr_dinero <= '1';
        end case;
    end process;
end behavioral;