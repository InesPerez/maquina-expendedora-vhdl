library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity top is
    generic(
        width_word: integer := 8;
        digit_number: integer := 8; --numero de digitos de la placa
        size_counter: integer := 3;
        size_prescaler: integer := 17;
        division_prescaler: integer := 250000;
        num_refrescos: integer := 4;
        monedas_types: integer := 5
    );
    port (
        clk_100Mh: in std_logic;
        monedas : in std_logic_vector (monedas_types-1 downto 0);
        ref_option:  in std_logic_vector(num_refrescos-1 downto 0);
        button_ok: in std_logic;
        reset : in std_logic;
        devolver: out std_logic;
        refresco : out std_logic;
        digit: out std_logic_vector (6 downto 0);--segmentos del digito
        dot:   out std_logic := '1';--punto
        elem:  out std_logic_vector (digit_number-1 downto 0)--8 digitos placa
     );
end top;

architecture structural of top is

    signal entrada_sync : std_logic_vector (num_refrescos+monedas_types downto 0);
    signal entrada_1_flancos : std_logic_vector (monedas_types-1 downto 0);
    signal entrada_2_flancos : std_logic_vector (num_refrescos-1 downto 0);
    signal entrada_3_flancos : std_logic;
    signal disp : string (width_word downto 1);
    signal total : natural;
    signal valor_moneda : natural;
    signal clk : std_logic;
    
    
    component prescaler is
    Generic(
        size_counter: integer := size_prescaler;
        digit_number: integer := division_prescaler
    );
    Port(
        clk: in std_logic;
        clk_prescaler: out std_logic
    );
    end component;

    component sincronizador
    generic(
        async_1_size: integer := monedas_types;
        async_2_size: integer := num_refrescos
    );
    port ( 
        clk : in std_logic;
        async_1_in : in std_logic_vector (async_1_size-1 downto 0);
        async_2_in : in std_logic_vector (async_2_size-1 downto 0);
        async_3_in : in std_logic;
        sync_out : out std_logic_vector (async_1_size+async_2_size downto 0)
    );
    end component;
    
    component detectorflancos
    generic(
        edge_1_size: integer := monedas_types;
        edge_2_size: integer := num_refrescos
    );
    port ( 
        clk : in std_logic;
        sync_in : in std_logic_vector(edge_1_size+edge_2_size downto 0);
        edge_1_out : out std_logic_vector (edge_1_size-1 downto 0);
        edge_2_out : out std_logic_vector(edge_2_size-1 downto 0);
        edge_3_out : out std_logic
    );
    end component;
    
    component decod_monedas
        port(
            clk : in std_logic;
            monedas : in std_logic_vector(4 downto 0);
            valor_moneda : out natural
        );
    end component;
    
    component fsm
        generic(
            width_word:    integer := width_word;
            num_refrescos: integer := num_refrescos
        );
        port (
            RESET:        in std_logic;
            clk:          in std_logic;
            valor_moneda: in natural;
            button_ok:    in std_logic;
            ref_option:   in std_logic_vector(num_refrescos-1 downto 0);
            devolver:     out std_logic;
            refresco:     out std_logic;
            disp:         out string(width_word downto 1)
        );
    end component;
    
    component display
        generic(
            digit_number: integer := digit_number; --numero de digitos de la placa
            size_counter: integer := size_counter
        );
        port( 
            disp:  in string (width_word downto 1);--palabra a mostrar
            reset: in std_logic;
            clk:   in std_logic;
            digit: out std_logic_vector (6 downto 0);--segmentos del digito
            dot:   out std_logic;--punto
            elem:  out std_logic_vector (digit_number-1 downto 0)--8 digitos placa
        );
    end component;
    
begin
    inst_prescaler : prescaler
        port map(
        clk => clk_100Mh,
        clk_prescaler => clk
        );
    inst_sincronizador : sincronizador
        port map(
        clk=>clk,
        async_1_in=>monedas,
        async_2_in=>ref_option,
        async_3_in=>button_ok,
        sync_out=>entrada_sync
        );
    inst_detectorflancos : detectorflancos
        port map(
        clk=>clk,
        sync_in=>entrada_sync,
        edge_1_out=>entrada_1_flancos,
        edge_2_out=>entrada_2_flancos,
        edge_3_out=>entrada_3_flancos
        );
    inst_decodmonedas : decod_monedas
        port map(
        clk=>clk,
        monedas=>entrada_1_flancos,
        valor_moneda=>valor_moneda
        );
    inst_fsm : fsm
        port map(
        RESET=>reset,
        clk=>clk,
        valor_moneda=>valor_moneda,
        button_ok=>entrada_3_flancos,
        ref_option=>entrada_2_flancos,
        devolver=>devolver,
        refresco=>refresco,
        disp=>disp
        );
    
    inst_display: display
        port map(
        disp=>disp,
        reset=>reset,
        clk=>clk,
        digit=>digit,
        dot=>dot,
        elem=>elem
        );
end structural;
