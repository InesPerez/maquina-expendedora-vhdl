library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity synchrnzr is
    port ( 
        clk : in std_logic;
        async_in : in std_logic;
        sync_out : out std_logic
    );
end synchrnzr;

architecture behavioral of synchrnzr is
    signal sreg : std_logic_vector(1 downto 0):="00";
begin
    process(clk)
    begin
        if rising_edge(clk) then
            sync_out <= sreg(1);
            sreg <= sreg(0) & async_in;
        end if;
    end process;
end behavioral;
