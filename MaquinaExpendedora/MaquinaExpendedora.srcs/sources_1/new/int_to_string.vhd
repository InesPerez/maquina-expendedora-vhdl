library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity int_to_string is
    generic(
        width_word: integer := 8
    );
    port ( 
        total : in natural;
        display_string : out string (width_word downto 1)
    );
end int_to_string;

architecture behavioral of int_to_string is
begin
    process(total)
        variable cent : natural :=0;
        variable cent_decenas : natural range 0 to 9:=0;
        variable cent_unidades : natural range 0 to 9:=0;
        variable euros : natural range 0 to 1:=0;
        variable string_cent_decenas : string (1 downto 1);
        variable string_cent_unidades : string (1 downto 1);
        variable string_euros : string (1 downto 1);
    begin
        if total<100 then
            cent:=total;
            euros:=0;
        else
            cent:=total mod 100;
            euros:=(total-cent)/100;
        end if;
        
        cent_unidades := cent mod 10;
        cent_decenas := ((cent mod 100) - cent_unidades)/10;
        
        case euros is
            when 0 => string_euros:="0";
            when 1 => string_euros:="1";
        end case;
        
        case cent_unidades is
            when 0 => string_cent_unidades:="0";
            when 1 => string_cent_unidades:="1";
            when 2 => string_cent_unidades:="2";
            when 3 => string_cent_unidades:="3";
            when 4 => string_cent_unidades:="4";
            when 5 => string_cent_unidades:="5";
            when 6 => string_cent_unidades:="6";
            when 7 => string_cent_unidades:="7";
            when 8 => string_cent_unidades:="8";
            when 9 => string_cent_unidades:="9";
        end case;
        
        case cent_decenas is
            when 0 => string_cent_decenas:="0";
            when 1 => string_cent_decenas:="1";
            when 2 => string_cent_decenas:="2";
            when 3 => string_cent_decenas:="3";
            when 4 => string_cent_decenas:="4";
            when 5 => string_cent_decenas:="5";
            when 6 => string_cent_decenas:="6";
            when 7 => string_cent_decenas:="7";
            when 8 => string_cent_decenas:="8";
            when 9 => string_cent_decenas:="9";
        end case;
        
        if total<100 then
            display_string<="     " & string_cent_decenas & string_cent_unidades & "c";
        else 
            display_string<="   " & string_euros & "." & string_cent_decenas & string_cent_unidades & "e";
        end if;
    end process;
end behavioral;
