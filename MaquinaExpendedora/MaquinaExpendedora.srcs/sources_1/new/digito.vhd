library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity digito is
    Port( letra: in character;
          digit: out std_logic_vector(6 downto 0)
    );
end digito;

architecture dataflow of digito is
begin
    with letra select
        digit <= "0000001" when '0' | 'd' | 'o',
                 "1001111" when '1' | 'i',
                 "0010010" when '2',
                 "0000110" when '3',
                 "1001100" when '4',
                 "0100100" when '5' | 's',
                 "0100000" when '6' | 'g',
                 "0001111" when '7',
                 "0000000" when '8',
                 "0000100" when '9',
                 "0001000" when 'a' | 'r',
                 "0110001" when 'c',
                 "0110000" when 'e',
                 "0111000" when 'f',
                 "1110001" when 'l',
                 "1001000" when 'n',
                 "1110000" when 't',
                 "1000001" when 'u',
                 "1111111" when ' ',
                 "1111110" when others;  
end dataflow;
