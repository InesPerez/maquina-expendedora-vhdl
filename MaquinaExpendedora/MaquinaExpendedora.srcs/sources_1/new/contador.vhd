library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity contador is
    generic(
        size: integer := 3;
        max: integer := 8 --la placa solo tiene 8 digitos
    );
    
    Port(
        clr_n: in std_logic;
        clk:   in std_logic;
        count: out std_logic_vector(size-1 downto 0)
    );
end contador;

architecture Behavioral of contador is
    signal reg: unsigned(size-1 downto 0) := (others => '0');
begin
    process(clk,clr_n)
    begin
        if clr_n = '0' then
            reg <= (others => '0');
        elsif rising_edge(clk) then
            if reg = max-1 then
                reg <= (others => '0');
            else
                reg <= reg +1; 
            end if;    
        end if;
    end process;
    
    count <= std_logic_vector(reg);
    
end Behavioral;
