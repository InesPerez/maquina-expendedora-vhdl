library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity decod_monedas is
    port (
        clk : in std_logic;
        monedas : in std_logic_vector(4 downto 0);
        valor_moneda : out natural
     );
end;

architecture behavioral of decod_monedas is

begin
    process(clk, monedas)
    variable valor: natural;
    begin
        if clk'event and clk='1' then
            if monedas > "0000" then
                case monedas is
                 when "00001" => valor:=5;
                 when "00010" => valor:=10;
                 when "00100" => valor:=20;
                 when "01000" => valor:=50;
                 when "10000" => valor:=100;
                 when others => valor:=0;
                end case;
                valor_moneda <= valor;
            end if;
        end if;
    end process;
end behavioral;