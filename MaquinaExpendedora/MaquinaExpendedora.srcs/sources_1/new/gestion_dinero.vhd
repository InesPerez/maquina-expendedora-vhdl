library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity gestion_dinero is
    generic(
        width_word: integer := 8
    );
    port (
        RESET:        in std_logic;
        clk:          in std_logic;
        valor_moneda: in natural;
        button_ok:    in std_logic;
        devolver:     out std_logic;
        refresco:     out std_logic;
        disp:         out string (width_word downto 1);
        bdf:          out std_logic;
        st: out std_logic_vector(1 downto 0)
    );
end gestion_dinero;

architecture behavioral of gestion_dinero is    
    type STATES is (S0, S1, S2, S3);
    signal current_state: STATES;
    signal next_state: STATES;
    signal stop: boolean;
    signal total_string: natural range 0 to 200;
    signal total: natural range 0 to 200;
    
component int_to_string
        generic(
            width_word: integer := width_word
        );
        port ( 
            total : in natural;
            display_string : out string (width_word downto 1)
        );
end component;
begin

inst_int_to_string : int_to_string
    port map(
        total=>total_string,
        display_string=>disp
    );

    state_register: process (RESET,clk)
    variable total_out: natural range 0 to 200;
    begin
        if RESET='0' then
            total <=0;
            current_state <= S0;
        elsif CLK'event and CLK='1' then
            if stop then
                total<=0;
            elsif not stop then
                total_out := total + valor_moneda;
                total <= total_out;
            end if;
            total_string <= total_out;
            current_state <= next_state;
        end if;
    end process;
    
    nextstate_decod: process (current_state,total,button_ok)
    begin
        next_state <= current_state;
        case current_state is
        when S0 =>
            if total=100 then
                next_state <= S1;
            elsif total>100 then
                next_state <= S2;
            end if;
            stop <= false;
        when S1 =>
            stop <= true;
            if button_ok='1' then
                next_state <= S3;
            end if;
        when S2 | S3 =>
            stop <= true;
            if button_ok='1' then
                next_state <= S0;
            end if;
        when others =>
            stop <= true;
            next_state <= S0;
        end case;
    end process;
    
    output_decod: process (current_state)
    begin
        case current_state is
        when S0 =>
            devolver<='0';
            refresco<='0';
            bdf<='0';
            st<="00";
        when S1 =>
            devolver<='0';
            refresco<='1';
            bdf<='0';
            st<="01";
        when S2 =>
            devolver<='1';
            refresco<='0';
            bdf<='0';
            st<="10";
        when S3 =>
            devolver<='0';
            refresco<='1';
            bdf<='1';
            st<="11";
        when others =>
            devolver<='0';
            refresco<='0';
            bdf<='0';
            st<="XX";
        end case;
    end process;
end behavioral;