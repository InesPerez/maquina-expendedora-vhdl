library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity gestion_refresco is
    generic(
        width_word: integer := 8;
        num_refrescos: integer :=4
    );

    port(
        clk :      in std_logic;
        refresco:  in std_logic_vector(num_refrescos-1 downto 0);
        disp:      out string(width_word downto 1)
    );
end gestion_refresco;

architecture behavioral of gestion_refresco is    
begin
process (clk)
    begin
        if rising_edge(clk) then
            if refresco = "1000" then
                disp <= "cocacola";
            elsif refresco = "0100" then
                disp <= "fanta n.";
            elsif refresco = "0010" then
                disp <= "  nestea";
            elsif refresco = "0001" then
                disp <= "    agua";
            end if;
        end if;
    end process;
 end behavioral;
 
 