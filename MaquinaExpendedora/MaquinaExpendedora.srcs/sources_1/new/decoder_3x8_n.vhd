library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decoder_3x8_n is
    Port(
        input:  in std_logic_vector(2 downto 0);
        ce:     in std_logic;
        output: out std_logic_vector(7 downto 0)
    );
end decoder_3x8_n;

architecture Dataflow of decoder_3x8_n is
begin
    with input select
    output <=   (0 => not ce,others => '1') when "000",
                (1 => not ce,others => '1') when "001",
                (2 => not ce,others => '1') when "010",
                (3 => not ce,others => '1') when "011",
                (4 => not ce,others => '1') when "100",
                (5 => not ce,others => '1') when "101",
                (6 => not ce,others => '1') when "110",
                (7 => not ce,others => '1') when "111",
                (others => '1') when others;
end Dataflow;
