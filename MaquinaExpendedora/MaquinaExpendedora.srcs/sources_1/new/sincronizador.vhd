library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity sincronizador is
    generic(
        async_1_size: integer := 5;
        async_2_size: integer := 4
    );
    port ( 
        clk : in std_logic;
        async_1_in : in std_logic_vector (async_1_size-1 downto 0);
        async_2_in : in std_logic_vector(async_2_size-1 downto 0);
        async_3_in : in std_logic;
        sync_out : out std_logic_vector (async_1_size+async_2_size downto 0)
    );
end;

architecture structural of sincronizador is

    component synchrnzr is
    port ( 
        clk : in std_logic;
        async_in : in std_logic;
        sync_out : out std_logic
    );
    end component;
    
begin

    inst_synchrnzr0 : synchrnzr
        port map(
            clk=>clk,
            async_in=>async_1_in(4),
            sync_out=>sync_out(9)
        );

    inst_synchrnzr1 : synchrnzr
        port map(
            clk=>clk,
            async_in=>async_1_in(3),
            sync_out=>sync_out(8)
        );

    inst_synchrnzr2 : synchrnzr
        port map(
            clk=>clk,
            async_in=>async_1_in(2),
            sync_out=>sync_out(7)
        );

    inst_synchrnzr3 : synchrnzr
        port map(
            clk=>clk,
            async_in=>async_1_in(1),
            sync_out=>sync_out(6)
        );

    inst_synchrnzr4 : synchrnzr
        port map(
            clk=>clk,
            async_in=>async_1_in(0),
            sync_out=>sync_out(5)
        );
        
    inst_synchrnzr5 : synchrnzr
        port map(
            clk=>clk,
            async_in=>async_2_in(3),
            sync_out=>sync_out(4)
        );

    inst_synchrnzr6 : synchrnzr
        port map(
            clk=>clk,
            async_in=>async_2_in(2),
            sync_out=>sync_out(3)
        );

    inst_synchrnzr7 : synchrnzr
        port map(
            clk=>clk,
            async_in=>async_2_in(1),
            sync_out=>sync_out(2)
        );

    inst_synchrnzr8 : synchrnzr
        port map(
            clk=>clk,
            async_in=>async_2_in(0),
            sync_out=>sync_out(1)
        );

    inst_synchrnzr9 : synchrnzr
        port map(
            clk=>clk,
            async_in=>async_3_in,
            sync_out=>sync_out(0)
        );
        
end structural;